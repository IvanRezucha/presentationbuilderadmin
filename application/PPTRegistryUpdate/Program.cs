﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Microsoft.Win32;

namespace PPTRegistryUpdate
{
    class Program
    {
        static void Main(string[] args)
        {
            string officeVersionKey = (string)Registry.GetValue(@"HKEY_CLASSES_ROOT\PowerPoint.Application\CurVer", "", null);

            if (officeVersionKey == null)
            {
                ////Office Not Installed or Registry Issues
                //MessageBox.Show("Microsoft PowerPoint does not appear to be installed. " + Properties.Settings.Default.AppNameFull.ToString() + "will now close.", "Warning");
                //Application.Exit();
                Console.WriteLine("Sorry, Microsoft PowerPoint does not appear to be installed.");
            }
            else if (officeVersionKey.Equals("PowerPoint.Application.12"))
            {
                //MessageBox.Show("BROWSER FLAGS SET HERE");
                //Office 2007
                try
                {
                    //Make/edit registry keys necessary to cause ppts to open in browser instead of application
                    Registry.ClassesRoot.CreateSubKey(@"PowerPoint.Show.8").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                    Registry.ClassesRoot.CreateSubKey(@"PowerPoint.SlideShow.8").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                    Registry.ClassesRoot.CreateSubKey(@"PowerPoint.Template.8").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                    Registry.ClassesRoot.CreateSubKey(@"PowerPoint.Presentation.12").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                    Registry.ClassesRoot.CreateSubKey(@"PowerPoint.Template.12").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                    Registry.ClassesRoot.CreateSubKey(@"PowerPoint.Show.12").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                    Registry.ClassesRoot.CreateSubKey(@"PowerPoint.TemplateMacroEnabled.12").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                    Registry.ClassesRoot.CreateSubKey(@"PowerPoint.ShowMacroEnabled.12").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                    Registry.ClassesRoot.CreateSubKey(@"PowerPoint.MacroEnabled.Presentation.12").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                    Registry.ClassesRoot.CreateSubKey(@"PowerPoint.MacroEnabled.Show.12").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                    Registry.ClassesRoot.CreateSubKey(@"PowerPoint.MacroEnabled.Template.12").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                    RegistryKey regKeyAppRoot = Registry.CurrentUser.CreateSubKey(@"Software\Microsoft\Windows\Shell\AttachmentExecute\{0002DF01-0000-0000-C000-000000000046}");
                    regKeyAppRoot.SetValue("PowerPoint.Show.8", 0, 0);
                    Console.WriteLine("Success!  Registry updated, Office 2007.");
                }
                catch (System.Security.SecurityException)
                {
                    Console.WriteLine("Sorry, You have inadequate permissions to perform the changes needed.");
                }
            }
            else if (officeVersionKey.Equals("PowerPoint.Application.11"))
            {
                //Office 2003
                try
                {
                    //writes registry setting to suppress powerpoint open/save dialog box when loading ppt to browser
                    RegistryKey regKeyAppRoot = Registry.CurrentUser.CreateSubKey(@"Software\Microsoft\Windows\Shell\AttachmentExecute\{0002DF01-0000-0000-C000-000000000046}");
                    regKeyAppRoot.SetValue("PowerPoint.Show.8", 0, 0);
                    Console.WriteLine("Success!  Registry updated, Office 2003.");
                }
                catch (System.Security.SecurityException)
                {
                    Console.WriteLine("Sorry, You have inadequate permissions to perform the changes needed.");
                }
            }
            else
            {
                //Unknown version of Office
                //MessageBox.Show("The currently installed version of Microsoft PowerPoint is not optimized for " + Properties.Settings.Default.AppNameFull.ToString() + ", this may cause the program to behave in an unexpected manner.","Warning");
                RegistryKey regKeyAppRoot = Registry.CurrentUser.CreateSubKey(@"Software\Microsoft\Windows\Shell\AttachmentExecute\{0002DF01-0000-0000-C000-000000000046}");
                regKeyAppRoot.SetValue("PowerPoint.Show.8", 0, 0);
                Console.WriteLine("Success!  Registry updated, Unknown Office version.");
            }
            Console.WriteLine("Press <Enter> to exit ...");
            Console.ReadLine();
        }
    }
}
