USE [HpPbLaserJet]
GO

/****** Object:  Table [dbo].[EventTypes]    Script Date: 08/25/2009 11:08:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[EventTypes](
	[TypeID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](20) NOT NULL,
 CONSTRAINT [PK_EventTypes] PRIMARY KEY CLUSTERED 
(
	[TypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

-- populate default values in EventTypes
insert into EventTypes (Name) values 
    ('Viewed'),
    ('Played'),
    ('Saved'),
    ('Exported'),
    ('Printed')
GO

USE [HpPbLaserJet]
GO

/****** Object:  Table [dbo].[Sessions]    Script Date: 08/25/2009 11:14:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Sessions](
	[Session] [varchar](36) NOT NULL,
	[UserID] [int] NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NULL,
 CONSTRAINT [PK_Sessions] PRIMARY KEY CLUSTERED 
(
	[Session] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[Sessions]  WITH CHECK ADD  CONSTRAINT [FK_Sessions_UserProfile] FOREIGN KEY([UserID])
REFERENCES [dbo].[UserProfile] ([UserID])
GO

ALTER TABLE [dbo].[Sessions] CHECK CONSTRAINT [FK_Sessions_UserProfile]
GO

USE [HpPbLaserJet]
GO

/****** Object:  Table [dbo].[Events]    Script Date: 08/25/2009 11:11:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Events](
	[EventID] [bigint] IDENTITY(1,1) NOT NULL,
	[SlideID] [int] NOT NULL,
	[Session] [varchar](36) NOT NULL,
	[TypeID] [int] NOT NULL,
	[CategoryID] [int] NULL,
	[Tag] [varchar](255) NULL,
	[EventDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Events] PRIMARY KEY CLUSTERED 
(
	[EventID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[Events]  WITH CHECK ADD  CONSTRAINT [FK_Events_EventTypes] FOREIGN KEY([TypeID])
REFERENCES [dbo].[EventTypes] ([TypeID])
GO

ALTER TABLE [dbo].[Events] CHECK CONSTRAINT [FK_Events_EventTypes]
GO

ALTER TABLE [dbo].[Events]  WITH CHECK ADD  CONSTRAINT [FK_Events_Sessions] FOREIGN KEY([Session])
REFERENCES [dbo].[Sessions] ([Session])
GO

ALTER TABLE [dbo].[Events] CHECK CONSTRAINT [FK_Events_Sessions]
GO

ALTER TABLE [dbo].[Events]  WITH CHECK ADD  CONSTRAINT [FK_Events_Categories] FOREIGN KEY(CategoryID)
REFERENCES [dbo].Category ([CategoryID])
GO

set identity_insert Category on
go
if not exists(select 1 from Category where CategoryID = -2)
begin
	insert into Category
	(
		CategoryID,
		CategoryTypeID,
		Category,
		StringSet,
		Depth,
		ParentCategoryID,
		IsProtected,
		SystemName,
		IsNew,
		Icon,
		ShowDescendantSlides
	) values (
		-2,
		NULL,
		'Uncategorized',
		NULL,
		1,
		NULL,
		1,
		'Uncategorized',
		0,
		NULL,
		0
	)
end
go
set identity_insert Category off
go