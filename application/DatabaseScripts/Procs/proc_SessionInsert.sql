-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE proc_SessionInsert
	@SessionID bigint = 0 output,
	@Session varchar(36),
	@UserID int,
	@StartDate datetime
AS
BEGIN
	SET NOCOUNT ON
	insert into Sessions (
		UserID, Session, StartDate
	) values (
		@UserID, @Session, @StartDate
	)
	select @SessionID = @@IDENTITY
	select @SessionID as SessionID
END
GO
