-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE proc_UserEventInsert
	@EventID bigint = 0 output,
	@SlideID int,
	@Session varchar(36),
	@CategoryID int = null,
	@TypeID int,
	@Tag varchar(255),
	@EventDate datetime
AS
BEGIN
	SET NOCOUNT ON;
	
	if exists(select 0 from Sessions where Session = @Session)
	begin
		insert into Events (
			SlideID, Session, TypeID, CategoryID, Tag, EventDate
		) values (
			@SlideID, @Session, @TypeID, @CategoryID, @Tag, @EventDate
		)
		select @EventID = @@IDENTITY
		select @EventID as EventID
	end
	else
		select 0
END
GO
