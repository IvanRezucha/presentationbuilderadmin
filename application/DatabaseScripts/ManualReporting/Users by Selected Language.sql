SELECT d.RegionName, e.LongName, Count(*) as "Number of Users"
FROM HpPbLaserJet.dbo.UserProfile a
join HpPbLaserJet.dbo.UserRegionLanguage b on a.UserID = b.UserID
join dbo.RegionLanguage c on b.RegionLanguageID = c.RegionLanguageID
join dbo.Region d on c.RegionID = d.RegionID
join dbo.Language e on c.LanguageID = e.LanguageID
Group BY d.RegionName, e.LongName
order by d.RegionName, e.LongName