SELECT     A.FirstName, A.LastName, C.Name AS Organization, COUNT(*) AS NumberOfLogins
FROM         UserProfile AS A INNER JOIN
                      UserLoginHistory AS B ON A.UserID = B.UserID INNER JOIN
                      JobTitles AS C ON A.JobTitle = C.Code
WHERE     (B.LoginApp = 'Client')
          AND B.LoginDate > '2009-05-01 00:00:00.001' AND B.LoginDate < '2009-08-01 00:00:00.001'
GROUP BY A.FirstName, A.LastName, C.Name