SELECT A.UserID
     , A.LastName 
     , A.FirstName 
     , C.[Name] as Organization 
     , D.[Name] as Country 
     , B.LoginApp 
     , Max(B.LoginDate) as LastLogin 
     , Count(*) as NumberOfLogins
FROM  UserProfile      A
    , UserLoginHistory B
    , JobTitles        C
    , Countries        D
WHERE A.UserId   = B.UserId
  AND A.Country  = D.Code
  AND A.JobTitle = C.Code
  AND B.LoginDate > '2009-05-01 00:00:00.001' AND B.LoginDate < '2009-08-01 00:00:00.001'

Group By A.UserID, A.FirstName, A.LastName, C.[Name], D.[Name], B.LoginApp
Order By A.LastName, B.LoginApp, LastLogin Desc
