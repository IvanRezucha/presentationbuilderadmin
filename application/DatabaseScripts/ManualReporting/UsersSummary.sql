-- '2009-04-30 23:59:59.000' and InstallDate < '2009-08-01 00:00:00.001'
declare @fyStartDate datetime = '2009-01-01 00:00:00.001'
declare @endDate datetime = '2009-08-01 00:00:00.001'
declare @logins30 int = 0
declare @logins60 int = 0
declare @logins90 int = 0
declare @logins12mo int = 0
declare @ytd int = 0

select @logins30 = COUNT(UserID) 
	from UserProfile
	where LastLoginDate > DateAdd(DD, -30, @endDate) and  LastLoginDate < @endDate
	--or InstallDate > DATEADD(dd, -30, @endDate) and InstallDate < @endDate
	

select @logins60 = COUNT(UserID) 
	from UserProfile
	where LastLoginDate > DateAdd(DD, -60, @endDate) and LastLoginDate < @endDate
	--or InstallDate > DATEADD(DD, -60, @endDate) and InstallDate < @endDate
	

select @logins90 = COUNT(UserID) 
	from UserProfile
	where LastLoginDate > DateAdd(DD, -90, @endDate) and LastLoginDate < @endDate
	--or InstallDate > DATEADD(dd, -90, @endDate) and InstallDate < @endDate
	

select @ytd = COUNT(UserID) 
	from UserProfile
	where LastLoginDate > @fyStartDate and LastLoginDate < @endDate
	--or InstallDate > DATEADD(YY, -1, @endDate) and InstallDate < @endDate
	

select @logins12mo = COUNT(UserID) 
	from UserProfile
	where LastLoginDate > DateAdd(YY, -1, @endDate) and LastLoginDate < @endDate
	--or InstallDate > DATEADD(YY, -1, @endDate) and InstallDate < @endDate
	
select @logins30 as 'Last 30 Days',
	@logins60 as 'Last 60 Days',
	@logins90 as 'Last 90 Days',
	@logins12mo as 'last 12 Months',
	@ytd as 'Year to Date'
	
declare @new30 int = 0
declare @new60 int = 0
declare @new90 int = 0


select COUNT(UserID) 
	from UserProfile
	where LastLoginDate > DateAdd(YY, -1, @endDate) and LastLoginDate < @endDate
	or InstallDate > DATEADD(YY, -1, @endDate) and InstallDate < @endDate
-- an active user is anyone who has logged in to a tool in the last 12 months or anyone who has registered in the last 12 months.

select MIN(LoginDate) from UserLoginHistory