/*Create User Login History Table */
CREATE TABLE [dbo].[UserLoginHistory] (
	[UserID] [int] NULL ,
	[LoginDate] [datetime] NULL ,
	[LoginApp] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

/*Create Job Titles Table */
CREATE TABLE [dbo].[JobTitles] (
	[Code] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

/*Create Countries Table*/
CREATE TABLE [dbo].[Countries] (
	[Code] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

/*Alter User Profile Table - Add 3 Columns*/
ALTER TABLE [dbo].[UserProfile] ADD 
        [Password] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
        [TemporaryPasswordInUse] [bit] NOT NULL DEFAULT (0),
        [RegionId] [int] NOT NULL DEFAULT (0),
        CONSTRAINT [DF_UserProfile_CompanySize] DEFAULT (0) FOR [CompanySize]
GO

/*Insert rows into JobTitles table */
Insert Into JobTitles ([Code],[Name]) values ('CPR','Channel Partner/Reseller'); 
Insert Into JobTitles ([Code],[Name]) values ('HPM','HP Marketing');
Insert Into JobTitles ([Code],[Name]) values ('HPO','HP Other');
Insert Into JobTitles ([Code],[Name]) values ('HPS','HP Sales');
Insert Into JobTitles ([Code],[Name]) values ('HPT','HP Training');
Insert Into JobTitles ([Code],[Name]) values ('OTH','Other');
Insert Into JobTitles ([Code],[Name]) values ('SPR','Supplies Reseller');
Go

/*Insert rows into countries table */
Insert Into Countries ([Code],[Name]) Values ('AG','Argentina');
Insert Into Countries ([Code],[Name]) Values ('AS','Austria');
Insert Into Countries ([Code],[Name]) Values ('AU','Australia');
Insert Into Countries ([Code],[Name]) Values ('BE','Belgium');
Insert Into Countries ([Code],[Name]) Values ('BO','Bolivia');
Insert Into Countries ([Code],[Name]) Values ('BR','Brazil');
Insert Into Countries ([Code],[Name]) Values ('BU','Bulgaria');
Insert Into Countries ([Code],[Name]) Values ('CA','Canada');
Insert Into Countries ([Code],[Name]) Values ('CH','China');
Insert Into Countries ([Code],[Name]) Values ('CL','Chile');
Insert Into Countries ([Code],[Name]) Values ('CO','Columbia');
Insert Into Countries ([Code],[Name]) Values ('CR','Caribbean');
Insert Into Countries ([Code],[Name]) Values ('CT','Croatia');
Insert Into Countries ([Code],[Name]) Values ('CZ','Czech Republic');
Insert Into Countries ([Code],[Name]) Values ('DE','Denmark');
Insert Into Countries ([Code],[Name]) Values ('EC','Ecuador');
Insert Into Countries ([Code],[Name]) Values ('ES','Estonia');
Insert Into Countries ([Code],[Name]) Values ('FI','Finland');
Insert Into Countries ([Code],[Name]) Values ('FR','France');
Insert Into Countries ([Code],[Name]) Values ('GC','Greece');
Insert Into Countries ([Code],[Name]) Values ('GR','Germany');
Insert Into Countries ([Code],[Name]) Values ('HK','Hong Kong');
Insert Into Countries ([Code],[Name]) Values ('HU','Hungary');
Insert Into Countries ([Code],[Name]) Values ('ID','Indonesia');
Insert Into Countries ([Code],[Name]) Values ('IN','India');
Insert Into Countries ([Code],[Name]) Values ('IR','Ireland');
Insert Into Countries ([Code],[Name]) Values ('IS','Israel');
Insert Into Countries ([Code],[Name]) Values ('IT','Italy');
Insert Into Countries ([Code],[Name]) Values ('JP','Japan');
Insert Into Countries ([Code],[Name]) Values ('KO','Korea');
Insert Into Countries ([Code],[Name]) Values ('LI','Lithuania');
Insert Into Countries ([Code],[Name]) Values ('LT','Latvia');
Insert Into Countries ([Code],[Name]) Values ('MA','Malaysia');
Insert Into Countries ([Code],[Name]) Values ('MX','Mexico');
Insert Into Countries ([Code],[Name]) Values ('NE','Netherlands');
Insert Into Countries ([Code],[Name]) Values ('NO','Norway');
Insert Into Countries ([Code],[Name]) Values ('NZ','New Zealand');
Insert Into Countries ([Code],[Name]) Values ('PA','Paraguay');
Insert Into Countries ([Code],[Name]) Values ('PE','Peru');
Insert Into Countries ([Code],[Name]) Values ('PH','Philippines');
Insert Into Countries ([Code],[Name]) Values ('PO','Poland');
Insert Into Countries ([Code],[Name]) Values ('PR','Portugal');
Insert Into Countries ([Code],[Name]) Values ('PU','Puerto Rico');
Insert Into Countries ([Code],[Name]) Values ('RO','Romania');
Insert Into Countries ([Code],[Name]) Values ('RU','Russian Federation');
Insert Into Countries ([Code],[Name]) Values ('SA','South Africa');
Insert Into Countries ([Code],[Name]) Values ('SI','Singapore');
Insert Into Countries ([Code],[Name]) Values ('SL','Slovak Republic');
Insert Into Countries ([Code],[Name]) Values ('SP','Spain');
Insert Into Countries ([Code],[Name]) Values ('SV','Slovenia');
Insert Into Countries ([Code],[Name]) Values ('SW','Sweden');
Insert Into Countries ([Code],[Name]) Values ('SZ','Switzerland');
Insert Into Countries ([Code],[Name]) Values ('TA','Taiwan');
Insert Into Countries ([Code],[Name]) Values ('TH','Thailand');
Insert Into Countries ([Code],[Name]) Values ('TU','Turkey');
Insert Into Countries ([Code],[Name]) Values ('UG','Uruguay');
Insert Into Countries ([Code],[Name]) Values ('UK','United Kingdom');
Insert Into Countries ([Code],[Name]) Values ('UR','Ukraine');
Insert Into Countries ([Code],[Name]) Values ('US','United States');
Insert Into Countries ([Code],[Name]) Values ('VI','Vietnam');
Insert Into Countries ([Code],[Name]) Values ('VZ','Venezuela');
Insert Into Countries ([Code],[Name]) Values ('ZZ','Other');

Go