using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using System.Diagnostics;

using ICSharpCode.SharpZipLib.Checksums;
using ICSharpCode.SharpZipLib.Zip;
using ICSharpCode.SharpZipLib.GZip;

namespace PlugIns.Helpers
{
    public static class ZipHelper
    {

        #region GetZippedFileBytes
        public static byte[] GetZippedFileBytes(System.Collections.ArrayList filePaths, string tempFilePath, string diagFile)
        {
            
            Crc32 crc = new Crc32();
            //MemoryStream ms = new MemoryStream();
            //ZipOutputStream s = new ZipOutputStream(ms);
            ZipOutputStream s = new ZipOutputStream(File.Create(tempFilePath));

            s.SetLevel(9); // 0 - store only to 9 - means best compression

            foreach (string filePath in filePaths)
            {
                try
                {
                    byte[] buffer = File.ReadAllBytes(filePath);
                    ZipEntry entry = new ZipEntry(Path.GetFileName(filePath));

                    entry.DateTime = DateTime.Now;

                    // set Size and the crc, because the information
                    // about the size and crc should be stored in the header
                    // if it is not set it is automatically written in the footer.
                    // (in this case size == crc == -1 in the header)
                    // Some ZIP programs have problems with zip files that don't store
                    // the size and crc in the header.
                    entry.Size = buffer.Length;

                    crc.Reset();
                    crc.Update(buffer);

                    entry.Crc = crc.Value;

                    s.PutNextEntry(entry);

                    s.Write(buffer, 0, buffer.Length);
                }
                catch (Exception ex)
                {
                    //output to log file
                    WriteToTrace(ex, filePath, diagFile);
                }
            }

            //byte[] zippedBytes = ms.ToArray();
            
            s.Finish();
            s.Close();
            byte[] zippedBytes = File.ReadAllBytes(tempFilePath);
            File.Delete(tempFilePath);
            return zippedBytes;
        }

        public static byte[] GetZippedFileBytes(System.Collections.ArrayList filePaths, string tempFilePath)
        {
            Crc32 crc = new Crc32();
            //MemoryStream ms = new MemoryStream();
            //ZipOutputStream s = new ZipOutputStream(ms);
            ZipOutputStream s = new ZipOutputStream(File.Create(tempFilePath));

            s.SetLevel(9); // 0 - store only to 9 - means best compression

            foreach (string filePath in filePaths)
            {
                byte[] buffer = File.ReadAllBytes(filePath);
                ZipEntry entry = new ZipEntry(Path.GetFileName(filePath));

                entry.DateTime = DateTime.Now;

                // set Size and the crc, because the information
                // about the size and crc should be stored in the header
                // if it is not set it is automatically written in the footer.
                // (in this case size == crc == -1 in the header)
                // Some ZIP programs have problems with zip files that don't store
                // the size and crc in the header.
                entry.Size = buffer.Length;

                crc.Reset();
                crc.Update(buffer);

                entry.Crc = crc.Value;

                s.PutNextEntry(entry);

                s.Write(buffer, 0, buffer.Length);
            }

            //byte[] zippedBytes = ms.ToArray();

            s.Finish();
            s.Close();
            byte[] zippedBytes = File.ReadAllBytes(tempFilePath);
            File.Delete(tempFilePath);
            return zippedBytes;
        }
        #endregion

        #region WriteZippedFileBytes
        public static void WriteZippedFileBytes(byte[] buffer, string writeToPath, string tempFilePath, string diagFile)
        {
            //MemoryStream ms = new MemoryStream(buffer);
            //ZipInputStream s = new ZipInputStream(ms);

            File.WriteAllBytes(tempFilePath, buffer);
            ZipInputStream s = new ZipInputStream(File.OpenRead(tempFilePath));

            ZipEntry theEntry;
            while ((theEntry = s.GetNextEntry()) != null)
            {
                try
                {
                    string fileName = theEntry.Name;

                    if (fileName != String.Empty)
                    {
                        FileStream streamWriter = File.Create(writeToPath + @"\" + theEntry.Name);

                        int size = 2048;
                        byte[] data = new byte[2048];
                        while (true)
                        {
                            size = s.Read(data, 0, data.Length);
                            if (size > 0)
                            {
                                streamWriter.Write(data, 0, size);
                            }
                            else
                            {
                                break;
                            }
                        }

                        streamWriter.Close();
                    }
                }
                catch (Exception ex)
                {
                    //output to log file
                    WriteToTrace(ex, theEntry.Name, diagFile);
                }
            }
            s.Close();
            File.Delete(tempFilePath);
        }

        public static void WriteZippedFileBytes(byte[] buffer, string writeToPath, string tempFilePath)
        {
            //MemoryStream ms = new MemoryStream(buffer);
            //ZipInputStream s = new ZipInputStream(ms);

            File.WriteAllBytes(tempFilePath, buffer);
            ZipInputStream s = new ZipInputStream(File.OpenRead(tempFilePath));

            ZipEntry theEntry;
            while ((theEntry = s.GetNextEntry()) != null)
            {
                string fileName = theEntry.Name;

                if (fileName != String.Empty)
                {
                    FileStream streamWriter = File.Create(writeToPath + @"\" + theEntry.Name);

                    int size = 2048;
                    byte[] data = new byte[2048];
                    while (true)
                    {
                        size = s.Read(data, 0, data.Length);
                        if (size > 0)
                        {
                            streamWriter.Write(data, 0, size);
                        }
                        else
                        {
                            break;
                        }
                    }

                    streamWriter.Close();
                }
            }
            s.Close();
            File.Delete(tempFilePath);
        }
        #endregion

        #region ZipFiles
        public static void ZipFiles(System.Collections.ArrayList filePaths, string writeToFilePath, string diagFile)
        {
            Crc32 crc = new Crc32();
            //MemoryStream ms = new MemoryStream();
            //ZipOutputStream s = new ZipOutputStream(ms);
            ZipOutputStream s = new ZipOutputStream(File.Create(writeToFilePath));

            s.SetLevel(9); // 0 - store only to 9 - means best compression

            foreach (string filePath in filePaths)
            {
                try
                {
                    if (File.Exists(filePath))
                    {
                        byte[] buffer = File.ReadAllBytes(filePath);
                        ZipEntry entry = new ZipEntry(Path.GetFileName(filePath));

                        entry.DateTime = DateTime.Now;

                        // set Size and the crc, because the information
                        // about the size and crc should be stored in the header
                        // if it is not set it is automatically written in the footer.
                        // (in this case size == crc == -1 in the header)
                        // Some ZIP programs have problems with zip files that don't store
                        // the size and crc in the header.
                        entry.Size = buffer.Length;

                        crc.Reset();
                        crc.Update(buffer);

                        entry.Crc = crc.Value;

                        s.PutNextEntry(entry);

                        s.Write(buffer, 0, buffer.Length);
                    }
                }
                catch (Exception ex)
                {
                    //output to log file
                    WriteToTrace(ex, filePath, diagFile);
                }
            }

            //byte[] zippedBytes = ms.ToArray();

            s.Finish();
            s.Close();
        }

        public static void ZipFiles(System.Collections.ArrayList filePaths, string writeToFilePath)
        {
            Crc32 crc = new Crc32();
            //MemoryStream ms = new MemoryStream();
            //ZipOutputStream s = new ZipOutputStream(ms);
            ZipOutputStream s = new ZipOutputStream(File.Create(writeToFilePath));

            s.SetLevel(9); // 0 - store only to 9 - means best compression

            foreach (string filePath in filePaths)
            {
                if (File.Exists(filePath))
                {
                    byte[] buffer = File.ReadAllBytes(filePath);
                    ZipEntry entry = new ZipEntry(Path.GetFileName(filePath));

                    entry.DateTime = DateTime.Now;

                    // set Size and the crc, because the information
                    // about the size and crc should be stored in the header
                    // if it is not set it is automatically written in the footer.
                    // (in this case size == crc == -1 in the header)
                    // Some ZIP programs have problems with zip files that don't store
                    // the size and crc in the header.
                    entry.Size = buffer.Length;

                    crc.Reset();
                    crc.Update(buffer);

                    entry.Crc = crc.Value;

                    s.PutNextEntry(entry);

                    s.Write(buffer, 0, buffer.Length);
                }
            }

            //byte[] zippedBytes = ms.ToArray();

            s.Finish();
            s.Close();
        }
        #endregion

        #region UnzipFiles
        public static void UnzipFiles(string zipFile, string writeToPath, bool isZipDeleted, string diagFile)
        {
            ZipInputStream s = new ZipInputStream(File.OpenRead(zipFile));

            ZipEntry theEntry;
            while ((theEntry = s.GetNextEntry()) != null)
            {
                try
                {
                    string fileName = theEntry.Name;

                    if (fileName != String.Empty)
                    {
                        //if the file already exists, make sure it is not read-only
                        if (File.Exists(writeToPath + @"\" + theEntry.Name))
                        {
                            FileInfo fi = new FileInfo(writeToPath + @"\" + theEntry.Name);
                            fi.Attributes = FileAttributes.Normal;
                        }
                        FileStream streamWriter = File.Create(writeToPath + @"\" + theEntry.Name);

                        int size = 2048;
                        byte[] data = new byte[2048];
                        while (true)
                        {
                            size = s.Read(data, 0, data.Length);
                            if (size > 0)
                            {
                                streamWriter.Write(data, 0, size);
                            }
                            else
                            {
                                break;
                            }
                        }

                        streamWriter.Close();
                    }
                }
                catch (Exception ex)
                {
                    //output to log file
                    WriteToTrace(ex, theEntry.Name, diagFile);
                }
            }
            s.Close();

            //delete the zip if isZipDeleted
            if (isZipDeleted)
            {
                File.Delete(zipFile);
            }
        
        }

        public static void UnzipFiles(string zipFile, string writeToPath, bool isZipDeleted)
        {
            ZipInputStream s = new ZipInputStream(File.OpenRead(zipFile));

            ZipEntry theEntry;
            while ((theEntry = s.GetNextEntry()) != null)
            {
                try
                {
                    string fileName = theEntry.Name;

                    if (fileName != String.Empty)
                    {
                        //if the file already exists, make sure it is not read-only
                        if (File.Exists(writeToPath + @"\" + theEntry.Name))
                        {
                            FileInfo fi = new FileInfo(writeToPath + @"\" + theEntry.Name);
                            fi.Attributes = FileAttributes.Normal;
                        }
                        FileStream streamWriter = File.Create(writeToPath + @"\" + theEntry.Name);

                        int size = 2048;
                        byte[] data = new byte[2048];
                        while (true)
                        {
                            size = s.Read(data, 0, data.Length);
                            if (size > 0)
                            {
                                streamWriter.Write(data, 0, size);
                            }
                            else
                            {
                                break;
                            }
                        }

                        streamWriter.Close();
                    }
                }
                catch (Exception ex)
                {
                    //TODO: Handle exception
                }
                
            }
            s.Close();

            //delete the zip if isZipDeleted
            if (isZipDeleted)
            {
                File.Delete(zipFile);
            }
        }
        #endregion

        private static void WriteToTrace(Exception x, string customDetails, string diagFile)
        {
            TextWriterTraceListener diags = new TextWriterTraceListener(diagFile);
            DateTime now = DateTime.Now;

            diags.WriteLine("Date/Time:  " + now.ToShortDateString() + " " + now.ToShortTimeString());
            diags.Flush();

            diags.WriteLine("File Processed:  " + customDetails);
            diags.Flush();

            diags.WriteLine("Exception Message:  " + x.Message);
            diags.Flush();

            diags.WriteLine("Exception Source:  " + x.Source);
            diags.Flush();

            if (x.InnerException != null)
            {
                diags.WriteLine("Inner Exception Message:  " + x.InnerException.Message);
                diags.Flush();
            }

            diags.WriteLine("Stack Trace:  " + System.Environment.NewLine + System.Environment.NewLine + x.StackTrace);
            diags.Flush();

            diags.WriteLine("------------------");
            diags.Flush();
        }
        

        
    }
}
