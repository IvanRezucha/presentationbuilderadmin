using System;
using System.Collections.Generic;
using System.Text;
using Aspose.Slides;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace PlugIns.Helpers
{
    public static class PptUtils
    {
        /// <summary>
        /// Sets the license.
        /// </summary>
        /// <param name="licensePath">The license path.</param>
        public static void SetLicense(string licensePath)
        {
            License lic = new License();
            lic.SetLicense(licensePath);
        }

        /// <summary>
        /// Returns an Image of the specified slide
        /// </summary>
        /// <param name="presentationFilePath"></param>
        /// <param name="slideIndex"></param>
        /// <param name="width">NOTE: obsolete/no effect</param>
        /// <param name="height">NOTE: obsolete/no effect</param>
        /// <param name="licensePath">Application's path to Aspose.Slides.lic.</param>
        /// <returns></returns>
        public static Image GetSlideImage(string presentationFilePath, int slideIndex, int width, int height, string licensePath)
        {
            SetLicense(licensePath);

            //Size imageSize = new Size(width, height);
            Aspose.Slides.Presentation tempPresentation = GetPresentationObject(presentationFilePath);
            //return tempPresentation.Slides[slideIndex].GetThumbnail(96D, 72D);
            //return tempPresentation.Slides[slideIndex].GetThumbnail(imageSize);
            //return tempPresentation.Slides[slideIndex].GetThumbnail(Convert.ToDouble(width/720), Convert.ToDouble(height/576));
            return tempPresentation.Slides[slideIndex].GetThumbnail(0.133, 0.125);

        }

        /// <summary>
        /// Gets the presentation object.
        /// </summary>
        /// <param name="presentationFilePath">The presentation file path.</param>
        /// <returns>The presentation.</returns>
        public static Aspose.Slides.Presentation GetPresentationObject(string presentationFilePath)
        {
            FileStream presStream = new FileStream(presentationFilePath, FileMode.Open, FileAccess.Read);
            Aspose.Slides.Presentation tempPresentation = new Aspose.Slides.Presentation(presStream);
            presStream.Close();
            return tempPresentation;
        }

        /// <summary>
        /// Writes the specified slide image to disk
        /// </summary>
        /// <param name="presentationFilePath"></param>
        /// <param name="imageFilePath"></param>
        /// <param name="slideIndex"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="imageFormat"></param>
        /// <param name="licensePath">application's path to Aspose.Slides.lic</param>
        public static void WriteSlideImage(string presentationFilePath, string imageFilePath, int slideIndex, int width, int height, ImageFormat imageFormat, string licensePath)
        {
            SetLicense(licensePath);

            Image slideImage = GetSlideImage(presentationFilePath, slideIndex, width, height, licensePath);
            slideImage.Save(imageFilePath, imageFormat);
        }

        /// <summary>
        /// Writes the slide images.
        /// </summary>
        /// <param name="presentationFilePaths">The presentation file paths.</param>
        /// <param name="imageFilePath">The image file path.</param>
        /// <param name="slideIndex">Index of the slide.</param>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        /// <param name="imageFormat">The image format.</param>
        /// <param name="licensePath">The license path.</param>
        public static List<string> WriteSlideImages(List<string> presentationFilePaths, string imageFilePath, int slideIndex, int width, int height, ImageFormat imageFormat, string licensePath)
        {
            SetLicense(licensePath);
            string imageFormatFileExtension = GetImageFormatFileExtension(imageFormat);
            List<string> slideImageFiles = new List<string>();
            foreach (string presentationFilePath in presentationFilePaths)
            {
                Image slideImage = GetSlideImage(presentationFilePath, slideIndex, width, height, licensePath);
                string fileName = Path.GetFileNameWithoutExtension(presentationFilePath);
                slideImage.Save(Path.Combine(imageFilePath, fileName + imageFormatFileExtension), imageFormat);
                slideImageFiles.Add(Path.Combine(imageFilePath, fileName + imageFormatFileExtension));
            }
            return slideImageFiles;
        }

        /// <summary>
        /// Gets the image format file extension.
        /// </summary>
        /// <param name="imageFormat">The image format.</param>
        /// <returns></returns>
        private static string GetImageFormatFileExtension(ImageFormat imageFormat)
        {

            if (imageFormat == ImageFormat.Jpeg)
            {
                return ".jpg";
            }
            else if (imageFormat == ImageFormat.Gif)
            {
                return ".gif";
            }
            else if (imageFormat == ImageFormat.Icon)
            {
                return ".ico";
            }
            else if (imageFormat == ImageFormat.Png)
            {
                return ".png";
            }
            else if (imageFormat == ImageFormat.Bmp)
            {
                return ".bmp";
            }
            else if (imageFormat == ImageFormat.Tiff)
            {
                return ".tif";
            }
            else
            {
                return ".jpg";
            }
        }

        /// <summary>
        /// Writes the slides in the give range to files.
        /// </summary>
        /// <param name="presentationFilePath">The presentation file path.</param>
        /// <param name="startSlidePosition">The start slide position.</param>
        /// <param name="endSlidePosition">The end slide position.</param>
        /// <param name="slideFilePath">The slide file path.</param>
        /// <param name="slideFilePrefix">The slide file prefix.</param>
        /// <param name="licensePath">The license path.</param>
        /// <returns>List of files written.</returns>
        public static List<string> WriteSlidesToFiles(string presentationFilePath, int startSlidePosition, int endSlidePosition, string slideFilePath, string slideFilePrefix, string licensePath)
        {
            List<string> newSlideFiles = new List<string>();
            SetLicense(licensePath);

            Aspose.Slides.Presentation tempPresentation = GetPresentationObject(presentationFilePath);
            for (int i = startSlidePosition; i <= endSlidePosition; i++)
            {
                Aspose.Slides.Presentation newPresentation = new Presentation();
                Aspose.Slides.Slide slide = tempPresentation.GetSlideByPosition(i);
                System.Collections.SortedList sl = new System.Collections.SortedList();
                tempPresentation.CloneSlide(slide, i, newPresentation, sl);
                newPresentation.DeleteUnusedMasters();
                newPresentation.Slides.RemoveAt(0);
                string fileName = String.Format("{0}_{1}.pot", slideFilePrefix, i.ToString());
                newPresentation.Write(Path.Combine(slideFilePath, fileName));
                newSlideFiles.Add(Path.Combine(slideFilePath, fileName));
            }
            return newSlideFiles;
        }

        /// <summary>
        /// Writes all the slides to files.
        /// </summary>
        /// <param name="presentationFilePath">The presentation file path.</param>
        /// <param name="slideFilePath">The slide file path.</param>
        /// <param name="slideFilePrefix">The slide file prefix.</param>
        /// <param name="licensePath">The license path.</param>
        /// <returns></returns>
        public static List<string> WriteSlidesToFiles(string presentationFilePath, string slideFilePath, string slideFilePrefix, string licensePath)
        {
            SetLicense(licensePath);
            Aspose.Slides.Presentation tempPresentation = GetPresentationObject(presentationFilePath);
            return WriteSlidesToFiles(presentationFilePath, 1, tempPresentation.Slides.Count, slideFilePath, slideFilePrefix, licensePath);
        }

        /// <summary>
        /// Gets the slide title.  If a slide title cannot be determined the filename is returned.
        /// </summary>
        /// <param name="presentationFilePath">The presentation file path.</param>
        /// <param name="slidePosition">The slide position.</param>
        /// <param name="licensePath">The license path.</param>
        /// <returns>(string) Slide title or filename</returns>
        public static string GetSlideTitle(string presentationFilePath, int slidePosition, string licensePath)
        {
            SetLicense(licensePath);
            Presentation pres = GetPresentationObject(presentationFilePath);
            try
            {
                Slide slide = pres.GetSlideByPosition(slidePosition);
                string title = ((Aspose.Slides.TextHolder)slide.Placeholders[0]).Text;
                title = title.Replace(System.Environment.NewLine, " ");
                title = title.Replace("\t", " ");
                title = title.Replace("\v", " ");
                return title;
            }
            catch (Exception)
            {
                return Path.GetFileName(presentationFilePath);
            }

        }

    }
}
