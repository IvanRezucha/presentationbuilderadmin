using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.IO;

namespace PlugIns.Helpers
{
    public class PowerPointCompressor : BackgroundWorker
    {
        private List<string> _SourcePpts;
        private string _TargetDirectory;
        private bool _AllowJpeg;
        private bool _FlattenEmbeddedObjects;
        private bool _AllowCropping;
        private int _JpegQuality;
       

        public PowerPointCompressor()
        {
            _IsCompressionInProgress = false;
            base.WorkerReportsProgress = true;
            base.DoWork += new DoWorkEventHandler(SlideCompressor_DoWork);
        }

        private bool _IsCompressionInProgress;

        public bool IsCompressionInProgress
        {
            get { return _IsCompressionInProgress; }
            set { _IsCompressionInProgress = value; }
        }

        public void CompressPresentationsAsync(List<string> sourcePpts, string targetDir, bool allowJpeg, bool flattenEmbeddedObjects, int jpegQuality, bool allowCropping)
        {
            if (_IsCompressionInProgress)
            {
                CompressionInProgressException compEx = new CompressionInProgressException("PowerPointCompressor is currently processing files.");
                throw compEx;
            }

            if (jpegQuality < 1 || jpegQuality > 9)
            {
                JpegQualityOutOfRangeException jpegEx = new JpegQualityOutOfRangeException("Jpeg quality out of range. The jpeg quality must be an integer between 1 and 9.");
                throw jpegEx;
            }

            _IsCompressionInProgress = true;
            _SourcePpts = sourcePpts;
            _TargetDirectory = targetDir;
            _AllowJpeg = allowJpeg;
            _FlattenEmbeddedObjects = flattenEmbeddedObjects;
            _JpegQuality = jpegQuality;
            _AllowCropping = allowCropping;
            base.RunWorkerAsync();
        }

        public void CompressPresentation(string sourcePpt, string targetDir, bool allowJpeg, bool flattenEmbeddedObjects, int jpegQuality, bool allowCropping)
        {
            if (_IsCompressionInProgress)
            {
                CompressionInProgressException compEx = new CompressionInProgressException("PowerPointCompresser is currently processing files.");
                throw compEx;
            }

            if (jpegQuality < 1 || jpegQuality > 9)
            {
                JpegQualityOutOfRangeException jpegEx = new JpegQualityOutOfRangeException("Jpeg quality out of range. The jpeg quality must be an integer between 1 and 9.");
                throw jpegEx;
            }

            _IsCompressionInProgress = true;
            _SourcePpts = new List<string>();
            _SourcePpts.Add(sourcePpt);
            _TargetDirectory = targetDir;
            _AllowJpeg = allowJpeg;
            _FlattenEmbeddedObjects = flattenEmbeddedObjects;
            _JpegQuality = jpegQuality;
            _AllowCropping = allowCropping;
            OptimizePresentations();
        }

        

        public void SlideCompressor_DoWork(object sender, DoWorkEventArgs e)
        {
            OptimizePresentations();
        } 

        private void OptimizePresentations()
        {
            NXPLiteCom.COptimizerClass compressor = new NXPLiteCom.COptimizerClass();
            compressor.AllowCropping = _AllowCropping;
            compressor.JpegQuality = _JpegQuality;
            compressor.FlattenEmbeddedObjects = _FlattenEmbeddedObjects;
            compressor.AllowJpeg = _AllowJpeg;

            int filesCompressed = 0;
            double percentCompressed = 0;
            foreach (string sourcePpt in _SourcePpts)
            {
                try
                {
                    compressor.Optimize(sourcePpt, Path.Combine(_TargetDirectory, Path.GetFileName(sourcePpt)));
                }
                catch (Exception ex)
                {

                    throw ex;
                }
                

                //only report progress if we are compressing asynchronously
                if (base.IsBusy)
                {
                    filesCompressed++;
                    percentCompressed = (Convert.ToDouble(filesCompressed) / Convert.ToDouble(_SourcePpts.Count)) * 100.00;
                    base.ReportProgress(Convert.ToInt32(percentCompressed), String.Format("Compressing file {0} of {1}", filesCompressed, _SourcePpts.Count)); 
                }
            }

            _IsCompressionInProgress = false;
        }

    }

    public class JpegQualityOutOfRangeException : ApplicationException
    {
        public JpegQualityOutOfRangeException(string message)
            : base(message)
        { 
        
        }
    }

    public class CompressionInProgressException : ApplicationException
    {
        public CompressionInProgressException(string message)
            : base(message)
        {

        }
    }
}
