using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

using System.IO;
using System.Threading;
using System.Windows.Forms;
using System.Collections;

namespace PlugIns.Helpers
{
    public partial class PptBuilder : BackgroundWorker
    {
        private ArrayList _sourceFiles;

        private int _totalFiles;
        private int _filesProcessed; //includes skipped files
        private int _filesSkipped;
        private ArrayList _skippedFiles;

        Aspose.Slides.License _license;
        private Aspose.Slides.Presentation _newPres;

        #region Constructor

        /// <summary>
        /// constructor
        /// </summary>
        public PptBuilder()
        {
            InitializeComponent();

            base.WorkerReportsProgress = true;
            base.WorkerSupportsCancellation = true;

            _license = new Aspose.Slides.License();
        }
        #endregion

        private bool _SupportsDiagnosticOutput;

        public bool SupportsDiagnosticOutput
        {
            get { return _SupportsDiagnosticOutput; }
            set { _SupportsDiagnosticOutput = value; }
        }

        private string _DiagnosticOutputFile;
            
        public string DiagnosticOutputFile
        {
            get { return _DiagnosticOutputFile; }
            set { _DiagnosticOutputFile = value; }      

          
           
        }

        private string _targetFile;

        /// <summary>
        /// The resulting file from the CreatePresentationAsync method. (Read-only)
        /// </summary>
        public string TargetFile
        {
            get { return _targetFile; }
        }

        private string _AsposeSlideLicensePath;

        /// <summary>
        /// (string) path to the Aspose.Slides dll license
        /// </summary>
        public string AsposeSlideLicensePath
        {
            get { return _AsposeSlideLicensePath; }
            set { _AsposeSlideLicensePath = value; }
        }
	
	

        /// <summary>
        /// Create a new presentation from existing presentation files.
        /// Assumes each source file has one slide only.
        /// </summary>
        /// <param name="sourceFiles"></param>
        /// <param name="targetFile"></param>
        public void CreatePresentationAsync(ArrayList sourceFiles, string targetFile)
        {
            //licenses the Aspose.Slides component
            _license.SetLicense(this.AsposeSlideLicensePath);

            _sourceFiles = sourceFiles;
            _targetFile = targetFile;

            //the target presentation
            _newPres = new Aspose.Slides.Presentation();

            //initialize progress vars
            _totalFiles = _sourceFiles.Count;
            _filesProcessed = 0; //includes skipped files
            _filesSkipped = 0;

			//added if to ensure not more than one background worker run is attempted at a time
			this.RunWorkerAsync();
        }


        /// <summary>
        /// no need to implement this event in your code, the work is already done!
        /// </summary>
        /// <param name="e"></param>
        protected override void OnDoWork(DoWorkEventArgs e)
        {
            base.OnDoWork(e);
            int progressPercent;
            Aspose.Slides.Slide firstSlideInNewPres = _newPres.GetSlideByPosition(1);
            
            foreach (string fileName in _sourceFiles)
            {
                progressPercent = Convert.ToInt32((Convert.ToDouble(_filesProcessed) /Convert.ToDouble( _totalFiles)) * 100.0);
                base.ReportProgress(progressPercent, String.Format("Adding slide {0} of {1}", _filesProcessed + 1, _totalFiles));
                
                if (File.Exists(fileName))
                {
                    try
                    {
                        //aspose makes you declare and supply the SortedList for the CloneSlide method
                        System.Collections.SortedList sortedList = new System.Collections.SortedList();
                        Aspose.Slides.Presentation importPres = new Aspose.Slides.Presentation(fileName);
                        Aspose.Slides.Slide slide = importPres.GetSlideByPosition(1);
                        importPres.CloneSlide(slide, _newPres.Slides.LastSlidePosition + 1, _newPres, sortedList);
                    }
                    catch (Exception ex)
                    {
                        _filesSkipped++;
                    }
                }
                else
                {
                    _filesSkipped++;
                }

                _filesProcessed++;
               
                progressPercent = Convert.ToInt32((Convert.ToDouble(_filesProcessed) /Convert.ToDouble( _totalFiles)) * 100.0);
                base.ReportProgress(progressPercent, String.Format("Adding slide {0} of {1}", _filesProcessed, _totalFiles));
            }
            _newPres.Slides.Remove(firstSlideInNewPres);
            try
            {
                _newPres.Write(_targetFile);
                e.Result = "Success";
            }
            catch (Exception ex)
            {
                e.Result = ex;
            }
        }

    }
}
