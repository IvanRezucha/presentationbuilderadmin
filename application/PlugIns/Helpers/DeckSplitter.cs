using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.ComponentModel;
using Aspose.Slides;

namespace PlugIns.Helpers
{
    public class DeckSplitter
    {
        private List<string> _NewSlideFiles;
        private BackgroundWorker _BgWorker;
        private string _PresentationFilePath;
        private int _StartSlidePosition;
        private int _EndSlidePosition;
        private string _SlideFilePath;
        private string _SlideFilePrefix;


        public DeckSplitter()
        {

        }

        /// <summary>
        /// Writes the slides in the give range to files.
        /// </summary>
        /// <param name="presentationFilePath">The presentation file path.</param>
        /// <param name="startSlidePosition">The start slide position.</param>
        /// <param name="endSlidePosition">The end slide position.</param>
        /// <param name="slideFilePath">The slide file path.</param>
        /// <param name="slideFilePrefix">The slide file prefix.</param>
        /// <param name="licensePath">The license path.</param>
        /// <returns>List of files written.</returns>
        public void WriteSlidesToFilesAsync(string presentationFilePath, int startSlidePosition, int endSlidePosition, string slideFilePath, string slideFilePrefix, string licensePath)
        {
            _NewSlideFiles = new List<string>();
            PptUtils.SetLicense(licensePath);
            WriteSlidesToFiles(presentationFilePath, startSlidePosition, endSlidePosition, slideFilePath, slideFilePrefix);
        }

        /// <summary>
        /// Writes all the slides to files.
        /// </summary>
        /// <param name="presentationFilePath">The presentation file path.</param>
        /// <param name="slideFilePath">The slide file path.</param>
        /// <param name="slideFilePrefix">The slide file prefix.</param>
        /// <param name="licensePath">The license path.</param>
        /// <returns></returns>
        public void WriteSlidesToFilesAsync(string presentationFilePath, string slideFilePath, string slideFilePrefix, string licensePath)
        {
            PptUtils.SetLicense(licensePath);
            Aspose.Slides.Presentation tempPresentation = PptUtils.GetPresentationObject(presentationFilePath);
            WriteSlidesToFiles(presentationFilePath, 1, tempPresentation.Slides.Count, slideFilePath, slideFilePrefix);
        }

        private void WriteSlidesToFiles(string presentationFilePath, int startSlidePosition, int endSlidePosition, string slideFilePath, string slideFilePrefix)
        {
            _BgWorker = new BackgroundWorker();
            _BgWorker.WorkerReportsProgress = true;
            _BgWorker.ProgressChanged += new ProgressChangedEventHandler(_BgWorker_ProgressChanged);
            _BgWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(_BgWorker_RunWorkerCompleted);
            _BgWorker.DoWork += new DoWorkEventHandler(_BgWorker_DoWork);

            _PresentationFilePath = presentationFilePath;
            _StartSlidePosition = startSlidePosition;
            _EndSlidePosition = endSlidePosition;
            _SlideFilePath = slideFilePath;
            _SlideFilePrefix = slideFilePrefix;
            _BgWorker.RunWorkerAsync();

        }

        private void _BgWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            Aspose.Slides.Presentation tempPresentation = PptUtils.GetPresentationObject(_PresentationFilePath);
            _NewSlideFiles = new List<string>();
            int slidesProcessed = 0;
            double percentProcessed = 0;
            for (int i = _StartSlidePosition; i <= _EndSlidePosition; i++)
            {
                try
                {
                    Aspose.Slides.Presentation newPresentation = new Presentation();
                    //the deleteSlide is the blank slide that is automatically inserted into the presentation when it is created
                    //we will want to delete this eventually

                    Aspose.Slides.Slide deleteSlide = newPresentation.GetSlideByPosition(1);
                    newPresentation.SlideSize = tempPresentation.SlideSize;
                    Aspose.Slides.Slide slide = tempPresentation.GetSlideByPosition(i);
                    System.Collections.SortedList sl = new System.Collections.SortedList();
                    tempPresentation.CloneSlide(slide, i, newPresentation, sl);
                    newPresentation.DeleteUnusedMasters();
                    newPresentation.Slides.Remove(deleteSlide);
                    string fileName = String.Format("{0}_{1:00#}.pot", _SlideFilePrefix, slide.SlidePosition);
                    newPresentation.Write(Path.Combine(_SlideFilePath, fileName));
                    _NewSlideFiles.Add(Path.Combine(_SlideFilePath, fileName));

                    slidesProcessed++;
                    percentProcessed = (Convert.ToDouble(slidesProcessed) / Convert.ToDouble(_EndSlidePosition - _StartSlidePosition + 1)) * 100.00;
                    _BgWorker.ReportProgress(Convert.ToInt32(percentProcessed), String.Format("Creating slide file {0} of {1}", slidesProcessed, _EndSlidePosition - _StartSlidePosition + 1));
                }
                catch (Exception ex)
                {
                    //skip it
                }
            }
        }

        private void _BgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            WriteSlidesToFilesProgressCompleted(_NewSlideFiles);
        }



        private void _BgWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            WriteSlidesToFilesProgressReported(e.ProgressPercentage, e.UserState.ToString());
        }

        public delegate void WriteSlidesToFilesProgressReportedDelegate(int progressPercent, string message);

        public event WriteSlidesToFilesProgressReportedDelegate WriteSlidesToFilesProgressReported;

        public delegate void WriteSlidesToFilesCompletedDelegate(List<string> newSlideFiles);

        public event WriteSlidesToFilesCompletedDelegate WriteSlidesToFilesProgressCompleted;


    }
}
