using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace PlugIns.Helpers
{
    public static class ExternalWindowController
    {

        [DllImport("user32.dll")]
        private static extern bool SetForegroundWindow(IntPtr hWnd);
        [DllImport("user32.dll")]
        private static extern bool ShowWindowAsync(IntPtr hWnd, int nCmdShow);
        [DllImport("user32.dll")]
        private static extern bool IsIconic(IntPtr hWnd);

        private const int SW_HIDE = 0;
        private const int SW_SHOWNORMAL = 1;
        private const int SW_SHOWMINIMIZED = 2;
        private const int SW_SHOWMAXIMIZED = 3;
        private const int SW_SHOWNOACTIVATE = 4;
        private const int SW_RESTORE = 9;
        private const int SW_SHOWDEFAULT = 10;

        /// <summary>
        /// Brings the window for the supplied process name to the foreground.
        /// </summary>
        /// <param name="processName">Name of the process you want to bring to the front.</param>
        public static void BringWindowToFront(string processName, ProcessIndex processIndex)
        {
            IntPtr hWnd = GetWindowHandle(processName, processIndex);
            BringToFront(hWnd);
            return;
        }

        /// <summary>
        /// Brings the window for the supplied process to the foreground.
        /// </summary>
        /// <param name="process">The process you want to bring to the front.</param>
        public static void BringWindowToFront(Process process)
        {
            if (process != null)
            {
                IntPtr hWnd = process.MainWindowHandle;
                BringToFront(hWnd);
                return;
            }
            else
            {
                NullProcessException ex = new NullProcessException();
                throw ex;
            }
        }

        /// <summary>
        /// Brings the window for the supplied window handle to the foreground.
        /// </summary>
        /// <param name="mainWindowHandle">The window handle you want to bring to the front</param>
        public static void BringWindowToFront(IntPtr mainWindowHandle)
        {
            BringToFront(mainWindowHandle);
        }

        /// <summary>
        /// Gets the window handle for a process.
        /// </summary>
        /// <param name="processName">Name of the process.</param>
        /// <returns>IntPtr, a pointer to the window.</returns>
        private static IntPtr GetWindowHandle(string processName, ProcessIndex processIndex)
        {
            IntPtr hWnd;
            // get the list of all processes by that name
            List<Process> processes = new List<Process>(Process.GetProcessesByName(processName));

            // if there is more than one process...
            if (processes.Count >= 1)
            {
                if (processIndex == ProcessIndex.FirstAvailable)
                {
                    hWnd = processes[0].MainWindowHandle;
                }
                else// if (processIndex == ProcessIndex.MostRecent)
                {
                    hWnd = processes[processes.Count - 1].MainWindowHandle;
                }
                return hWnd;
            }
            else
            {
                ProcessNotRunningException ex = new ProcessNotRunningException(processName);
                throw ex;
            }
        }

        /// <summary>
        /// Brings a window to the foreground
        /// </summary>
        /// <param name="hWnd">window handle</param>
        private static void BringToFront(IntPtr hWnd)
        {
            // if iconic, we need to restore the window
            if (IsIconic(hWnd))
            {
                ShowWindowAsync(hWnd, SW_RESTORE);
            }
            // bring it to the foreground
            SetForegroundWindow(hWnd);
            // exit our process
            return;
        }

        public enum ProcessIndex
        {
            FirstAvailable,
            MostRecent
        }
    }

    public class ProcessNotRunningException : ApplicationException
    {
        private string _ProcessName;
        public ProcessNotRunningException(string processName)
        {
            _ProcessName = processName;
        }

        public override string Message
        {
            get
            {
                return String.Format("The process [{0}] is not running. You cannot bring the window to the front.", _ProcessName);
            }
        }

    }

    public class NullProcessException : ApplicationException
    {
        public NullProcessException()
        {

        }

        public override string Message
        {
            get
            {
                return "The process is null, it does not have a window.";
            }
        }
    }
}
