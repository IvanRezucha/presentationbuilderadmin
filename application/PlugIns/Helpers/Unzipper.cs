using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

using System.IO;
using System.Threading;
using System.Windows.Forms;
using System.Collections;

using ICSharpCode.SharpZipLib.Checksums;
using ICSharpCode.SharpZipLib.Zip;
using ICSharpCode.SharpZipLib.GZip;

namespace PlugIns.Helpers
{
    public partial class Unzipper : BackgroundWorker
    {
        #region Class Variables

        string _zipFile;
        string _writeToPath;
        bool _isZipDeleted;

        #endregion
        //class variables

        public Unzipper()
        {
            InitializeComponent();
            base.WorkerReportsProgress = true;
            base.WorkerSupportsCancellation = true;
        }

        /// <summary>
        /// Extracts a .zip file asynchronously.  Inherits from the BackgroundWorker class.
        /// You need to implement the OnProgressReported and OnRunWorkerCompleted events
        /// </summary>
        /// <param name="zipFile">The .zip file to extract</param>
        /// <param name="writeToPath">Where all the files will be extracted to</param>
        /// <param name="isZipDeleted">Whether or not the .zip file will be deleted after the extraction is completed</param>
        public void UnzipAsync(string zipFile, string writeToPath, bool isZipDeleted)
        {
            //initialize class variables
            _zipFile = zipFile;
            _writeToPath = writeToPath;
            _isZipDeleted = isZipDeleted;

            //start unzipping
            base.RunWorkerAsync();
        }       

        protected override void OnDoWork(DoWorkEventArgs e)
        {
            base.OnDoWork(e);

            ZipInputStream s = new ZipInputStream(File.OpenRead(_zipFile));

            ZipEntry theEntry;
            //int to keep track of progress
            int filesUnzipped = 0;
            while ((theEntry = s.GetNextEntry()) != null)
            {
                string fileName = theEntry.Name;

                
                if (fileName != String.Empty)
                {
                    //if the file already exists, make sure it is not read-only
                    if (File.Exists(_writeToPath + @"\" + theEntry.Name))
                    {
                        FileInfo fi = new FileInfo(_writeToPath + @"\" + theEntry.Name);
                        fi.Attributes = FileAttributes.Normal;
                    }
                    FileStream streamWriter = File.Create(_writeToPath + @"\" + theEntry.Name);

                    int size = 2048;
                    byte[] data = new byte[2048];
                    while (true)
                    {
                        size = s.Read(data, 0, data.Length);
                        if (size > 0)
                        {
                            streamWriter.Write(data, 0, size);
                        }
                        else
                        {
                            break;
                        }
                    }

                    streamWriter.Close();
                }

                //we want to report progress, but the zip object doesn't know how many files it has in it
                //so...we are reporting the number of files processed, not the progress precentage
                //when we get to 99, start over so we don't mess up our progress bar
                
                if (filesUnzipped < 99)
                {
                    filesUnzipped++;
                }
                else
                {
                    filesUnzipped = 1;
                }
                base.ReportProgress(filesUnzipped, "Extracting files...");
            }
            s.Close();

            base.ReportProgress(99, "Extracting files...");

            //delete the zip if isZipDeleted
            if (_isZipDeleted)
            {
                File.Delete(_zipFile);
            }

        }

    }
}
