﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Text;
using System.Data;
using System.Configuration;
using System.Threading;
using System.IO;

namespace PlugIns.Helpers
{
    /// <summary>
    /// A delegate used by invoking code to allow an IntervalInvokingWorker to report an error to
    /// a caller and allow the caller to decide what to do with the error.
    /// </summary>
    /// <param name="sender">The IntervalInvokingWorker that raised the error.</param>
    /// <param name="e">The Exception raised by the IntervalInvokingWorker.</param>
    public delegate void IntervalInvokingWorkerErrorHandler(object sender, Exception e);
    /// <summary>
    /// A background worker implementation that invokes its delegate on a timer.
    /// </summary>
    public partial class IntervalInvokingWorker : BackgroundWorker
    {
        private int _interval = 60000;
        private int _startIn = 60000;
        protected Timer invokingTimer;
        protected AutoResetEvent autoEvent;
        private bool _paused, _stopped = false;
        /// <summary>
        /// A handler used to communicate errors to caller.
        /// </summary>
        public event IntervalInvokingWorkerErrorHandler Error;

        #region Constructors
        public IntervalInvokingWorker() : base()
        {
            InitializeComponent();
            InitTimer();
        }

        public IntervalInvokingWorker(IContainer container) : base()
        {
            container.Add(this);
            InitializeComponent();
            InitTimer();
        }
        #endregion
        /// <summary>
        /// The time in milliseconds that this worker will invoke.
        /// </summary>
        public int Interval
        {
            get { return _interval; }
            set 
            {
                _interval = value;
                invokingTimer.Change(_startIn, _interval);
            }
        }
        /// <summary>
        /// The time in milliseconds that this worker will begin working.
        /// </summary>
        public int StartIn
        {
            get { return _startIn; }
            set 
            {
                _startIn = value; 
                invokingTimer.Change(_startIn, _interval);
            }
        }

        protected void InitTimer()
        {
            autoEvent = new AutoResetEvent(false);
            invokingTimer = new Timer(new TimerCallback(OnDoTimerEvent),
                autoEvent, _startIn, _interval);
        }

        protected virtual void OnDoTimerEvent(Object stateInfo)
        {
            if (_paused || _stopped) return;
            AutoResetEvent ae = (AutoResetEvent)stateInfo;
            try
            {
                this.RunWorkerAsync();
            }
            catch (Exception e) 
            {
                this.OnError(e);
            }
            finally
            {
                ae.Set();
            }
        }

        protected virtual void OnError(Exception e)
        {
            if (this.Error != null)
                this.Error(this, e);
            else
                throw e;
        }

        public virtual void PauseTimerAndRun()
        {
            if (!this.IsBusy)
            {
                try
                {
                    AutoResetEvent ae = new AutoResetEvent(false);
                    invokingTimer.Dispose(ae);
                    ae.WaitOne(-1);
                    ae.Set();
                    this.RunWorkerAsync();
                    this.InitTimer();
                }
                catch (Exception e)
                {
                    this.OnError(e);
                }
            }
        }

        public void Pause()
        {
            _paused = true;
        }

        public void Resume()
        {
            if (!_stopped)
            {
                _paused = false;
            }
        }

        public void Stop()
        {
            _stopped = _paused = true;
        }

        public void Start()
        {
            this.InitTimer();
            _paused = _stopped = false;
        }
    }
}
