﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Client.FileUtilities
{
    //FileManager is a singleton.  It is responsible for creating directories and 
    //exposing their paths and paths of files used in the application
    public sealed class LocationManager
    {
        private static LocationManager instance = new LocationManager();
        private IList<string> directories = new List<string>();

        private string appRefDirectory;
        private string localApplicationData;
        private string appDataDirectory;
        private string desktopDirectory;
        private string settingsDirectory;
        private string thumbnailDirectory;
        private string slideDirectory;
        private string xmlDirectory;
        private string tempDirectory;

        private LocationManager()
        {
            SetDirectoryPaths();
            VerifyDirectoriesExist();
        }

        public static LocationManager Instance
        {
            get
            {
                return instance;
            }
        }

        private void SetDirectoryPaths()
        {
            appRefDirectory = Path.Combine(System.Environment.GetFolderPath(Environment.SpecialFolder.Programs), Properties.Settings.Default.PublisherName);
            localApplicationData = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            appDataDirectory = Path.Combine(localApplicationData, Properties.Settings.Default.AppDataDirectory);
            desktopDirectory = System.Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            settingsDirectory = Path.Combine(appDataDirectory, "Settings");
            thumbnailDirectory = Path.Combine(localApplicationData, appDataDirectory + @"\Resources\Images\ThumbSlide");
            slideDirectory = Path.Combine(localApplicationData, appDataDirectory + @"\Resources\Ppt");
            xmlDirectory = Path.Combine(localApplicationData, appDataDirectory + @"\Resources\Xml");
            tempDirectory = Path.Combine(localApplicationData, appDataDirectory + @"\Temp");

            directories.Add(appRefDirectory);
            directories.Add(localApplicationData);
            directories.Add(appDataDirectory);
            directories.Add(desktopDirectory);
            directories.Add(settingsDirectory);
            directories.Add(thumbnailDirectory);
            directories.Add(slideDirectory);
            directories.Add(xmlDirectory);
            directories.Add(tempDirectory);
        }

        private void VerifyDirectoriesExist()
        {
            foreach (string directory in directories)
            {
                if (!Directory.Exists(directory))
                    Directory.CreateDirectory(directory);
            }
        }

        //Directories
        public string AppRefDirectory
        {
            get { return appRefDirectory; }
        }
        public string LocalApplicationData
        {
            get { return localApplicationData; }
        }
        public string AppDataDirectory
        {
            get { return appRefDirectory; }
        }
        public string DesktopDirectory
        {
            get { return desktopDirectory; }
        }
        public string SettingsDirectory
        {
            get { return settingsDirectory; }
        }
        public string ThumbnailDirectory
        {
            get { return thumbnailDirectory; }
        }
        public string SlideDirectory
        {
            get { return slideDirectory; }
        }
        public string XmlDirectory
        {
            get { return xmlDirectory; }
        }
        public string TempDirectory
        {
            get { return tempDirectory; }
        }
        //HtmlDirectory - leave that in the app for now.  It references the Application object and not sure that I want to reference that in here yet.

        //Files
        public string MyRegionLanguagesXmlFile
        {
            get
            {
                return Path.Combine(xmlDirectory, AppSettings.LocalFiles.Default.MyRegionLangauges);
            }
        }
        public string DesktopShortcutFile
        {
            get
            {
                return Path.Combine(desktopDirectory, Properties.Settings.Default.AppNameFull + ".APPREF-MS");
            }
        }
        public string StartMenuShortcutFile
        {
            get
            {
                return Path.Combine(appRefDirectory, Properties.Settings.Default.AppNameFull + ".APPREF-MS");
            }
        }
        public string UserSettingsXmlFile
        {
            get
            {
                return Path.Combine(settingsDirectory, "UserSettings.xml");
            }
        }
        public string ContentFilesXmlFile
        {
            get
            {
                return Path.Combine(xmlDirectory, AppSettings.LocalFiles.Default.ContentFiles);
            }
        }
        public string MainDataXmlFile
        {
            get
            {
                return Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, AppSettings.LocalFiles.Default.MainData);
            }
        }
        public string SupportFilesXmlFile
        {
            get
            {
                return Path.Combine(xmlDirectory, AppSettings.LocalFiles.Default.SupportFilesData);
            }
        }
        public string SlideDataPartFile(string prefix)
        {
            string slideFile = String.Format("{0}{1}", prefix, AppSettings.LocalFiles.Default.SlideDataFilePart);
            return Path.Combine(xmlDirectory, slideFile);
        }
        public string SlideLibarayDataFile
        {
            get
            {
                return Path.Combine(xmlDirectory, AppSettings.LocalFiles.Default.SlideLibraryData);
            }
        }
        public string MissingFile
        {
            get
            {
                return Path.Combine(AppSettings.LocalPaths.Default.HtmlDirectory, "MissingFile.htm");
            }
        }
    }
}
