﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections.Specialized;

namespace Client
{
    public partial class Profile : Form
    {
        private wsUserRegistration.UserRegistrationService userService;
        Client.wsUserRegistration.UserRegistrationDS.UserProfileRow profileRow;
        List<Country> countries;
        List<JobTitle> jobTitles;      
                     

        public Profile()
        {
            Init();
        }

        public Profile(string messageText)
        {
            Init();
            lblMessage.Text = messageText;
        }
        public void Init()
        {
            InitializeComponent();
            userService = new Client.wsUserRegistration.UserRegistrationService();
            userService.UpdateProfileInformationCompleted += new Client.wsUserRegistration.UpdateProfileInformationCompletedEventHandler(userService_UpdateProfileInformationCompleted);
        }

        private void Profile_Load(object sender, EventArgs e)
        {
            Client.wsUserRegistration.UserRegistrationDS.UserProfileDataTable userProfileTable = userService.GetUserByEmail(Properties.Settings.Default.UserRegistrationEmail);
            jobTitles = GetJobTitles();
            countries = GetCountries();
            this.profileControl1.PopulateCountryDropDown(countries);
            this.profileControl1.PopulateJobTitleDropDown(jobTitles);
            if (userProfileTable.Rows.Count > 0)
            {
                profileRow = userProfileTable.Rows[0] as Client.wsUserRegistration.UserRegistrationDS.UserProfileRow;
                this.profileControl1.Populate(profileRow);
            }

            //ValidateControls();
        }



        private List<Country> GetCountries()
        {
            List<Country> countryList = new List<Country>();
            wsUserRegistration.UserRegistrationDS.CountriesDataTable countriesTable = userService.GetCountries();
            foreach (wsUserRegistration.UserRegistrationDS.CountriesRow row in countriesTable)
                countryList.Add(new Country(row.Name, row.Code));
            return countryList;
        }
        private List<JobTitle> GetJobTitles()
        {
            List<JobTitle> jobTitles = new List<JobTitle>();
            wsUserRegistration.UserRegistrationDS.JobTitlesDataTable jobTitlesTable = userService.GetJobTitles();
            foreach (wsUserRegistration.UserRegistrationDS.JobTitlesRow row in jobTitlesTable)
                jobTitles.Add(new JobTitle(row.Name, row.Code));
            return jobTitles;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (!this.profileControl1.IsProfileValid())
                return;

            toolStripStatusLabel1.Text = "Updating...";
            pictureBox1.Visible = true;

            string email = Properties.Settings.Default.UserRegistrationEmail;

            // save region information to user's properties to show correct regional start page
            Properties.Settings.Default.ClientStartPageRegionPreference = this.profileControl1.RegionProperty;
            Properties.Settings.Default.ClientStartPagePreferenceSet = true;
            Properties.Settings.Default.Save();

            userService.UpdateProfileInformationAsync(email
                , this.profileControl1.FirstName
                , this.profileControl1.LastName
                , this.profileControl1.PhoneNumber
                , this.profileControl1.Country
                , this.profileControl1.RegionProperty
                , this.profileControl1.CompanyNameProperty
                , this.profileControl1.CompanySize
                , this.profileControl1.JobTitle
                , this.profileControl1.Referrer);
        }

        private void userService_UpdateProfileInformationCompleted(object sender, Client.wsUserRegistration.UpdateProfileInformationCompletedEventArgs e)
        {   
            pictureBox1.Visible = false;
            if (e.Error!=null && e.Error.Message.Equals("Unable to connect to the remote server"))
            {
                toolStripStatusLabel1.Text = "Update Failed.  Check internet connectivity.";
            }
            else if (e.Error != null)
            {
                toolStripStatusLabel1.Text = "Update Failed.  " + e.Error.Message + ".";
            }
            else
            {
                toolStripStatusLabel1.Text = "Update Complete";
                this.Close();
            }
        }

        private void Profile_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!this.profileControl1.IsProfileValid())
            {
                MessageBox.Show("Please complete all profile information.", "Your Profile");
                e.Cancel = true;
            }
        }
    }

    public class Country : LookUpType
    {
        public Country(string Name, string Code)
            : base(Name, Code)
        {
        }
    }
    public class JobTitle : LookUpType
    {
        public JobTitle(string Name, string Code)
            : base(Name, Code)
        {
        }
    }
    public class LookUpType
    {
        private string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        private string code;

        public string Code
        {
            get { return code; }
            set { code = value; }
        }

        public LookUpType(string Name, string Code)
        {
            this.name = Name;
            this.code = Code;
        }
    }
}
