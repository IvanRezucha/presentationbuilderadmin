using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace Client.Components
{
    class ListViewIndexComparer : System.Collections.IComparer
    {
        public int Compare(object x, object y)
        {
            return ((ListViewItem)x).Index - ((ListViewItem)y).Index;
        }
    }
}
