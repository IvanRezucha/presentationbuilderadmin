﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Client.Components
{
	public enum UpdateAvailable
	{
		Yes,
		No,
		OfflineOrError
	}
}
