using System;
using System.Collections.Generic;
using System.Text;

namespace Client.Components
{
    public static class CommonTasksHelper
    {
        public static string GetIps()
        {
            string hostName = System.Net.Dns.GetHostName();
            System.Net.IPHostEntry ipHostEntry = System.Net.Dns.GetHostEntry(hostName);
            Stack<System.Net.IPAddress> ips = new Stack<System.Net.IPAddress>(ipHostEntry.AddressList);
            StringBuilder ipList = new StringBuilder();

            for (int i = 0; i < ips.Count; i++)
            {
                ipList.Append(ips.Pop().ToString());
                if (i+1 < ips.Count)
                {
                    ipList.Append("|");
                }
            }
            return ipList.ToString();
        }
    }
}
