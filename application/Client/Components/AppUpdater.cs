﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Deployment.Application;

namespace Client.Components
{
	class AppUpdater
	{
		public static bool CheckDomainConnection()
		{
			bool isDomainAvailable = false;
			try
			{
				wsIpgSlideUpdater.SlideUpdater slideUpdater = new Client.wsIpgSlideUpdater.SlideUpdater();
				isDomainAvailable = slideUpdater.IsServiceAvailable();
			}
			catch (Exception ex)
			{
				//couldn't connect to the domain, so we can't get an update
				isDomainAvailable = false;
			}
			return isDomainAvailable;
		}

		public static UpdateAvailable CheckForAppUpdate()
		{
			if (!ApplicationDeployment.IsNetworkDeployed)
				return UpdateAvailable.No;

			if (CheckDomainConnection())
			{
				if (ApplicationDeployment.CurrentDeployment.CheckForUpdate())
				{
					return UpdateAvailable.Yes;
				}
				else
				{
					return UpdateAvailable.No;
				}
			}
			else
			{
				return UpdateAvailable.OfflineOrError;
			}
		}

		
	}
}
