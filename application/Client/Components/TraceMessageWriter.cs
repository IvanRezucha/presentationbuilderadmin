using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

namespace Client.Components
{
	public static class TraceMessageWriter
	{
		public static void WriteTraceMessage(string message)
		{
			Trace.WriteLine(String.Format("{0} {1}: {2}", DateTime.Now.ToShortDateString(), DateTime.Now.ToLongTimeString(), message));
			Trace.Flush();
		}

		public static void WriteBlankLine()
		{
			Trace.WriteLine("");
			Trace.Flush();
		}
	}
}
