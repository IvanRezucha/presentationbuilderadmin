﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Drawing;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;

namespace Client.Components
{
    /// <summary>
    /// Overall goal of this class is to make it simple to keep our own copy of the data stored in
    /// [user]\Local Settings\Apps\2.0\Data\[undecipherable mess]\data\user.config in a place we control, e.g.
    /// [user]\Local Settings\Application Data\[project name]\UserSettingPersister.xml.  That way we
    /// can have greater control over the uninstall/reinstall process since we won't lose user settings any more.
    /// </summary>
    public class UserSettingsManager
    {
        /// <summary>
        /// Gets the name of the persisted settings path and file.
        /// </summary>
        /// <value>The name of the persisted settings path and file.</value>
        private string PersistedSettingsPathAndFileName
        {
            get
            {
                return Path.Combine(Path.Combine(LocalAppDataPath, "Settings"), "UserSettings.xml");
            }
        }

        private string LocalAppDataPath
        {
            get
            {
                return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), Properties.Settings.Default.AppDataDirectory.ToString());
            }
        }

        /// <summary>
        /// Verifies that the persisted location exists.  If it does not, creates it and writes default settings to it
        /// </summary>
        public void VerifyLocalUserSettings()
        {
            if (!File.Exists(PersistedSettingsPathAndFileName))
            {
                if (!Directory.Exists(Path.GetDirectoryName(PersistedSettingsPathAndFileName)))
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(PersistedSettingsPathAndFileName));
                }
                WriteLocalDataSettingsFileFromDefault();
            }
        }


        private bool IsAppliationSetting(string name)
        {
            bool isAppSetting = false;
            System.Configuration.SettingsProperty property = Properties.Settings.Default.Properties[name];
            if (property != null)
            {
                foreach (object key in property.Attributes.Keys)
                {
                    object val = property.Attributes[key];
                    if (val is System.Configuration.ApplicationScopedSettingAttribute)
                    {
                        isAppSetting = true;
                        break;
                    }
                }
            }
            return isAppSetting;
        }

        private void SetProperty(string name, string value, string type)
        {
            try
            {
                switch (type)
                {
                    case "System.String":
                        //don't change URLs
                        /*
                        if (value.StartsWith("http://"))
                        {
                            //Trace.WriteLine(String.Format("Skipping {0} because it is a URL: {1}", name, value));
                            continue;
                        }
                        //don't change product name
                        if (name.Equals("ProductName", StringComparison.CurrentCultureIgnoreCase))
                        {
                            Trace.WriteLine(String.Format("Skipping {0} because it is not a user setting: {1}", name, value));
                            continue;
                        }
                         */
                        Properties.Settings.Default[name] = value;
                        break;

                    case "System.Int32":
                        Properties.Settings.Default[name] = Convert.ToInt32(value);
                        break;

                    case "System.Int64":
                        Properties.Settings.Default[name] = Convert.ToInt64(value);
                        break;

                    case "System.DateTime":
                        Properties.Settings.Default[name] = Convert.ToDateTime(value);
                        break;

                    case "System.Guid":
                        Properties.Settings.Default[name] = new Guid(value);
                        break;

                    case "System.Boolean":
                        Properties.Settings.Default[name] = Convert.ToBoolean(value);
                        break;

                    case "System.Drawing.Size":
                        //assumes {Width=800, Height=600} is the format.
                        Size size = (Size)TypeDescriptor.GetConverter(typeof(System.Drawing.Size)).ConvertFromString(value.Replace("{Width=", "").Replace("Height=", "").Replace("}", ""));
                        Properties.Settings.Default[name] = size;
                        break;

                    case "System.Drawing.Point":
                        //assumes {X=20,Y=20} is the format
                        Point point = (Point)TypeDescriptor.GetConverter(typeof(System.Drawing.Point)).ConvertFromString(value.Replace("{X=", "").Replace("Y=", "").Replace("}", ""));
                        Properties.Settings.Default[name] = point;
                        break;

                    default:
                        throw new Exception("Type not handled.");
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine(String.Format("Error on property Name: {0}, Type: {1}, Value: {2}, Error: {3}", name, type, value, ex.ToString()));
            }
        }

        /// <summary>
        /// Reads our local data settings values into the default store used by ClickOnce.
        /// </summary>
        public void ReadLocalDataSettingsIntoDefault()
        {
            string name, type, value;
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load(PersistedSettingsPathAndFileName);

            XmlNodeList settingsNodeList = xmlDocument.SelectNodes("userSettings/userSetting");
            foreach (XmlNode settingsNode in settingsNodeList)
            {
                name = settingsNode.Attributes["name"].Value;
                type = settingsNode.Attributes["type"].Value;
                value = settingsNode.ChildNodes[0].InnerText;

                if (!value.StartsWith("http://") 
                    && !name.Equals("ProductName", StringComparison.CurrentCultureIgnoreCase)
                    && !IsAppliationSetting(name))
                {
                    SetProperty(name, value, type);
                }
            }

            Properties.Settings.Default.Save();
        }

        /// <summary>
        /// Writes the local data settings file from the default ClickOnce store into our home-grown data file.
        /// </summary>
        public void WriteLocalDataSettingsFileFromDefault()
        {
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.Encoding = Encoding.UTF8;

            using (XmlWriter writer = XmlWriter.Create(PersistedSettingsPathAndFileName, settings))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("userSettings");


                foreach (System.Configuration.SettingsProperty property in Properties.Settings.Default.Properties)
                {
                    writer.WriteStartElement("userSetting");

                    writer.WriteAttributeString("name", property.Name);
                    writer.WriteAttributeString("type", property.PropertyType.UnderlyingSystemType.FullName);
                    writer.WriteStartElement("value");
                    writer.WriteValue(String.Format("{0}", Properties.Settings.Default[property.Name]));
                    writer.WriteEndElement();

                    writer.WriteEndElement();
                }

                writer.WriteEndElement();

                writer.WriteEndDocument();
                writer.Flush();
            }
        }

    }
}
