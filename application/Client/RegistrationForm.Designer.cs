﻿namespace Client
{
	partial class RegistrationForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.txtEmailDetails = new System.Windows.Forms.TextBox();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btnDontRegister1 = new System.Windows.Forms.Button();
            this.btnNext1 = new System.Windows.Forms.Button();
            this.txtRegistrationEmail = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btnBack = new System.Windows.Forms.Button();
            this.btnDontRegister2 = new System.Windows.Forms.Button();
            this.btnFinish = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.profileControl1 = new Client.ProfileControl();
            this.tabWizard.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // gradientCaptionWizardTitle
            // 
            this.gradientCaptionWizardTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gradientCaptionWizardTitle.Location = new System.Drawing.Point(0, 0);
            this.gradientCaptionWizardTitle.Name = "gradientCaptionWizardTitle";
            this.gradientCaptionWizardTitle.Size = new System.Drawing.Size(401, 65);
            this.gradientCaptionWizardTitle.TabIndex = 4;
            this.gradientCaptionWizardTitle.Text = "Application Registration Wizard";
            // 
            // tabWizard
            // 
            this.tabWizard.Controls.Add(this.tabPage1);
            this.tabWizard.Controls.Add(this.tabPage2);
            this.tabWizard.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabWizard.Location = new System.Drawing.Point(0, 0);
            this.tabWizard.Size = new System.Drawing.Size(401, 388);
            // 
            // txtEmailDetails
            // 
            this.txtEmailDetails.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtEmailDetails.BackColor = System.Drawing.SystemColors.Control;
            this.txtEmailDetails.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtEmailDetails.Location = new System.Drawing.Point(7, 49);
            this.txtEmailDetails.Multiline = true;
            this.txtEmailDetails.Name = "txtEmailDetails";
            this.txtEmailDetails.ReadOnly = true;
            this.txtEmailDetails.Size = new System.Drawing.Size(379, 71);
            this.txtEmailDetails.TabIndex = 10;
            this.txtEmailDetails.Text = "It appears this copy of HP Printing Sales Guide is unregistered. Please enter you" +
                "r email address to begin the registration process. ";
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.btnDontRegister1);
            this.tabPage1.Controls.Add(this.btnNext1);
            this.tabPage1.Controls.Add(this.txtRegistrationEmail);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.txtEmailDetails);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(393, 362);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "tabPageEmail";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // btnDontRegister1
            // 
            this.btnDontRegister1.Location = new System.Drawing.Point(6, 331);
            this.btnDontRegister1.Name = "btnDontRegister1";
            this.btnDontRegister1.Size = new System.Drawing.Size(82, 23);
            this.btnDontRegister1.TabIndex = 1;
            this.btnDontRegister1.Text = "Don\'t Register";
            this.btnDontRegister1.UseVisualStyleBackColor = true;
            this.btnDontRegister1.Click += new System.EventHandler(this.btnDontRegister_Click);
            // 
            // btnNext1
            // 
            this.btnNext1.Location = new System.Drawing.Point(310, 331);
            this.btnNext1.Name = "btnNext1";
            this.btnNext1.Size = new System.Drawing.Size(75, 23);
            this.btnNext1.TabIndex = 2;
            this.btnNext1.Text = "Next >";
            this.btnNext1.UseVisualStyleBackColor = true;
            this.btnNext1.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // txtRegistrationEmail
            // 
            this.txtRegistrationEmail.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtRegistrationEmail.Location = new System.Drawing.Point(104, 126);
            this.txtRegistrationEmail.MaxLength = 100;
            this.txtRegistrationEmail.Name = "txtRegistrationEmail";
            this.txtRegistrationEmail.Size = new System.Drawing.Size(230, 20);
            this.txtRegistrationEmail.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(59, 129);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "E-Mail:";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.profileControl1);
            this.tabPage2.Controls.Add(this.btnBack);
            this.tabPage2.Controls.Add(this.btnDontRegister2);
            this.tabPage2.Controls.Add(this.btnFinish);
            this.tabPage2.Controls.Add(this.label10);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(393, 362);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // btnBack
            // 
            this.btnBack.Location = new System.Drawing.Point(6, 331);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(75, 23);
            this.btnBack.TabIndex = 47;
            this.btnBack.Text = "< Back";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnDontRegister2
            // 
            this.btnDontRegister2.Location = new System.Drawing.Point(155, 331);
            this.btnDontRegister2.Name = "btnDontRegister2";
            this.btnDontRegister2.Size = new System.Drawing.Size(82, 23);
            this.btnDontRegister2.TabIndex = 8;
            this.btnDontRegister2.Text = "Don\'t Register";
            this.btnDontRegister2.UseVisualStyleBackColor = true;
            this.btnDontRegister2.Click += new System.EventHandler(this.btnDontRegister_Click);
            // 
            // btnFinish
            // 
            this.btnFinish.Location = new System.Drawing.Point(310, 331);
            this.btnFinish.Name = "btnFinish";
            this.btnFinish.Size = new System.Drawing.Size(75, 23);
            this.btnFinish.TabIndex = 9;
            this.btnFinish.Text = "Finish";
            this.btnFinish.UseVisualStyleBackColor = true;
            this.btnFinish.Click += new System.EventHandler(this.btnFinish_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(8, 46);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(316, 13);
            this.label10.TabIndex = 30;
            this.label10.Text = "Please fill in the following information to complete your registration.";
            // 
            // errorProvider
            // 
            this.errorProvider.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.errorProvider.ContainerControl = this;
            // 
            // profileControl1
            // 
            this.profileControl1.CompanyNameProperty = "";
            this.profileControl1.CompanySize = 0;
            this.profileControl1.FirstName = "";
            this.profileControl1.LastName = "";
            this.profileControl1.Location = new System.Drawing.Point(6, 62);
            this.profileControl1.Name = "profileControl1";
            this.profileControl1.PhoneNumber = "";
            this.profileControl1.Referrer = "";
            this.profileControl1.Size = new System.Drawing.Size(375, 256);
            this.profileControl1.TabIndex = 48;
            // 
            // RegistrationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(401, 388);
            this.Name = "RegistrationForm";
            this.Text = "Registration";
            this.Load += new System.EventHandler(this.RegistrationForm_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.RegistrationForm_FormClosing);
            this.tabWizard.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TextBox txtEmailDetails;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.TextBox txtRegistrationEmail;
		private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Button btnDontRegister1;
		private System.Windows.Forms.Button btnNext1;
		private System.Windows.Forms.Button btnDontRegister2;
		private System.Windows.Forms.Button btnFinish;
        private System.Windows.Forms.ErrorProvider errorProvider;
		private System.Windows.Forms.Button btnBack;
        private ProfileControl profileControl1;
	}
}