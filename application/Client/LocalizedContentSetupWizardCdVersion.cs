using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Client
{
    public partial class LocalizedContentSetupWizardCdVersion : MasterWizardForm
    {
        //class variables
        private string _CdDrive;
		private DataSets.MyRegionLanguages _MyRegLangs = new Client.DataSets.MyRegionLanguages();
		private Client.wsIpgSlideUpdater.SlideLibrary _slidesToDownload;

        public LocalizedContentSetupWizardCdVersion()
        {
            
            InitializeComponent();
        }

        private void LocalizedContentSetupWizard_Load(object sender, EventArgs e)
        {
            //this will get the cd drive and copy the xml while the user is reading the welcome screen
			//backgroundWorkerCopyXml.RunWorkerAsync();
			
            string welcomeMessage;
            welcomeMessage = String.Format("Welcome to the {0} Application Setup Wizard", Properties.Settings.Default.AppNameFull);
            richTextBoxWelcomeTitle.Text = welcomeMessage;
            
        }

        private void btnWelcomeNext_Click(object sender, EventArgs e)
        {
			this.Cursor = Cursors.WaitCursor;

			DetermineCdDrive();
			CopyXml();

			this.Cursor = Cursors.Default;

            regionLanguageGroupGrid.FillRegionLanguageGroupGrid();
            tabWizard.SelectTab(tabPageRegionLanguages);
        }

        private void btnRegLangNext_Click(object sender, EventArgs e)
        {
            if (!regionLanguageGroupGrid.SaveMyRegionLanguages())
            {
                return;
            }
            tabWizard.SelectTab(tabPageContentUpdateProgress);

            backgroundWorkerCopyPptAndThumbs.RunWorkerAsync();
        }

        private void btnFinished_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void DetermineCdDrive()
        {
            backgroundWorkerCopyXml.ReportProgress(0, "Initializing content setup...");
            List<string> drives = new List<string>(Environment.GetLogicalDrives());
            foreach (string driveLetter in drives)
            {
                DriveInfo drive = new DriveInfo(driveLetter);
                if (drive.DriveType == DriveType.CDRom || drive.DriveType == DriveType.Removable)
                {
                    if (Directory.Exists(Path.Combine(driveLetter, "Resources")))
                    {
                        //we found the right drive
                        //capture it and get out here
                        _CdDrive = driveLetter;
                        return;
                    }
                }
            }
        }

        private void CopyPptFiles()
        {

			_MyRegLangs._MyRegionLanguages.Clear();
			_MyRegLangs.ReadXml(Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, AppSettings.LocalFiles.Default.MyRegionLangauges));
			_slidesToDownload = new Client.wsIpgSlideUpdater.SlideLibrary();

			_slidesToDownload.ReadXml(Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, AppSettings.LocalFiles.Default.MainData));
			Client.wsIpgSlideUpdater.SlideLibrary regionSpecificSlides;

			foreach (DataRow row in _MyRegLangs._MyRegionLanguages.Rows)
			{
				regionSpecificSlides = new Client.wsIpgSlideUpdater.SlideLibrary();
				string regionLanguageID = row["RegionLanguageID"].ToString();
				regionSpecificSlides.EnforceConstraints = false;
				regionSpecificSlides.ReadXml(Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, regionLanguageID + AppSettings.LocalFiles.Default.SlideDataFilePart));
				_slidesToDownload.Merge(regionSpecificSlides);
			}

			backgroundWorkerCopyPptAndThumbs.ReportProgress(0, "Copying PowerPoint files...");
            //make sure they have this directory
            if (!Directory.Exists(AppSettings.LocalPaths.Default.SlideDirectory))
            {
                Directory.CreateDirectory(AppSettings.LocalPaths.Default.SlideDirectory);
            }

            //the path on the CD according to the drive letter the user selected
            string cdDir = Path.Combine(Path.Combine(_CdDrive, "Resources"), "Ppt");

            //copy the files
            int filesCopied = 0;
            double percentCopied = 0;
			string sourceFile = "";

			foreach (DataRow row in _slidesToDownload.Slide.Rows)
            {
				sourceFile = Path.Combine(cdDir,row["FileName"].ToString());

                try
                {
                    //copy the file
                    File.Copy(sourceFile, Path.Combine(AppSettings.LocalPaths.Default.SlideDirectory, Path.GetFileName(sourceFile)), true);
                    FileInfo fi = new FileInfo(Path.Combine(AppSettings.LocalPaths.Default.SlideDirectory, Path.GetFileName(sourceFile)));
                    fi.IsReadOnly = false;
                }
                catch (Exception ex)
                {
                    //do nothing
                }

                //report progress
                filesCopied++;
                percentCopied = (Convert.ToDouble(filesCopied) / Convert.ToDouble(_slidesToDownload.Slide.Rows.Count)) * 100.00;
				backgroundWorkerCopyPptAndThumbs.ReportProgress(Convert.ToInt32(percentCopied), String.Format("Copying PowerPoint file {0} of {1}.", filesCopied, _slidesToDownload.Slide.Rows.Count));
            }
        }

        private void CopyThumbs()
        {
            backgroundWorkerCopyPptAndThumbs.ReportProgress(0, "Copying images...");
            //make sure they have this directory
            if (!Directory.Exists(AppSettings.LocalPaths.Default.ThumbnailDirectory))
            {
                Directory.CreateDirectory(AppSettings.LocalPaths.Default.ThumbnailDirectory);
            }

            //the path on the CD according to the drive letter the user selected
            string cdDir = Path.Combine(Path.Combine(Path.Combine(_CdDrive, "Resources"), "Images"), "ThumbSlide");

            int filesCopied = 0;
            double percentCopied = 0;
			string sourceFile = "";

            foreach (DataRow row in _slidesToDownload.Slide.Rows)
            {
				sourceFile = Path.Combine(cdDir,Path.ChangeExtension(row["FileName"].ToString(), ".jpg"));
                try
                {
                    //copy the file
                    File.Copy(sourceFile, Path.Combine(AppSettings.LocalPaths.Default.ThumbnailDirectory, Path.GetFileName(sourceFile)), true);
                    FileInfo fi = new FileInfo(Path.Combine(AppSettings.LocalPaths.Default.ThumbnailDirectory, Path.GetFileName(sourceFile)));
                    fi.IsReadOnly = false;
                }
                catch (Exception ex)
                {
                    //do nothing
                }

                //report progress
                filesCopied++;
				percentCopied = (Convert.ToDouble(filesCopied) / Convert.ToDouble(_slidesToDownload.Slide.Rows.Count)) * 100.00;
				backgroundWorkerCopyPptAndThumbs.ReportProgress(Convert.ToInt32(percentCopied), String.Format("Copying image file {0} of {1}.", filesCopied, _slidesToDownload.Slide.Rows.Count));
            }

        }

        private void CopyXml()
        {
            backgroundWorkerCopyXml.ReportProgress(0, "Copying application data...");
            //make sure they have this directory
            if (!Directory.Exists(AppSettings.LocalPaths.Default.XmlDirectory))
            {
                Directory.CreateDirectory(AppSettings.LocalPaths.Default.XmlDirectory);
            }

            //the path on the CD according to the drive letter the user selected
            string cdDir = Path.Combine(Path.Combine(_CdDrive, "Resources"), "Xml");
            // This is a less-than-desirable technique but it will prevent the app from crashing when
            // the user clicks through the wizard too fast
			try
			{
				File.Copy(Path.Combine(cdDir, "LocallyAvailableLanguages.xml"), Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, "LocallyAvailableLanguages.xml"), true);
			}
			catch
			{
				//do nothing
			}

            //copy the files
            List<string> sourceFiles = new List<string>(Directory.GetFiles(cdDir));
			//int filesCopied = 0;
			//double percentCopied = 0;
            foreach (string sourceFile in sourceFiles)
            {
                try
                {
					//see the comment above
					if (!Path.GetFileName(sourceFile).ToString().Equals("LocallyAvailableLanguages.xml", StringComparison.CurrentCultureIgnoreCase))
					{
						//copy the file
						File.Copy(sourceFile, Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, Path.GetFileName(sourceFile)), true);
						FileInfo fi = new FileInfo(Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, Path.GetFileName(sourceFile)));
						fi.IsReadOnly = false;
					}
                }
                catch (Exception ex)
                {
                    //do nothing
                }

                //report progress
				//filesCopied++;
				//percentCopied = (Convert.ToDouble(filesCopied) / Convert.ToDouble(sourceFiles.Count)) * 100.00;
				//backgroundWorkerCopyXml.ReportProgress(Convert.ToInt32(percentCopied), String.Format("Copying application data file {0} of {1}.", filesCopied, sourceFiles.Count));
            }

        }

        private void DoSetupActivities()
        {
            AppSetup appSetup = new AppSetup();
            appSetup.RegistrySetup();
            appSetup.CreateDesktopShortcut();

            //update user settings to reflect that setup activities have occurred
            Properties.Settings.Default.LastLocalizedContentID = 50; //this is the DataUpdateID at the time of app cd deployment
            Properties.Settings.Default.IsFirstRun = false;
            Properties.Settings.Default.IsAppLocalizedContentReady = true;
            Properties.Settings.Default.Save();

            Upgrades.UpgradeSettings.Default.IsReadyFor_1_9 = true;
            Upgrades.UpgradeSettings.Default.Save();
        }

        private void backgroundWorkerCopyPptAndThumbs_DoWork(object sender, DoWorkEventArgs e)
        {
            CopyPptFiles();
            CopyThumbs();
            DoSetupActivities();
        }

        private void backgroundWorkerCopyPptAndThumbs_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            labelStatus.Visible = true;
            progressBarContentUpdate.Visible = true;

            labelStatus.Text = e.UserState.ToString();
            progressBarContentUpdate.Value = e.ProgressPercentage;
        }

        private void backgroundWorkerCopyPptAndThumbs_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void backgroundWorkerCopyXml_DoWork(object sender, DoWorkEventArgs e)
        {
			//DetermineCdDrive();
			//CopyXml();
        }

        private void backgroundWorkerCopyXml_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {

        }

        private void backgroundWorkerCopyXml_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

        }

    }
}