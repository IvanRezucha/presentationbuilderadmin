using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Client
{
    public partial class MyRegionLanguagesDialog : MasterForm
    {
        public MyRegionLanguagesDialog()
        {
            InitializeComponent();

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void UpdateRegionLang(bool update_index)
        {
            if (regionLanguageGroupGrid.SaveMyRegionLanguages())
            {
                if (regionLanguageGroupGrid.IsNewRegionLanguagesSelected)
                {
                    if(update_index)
                    ((IpgMain)this.Owner).Start_Update_Index_Timer();
                    ContentUpdateForm contentUpdateForm;
                    if(Client.Properties.Settings.Default.FirstContent)
                        contentUpdateForm = new ContentUpdateForm(ContentUpdater.UpdateType.LocalizedFull);
                    else
                        contentUpdateForm = new ContentUpdateForm(ContentUpdater.UpdateType.LocalizedRegular);
                    contentUpdateForm.Owner = this.Owner;
                    contentUpdateForm.Show(); //ShowDialog();
                }
                else
                {
                    ((IpgMain)this.Owner).PopulateLocalizedTreeviews();
                }
                Close();
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (((IpgMain)this.Owner)._index_in_progress)
            {
                DialogResult result = MessageBox.Show("This action will stop your index. If you want to continue indexing, please click cancel, otherwise your index will be stopped and you will need to manually begin the index process when you are done by clicking [Update -> Indexing Options -> Index All].  Would you like to continue?", "Cancel Indexing?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
                if (result == DialogResult.Yes)
                {//Cancel index and continue with change.
                    //Stop Index
                    //Clear index files. We have no idea where it stopped and it is difficult to continue since they are now adding new regions/languages
                    ((IpgMain)this.Owner).Stop_and_Clear_Index();
                    //Add regions
                    UpdateRegionLang(false);
                }
            }
            else
            {// no index in progress continue adding
                UpdateRegionLang(true);
            }
        }

        private void MyRegionLanguagesDialog_Load(object sender, EventArgs e)
        {
            regionLanguageGroupGrid.FillRegionLanguageGroupGrid();
            regionLanguageGroupGrid.LoadMyRegionLanguages();
        }

    }
}