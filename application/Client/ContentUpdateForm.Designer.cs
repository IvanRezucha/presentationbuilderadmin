namespace Client
{
    partial class ContentUpdateForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ContentUpdateForm));
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.pnlBody = new System.Windows.Forms.Panel();
            this.btnPauseUpdate = new System.Windows.Forms.Button();
            this.cboUpdateType = new System.Windows.Forms.ComboBox();
            this.lblUpdateStatus = new System.Windows.Forms.Label();
            this.pbStatus = new System.Windows.Forms.ProgressBar();
            this.lblMessage = new System.Windows.Forms.Label();
            this.lblUpdateType = new System.Windows.Forms.Label();
            this.pnlTop = new System.Windows.Forms.Panel();
            this.lblHeading = new System.Windows.Forms.Label();
            this.pnlBody.SuspendLayout();
            this.pnlTop.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(295, 286);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 7;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(214, 286);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 6;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // pnlBody
            // 
            this.pnlBody.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlBody.BackColor = System.Drawing.Color.White;
            this.pnlBody.Controls.Add(this.btnPauseUpdate);
            this.pnlBody.Controls.Add(this.cboUpdateType);
            this.pnlBody.Controls.Add(this.lblUpdateStatus);
            this.pnlBody.Controls.Add(this.pbStatus);
            this.pnlBody.Controls.Add(this.lblMessage);
            this.pnlBody.Controls.Add(this.lblUpdateType);
            this.pnlBody.Location = new System.Drawing.Point(0, 57);
            this.pnlBody.Name = "pnlBody";
            this.pnlBody.Size = new System.Drawing.Size(401, 223);
            this.pnlBody.TabIndex = 5;
            // 
            // btnPauseUpdate
            // 
            this.btnPauseUpdate.Enabled = false;
            this.btnPauseUpdate.Location = new System.Drawing.Point(257, 189);
            this.btnPauseUpdate.Name = "btnPauseUpdate";
            this.btnPauseUpdate.Size = new System.Drawing.Size(133, 23);
            this.btnPauseUpdate.TabIndex = 8;
            this.btnPauseUpdate.Text = "Continue Update Later";
            this.btnPauseUpdate.UseVisualStyleBackColor = true;
            this.btnPauseUpdate.Click += new System.EventHandler(this.btnPauseUpdate_Click);
            // 
            // cboUpdateType
            // 
            this.cboUpdateType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboUpdateType.FormattingEnabled = true;
            this.cboUpdateType.Location = new System.Drawing.Point(128, 11);
            this.cboUpdateType.Name = "cboUpdateType";
            this.cboUpdateType.Size = new System.Drawing.Size(261, 21);
            this.cboUpdateType.TabIndex = 6;
            this.cboUpdateType.SelectedValueChanged += new System.EventHandler(this.cboUpdateType_SelectedValueChanged);
            // 
            // lblUpdateStatus
            // 
            this.lblUpdateStatus.AutoSize = true;
            this.lblUpdateStatus.Location = new System.Drawing.Point(16, 194);
            this.lblUpdateStatus.Name = "lblUpdateStatus";
            this.lblUpdateStatus.Size = new System.Drawing.Size(75, 13);
            this.lblUpdateStatus.TabIndex = 5;
            this.lblUpdateStatus.Text = "Update Status";
            this.lblUpdateStatus.Visible = false;
            // 
            // pbStatus
            // 
            this.pbStatus.Location = new System.Drawing.Point(15, 164);
            this.pbStatus.Name = "pbStatus";
            this.pbStatus.Size = new System.Drawing.Size(374, 23);
            this.pbStatus.TabIndex = 4;
            this.pbStatus.Visible = false;
            // 
            // lblMessage
            // 
            this.lblMessage.BackColor = System.Drawing.Color.White;
            this.lblMessage.Location = new System.Drawing.Point(3, 45);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(395, 116);
            this.lblMessage.TabIndex = 3;
            // 
            // lblUpdateType
            // 
            this.lblUpdateType.AutoSize = true;
            this.lblUpdateType.Location = new System.Drawing.Point(13, 11);
            this.lblUpdateType.Name = "lblUpdateType";
            this.lblUpdateType.Size = new System.Drawing.Size(109, 13);
            this.lblUpdateType.TabIndex = 1;
            this.lblUpdateType.Text = "Content Update Type";
            // 
            // pnlTop
            // 
            this.pnlTop.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pnlTop.Controls.Add(this.lblHeading);
            this.pnlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTop.Location = new System.Drawing.Point(0, 0);
            this.pnlTop.Name = "pnlTop";
            this.pnlTop.Size = new System.Drawing.Size(401, 57);
            this.pnlTop.TabIndex = 4;
            // 
            // lblHeading
            // 
            this.lblHeading.AutoSize = true;
            this.lblHeading.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeading.Location = new System.Drawing.Point(12, 18);
            this.lblHeading.Name = "lblHeading";
            this.lblHeading.Size = new System.Drawing.Size(140, 24);
            this.lblHeading.TabIndex = 0;
            this.lblHeading.Text = "Content Update";
            // 
            // ContentUpdateForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(401, 316);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.pnlBody);
            this.Controls.Add(this.pnlTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "ContentUpdateForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = global::Client.Properties.Settings.Default.AppNameFull;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ContentUpdateForm_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ContentUpdateForm_FormClosed);
            this.Load += new System.EventHandler(this.ContentUpdateForm_Load);
            this.pnlBody.ResumeLayout(false);
            this.pnlBody.PerformLayout();
            this.pnlTop.ResumeLayout(false);
            this.pnlTop.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblHeading;
        private System.Windows.Forms.Panel pnlTop;
        private System.Windows.Forms.Panel pnlBody;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblUpdateType;
        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.Label lblUpdateStatus;
        private System.Windows.Forms.ProgressBar pbStatus;
        private System.Windows.Forms.ComboBox cboUpdateType;
		private System.Windows.Forms.Button btnPauseUpdate;
    }
}