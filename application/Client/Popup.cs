using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Client
{
    public partial class Popup : Form
    {
        string incoming;
        Point startPosition;

        public Popup(string incomingText, Point cursorPosition)
        {
            InitializeComponent();
            incoming = incomingText;
            startPosition = cursorPosition;
        }

        private void Popup_Load(object sender, EventArgs e)
        {
            try
            {
                string fileName = incoming.Replace(".dmpopup", ".rtf");
                if(((IpgMain)this.Owner).BrowseFileCheckExists(fileName))
                {
                    //fileName = fileName.Remove(0, 8);
                    rtbBody.LoadFile(fileName, RichTextBoxStreamType.RichText);
                    this.Location = startPosition;
                }
            }
            catch (Exception ex)
            { 
                //TODO
            }   
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Popup_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}