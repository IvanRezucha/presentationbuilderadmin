using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using Client.BLL;
using System.Deployment.Application;
using Client.wsUserRegistration;
using Client.wsUsageLogging;
using System.Collections;
using System.Net.NetworkInformation; //**SS Network Check


namespace Client
{
    public enum EventType : int
    {
        Viewed = 1,
        Played = 2,
        Saved = 3,
        Exported = 4,
        Printed = 5
    }
    
    public partial class IpgMain : MasterForm
    {
        #region Class Variables

        //class variables
        public event NoAppUpdateEventHandler NoAppUpdate;
        public wsIpgSlideUpdater.SlideLibrary dsSlideLibrary;
        DataSets.MyPresentations dsMyPresentations = new Client.DataSets.MyPresentations();
        System.Collections.ArrayList currentChildSlides = new System.Collections.ArrayList();
        int currentMySlides = -1;//MyPresentationID or -1 for new presentation
        bool mySlidesHasChanges = false;
        System.Collections.ArrayList draggedItems = new System.Collections.ArrayList();
        string draggedItemsControl;
        TreeNode gRightClickedNode = new TreeNode();
        int currentBrowsedSlide;
        //bool reselectThumbOnBrowserLoad;
        string currentBrowsedSlideSet;
        ApplicationDeployment appUpdate;
        List<int> _selectedFilterIDs;
        BrowsedDocType lastBrowseType;
        private ContentUpdater _ContentUpdater;
        private wsIssueLogger.Logger _IssueLoggerService;
        // Objects support user event logging
        // object for synchronizing access to the log file.
        private Object _logSync = new Object();
        private UserEventsDS eventsCache;
        private long eventId = 0;
        private int userId = 0;
        private Guid sessionId;
        private DateTime sessionStart;
        private bool _logEvent = true;
        private bool _flushToService = true;
        private bool _NetConnected = true;
        public bool isFirstRun = false;
        public bool _fullIndex = false;
        public List<string> _toBeIndexed = new List<string>();
        public List<string> _removeIndex = new List<string>();
        public bool _UpdateIndex = false;
        public bool _index_in_progress = false;
        BLL.LuceneIndex ind = new BLL.LuceneIndex();
        

        private wsIpgSlideUpdater.SlideLibrary _MainSlideLibrary;
        public wsIpgSlideUpdater.SlideLibrary MainSlideLibrary
        {
            get { return _MainSlideLibrary; }
            set { _MainSlideLibrary = value; }
        }

        private List<wsIpgSlideUpdater.SlideLibrary> _LocalizedSlideLibraries;
        public List<wsIpgSlideUpdater.SlideLibrary> LocalizedSlideLibraries
        {
            get { return _LocalizedSlideLibraries; }
            set { _LocalizedSlideLibraries = value; }
        }

        private ViewMode _CurrentViewMode;
        public ViewMode CurrentViewMode
        {
            get { return _CurrentViewMode; }
            set { _CurrentViewMode = value; }
        }

        #endregion

        #region Properties

        ///// <summary>
        ///// Gets or sets the current child slides. This keeps track of the current thumbnail list
        ///// </summary>
        ///// <value>The current child slides.</value>
        //public List<wsIpgSlideUpdater.SlideLibrary.SlideRow> CurrentChildSlides
        //{
        //    get { return currentChildSlides; }
        //    set { currentChildSlides = value; }
        //}

        /// <summary>
        /// Gets or sets the current child slides. This keeps track of the current thumbnail list
        /// </summary>
        /// <value>The current child slides.</value>
        public System.Collections.ArrayList CurrentChildSlides
        {
            get { return currentChildSlides; }
            set { currentChildSlides = value; }
        }

        /// <summary>
        /// Gets or sets the current browsed slide set. Currently The browsed slide set is either Library Slides or MySlides Slides.
        /// </summary>
        /// <value>The current browsed slide set.</value>
        public string CurrentBrowsedSlideSet
        {
            get { return currentBrowsedSlideSet; }
            set { currentBrowsedSlideSet = value; }
        }

        public bool ShowResumeUpdateButton
        {
            get
            {
                return toolStripStatusLabelResume.Visible;
            }
            set
            {

                toolStripStatusLabelResume.Visible = value;
            }
        }

        public int UserID
        {
            get { return userId; }
            set { userId = value; }
        }

        #endregion

        #region Constructor
        public IpgMain()
        {
            InitializeComponent();
            // wire up the event
            NetworkChange.NetworkAvailabilityChanged += new NetworkAvailabilityChangedEventHandler(ApplicationNetworkAvailabilityChanged);
            NoAppUpdate += new NoAppUpdateEventHandler(EnforceProfile);
            SetupAppDirectories();
            SetupUserData();
            CurrentViewMode = ViewMode.Browse;
            _ContentUpdater = new ContentUpdater();
            _IssueLoggerService = new Client.wsIssueLogger.Logger();
        }
        #endregion

        void ApplicationNetworkAvailabilityChanged(object sender, NetworkAvailabilityEventArgs networkAvailabilityEventArgs)
        {
            // check to see if the interface is avilable
            string adpStatus = NetworkInterface.GetIsNetworkAvailable() ? "Connected" : "Disconnected";
            if (adpStatus == "Disconnected")
            {
                _NetConnected = false;
            }
            else
            {
                _NetConnected = true;
            }
        }

        #region form events




        private void IpgMain_Load(object sender, EventArgs e)
        { 
            //make sure that we have persisted user settings into the file of our own choosing
            Components.UserSettingsManager userSettingsManager = new Client.Components.UserSettingsManager();
            userSettingsManager.VerifyLocalUserSettings();
            //and now that we are sure, read them from the persisted location into the default location
            userSettingsManager.ReadLocalDataSettingsIntoDefault();

            //Reset the IsAppUpdate flag to false. It will be set later if there is a new update.
            Client.Properties.Settings.Default.IsAppUpdate = false;

            ////make sure user settings get upgraded for a given build
            //if (Properties.Settings.Default.UpgradeSettings)
            //{
            //    Properties.Settings.Default.Upgrade();
            //    Properties.Settings.Default.UpgradeSettings = false;
            //    Properties.Settings.Default.Save();
            //}
            isFirstRun = Properties.Settings.Default.IsFirstRun;

            //MessageBox.Show("Work in Progress: " + _ContentUpdater.IsWorkInProgress.ToString() + " - First Run: " + isFirstRun + " - First Content: " + Client.Properties.Settings.Default.FirstContent.ToString());

            //prepare ourselves for a possible major upgrade
            MajorUpgrade.Utilities.Upgrader majorUpgrader = new MajorUpgrade.Utilities.Upgrader();
            if (majorUpgrader.DetectMajorUpdate(Properties.Settings.Default.AppNameFull, Properties.Settings.Default.PublisherName))
            {
                Close();
                Application.Exit();
                return;
            }
            //else if no major update AND this is not first run and they have the desktop icon, repair it to the current app
            //this will execute each time the app is run, so we should definitely remove this once we assume all users are on the major upgrade
            else if (!Properties.Settings.Default.IsFirstRun)
            {
                AppSetup appReSetup = new AppSetup();
                //if desktop icon exists
                if (appReSetup.DoesDesktopShortcutExist)
                    appReSetup.CreateDesktopShortcut();
            }

            if (Properties.Settings.Default.IsFirstRun)
            {
                //Need to set this to true so that when the program loads it will do a full download.
                Properties.Settings.Default.FirstContent = true;
                Properties.Settings.Default.Save();

                Properties.Settings.Default.IsSupportFilesLocalized = true;
                Properties.Settings.Default.Save();

                //only two uses in any case
                Properties.Settings.Default.RemainingFreeUses = 2;
                Properties.Settings.Default.Save();

                //now make them register before anything else
                RegistrationForm frmRegistration = new RegistrationForm(Properties.Settings.Default.RemainingFreeUses);
                frmRegistration.ShowDialog(this);

                //if they decided not to register, decrement their remaining uses
                if (!Properties.Settings.Default.UserHasRegistered)
                {
                    Properties.Settings.Default.RemainingFreeUses--;
                    Properties.Settings.Default.Save();
                }

                if (Properties.Settings.Default.InstallType == PlugIns.Definitions.InstallationType.Online)
                {
                    ApplicationSetupWizard setupWiz = new ApplicationSetupWizard();
                    setupWiz.Owner = this;
                    setupWiz.ShowDialog();

                    //if this is > 0 then we got the content update, let's start using this app
                    //otherwise quit                    

                    //*SS Removiung "If" Block and just gonna show the Splash screen
                    //if ((!Properties.Settings.Default.IsContentLocalizationSupported && Properties.Settings.Default.LastLocalizedContentID > 0) ||
                    //(Properties.Settings.Default.IsContentLocalizationSupported && Properties.Settings.Default.LastLocalizedContentID > 0))
                    //{
                        SplashScreen.SplashScreen.ShowSplashScreen();
                        SplashScreen.SplashScreen.SetStatus("Setting up Slide Library...");
                    //}
                    //else
                    //{
                        //Close();
                        //return;
                    //}
                }
                else if (Properties.Settings.Default.InstallType == PlugIns.Definitions.InstallationType.CD)
                {
                    LocalizedContentSetupWizardCdVersion cdSetup = new LocalizedContentSetupWizardCdVersion();
                    cdSetup.Owner = this;
                    if (cdSetup.ShowDialog() != DialogResult.OK)
                    {
                        Close();
                        return;
                    }
                }

                
            }
            else
            {

                //now they're registered (via form or from a previous run) or they're not and what happens depends on how many uses they have
                if (!Properties.Settings.Default.UserHasRegistered)
                {
                    RegistrationForm frmRegistration = new RegistrationForm(Properties.Settings.Default.RemainingFreeUses);
                    frmRegistration.ShowDialog(this);

                    if (!Properties.Settings.Default.UserHasRegistered)
                    {
                        if (Properties.Settings.Default.RemainingFreeUses <= 0)
                        {
                            //warnings to the user about why we won't let them continue occurred in the registration form so we can just bail
                            Close();
                            return;
                        }
                        else
                        {
                            Properties.Settings.Default.RemainingFreeUses--;
                            Properties.Settings.Default.Save();
                        }
                    }
                }

                SplashScreen.SplashScreen.ShowSplashScreen();

                //check for application updates!
                SplashScreen.SplashScreen.SetStatus("Checking for application updates...");
                CheckForAppUpdate(true);

            }

            //we need to make sure this app has downloaded localized content
            if (Properties.Settings.Default.IsContentLocalizationSupported)
            {
                if (!Properties.Settings.Default.IsAppLocalizedContentReady)
                {
                    LocalizedContentAdapterWizard lwiz = new LocalizedContentAdapterWizard();
                    lwiz.ShowDialog();
                }
            }

            Upgrades.Upgrader upgrader = new Client.Upgrades.Upgrader();
            upgrader.DoNecessaryUpgrades();

            SplashScreen.SplashScreen.SetReferencePoint();
            SplashScreen.SplashScreen.SetStatus("Initializing data...");

            SplashScreen.SplashScreen.SetReferencePoint();
            SplashScreen.SplashScreen.SetStatus("Loading Slide Library data...");

            SetupCustomFeatures();

            PopulateLocalizedTreeviews();

            this.PrepareMySlides();
            SplashScreen.SplashScreen.SetReferencePoint();
            SplashScreen.SplashScreen.SetStatus("Loading My Presentations...");
            this.LoadMyPresentations();
            this.PopulateMySlideDecks();


            //Check if Indexing has been done and setup Search if they are allowing indexing
            if (!isFirstRun)
            {
                Setup_Indexing_and_Search_Fields();
                //Let's check the index to see if it is good/
                //LuceneSearch ls = new LuceneSearch();
                //LuceneIndex li = new LuceneIndex();
                //bool b = ls.is_index_valid();
                //if (!b)
                //    li.Clean_up_Index();
            }
            
            //SS*** Added for Advanced Search setup - Even if it is turned off. Just easier to do it here.
            PopulateAdvancedSearch();
           

            SetupButtons();
            ViewModeSetup();

            splPreviewType.Panel1Collapsed = true;

            clkAppUpdate.Start();

            AppSetup appSetup = new AppSetup();
            try
            {
                appSetup.RegistrySetup();
                Properties.Settings.Default.Save();
                Properties.Settings.Default.RegistryHasBeenConfigured = true;
            }
            catch (Exception)
            {
                // This will fix the registry once, and never again. Theoretically, these settings could be altered
                // by another program outside of the psg, and the user would see ppt slides open in powerpoint.  We 
                // have a fix for this, though.
                if (!Properties.Settings.Default.RegistryHasBeenConfigured)
                {
                    Process process = new Process();
                    try
                    {
                        process.Exited += new EventHandler(process_Exited);
                        process.EnableRaisingEvents = true;
                        string directoryPath = Path.Combine(Application.StartupPath, @"RegistrySetup");
                        process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                        process.StartInfo.FileName = Path.Combine(directoryPath, "PPTRegistryUpdatev2.exe");
                        process.Start();
                    }
                    catch (Exception)
                    {
                        try { process.Kill(); }
                        catch (Exception) { }
                        MessageBox.Show("Powerpoint settings were unable to be configured due to security settings of this machine." + Environment.NewLine + "Please contact support with questions if this program is not displaying powerpoints correctly.", "Powerpoint Settings");
                    }
                }
            }
        }

        private void process_Exited(object sender, EventArgs e)
        {
            Process process = sender as Process;
            int exitcode = process.ExitCode;
            if (exitcode != 0)
            {
                MessageBox.Show("Powerpoint settings were unable to be configured due to security settings of this machine." + Environment.NewLine + "Please contact support with questions if this program is not displaying powerpoints correctly.", "Powerpoint Settings");
            }
            else
            {
                Properties.Settings.Default.RegistryHasBeenConfigured = true;
                Properties.Settings.Default.Save();
            }
        }

        private void IpgMain_Shown(object sender, EventArgs e)
        {
            bool isEnabled = false;
            //try to hook up to the DB and record that you logged in 
            if (Components.AppUpdater.CheckDomainConnection())
            {
                try
                {
                    wsUserRegistration.UserRegistrationService urs = new Client.wsUserRegistration.UserRegistrationService();
                    urs.RecordUserLoginDateTimeAsync(Properties.Settings.Default.UserRegistrationEmail, "Client");
                }
                catch
                {
                    //do nothing
                }
                if (Properties.Settings.Default.UserHasRegistered)
                {
                    isEnabled = true;
                }
            }
            else
            {
                isEnabled = false;
            }
            profileToolStripMenuItem.Enabled = isEnabled;

            StartPowerPoint();
            _ContentUpdater.CheckForUpdateCompleted += new ContentUpdater.CheckForUpdateCompletedDelegate(_ContentUpdater_CheckForUpdateCompleted);
            
            if (!_ContentUpdater.IsWorkInProgress)
            {
                _ContentUpdater.CheckForLocalizedContentUpdateAsync(true);
            }
            else
            {
                MessageBox.Show(String.Format("The {0} content updating system is busy. Please try again later.", Properties.Settings.Default.AppNameFull));
            }
            GoToStartPage();

            if (Properties.Settings.Default.IsUpdatePaused)
            {
                ShowResumeUpdateButton = true;
            }
            else
            {
                ShowResumeUpdateButton = false;
            }
        }

        private void EnforceProfile(object sender, EventArgs e)
        {
            if (Components.AppUpdater.CheckDomainConnection())
            {
                ProfileEnforcer enforcer = new ProfileEnforcer();
                enforcer.Enforce();
            }
        }

        private void IpgMain_Layout(object sender, LayoutEventArgs e)
        {
            if (SplashScreen.SplashScreen.SplashForm != null)
            {
                this.Activate();
                SplashScreen.SplashScreen.CloseForm();
            }
        }

        private void IpgMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            //((ContentUpdateForm)Owner).CancelClicked = true;

            //save the default settings into our own persisted store
            Components.UserSettingsManager userSettingsManager = new Client.Components.UserSettingsManager();
            userSettingsManager.WriteLocalDataSettingsFileFromDefault();

            if (mySlidesHasChanges)
            {
                VerifyClearMySlides();
            }
            

            FinalizeSession();
            try
            {
                this.serviceLoggingWorker.PauseTimerAndRun();
            }
            catch (Exception) { }
            
        }

        #endregion

        #region Filters

        private void FilterSetup()
        {

            wsIpgSlideUpdater.SlideLibrary dsFilterData = new Client.wsIpgSlideUpdater.SlideLibrary();
            //make sure we have the xml we need
            if (!File.Exists(Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, AppSettings.LocalFiles.Default.MainData)))
            {
                try
                {
                    System.Net.WebClient wc = new System.Net.WebClient();
                    wc.DownloadFile(String.Format("{0}LocalizedUpdateData/{1}", Properties.Settings.Default.DataUpdateBaseURI, AppSettings.LocalFiles.Default.MainData),
                        Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, AppSettings.LocalFiles.Default.MainData));
                }
                catch (Exception ex)
                {
                    //TODO: handle this better
                    MessageBox.Show(String.Format("There was an error loading the {0} Slide Library. The application could not find the file: {1}. Please contact support for help with this issue.", Properties.Settings.Default.AppNameFull, Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, AppSettings.LocalFiles.Default.MainData)));
                    return;
                }
            }
            //get the filter data
            string mainDataFile1 = Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, AppSettings.LocalFiles.Default.MainData);
            try
            {
                dsFilterData.ReadXml(mainDataFile1);
            }
            catch
            {
                HandleCorruptedFile(mainDataFile1);
            }

            // Loop through the filter groups and add filter group tabs
            _selectedFilterIDs = new List<int>();
            foreach (wsIpgSlideUpdater.SlideLibrary.FilterGroupRow filterGroupRow in dsFilterData.FilterGroup.Rows)
            {
                // Create the tab and listview
                TabPage filterTab = new TabPage();
                ListView filterList = new ListView();
                filterList.CheckBoxes = true;
                filterList.View = View.List;
                filterList.Dock = DockStyle.Fill;

                // Now add related filter choices
                foreach (wsIpgSlideUpdater.SlideLibrary.FilterRow filterRow in filterGroupRow.GetFilterRows())
                {
                    ListViewItem filterItem = new ListViewItem(filterRow.FilterName);
                    filterItem.Tag = filterRow;
                    filterItem.Checked = true;
                    filterList.Items.Add(filterItem);
                    _selectedFilterIDs.Add(filterRow.FilterID);
                }

                // Add the tab
                filterTab.Tag = filterGroupRow;
                filterTab.Text = filterGroupRow.FilterGroupName;
                tabFilterGroups.TabPages.Add(filterTab);
                filterTab.Controls.Add(filterList);
            }
        }

        private void GetSelectedFilters()
        {
            _selectedFilterIDs = new List<int>();
            //determine which filters have been selected
            //check each TabPage
            foreach (TabPage tp in tabFilterGroups.TabPages)
            {
                //now find the ListView on the TabPage
                foreach (Control control in tp.Controls)
                {
                    if (control.GetType() == typeof(ListView))
                    {
                        ListView filtersListView = (ListView)control;
                        foreach (ListViewItem lvItem in filtersListView.CheckedItems)
                        {
                            _selectedFilterIDs.Add(((wsIpgSlideUpdater.SlideLibrary.FilterRow)lvItem.Tag).FilterID);
                        }
                    }
                }
            }
        }

        private void btnApplyFilterSettings_Click(object sender, EventArgs e)
        {
            GetSelectedFilters();
            PopulateLocalizedTreeviews();
            navigationPaneLeftPane.SelectNavigationPage(navigationPanePageSlideLibrary);
        }

        #endregion

        #region Initalization methods

        private void SetupAppDirectories()
        {
            //initializes application data directories, creates them if not already existing
            AppSettings.LocalPaths.Default.ThumbnailDirectory = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                Properties.Settings.Default.AppDataDirectory.ToString() + @"\Resources\Images\ThumbSlide");
            if (!Directory.Exists(AppSettings.LocalPaths.Default.ThumbnailDirectory))
            {
                Directory.CreateDirectory(AppSettings.LocalPaths.Default.ThumbnailDirectory);
            }

            AppSettings.LocalPaths.Default.SlideDirectory = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                            Properties.Settings.Default.AppDataDirectory.ToString() + @"\Resources\Ppt");
            if (!Directory.Exists(AppSettings.LocalPaths.Default.SlideDirectory))
            {
                Directory.CreateDirectory(AppSettings.LocalPaths.Default.SlideDirectory);
            }

            AppSettings.LocalPaths.Default.HtmlDirectory = Path.Combine(Application.StartupPath, @"Html");

            AppSettings.LocalPaths.Default.TempDirectory = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                            Properties.Settings.Default.AppDataDirectory.ToString() + @"\Temp");
            if (!Directory.Exists(AppSettings.LocalPaths.Default.TempDirectory))
            {
                Directory.CreateDirectory(AppSettings.LocalPaths.Default.TempDirectory);
            }
            AppSettings.LocalPaths.Default.XmlDirectory = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                        Properties.Settings.Default.AppDataDirectory.ToString() + @"\Resources\Xml");
            if (!Directory.Exists(AppSettings.LocalPaths.Default.XmlDirectory))
            {
                Directory.CreateDirectory(AppSettings.LocalPaths.Default.XmlDirectory);
            }
        }

        public void SetupUserData()
        {
            userId = GetUserId(Properties.Settings.Default.UserRegistrationEmail);
            sessionId = Guid.NewGuid();
            sessionStart = DateTime.Now;
            UserEventsDS events = new UserEventsDS();
            string path = Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, AppSettings.LocalFiles.Default.UserEventsData);

            if (File.Exists(path))
            {
                events.ReadXml(path);
                if (events.EventTypes.Count == 0)
                    SetupSeedData(events);
            }
            else
                SetupSeedData(events);
            UserEventsDS.SessionsRow row = events.Sessions.NewSessionsRow();
            row.UserID = userId;
            row.Session = sessionId.ToString();
            row.StartDate = sessionStart;
            events.Sessions.AddSessionsRow(row);
            events.WriteXml(path);
        }

        private void SetupSeedData(UserEventsDS events)
        {
            events.EventTypes.AddEventTypesRow("Viewed");
            events.EventTypes.AddEventTypesRow("Played");
            events.EventTypes.AddEventTypesRow("Saved");
            events.EventTypes.AddEventTypesRow("Exported");
            events.EventTypes.AddEventTypesRow("Printed");
        }

        private int GetUserId(string email)
        {
            if (Properties.Settings.Default.UserID != 0)
                return Properties.Settings.Default.UserID;
            try
            {               
                UsageLoggingService service = new UsageLoggingService();
                int userId = service.GetUserByEmail(email);
                if (userId != 0)
                {
                    Properties.Settings.Default.UserID = userId;
                    Properties.Settings.Default.Save();
                }
                return userId;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public void LoadPreviewSlidePreviewThumbsAsync()
        {
            //_TempStopwatch = new Stopwatch();
            //_TempStopwatch.Start();
            navigationPanePageSlideLibrary.Enabled = false;
            if (backgroundWorkerPreviewThumbnails.IsBusy)
            {
                return;
                //backgroundWorkerPreviewThumbnails.CancelAsync();
                //System.Threading.Thread.Sleep(300);
            }

            imlTinyThumbs.Images.Clear();
            lstPreview.LargeImageList = null;
            try
            {
                backgroundWorkerPreviewThumbnails.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                LogIssue(ex, "backgroundWorkerPreviewThumbnails.RunWorkerAsync()");
            }

        }

        private void backgroundWorkerPreviewThumbnails_DoWork(object sender, DoWorkEventArgs e)
        {
            int i = 0;
            double percentProcessed;
            foreach (wsIpgSlideUpdater.SlideLibrary.SlideRow slide in currentChildSlides)
            {
                string imageFilename = slide.FileName.Replace(".pot", ".jpg");

                if (!System.IO.File.Exists(Path.Combine(AppSettings.LocalPaths.Default.ThumbnailDirectory, imageFilename)))
                {
                    //if the file wasn't found, let's try to get it
                    try
                    {
                        i++;
                        percentProcessed = (Convert.ToDouble(i) / Convert.ToDouble(currentChildSlides.Count)) * 100.00;
                        backgroundWorkerPreviewThumbnails.ReportProgress(Convert.ToInt32(percentProcessed), "Downloading missing thumbnail image");

                        //System.Net.WebClient wc = new System.Net.WebClient();
                        WebHelpers.WebClientHelper wc = new Client.WebHelpers.WebClientHelper();
                        wc.DownloadFile(Properties.Settings.Default.ServerImageBaseURI + imageFilename, Path.Combine(AppSettings.LocalPaths.Default.ThumbnailDirectory, imageFilename));
                        imlTinyThumbs.Images.Add(imageFilename, Image.FromFile(Path.Combine(AppSettings.LocalPaths.Default.ThumbnailDirectory, imageFilename)));
                    }
                    catch (Exception ex)
                    {
                        LogIssue(ex, String.Format("Thumbnail was not found. Tried to download and add to image list.  {0}{1}", Properties.Settings.Default.ServerImageBaseURI, imageFilename));
                    }
                }
                else
                {
                    try
                    {
                        i++;
                        percentProcessed = (Convert.ToDouble(i) / Convert.ToDouble(currentChildSlides.Count)) * 100.00;
                        backgroundWorkerPreviewThumbnails.ReportProgress(Convert.ToInt32(percentProcessed), "Loading thumbnails");

                        imlTinyThumbs.Images.Add(imageFilename, Image.FromFile(Path.Combine(AppSettings.LocalPaths.Default.ThumbnailDirectory, imageFilename)));
                    }
                    catch (Exception ex)
                    {
                        LogIssue(ex, "Thumbnail was found. Tried to add to image list.");
                    }
                }
            }
        }

        private void backgroundWorkerPreviewThumbnails_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            toolStripStatusLabelThumbnail.Text = e.UserState.ToString();
            toolStripProgressBarThumbnail.Value = e.ProgressPercentage;
            toolStripStatusLabelThumbnail.Visible = true;
            toolStripProgressBarThumbnail.Visible = true;
        }

        private void backgroundWorkerPreviewThumbnails_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            toolStripStatusLabelThumbnail.Visible = false;
            toolStripProgressBarThumbnail.Visible = false;
            if (!e.Cancelled)
            {
                lstPreview.LargeImageList = imlTinyThumbs;
                navigationPanePageSlideLibrary.Enabled = true;
                PopulatePreviewSlidesList();
                ActivatePreviewListItem(0);
                this.SetLoggingEnabled(true);
            }

            SetupButtons();
            //_TempStopwatch.Stop();
            //Debug.WriteLine("This click took: " + _TempStopwatch.Elapsed.ToString()); 

        }

        public void LoadMySlidesThumb(string thumbnailFilename)
        {
            // TODO generates a stack overflow under certain conditions
            if (File.Exists(Path.Combine(AppSettings.LocalPaths.Default.ThumbnailDirectory, thumbnailFilename)))
            {
                try
                {
                    imlSlideThumbs.Images.Add(thumbnailFilename, Image.FromFile(Path.Combine(AppSettings.LocalPaths.Default.ThumbnailDirectory, thumbnailFilename)));
                }
                catch (OutOfMemoryException ox)
                {
                    this.LogIssue(ox, "IpgMain::LoadMySlidesThumb(\"" + thumbnailFilename + "\"): Occurs for a case when the thumb image displayed in the image list is blank.");
                }
            }
        }

        private void PrepareMySlides()
        {
            this.lstMySlides.LargeImageList = this.imlSlideThumbs;
            this.lstMySlides.ListViewItemSorter = new Components.ListViewIndexComparer();
            this.lstMySlides.InsertionMark.Color = SystemColors.GradientActiveCaption;
        }

        private void PopulateMySlideDecks()
        {
            if (tvwMySlideDecks.Nodes.Count > 0)
            {
                tvwMySlideDecks.Nodes.Clear();
            }

            tvwMySlideDecks.BeginUpdate();
            TreeNode rootNode = new TreeNode("My Slide Decks");
            rootNode.Tag = "root";
            rootNode.ImageKey = "my-slide-decks.ico";
            rootNode.SelectedImageKey = "my-slide-decks.ico";
            rootNode.StateImageKey = "my-slide-decks.ico";
            tvwMySlideDecks.Nodes.Add(rootNode);

            //create my presentation nodes
            foreach (DataSets.MyPresentations.PresentationRow presentation in dsMyPresentations.Presentation.Rows)
            {
                SplashScreen.SplashScreen.SetReferencePoint();
                TreeNode presentationNode = new TreeNode(presentation.PresentationName);
                presentationNode.Tag = presentation;
                presentationNode.ImageKey = "multi-slides.ico";
                presentationNode.SelectedImageKey = "multi-slides.ico";
                presentationNode.StateImageKey = "multi-slides.ico";
                rootNode.Nodes.Add(presentationNode);
                DataRow[] slides = dsMyPresentations.PresentationSlide.Select("PresentationID =  " + presentation.PresentationID.ToString());

                //we decided not to show these
                //foreach (DataSets.MyPresentations.PresentationSlideRow slide in slides)
                //{
                //    TreeNode slideNode = new TreeNode(slide.Title);
                //    slideNode.Tag = slide;
                //    slideNode.ImageKey = "single-slide.ico";
                //    slideNode.SelectedImageKey = "single-slide.ico";
                //    slideNode.StateImageKey = "single-slide.ico";
                //    presentationNode.Nodes.Add(slideNode);
                //}
            }

            tvwMySlideDecks.EndUpdate();

            if (tvwMySlideDecks.Nodes.Count > 0)
            {
                tvwMySlideDecks.Nodes[0].Expand();
            }
        }

        private void LoadMyPresentations()
        {
            SplashScreen.SplashScreen.SetReferencePoint();
            if (!File.Exists(AppSettings.LocalPaths.Default.XmlDirectory + "\\MyPresentations.xml"))
            {
                dsMyPresentations.WriteXml(AppSettings.LocalPaths.Default.XmlDirectory + "\\MyPresentations.xml");
            }
            dsMyPresentations.ReadXml(AppSettings.LocalPaths.Default.XmlDirectory + "\\MyPresentations.xml");
        }

        public void SetupButtons()
        {
            tsbSlideLibraryAddToMySlides.Text = "Add to" + Environment.NewLine + "My Slides";
            tsbSlideLibrarySelectAll.Text = "Select" + Environment.NewLine + "All";
            tsbSlideLibraryExportPpt.Text = "Export to" + Environment.NewLine + "PowerPoint";
            tsbSlideLibraryPlay.Text = "Play";
            tsbSlideLibraryPrint.Text = "Print";


            tsbMySlideRemoveSlide.Text = "Remove";
            tsbMySlidesPlay.Text = "Play";
            tsbMySlidesSave.Text = "Save";
            tsbMySlidesSaveAs.Text = "Save As";
            tsbMySlidesSelectAll.Text = "Select" + Environment.NewLine + "All";
            tsbMySlidesClear.Text = "Remove" + Environment.NewLine + "All";
            tsbMySlidesExportPpt.Text = "Export to" + Environment.NewLine + "PowerPoint";
            tsbMySlidesPrint.Text = "Print";

            if (mySlidesHasChanges)
            {
                tsbMySlidesSave.Enabled = true;
                tsbMySlidesSaveAs.Enabled = true;
            }
            else
            {
                tsbMySlidesSave.Enabled = false;
                tsbMySlidesSaveAs.Enabled = false;
            }

            if (lstMySlides.Items.Count > 0)
            {
                tsbMySlidesPlay.Enabled = true;
                //tsbMySlidesPreviewSlide.Enabled = true;
                tsbMySlidesSelectAll.Enabled = true;
                tsbMySlidesClear.Enabled = true;
                tsbMySlideRemoveSlide.Enabled = true;
                tsbMySlidesExportPpt.Enabled = true;
                tsbMySlidesPrint.Enabled = true;
            }
            else
            {
                tsbMySlidesPlay.Enabled = false;
                //tsbMySlidesPreviewSlide.Enabled = false;
                tsbMySlidesSelectAll.Enabled = false;
                tsbMySlidesClear.Enabled = false;
                tsbMySlideRemoveSlide.Enabled = false;
                tsbMySlidesExportPpt.Enabled = false;
                tsbMySlidesPrint.Enabled = false;
            }

            if (lstPreview.Items.Count > 0)
            {
                tsbSlideLibraryAddToMySlides.Enabled = true;
                //tsbSlideLibraryPreview.Enabled = true;
                tsbSlideLibrarySelectAll.Enabled = true;
                tsbSlideLibraryPlay.Enabled = true;
                tsbSlideLibraryExportPpt.Enabled = true;
                tsbSlideLibraryPrint.Enabled = true;
            }
            else
            {
                tsbSlideLibraryAddToMySlides.Enabled = false;
                //tsbSlideLibraryPreview.Enabled = false;
                tsbSlideLibrarySelectAll.Enabled = false;
                tsbSlideLibraryPlay.Enabled = false;
                tsbSlideLibraryExportPpt.Enabled = false;
                tsbSlideLibraryPrint.Enabled = false;
            }
        }

        private void SetupCustomFeatures()
        {

            if (Properties.Settings.Default.IsContentLocalizationSupported)
            {

            }
            else
            {

            }

            //setup filtering support
            if (Properties.Settings.Default.IsFilteringSupported)
            {
                navigationPanePageFilters.Visible = true;
                FilterSetup();
            }
            else
            {
                //navigationPanePageFilters.Visible = false;
                navigationPaneLeftPane.NavigationPages.Remove(navigationPanePageFilters);
            }

            //setup settings support
            if (Properties.Settings.Default.IsSettingsSupported)
            {
                tsbMainSettings.Visible = true;
            }
            else
            {
                tsbMainSettings.Visible = false;
            }
            //setup registry fix help text
            if (Properties.Settings.Default.RegistryFixHelp)
            {
                llRegistryFix.Visible = true;
                btnregistryFixClose.Visible = true;
            }
            else
            {
                llRegistryFix.Visible = false;
                btnregistryFixClose.Visible = false;
            }
        }

        public void ViewModeSetup()
        {

            splPreviewType.Panel1Collapsed = false;
            if (CurrentViewMode == ViewMode.Build)
            {
                //configure panels
                splViewers.Panel1Collapsed = false;
                splViewers.Panel2Collapsed = false;
                splPreviewType.Panel1Collapsed = false;
                splPreviewType.Panel2Collapsed = false;

                splViewers.Orientation = Orientation.Horizontal;
                if (splViewers.SplitterDistance > 525)
                {
                    splViewers.SplitterDistance = 525;
                }
                if (splPreviewType.SplitterDistance < 100)
                {
                    splPreviewType.SplitterDistance = 100;
                }

                lstPreview.LargeImageList = imlTinyThumbs;

                //show/hide buttons
                tsbSlideLibraryAddToMySlides.Visible = true;
                tsbSlideLibraryExportPpt.Visible = true;
                tsbSlideLibraryPlay.Visible = true;
                tsbSlideLibrarySelectAll.Visible = true;
                if (Properties.Settings.Default.AllowIndexing && ind.Index_Exists() && !_index_in_progress)
                {
                    if (splViewerBottom.Panel2Collapsed)
                    {
                        tsbSearchShow.Visible = true;
                        tsbSearchHide.Visible = false;
                    }
                    else
                    {
                        tsbSearchShow.Visible = false;
                        tsbSearchHide.Visible = true;
                    }
                }
                else
                {
                    splViewerBottom.Panel2Collapsed = true;
                    tsbSearchShow.Visible = false;
                    tsbSearchHide.Visible = false;
                }
            }
            else if (CurrentViewMode == ViewMode.Browse)
            {
                //configure panels
                splViewers.Panel1Collapsed = false;
                splViewers.Panel2Collapsed = true;
                splPreviewType.Panel1Collapsed = false;
                splPreviewType.Panel2Collapsed = false;
                if (splPreviewType.SplitterDistance < 100)
                {
                    splPreviewType.SplitterDistance = 100;
                }

                lstPreview.LargeImageList = imlTinyThumbs;

                //show/hide buttons
                tsbSlideLibraryAddToMySlides.Visible = true;
                tsbSlideLibraryExportPpt.Visible = true;
                tsbSlideLibraryPlay.Visible = true;
                tsbSlideLibrarySelectAll.Visible = true;
                tsbSearchShow.Visible = false;
                tsbSearchHide.Visible = false;
            }            
        }

        public void PopulateLocalizedTreeviews()
        {
            List<int> RemoveRegionLanguageList = new List<int>();
            wsIpgSlideUpdater.SlideLibrary dsTempSlideLibrary = new Client.wsIpgSlideUpdater.SlideLibrary();
            //make sure we have the xml we need
            if (!File.Exists(Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, AppSettings.LocalFiles.Default.MainData)))
            {
                try
                {
                    System.Net.WebClient wc = new System.Net.WebClient();
                    wc.DownloadFile(String.Format("{0}LocalizedUpdateData/{1}", Properties.Settings.Default.DataUpdateBaseURI, AppSettings.LocalFiles.Default.MainData),
                        Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, AppSettings.LocalFiles.Default.MainData));
                }
                catch (Exception ex)
                {
                    //TODO: handle this better
                    MessageBox.Show(String.Format("There was an error loading the {0} Slide Library. The application could not find the file: {1}. Please contact support for help with this issue.", Properties.Settings.Default.AppNameFull, Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, AppSettings.LocalFiles.Default.MainData)));
                    return;
                }
            }
            string mainDataFile = Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, AppSettings.LocalFiles.Default.MainData);
            try
            {
                dsTempSlideLibrary.ReadXml(mainDataFile);
            }
            catch
            {
                HandleCorruptedFile(mainDataFile);
            }
            MainSlideLibrary = new Client.wsIpgSlideUpdater.SlideLibrary();
            MainSlideLibrary.Merge(dsTempSlideLibrary);

            DataSets.MyRegionLanguages dsMyRegLangs = new Client.DataSets.MyRegionLanguages();
            //if they are missing the MyRegionLangauges file, prompt them
            if (!File.Exists(Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, AppSettings.LocalFiles.Default.MyRegionLangauges)))
            {
                //At this point they will need to donwload new files. We need to reset the index properties and redo the full index.

                MessageBox.Show("Your Region/Languages Settings are missing or have been corrupted. Click OK to begin repairing your settings.", "Region/Languages Settings Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                MyRegionLanguagesDialog myRegLangDialog = new MyRegionLanguagesDialog();
                myRegLangDialog.ShowDialog(this);
            }
            dsMyRegLangs.ReadXml(Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, AppSettings.LocalFiles.Default.MyRegionLangauges));

            if (Properties.Settings.Default.IsContentLocalizationSupported)
            {
                LocalizedSlideLibraries = new List<Client.wsIpgSlideUpdater.SlideLibrary>();

                if (tabRegionLanguageTrees.TabCount > 0)
                {
                    tabRegionLanguageTrees.TabPages.Clear();
                }
                foreach (DataSets.MyRegionLanguages.MyRegionLanguagesRow myRegLangRow in dsMyRegLangs._MyRegionLanguages.Rows)
                {
                    //make sure we have the xml we need
                    if (!File.Exists(Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, String.Format("{0}{1}", myRegLangRow.RegionLanguageID, AppSettings.LocalFiles.Default.SlideDataFilePart))))
                    {
                        try
                        {
                            System.Net.WebClient wc = new System.Net.WebClient();
                            wc.DownloadFile(String.Format("{0}LocalizedUpdateData/{1}{2}", Properties.Settings.Default.DataUpdateBaseURI, myRegLangRow.RegionLanguageID.ToString(), AppSettings.LocalFiles.Default.SlideDataFilePart),
                                Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, String.Format("{0}{1}", myRegLangRow.RegionLanguageID, AppSettings.LocalFiles.Default.SlideDataFilePart)));
                        }
                        catch (Exception ex)
                        {
                            //TODO: handle this better
                            MessageBox.Show(String.Format("There was an error loading the {0} Slide Library. The application could not find the file: {1}. Please contact support for help with this issue.", Properties.Settings.Default.AppNameFull, Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, String.Format("{0}{1}", myRegLangRow.RegionLanguageID, AppSettings.LocalFiles.Default.SlideDataFilePart))));
                            return;
                        }
                    }

                    //make sure this RegionLanguagues xml file exists before we load it
                    if (File.Exists(Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, String.Format("{0}{1}", myRegLangRow.RegionLanguageID, AppSettings.LocalFiles.Default.SlideDataFilePart))))
                    {
                        //create a localized dataset for this RegLang
                        wsIpgSlideUpdater.SlideLibrary dsLocalizedSlideLibrary = new Client.wsIpgSlideUpdater.SlideLibrary();
                        dsLocalizedSlideLibrary.EnforceConstraints = false;
                        string slideDataFile = Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, String.Format("{0}{1}", myRegLangRow.RegionLanguageID, AppSettings.LocalFiles.Default.SlideDataFilePart));

                        try
                        {
                            dsLocalizedSlideLibrary.ReadXml(slideDataFile);
                        }
                        catch(Exception ex)
                        {
                            HandleCorruptedFile(slideDataFile);
                        }
                        
                        dsLocalizedSlideLibrary.Category.Merge(dsTempSlideLibrary.Category);

                        //add it to the collection
                        LocalizedSlideLibraries.Add(dsTempSlideLibrary);

                        Client.wsIpgSlideUpdater.SlideLibrary.RegionLanguageRow rlRow = dsTempSlideLibrary.RegionLanguage.FindByRegionLanguageID(myRegLangRow.RegionLanguageID);
                        //make the tab page

                        if (rlRow != null)
                        {
                            TabPage regLangTabPage = new TabPage();
                            regLangTabPage.Text = String.Format("{0}/{1}",
                                dsTempSlideLibrary.RegionLanguage.FindByRegionLanguageID(myRegLangRow.RegionLanguageID).RegionRow.ShortName,
                                dsTempSlideLibrary.RegionLanguage.FindByRegionLanguageID(myRegLangRow.RegionLanguageID).LanguageRow.LongName);
                            //regLangTabPage.Tag = rlRow;
                            tabRegionLanguageTrees.TabPages.Add(regLangTabPage);

                            //make a catSlide tree, fill it, and put it on the tab
                            BLL.CategorySlideTreeView catSlideTree = new BLL.CategorySlideTreeView();
                            catSlideTree.ImageList = imlIcons16;
                            catSlideTree.AllowDrop = true;
                            //catSlideTree.Tag = rlRow;
                            catSlideTree.RightClickSelectsNode = true;
                            regLangTabPage.Controls.Add(catSlideTree);

                            catSlideTree.FillCategorySlideData(ApplySlideLibraryFilters(dsLocalizedSlideLibrary));
                            catSlideTree.Dock = DockStyle.Fill;

                            //merge the localized content into the main trees slidelibrary dataset
                        }
                        else
                        {
                            RemoveRegionLanguageList.Add(myRegLangRow.RegionLanguageID);
                        }

                        MainSlideLibrary.Merge(dsLocalizedSlideLibrary);
                    }
                }

                if (RemoveRegionLanguageList.Count > 0)
                {
                    foreach (int i in RemoveRegionLanguageList)
                    {
                        Client.DataSets.MyRegionLanguages.MyRegionLanguagesRow row = dsMyRegLangs._MyRegionLanguages.FindByRegionLanguageID (i);
                        row.Delete();
                    }
                    dsMyRegLangs.WriteXml(Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, AppSettings.LocalFiles.Default.MyRegionLangauges));
                }
            }
            else
            {
                tabRegionLanguageTrees.Dispose();
                navigationPanePageSlideLibrary.Controls.Clear();

                DataSets.MyRegionLanguages.MyRegionLanguagesRow myRegLangRow = (DataSets.MyRegionLanguages.MyRegionLanguagesRow)dsMyRegLangs._MyRegionLanguages[0];

                //make sure this RegionLanguagues xml file exists before we load it
                if (File.Exists(Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, String.Format("{0}{1}", myRegLangRow.RegionLanguageID, AppSettings.LocalFiles.Default.SlideDataFilePart))))
                {
                    //create a localized dataset for this RegLang
                    wsIpgSlideUpdater.SlideLibrary dsLocalizedSlideLibrary = new Client.wsIpgSlideUpdater.SlideLibrary();
                    dsLocalizedSlideLibrary.EnforceConstraints = false;
                    string slideDataFile = Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, String.Format("{0}{1}", myRegLangRow.RegionLanguageID, AppSettings.LocalFiles.Default.SlideDataFilePart));
                    try
                    {
                        dsLocalizedSlideLibrary.ReadXml(slideDataFile);
                    }
                    catch
                    {
                        HandleCorruptedFile(slideDataFile);
                    }
                    dsLocalizedSlideLibrary.Category.Merge(dsTempSlideLibrary.Category);

                    //make a catSlide tree, fill it
                    BLL.CategorySlideTreeView catSlideTree = new BLL.CategorySlideTreeView();
                    navigationPanePageSlideLibrary.Controls.Add(catSlideTree);
                    catSlideTree.ImageList = imlIcons16;
                    catSlideTree.AllowDrop = true;
                    //catSlideTree.Tag = rlRow;
                    catSlideTree.RightClickSelectsNode = true;
                    catSlideTree.FillCategorySlideData(dsLocalizedSlideLibrary);
                    catSlideTree.Dock = DockStyle.Fill;

                    //merge the localized content into the main trees slidelibrary dataset
                    MainSlideLibrary.Merge(dsLocalizedSlideLibrary);
                }
            }

            //some of the orignal functionality, such as saving My Slides relies upon dsSlideLibrary
            //that's why we're doing this
            dsSlideLibrary = new Client.wsIpgSlideUpdater.SlideLibrary();
            dsSlideLibrary.Merge(MainSlideLibrary);
        }

        #region TreeView Setup

        public wsIpgSlideUpdater.SlideLibrary ApplySlideLibraryFilters(wsIpgSlideUpdater.SlideLibrary dsFilterSlideLibrary)
        {
            foreach (wsIpgSlideUpdater.SlideLibrary.SlideRow slide in dsFilterSlideLibrary.Slide.Rows)
            {
                if (NotInSelectedFilters(slide))
                {
                    List<wsIpgSlideUpdater.SlideLibrary.SlideCategoryRow> slideCats = new List<Client.wsIpgSlideUpdater.SlideLibrary.SlideCategoryRow>(slide.GetSlideCategoryRows());
                    foreach (wsIpgSlideUpdater.SlideLibrary.SlideCategoryRow slideCat in slideCats)
                    {
                        dsFilterSlideLibrary.SlideCategory.RemoveSlideCategoryRow(slideCat);
                    }
                }
            }
            return dsFilterSlideLibrary;
        }

        private bool NotInSelectedFilters(wsIpgSlideUpdater.SlideLibrary.SlideRow slide)
        {

            List<wsIpgSlideUpdater.SlideLibrary.SlideFilterRow> slideFilters = new List<Client.wsIpgSlideUpdater.SlideLibrary.SlideFilterRow>(slide.GetSlideFilterRows());

            //check each of this slide's SlideFilter rows
            //if we find one that has been selected return false (it IS in the selected filters)
            foreach (wsIpgSlideUpdater.SlideLibrary.SlideFilterRow slideFilter in slideFilters)
            {
                if (_selectedFilterIDs.Contains(slideFilter.FilterID))
                {
                    return false;
                }
            }
            //if we got here then we didn't find any associated SlideFilters that matched the selected filters
            return true;
        }

        private string GetSlideFilterText(wsIpgSlideUpdater.SlideLibrary.SlideRow slide)
        {

            List<wsIpgSlideUpdater.SlideLibrary.SlideFilterRow> slideFilters = new List<Client.wsIpgSlideUpdater.SlideLibrary.SlideFilterRow>(slide.GetSlideFilterRows());
            StringBuilder sb = new StringBuilder();
            foreach (wsIpgSlideUpdater.SlideLibrary.SlideFilterRow slideFilter in slideFilters)
            {
                try
                {
                    sb.Append(slideFilter.FilterRow.FilterGroupRow.FilterGroupName + "-" + slideFilter.FilterRow.FilterName);
                    sb.Append(System.Environment.NewLine);
                }
                catch (Exception ex)
                {
                    //TODO: handle exception
                }
            }
            //if we got some slide filters return them, otherwise just return the title
            if (sb.Length > 0)
            {
                return slide.Title + System.Environment.NewLine + sb.ToString();
            }

            return slide.Title;

        }

        #endregion

        #endregion

        #region TreeView Events

        public void ActivatePreviewListItem(int index)
        {
            lstPreview.Focus();
            lstPreview.Items[index].Focused = true;
            lstPreview.Items[index].Selected = true;
            // TODO: from here down does absolutely nothing.
            // The above 3 lines raise the selected index changed event for lstPreview.
            object eventSender = new object();
            ListViewItemSelectionChangedEventArgs eventArgs = new ListViewItemSelectionChangedEventArgs(lstPreview.Items[index], index, true);
            //this.lstPreview_ItemSelectionChanged(eventSender, eventArgs);
        }

        private void ActivateMySlideListItem(int index)
        {
            try
            {
                lstMySlides.Focus();
                lstMySlides.Items[index].Focused = true;
                lstMySlides.Items[index].Selected = true;
                object eventSender = new object();
                ListViewItemSelectionChangedEventArgs eventArgs = new ListViewItemSelectionChangedEventArgs(lstMySlides.Items[index], index, true);
                // TODO: shouldn't sender be lstMySlides
                this.lstMySlides_ItemSelectionChanged(eventSender, eventArgs);
            }
            catch (Exception ex)
            {
                //TODO: do nothing???
            }
        }

        #endregion

        #region DragDrop

        private void lstMySlides_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.All;
        }

        private void lstMySlides_DragOver(object sender, DragEventArgs e)
        {
            Point targetPoint = this.lstMySlides.PointToClient(new Point(e.X, e.Y));
            // Retrieve the index of the item closest to the mouse pointer.
            int targetIndex = lstMySlides.InsertionMark.NearestIndex(targetPoint);

            // Confirm that the mouse pointer is not over the dragged item.
            if (targetIndex > -1)
            {
                // Determine whether the mouse pointer is to the left or
                // the right of the midpoint of the closest item and set
                // the InsertionMark.AppearsAfterItem property accordingly.
                Rectangle itemBounds = lstMySlides.GetItemRect(targetIndex);
                if (targetPoint.X > itemBounds.Left + (itemBounds.Width / 2))
                {
                    lstMySlides.InsertionMark.AppearsAfterItem = true;
                }
                else
                {
                    lstMySlides.InsertionMark.AppearsAfterItem = false;
                }
            }

            // Set the location of the insertion mark. If the mouse is
            // over the dragged item, the targetIndex value is -1 and
            // the insertion mark disappears.
            lstMySlides.InsertionMark.Index = targetIndex;
        }

        private void lstPreview_ItemMouseHover(object sender, ListViewItemMouseHoverEventArgs e)
        {
            Cursor = Cursors.Hand;
        }

        private void lstPreview_ItemDrag(object sender, ItemDragEventArgs e)
        {
            this.draggedItemsControl = "Preview";
            if (this.draggedItems.Count > 0)
            {
                this.draggedItems.Clear();
            }
            foreach (ListViewItem lvItem in lstPreview.SelectedItems)
            {
                this.draggedItems.Add(lvItem.Tag);
                LoadMySlidesThumb(lvItem.ImageKey);
            }

            ((Control)sender).DoDragDrop(((Control)sender).Name, DragDropEffects.All);
        }

        private void DropPreviewSlides()
        {
            // Retrieve the index of the insertion mark;
            int targetIndex = lstMySlides.InsertionMark.Index;

            // If the insertion mark is not visible, add item and exit the method.
            if (targetIndex == -1)
            {
                foreach (wsIpgSlideUpdater.SlideLibrary.SlideRow slide in this.draggedItems)
                {
                    string imageFileName = slide.FileName.Replace("pot", "jpg");
                    ListViewItem newItem = new ListViewItem(slide.Title, imageFileName);
                    newItem.Tag = slide;
                    this.lstMySlides.Items.Add(newItem);
                }
            }
            else
            {
                // If the insertion mark is to the right of the item with
                // the corresponding index, increment the target index.
                if (lstMySlides.InsertionMark.AppearsAfterItem)
                {
                    targetIndex++;
                }
                int i = 0;
                foreach (wsIpgSlideUpdater.SlideLibrary.SlideRow slide in this.draggedItems)
                {
                    string imageFileName = slide.FileName.Replace("pot", "jpg");
                    ListViewItem newItem = new ListViewItem(slide.Title, imageFileName);
                    newItem.Tag = slide;
                    this.lstMySlides.Items.Insert(targetIndex + i, newItem);
                    i++;
                }
            }
        }

        private void lstMySlides_DragDrop(object sender, DragEventArgs e)
        {
            if (!mySlidesHasChanges)
            {
                lblMySlides.Text = lblMySlides.Text + "*";
            }
            mySlidesHasChanges = true;

            if (this.draggedItemsControl == "Preview")
            {
                DropPreviewSlides();
            }
            else if (this.draggedItemsControl == "MySlides")
            {
                DropMySlides();
            }
            CountMySlides();
            SetupButtons();
        }

        private void lstMySlides_ItemDrag(object sender, ItemDragEventArgs e)
        {
            this.draggedItemsControl = "MySlides";
            if (draggedItems.Count > 0)
            {
                draggedItems.Clear();
            }
            foreach (ListViewItem lvItem in lstMySlides.SelectedItems)
            {
                draggedItems.Add(lvItem);
            }
            ((Control)sender).DoDragDrop(((Control)sender).Name, DragDropEffects.All);
        }

        private void DropMySlides()
        {
            // Retrieve the index of the insertion mark;
            int targetIndex = lstMySlides.InsertionMark.Index;

            // If the insertion mark is to the right of the item with
            // the corresponding index, increment the target index.
            if (lstMySlides.InsertionMark.AppearsAfterItem)
            {
                targetIndex++;
            }
            int i = 0;
            foreach (ListViewItem oldItem in this.draggedItems)
            {
                ListViewItem newItem = (ListViewItem)oldItem.Clone();
                this.lstMySlides.Items.Insert(targetIndex + i, newItem);
                this.lstMySlides.Items.Remove(oldItem);
                i++;
            }


        }

        #endregion

        #region ToolStrip events

        #region tsbSlideLibrary

        //tool strip button click events
        private void tsbSlideLibraryAddToMySlides_Click(object sender, EventArgs e)
        {
            OnAddToMySlides();
        }

        private void tsbSlideLibrarySelectAll_Click(object sender, EventArgs e)
        {
            SlideLibrarySelectAll();
        }

        private void tsbSlideLibraryExportPpt_Click(object sender, EventArgs e)
        {
            ExportSlideLibraryToPpt();
        }

        private void tsbSlideLibraryBuilderMode_Click(object sender, EventArgs e)
        {
            CurrentViewMode = ViewMode.Build;
            ViewModeSetup();
        }

        private void tsbSlideLibraryBrowseMode_Click(object sender, EventArgs e)
        {
            CurrentViewMode = ViewMode.Browse;
            ViewModeSetup();
        }

        private void tsbSlideLibraryPreviewForward_Click(object sender, EventArgs e)
        {
            if (currentBrowsedSlideSet == null || currentBrowsedSlideSet == "")
            {
                return;
            }

            int currentListViewItemCount = 0;
            ListView currentListView = new ListView();
            if (currentBrowsedSlideSet == "Library")
            {
                currentListViewItemCount = lstPreview.Items.Count;
                currentListView = lstPreview;
            }
            else if (currentBrowsedSlideSet == "MySlides")
            {
                currentListViewItemCount = lstMySlides.Items.Count;
                currentListView = lstMySlides;
            }

            if (currentListView.Items.Count < 1)
            {
                return;
            }

            int currentSelectedListIndex;
            if (currentListViewItemCount > currentBrowsedSlide + 1) //there is a next
            {

                currentSelectedListIndex = currentListView.SelectedItems[0].Index;
                currentListView.Items[currentSelectedListIndex].Selected = false;
                currentListView.Items[currentSelectedListIndex + 1].Selected = true;
                currentSelectedListIndex = currentListView.SelectedItems[0].Index;

                //currentBrowsedSlide++;
            }
            else //go back to the beginning
            {

                currentListView.SelectedItems.Clear();
                currentListView.Items[0].Selected = true;
                currentBrowsedSlide = 0;
            }
            //LoadSlideToBrowser(currentSelectedListIndex);

        }

        private void tsbSlideLibraryPreviewBack_Click(object sender, EventArgs e)
        {
            if (currentBrowsedSlideSet == null || currentBrowsedSlideSet == "")
            {
                return;
            }

            int currentListViewItemCount = 0;
            ListView currentListView = new ListView();
            if (currentBrowsedSlideSet == "Library")
            {
                currentListViewItemCount = lstPreview.Items.Count;
                currentListView = lstPreview;
            }
            else if (currentBrowsedSlideSet == "MySlides")
            {
                currentListViewItemCount = lstMySlides.Items.Count;
                currentListView = lstMySlides;
            }

            if (currentListView.Items.Count < 1)
            {
                return;
            }

            int currentSelectedListIndex;
            if (currentBrowsedSlide - 1 >= 0) //there is a previous
            {
                currentSelectedListIndex = currentListView.SelectedItems[0].Index;
                currentListView.Items[currentSelectedListIndex].Selected = false;
                currentListView.Items[currentSelectedListIndex - 1].Selected = true;
                currentSelectedListIndex = currentListView.SelectedItems[0].Index;

                //currentBrowsedSlide--;
            }
            else //go back to the end
            {
                currentListView.SelectedItems.Clear();
                currentListView.Items[currentListViewItemCount - 1].Selected = true;
                currentBrowsedSlide = currentListViewItemCount - 1;
            }
            //LoadSlideToBrowser(currentBrowsedSlide);
        }

        private void tsbSlideLibraryPlay_Click(object sender, EventArgs e)
        {
            tsbSlideLibraryPlay.Enabled = false;
            this.Cursor = Cursors.WaitCursor;
            PlaySlideLibrarySlides();
            this.Cursor = Cursors.Default;
        }

        private void tsbSlideLibraryPrint_Click(object sender, EventArgs e)
        {
            PrintSlideLibrarySlides();
        }

        private void tsbSlideLibraryPreviewHome_Click(object sender, EventArgs e)
        {
            GoToStartPage();
        }

        #endregion

        #region tsbMain

        private void tsbMainBuildMode_Click(object sender, EventArgs e)
        {
            CurrentViewMode = ViewMode.Build;
            ViewModeSetup();
        }

        private void tsbMainBrowseMode_Click(object sender, EventArgs e)
        {
            CurrentViewMode = ViewMode.Browse;
            ViewModeSetup();
        }

        private void tsbMainHideNavigation_Click(object sender, EventArgs e)
        {
            splForm.Panel1Collapsed = true;
            tsbMainHideNavigation.Visible = false;
            tsbMainShowNavigation.Visible = true;
            //tsbSearchShow.Visible = true;
        }

        private void tsbMainShowNavigation_Click(object sender, EventArgs e)
        {
            splForm.Panel1Collapsed = false;
            //tsbSearchShow.Visible = false;
            tsbMainHideNavigation.Visible = true;
            tsbMainShowNavigation.Visible = false;
        }

        private void tsbMainHelp_Click(object sender, EventArgs e)
        {
            if (File.Exists(Path.Combine(Application.StartupPath, @"Resources\Pdf\Deck Manager Help.pdf")))
            {
                System.Diagnostics.Process.Start(Path.Combine(Application.StartupPath, @"Resources\Pdf\Deck Manager Help.pdf"));
            }
        }

        private void tsbMainUpdateApplication_Click(object sender, EventArgs e)
        {
            CheckForAppUpdate(false);
        }

        private void tsbMainUpdateData_Click(object sender, EventArgs e)
        {
            if (Components.AppUpdater.CheckDomainConnection())
            {
                if (!_ContentUpdater.IsWorkInProgress)
                {
                    if (Properties.Settings.Default.IndexInProgress)
                    {
                        DialogResult result = MessageBox.Show("This will stop your current index. Would like to continue?", "Continue?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (result == DialogResult.Yes)
                        {
                            Stop_and_Clear_Index();
                            navigationPanePageAdvancedSearch.Visible = false;
                            txtSearch.Visible = false;
                            btnSearchIndex.Visible = false;
                            btnSearchFAQ.Visible = false;
                            navigationPaneLeftPane.Refresh();
                            _ContentUpdater.CheckForLocalizedContentUpdateAsync(false);
                            
                        }
                    }else
                        _ContentUpdater.CheckForLocalizedContentUpdateAsync(false);
                    
                }
                else
                {
                    MessageBox.Show(String.Format("The {0} content updating system is busy. Please try again later.", Properties.Settings.Default.AppNameFull));
                }
            }
            else
            {
                MessageBox.Show("The online update resource could not be contacted.  Please check your Internet connection or try again later.",
                       "Online Update Connection Error",
                       MessageBoxButtons.OK,
                       MessageBoxIcon.Information);
            }
        }

        private void tsbMainHelpManual_Click(object sender, EventArgs e)
        {
            Help helper = new Help();
            helper.Owner = this;
            helper.Show();
        }

        private void tsbMainHelpAbout_Click(object sender, EventArgs e)
        {
            string version = ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString();
            MessageBox.Show(version, "Current Application Version", MessageBoxButtons.OK, MessageBoxIcon.None);
        }

        private void tsbMyRegionLanguages_Click(object sender, EventArgs e)
        {
            MyRegionLanguagesDialog myRegLangDialog = new MyRegionLanguagesDialog();
            myRegLangDialog.Owner = this;
            myRegLangDialog.ShowDialog();
        }

        private void registerContentIssuesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string to = Properties.Settings.Default.ContentIssuesEmail;
            string subject = String.Format("{0} Feedback", Properties.Settings.Default.AppNameFull);
            string body = "";
            System.Diagnostics.Process.Start(string.Format("mailto:{0}?Subject={1}&Body={2}", to, subject, body));
        }

        private void registerSoftwareIssuesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string to = Properties.Settings.Default.SoftwareIssuesEmail;
            string subject = String.Format("{0} Feedback", Properties.Settings.Default.AppNameFull);
            string body = "";
            System.Diagnostics.Process.Start(string.Format("mailto:{0}?Subject={1}&Body={2}", to, subject, body));
        }
        #endregion

        #region tsbMySlides

        private void tsbMySlidesExportPpt_Click(object sender, EventArgs e)
        {
            ExportMySlidesToPpt();
        }

        private void tsbMySlidesPrint_Click(object sender, EventArgs e)
        {
            PrintMySlides();
        }

        private void tsbMySlidesRemoveSlide_Click(object sender, EventArgs e)
        {
            if (lstMySlides.Items.Count == lstMySlides.SelectedItems.Count)
            {
                ClearMySlides();
            }
            else
            {
                RemoveMySlide();
            }
        }

        private void tsbMySlidesSave_Click(object sender, EventArgs e)
        {
            if (currentMySlides == -1)
            {
                ShowSaveDialog();
            }
            else
            {
                SavePptXml(currentMySlides, "");
            }
        }

        private void tsbMySlidesSaveAs_Click(object sender, EventArgs e)
        {
            ShowSaveDialog();
        }

        private void tsbMySlidesSelectAll_Click(object sender, EventArgs e)
        {
            MySlidesSelectAll();
        }

        private void tsbMySlidesPlay_Click(object sender, EventArgs e)
        {
            PlayMySlides();
        }

        private void tsbMySlidesClear_Click(object sender, EventArgs e)
        {
            ClearMySlides();
        }

        #endregion

        #endregion

        #region ContextMenu events

        #region miSlideLibraryPreview

        private void miSlideLibraryPreviewAddToMySlides_Click(object sender, EventArgs e)
        {
            OnAddToMySlides();
        }

        private void miSlideLibraryPreviewSelectAll_Click(object sender, EventArgs e)
        {
            SlideLibrarySelectAll();
        }

        #endregion

        #region miMySlides

        private void miMySlidesRemove_Click(object sender, EventArgs e)
        {
            if (lstMySlides.Items.Count == lstMySlides.SelectedItems.Count)
            {
                ClearMySlides();
            }
            else
            {
                RemoveMySlide();
            }
        }

        private void miMySlidesSave_Click(object sender, EventArgs e)
        {
            if (currentMySlides == -1)
            {
                ShowSaveDialog();
            }
            else
            {
                SavePptXml(currentMySlides, "");
            }
        }

        private void miMySlidesSaveAs_Click(object sender, EventArgs e)
        {
            ShowSaveDialog();
        }

        private void miMySlidesPlay_Click(object sender, EventArgs e)
        {
            PlayMySlides();
        }

        private void miMySlidesSelectAll_Click(object sender, EventArgs e)
        {
            MySlidesSelectAll();
        }

        private void miMySlidesClear_Click(object sender, EventArgs e)
        {
            ClearMySlides();
        }

        private void miMySlidesExportPpt_Click(object sender, EventArgs e)
        {
            ExportMySlidesToPpt();
        }

        private void miMySlidesPrint_Click(object sender, EventArgs e)
        {
            PrintMySlides();
        }

        #endregion

        private void miMyPresentationsDelete_Click(object sender, EventArgs e)
        {
            if (tvwMySlideDecks.SelectedNode.Tag.GetType() == typeof(Client.DataSets.MyPresentations.PresentationRow))
            {
                dsMyPresentations.Presentation.RemovePresentationRow((Client.DataSets.MyPresentations.PresentationRow)tvwMySlideDecks.SelectedNode.Tag);
                dsMyPresentations.WriteXml(AppSettings.LocalPaths.Default.XmlDirectory + "\\MyPresentations.xml");
                this.PopulateMySlideDecks();
                ResetMySlides();
                SetupButtons();
            }

        }

        private void cmnMyPresentations_Opened(object sender, EventArgs e)
        {
            tvwMySlideDecks.SelectedNode = gRightClickedNode;
            if (tvwMySlideDecks.SelectedNode != null)
            {
                if (tvwMySlideDecks.SelectedNode.Tag.GetType() != typeof(Client.DataSets.MyPresentations.PresentationRow))
                {
                    miMyPresentationsDelete.Enabled = false;
                }
                else
                {
                    miMyPresentationsDelete.Enabled = true;
                }
            }
        }

        #endregion

        #region My Slides helpers

        private void ExportMySlidesToPpt()
        {
            if (lstMySlides.Items.Count > 0)
            {
                System.Collections.ArrayList fileNames = GetMySlideFileNames();

                diaExportSave.InitialDirectory = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                DialogResult saveResult;
                saveResult = diaExportSave.ShowDialog();
                if (saveResult == DialogResult.OK)
                {
                    ToggleExportButtons(false);
                    exportPptBuilder.AsposeSlideLicensePath = Path.Combine(Application.StartupPath, @"lic\Aspose.Slides.lic");
                    exportPptBuilder.CreatePresentationAsync(fileNames, diaExportSave.FileName);
                    if (lstMySlides.SelectedItems.Count > 0)
                    {
                        LogEventForSlides(lstMySlides.SelectedItems, EventType.Exported);
                        this.SetLoggingEnabled(false);
                        ActivateMySlideListItem(lstMySlides.SelectedItems[0].Index);
                        this.SetLoggingEnabled(true);
                    }
                    else if (lstMySlides.Items.Count > 0)
                    {
                        LogEventForSlides(lstMySlides.Items, EventType.Exported);
                        this.SetLoggingEnabled(false);
                        ActivateMySlideListItem(0);
                        this.SetLoggingEnabled(true);
                    }
                }
            }
        }

        private void RemoveMySlide()
        {
            if (this.lstMySlides.SelectedItems.Count > 0)
            {
                int lowestSelectIndex = lstMySlides.SelectedIndices[0];
                foreach (ListViewItem lvItem in lstMySlides.SelectedItems)
                {
                    lstMySlides.Items.Remove(lvItem);
                }
                if (!mySlidesHasChanges)
                {
                    lblMySlides.Text = lblMySlides.Text + "*";
                }
                if (lstMySlides.Items.Count > 0)
                {
                    mySlidesHasChanges = true;
                }
                CountMySlides();
                SetupButtons();
                if (lstMySlides.Items.Count > 0)
                {
                    lstMySlides.Items[lowestSelectIndex].Selected = true;
                }
            }
        }

        private void PlayMySlides()
        {
            if (lstMySlides.Items.Count > 0)
            {
                TogglePlayButtons(false);
                System.Collections.ArrayList fileNames = GetMySlideFileNames();

                playPptBuilder.AsposeSlideLicensePath = Path.Combine(Application.StartupPath, @"lic\Aspose.Slides.lic");
                playPptBuilder.CreatePresentationAsync(fileNames, Path.Combine(AppSettings.LocalPaths.Default.SlideDirectory, "PlayShow.pps"));

                if (lstMySlides.SelectedItems.Count > 0)
                    LogEventForSlides(lstMySlides.SelectedItems, EventType.Played);
                else
                    LogEventForSlides(lstMySlides.Items, EventType.Played);
            }
        }

        public void SavePptXml(int presID, string presName)
        {
            DataSets.MyPresentations.PresentationRow presentation;
            if (presID == -1)
            {
                //it's a new presentation
                presentation = this.dsMyPresentations.Presentation.AddPresentationRow(presName);
            }
            else
            {
                //it's an existing presentation, get it and clear the slides.
                presentation = this.dsMyPresentations.Presentation.FindByPresentationID(presID);
                DataRow[] deleteSlides = this.dsMyPresentations.PresentationSlide.Select("PresentationID = " + presentation.PresentationID.ToString());
                foreach (DataSets.MyPresentations.PresentationSlideRow deleteSlide in deleteSlides)
                {
                    this.dsMyPresentations.PresentationSlide.Rows.Remove(deleteSlide);
                }
            }
            int i = 1;
            foreach (ListViewItem lvItem in lstMySlides.Items)
            {
                DataSets.MyPresentations.PresentationSlideRow presSlide;
                if (lvItem.Tag.GetType() == typeof(wsIpgSlideUpdater.SlideLibrary.SlideRow))
                {
                    wsIpgSlideUpdater.SlideLibrary.SlideRow slide = (wsIpgSlideUpdater.SlideLibrary.SlideRow)lvItem.Tag;
                    presSlide = this.dsMyPresentations.PresentationSlide.AddPresentationSlideRow(slide.SlideID,
                                                                                                        presentation, "Library",
                                                                                                        slide.Title,
                                                                                                        i, 
                                                                                                        GetCategoryID(slide));
                    LogEventForSlide(slide, EventType.Saved);
                }

                i++;
            }
            dsMyPresentations.WriteXml(AppSettings.LocalPaths.Default.XmlDirectory + "\\MyPresentations.xml");
            this.PopulateMySlideDecks();
            ResetMySlides();
            SelectMyPresentationNode(presentation.PresentationID);
        }

        private void ShowSaveDialog()
        {
            string presName = "";
            if (currentMySlides != -1)
            {
                DataSets.MyPresentations.PresentationRow presentation = dsMyPresentations.Presentation.FindByPresentationID(currentMySlides);
                presName = presentation.PresentationName;
            }
            SaveDialog saveForm = new SaveDialog(currentMySlides, presName);
            saveForm.Owner = this;
            saveForm.ShowDialog();
        }

        private void MySlidesSelectAll()
        {
            if (lstMySlides.Items.Count > 0)
            {
                lstMySlides.Focus();
                foreach (ListViewItem lvItem in lstMySlides.Items)
                {
                    lvItem.Selected = true;
                }
            }
        }

        private void CountMySlides()
        {
            this.statusMySlidesQuantity.Text = this.lstMySlides.Items.Count.ToString();
        }

        private void ClearMySlides()
        {
            if (mySlidesHasChanges)
            {
                VerifyClearMySlides();
            }
            else
            {
                ResetMySlides();
            }
            mySlidesHasChanges = false;
            SetupButtons();
        }

        private void VerifyClearMySlides()
        {
            DialogResult result = MessageBox.Show("Your My Slides have changed.  Would you like to save the changes?", "Save My Slides?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
            switch (result)
            {
                case DialogResult.Yes:
                    if (currentMySlides == -1)
                    {
                        ShowSaveDialog();
                    }
                    else
                    {
                        SavePptXml(currentMySlides, "");
                    }
                    ResetMySlides();
                    break;
                case DialogResult.No:
                    ResetMySlides();
                    break;
                default:
                    break;
            }
        }

        private void ResetMySlides()
        {
            lstMySlides.Items.Clear();
            mySlidesHasChanges = false;
            currentMySlides = -1;
            lblMySlides.Text = "My Slides";
            this.statusMySlidesQuantity.Text = "0";

        }

        private void PrintMySlides()
        {
            if (lstMySlides.Items.Count > 0)
            {
                TogglePrintButtons(false);
                System.Collections.ArrayList fileNames = GetMySlideFileNames();

                printPptBuilder.AsposeSlideLicensePath = Path.Combine(Application.StartupPath, @"lic\Aspose.Slides.lic");
                printPptBuilder.CreatePresentationAsync(fileNames, Path.Combine(AppSettings.LocalPaths.Default.SlideDirectory, "PrintPres.ppt"));
                if (lstMySlides.SelectedItems.Count > 0)
                {
                    ActivateMySlideListItem(lstMySlides.SelectedItems[0].Index);
                }
                else if (lstMySlides.Items.Count > 0)
                {
                    ActivateMySlideListItem(0);
                }
                LogEventForMySlides(EventType.Printed);
            }
        }

        private System.Collections.ArrayList GetMySlideFileNames()
        {
            System.Collections.ArrayList fileNames = new System.Collections.ArrayList();

            if (lstMySlides.SelectedItems.Count > 0) //only selected
            {
                foreach (ListViewItem lvItem in lstMySlides.SelectedItems)
                {
                    wsIpgSlideUpdater.SlideLibrary.SlideRow slide = (wsIpgSlideUpdater.SlideLibrary.SlideRow)lvItem.Tag;
                    fileNames.Add(Path.Combine(AppSettings.LocalPaths.Default.SlideDirectory, slide.FileName));
                }
            }
            else //all of them
            {
                foreach (ListViewItem lvItem in lstMySlides.Items)
                {
                    wsIpgSlideUpdater.SlideLibrary.SlideRow slide = (wsIpgSlideUpdater.SlideLibrary.SlideRow)lvItem.Tag;
                    fileNames.Add(Path.Combine(AppSettings.LocalPaths.Default.SlideDirectory, slide.FileName));
                }
            }
            return fileNames;
        }

        #endregion

        #region Slide Library Pane helpers

        private void OnAddToMySlides()
        {
            if (lstPreview.SelectedItems.Count > 0)
            {
                CurrentViewMode = ViewMode.Build;
                ViewModeSetup();

                if (this.draggedItems.Count > 0)
                {
                    this.draggedItems.Clear();
                }
                foreach (ListViewItem lvItem in lstPreview.SelectedItems)
                {
                    draggedItems.Add(lvItem);
                    LoadMySlidesThumb(lvItem.ImageKey);
                }

                if (!mySlidesHasChanges)
                {
                    lblMySlides.Text = lblMySlides.Text + "*";
                }
                mySlidesHasChanges = true;

                AddToMySlides();
                CountMySlides();
                SetupButtons();
            }
        }

        private void AddToMySlides()
        {
            foreach (ListViewItem oldItem in this.draggedItems)
            {
                ListViewItem newItem = (ListViewItem)oldItem.Clone();
                this.lstMySlides.Items.Add(newItem);
            }
        }

        private void SlideLibrarySelectAll()
        {
            if (lstPreview.Items.Count > 0)
            {
                lstPreview.Focus();
                foreach (ListViewItem lvItem in lstPreview.Items)
                {
                    lvItem.Selected = true;
                    lvItem.Focused = true;
                }
            }
        }

        private void PlaySlideLibrarySlides()
        {
            if (lstPreview.Items.Count > 0)
            {
                TogglePlayButtons(false);
                System.Collections.ArrayList fileNames = GetSlideLibraryPreviewFileNames();

                playPptBuilder.AsposeSlideLicensePath = Path.Combine(Application.StartupPath, @"lic\Aspose.Slides.lic");
                playPptBuilder.CreatePresentationAsync(fileNames, Path.Combine(AppSettings.LocalPaths.Default.SlideDirectory, "PlayShow.pps"));
                //ActivatePreviewListItem(lstPreview.SelectedItems[0].Index);
                if (lstPreview.SelectedItems.Count > 0)
                    LogEventForSlides(lstPreview.SelectedItems, EventType.Played);
                else
                    LogEventForSlides(lstPreview.Items, EventType.Played);
            }
        }

        public void DisableSlideLibraryButtons()
        {
            tsbSlideLibraryAddToMySlides.Enabled = false;
            tsbSlideLibrarySelectAll.Enabled = false;
            tsbSlideLibraryExportPpt.Enabled = false;
            tsbSlideLibraryPlay.Enabled = false;
            tsbSlideLibraryPrint.Enabled = false;
        }

        private void ExportSlideLibraryToPpt()
        {
            if (lstPreview.Items.Count > 0)
            {
                System.Collections.ArrayList fileNames = GetSlideLibraryPreviewFileNames();

                //Client.Components.PptHelper.ExportPresentationFromSlideFiles(fileNames, slideDirectory);
                diaExportSave.InitialDirectory = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                DialogResult saveResult;
                saveResult = diaExportSave.ShowDialog();
                if (saveResult == DialogResult.OK)
                {
                    if (lstPreview.SelectedItems.Count > 0)
                    {
                        LogEventForSlides(lstPreview.SelectedItems, EventType.Exported);
                        this.SetLoggingEnabled(false);
                        ActivatePreviewListItem(lstPreview.SelectedItems[0].Index);
                        this.SetLoggingEnabled(true);
                    }
                    else if (lstPreview.Items.Count > 0)
                    {
                        LogEventForSlides(lstPreview.Items, EventType.Exported);
                        this.SetLoggingEnabled(false);
                        ActivatePreviewListItem(0);
                        this.SetLoggingEnabled(true);
                    }
                    ToggleExportButtons(false);
                    exportPptBuilder.AsposeSlideLicensePath = Path.Combine(Application.StartupPath, @"lic\Aspose.Slides.lic");
                    exportPptBuilder.CreatePresentationAsync(fileNames, diaExportSave.FileName);
                }
            }
        }

        private void PrintSlideLibrarySlides()
        {
            if (lstPreview.Items.Count > 0)
            {
                TogglePrintButtons(false);
                System.Collections.ArrayList fileNames = GetSlideLibraryPreviewFileNames();
                printPptBuilder.AsposeSlideLicensePath = Path.Combine(Application.StartupPath, @"lic\Aspose.Slides.lic");
                printPptBuilder.CreatePresentationAsync(fileNames, Path.Combine(AppSettings.LocalPaths.Default.SlideDirectory, "PrintPres.ppt"));
                if (lstPreview.SelectedItems.Count > 0)
                    LogEventForSlides(lstPreview.SelectedItems, EventType.Printed);
                else
                    LogEventForSlides(lstPreview.Items, EventType.Printed);
            }
        }

        private System.Collections.ArrayList GetSlideLibraryPreviewFileNames()
        {
            System.Collections.ArrayList fileNames = new System.Collections.ArrayList();

            if (lstPreview.SelectedItems.Count > 0) //only selected
            {
                foreach (ListViewItem lvItem in lstPreview.SelectedItems)
                {
                    wsIpgSlideUpdater.SlideLibrary.SlideRow slide = (wsIpgSlideUpdater.SlideLibrary.SlideRow)lvItem.Tag;
                    fileNames.Add(Path.Combine(AppSettings.LocalPaths.Default.SlideDirectory, slide.FileName));
                }
            }
            else//do them all
            {
                foreach (ListViewItem lvItem in lstPreview.Items)
                {
                    wsIpgSlideUpdater.SlideLibrary.SlideRow slide = (wsIpgSlideUpdater.SlideLibrary.SlideRow)lvItem.Tag;
                    fileNames.Add(Path.Combine(AppSettings.LocalPaths.Default.SlideDirectory, slide.FileName));
                }
            }
            return fileNames;
        }

        public void PopulatePreviewSlidesList()
        {
            foreach (wsIpgSlideUpdater.SlideLibrary.SlideRow slide in this.currentChildSlides)
            {
                string imageFileName = slide.FileName.Replace(".pot", ".jpg");
                ListViewItem newItem = new ListViewItem(slide.Title, imageFileName);
                newItem.Tag = slide;

                this.lstPreview.Items.Add(newItem);
            }
            if (splPreviewType.Panel1Collapsed)
            {
                splPreviewType.Panel1Collapsed = false;
            }
        }

        private void ShowSlideRegions(wsIpgSlideUpdater.SlideLibrary.SlideRow slide)
        {

            StringBuilder sb = new StringBuilder();

            wsIpgSlideUpdater.SlideLibrary.SlideRegionLanguageRow[] slideRegLangs = dsSlideLibrary.Slide.FindBySlideID(slide.SlideID).GetSlideRegionLanguageRows();
            foreach (wsIpgSlideUpdater.SlideLibrary.SlideRegionLanguageRow slideRegLang in slideRegLangs)
            {
                sb.Append(dsSlideLibrary.Region.FindByRegionID(slideRegLang.RegionLanguageRow.RegionID).ShortName);
                sb.Append("-");
                sb.Append(dsSlideLibrary.Language.FindByLanguageID(slideRegLang.RegionLanguageRow.LanguageID).LongName);
                sb.Append(", ");
            }

            if (sb.Length > 0)
            {
                sb.Remove(sb.Length - 2, 2);
                tslRegionStatus.Text = "Slide Region/Language(s): " + sb.ToString();
                tslRegionStatus.Visible = true;
                tsRegionStatus.Visible = true;
            }

        }

        #endregion

        #region Play/Export/Print

        private void exportPptBuilder_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {

            }
            else if (e.Error != null)
            {

            }
            else
            {
                System.Diagnostics.Process.Start(exportPptBuilder.TargetFile);
                lblMainStatusMessage.Text = "Export Finished";
                pbMainStatusProgress.Value = 0;
            }
            pbMainStatusProgress.Visible = false;
            lblMainStatusMessage.Visible = false;
            ToggleExportButtons(true);
        }

        private void exportPptBuilder_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            lblMainStatusMessage.Visible = true;
            pbMainStatusProgress.Visible = true;

            lblMainStatusMessage.Text = "Slide Export: " + e.UserState.ToString();
            pbMainStatusProgress.Value = e.ProgressPercentage;
        }

        private void playPptBuilder_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {

            }
            else if (e.Error != null)
            {

            }
            else
            {
                System.Diagnostics.Process.Start(playPptBuilder.TargetFile);
                lblMainStatusMessage.Text = "Launching Show...";
                pbMainStatusProgress.Value = 0;
            }
            pbMainStatusProgress.Visible = false;
            lblMainStatusMessage.Visible = false;
            TogglePlayButtons(true);
        }

        private void playPptBuilder_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            lblMainStatusMessage.Visible = true;
            pbMainStatusProgress.Visible = true;

            lblMainStatusMessage.Text = "Building Show: " + e.UserState.ToString();
            pbMainStatusProgress.Value = e.ProgressPercentage;
        }

        private void printPptBuilder_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {

            }
            else if (e.Error != null)
            {

            }
            else
            {
                webPrinter.SendToBack();
                webPrinter.Visible = true;
                webPrinter.Navigate(printPptBuilder.TargetFile);

                lblMainStatusMessage.Text = "Printing presentation to default printer...";
                pbMainStatusProgress.Value = 100;
                System.Threading.Thread.Sleep(new TimeSpan(0, 0, 3));
                pbMainStatusProgress.Visible = false;
                lblMainStatusMessage.Visible = false;
                TogglePrintButtons(true);
            }

        }

        private void printPptBuilder_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            lblMainStatusMessage.Visible = true;
            pbMainStatusProgress.Visible = true;

            lblMainStatusMessage.Text = "Building Presentation: " + e.UserState.ToString();
            pbMainStatusProgress.Value = e.ProgressPercentage;
        }

        private void TogglePlayButtons(bool isEnabled)
        {
            tsbMySlidesPlay.Enabled = isEnabled;
            tsbSlideLibraryPlay.Enabled = isEnabled;
            miMySlidesPlay.Enabled = isEnabled;
        }

        private void ToggleExportButtons(bool isEnabled)
        {
            tsbMySlidesExportPpt.Enabled = isEnabled;
            tsbSlideLibraryExportPpt.Enabled = isEnabled;
            miMySlidesExportPpt.Enabled = isEnabled;
        }

        private void TogglePrintButtons(bool isEnabled)
        {
            tsbSlideLibraryPrint.Enabled = isEnabled;
            tsbMySlidesPrint.Enabled = isEnabled;
            miMySlidesPrint.Enabled = isEnabled;

        }

        private void webPrinter_Navigated(object sender, WebBrowserNavigatedEventArgs e)
        {
            webPrinter.ShowPrintDialog();
            webPrinter.Visible = false;
        }

        #endregion

        #region Browsing

        private void webSlidePreview_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            if (!e.Url.ToString().EndsWith(".pot"))
            {
                tslRegionStatus.Text = "";
            }
            switch (lastBrowseType)
            {
                case BrowsedDocType.Pot:
                    if (currentBrowsedSlideSet == "Library")
                    {
                        //browseOnSelection = false;
                        lstPreview.Focus();
                        if (lstPreview.SelectedItems.Count == 1)
                        {
                            lstPreview.Items[currentBrowsedSlide].Focused = true;
                        }
                    }
                    else if (currentBrowsedSlideSet == "MySlides")
                    {
                        //browseOnSelection = false;
                        lstMySlides.Focus();
                        if (lstMySlides.SelectedItems.Count == 1)
                        {
                            lstMySlides.Items[currentBrowsedSlide].Focused = true;
                        }
                    }
                    break;
                case BrowsedDocType.Http:
                    PlugIns.Helpers.ExternalWindowController.BringWindowToFront("iexplore", PlugIns.Helpers.ExternalWindowController.ProcessIndex.MostRecent);
                    break;
                case BrowsedDocType.Pdf:
                    PlugIns.Helpers.ExternalWindowController.BringWindowToFront("AcroRd32", PlugIns.Helpers.ExternalWindowController.ProcessIndex.MostRecent);
                    break;
                case BrowsedDocType.Popup:
                    break;
                default:
                    break;
            }
        }

        private void webSlidePreview_Navigating(object sender, WebBrowserNavigatingEventArgs e)
        {

            //if this happens, the file couldn't be found, let's see if we can get it
            if (e.Url.AbsolutePath == @"C:/WINDOWS/system32/shdoclc.dll/dnserror.htm")
            {
                e.Cancel = true;
                if (BrowseFileCheckExists(Path.Combine(AppSettings.LocalPaths.Default.SlideDirectory, Path.GetFileName(e.Url.ToString()))))
                {
                    webSlidePreview.Navigate(Path.Combine(AppSettings.LocalPaths.Default.SlideDirectory, Path.GetFileName(e.Url.ToString())));
                }
                return;
            }

            if (e.Url.AbsolutePath == "blank")
            {
                e.Cancel = true;
                return;
            }

            if (e.Url.ToString().Contains(Properties.Settings.Default.Domain))
            {
                picPptScrollHider.Visible = false;
                webSlidePreview.ScrollBarsEnabled = true;
                picPptScrollHider.SendToBack();
                return;
            }

            if (e.Url.ToString().EndsWith("dmpopup"))
            {
                e.Cancel = true;
                Popup popper = new Popup(e.Url.LocalPath, Cursor.Position);
                popper.Owner = this;
                popper.ShowDialog();
                lastBrowseType = BrowsedDocType.Popup;
            }
            else if (e.Url.ToString().StartsWith("http"))
            {
                lastBrowseType = BrowsedDocType.Http;
                e.Cancel = true;
                System.Diagnostics.Process.Start(e.Url.ToString());
            }
            else if (e.Url.ToString().EndsWith("pdf"))
            {
                lastBrowseType = BrowsedDocType.Pdf;
                e.Cancel = true;
                System.Diagnostics.Process.Start(e.Url.ToString());
            }
            else if (e.Url.ToString().EndsWith("pot"))
            {
                this.Refresh();
                picPptScrollHider.Visible = true;
                picPptScrollHider.BringToFront();
                webSlidePreview.ScrollBarsEnabled = false;
                lastBrowseType = BrowsedDocType.Pot;
                this.Refresh();
            }
            else
            {
                //do nothing special
            }
        }

        private void LoadSlideToBrowser(int slideListIndex)
        {
            if ((lstMySlides.Items.Count == 0 && currentBrowsedSlideSet == "MySlides") ||
                (lstPreview.Items.Count == 0 && currentBrowsedSlideSet == "Library") ||
                (lstPreview.Items.Count == 0 && lstMySlides.Items.Count == 0))
            {
                //the above conditions do not have slides to load
                tsRegionStatus.Visible = false;
                return;
            }
            else
            {
                wsIpgSlideUpdater.SlideLibrary.SlideRow slide = null;
                if (currentBrowsedSlideSet == "Library")
                {
                    if (currentChildSlides.Count > 0)
                    {
                        slide = (wsIpgSlideUpdater.SlideLibrary.SlideRow)this.currentChildSlides[slideListIndex];
                    }
                }
                else //if (currentBrowsedSlideSet == "MySlides")
                {
                    if (lstMySlides.Items.Count > 0)
                    {
                        slide = (wsIpgSlideUpdater.SlideLibrary.SlideRow)lstMySlides.Items[slideListIndex].Tag;
                    }
                }

                string slideFile = Path.Combine(AppSettings.LocalPaths.Default.SlideDirectory, slide.FileName);

                if (BrowseFileCheckExists(slideFile))
                {
                    webSlidePreview.Navigate(slideFile);
                    picPptScrollHider.Visible = true;
                    if (Properties.Settings.Default.IsContentLocalizationSupported)
                    {
                        ShowSlideRegions(slide);
                    }
                    LogEventForSlide(slide, EventType.Viewed);
                }
            }
        }

        public bool BrowseFileCheckExists(string browseToFile)
        {
            bool result = true;
            //if the slide file doesn't exist, download it
            if (!File.Exists(browseToFile))
            {
                if (Components.AppUpdater.CheckDomainConnection())
                {
                    //System.Net.WebClient wc = new System.Net.WebClient();
                    WebHelpers.WebClientHelper wc = new Client.WebHelpers.WebClientHelper();
                    string fileUri = Properties.Settings.Default.ServerSlideBaseURI + Path.GetFileName(browseToFile);
                    //download the missing file
                    try
                    {
                        if (_NetConnected)
                        {
                            wc.DownloadFile(fileUri, browseToFile);
                        }
                        else
                        {
                            //If no internet connection - Show error page
                            webSlidePreview.Navigate(Path.Combine(AppSettings.LocalPaths.Default.HtmlDirectory, "NoInternet.htm"));
                            result = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        //if something bad happens, show the error page
                        webSlidePreview.Navigate(Path.Combine(AppSettings.LocalPaths.Default.HtmlDirectory, "MissingFile.htm"));
                        result = false;
                    }
                }
                else
                {//If domain is not available then show custom error in window
                    //If no internet connection - Show error page
                    webSlidePreview.Navigate(Path.Combine(AppSettings.LocalPaths.Default.HtmlDirectory, "NoInternet.htm"));
                    result = false;
                }
            }
            return result;
        }


        private void DownloadWelcomePageAssets()
        {
            string clientLocation = Path.Combine(AppSettings.LocalPaths.Default.HtmlDirectory, "Assets");
            Client.wsAdmin.Admin adminService = new Client.wsAdmin.Admin();
            DataTable fileList = adminService.GetAnnouncementAssetFileList();
            foreach (DataRow tableRow in fileList.Rows)
            {
                string fileName = tableRow["FileName"].ToString();
                string webLocation = Properties.Settings.Default.StartPageAssetFolder + fileName;
                DownloadFile(webLocation, clientLocation + @"\" + fileName);
            }
        }

        private void DownloadFile(string webLocation, string clientLocation)
        {
            System.Net.WebClient webClient = new System.Net.WebClient();
            webClient.DownloadFile(webLocation, clientLocation);
        }

        private void SetClientStartPagePreference()
        {
            string myRegionLanguagesFile = Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, AppSettings.LocalFiles.Default.MyRegionLangauges);
            if (File.Exists(myRegionLanguagesFile))
            {
                DataSets.MyRegionLanguages dsMyRegLangs = new Client.DataSets.MyRegionLanguages();
                dsMyRegLangs.ReadXml(myRegionLanguagesFile);
                List<int> regionLanguageIds = new List<int>();
                foreach (DataSets.MyRegionLanguages.MyRegionLanguagesRow row in dsMyRegLangs._MyRegionLanguages.Rows)
                {
                    regionLanguageIds.Add(row.RegionLanguageID);
                }
                if (regionLanguageIds.Count > 0)
                {
                    Client.BLL.RegionLanaguageAccessor accessor = new Client.BLL.RegionLanaguageAccessor();
                    int regionId = accessor.GetRegionIdFromMyRegionLanaguages(regionLanguageIds[0]);
                    if (regionId != 0)
                    {
                        Properties.Settings.Default.ClientStartPageRegionPreference = regionId;
                        Properties.Settings.Default.ClientStartPagePreferenceSet = true;
                        Properties.Settings.Default.Save();
                    }
                }
            }
        }

        /// <summary>
        /// Goes to start page.
        /// </summary>
        private void GoToStartPage()
        {
            bool connected = Components.AppUpdater.CheckDomainConnection();
            //go to the announcement page if we are connected and this flavor supports announcements
            if (connected && Properties.Settings.Default.IsAnnouncementsSupported)
            {
                try
                {
                    wsIpgSlideUpdater.SlideUpdater ws = new Client.wsIpgSlideUpdater.SlideUpdater();
                    //only show announcments page if they are available
                    if (ws.IsAnnouncementsAvailable())
                    {
                        if (!Properties.Settings.Default.ClientStartPagePreferenceSet)
                            SetClientStartPagePreference();
                        DownloadWelcomePageAssets();
                        int regionId = Properties.Settings.Default.ClientStartPageRegionPreference;
                        string startPageUrl = Properties.Settings.Default.StartPage + "?regionId=" + regionId;
                        System.Net.WebClient webClient = new System.Net.WebClient();
                        webClient.DownloadFile(startPageUrl, Path.Combine(AppSettings.LocalPaths.Default.HtmlDirectory, AppSettings.LocalFiles.Default.Announcements));
                        webSlidePreview.Navigate(Path.Combine(AppSettings.LocalPaths.Default.HtmlDirectory, AppSettings.LocalFiles.Default.Announcements));
                    }
                    else
                    {
                        webSlidePreview.Navigate(Path.Combine(AppSettings.LocalPaths.Default.HtmlDirectory, AppSettings.LocalFiles.Default.WelcomePage));
                    }
                }
                catch (Exception ex)
                {
                    webSlidePreview.Navigate(Path.Combine(AppSettings.LocalPaths.Default.HtmlDirectory, AppSettings.LocalFiles.Default.WelcomePage));
                }
            }
            else
            {
                webSlidePreview.Navigate(Path.Combine(AppSettings.LocalPaths.Default.HtmlDirectory, AppSettings.LocalFiles.Default.WelcomePage));
            }
        }

        #endregion

        #region MyPresentations

        private void SelectMyPresentationNode(int presentationID)
        {
            foreach (TreeNode node in tvwMySlideDecks.Nodes[0].Nodes)
            {
                int currentNodePresID = ((DataSets.MyPresentations.PresentationRow)node.Tag).PresentationID;
                if (presentationID == currentNodePresID)
                {
                    tvwMySlideDecks.SelectedNode = node;
                    tvwMySlideDecks.Nodes[0].Nodes[node.Index].Expand();
                    return;
                }
            }

        }

        private void tvwMySlideDecks_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (e.Node.Tag.GetType().ToString() == "Client.DataSets.MyPresentations+PresentationRow")
            {
                if (mySlidesHasChanges)
                {
                    DialogResult result = MessageBox.Show("Your My Slides have changed.  Would you like to save the changes?", "Save My Slides?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
                    switch (result)
                    {
                        case DialogResult.Yes:
                            ShowSaveDialog();
                            ShowSavedPresentationSlides(e.Node);
                            break;
                        case DialogResult.No:
                            ShowSavedPresentationSlides(e.Node);
                            break;
                        default:
                            break;
                    }
                }
                else
                {
                    CurrentViewMode = ViewMode.Build;
                    ViewModeSetup();
                    ShowSavedPresentationSlides(e.Node);
                }
            }
            CountMySlides();
            mySlidesHasChanges = false;
            SetupButtons();
            gRightClickedNode = e.Node;
        }

        private void ShowSavedPresentationSlides(TreeNode node)
        {
            // TODO: figure out how to get a CategoryID here
            lblMySlides.Text = "My Slides - " + ((DataSets.MyPresentations.PresentationRow)node.Tag).PresentationName;
            currentMySlides = ((DataSets.MyPresentations.PresentationRow)node.Tag).PresentationID;
            if (lstMySlides.Items.Count > 0)
            {
                lstMySlides.Items.Clear();
            }
            DataRow[] presSlides = dsMyPresentations.PresentationSlide.Select("PresentationID=" + currentMySlides.ToString());

            if (presSlides != null)
            {
                foreach (DataSets.MyPresentations.PresentationSlideRow presSlide in presSlides)
                {
                    //DataSets.MyPresentations.PresentationSlideRow presSlide = (DataSets.MyPresentations.PresentationSlideRow)slideNode.Tag;
                    wsIpgSlideUpdater.SlideLibrary.SlideRow slide = dsSlideLibrary.Slide.FindBySlideID(presSlide.SlideID);
                    if (slide != null)
                    {
                        slide.CategoryID = this.GetCategoryID(presSlide);
                        string imageFileName = slide.FileName.Replace("pot", "jpg");
                        LoadMySlidesThumb(imageFileName);
                        ListViewItem newItem = new ListViewItem(slide.Title, imageFileName);
                        newItem.Tag = slide;
                        lstMySlides.Items.Add(newItem);
                    }
                }
            }
        }

        private void tvwMySlideDecks_AfterExpand(object sender, TreeViewEventArgs e)
        {
            tvwMySlideDecks.SelectedNode = e.Node;
        }

        #endregion

        #region ListView events

        private void lstPreview_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            Object tag = e.Item.Tag;
            if (lstPreview.SelectedItems.Count == 1)
            {
                currentBrowsedSlideSet = "Library";
                currentBrowsedSlide = lstPreview.SelectedItems[0].Index;
                LoadSlideToBrowser(currentBrowsedSlide);
            }
        }

        private void lstMySlides_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            if (lstMySlides.SelectedItems.Count == 1)
            {
                currentBrowsedSlideSet = "MySlides";
                currentBrowsedSlide = lstMySlides.SelectedItems[0].Index;
                LoadSlideToBrowser(currentBrowsedSlide);

            }
        }

        private void lstMySlides_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                if (lstMySlides.Items.Count == lstMySlides.SelectedItems.Count)
                {
                    ClearMySlides();
                }
                else
                {
                    RemoveMySlide();
                }
            }
        }

        #endregion

        #region Misc control events

        private void splForm_SplitterMoved(object sender, SplitterEventArgs e)
        {
            //the form looks weird if you don't do this
            splForm.Refresh();
        }

        private void tabRegionLanguageTrees_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if there aren't any tabs forget about it
            if (tabRegionLanguageTrees.TabPages.Count < 1)
            {
                return;
            }
            //get the treeview that is on the selected tab
            BLL.CategorySlideTreeView currentTabTree = null;
            foreach (Control cont in tabRegionLanguageTrees.SelectedTab.Controls)
            {
                if (cont.GetType() == typeof(BLL.CategorySlideTreeView))
                {
                    currentTabTree = (BLL.CategorySlideTreeView)cont;
                    break;
                }
            }

            //it may seem weird to set the SelectedNode to null and then set it equal to the node that was already selected
            //but, what we want is for the AfterSelect event of the treeview to fire after the user switches tabs.
            if (currentTabTree != null)
            {
                if (currentTabTree.SelectedNode != null)
                {
                    TreeNode node = currentTabTree.SelectedNode;
                    currentTabTree.SelectedNode = null;
                    currentTabTree.SelectedNode = node;

                }

            }
        }

        #endregion

        #region Misc

        private static void StartPowerPoint()
        {
            List<Process> processes = new List<Process>(Process.GetProcessesByName("POWERPNT"));
            if (processes.Count < 1)
            {
                try
                {
                    Process p = new System.Diagnostics.Process();
                    p.StartInfo.FileName = "POWERPNT";
                    p.StartInfo.Arguments = @"/splash";
                    p.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                    p.Start();
                }
                catch (Win32Exception)
                {
                    MessageBox.Show(
                        "Microsoft PowerPoint does not appear to be installed on your computer.\r\n\r\n" +
                        "Please install Microsoft PowerPoint and restart The Printing Sales Guide.",
                        "Microsoft PowerPoint Required",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private void LogIssue(Exception ex, string customMessage)
        {
            try
            {
                TimeZone timeZone = TimeZone.CurrentTimeZone;
                _IssueLoggerService.LogIssueAsync(Client.wsIssueLogger.Applications.HpPrintingSalesGuide, Components.CommonTasksHelper.GetIps(), timeZone.StandardName, ex.Message, ex.StackTrace, customMessage);

            }
            catch (Exception x)
            {
                //do nothing
            }
        }

        #endregion

        #region Application Update

        private void clkAppUpdate_Tick(object sender, EventArgs e)
        {
            CheckForAppUpdate(true);
        }

        private void CheckForAppUpdate(bool isSilent)
        {
            
            //First need to check to see if there is an content update in progress, if so let's hold up on the update.
            if (IsFormAlreadyOpen(typeof(ContentUpdateForm)) == null)
            {
                                
                switch (Components.AppUpdater.CheckForAppUpdate())
                {
                    case Client.Components.UpdateAvailable.Yes:
                        //set the IsAppUpdate to true so that the content update will not fire.
                        Client.Properties.Settings.Default.IsAppUpdate = true;
                        lblMainStatusMessage.Text = "An application update has been detected and is currently downloading.";
                        lblMainStatusMessage.Visible = true;
                        appUpdate = ApplicationDeployment.CurrentDeployment;
                        appUpdate.UpdateCompleted += new AsyncCompletedEventHandler(appUpdate_UpdateCompleted);
                        appUpdate.UpdateAsync();
                        break;

                    case Client.Components.UpdateAvailable.No:
                        if (!isSilent)
                        {
                            MessageBox.Show("There are currently no updates available for download.",
                                            "No Update Available",
                                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        NoAppUpdate(this, new EventArgs());
                        break;

                    case Client.Components.UpdateAvailable.OfflineOrError:
                        if (!isSilent)
                        {
                            MessageBox.Show("The online update resource could not be contacted.  Please check your Internet connection or try again later.",
                                                   "Online Update Connection Error",
                                                   MessageBoxButtons.OK,
                                                   MessageBoxIcon.Information);
                        }
                        NoAppUpdate(this, new EventArgs());
                        break;

                    default:
                        break;
                }
            }
        }

        private void appUpdate_UpdateCompleted(object sender, AsyncCompletedEventArgs e)
        {
            lblMainStatusMessage.Text = "";
            lblMainStatusMessage.Visible = false;
            if (e.Cancelled)
            {
                MessageBox.Show("The update of the application's latest version was cancelled.");
                return;
            }
            else if (e.Error != null)
            {
                MessageBox.Show("ERROR: Could not install the latest version of the application. Reason: \n" + e.Error.Message + "\nPlease report this error to the system administrator.");
                return;
            }
                        
            ApplicationUpdateComplete appUpdateComplete = new ApplicationUpdateComplete();
            appUpdateComplete.Owner = this;
            appUpdateComplete.ShowDialog();
        }

        private void toolStripStatusLabelResume_Click(object sender, EventArgs e)
        {
            if (!_ContentUpdater.IsWorkInProgress)
            {
                _ContentUpdater.CheckForLocalizedContentUpdateAsync(true);
            }
            else
            {
                MessageBox.Show(String.Format("The {0} content updating system is busy. Please try again later.", Properties.Settings.Default.AppNameFull));
            }
        }

        #endregion

        #region Content Update

        /// <summary>
        /// _s the content updater_ check for update completed.
        /// </summary>
        /// <param name="result">The result.</param>
        /// <param name="isSilentlyChecked">if set to <c>true</c> [is silently checked].</param>
        private void _ContentUpdater_CheckForUpdateCompleted(ContentUpdater.ContentUpdateCheckResult result, bool isSilentlyChecked)
        {
            //MessageBox.Show("Work in Progress: " + _ContentUpdater.IsWorkInProgress.ToString() + " - First Run: " + isFirstRun + " - First Content: " + Client.Properties.Settings.Default.FirstContent.ToString());

            //Check to see if there is an app update before completing this. If so let the user know.
            if (Client.Properties.Settings.Default.IsAppUpdate)
            {

            }
            else
            {
                //if we checked silently and there is an update available, go get it
                if (isSilentlyChecked)
                {

                    switch (result)
                    {
                        case ContentUpdater.ContentUpdateCheckResult.UpdateAvailable:
                            if (!_ContentUpdater.IsWorkInProgress)
                            {
                                ContentUpdateForm contentUpdateForm;

                                if (isFirstRun)
                                {
                                    contentUpdateForm = new ContentUpdateForm(ContentUpdater.UpdateType.LocalizedRegular, isFirstRun);
                                    tmr.Start();
                                }
                                else if (Client.Properties.Settings.Default.FirstContent)
                                {
                                    contentUpdateForm = new ContentUpdateForm(ContentUpdater.UpdateType.LocalizedFull);
                                    tmr.Start();
                                }
                                else
                                {
                                    contentUpdateForm = new ContentUpdateForm(ContentUpdater.UpdateType.LocalizedRegular);
                                    tmr2.Start();
                                }

                                if (Client.Properties.Settings.Default.FirstContent)
                                {
                                    Client.Properties.Settings.Default.FirstContent = false;
                                    Client.Properties.Settings.Default.Save();
                                }
                                contentUpdateForm.Owner = this;
                                contentUpdateForm.Show();// .ShowDialog();
                            }
                            else
                            {
                                MessageBox.Show(String.Format("The {0} content updating system is busy. Please try again later.", Properties.Settings.Default.AppNameFull));
                            }

                            break;
                        case ContentUpdater.ContentUpdateCheckResult.ResumeUpdate:
                            if (!_ContentUpdater.IsWorkInProgress)
                            {
                                ContentUpdateForm contentUpdateForm;
                                contentUpdateForm = new ContentUpdateForm(ContentUpdater.UpdateType.LocalizedPartial);
                                contentUpdateForm.Owner = this;
                                contentUpdateForm.Show(); // ShowDialog();
                                tmr2.Start();
                            }
                            break;
                        default:
                            //do nothing
                            break;
                    }
                }
                //otherwise, let's give the user some feedback/options depending on the result of the check
                else
                {
                    switch (result)
                    {
                        case ContentUpdater.ContentUpdateCheckResult.UpdateAvailable:
                            if (!_ContentUpdater.IsWorkInProgress)
                            {
                                ContentUpdateForm contentUpdateForm;
                                contentUpdateForm = new ContentUpdateForm(ContentUpdater.UpdateType.LocalizedRegular);
                                contentUpdateForm.Owner = this;
                                contentUpdateForm.Show(); // ShowDialog();
                                tmr2.Start();
                            }
                            else
                            {
                                MessageBox.Show(String.Format("The {0} content updating system is busy. Please try again later.", Properties.Settings.Default.AppNameFull));
                            }

                            break;
                        case ContentUpdater.ContentUpdateCheckResult.ContentIsCurrent:
                            DialogResult messageBoxResult;
                            messageBoxResult = MessageBox.Show("You have the most current version of the content.  If you believe you may be missing content you may choose to download all of the available content.  Would you like to download all of the available content?", "Content Is Current", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                            if (messageBoxResult == DialogResult.Yes)
                            {
                                if (!_ContentUpdater.IsWorkInProgress)
                                {
                                    ContentUpdateForm contentUpdateForm2;
                                    contentUpdateForm2 = new ContentUpdateForm(ContentUpdater.UpdateType.LocalizedFull);
                                    contentUpdateForm2.Owner = this;
                                    contentUpdateForm2.Show(); // ShowDialog();
                                    tmr.Start();
                                }
                                else
                                {
                                    MessageBox.Show(String.Format("The {0} content updating system is busy. Please try again later.", Properties.Settings.Default.AppNameFull));
                                }
                            }
                            break;
                        case ContentUpdater.ContentUpdateCheckResult.ResumeUpdate:
                            if (!_ContentUpdater.IsWorkInProgress)
                            {
                                ContentUpdateForm contentUpdateForm;
                                contentUpdateForm = new ContentUpdateForm(ContentUpdater.UpdateType.LocalizedPartial);
                                contentUpdateForm.Owner = this;
                                contentUpdateForm.Show(); // ShowDialog();
                                //contentUpdateForm._ContentUpdater.ContentUpdateCompleted += new ContentUpdater.ContentUpdateCompletedDelegate(contentUpdater_ContentUpdateCompleted);
                                tmr2.Start();
                            }
                            else
                            {
                                MessageBox.Show(String.Format("The {0} content updating system is busy. Please try again later.", Properties.Settings.Default.AppNameFull));
                            }

                            break;
                        case ContentUpdater.ContentUpdateCheckResult.Error:
                            MessageBox.Show("The application was unable to determine if a content update is available. There may be a problem connecting to the server. Please try again later.", "Error Checking for Update", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            tmr2.Stop();
                            break;
                        case ContentUpdater.ContentUpdateCheckResult.Cancelled:
                            MessageBox.Show("The application was unable to determine if a content update is available. There may be a problem connecting to the server. Please try again later.", "Error Checking for Update", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            tmr2.Stop();
                            break;
                        default:
                            MessageBox.Show("The application was unable to determine if a content update is available. There may be a problem connecting to the server. Please try again later.", "Error Checking for Update", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            tmr2.Stop();
                            break;
                    }
                }
           }
        }

        private void contentUpdater_ContentUpdateCompleted()
        {
            MessageBox.Show("just hit the content update completed");
        }

        public void UpdateIPGContentProgress(int progressPercent)
        {//*SS allows the contentUpdateForm to update the IPG Content Progress Bar
            if (pbContentUpdate.Visible == false)
                pbContentUpdate.Visible = true;
            if (lblMainStatusContentUpdate.Visible == false)
                lblMainStatusContentUpdate.Visible = true;
            pbContentUpdate.Value = progressPercent;
        }

        public void UpdateIPGContentProgressComplete()
        {//*SS allows the contentUpdateForm to update the IPG Content Progress Bar
            if (pbContentUpdate.Visible == true)
                pbContentUpdate.Visible = false;
            if (lblMainStatusContentUpdate.Visible == true)
                lblMainStatusContentUpdate.Visible = false;
            pbContentUpdate.Value = 0;

           
        }

        public void UpdateIndexProgress(int progressPercent)
        {//*SS allows the contentUpdateForm to update the Index Progress Bar
            if (pbIndex.Visible == false)
                pbIndex.Visible = true;
            if (lblIndex.Visible == false)
                lblIndex.Visible = true;
            if (btnSearchIndex.Visible == true)
                btnSearchIndex.Visible = false;
            if (txtSearch.Visible == true)
                txtSearch.Visible = false;
            if (btnSearchFAQ.Visible == false)
                btnSearchFAQ.Visible = true;
            if (navigationPanePageAdvancedSearch.Visible == true)
            {
                navigationPanePageAdvancedSearch.Visible = false;
                navigationPaneLeftPane.Refresh();
            }
            if(!splViewerBottom.Panel2Collapsed)
                splViewerBottom.Panel2Collapsed = true;
            pbIndex.Value = progressPercent;
        }

        public void UpdateIndexProgressComplete()
        {//*SS allows the contentUpdateForm to update the Index Progress Bar
            if (pbIndex.Visible == true)
                pbIndex.Visible = false;
            if (lblIndex.Visible == true)
                lblIndex.Visible = false;
            if (btnSearchIndex.Visible == false)
                btnSearchIndex.Visible = true;
            if (txtSearch.Visible == false)
                txtSearch.Visible = true;
            if (btnSearchFAQ.Visible == false)
                btnSearchFAQ.Visible = true;
            if (navigationPanePageAdvancedSearch.Visible == false)
            {
                navigationPanePageAdvancedSearch.Visible = true;
                navigationPaneLeftPane.Refresh();
            }
            pbIndex.Value = 0;
            _fullIndex = false;
            _UpdateIndex = false;
            _index_in_progress = false;            
        }

        private void Content_Update_toBeIndexed(string file)
        {
            _toBeIndexed.Add(file);
        }

        private void Content_Update_removeIndex(string file)
        {
            _removeIndex.Add(file);
        }

        private void _ContentUpdater_ContentUpdateProgressReported(string message, int progressPercent)
        {
            lblMainStatusContentUpdate.Text = message;
            pbContentUpdate.Value = progressPercent;
            if ( pbContentUpdate.Visible == false)
                pbContentUpdate.Visible = true;
        }

        private void _ContentUpdater_ContentUpdateCompleted()
        {
            

            lblMainStatusContentUpdate.Text = "";
            lblMainStatusContentUpdate.Visible = false;
            pbContentUpdate.Value = 0;
            pbContentUpdate.Visible = false;
            this.Refresh();
            PopulateLocalizedTreeviews();//((IpgMain)this.Owner).            
            _index_in_progress = false;
            GoToStartPage();

            //report that a content update has occurred
            wsIpgSlideUpdater.SlideUpdater wsSlideUpdater = new Client.wsIpgSlideUpdater.SlideUpdater();
            try
            {
                wsSlideUpdater.SendClientDataUpdateInfoAsync(Properties.Settings.Default.LastLocalizedContentID,
                    ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString(),
                    Environment.OSVersion.ToString());
            }
            catch (Exception ex)
            {
                //don't worry about it, the user doesn't care
            }
        }

        #endregion

        

        #region Enums

        public enum BrowsedSlideSet
        {
            Library,
            MySlides
        }
        private enum BrowsedDocType
        {
            Pot,
            Http,
            Pdf,
            Popup
        }
        public enum ViewMode
        {
            Browse,
            Build
        }

        #endregion

        private void tsbShowAnnouncements_Click(object sender, EventArgs e)
        {
            ShowAnnouncements();
        }

        private void ShowAnnouncements()
        {
            splViewers.Panel1Collapsed = true;
            splViewers.Panel2Collapsed = true;
            splPreviewType.Panel1Collapsed = true;
            splPreviewType.Panel2Collapsed = false;
            picPptScrollHider.Visible = false;
            webSlidePreview.ScrollBarsEnabled = true;
            GoToStartPage();
        }

        private void supportFAQToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FAQ faqHelper = new FAQ();
            faqHelper.Owner = this;
            faqHelper.Show();
        }

        private void profileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Components.AppUpdater.CheckDomainConnection())
            {
                Profile profileForm = new Profile();
                profileForm.ShowDialog();
            }
            else
            {
                MessageBox.Show("Your profile was not able to be loaded.  Please check your internet connectivity.", "Your Profile");
            }
        }

        public void PauseLoggingWorkers()
        {
            this.serviceLoggingWorker.Pause();
            this.usageLoggingWorker.Pause();
        }

        public void ResumeLoggingWorkers()
        {
            this.serviceLoggingWorker.Resume();
            this.usageLoggingWorker.Resume();
        }

        public void SetLoggingEnabled(bool enabled)
        {
            _logEvent = enabled;
        }

        public bool IsLoggingEnabled()
        {
            return _logEvent;
        }

        /// <summary>
        /// Logs an event relating to all slides in "My Slides"
        /// </summary>
        /// <param name="eventName">The name of the event</param>
        private void LogEventForMySlides(EventType eventTypeId)
        {
            LogEventForSlides(lstMySlides.Items, eventTypeId);
        }

        private void LogEventForSlide(wsIpgSlideUpdater.SlideLibrary.SlideRow slide, EventType eventTypeId)
        {
            if (this.IsLoggingEnabled())
            {
                InitUserEvents();
                DataRow row = eventsCache.Events.NewRow();
                row["EventID"] = this.eventId;
                row["SlideID"] = slide.SlideID;
                row["CategoryID"] = GetCategoryID(slide);
                row["Session"] = this.sessionId;
                row["TypeID"] = (int)eventTypeId;
                row["Tag"] = slide.ToString();
                row["EventDate"] = DateTime.Now;
                eventsCache.Events.Rows.Add(row);
            }
        }

        private void LogEventForSlides(IList list, EventType type)
        {
            foreach (Object o in list)
            {
                if (o is ListViewItem)
                {
                    ListViewItem item = (ListViewItem)o;
                    if (item.Tag is wsIpgSlideUpdater.SlideLibrary.SlideRow)
                    {
                        LogEventForSlide((wsIpgSlideUpdater.SlideLibrary.SlideRow)item.Tag, type);
                    }
                }
            }
        }

        private int GetCategoryID(wsIpgSlideUpdater.SlideLibrary.SlideRow slide)
        {
            try
            {
                return slide.CategoryID;
            }
            catch (Exception)
            {
                return -2;
            }
        }

        private int GetCategoryID(DataSets.MyPresentations.PresentationSlideRow row)
        {
            try
            {
                return row.CategoryID;
            }
            catch (Exception)
            {
                return -2;
            }
        }

        private void FinalizeSession()
        {
            lock (_logSync)
            {
                string path = Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, AppSettings.LocalFiles.Default.UserEventsData);
                UserEventsDS ue = new UserEventsDS();
                ue.ReadXml(path);
                DataRow[] sessionRows = ue.Sessions.Select("Session='" + this.sessionId.ToString() + "'");
                if (sessionRows.Length > 0)
                {
                    sessionRows[0]["EndDate"] = DateTime.Now;
                    ue.Sessions.AcceptChanges();
                    ue.WriteXml(path);
                }
            }
        }

        private void InitUserEvents()
        {
            if (eventsCache == null)
            {
                string path = Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, AppSettings.LocalFiles.Default.UserEventsData);
                eventsCache = new UserEventsDS();
                eventsCache.Events.TableNewRow += new DataTableNewRowEventHandler(Events_TableNewRow);
            }
        }

        private void Events_TableNewRow(object sender, DataTableNewRowEventArgs e)
        {
            this.eventId = Convert.ToInt64(e.Row["EventID"]);
        }

        private bool HasEventsCache()
        {
            if (eventsCache != null)
            {
                return eventsCache.Events.Count > 0;
            }
            return false;
        }

        private void usageLoggingWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            string path = Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, AppSettings.LocalFiles.Default.UserEventsData);
            if (!HasEventsCache()) return;// nothing to log, so bail.
            lock (_logSync)
            {
                UserEventsDS target = new UserEventsDS();
                if (File.Exists(path))
                    target.ReadXml(path);
                long id = 0;
                target.Events.TableNewRow += new DataTableNewRowEventHandler(
                    delegate(object s, DataTableNewRowEventArgs args)
                    {
                        id = Convert.ToInt64(args.Row["EventID"]);
                    }
                );
                // copy cached events to local persistent store.
                foreach (DataRow dr in eventsCache.Events.Rows)
                {
                    DataRow newRow = target.Events.NewRow();
                    newRow.ItemArray = (object[])dr.ItemArray.Clone();
                    newRow["EventID"] = id;
                    target.Events.Rows.Add(newRow);
                }
                target.WriteXml(path);
                eventsCache.Events.Clear();
            }
        }

        private void serviceLoggingWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            if (!_flushToService) return;
            UsageLoggingService service = new UsageLoggingService();
            if (this.IsServiceAvailable(service))
            {
                // Flush in-memory cached data and block until it's finished
                if (!usageLoggingWorker.IsBusy) 
                    usageLoggingWorker.PauseTimerAndRun();
                while (usageLoggingWorker.IsBusy)
                {
                    System.Threading.Thread.Sleep(500);
                }

                if (this.userId == 0 && Properties.Settings.Default.UserID == 0)
                {
                    this.userId = service.GetUserByEmail(Properties.Settings.Default.UserRegistrationEmail);
                    Properties.Settings.Default.UserID = this.userId;
                    Properties.Settings.Default.Save();
                }
                if (this.userId == 0) return;

                string path = Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, AppSettings.LocalFiles.Default.UserEventsData);
                UserEventsDS ds = new UserEventsDS();
                lock (_logSync) 
                {
                    ds.ReadXml(path);
                }
                service.SaveEventsBatchCompleted += new SaveEventsBatchCompletedEventHandler(LoggingService_SaveEventsBatchCompleted);
                List<SessionItem> sessions = new List<SessionItem>();
                foreach (UserEventsDS.SessionsRow row in ds.Sessions)
                {
                    SessionItem item = new SessionItem();
                    item.Session = row.Session;
                    item.UserId = this.userId;
                    item.StartDate = row.StartDate;
                    try
                    {
                        item.EndDate = row.EndDate;
                    }
                    catch (StrongTypingException)
                    {
                        //item.EndDate = new DateTime(1970, 1, 1);
                    }
                    sessions.Add(item);
                }
                ds.AcceptChanges();

                try
                {
                    if (service.SaveSessionBatch(sessions.ToArray()))
                    {
                        List<EventItem> events = new List<EventItem>();
                        foreach (UserEventsDS.EventsRow row in ds.Events)
                        {
                            EventItem item = new EventItem();
                            item.SlideId = row.SlideID;
                            item.TypeId = row.TypeID;
                            item.CategoryId = row.CategoryID;
                            item.Session = row.Session;
                            item.EventDate = row.EventDate;
                            item.Tag = row.Tag;
                            events.Add(item);
                        }
                        service.SaveEventsBatchAsync(events.ToArray(), Guid.NewGuid());
                    }
                }
                catch (Exception)
                {                  
                }
                
            }
        }

        private void LoggingService_SaveEventsBatchCompleted(object sender, SaveEventsBatchCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                LogIssue(e.Error, String.Format("LoggingService_SaveEventsBatchCompleted failed.  Operation cancelled: {0}", e.Cancelled));
                return;
            }
            if (e.Result)
            {
                lock (_logSync)
                {
                    string path = Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, AppSettings.LocalFiles.Default.UserEventsData);
                    UserEventsDS ue = new UserEventsDS();
                    ue.ReadXml(path);
                    ue.Events.Clear();
                    // remove all sessions except the current
                    DataRow[] toDelete = ue.Sessions.Select("Session <> '" + this.sessionId.ToString() + "'");
                    foreach (DataRow sessionRow in toDelete)
                    {
                        sessionRow.Delete();
                    }
                    ue = null;
                    ue = new UserEventsDS();
                    UserEventsDS.SessionsRow newRow = ue.Sessions.NewSessionsRow();
                    newRow.Session = this.sessionId.ToString();
                    newRow.StartDate = sessionStart;
                    newRow.UserID = userId;
                    ue.Sessions.AddSessionsRow(newRow);
                    ue.WriteXml(path);
                }
            }
        }

        private bool IsServiceAvailable(UsageLoggingService service)
        {
            try
            {
                return service.IsServiceAvailable();
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void Start_Update_Index_Timer()
        {
            if(Properties.Settings.Default.IndexComplete)
            tmr2.Start();
        }

        private void tmr_Tick(object sender, EventArgs e)
        {
            //ind = new BLL.LuceneIndex();
            if (_fullIndex)
            {
                //MessageBox.Show("Full Index");
                tmr.Stop();
                //ind.IndexUpdateProgressReported += new BLL.LuceneIndex.IndexUpdateProgressReportedDelegate(indexUpdater_ContentUpdateProgressReported);
                //ind.IndexUpdateCompleted += new BLL.LuceneIndex.IndexUpdateCompletedDelegate(indexUpdater_ContentUpdateCompleted);
                //ind.index_all();
                _index_in_progress = true;
                Start_Full_Index();
            }
            else
            { //MessageBox.Show("Not full Index"); 
            }
        }

        private void tmr2_Tick(object sender, EventArgs e)
        {
            if (_UpdateIndex)
            {
                tmr2.Stop();
                lblIndex.Text = "Indexing:";
                ind.IndexUpdateProgressReported += new BLL.LuceneIndex.IndexUpdateProgressReportedDelegate(indexUpdater_ContentUpdateProgressReported);
                ind.IndexUpdateCompleted += new BLL.LuceneIndex.IndexUpdateCompletedDelegate(indexUpdater_ContentUpdateCompleted);
                if (_toBeIndexed.Count > 0)
                {
                    if (Client.Properties.Settings.Default.IndexInProgress)
                        _toBeIndexed = ind.Merge_New_TBI(_toBeIndexed);
                    ind.Update_Index_Files(_toBeIndexed);
                    _index_in_progress = true;
                }
                if (_removeIndex.Count > 0)
                {
                    ind.Remove_Index_Files(_removeIndex);
                    _index_in_progress = true;
                }
                ViewModeSetup();
            }
        }

        private void HandleCorruptedFile(string file)
        {
            MessageBox.Show("The system encountered a corrupted file.  Please restart the application to refresh the data.  The application will now close.");
            try
            {
                File.Delete(file);
            }
            catch
            {
                //Fail Silently
            }
            Application.Exit();
        }

        #region Index updater events

        /// <summary>
        /// Contents the updater_ content update completed.
        /// </summary>
        private void indexUpdater_ContentUpdateCompleted()
        {

            //*SS Update the IPG progress bar
            UpdateIndexProgressComplete();

        }

        /// <summary>
        /// Contents the updater_ content update progress reported.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="progressPercent">The progress percent.</param>
        private void indexUpdater_ContentUpdateProgressReported(int progressPercent)
        {
            //this.Refresh();

            //*SS Update the IPG progress bar
            UpdateIndexProgress(progressPercent);
        }

        #endregion

        private void PopulateAdvancedSearch()
        {
            wsIpgSlideUpdater.SlideLibrary dsTempSlideLibrary = new Client.wsIpgSlideUpdater.SlideLibrary();
            string mainDataFile2 = Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, AppSettings.LocalFiles.Default.MainData);
            try
            {
                dsTempSlideLibrary.ReadXml(mainDataFile2);
            }
            catch
            {
                HandleCorruptedFile(mainDataFile2);
            }

            string filter = "ParentCategoryID=21";
            List<wsIpgSlideUpdater.SlideLibrary.CategoryRow> catr = new List<Client.wsIpgSlideUpdater.SlideLibrary.CategoryRow>((wsIpgSlideUpdater.SlideLibrary.CategoryRow[])dsTempSlideLibrary.Category.Select(filter));
            foreach (wsIpgSlideUpdater.SlideLibrary.CategoryRow cr in catr)
            {
                cbCategories.Items.Add(cr.Category);
            }
        }

        private void btnSearchIndex_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtSearch.Text))
            {
                psg_search(false);                
            }

        }

        private void Fill_Search_Results(LuceneSearch ls)
        {
            //Fill in the results hits.
            lblSearchResultsCount.Text = "(" + ls.ResultHits.ToString() + " Results)";

            ListViewItem li;            
            lvSearch.Items.Clear();
           
            foreach (SearchItem si in ls.SearchItems)
            {
                li = new ListViewItem();
                //Thumbnail
                if(si.FileName.ToLower().IndexOf(".pot") > 0)
                {                        
                    li.ImageIndex = 7;
                }else if(si.FileName.ToLower().IndexOf(".ppt") > 0)
                {
                    li.ImageIndex = 7;
                }
                else if (si.FileName.ToLower().IndexOf(".rtf") > 0)
                {
                    li.ImageIndex = 43;
                }
                else if (si.FileName.ToLower().IndexOf(".doc") > 0)
                {
                    li.ImageIndex = 43;
                }
                else if (si.FileName.ToLower().IndexOf(".pdf") > 0)
                {
                    li.ImageIndex = 42;
                }
                else if (si.FileName.ToLower().IndexOf(".txt") > 0)
                {
                    li.ImageIndex = 43;
                }
                else
                {

                }         
                
                
                // li.SubItems.Add()
                //Title
                if (string.IsNullOrEmpty(si.Title))
                    li.SubItems.Add(si.FileName.Remove(si.FileName.LastIndexOf(@".")).ToString().Substring(si.FileName.LastIndexOf(@"\") + 1)).Tag = si.FileName.ToString();                    
                else
                    li.SubItems.Add(si.Title.ToString()).Tag = si.FileName.ToString();
                //RegLang
                li.SubItems.Add(si.RegionLang.ToString()).Tag = si.RegionLangID.ToString();
                //Category
                 li.SubItems.Add(si.Category.ToString());
                 //Top Level Category
                 li.SubItems.Add(si.Top_Category.ToString());
                //Modified Date
                 li.SubItems.Add(si.ModifiedDate.ToString());

                lvSearch.Items.Add(li);  
            }

            //imlTinyThumbs.Images.Add(imageFilename, Image.FromFile(Path.Combine(AppSettings.LocalPaths.Default.ThumbnailDirectory, imageFilename)));
        }        

        private void tsbSearchShow_Click(object sender, EventArgs e)
        {
            splViewerBottom.Panel2Collapsed = false;
            tsbSearchShow.Visible = false;
            tsbSearchHide.Visible = true;
        }

        private void tsbSearchHide_Click(object sender, EventArgs e)
        {
            splViewerBottom.Panel2Collapsed = true;
            tsbSearchShow.Visible = true;
            tsbSearchHide.Visible = false;
        }

        private void cbAdvancedSearchDate_CheckedChanged(object sender, EventArgs e)
        {
            lblFrom.Enabled = cbAdvancedSearchDate.Checked;
            lblTo.Enabled = cbAdvancedSearchDate.Checked;
            dtFrom.Enabled = cbAdvancedSearchDate.Checked;
            dtTo.Enabled = cbAdvancedSearchDate.Checked;
        }

        private void cbAdvancedSearchCategories_CheckedChanged(object sender, EventArgs e)
        {
            cbCategories.Enabled = cbAdvancedSearchCategories.Checked;
            
        }

        private void lvSearch_DoubleClick(object sender, EventArgs e)
        {

            //if (lvSearch.SelectedItems[0].SubItems[1].Tag.ToString().ToLower().IndexOf(".pot") > 0)
            //{
                webSlidePreview.Navigate(lvSearch.SelectedItems[0].SubItems[1].Tag.ToString());
            //}
            //MessageBox.Show("Item clicked. Should open corresponding file [" + lvSearch.SelectedItems[0].SubItems[1].Text + "][" +lvSearch.SelectedItems[0].SubItems[1].Tag.ToString()+"]" );
            select_item_in_tree_node(lvSearch.SelectedItems[0].SubItems[1].Text);
            foreach (TabPage tb in tabRegionLanguageTrees.TabPages)
	        {
                if (tb.Text == lvSearch.SelectedItems[0].SubItems[2].Text)


                    tabRegionLanguageTrees.SelectedTab = tb;
	        }
        }

        private void select_item_in_tree_node(string cat)
        {
            //Search the controls for the treeview.
            foreach(Control ctrl in tabRegionLanguageTrees.Controls)
            {//We know the treeview is in the tabRegionLanguages Control so start searching there
                try
                {                       
                    foreach (Control ctrl2 in ctrl.Controls)
                    {
                        if (ctrl2.GetType() == typeof(BLL.CategorySlideTreeView))
                        {//We have foudn the right control
                            BLL.CategorySlideTreeView cst = (CategorySlideTreeView)ctrl2;                                
                            foreach (TreeNode node in cst.Nodes)
                            {//Need to find the correct node category
                                if (node.Text.ToLower() == cat.ToLower())
                                    cst.SelectedNode = node;
                                //Need to begin recursive iteration of the node to find the category
                                iterate_nodes_to_select_category(cst,node, cat);
                            }
                            //After the category is selected we need to select the item.
                            foreach (TreeNode node in cst.SelectedNode.Nodes)
                            {//Need to find the correct node category
                                if (node.Text.ToLower() == cat.ToLower())
                                    cst.SelectedNode = node;
                            }
                        }
                    }
                }
                catch (Exception)
                {                    
                }  
            }

        }
        private void iterate_nodes_to_select_category(BLL.CategorySlideTreeView cst, TreeNode inNode, string cat)
        {//Alows for recursive iteration of the nodes.

            foreach (TreeNode node in inNode.Nodes)
            {
                if (node.Text.ToLower() == cat.ToLower())
                    cst.SelectedNode = node;

                 iterate_nodes_to_select_category(cst,node, cat);

            }
        }

        private void txtSearch_Enter(object sender, EventArgs e)
        {//Make the the search button default when entering th esearch textbox
            IpgMain.ActiveForm.AcceptButton = btnSearchIndex;            
        }



        private void txtSearch_Leave(object sender, EventArgs e)
        {//remove the default button when leaving the search textbox
            try
            {
                IpgMain.ActiveForm.AcceptButton = null;
            }
            catch (Exception)
            {
            }
            
        }

        private void btnAdvancedSearchSubmit_Click(object sender, EventArgs e)
        {
            psg_search(true);  
        }

        private void psg_search(bool advanced)
        {   
            //setup search fields for pre search
            txtSearch.Enabled = false;
            btnAdvancedSearchSubmit.Enabled = false;
            btnSearchIndex.Enabled = false;
            this.Cursor = Cursors.WaitCursor;

            //Text Search criteria
            string strContent = "";
            string strFileName = "";
            string strTitle = "";
            if (advanced)
            {                
                if (!string.IsNullOrEmpty(txtSearchBox.Text))
                {//send text search for advnaced
                    strFileName = txtSearchBox.Text.ToLower();
                    strContent = txtSearchBox.Text.ToLower();
                    strTitle = txtSearchBox.Text.ToLower();
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(txtSearch.Text))
                {//send text search for basic. only search content
                    strFileName = txtSearch.Text.ToLower();
                    strContent = txtSearch.Text.ToLower();
                    strTitle = txtSearch.Text.ToLower();
                }
            }

            //Date Search criteria
            DateTime beginDate = DateTime.Parse("1/1/1972");
            DateTime endDate = DateTime.Now;
            if (cbAdvancedSearchDate.Checked && advanced)
            {// send date range for advanced search
                beginDate = dtFrom.Value;
                endDate = dtTo.Value;
            }

            //Category Search Criteria
            List<string> strCats = new List<string>();
            //If advanced search then send category list if that option is selected
            if (cbAdvancedSearchCategories.Checked && advanced)
                foreach (string cat in cbCategories.CheckedItems)
                {
                    strCats.Add(cat);
                }

            //Search is built now run search and populate the lucenesearch object with the results
            LuceneSearch ls = new LuceneSearch();
            ls = ls.Advanced_Search(strCats, strContent, strFileName, strTitle, beginDate, endDate);
            //Send the results to be viewed on the string.
            Fill_Search_Results(ls);

            txtSearch.Enabled = true;
            btnSearchIndex.Enabled = true;
            btnAdvancedSearchSubmit.Enabled = true;
            this.Cursor = Cursors.Default;
            CurrentViewMode = ViewMode.Build;
            splViewerBottom.Panel2Collapsed = false;
            tsbSearchShow.Visible = false;
            tsbSearchHide.Visible = true;
            ViewModeSetup();
        }

        private void tsddIndexing_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            switch (e.ClickedItem.Text.ToLower())
            {
                case "index all":
                    if (Properties.Settings.Default.AllowIndexing)
                    {
                        ind.Clean_up_Index();
                        Start_Full_Index();
                    }
                    break;
                case "turn off indexing":
                    if (Properties.Settings.Default.AllowIndexing)
                    {
                       Client.Properties.Settings.Default.AllowIndexing = false;
                        Properties.Settings.Default.Save();
                        if (Properties.Settings.Default.IndexInProgress)
                        {
                            DialogResult result = MessageBox.Show("This will stop your current index. Would like to continue?", "Continue?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (result == DialogResult.Yes)
                            {
                                Stop_and_Clear_Index();
                                MessageBox.Show("Indexing has been shut off. It will take a moment to close any indexes currently in progress. If you turn on indexing, you will need to manually begin a full index from the drop down menu.", "Indexing Shut Off", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                        }
                    }
                    Setup_Indexing_and_Search_Fields();
                    break;
                case "turn on indexing":
                    if (!Properties.Settings.Default.AllowIndexing)
                    {
                       Client.Properties.Settings.Default.AllowIndexing = true;
                        Properties.Settings.Default.Save();
                    }
                    Setup_Indexing_and_Search_Fields();
                    break;               
                default:
                    break;
            }            
        }

        private void Setup_Indexing_and_Search_Fields()
        {

            if (Client.Properties.Settings.Default.AllowIndexing && !_ContentUpdater.IsWorkInProgress && !_ContentUpdater.IsUpdatePaused)
            {//if allow indexing is true then show search fields and indexing options
                tsddIndexing.DropDownItems[0].Visible = true;
                tsddIndexing.DropDownItems[1].Visible = true;
                tsddIndexing.DropDownItems[2].Visible = false;
                //if index files exist show search fields otherwise hide them since there is nothing to search
                if (ind.Index_Exists() && !Client.Properties.Settings.Default.IndexInProgress)
                {
                    navigationPanePageAdvancedSearch.Visible = true;
                    txtSearch.Visible = true;
                    btnSearchIndex.Visible = true;
                    btnSearchFAQ.Visible = true;
                }
                else
                {
                    navigationPanePageAdvancedSearch.Visible = false;
                    txtSearch.Visible = false;
                    btnSearchIndex.Visible = false;
                    //btnSearchFAQ.Visible = false;
                }
                //Determine if we need to start index
                if (Client.Properties.Settings.Default.IndexInProgress)
                {
                    Continue_Index();
                }
                else
                {
                    if (!Client.Properties.Settings.Default.IndexComplete)
                    {
                        DialogResult result = MessageBox.Show("You currently do not have you files indexed. To take advantage of the search features we need to complete this process. Would you like to index now?", "Would you like to index?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (result == DialogResult.No){
                            Client.Properties.Settings.Default.AllowIndexing = false;
                            MessageBox.Show("Indexing and Search have been disabled. You can enable this at any time and index your files by selecting the menu option [Update -> Indexing Options]");
                            Setup_Indexing_and_Search_Fields();
                        }else
                            Start_Full_Index();
                    }
                }
            }
            else
            {//if allow indexing is false then hide search fields and indexing options except the ability to turn it on
                tsddIndexing.DropDownItems[0].Visible = false;
                tsddIndexing.DropDownItems[1].Visible = false;
                tsddIndexing.DropDownItems[2].Visible = true;
                navigationPanePageAdvancedSearch.Visible = false;
                txtSearch.Visible = false;
                btnSearchIndex.Visible = false;
                btnSearchFAQ.Visible = false;
            }
            ViewModeSetup();
            navigationPaneLeftPane.Refresh();
        }

        private void Start_Full_Index()
        {
            _index_in_progress = true;
            ind.IndexUpdateProgressReported += new BLL.LuceneIndex.IndexUpdateProgressReportedDelegate(indexUpdater_ContentUpdateProgressReported);
            ind.IndexUpdateCompleted += new BLL.LuceneIndex.IndexUpdateCompletedDelegate(indexUpdater_ContentUpdateCompleted);
            ind.index_all();
        }
        private void Continue_Index()
        {
            _index_in_progress = true;
            ind.IndexUpdateProgressReported += new BLL.LuceneIndex.IndexUpdateProgressReportedDelegate(indexUpdater_ContentUpdateProgressReported);
            ind.IndexUpdateCompleted += new BLL.LuceneIndex.IndexUpdateCompletedDelegate(indexUpdater_ContentUpdateCompleted);
            ind.Continue_Index();
        }

        public void Stop_and_Clear_Index()
        {
            ind.Stop_and_Clear_Index();
        }

        private void btnSearchFAQ_Click(object sender, EventArgs e)
        {
            Show_Search_FAQ();
        }

        private void btnASearchFAQ_Click(object sender, EventArgs e)
        {
            Show_Search_FAQ();
        }

        private void Show_Search_FAQ()
        {
            SearchFAQ faqHelper = new SearchFAQ();
            faqHelper.Owner = this;
            faqHelper.Show();
        }

        public static Form IsFormAlreadyOpen(Type FormType)
        {
            foreach (Form OpenForm in Application.OpenForms)
            {
                if (OpenForm.GetType() == FormType)
                    return OpenForm;
            }

            return null;
        }

        private void llRegistryFix_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://staging.boise.wirestone.com/clients/hp/psg/creationstation/help/support.htm"); 
        }

        private void btnregistryFixClose_Click(object sender, EventArgs e)
        {
            llRegistryFix.Visible = false;
            btnregistryFixClose.Visible = false;
            Client.Properties.Settings.Default.RegistryFixHelp = false;
        }

    }    

    public delegate void NoAppUpdateEventHandler(object sender, EventArgs e);

}
