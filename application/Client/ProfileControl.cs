﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Client.BLL;
using System.Collections.Specialized;

namespace Client
{
    public partial class ProfileControl : UserControl
    {
        private wsIpgSlideUpdater.SlideLibrary dsRegionLanguages;
        RegionLanaguageAccessor regionLanguageAccessor;

        public ProfileControl()
        {
            InitializeComponent();
        }

        private void ProfileControl_Load(object sender, EventArgs e)
        {
            InitializeErrorChecking();
            regionLanguageAccessor = new RegionLanaguageAccessor();
            PopulateRegionDropDown();
        }

        private void InitializeErrorChecking()
        {
            this.errorProvider1.SetError(txtFirstName, "");
            this.errorProvider1.SetError(txtLastName, "");
            this.errorProvider1.SetError(txtPhoneNumber, "");
            this.errorProvider1.SetError(txtCompanyName, "");
            this.errorProvider1.SetError(txtCompanySize, "");
            this.errorProvider1.SetError(txtReferrer, "");
            this.errorProvider1.SetError(ddlRegion, "");
            this.errorProvider1.SetError(ddlJobTitles, "");
            this.errorProvider1.SetError(ddlCountries, "");

            this.txtFirstName.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateInput);
            this.txtLastName.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateInput);
            this.txtPhoneNumber.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateInput);
            this.txtCompanyName.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateInput);
            this.txtCompanySize.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateInput);
            this.txtReferrer.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateInput);
            this.ddlRegion.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateInput);
            this.ddlJobTitles.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateInput);
            this.ddlCountries.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateInput);
        }

        private void ValidateInput(object sender, CancelEventArgs e)
        {
            bool ctrlHasError = false;
            Control ctrl = (Control)sender;
            switch (ctrl.Name)
            {
                case "ddlRegion":
                    {
                        ComboBox regionCombo = ctrl as ComboBox;
                        if (regionCombo.SelectedIndex.Equals(0))
                        {
                            SetError(ctrl, "Please select a region.");
                            ctrlHasError = true;
                        }
                        break;
                    }
                case "ddlJobTitles":
                    {
                        ComboBox jobTitleCombo = ctrl as ComboBox;
                        if (jobTitleCombo.SelectedIndex.Equals(0))
                        {
                            SetError(ctrl, "Please select a job title.");
                            ctrlHasError = true;
                        }
                        break;
                    }
                case "ddlCountries":
                    {
                        ComboBox countriesCombo = ctrl as ComboBox;
                        if (countriesCombo.SelectedIndex.Equals(0))
                        {
                            SetError(ctrl, "Please select a country.");
                            ctrlHasError = true;
                        }
                        break;
                    }
                case "txtCompanySize":
                    {
                        if (ctrl.Text.Trim().Length == 0)
                        {
                            SetError(ctrl, "Company Size is required.");
                            ctrlHasError = true;
                        }
                        else
                        {
                            int result;
                            ctrl.Text = ctrl.Text.Replace(",", string.Empty);
                            ctrl.Text = ctrl.Text.Replace(".", string.Empty);
                            bool conversionSucceeded = Int32.TryParse(ctrl.Text, out result);
                            if (!conversionSucceeded || result < 1)
                            {
                                SetError(ctrl, "Company Size must be a postive whole number greater than 0.");
                                ctrlHasError = true;
                            }

                        }
                        break;
                    }
                case "txtFirstName":
                    {
                        if (ctrl.Text.Trim().Length == 0)
                        {
                            SetError(ctrl, "First Name is required.");
                            ctrlHasError = true;
                        }
                        break;
                    }
                case "txtLastName":
                    {
                        if (ctrl.Text.Trim().Length == 0)
                        {
                            SetError(ctrl, "Last Name is required.");
                            ctrlHasError = true;
                        }
                        break;
                    }
                case "txtPhoneNumber":
                    {
                        if (ctrl.Text.Trim().Length == 0)
                        {
                            SetError(ctrl, "Phone Number is required.");
                            ctrlHasError = true;
                        }
                        break;
                    }
                case "txtCompanyName":
                    {
                        if (ctrl.Text.Trim().Length == 0)
                        {
                            SetError(ctrl, "Company Name is required.");
                            ctrlHasError = true;
                        }
                        break;
                    }
                case "txtReferrer":
                    {
                        if (ctrl.Text.Trim().Length == 0)
                        {
                            SetError(ctrl, "Referrer is required.");
                            ctrlHasError = true;
                        }
                        break;
                    }
            }
            if (!ctrlHasError)
                ClearError(ctrl);
        }

        public bool IsProfileValid()
        {
            bool isValid = true;
            foreach (Control control in this.Controls)
            {
                if (control is System.Windows.Forms.TextBox || control is System.Windows.Forms.ComboBox)
                {
                    if (this.errorProvider1.GetError(control).Length != 0)
                    {
                        isValid = false;
                        break;
                    }
                }
                if (control is System.Windows.Forms.TextBox)
                {
                    string text = control.Text.ToString().Trim();
                    if (text.Length == 0)
                    {
                        isValid = false;
                        break;
                    }
                }
                else if (control is System.Windows.Forms.ComboBox)
                {
                    ComboBox ctrl = control as ComboBox;
                    if (ctrl.SelectedIndex.Equals(0))
                    {
                        isValid = false;
                        break;
                    }
                }
            }
            return isValid;
        }

        public void ValidateControls()
        {
            foreach (Control control in this.Controls)
            {
                if (control is ComboBox)
                {
                    ComboBox ddl = control as ComboBox;
                    if (ddl.SelectedIndex == 0)
                        this.SetError(control, "Please select an item.");
                }
                else if (control is TextBox)
                {
                    TextBox txt = control as TextBox;
                    if (txt.Text.Equals("0") || string.IsNullOrEmpty(txt.Text))
                    {
                        this.SetError(control, "Please complete.");
                    }
                }
            }
        }

        public void SetError(Control ctrl, string errorMessage)
        {
            this.errorProvider1.SetError(ctrl, errorMessage);
        }

        private void ClearError(Control ctrl)
        {
            this.errorProvider1.SetError(ctrl, "");
        }

        private void PopulateRegionDropDown()
        {
            dsRegionLanguages = regionLanguageAccessor.GetRegionLanguagesDataSet();
            ddlRegion.Items.Clear();
            ddlRegion.Items.Add("Please Select...");
            ddlRegion.DisplayMember = "RegionName";
            foreach (wsIpgSlideUpdater.SlideLibrary.RegionRow regionRow in dsRegionLanguages.Region.Rows)
            {
                if (regionRow.RegionName != "Common")
                    ddlRegion.Items.Add(regionRow);
            }
            ddlRegion.SelectedIndex = 0;
        }

        public void PopulateCountryDropDown(List<Country> countries)
        {
            ddlCountries.Items.Clear();
            ddlCountries.Items.Add("Please Select...");
            ddlCountries.DisplayMember = "Name";
            foreach (Country country in countries)
                ddlCountries.Items.Add(country);
            ddlCountries.SelectedIndex = 0;
        }

        public void PopulateJobTitleDropDown(List<JobTitle> jobTitles)
        {
            ddlJobTitles.Items.Clear();
            ddlJobTitles.Items.Add("Please Select...");
            ddlJobTitles.DisplayMember = "Name";
            foreach (JobTitle jobTitle in jobTitles)
                ddlJobTitles.Items.Add(jobTitle);
            ddlJobTitles.SelectedIndex = 0;
        }

        public void Populate(Client.wsUserRegistration.UserRegistrationDS.UserProfileRow psgProfile)
        {
            this.FirstName = psgProfile.IsFirstNameNull() ? string.Empty : psgProfile.FirstName;
            this.LastName = psgProfile.IsLastNameNull() ? string.Empty : psgProfile.LastName;
            this.PhoneNumber = psgProfile.IsPhoneNull() ? string.Empty : psgProfile.Phone;
            this.Country = psgProfile.IsCountryNull() ? string.Empty : psgProfile.Country;
            this.RegionProperty = psgProfile.IsRegionIdNull() ? 0 : psgProfile.RegionId;
            this.CompanyNameProperty = psgProfile.IsCompanyNameNull() ? string.Empty : psgProfile.CompanyName;
            this.CompanySize = psgProfile.IsCompanySizeNull() ? 0 : psgProfile.CompanySize;
            this.JobTitle = psgProfile.IsJobTitleNull() ? string.Empty : psgProfile.JobTitle;
            this.Referrer = psgProfile.IsReferenceSourceNull() ? string.Empty : psgProfile.ReferenceSource;

            ValidateControls();
        }

        public string FirstName
        {
            get
            {
                return txtFirstName.Text;
            }
            set
            {
                txtFirstName.Text = value;
            }
        }
        public string LastName
        {
            get
            {
                return txtLastName.Text;
            }
            set
            {
                txtLastName.Text = value;
            }
        }
        public string PhoneNumber
        {
            get
            {
                return txtPhoneNumber.Text;
            }
            set
            {
                txtPhoneNumber.Text = value;
            }
        }
        public string Country
        {
            get
            {
                Country country = ddlCountries.SelectedItem as Country;
                return country.Code;
            }
            set
            {
                int index = 0;
                foreach (object item in ddlCountries.Items)
                {
                    if (item is Country)
                    {
                        Country country = item as Country;
                        if (country.Code == value)
                        {
                            ddlCountries.SelectedIndex = index;
                            break;
                        }
                    }
                    index++;
                }
            }
        }
        public int RegionProperty
        {
            get
            {
                Client.wsIpgSlideUpdater.SlideLibrary.RegionRow row = ddlRegion.SelectedItem as Client.wsIpgSlideUpdater.SlideLibrary.RegionRow;
                return row.RegionID;
            }
            set
            {
                int index = 0;
                foreach (object item in ddlRegion.Items)
                {
                    if (item is Client.wsIpgSlideUpdater.SlideLibrary.RegionRow)
                    {
                        Client.wsIpgSlideUpdater.SlideLibrary.RegionRow row = item as Client.wsIpgSlideUpdater.SlideLibrary.RegionRow;
                        if (row.RegionID == value)
                        {
                            ddlRegion.SelectedIndex = index;
                            break;
                        }
                    }
                    index++;
                }
            }
        }
        public string CompanyNameProperty
        {
            get
            {
                return txtCompanyName.Text;
            }
            set
            {
                txtCompanyName.Text = value;
            }
        }
        public int CompanySize
        {
            get
            {
                int size = 0;
                Int32.TryParse(txtCompanySize.Text, out size);
                return size;
            }
            set
            {
                txtCompanySize.Text = value.ToString();
            }
        }
        public string JobTitle
        {
            get
            {
                JobTitle jobTitle = ddlJobTitles.SelectedItem as JobTitle;
                return jobTitle.Code;
            }
            set
            {
                int index = 0;
                foreach (object item in ddlJobTitles.Items)
                {
                    if (item is JobTitle)
                    {
                        JobTitle jobTitle = item as JobTitle;
                        if (jobTitle.Code == value)
                        {
                            ddlJobTitles.SelectedIndex = index;
                            break;
                        }
                    }
                    index++;
                }
            }
        }
        public string Referrer
        {
            get
            {
                return txtReferrer.Text;
            }
            set
            {
                txtReferrer.Text = value;
            }
        }

    }
}
