namespace Client
{
    partial class MyRegionLanguagesDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gradientCaptionFormTitle = new Ascend.Windows.Forms.GradientCaption();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.regionLanguageGroupGrid = new Client.UserControls.RegionLanguageGroupGrid();
            ((System.ComponentModel.ISupportInitialize)(this.regionLanguageGroupGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // gradientCaptionFormTitle
            // 
            this.gradientCaptionFormTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gradientCaptionFormTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gradientCaptionFormTitle.Location = new System.Drawing.Point(0, 0);
            this.gradientCaptionFormTitle.Name = "gradientCaptionFormTitle";
            this.gradientCaptionFormTitle.Size = new System.Drawing.Size(444, 50);
            this.gradientCaptionFormTitle.TabIndex = 1;
            this.gradientCaptionFormTitle.Text = "Choose your preferred Region/Languages";
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(276, 452);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 2;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(357, 452);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // regionLanguageGroupGrid
            // 
            this.regionLanguageGroupGrid.AllowUserToAddRows = false;
            this.regionLanguageGroupGrid.BackgroundColor = System.Drawing.Color.White;
            this.regionLanguageGroupGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.regionLanguageGroupGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.regionLanguageGroupGrid.CollapseIcon = null;
            this.regionLanguageGroupGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.regionLanguageGroupGrid.ColumnHeadersVisible = false;
            this.regionLanguageGroupGrid.ExpandIcon = null;
            this.regionLanguageGroupGrid.Location = new System.Drawing.Point(0, 50);
            this.regionLanguageGroupGrid.Name = "regionLanguageGroupGrid";
            this.regionLanguageGroupGrid.RowHeadersVisible = false;
            this.regionLanguageGroupGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.regionLanguageGroupGrid.Size = new System.Drawing.Size(444, 396);
            this.regionLanguageGroupGrid.TabIndex = 4;
            // 
            // MyRegionLanguagesDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(444, 486);
            this.Controls.Add(this.regionLanguageGroupGrid);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.gradientCaptionFormTitle);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "MyRegionLanguagesDialog";
            this.Text = "My Regions and Languages";
            this.Load += new System.EventHandler(this.MyRegionLanguagesDialog_Load);
            ((System.ComponentModel.ISupportInitialize)(this.regionLanguageGroupGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Ascend.Windows.Forms.GradientCaption gradientCaptionFormTitle;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private Client.UserControls.RegionLanguageGroupGrid regionLanguageGroupGrid;
    }
}