namespace Client
{
    partial class IpgMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(IpgMain));
            this.tsPreview = new System.Windows.Forms.ToolStrip();
            this.tsbSlideLibraryAddToMySlides = new System.Windows.Forms.ToolStripButton();
            this.tsbSlideLibraryPlay = new System.Windows.Forms.ToolStripButton();
            this.tsbSlideLibraryExportPpt = new System.Windows.Forms.ToolStripButton();
            this.tsbSlideLibraryPrint = new System.Windows.Forms.ToolStripButton();
            this.tsbSlideLibrarySelectAll = new System.Windows.Forms.ToolStripButton();
            this.tsMySlides = new System.Windows.Forms.ToolStrip();
            this.tsbMySlidesSave = new System.Windows.Forms.ToolStripButton();
            this.tsbMySlidesSaveAs = new System.Windows.Forms.ToolStripButton();
            this.tsbMySlideRemoveSlide = new System.Windows.Forms.ToolStripButton();
            this.tsbMySlidesClear = new System.Windows.Forms.ToolStripButton();
            this.tsbMySlidesPlay = new System.Windows.Forms.ToolStripButton();
            this.tsbMySlidesExportPpt = new System.Windows.Forms.ToolStripButton();
            this.tsbMySlidesPrint = new System.Windows.Forms.ToolStripButton();
            this.tsbMySlidesSelectAll = new System.Windows.Forms.ToolStripButton();
            this.splForm = new System.Windows.Forms.SplitContainer();
            this.navigationPaneLeftPane = new Ascend.Windows.Forms.NavigationPane();
            this.navigationPanePageSlideLibrary = new Ascend.Windows.Forms.NavigationPanePage();
            this.tabRegionLanguageTrees = new System.Windows.Forms.TabControl();
            this.navigationPanePageMyPresentations = new Ascend.Windows.Forms.NavigationPanePage();
            this.tvwMySlideDecks = new System.Windows.Forms.TreeView();
            this.cmnMyPresentations = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.miMyPresentationsDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.imlIcons16 = new System.Windows.Forms.ImageList(this.components);
            this.categorySlideTreeViewPresentations = new Client.BLL.CategorySlideTreeView();
            this.navigationPanePageAdvancedSearch = new Ascend.Windows.Forms.NavigationPanePage();
            this.lblSearchString = new System.Windows.Forms.Label();
            this.txtSearchBox = new System.Windows.Forms.TextBox();
            this.cbAdvancedSearchDate = new System.Windows.Forms.CheckBox();
            this.lblFrom = new System.Windows.Forms.Label();
            this.dtFrom = new System.Windows.Forms.DateTimePicker();
            this.lblTo = new System.Windows.Forms.Label();
            this.dtTo = new System.Windows.Forms.DateTimePicker();
            this.cbAdvancedSearchCategories = new System.Windows.Forms.CheckBox();
            this.cbCategories = new System.Windows.Forms.CheckedListBox();
            this.btnAdvancedSearchSubmit = new System.Windows.Forms.Button();
            this.btnASearchFAQ = new System.Windows.Forms.Button();
            this.navigationPanePageFilters = new Ascend.Windows.Forms.NavigationPanePage();
            this.tabFilterGroups = new System.Windows.Forms.TabControl();
            this.gradientPanel1 = new Ascend.Windows.Forms.GradientPanel();
            this.btnApplyFilterSettings = new System.Windows.Forms.Button();
            this.gradientCaption4 = new Ascend.Windows.Forms.GradientCaption();
            this.splViewers = new System.Windows.Forms.SplitContainer();
            this.splPreview = new System.Windows.Forms.SplitContainer();
            this.btnregistryFixClose = new System.Windows.Forms.Button();
            this.llRegistryFix = new System.Windows.Forms.LinkLabel();
            this.btnSearchFAQ = new System.Windows.Forms.Button();
            this.pbIndex = new System.Windows.Forms.ProgressBar();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.lblIndex = new System.Windows.Forms.Label();
            this.btnSearchIndex = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.lblPreview = new System.Windows.Forms.Label();
            this.splPreviewType = new System.Windows.Forms.SplitContainer();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.lstPreview = new System.Windows.Forms.ListView();
            this.cmnSlideLibraryPreview = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.miSlideLibraryPreviewAddToMySlides = new System.Windows.Forms.ToolStripMenuItem();
            this.miSlideLibraryPreviewSelectAll = new System.Windows.Forms.ToolStripMenuItem();
            this.imlTinyThumbs = new System.Windows.Forms.ImageList(this.components);
            this.tsRegionStatus = new System.Windows.Forms.ToolStrip();
            this.tslRegionStatus = new System.Windows.Forms.ToolStripLabel();
            this.tsbSlideLibraryPreviewNav = new System.Windows.Forms.ToolStrip();
            this.tsbSlideLibraryPreviewHome = new System.Windows.Forms.ToolStripButton();
            this.tsbSlideLibraryPreviewBack = new System.Windows.Forms.ToolStripButton();
            this.tsbSlideLibraryPreviewForward = new System.Windows.Forms.ToolStripButton();
            this.tslSlideLibraryPreviewSlideRegion = new System.Windows.Forms.ToolStripLabel();
            this.webSlidePreview = new System.Windows.Forms.WebBrowser();
            this.picPptScrollHider = new System.Windows.Forms.PictureBox();
            this.splViewerBottom = new System.Windows.Forms.SplitContainer();
            this.splMySlides = new System.Windows.Forms.SplitContainer();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblMySlides = new System.Windows.Forms.Label();
            this.statusMySlides = new System.Windows.Forms.StatusStrip();
            this.lstMySlides = new System.Windows.Forms.ListView();
            this.cmnMySlides = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.miMySlidesSave = new System.Windows.Forms.ToolStripMenuItem();
            this.miMySlidesSaveAs = new System.Windows.Forms.ToolStripMenuItem();
            this.miMySlidesRemove = new System.Windows.Forms.ToolStripMenuItem();
            this.miMySlidesClear = new System.Windows.Forms.ToolStripMenuItem();
            this.miMySlidesPlay = new System.Windows.Forms.ToolStripMenuItem();
            this.miMySlidesExportPpt = new System.Windows.Forms.ToolStripMenuItem();
            this.miMySlidesPrint = new System.Windows.Forms.ToolStripMenuItem();
            this.miMySlidesSelectAll = new System.Windows.Forms.ToolStripMenuItem();
            this.imlSlideThumbs = new System.Windows.Forms.ImageList(this.components);
            this.splSearch = new System.Windows.Forms.SplitContainer();
            this.lblSearchResultsCount = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lvSearch = new System.Windows.Forms.ListView();
            this.Thumbnail = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Title = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.RegionLang = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Category = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.TopCategory = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ModifiedDate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.statusMySlidesCount = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusMySlidesQuantity = new System.Windows.Forms.ToolStripStatusLabel();
            this.diaPrint = new System.Windows.Forms.PrintDialog();
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.tsMain = new System.Windows.Forms.ToolStrip();
            this.tsbMainBuildMode = new System.Windows.Forms.ToolStripButton();
            this.tsbMainBrowseMode = new System.Windows.Forms.ToolStripButton();
            this.tsbMainHideNavigation = new System.Windows.Forms.ToolStripButton();
            this.tsbMainShowNavigation = new System.Windows.Forms.ToolStripButton();
            this.tsbMainHelp = new System.Windows.Forms.ToolStripSplitButton();
            this.tsbMainHelpManual = new System.Windows.Forms.ToolStripMenuItem();
            this.supportFAQToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registerContentIssuesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registerSoftwareIssuesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsbMainHelpAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.tsbMainUpdates = new System.Windows.Forms.ToolStripSplitButton();
            this.tsbMainUpdateApplication = new System.Windows.Forms.ToolStripMenuItem();
            this.tsbMainUpdateData = new System.Windows.Forms.ToolStripMenuItem();
            this.profileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsddIndexing = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmIndexAll = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmTurnOffIndex = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmTurnOnIndex = new System.Windows.Forms.ToolStripMenuItem();
            this.tsbMainSettings = new System.Windows.Forms.ToolStripSplitButton();
            this.tsbMyRegionLanguages = new System.Windows.Forms.ToolStripMenuItem();
            this.tsbSearchShow = new System.Windows.Forms.ToolStripButton();
            this.tsbSearchHide = new System.Windows.Forms.ToolStripButton();
            this.tsbShowAnnouncements = new System.Windows.Forms.ToolStripButton();
            this.diaExportSave = new System.Windows.Forms.SaveFileDialog();
            this.stsMain = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabelThumbnail = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripProgressBarThumbnail = new System.Windows.Forms.ToolStripProgressBar();
            this.lblMainStatusMessage = new System.Windows.Forms.ToolStripStatusLabel();
            this.pbMainStatusProgress = new System.Windows.Forms.ToolStripProgressBar();
            this.lblMainStatusContentUpdate = new System.Windows.Forms.ToolStripStatusLabel();
            this.pbContentUpdate = new System.Windows.Forms.ToolStripProgressBar();
            this.toolStripStatusLabelResume = new System.Windows.Forms.ToolStripStatusLabel();
            this.clkWait = new System.Windows.Forms.Timer(this.components);
            this.exportPptBuilder = new PlugIns.Helpers.PptBuilder();
            this.playPptBuilder = new PlugIns.Helpers.PptBuilder();
            this.printPptBuilder = new PlugIns.Helpers.PptBuilder();
            this.webPrinter = new System.Windows.Forms.WebBrowser();
            this.clkAppUpdate = new System.Windows.Forms.Timer(this.components);
            this.backgroundWorkerPreviewThumbnails = new System.ComponentModel.BackgroundWorker();
            this.usageLoggingWorker = new PlugIns.Helpers.IntervalInvokingWorker(this.components);
            this.serviceLoggingWorker = new PlugIns.Helpers.IntervalInvokingWorker(this.components);
            this.tmr = new System.Windows.Forms.Timer(this.components);
            this.tmr2 = new System.Windows.Forms.Timer(this.components);
            this.printPreviewDialog1 = new System.Windows.Forms.PrintPreviewDialog();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.tsPreview.SuspendLayout();
            this.tsMySlides.SuspendLayout();
            this.splForm.Panel1.SuspendLayout();
            this.splForm.Panel2.SuspendLayout();
            this.splForm.SuspendLayout();
            this.navigationPaneLeftPane.SuspendLayout();
            this.navigationPanePageSlideLibrary.SuspendLayout();
            this.navigationPanePageMyPresentations.SuspendLayout();
            this.cmnMyPresentations.SuspendLayout();
            this.navigationPanePageAdvancedSearch.SuspendLayout();
            this.navigationPanePageFilters.SuspendLayout();
            this.gradientPanel1.SuspendLayout();
            this.splViewers.Panel1.SuspendLayout();
            this.splViewers.Panel2.SuspendLayout();
            this.splViewers.SuspendLayout();
            this.splPreview.Panel1.SuspendLayout();
            this.splPreview.Panel2.SuspendLayout();
            this.splPreview.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.splPreviewType.Panel1.SuspendLayout();
            this.splPreviewType.Panel2.SuspendLayout();
            this.splPreviewType.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.cmnSlideLibraryPreview.SuspendLayout();
            this.tsRegionStatus.SuspendLayout();
            this.tsbSlideLibraryPreviewNav.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picPptScrollHider)).BeginInit();
            this.splViewerBottom.Panel1.SuspendLayout();
            this.splViewerBottom.Panel2.SuspendLayout();
            this.splViewerBottom.SuspendLayout();
            this.splMySlides.Panel1.SuspendLayout();
            this.splMySlides.Panel2.SuspendLayout();
            this.splMySlides.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.cmnMySlides.SuspendLayout();
            this.splSearch.Panel1.SuspendLayout();
            this.splSearch.Panel2.SuspendLayout();
            this.splSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.tsMain.SuspendLayout();
            this.stsMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // tsPreview
            // 
            this.tsPreview.BackColor = System.Drawing.Color.White;
            this.tsPreview.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tsPreview.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsPreview.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.tsPreview.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbSlideLibraryAddToMySlides,
            this.tsbSlideLibraryPlay,
            this.tsbSlideLibraryExportPpt,
            this.tsbSlideLibraryPrint,
            this.tsbSlideLibrarySelectAll});
            this.tsPreview.Location = new System.Drawing.Point(0, 27);
            this.tsPreview.Name = "tsPreview";
            this.tsPreview.Size = new System.Drawing.Size(786, 31);
            this.tsPreview.TabIndex = 0;
            // 
            // tsbSlideLibraryAddToMySlides
            // 
            this.tsbSlideLibraryAddToMySlides.BackColor = System.Drawing.Color.White;
            this.tsbSlideLibraryAddToMySlides.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(41)))), ((int)(((byte)(94)))));
            this.tsbSlideLibraryAddToMySlides.Image = global::Client.Properties.Resources.AddBluePNG;
            this.tsbSlideLibraryAddToMySlides.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbSlideLibraryAddToMySlides.Name = "tsbSlideLibraryAddToMySlides";
            this.tsbSlideLibraryAddToMySlides.Size = new System.Drawing.Size(28, 28);
            this.tsbSlideLibraryAddToMySlides.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.tsbSlideLibraryAddToMySlides.ToolTipText = "Add selected slide(s) to MySlides";
            this.tsbSlideLibraryAddToMySlides.Click += new System.EventHandler(this.tsbSlideLibraryAddToMySlides_Click);
            // 
            // tsbSlideLibraryPlay
            // 
            this.tsbSlideLibraryPlay.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(41)))), ((int)(((byte)(94)))));
            this.tsbSlideLibraryPlay.Image = global::Client.Properties.Resources.PlayBluePNG;
            this.tsbSlideLibraryPlay.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbSlideLibraryPlay.Name = "tsbSlideLibraryPlay";
            this.tsbSlideLibraryPlay.Size = new System.Drawing.Size(28, 28);
            this.tsbSlideLibraryPlay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.tsbSlideLibraryPlay.ToolTipText = "Play Current Slide Library Slides";
            this.tsbSlideLibraryPlay.Click += new System.EventHandler(this.tsbSlideLibraryPlay_Click);
            // 
            // tsbSlideLibraryExportPpt
            // 
            this.tsbSlideLibraryExportPpt.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(41)))), ((int)(((byte)(94)))));
            this.tsbSlideLibraryExportPpt.Image = global::Client.Properties.Resources.ExportBluePNG;
            this.tsbSlideLibraryExportPpt.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbSlideLibraryExportPpt.Name = "tsbSlideLibraryExportPpt";
            this.tsbSlideLibraryExportPpt.Size = new System.Drawing.Size(28, 28);
            this.tsbSlideLibraryExportPpt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.tsbSlideLibraryExportPpt.ToolTipText = "Export to PowerPoint";
            this.tsbSlideLibraryExportPpt.Click += new System.EventHandler(this.tsbSlideLibraryExportPpt_Click);
            // 
            // tsbSlideLibraryPrint
            // 
            this.tsbSlideLibraryPrint.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(41)))), ((int)(((byte)(94)))));
            this.tsbSlideLibraryPrint.Image = global::Client.Properties.Resources.PrintBluePNG;
            this.tsbSlideLibraryPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbSlideLibraryPrint.Name = "tsbSlideLibraryPrint";
            this.tsbSlideLibraryPrint.Size = new System.Drawing.Size(28, 28);
            this.tsbSlideLibraryPrint.ToolTipText = "Print Slide Library Viewer Slides";
            this.tsbSlideLibraryPrint.Click += new System.EventHandler(this.tsbSlideLibraryPrint_Click);
            // 
            // tsbSlideLibrarySelectAll
            // 
            this.tsbSlideLibrarySelectAll.BackColor = System.Drawing.Color.White;
            this.tsbSlideLibrarySelectAll.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(41)))), ((int)(((byte)(94)))));
            this.tsbSlideLibrarySelectAll.Image = global::Client.Properties.Resources.SelectAllBluePNG;
            this.tsbSlideLibrarySelectAll.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbSlideLibrarySelectAll.Name = "tsbSlideLibrarySelectAll";
            this.tsbSlideLibrarySelectAll.Size = new System.Drawing.Size(28, 28);
            this.tsbSlideLibrarySelectAll.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.tsbSlideLibrarySelectAll.ToolTipText = "Select All";
            this.tsbSlideLibrarySelectAll.Click += new System.EventHandler(this.tsbSlideLibrarySelectAll_Click);
            // 
            // tsMySlides
            // 
            this.tsMySlides.AllowItemReorder = true;
            this.tsMySlides.BackColor = System.Drawing.Color.White;
            this.tsMySlides.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.tsMySlides.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tsMySlides.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsMySlides.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.tsMySlides.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbMySlidesSave,
            this.tsbMySlidesSaveAs,
            this.tsbMySlideRemoveSlide,
            this.tsbMySlidesClear,
            this.tsbMySlidesPlay,
            this.tsbMySlidesExportPpt,
            this.tsbMySlidesPrint,
            this.tsbMySlidesSelectAll});
            this.tsMySlides.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.tsMySlides.Location = new System.Drawing.Point(0, 29);
            this.tsMySlides.Name = "tsMySlides";
            this.tsMySlides.Size = new System.Drawing.Size(786, 31);
            this.tsMySlides.TabIndex = 0;
            // 
            // tsbMySlidesSave
            // 
            this.tsbMySlidesSave.BackColor = System.Drawing.Color.White;
            this.tsbMySlidesSave.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(79)))), ((int)(((byte)(46)))));
            this.tsbMySlidesSave.Image = global::Client.Properties.Resources.SaveGreenPNG;
            this.tsbMySlidesSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbMySlidesSave.Name = "tsbMySlidesSave";
            this.tsbMySlidesSave.Size = new System.Drawing.Size(28, 28);
            this.tsbMySlidesSave.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.tsbMySlidesSave.ToolTipText = "Save";
            this.tsbMySlidesSave.Click += new System.EventHandler(this.tsbMySlidesSave_Click);
            // 
            // tsbMySlidesSaveAs
            // 
            this.tsbMySlidesSaveAs.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(79)))), ((int)(((byte)(46)))));
            this.tsbMySlidesSaveAs.Image = global::Client.Properties.Resources.SaveAsGreenPNG;
            this.tsbMySlidesSaveAs.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbMySlidesSaveAs.Name = "tsbMySlidesSaveAs";
            this.tsbMySlidesSaveAs.Size = new System.Drawing.Size(28, 28);
            this.tsbMySlidesSaveAs.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.tsbMySlidesSaveAs.ToolTipText = "Save As..";
            this.tsbMySlidesSaveAs.Click += new System.EventHandler(this.tsbMySlidesSaveAs_Click);
            // 
            // tsbMySlideRemoveSlide
            // 
            this.tsbMySlideRemoveSlide.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(79)))), ((int)(((byte)(46)))));
            this.tsbMySlideRemoveSlide.Image = global::Client.Properties.Resources.RemoveGreenPNG;
            this.tsbMySlideRemoveSlide.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbMySlideRemoveSlide.Name = "tsbMySlideRemoveSlide";
            this.tsbMySlideRemoveSlide.Size = new System.Drawing.Size(28, 28);
            this.tsbMySlideRemoveSlide.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.tsbMySlideRemoveSlide.ToolTipText = "Remove selected slide(s) from MySlides";
            this.tsbMySlideRemoveSlide.Click += new System.EventHandler(this.tsbMySlidesRemoveSlide_Click);
            // 
            // tsbMySlidesClear
            // 
            this.tsbMySlidesClear.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(79)))), ((int)(((byte)(46)))));
            this.tsbMySlidesClear.Image = global::Client.Properties.Resources.ClearAllGreenPNG;
            this.tsbMySlidesClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbMySlidesClear.Name = "tsbMySlidesClear";
            this.tsbMySlidesClear.Size = new System.Drawing.Size(28, 28);
            this.tsbMySlidesClear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.tsbMySlidesClear.ToolTipText = "Clear My Slides";
            this.tsbMySlidesClear.Click += new System.EventHandler(this.tsbMySlidesClear_Click);
            // 
            // tsbMySlidesPlay
            // 
            this.tsbMySlidesPlay.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(79)))), ((int)(((byte)(46)))));
            this.tsbMySlidesPlay.Image = global::Client.Properties.Resources.PlayGreenPNG;
            this.tsbMySlidesPlay.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbMySlidesPlay.Name = "tsbMySlidesPlay";
            this.tsbMySlidesPlay.Size = new System.Drawing.Size(28, 28);
            this.tsbMySlidesPlay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.tsbMySlidesPlay.ToolTipText = "Play My Slides";
            this.tsbMySlidesPlay.Click += new System.EventHandler(this.tsbMySlidesPlay_Click);
            // 
            // tsbMySlidesExportPpt
            // 
            this.tsbMySlidesExportPpt.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(79)))), ((int)(((byte)(46)))));
            this.tsbMySlidesExportPpt.Image = global::Client.Properties.Resources.ExportGreenPNG;
            this.tsbMySlidesExportPpt.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbMySlidesExportPpt.Name = "tsbMySlidesExportPpt";
            this.tsbMySlidesExportPpt.Size = new System.Drawing.Size(28, 28);
            this.tsbMySlidesExportPpt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.tsbMySlidesExportPpt.ToolTipText = "Export to PowerPoint";
            this.tsbMySlidesExportPpt.Click += new System.EventHandler(this.tsbMySlidesExportPpt_Click);
            // 
            // tsbMySlidesPrint
            // 
            this.tsbMySlidesPrint.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(79)))), ((int)(((byte)(46)))));
            this.tsbMySlidesPrint.Image = global::Client.Properties.Resources.PrintGreenPNG;
            this.tsbMySlidesPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbMySlidesPrint.Name = "tsbMySlidesPrint";
            this.tsbMySlidesPrint.Size = new System.Drawing.Size(28, 28);
            this.tsbMySlidesPrint.ToolTipText = "Print My Slides";
            this.tsbMySlidesPrint.Click += new System.EventHandler(this.tsbMySlidesPrint_Click);
            // 
            // tsbMySlidesSelectAll
            // 
            this.tsbMySlidesSelectAll.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(79)))), ((int)(((byte)(46)))));
            this.tsbMySlidesSelectAll.Image = global::Client.Properties.Resources.SelectAllGreenPNG;
            this.tsbMySlidesSelectAll.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbMySlidesSelectAll.Name = "tsbMySlidesSelectAll";
            this.tsbMySlidesSelectAll.Size = new System.Drawing.Size(28, 28);
            this.tsbMySlidesSelectAll.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.tsbMySlidesSelectAll.ToolTipText = "Select All";
            this.tsbMySlidesSelectAll.Click += new System.EventHandler(this.tsbMySlidesSelectAll_Click);
            // 
            // splForm
            // 
            this.splForm.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splForm.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.splForm.Location = new System.Drawing.Point(0, 24);
            this.splForm.Name = "splForm";
            // 
            // splForm.Panel1
            // 
            this.splForm.Panel1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.splForm.Panel1.Controls.Add(this.navigationPaneLeftPane);
            // 
            // splForm.Panel2
            // 
            this.splForm.Panel2.Controls.Add(this.splViewers);
            this.splForm.Size = new System.Drawing.Size(992, 667);
            this.splForm.SplitterDistance = 201;
            this.splForm.SplitterWidth = 5;
            this.splForm.TabIndex = 1;
            this.splForm.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.splForm_SplitterMoved);
            // 
            // navigationPaneLeftPane
            // 
            this.navigationPaneLeftPane.AllowAddOrRemove = false;
            this.navigationPaneLeftPane.ButtonActiveGradientHighColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(225)))), ((int)(((byte)(155)))));
            this.navigationPaneLeftPane.ButtonActiveGradientLowColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(165)))), ((int)(((byte)(78)))));
            this.navigationPaneLeftPane.ButtonBorderColor = System.Drawing.SystemColors.MenuHighlight;
            this.navigationPaneLeftPane.ButtonFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.navigationPaneLeftPane.ButtonForeColor = System.Drawing.SystemColors.ControlText;
            this.navigationPaneLeftPane.ButtonGradientHighColor = System.Drawing.SystemColors.ButtonHighlight;
            this.navigationPaneLeftPane.ButtonGradientLowColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.navigationPaneLeftPane.ButtonHighlightGradientHighColor = System.Drawing.Color.White;
            this.navigationPaneLeftPane.ButtonHighlightGradientLowColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(165)))), ((int)(((byte)(78)))));
            this.navigationPaneLeftPane.CaptionBorderColor = System.Drawing.SystemColors.ActiveCaption;
            this.navigationPaneLeftPane.CaptionFont = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold);
            this.navigationPaneLeftPane.CaptionForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.navigationPaneLeftPane.CaptionGradientHighColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.navigationPaneLeftPane.CaptionGradientLowColor = System.Drawing.SystemColors.ActiveCaption;
            this.navigationPaneLeftPane.Controls.Add(this.navigationPanePageSlideLibrary);
            this.navigationPaneLeftPane.Controls.Add(this.navigationPanePageMyPresentations);
            this.navigationPaneLeftPane.Controls.Add(this.navigationPanePageAdvancedSearch);
            this.navigationPaneLeftPane.Controls.Add(this.navigationPanePageFilters);
            this.navigationPaneLeftPane.Cursor = System.Windows.Forms.Cursors.Default;
            this.navigationPaneLeftPane.Dock = System.Windows.Forms.DockStyle.Fill;
            this.navigationPaneLeftPane.FooterGradientHighColor = System.Drawing.SystemColors.ButtonHighlight;
            this.navigationPaneLeftPane.FooterGradientLowColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.navigationPaneLeftPane.FooterHeight = 30;
            this.navigationPaneLeftPane.FooterHighlightGradientHighColor = System.Drawing.Color.White;
            this.navigationPaneLeftPane.FooterHighlightGradientLowColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(165)))), ((int)(((byte)(78)))));
            this.navigationPaneLeftPane.ImageInCaption = true;
            this.navigationPaneLeftPane.Location = new System.Drawing.Point(0, 0);
            this.navigationPaneLeftPane.Name = "navigationPaneLeftPane";
            this.navigationPaneLeftPane.NavigationPages.AddRange(new Ascend.Windows.Forms.NavigationPanePage[] {
            this.navigationPanePageSlideLibrary,
            this.navigationPanePageMyPresentations,
            this.navigationPanePageAdvancedSearch,
            this.navigationPanePageFilters});
            this.navigationPaneLeftPane.Size = new System.Drawing.Size(201, 667);
            this.navigationPaneLeftPane.TabIndex = 5;
            this.navigationPaneLeftPane.VisibleButtonCount = 4;
            // 
            // navigationPanePageSlideLibrary
            // 
            this.navigationPanePageSlideLibrary.ActiveGradientHighColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(225)))), ((int)(((byte)(155)))));
            this.navigationPanePageSlideLibrary.ActiveGradientLowColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(165)))), ((int)(((byte)(78)))));
            this.navigationPanePageSlideLibrary.AutoScroll = true;
            this.navigationPanePageSlideLibrary.ButtonFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.navigationPanePageSlideLibrary.ButtonForeColor = System.Drawing.SystemColors.ControlText;
            this.navigationPanePageSlideLibrary.Controls.Add(this.tabRegionLanguageTrees);
            this.navigationPanePageSlideLibrary.GradientHighColor = System.Drawing.SystemColors.ButtonHighlight;
            this.navigationPanePageSlideLibrary.GradientLowColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.navigationPanePageSlideLibrary.HighlightGradientHighColor = System.Drawing.Color.White;
            this.navigationPanePageSlideLibrary.HighlightGradientLowColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(165)))), ((int)(((byte)(78)))));
            this.navigationPanePageSlideLibrary.Image = global::Client.Properties.Resources.SlideLibraryPNG;
            this.navigationPanePageSlideLibrary.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.navigationPanePageSlideLibrary.ImageFooter = null;
            this.navigationPanePageSlideLibrary.ImageIndex = -1;
            this.navigationPanePageSlideLibrary.ImageIndexFooter = -1;
            this.navigationPanePageSlideLibrary.ImageKey = "";
            this.navigationPanePageSlideLibrary.ImageKeyFooter = "";
            this.navigationPanePageSlideLibrary.ImageList = null;
            this.navigationPanePageSlideLibrary.ImageListFooter = null;
            this.navigationPanePageSlideLibrary.Key = "SlideLibrary";
            this.navigationPanePageSlideLibrary.Location = new System.Drawing.Point(1, 27);
            this.navigationPanePageSlideLibrary.Name = "navigationPanePageSlideLibrary";
            this.navigationPanePageSlideLibrary.Size = new System.Drawing.Size(199, 474);
            this.navigationPanePageSlideLibrary.TabIndex = 1;
            this.navigationPanePageSlideLibrary.Text = "Slide Library";
            this.navigationPanePageSlideLibrary.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.navigationPanePageSlideLibrary.ToolTipText = null;
            // 
            // tabRegionLanguageTrees
            // 
            this.tabRegionLanguageTrees.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabRegionLanguageTrees.Location = new System.Drawing.Point(0, 0);
            this.tabRegionLanguageTrees.Name = "tabRegionLanguageTrees";
            this.tabRegionLanguageTrees.SelectedIndex = 0;
            this.tabRegionLanguageTrees.Size = new System.Drawing.Size(199, 474);
            this.tabRegionLanguageTrees.TabIndex = 7;
            this.tabRegionLanguageTrees.SelectedIndexChanged += new System.EventHandler(this.tabRegionLanguageTrees_SelectedIndexChanged);
            // 
            // navigationPanePageMyPresentations
            // 
            this.navigationPanePageMyPresentations.ActiveGradientHighColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(225)))), ((int)(((byte)(155)))));
            this.navigationPanePageMyPresentations.ActiveGradientLowColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(165)))), ((int)(((byte)(78)))));
            this.navigationPanePageMyPresentations.AutoScroll = true;
            this.navigationPanePageMyPresentations.ButtonFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.navigationPanePageMyPresentations.ButtonForeColor = System.Drawing.SystemColors.ControlText;
            this.navigationPanePageMyPresentations.Controls.Add(this.tvwMySlideDecks);
            this.navigationPanePageMyPresentations.Controls.Add(this.categorySlideTreeViewPresentations);
            this.navigationPanePageMyPresentations.GradientHighColor = System.Drawing.SystemColors.ButtonHighlight;
            this.navigationPanePageMyPresentations.GradientLowColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.navigationPanePageMyPresentations.HighlightGradientHighColor = System.Drawing.Color.White;
            this.navigationPanePageMyPresentations.HighlightGradientLowColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(165)))), ((int)(((byte)(78)))));
            this.navigationPanePageMyPresentations.Image = global::Client.Properties.Resources.MyPresentationsPNG;
            this.navigationPanePageMyPresentations.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.navigationPanePageMyPresentations.ImageFooter = null;
            this.navigationPanePageMyPresentations.ImageIndex = -1;
            this.navigationPanePageMyPresentations.ImageIndexFooter = -1;
            this.navigationPanePageMyPresentations.ImageKey = "";
            this.navigationPanePageMyPresentations.ImageKeyFooter = "";
            this.navigationPanePageMyPresentations.ImageList = null;
            this.navigationPanePageMyPresentations.ImageListFooter = null;
            this.navigationPanePageMyPresentations.Key = "MyPresentations";
            this.navigationPanePageMyPresentations.Location = new System.Drawing.Point(1, 27);
            this.navigationPanePageMyPresentations.Name = "navigationPanePageMyPresentations";
            this.navigationPanePageMyPresentations.Size = new System.Drawing.Size(199, 474);
            this.navigationPanePageMyPresentations.TabIndex = 4;
            this.navigationPanePageMyPresentations.Text = "My Presentations";
            this.navigationPanePageMyPresentations.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.navigationPanePageMyPresentations.ToolTipText = null;
            // 
            // tvwMySlideDecks
            // 
            this.tvwMySlideDecks.ContextMenuStrip = this.cmnMyPresentations;
            this.tvwMySlideDecks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tvwMySlideDecks.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tvwMySlideDecks.HideSelection = false;
            this.tvwMySlideDecks.ImageKey = "my-slide-decks.ico";
            this.tvwMySlideDecks.ImageList = this.imlIcons16;
            this.tvwMySlideDecks.Location = new System.Drawing.Point(0, 0);
            this.tvwMySlideDecks.Name = "tvwMySlideDecks";
            this.tvwMySlideDecks.SelectedImageKey = "my-slide-decks.ico";
            this.tvwMySlideDecks.Size = new System.Drawing.Size(199, 474);
            this.tvwMySlideDecks.TabIndex = 1;
            this.tvwMySlideDecks.AfterExpand += new System.Windows.Forms.TreeViewEventHandler(this.tvwMySlideDecks_AfterExpand);
            this.tvwMySlideDecks.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvwMySlideDecks_AfterSelect);
            // 
            // cmnMyPresentations
            // 
            this.cmnMyPresentations.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miMyPresentationsDelete});
            this.cmnMyPresentations.Name = "cmnMyPresentations";
            this.cmnMyPresentations.Size = new System.Drawing.Size(177, 26);
            this.cmnMyPresentations.Opened += new System.EventHandler(this.cmnMyPresentations_Opened);
            // 
            // miMyPresentationsDelete
            // 
            this.miMyPresentationsDelete.Image = global::Client.Properties.Resources.RemoveRedPNG;
            this.miMyPresentationsDelete.Name = "miMyPresentationsDelete";
            this.miMyPresentationsDelete.Size = new System.Drawing.Size(176, 22);
            this.miMyPresentationsDelete.Text = "Delete Presentation";
            this.miMyPresentationsDelete.Click += new System.EventHandler(this.miMyPresentationsDelete_Click);
            // 
            // imlIcons16
            // 
            this.imlIcons16.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imlIcons16.ImageStream")));
            this.imlIcons16.TransparentColor = System.Drawing.Color.Transparent;
            this.imlIcons16.Images.SetKeyName(0, "color-printing.ico");
            this.imlIcons16.Images.SetKeyName(1, "digital-imaging.ico");
            this.imlIcons16.Images.SetKeyName(2, "large-format.ico");
            this.imlIcons16.Images.SetKeyName(3, "MFP-AIO.ico");
            this.imlIcons16.Images.SetKeyName(4, "mono-printing.ico");
            this.imlIcons16.Images.SetKeyName(5, "multi-slides.ico");
            this.imlIcons16.Images.SetKeyName(6, "my-slide-decks.ico");
            this.imlIcons16.Images.SetKeyName(7, "single-slide.ico");
            this.imlIcons16.Images.SetKeyName(8, "Folder.ico");
            this.imlIcons16.Images.SetKeyName(9, "Slide-Folder.ico");
            this.imlIcons16.Images.SetKeyName(10, "Multi-Slide-Folder.ico");
            this.imlIcons16.Images.SetKeyName(11, "my-presentations.ico");
            this.imlIcons16.Images.SetKeyName(12, "slide-library.ico");
            this.imlIcons16.Images.SetKeyName(13, "X-rd.ico");
            this.imlIcons16.Images.SetKeyName(14, "arrow_L.ico");
            this.imlIcons16.Images.SetKeyName(15, "arrow_R.ico");
            this.imlIcons16.Images.SetKeyName(16, "tiny_folder_red.ico");
            this.imlIcons16.Images.SetKeyName(17, "tiny_folder_brn.ico");
            this.imlIcons16.Images.SetKeyName(18, "tiny_folder_drk_gry.ico");
            this.imlIcons16.Images.SetKeyName(19, "tiny_folder_drk-blu.ico");
            this.imlIcons16.Images.SetKeyName(20, "tiny_folder_drk-grn.ico");
            this.imlIcons16.Images.SetKeyName(21, "tiny_folder_lgt_gry.ico");
            this.imlIcons16.Images.SetKeyName(22, "tiny_folder_lgt-blu.ico");
            this.imlIcons16.Images.SetKeyName(23, "tiny_folder_lgt-grn.ico");
            this.imlIcons16.Images.SetKeyName(24, "tiny_folder_magenta.ico");
            this.imlIcons16.Images.SetKeyName(25, "tiny_folder_med-blu.ico");
            this.imlIcons16.Images.SetKeyName(26, "tiny_folder_orng.ico");
            this.imlIcons16.Images.SetKeyName(27, "tiny_folder_ylo.ico");
            this.imlIcons16.Images.SetKeyName(28, "tiny_folder_298527.ico");
            this.imlIcons16.Images.SetKeyName(29, "tiny_folder_246147.ico");
            this.imlIcons16.Images.SetKeyName(30, "ylw_242_171_1.ico");
            this.imlIcons16.Images.SetKeyName(31, "brwn_162_60_6.ico");
            this.imlIcons16.Images.SetKeyName(32, "drkblu_83_100_203.ico");
            this.imlIcons16.Images.SetKeyName(33, "drkgrn_36_97_71.ico");
            this.imlIcons16.Images.SetKeyName(34, "ltblu_0_152_246.ico");
            this.imlIcons16.Images.SetKeyName(35, "ltgrn_100_185_0.ico");
            this.imlIcons16.Images.SetKeyName(36, "mdgrn_41_133_39.ico");
            this.imlIcons16.Images.SetKeyName(37, "mdgry_123_123_121.ico");
            this.imlIcons16.Images.SetKeyName(38, "medblu_0_113_180.ico");
            this.imlIcons16.Images.SetKeyName(39, "mgnta_204_0_102.ico");
            this.imlIcons16.Images.SetKeyName(40, "orng_235_95_1.ico");
            this.imlIcons16.Images.SetKeyName(41, "red_176_28_46.ico");
            this.imlIcons16.Images.SetKeyName(42, "pdf-icon.gif");
            this.imlIcons16.Images.SetKeyName(43, "doc_icon_16x16.gif");
            // 
            // categorySlideTreeViewPresentations
            // 
            this.categorySlideTreeViewPresentations.Dock = System.Windows.Forms.DockStyle.Fill;
            this.categorySlideTreeViewPresentations.EntryImage = null;
            this.categorySlideTreeViewPresentations.HideSelection = false;
            this.categorySlideTreeViewPresentations.Location = new System.Drawing.Point(0, 0);
            this.categorySlideTreeViewPresentations.Name = "categorySlideTreeViewPresentations";
            this.categorySlideTreeViewPresentations.RightClickSelectsNode = false;
            this.categorySlideTreeViewPresentations.Size = new System.Drawing.Size(199, 474);
            this.categorySlideTreeViewPresentations.TabIndex = 0;
            // 
            // navigationPanePageAdvancedSearch
            // 
            this.navigationPanePageAdvancedSearch.ActiveGradientHighColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(225)))), ((int)(((byte)(155)))));
            this.navigationPanePageAdvancedSearch.ActiveGradientLowColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(165)))), ((int)(((byte)(78)))));
            this.navigationPanePageAdvancedSearch.AutoScroll = true;
            this.navigationPanePageAdvancedSearch.ButtonFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.navigationPanePageAdvancedSearch.ButtonForeColor = System.Drawing.SystemColors.ControlText;
            this.navigationPanePageAdvancedSearch.Controls.Add(this.lblSearchString);
            this.navigationPanePageAdvancedSearch.Controls.Add(this.txtSearchBox);
            this.navigationPanePageAdvancedSearch.Controls.Add(this.cbAdvancedSearchDate);
            this.navigationPanePageAdvancedSearch.Controls.Add(this.lblFrom);
            this.navigationPanePageAdvancedSearch.Controls.Add(this.dtFrom);
            this.navigationPanePageAdvancedSearch.Controls.Add(this.lblTo);
            this.navigationPanePageAdvancedSearch.Controls.Add(this.dtTo);
            this.navigationPanePageAdvancedSearch.Controls.Add(this.cbAdvancedSearchCategories);
            this.navigationPanePageAdvancedSearch.Controls.Add(this.cbCategories);
            this.navigationPanePageAdvancedSearch.Controls.Add(this.btnAdvancedSearchSubmit);
            this.navigationPanePageAdvancedSearch.Controls.Add(this.btnASearchFAQ);
            this.navigationPanePageAdvancedSearch.GradientHighColor = System.Drawing.SystemColors.ButtonHighlight;
            this.navigationPanePageAdvancedSearch.GradientLowColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.navigationPanePageAdvancedSearch.HighlightGradientHighColor = System.Drawing.Color.White;
            this.navigationPanePageAdvancedSearch.HighlightGradientLowColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(165)))), ((int)(((byte)(78)))));
            this.navigationPanePageAdvancedSearch.Image = global::Client.Properties.Resources.Advanced_SearchPNG;
            this.navigationPanePageAdvancedSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.navigationPanePageAdvancedSearch.ImageFooter = null;
            this.navigationPanePageAdvancedSearch.ImageIndex = -1;
            this.navigationPanePageAdvancedSearch.ImageIndexFooter = -1;
            this.navigationPanePageAdvancedSearch.ImageKey = "";
            this.navigationPanePageAdvancedSearch.ImageKeyFooter = "";
            this.navigationPanePageAdvancedSearch.ImageList = null;
            this.navigationPanePageAdvancedSearch.ImageListFooter = null;
            this.navigationPanePageAdvancedSearch.Key = "navigationPanePageAdvancedSearch";
            this.navigationPanePageAdvancedSearch.Location = new System.Drawing.Point(1, 27);
            this.navigationPanePageAdvancedSearch.Name = "navigationPanePageAdvancedSearch";
            this.navigationPanePageAdvancedSearch.Size = new System.Drawing.Size(199, 474);
            this.navigationPanePageAdvancedSearch.TabIndex = 5;
            this.navigationPanePageAdvancedSearch.Text = "Advanced Search";
            this.navigationPanePageAdvancedSearch.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.navigationPanePageAdvancedSearch.ToolTipText = null;
            this.navigationPanePageAdvancedSearch.Visible = false;
            // 
            // lblSearchString
            // 
            this.lblSearchString.AutoSize = true;
            this.lblSearchString.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblSearchString.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSearchString.Location = new System.Drawing.Point(21, 3);
            this.lblSearchString.Name = "lblSearchString";
            this.lblSearchString.Size = new System.Drawing.Size(80, 13);
            this.lblSearchString.TabIndex = 9;
            this.lblSearchString.Text = "Search Text:";
            // 
            // txtSearchBox
            // 
            this.txtSearchBox.Location = new System.Drawing.Point(23, 19);
            this.txtSearchBox.Name = "txtSearchBox";
            this.txtSearchBox.Size = new System.Drawing.Size(171, 20);
            this.txtSearchBox.TabIndex = 8;
            // 
            // cbAdvancedSearchDate
            // 
            this.cbAdvancedSearchDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbAdvancedSearchDate.Location = new System.Drawing.Point(0, 41);
            this.cbAdvancedSearchDate.Margin = new System.Windows.Forms.Padding(0, 0, 0, 15);
            this.cbAdvancedSearchDate.Name = "cbAdvancedSearchDate";
            this.cbAdvancedSearchDate.Padding = new System.Windows.Forms.Padding(25, 0, 0, 0);
            this.cbAdvancedSearchDate.Size = new System.Drawing.Size(199, 24);
            this.cbAdvancedSearchDate.TabIndex = 2;
            this.cbAdvancedSearchDate.Text = "Filter By Date";
            this.cbAdvancedSearchDate.CheckedChanged += new System.EventHandler(this.cbAdvancedSearchDate_CheckedChanged);
            // 
            // lblFrom
            // 
            this.lblFrom.AutoSize = true;
            this.lblFrom.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblFrom.Enabled = false;
            this.lblFrom.Location = new System.Drawing.Point(55, 68);
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.Size = new System.Drawing.Size(33, 13);
            this.lblFrom.TabIndex = 7;
            this.lblFrom.Text = "From:";
            // 
            // dtFrom
            // 
            this.dtFrom.Enabled = false;
            this.dtFrom.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtFrom.Location = new System.Drawing.Point(90, 65);
            this.dtFrom.Name = "dtFrom";
            this.dtFrom.Size = new System.Drawing.Size(103, 20);
            this.dtFrom.TabIndex = 6;
            // 
            // lblTo
            // 
            this.lblTo.AutoSize = true;
            this.lblTo.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblTo.Enabled = false;
            this.lblTo.Location = new System.Drawing.Point(64, 93);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(23, 13);
            this.lblTo.TabIndex = 9;
            this.lblTo.Text = "To:";
            // 
            // dtTo
            // 
            this.dtTo.Enabled = false;
            this.dtTo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtTo.Location = new System.Drawing.Point(90, 90);
            this.dtTo.Name = "dtTo";
            this.dtTo.Size = new System.Drawing.Size(103, 20);
            this.dtTo.TabIndex = 8;
            // 
            // cbAdvancedSearchCategories
            // 
            this.cbAdvancedSearchCategories.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.cbAdvancedSearchCategories.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbAdvancedSearchCategories.Location = new System.Drawing.Point(1, 112);
            this.cbAdvancedSearchCategories.Margin = new System.Windows.Forms.Padding(0, 0, 0, 15);
            this.cbAdvancedSearchCategories.Name = "cbAdvancedSearchCategories";
            this.cbAdvancedSearchCategories.Padding = new System.Windows.Forms.Padding(25, 0, 0, 0);
            this.cbAdvancedSearchCategories.Size = new System.Drawing.Size(199, 24);
            this.cbAdvancedSearchCategories.TabIndex = 6;
            this.cbAdvancedSearchCategories.Text = "Filter By Category";
            this.cbAdvancedSearchCategories.UseVisualStyleBackColor = false;
            this.cbAdvancedSearchCategories.CheckedChanged += new System.EventHandler(this.cbAdvancedSearchCategories_CheckedChanged);
            // 
            // cbCategories
            // 
            this.cbCategories.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.cbCategories.CheckOnClick = true;
            this.cbCategories.Enabled = false;
            this.cbCategories.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbCategories.Items.AddRange(new object[] {
            "Color",
            "Black/White",
            "Fax",
            "Help"});
            this.cbCategories.Location = new System.Drawing.Point(35, 143);
            this.cbCategories.Name = "cbCategories";
            this.cbCategories.Size = new System.Drawing.Size(158, 260);
            this.cbCategories.TabIndex = 7;
            // 
            // btnAdvancedSearchSubmit
            // 
            this.btnAdvancedSearchSubmit.AutoSize = true;
            this.btnAdvancedSearchSubmit.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnAdvancedSearchSubmit.Image = global::Client.Properties.Resources.Advanced_Search_blue;
            this.btnAdvancedSearchSubmit.Location = new System.Drawing.Point(49, 424);
            this.btnAdvancedSearchSubmit.Margin = new System.Windows.Forms.Padding(1);
            this.btnAdvancedSearchSubmit.Name = "btnAdvancedSearchSubmit";
            this.btnAdvancedSearchSubmit.Padding = new System.Windows.Forms.Padding(1);
            this.btnAdvancedSearchSubmit.Size = new System.Drawing.Size(100, 32);
            this.btnAdvancedSearchSubmit.TabIndex = 3;
            this.btnAdvancedSearchSubmit.Text = "Search";
            this.btnAdvancedSearchSubmit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAdvancedSearchSubmit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAdvancedSearchSubmit.UseVisualStyleBackColor = false;
            this.btnAdvancedSearchSubmit.Click += new System.EventHandler(this.btnAdvancedSearchSubmit_Click);
            // 
            // btnASearchFAQ
            // 
            this.btnASearchFAQ.BackColor = System.Drawing.Color.White;
            this.btnASearchFAQ.BackgroundImage = global::Client.Properties.Resources.HelpGreyPNG;
            this.btnASearchFAQ.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnASearchFAQ.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnASearchFAQ.FlatAppearance.BorderSize = 0;
            this.btnASearchFAQ.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnASearchFAQ.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btnASearchFAQ.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnASearchFAQ.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnASearchFAQ.Location = new System.Drawing.Point(150, 430);
            this.btnASearchFAQ.Margin = new System.Windows.Forms.Padding(0);
            this.btnASearchFAQ.Name = "btnASearchFAQ";
            this.btnASearchFAQ.Size = new System.Drawing.Size(25, 21);
            this.btnASearchFAQ.TabIndex = 7;
            this.btnASearchFAQ.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnASearchFAQ.UseVisualStyleBackColor = false;
            this.btnASearchFAQ.Click += new System.EventHandler(this.btnASearchFAQ_Click);
            // 
            // navigationPanePageFilters
            // 
            this.navigationPanePageFilters.ActiveGradientHighColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(225)))), ((int)(((byte)(155)))));
            this.navigationPanePageFilters.ActiveGradientLowColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(165)))), ((int)(((byte)(78)))));
            this.navigationPanePageFilters.AutoScroll = true;
            this.navigationPanePageFilters.ButtonFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.navigationPanePageFilters.ButtonForeColor = System.Drawing.SystemColors.ControlText;
            this.navigationPanePageFilters.Controls.Add(this.tabFilterGroups);
            this.navigationPanePageFilters.Controls.Add(this.gradientPanel1);
            this.navigationPanePageFilters.Controls.Add(this.gradientCaption4);
            this.navigationPanePageFilters.GradientHighColor = System.Drawing.SystemColors.ButtonHighlight;
            this.navigationPanePageFilters.GradientLowColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.navigationPanePageFilters.HighlightGradientHighColor = System.Drawing.Color.White;
            this.navigationPanePageFilters.HighlightGradientLowColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(165)))), ((int)(((byte)(78)))));
            this.navigationPanePageFilters.Image = global::Client.Properties.Resources.FilterPNG;
            this.navigationPanePageFilters.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.navigationPanePageFilters.ImageFooter = null;
            this.navigationPanePageFilters.ImageIndex = -1;
            this.navigationPanePageFilters.ImageIndexFooter = -1;
            this.navigationPanePageFilters.ImageKey = "";
            this.navigationPanePageFilters.ImageKeyFooter = "";
            this.navigationPanePageFilters.ImageList = null;
            this.navigationPanePageFilters.ImageListFooter = null;
            this.navigationPanePageFilters.Key = "Filters";
            this.navigationPanePageFilters.Location = new System.Drawing.Point(1, 27);
            this.navigationPanePageFilters.Name = "navigationPanePageFilters";
            this.navigationPanePageFilters.Size = new System.Drawing.Size(199, 474);
            this.navigationPanePageFilters.TabIndex = 3;
            this.navigationPanePageFilters.Text = "Filters";
            this.navigationPanePageFilters.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.navigationPanePageFilters.ToolTipText = null;
            // 
            // tabFilterGroups
            // 
            this.tabFilterGroups.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabFilterGroups.Location = new System.Drawing.Point(2, 2);
            this.tabFilterGroups.Name = "tabFilterGroups";
            this.tabFilterGroups.SelectedIndex = 0;
            this.tabFilterGroups.Size = new System.Drawing.Size(197, 394);
            this.tabFilterGroups.TabIndex = 15;
            // 
            // gradientPanel1
            // 
            this.gradientPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gradientPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.gradientPanel1.Controls.Add(this.btnApplyFilterSettings);
            this.gradientPanel1.Location = new System.Drawing.Point(0, 419);
            this.gradientPanel1.Name = "gradientPanel1";
            this.gradientPanel1.Size = new System.Drawing.Size(199, 55);
            this.gradientPanel1.TabIndex = 14;
            // 
            // btnApplyFilterSettings
            // 
            this.btnApplyFilterSettings.Location = new System.Drawing.Point(11, 15);
            this.btnApplyFilterSettings.Name = "btnApplyFilterSettings";
            this.btnApplyFilterSettings.Size = new System.Drawing.Size(181, 23);
            this.btnApplyFilterSettings.TabIndex = 13;
            this.btnApplyFilterSettings.Text = "Apply Filter Settings";
            this.btnApplyFilterSettings.UseVisualStyleBackColor = true;
            this.btnApplyFilterSettings.Click += new System.EventHandler(this.btnApplyFilterSettings_Click);
            // 
            // gradientCaption4
            // 
            this.gradientCaption4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gradientCaption4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.gradientCaption4.Location = new System.Drawing.Point(0, 395);
            this.gradientCaption4.Name = "gradientCaption4";
            this.gradientCaption4.Size = new System.Drawing.Size(201, 24);
            this.gradientCaption4.TabIndex = 11;
            this.gradientCaption4.Text = "Actions";
            // 
            // splViewers
            // 
            this.splViewers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splViewers.Location = new System.Drawing.Point(0, 0);
            this.splViewers.Name = "splViewers";
            this.splViewers.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splViewers.Panel1
            // 
            this.splViewers.Panel1.Controls.Add(this.splPreview);
            // 
            // splViewers.Panel2
            // 
            this.splViewers.Panel2.Controls.Add(this.splViewerBottom);
            this.splViewers.Size = new System.Drawing.Size(786, 667);
            this.splViewers.SplitterDistance = 325;
            this.splViewers.SplitterWidth = 5;
            this.splViewers.TabIndex = 0;
            // 
            // splPreview
            // 
            this.splPreview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splPreview.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splPreview.Location = new System.Drawing.Point(0, 0);
            this.splPreview.Name = "splPreview";
            this.splPreview.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splPreview.Panel1
            // 
            this.splPreview.Panel1.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.splPreview.Panel1.Controls.Add(this.btnregistryFixClose);
            this.splPreview.Panel1.Controls.Add(this.llRegistryFix);
            this.splPreview.Panel1.Controls.Add(this.btnSearchFAQ);
            this.splPreview.Panel1.Controls.Add(this.pbIndex);
            this.splPreview.Panel1.Controls.Add(this.txtSearch);
            this.splPreview.Panel1.Controls.Add(this.lblIndex);
            this.splPreview.Panel1.Controls.Add(this.btnSearchIndex);
            this.splPreview.Panel1.Controls.Add(this.pictureBox2);
            this.splPreview.Panel1.Controls.Add(this.tsPreview);
            this.splPreview.Panel1.Controls.Add(this.lblPreview);
            // 
            // splPreview.Panel2
            // 
            this.splPreview.Panel2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.splPreview.Panel2.Controls.Add(this.splPreviewType);
            this.splPreview.Size = new System.Drawing.Size(786, 325);
            this.splPreview.SplitterDistance = 58;
            this.splPreview.SplitterWidth = 1;
            this.splPreview.TabIndex = 0;
            // 
            // btnregistryFixClose
            // 
            this.btnregistryFixClose.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.btnregistryFixClose.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.btnregistryFixClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnregistryFixClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnregistryFixClose.ForeColor = System.Drawing.Color.Red;
            this.btnregistryFixClose.Image = global::Client.Properties.Resources.red_x;
            this.btnregistryFixClose.Location = new System.Drawing.Point(743, 2);
            this.btnregistryFixClose.Margin = new System.Windows.Forms.Padding(0);
            this.btnregistryFixClose.Name = "btnregistryFixClose";
            this.btnregistryFixClose.Size = new System.Drawing.Size(12, 12);
            this.btnregistryFixClose.TabIndex = 7;
            this.toolTip1.SetToolTip(this.btnregistryFixClose, "Clicking the \'x\' will remove this help question.");
            this.btnregistryFixClose.UseVisualStyleBackColor = false;
            this.btnregistryFixClose.Click += new System.EventHandler(this.btnregistryFixClose_Click);
            // 
            // llRegistryFix
            // 
            this.llRegistryFix.AutoSize = true;
            this.llRegistryFix.ForeColor = System.Drawing.Color.Red;
            this.llRegistryFix.LinkColor = System.Drawing.Color.Red;
            this.llRegistryFix.Location = new System.Drawing.Point(559, 6);
            this.llRegistryFix.Name = "llRegistryFix";
            this.llRegistryFix.Size = new System.Drawing.Size(187, 13);
            this.llRegistryFix.TabIndex = 6;
            this.llRegistryFix.TabStop = true;
            this.llRegistryFix.Text = "Are you having trouble viewing slides?";
            this.llRegistryFix.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.llRegistryFix_LinkClicked);
            // 
            // btnSearchFAQ
            // 
            this.btnSearchFAQ.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnSearchFAQ.BackColor = System.Drawing.Color.White;
            this.btnSearchFAQ.BackgroundImage = global::Client.Properties.Resources.HelpGreyPNG;
            this.btnSearchFAQ.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnSearchFAQ.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnSearchFAQ.FlatAppearance.BorderSize = 0;
            this.btnSearchFAQ.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnSearchFAQ.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btnSearchFAQ.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearchFAQ.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearchFAQ.Location = new System.Drawing.Point(590, 33);
            this.btnSearchFAQ.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.btnSearchFAQ.Name = "btnSearchFAQ";
            this.btnSearchFAQ.Size = new System.Drawing.Size(25, 21);
            this.btnSearchFAQ.TabIndex = 5;
            this.btnSearchFAQ.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSearchFAQ.UseVisualStyleBackColor = false;
            this.btnSearchFAQ.Visible = false;
            this.btnSearchFAQ.Click += new System.EventHandler(this.btnSearchFAQ_Click);
            // 
            // pbIndex
            // 
            this.pbIndex.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.pbIndex.Location = new System.Drawing.Point(666, 33);
            this.pbIndex.Name = "pbIndex";
            this.pbIndex.Size = new System.Drawing.Size(117, 18);
            this.pbIndex.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.pbIndex.TabIndex = 1;
            this.pbIndex.Visible = false;
            // 
            // txtSearch
            // 
            this.txtSearch.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.txtSearch.Location = new System.Drawing.Point(618, 33);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(128, 20);
            this.txtSearch.TabIndex = 4;
            this.txtSearch.Visible = false;
            this.txtSearch.Enter += new System.EventHandler(this.txtSearch_Enter);
            this.txtSearch.Leave += new System.EventHandler(this.txtSearch_Leave);
            // 
            // lblIndex
            // 
            this.lblIndex.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblIndex.AutoSize = true;
            this.lblIndex.BackColor = System.Drawing.Color.White;
            this.lblIndex.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIndex.ForeColor = System.Drawing.Color.Black;
            this.lblIndex.Location = new System.Drawing.Point(615, 36);
            this.lblIndex.Name = "lblIndex";
            this.lblIndex.Size = new System.Drawing.Size(50, 13);
            this.lblIndex.TabIndex = 2;
            this.lblIndex.Text = "Indexing:";
            this.lblIndex.Visible = false;
            // 
            // btnSearchIndex
            // 
            this.btnSearchIndex.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnSearchIndex.BackColor = System.Drawing.Color.White;
            this.btnSearchIndex.BackgroundImage = global::Client.Properties.Resources.Search_Base_Button;
            this.btnSearchIndex.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnSearchIndex.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnSearchIndex.FlatAppearance.BorderSize = 0;
            this.btnSearchIndex.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnSearchIndex.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btnSearchIndex.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearchIndex.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearchIndex.Location = new System.Drawing.Point(747, 32);
            this.btnSearchIndex.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.btnSearchIndex.Name = "btnSearchIndex";
            this.btnSearchIndex.Size = new System.Drawing.Size(35, 21);
            this.btnSearchIndex.TabIndex = 3;
            this.btnSearchIndex.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSearchIndex.UseVisualStyleBackColor = false;
            this.btnSearchIndex.Visible = false;
            this.btnSearchIndex.Click += new System.EventHandler(this.btnSearchIndex_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Client.Properties.Resources.SlideLibraryPreviewPNG;
            this.pictureBox2.Location = new System.Drawing.Point(3, -1);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(24, 24);
            this.pictureBox2.TabIndex = 3;
            this.pictureBox2.TabStop = false;
            // 
            // lblPreview
            // 
            this.lblPreview.AutoSize = true;
            this.lblPreview.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPreview.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblPreview.Location = new System.Drawing.Point(27, 1);
            this.lblPreview.Margin = new System.Windows.Forms.Padding(1, 0, 3, 0);
            this.lblPreview.Name = "lblPreview";
            this.lblPreview.Size = new System.Drawing.Size(167, 20);
            this.lblPreview.TabIndex = 1;
            this.lblPreview.Text = "Slide Library Viewer";
            // 
            // splPreviewType
            // 
            this.splPreviewType.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.splPreviewType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splPreviewType.Location = new System.Drawing.Point(0, 0);
            this.splPreviewType.Name = "splPreviewType";
            // 
            // splPreviewType.Panel1
            // 
            this.splPreviewType.Panel1.Controls.Add(this.panel1);
            this.splPreviewType.Panel1.Controls.Add(this.lstPreview);
            // 
            // splPreviewType.Panel2
            // 
            this.splPreviewType.Panel2.Controls.Add(this.tsRegionStatus);
            this.splPreviewType.Panel2.Controls.Add(this.tsbSlideLibraryPreviewNav);
            this.splPreviewType.Panel2.Controls.Add(this.webSlidePreview);
            this.splPreviewType.Panel2.Controls.Add(this.picPptScrollHider);
            this.splPreviewType.Size = new System.Drawing.Size(786, 266);
            this.splPreviewType.SplitterDistance = 97;
            this.splPreviewType.SplitterWidth = 5;
            this.splPreviewType.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.pictureBox3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(97, 25);
            this.panel1.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(19, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Thumbnails";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::Client.Properties.Resources.SlideLibraryPNG;
            this.pictureBox3.Location = new System.Drawing.Point(4, 1);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(23, 19);
            this.pictureBox3.TabIndex = 4;
            this.pictureBox3.TabStop = false;
            // 
            // lstPreview
            // 
            this.lstPreview.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lstPreview.BackColor = System.Drawing.Color.White;
            this.lstPreview.ContextMenuStrip = this.cmnSlideLibraryPreview;
            this.lstPreview.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F);
            this.lstPreview.HideSelection = false;
            this.lstPreview.LargeImageList = this.imlTinyThumbs;
            this.lstPreview.Location = new System.Drawing.Point(0, 25);
            this.lstPreview.Name = "lstPreview";
            this.lstPreview.Size = new System.Drawing.Size(98, 241);
            this.lstPreview.TabIndex = 0;
            this.lstPreview.UseCompatibleStateImageBehavior = false;
            this.lstPreview.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.lstPreview_ItemDrag);
            this.lstPreview.ItemMouseHover += new System.Windows.Forms.ListViewItemMouseHoverEventHandler(this.lstPreview_ItemMouseHover);
            this.lstPreview.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.lstPreview_ItemSelectionChanged);
            // 
            // cmnSlideLibraryPreview
            // 
            this.cmnSlideLibraryPreview.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miSlideLibraryPreviewAddToMySlides,
            this.miSlideLibraryPreviewSelectAll});
            this.cmnSlideLibraryPreview.Name = "cmnSlideLibraryPreview";
            this.cmnSlideLibraryPreview.Size = new System.Drawing.Size(252, 48);
            // 
            // miSlideLibraryPreviewAddToMySlides
            // 
            this.miSlideLibraryPreviewAddToMySlides.Image = global::Client.Properties.Resources.AddBluePNG;
            this.miSlideLibraryPreviewAddToMySlides.Name = "miSlideLibraryPreviewAddToMySlides";
            this.miSlideLibraryPreviewAddToMySlides.Size = new System.Drawing.Size(251, 22);
            this.miSlideLibraryPreviewAddToMySlides.Text = "Add Selected Slide(s) to My Slides";
            this.miSlideLibraryPreviewAddToMySlides.Click += new System.EventHandler(this.miSlideLibraryPreviewAddToMySlides_Click);
            // 
            // miSlideLibraryPreviewSelectAll
            // 
            this.miSlideLibraryPreviewSelectAll.Image = global::Client.Properties.Resources.SelectAllBluePNG;
            this.miSlideLibraryPreviewSelectAll.Name = "miSlideLibraryPreviewSelectAll";
            this.miSlideLibraryPreviewSelectAll.Size = new System.Drawing.Size(251, 22);
            this.miSlideLibraryPreviewSelectAll.Text = "Select All";
            this.miSlideLibraryPreviewSelectAll.Click += new System.EventHandler(this.miSlideLibraryPreviewSelectAll_Click);
            // 
            // imlTinyThumbs
            // 
            this.imlTinyThumbs.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imlTinyThumbs.ImageSize = new System.Drawing.Size(40, 30);
            this.imlTinyThumbs.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // tsRegionStatus
            // 
            this.tsRegionStatus.AutoSize = false;
            this.tsRegionStatus.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tsRegionStatus.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsRegionStatus.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslRegionStatus});
            this.tsRegionStatus.Location = new System.Drawing.Point(0, 25);
            this.tsRegionStatus.Name = "tsRegionStatus";
            this.tsRegionStatus.Size = new System.Drawing.Size(684, 16);
            this.tsRegionStatus.Stretch = true;
            this.tsRegionStatus.TabIndex = 3;
            // 
            // tslRegionStatus
            // 
            this.tslRegionStatus.Name = "tslRegionStatus";
            this.tslRegionStatus.Size = new System.Drawing.Size(0, 13);
            // 
            // tsbSlideLibraryPreviewNav
            // 
            this.tsbSlideLibraryPreviewNav.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsbSlideLibraryPreviewNav.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbSlideLibraryPreviewHome,
            this.tsbSlideLibraryPreviewBack,
            this.tsbSlideLibraryPreviewForward,
            this.tslSlideLibraryPreviewSlideRegion});
            this.tsbSlideLibraryPreviewNav.Location = new System.Drawing.Point(0, 0);
            this.tsbSlideLibraryPreviewNav.Name = "tsbSlideLibraryPreviewNav";
            this.tsbSlideLibraryPreviewNav.Size = new System.Drawing.Size(684, 25);
            this.tsbSlideLibraryPreviewNav.TabIndex = 1;
            // 
            // tsbSlideLibraryPreviewHome
            // 
            this.tsbSlideLibraryPreviewHome.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbSlideLibraryPreviewHome.Image = global::Client.Properties.Resources.HomeBMP;
            this.tsbSlideLibraryPreviewHome.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbSlideLibraryPreviewHome.Name = "tsbSlideLibraryPreviewHome";
            this.tsbSlideLibraryPreviewHome.Size = new System.Drawing.Size(23, 22);
            this.tsbSlideLibraryPreviewHome.Click += new System.EventHandler(this.tsbSlideLibraryPreviewHome_Click);
            // 
            // tsbSlideLibraryPreviewBack
            // 
            this.tsbSlideLibraryPreviewBack.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbSlideLibraryPreviewBack.Image = global::Client.Properties.Resources.BackPNG;
            this.tsbSlideLibraryPreviewBack.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbSlideLibraryPreviewBack.Name = "tsbSlideLibraryPreviewBack";
            this.tsbSlideLibraryPreviewBack.Size = new System.Drawing.Size(23, 22);
            this.tsbSlideLibraryPreviewBack.ToolTipText = "Previous Slide";
            this.tsbSlideLibraryPreviewBack.Click += new System.EventHandler(this.tsbSlideLibraryPreviewBack_Click);
            // 
            // tsbSlideLibraryPreviewForward
            // 
            this.tsbSlideLibraryPreviewForward.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbSlideLibraryPreviewForward.Image = global::Client.Properties.Resources.ForwardPNG;
            this.tsbSlideLibraryPreviewForward.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbSlideLibraryPreviewForward.Name = "tsbSlideLibraryPreviewForward";
            this.tsbSlideLibraryPreviewForward.Size = new System.Drawing.Size(23, 22);
            this.tsbSlideLibraryPreviewForward.ToolTipText = "Next Slide";
            this.tsbSlideLibraryPreviewForward.Click += new System.EventHandler(this.tsbSlideLibraryPreviewForward_Click);
            // 
            // tslSlideLibraryPreviewSlideRegion
            // 
            this.tslSlideLibraryPreviewSlideRegion.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tslSlideLibraryPreviewSlideRegion.Name = "tslSlideLibraryPreviewSlideRegion";
            this.tslSlideLibraryPreviewSlideRegion.Size = new System.Drawing.Size(0, 22);
            // 
            // webSlidePreview
            // 
            this.webSlidePreview.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.webSlidePreview.Location = new System.Drawing.Point(0, 41);
            this.webSlidePreview.MinimumSize = new System.Drawing.Size(20, 20);
            this.webSlidePreview.Name = "webSlidePreview";
            this.webSlidePreview.Size = new System.Drawing.Size(681, 222);
            this.webSlidePreview.TabIndex = 0;
            this.webSlidePreview.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.webSlidePreview_DocumentCompleted);
            this.webSlidePreview.Navigating += new System.Windows.Forms.WebBrowserNavigatingEventHandler(this.webSlidePreview_Navigating);
            // 
            // picPptScrollHider
            // 
            this.picPptScrollHider.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.picPptScrollHider.BackColor = System.Drawing.Color.Black;
            this.picPptScrollHider.Location = new System.Drawing.Point(661, 41);
            this.picPptScrollHider.Name = "picPptScrollHider";
            this.picPptScrollHider.Size = new System.Drawing.Size(23, 225);
            this.picPptScrollHider.TabIndex = 2;
            this.picPptScrollHider.TabStop = false;
            this.picPptScrollHider.Visible = false;
            // 
            // splViewerBottom
            // 
            this.splViewerBottom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splViewerBottom.Location = new System.Drawing.Point(0, 0);
            this.splViewerBottom.Margin = new System.Windows.Forms.Padding(0);
            this.splViewerBottom.Name = "splViewerBottom";
            this.splViewerBottom.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splViewerBottom.Panel1
            // 
            this.splViewerBottom.Panel1.Controls.Add(this.splMySlides);
            // 
            // splViewerBottom.Panel2
            // 
            this.splViewerBottom.Panel2.Controls.Add(this.splSearch);
            this.splViewerBottom.Size = new System.Drawing.Size(786, 337);
            this.splViewerBottom.SplitterDistance = 168;
            this.splViewerBottom.SplitterWidth = 1;
            this.splViewerBottom.TabIndex = 1;
            // 
            // splMySlides
            // 
            this.splMySlides.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splMySlides.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splMySlides.Location = new System.Drawing.Point(0, 0);
            this.splMySlides.Name = "splMySlides";
            this.splMySlides.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splMySlides.Panel1
            // 
            this.splMySlides.Panel1.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.splMySlides.Panel1.Controls.Add(this.pictureBox1);
            this.splMySlides.Panel1.Controls.Add(this.lblMySlides);
            this.splMySlides.Panel1.Controls.Add(this.tsMySlides);
            // 
            // splMySlides.Panel2
            // 
            this.splMySlides.Panel2.Controls.Add(this.statusMySlides);
            this.splMySlides.Panel2.Controls.Add(this.lstMySlides);
            this.splMySlides.Size = new System.Drawing.Size(786, 168);
            this.splMySlides.SplitterDistance = 60;
            this.splMySlides.SplitterWidth = 1;
            this.splMySlides.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Client.Properties.Resources.MySlidesPNG;
            this.pictureBox1.Location = new System.Drawing.Point(4, -1);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(24, 24);
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // lblMySlides
            // 
            this.lblMySlides.AutoSize = true;
            this.lblMySlides.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMySlides.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblMySlides.Location = new System.Drawing.Point(28, 1);
            this.lblMySlides.Name = "lblMySlides";
            this.lblMySlides.Size = new System.Drawing.Size(85, 20);
            this.lblMySlides.TabIndex = 0;
            this.lblMySlides.Text = "My Slides";
            // 
            // statusMySlides
            // 
            this.statusMySlides.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.statusMySlides.Location = new System.Drawing.Point(0, 85);
            this.statusMySlides.Name = "statusMySlides";
            this.statusMySlides.Size = new System.Drawing.Size(786, 22);
            this.statusMySlides.SizingGrip = false;
            this.statusMySlides.TabIndex = 2;
            this.statusMySlides.Text = "statusStrip1";
            // 
            // lstMySlides
            // 
            this.lstMySlides.AllowDrop = true;
            this.lstMySlides.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lstMySlides.BackColor = System.Drawing.Color.White;
            this.lstMySlides.ContextMenuStrip = this.cmnMySlides;
            this.lstMySlides.HideSelection = false;
            this.lstMySlides.LargeImageList = this.imlSlideThumbs;
            this.lstMySlides.Location = new System.Drawing.Point(0, 0);
            this.lstMySlides.Name = "lstMySlides";
            this.lstMySlides.Size = new System.Drawing.Size(786, 82);
            this.lstMySlides.TabIndex = 1;
            this.lstMySlides.UseCompatibleStateImageBehavior = false;
            this.lstMySlides.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.lstMySlides_ItemDrag);
            this.lstMySlides.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.lstMySlides_ItemSelectionChanged);
            this.lstMySlides.DragDrop += new System.Windows.Forms.DragEventHandler(this.lstMySlides_DragDrop);
            this.lstMySlides.DragEnter += new System.Windows.Forms.DragEventHandler(this.lstMySlides_DragEnter);
            this.lstMySlides.DragOver += new System.Windows.Forms.DragEventHandler(this.lstMySlides_DragOver);
            this.lstMySlides.KeyUp += new System.Windows.Forms.KeyEventHandler(this.lstMySlides_KeyUp);
            // 
            // cmnMySlides
            // 
            this.cmnMySlides.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miMySlidesSave,
            this.miMySlidesSaveAs,
            this.miMySlidesRemove,
            this.miMySlidesClear,
            this.miMySlidesPlay,
            this.miMySlidesExportPpt,
            this.miMySlidesPrint,
            this.miMySlidesSelectAll});
            this.cmnMySlides.Name = "cmnMySlides";
            this.cmnMySlides.Size = new System.Drawing.Size(186, 180);
            // 
            // miMySlidesSave
            // 
            this.miMySlidesSave.Image = global::Client.Properties.Resources.SaveGreenPNG;
            this.miMySlidesSave.Name = "miMySlidesSave";
            this.miMySlidesSave.Size = new System.Drawing.Size(185, 22);
            this.miMySlidesSave.Text = "Save";
            this.miMySlidesSave.Click += new System.EventHandler(this.miMySlidesSave_Click);
            // 
            // miMySlidesSaveAs
            // 
            this.miMySlidesSaveAs.Image = global::Client.Properties.Resources.SaveAsGreenPNG;
            this.miMySlidesSaveAs.Name = "miMySlidesSaveAs";
            this.miMySlidesSaveAs.Size = new System.Drawing.Size(185, 22);
            this.miMySlidesSaveAs.Text = "Save As..";
            this.miMySlidesSaveAs.Click += new System.EventHandler(this.miMySlidesSaveAs_Click);
            // 
            // miMySlidesRemove
            // 
            this.miMySlidesRemove.Image = global::Client.Properties.Resources.RemoveGreenPNG;
            this.miMySlidesRemove.Name = "miMySlidesRemove";
            this.miMySlidesRemove.Size = new System.Drawing.Size(185, 22);
            this.miMySlidesRemove.Text = "Remove Slide(s)";
            this.miMySlidesRemove.Click += new System.EventHandler(this.miMySlidesRemove_Click);
            // 
            // miMySlidesClear
            // 
            this.miMySlidesClear.Image = global::Client.Properties.Resources.ClearAllGreenPNG;
            this.miMySlidesClear.Name = "miMySlidesClear";
            this.miMySlidesClear.Size = new System.Drawing.Size(185, 22);
            this.miMySlidesClear.Text = "Clear My Slides";
            this.miMySlidesClear.Click += new System.EventHandler(this.miMySlidesClear_Click);
            // 
            // miMySlidesPlay
            // 
            this.miMySlidesPlay.Image = global::Client.Properties.Resources.PlayGreenPNG;
            this.miMySlidesPlay.Name = "miMySlidesPlay";
            this.miMySlidesPlay.Size = new System.Drawing.Size(185, 22);
            this.miMySlidesPlay.Text = "Play My Slides";
            this.miMySlidesPlay.Click += new System.EventHandler(this.miMySlidesPlay_Click);
            // 
            // miMySlidesExportPpt
            // 
            this.miMySlidesExportPpt.Image = global::Client.Properties.Resources.ExportGreenPNG;
            this.miMySlidesExportPpt.Name = "miMySlidesExportPpt";
            this.miMySlidesExportPpt.Size = new System.Drawing.Size(185, 22);
            this.miMySlidesExportPpt.Text = "Export to PowerPoint";
            this.miMySlidesExportPpt.Click += new System.EventHandler(this.miMySlidesExportPpt_Click);
            // 
            // miMySlidesPrint
            // 
            this.miMySlidesPrint.Image = global::Client.Properties.Resources.PrintGreenPNG;
            this.miMySlidesPrint.Name = "miMySlidesPrint";
            this.miMySlidesPrint.Size = new System.Drawing.Size(185, 22);
            this.miMySlidesPrint.Text = "Print";
            this.miMySlidesPrint.Click += new System.EventHandler(this.miMySlidesPrint_Click);
            // 
            // miMySlidesSelectAll
            // 
            this.miMySlidesSelectAll.Image = global::Client.Properties.Resources.SelectAllGreenPNG;
            this.miMySlidesSelectAll.Name = "miMySlidesSelectAll";
            this.miMySlidesSelectAll.Size = new System.Drawing.Size(185, 22);
            this.miMySlidesSelectAll.Text = "Select All";
            this.miMySlidesSelectAll.Click += new System.EventHandler(this.miMySlidesSelectAll_Click);
            // 
            // imlSlideThumbs
            // 
            this.imlSlideThumbs.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imlSlideThumbs.ImageSize = new System.Drawing.Size(120, 90);
            this.imlSlideThumbs.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // splSearch
            // 
            this.splSearch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splSearch.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splSearch.Location = new System.Drawing.Point(0, 0);
            this.splSearch.Name = "splSearch";
            this.splSearch.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splSearch.Panel1
            // 
            this.splSearch.Panel1.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.splSearch.Panel1.Controls.Add(this.lblSearchResultsCount);
            this.splSearch.Panel1.Controls.Add(this.pictureBox4);
            this.splSearch.Panel1.Controls.Add(this.label2);
            // 
            // splSearch.Panel2
            // 
            this.splSearch.Panel2.Controls.Add(this.lvSearch);
            this.splSearch.Size = new System.Drawing.Size(786, 168);
            this.splSearch.SplitterDistance = 30;
            this.splSearch.SplitterWidth = 1;
            this.splSearch.TabIndex = 1;
            // 
            // lblSearchResultsCount
            // 
            this.lblSearchResultsCount.AutoSize = true;
            this.lblSearchResultsCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSearchResultsCount.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblSearchResultsCount.Location = new System.Drawing.Point(166, 1);
            this.lblSearchResultsCount.Name = "lblSearchResultsCount";
            this.lblSearchResultsCount.Size = new System.Drawing.Size(77, 17);
            this.lblSearchResultsCount.TabIndex = 3;
            this.lblSearchResultsCount.Text = "(0 Results)";
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::Client.Properties.Resources.Search_PNG;
            this.pictureBox4.Location = new System.Drawing.Point(4, -1);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(24, 24);
            this.pictureBox4.TabIndex = 2;
            this.pictureBox4.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label2.Location = new System.Drawing.Point(28, 1);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(132, 20);
            this.label2.TabIndex = 0;
            this.label2.Text = "Search Results";
            // 
            // lvSearch
            // 
            this.lvSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvSearch.BackColor = System.Drawing.Color.White;
            this.lvSearch.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Thumbnail,
            this.Title,
            this.RegionLang,
            this.Category,
            this.TopCategory,
            this.ModifiedDate});
            this.lvSearch.FullRowSelect = true;
            this.lvSearch.HideSelection = false;
            this.lvSearch.Location = new System.Drawing.Point(0, 0);
            this.lvSearch.MultiSelect = false;
            this.lvSearch.Name = "lvSearch";
            this.lvSearch.ShowGroups = false;
            this.lvSearch.Size = new System.Drawing.Size(786, 135);
            this.lvSearch.SmallImageList = this.imlIcons16;
            this.lvSearch.TabIndex = 1;
            this.lvSearch.UseCompatibleStateImageBehavior = false;
            this.lvSearch.View = System.Windows.Forms.View.Details;
            this.lvSearch.DoubleClick += new System.EventHandler(this.lvSearch_DoubleClick);
            // 
            // Thumbnail
            // 
            this.Thumbnail.Text = "";
            this.Thumbnail.Width = 50;
            // 
            // Title
            // 
            this.Title.Text = "Title";
            this.Title.Width = 200;
            // 
            // RegionLang
            // 
            this.RegionLang.Text = "Region/Language";
            this.RegionLang.Width = 100;
            // 
            // Category
            // 
            this.Category.Text = "Category";
            this.Category.Width = 150;
            // 
            // TopCategory
            // 
            this.TopCategory.Text = "Top Category";
            this.TopCategory.Width = 150;
            // 
            // ModifiedDate
            // 
            this.ModifiedDate.Text = "Modified Date";
            this.ModifiedDate.Width = 125;
            // 
            // statusMySlidesCount
            // 
            this.statusMySlidesCount.Name = "statusMySlidesCount";
            this.statusMySlidesCount.Size = new System.Drawing.Size(79, 17);
            this.statusMySlidesCount.Text = "# of My Slides:";
            // 
            // statusMySlidesQuantity
            // 
            this.statusMySlidesQuantity.Name = "statusMySlidesQuantity";
            this.statusMySlidesQuantity.Size = new System.Drawing.Size(0, 17);
            // 
            // diaPrint
            // 
            this.diaPrint.UseEXDialog = true;
            // 
            // toolStripContainer1
            // 
            this.toolStripContainer1.BottomToolStripPanelVisible = false;
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Controls.Add(this.tsMain);
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(992, 22);
            this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Top;
            this.toolStripContainer1.LeftToolStripPanelVisible = false;
            this.toolStripContainer1.Location = new System.Drawing.Point(0, 0);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.RightToolStripPanelVisible = false;
            this.toolStripContainer1.Size = new System.Drawing.Size(992, 22);
            this.toolStripContainer1.TabIndex = 3;
            this.toolStripContainer1.Text = "toolStripContainer1";
            this.toolStripContainer1.TopToolStripPanelVisible = false;
            // 
            // tsMain
            // 
            this.tsMain.BackColor = System.Drawing.Color.White;
            this.tsMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tsMain.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbMainBuildMode,
            this.tsbMainBrowseMode,
            this.tsbMainHideNavigation,
            this.tsbMainShowNavigation,
            this.tsbMainHelp,
            this.tsbMainUpdates,
            this.tsbMainSettings,
            this.tsbSearchShow,
            this.tsbSearchHide,
            this.tsbShowAnnouncements});
            this.tsMain.Location = new System.Drawing.Point(0, 0);
            this.tsMain.Name = "tsMain";
            this.tsMain.Size = new System.Drawing.Size(992, 22);
            this.tsMain.TabIndex = 0;
            // 
            // tsbMainBuildMode
            // 
            this.tsbMainBuildMode.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tsbMainBuildMode.Image = global::Client.Properties.Resources.BuildGreyPNG;
            this.tsbMainBuildMode.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbMainBuildMode.Name = "tsbMainBuildMode";
            this.tsbMainBuildMode.Size = new System.Drawing.Size(88, 19);
            this.tsbMainBuildMode.Text = "Build Mode";
            this.tsbMainBuildMode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.tsbMainBuildMode.Click += new System.EventHandler(this.tsbMainBuildMode_Click);
            // 
            // tsbMainBrowseMode
            // 
            this.tsbMainBrowseMode.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tsbMainBrowseMode.Image = global::Client.Properties.Resources.BrowseGreyPNG;
            this.tsbMainBrowseMode.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbMainBrowseMode.Name = "tsbMainBrowseMode";
            this.tsbMainBrowseMode.Size = new System.Drawing.Size(99, 19);
            this.tsbMainBrowseMode.Text = "Browse Mode";
            this.tsbMainBrowseMode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.tsbMainBrowseMode.Click += new System.EventHandler(this.tsbMainBrowseMode_Click);
            // 
            // tsbMainHideNavigation
            // 
            this.tsbMainHideNavigation.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tsbMainHideNavigation.Image = global::Client.Properties.Resources.HideNavGreyPNG;
            this.tsbMainHideNavigation.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbMainHideNavigation.Name = "tsbMainHideNavigation";
            this.tsbMainHideNavigation.Size = new System.Drawing.Size(113, 19);
            this.tsbMainHideNavigation.Text = "Hide Navigation";
            this.tsbMainHideNavigation.Click += new System.EventHandler(this.tsbMainHideNavigation_Click);
            // 
            // tsbMainShowNavigation
            // 
            this.tsbMainShowNavigation.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tsbMainShowNavigation.Image = global::Client.Properties.Resources.ShowNavGreyPNG;
            this.tsbMainShowNavigation.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbMainShowNavigation.Name = "tsbMainShowNavigation";
            this.tsbMainShowNavigation.Size = new System.Drawing.Size(117, 19);
            this.tsbMainShowNavigation.Text = "Show Navigation";
            this.tsbMainShowNavigation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.tsbMainShowNavigation.Visible = false;
            this.tsbMainShowNavigation.Click += new System.EventHandler(this.tsbMainShowNavigation_Click);
            // 
            // tsbMainHelp
            // 
            this.tsbMainHelp.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsbMainHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbMainHelpManual,
            this.supportFAQToolStripMenuItem,
            this.registerContentIssuesToolStripMenuItem,
            this.registerSoftwareIssuesToolStripMenuItem,
            this.tsbMainHelpAbout});
            this.tsbMainHelp.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tsbMainHelp.Image = global::Client.Properties.Resources.HelpGreyPNG;
            this.tsbMainHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbMainHelp.Name = "tsbMainHelp";
            this.tsbMainHelp.Size = new System.Drawing.Size(64, 19);
            this.tsbMainHelp.Text = "Help";
            this.tsbMainHelp.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.tsbMainHelp.ToolTipText = "Help";
            // 
            // tsbMainHelpManual
            // 
            this.tsbMainHelpManual.Name = "tsbMainHelpManual";
            this.tsbMainHelpManual.Size = new System.Drawing.Size(192, 22);
            this.tsbMainHelpManual.Text = "Help Manual";
            this.tsbMainHelpManual.Click += new System.EventHandler(this.tsbMainHelpManual_Click);
            // 
            // supportFAQToolStripMenuItem
            // 
            this.supportFAQToolStripMenuItem.Name = "supportFAQToolStripMenuItem";
            this.supportFAQToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.supportFAQToolStripMenuItem.Text = "Support FAQ";
            this.supportFAQToolStripMenuItem.Click += new System.EventHandler(this.supportFAQToolStripMenuItem_Click);
            // 
            // registerContentIssuesToolStripMenuItem
            // 
            this.registerContentIssuesToolStripMenuItem.Name = "registerContentIssuesToolStripMenuItem";
            this.registerContentIssuesToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.registerContentIssuesToolStripMenuItem.Text = "Report Content Issues";
            this.registerContentIssuesToolStripMenuItem.Click += new System.EventHandler(this.registerContentIssuesToolStripMenuItem_Click);
            // 
            // registerSoftwareIssuesToolStripMenuItem
            // 
            this.registerSoftwareIssuesToolStripMenuItem.Name = "registerSoftwareIssuesToolStripMenuItem";
            this.registerSoftwareIssuesToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.registerSoftwareIssuesToolStripMenuItem.Text = "Report Software Issues";
            this.registerSoftwareIssuesToolStripMenuItem.Click += new System.EventHandler(this.registerSoftwareIssuesToolStripMenuItem_Click);
            // 
            // tsbMainHelpAbout
            // 
            this.tsbMainHelpAbout.Name = "tsbMainHelpAbout";
            this.tsbMainHelpAbout.Size = new System.Drawing.Size(192, 22);
            this.tsbMainHelpAbout.Text = "About";
            this.tsbMainHelpAbout.Click += new System.EventHandler(this.tsbMainHelpAbout_Click);
            // 
            // tsbMainUpdates
            // 
            this.tsbMainUpdates.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsbMainUpdates.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbMainUpdateApplication,
            this.tsbMainUpdateData,
            this.profileToolStripMenuItem,
            this.tsddIndexing});
            this.tsbMainUpdates.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tsbMainUpdates.Image = global::Client.Properties.Resources.UpdateGreyPNG;
            this.tsbMainUpdates.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbMainUpdates.Name = "tsbMainUpdates";
            this.tsbMainUpdates.Size = new System.Drawing.Size(77, 19);
            this.tsbMainUpdates.Text = "Update";
            // 
            // tsbMainUpdateApplication
            // 
            this.tsbMainUpdateApplication.Name = "tsbMainUpdateApplication";
            this.tsbMainUpdateApplication.Size = new System.Drawing.Size(164, 22);
            this.tsbMainUpdateApplication.Text = "Application";
            this.tsbMainUpdateApplication.Click += new System.EventHandler(this.tsbMainUpdateApplication_Click);
            // 
            // tsbMainUpdateData
            // 
            this.tsbMainUpdateData.Name = "tsbMainUpdateData";
            this.tsbMainUpdateData.Size = new System.Drawing.Size(164, 22);
            this.tsbMainUpdateData.Text = "Data";
            this.tsbMainUpdateData.Click += new System.EventHandler(this.tsbMainUpdateData_Click);
            // 
            // profileToolStripMenuItem
            // 
            this.profileToolStripMenuItem.Name = "profileToolStripMenuItem";
            this.profileToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.profileToolStripMenuItem.Text = "Profile";
            this.profileToolStripMenuItem.Click += new System.EventHandler(this.profileToolStripMenuItem_Click);
            // 
            // tsddIndexing
            // 
            this.tsddIndexing.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmIndexAll,
            this.tsmTurnOffIndex,
            this.tsmTurnOnIndex});
            this.tsddIndexing.Name = "tsddIndexing";
            this.tsddIndexing.Size = new System.Drawing.Size(164, 22);
            this.tsddIndexing.Text = "Indexing Options";
            this.tsddIndexing.DropDownItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.tsddIndexing_DropDownItemClicked);
            // 
            // tsmIndexAll
            // 
            this.tsmIndexAll.Name = "tsmIndexAll";
            this.tsmIndexAll.Size = new System.Drawing.Size(167, 22);
            this.tsmIndexAll.Text = "Index All";
            // 
            // tsmTurnOffIndex
            // 
            this.tsmTurnOffIndex.Name = "tsmTurnOffIndex";
            this.tsmTurnOffIndex.Size = new System.Drawing.Size(167, 22);
            this.tsmTurnOffIndex.Text = "Turn Off Indexing";
            // 
            // tsmTurnOnIndex
            // 
            this.tsmTurnOnIndex.Name = "tsmTurnOnIndex";
            this.tsmTurnOnIndex.Size = new System.Drawing.Size(167, 22);
            this.tsmTurnOnIndex.Text = "Turn On Indexing";
            this.tsmTurnOnIndex.Visible = false;
            // 
            // tsbMainSettings
            // 
            this.tsbMainSettings.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsbMainSettings.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbMyRegionLanguages});
            this.tsbMainSettings.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tsbMainSettings.Image = global::Client.Properties.Resources.SettingsPNG;
            this.tsbMainSettings.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbMainSettings.Name = "tsbMainSettings";
            this.tsbMainSettings.Size = new System.Drawing.Size(121, 19);
            this.tsbMainSettings.Text = "Region Settings";
            // 
            // tsbMyRegionLanguages
            // 
            this.tsbMyRegionLanguages.Name = "tsbMyRegionLanguages";
            this.tsbMyRegionLanguages.Size = new System.Drawing.Size(208, 22);
            this.tsbMyRegionLanguages.Text = "My Regions & Languages...";
            this.tsbMyRegionLanguages.Click += new System.EventHandler(this.tsbMyRegionLanguages_Click);
            // 
            // tsbSearchShow
            // 
            this.tsbSearchShow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tsbSearchShow.Image = global::Client.Properties.Resources.ShowSearchGreyPNG;
            this.tsbSearchShow.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbSearchShow.Name = "tsbSearchShow";
            this.tsbSearchShow.Size = new System.Drawing.Size(94, 19);
            this.tsbSearchShow.Text = "Show Search";
            this.tsbSearchShow.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.tsbSearchShow.Visible = false;
            this.tsbSearchShow.Click += new System.EventHandler(this.tsbSearchShow_Click);
            // 
            // tsbSearchHide
            // 
            this.tsbSearchHide.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tsbSearchHide.Image = global::Client.Properties.Resources.HideSearchGreyPNG;
            this.tsbSearchHide.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbSearchHide.Name = "tsbSearchHide";
            this.tsbSearchHide.Size = new System.Drawing.Size(90, 19);
            this.tsbSearchHide.Text = "Hide Search";
            this.tsbSearchHide.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.tsbSearchHide.Visible = false;
            this.tsbSearchHide.Click += new System.EventHandler(this.tsbSearchHide_Click);
            // 
            // tsbShowAnnouncements
            // 
            this.tsbShowAnnouncements.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbShowAnnouncements.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbShowAnnouncements.Name = "tsbShowAnnouncements";
            this.tsbShowAnnouncements.Size = new System.Drawing.Size(127, 19);
            this.tsbShowAnnouncements.Text = "View Announcements";
            this.tsbShowAnnouncements.Click += new System.EventHandler(this.tsbShowAnnouncements_Click);
            // 
            // diaExportSave
            // 
            this.diaExportSave.DefaultExt = "ppt";
            this.diaExportSave.FileName = "Untitled.ppt";
            this.diaExportSave.Filter = "PowerPoint Presentation (*.ppt)|*.ppt";
            this.diaExportSave.RestoreDirectory = true;
            // 
            // stsMain
            // 
            this.stsMain.BackColor = System.Drawing.Color.White;
            this.stsMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabelThumbnail,
            this.toolStripProgressBarThumbnail,
            this.lblMainStatusMessage,
            this.pbMainStatusProgress,
            this.lblMainStatusContentUpdate,
            this.pbContentUpdate,
            this.toolStripStatusLabelResume});
            this.stsMain.Location = new System.Drawing.Point(0, 694);
            this.stsMain.Name = "stsMain";
            this.stsMain.Size = new System.Drawing.Size(992, 22);
            this.stsMain.TabIndex = 4;
            this.stsMain.Text = "statusStrip1";
            // 
            // toolStripStatusLabelThumbnail
            // 
            this.toolStripStatusLabelThumbnail.Name = "toolStripStatusLabelThumbnail";
            this.toolStripStatusLabelThumbnail.Size = new System.Drawing.Size(65, 17);
            this.toolStripStatusLabelThumbnail.Text = "Thumbnail";
            this.toolStripStatusLabelThumbnail.Visible = false;
            // 
            // toolStripProgressBarThumbnail
            // 
            this.toolStripProgressBarThumbnail.Name = "toolStripProgressBarThumbnail";
            this.toolStripProgressBarThumbnail.Size = new System.Drawing.Size(150, 16);
            this.toolStripProgressBarThumbnail.Visible = false;
            // 
            // lblMainStatusMessage
            // 
            this.lblMainStatusMessage.Name = "lblMainStatusMessage";
            this.lblMainStatusMessage.Size = new System.Drawing.Size(69, 17);
            this.lblMainStatusMessage.Text = "Main Status";
            this.lblMainStatusMessage.Visible = false;
            // 
            // pbMainStatusProgress
            // 
            this.pbMainStatusProgress.Name = "pbMainStatusProgress";
            this.pbMainStatusProgress.Size = new System.Drawing.Size(200, 16);
            this.pbMainStatusProgress.Visible = false;
            // 
            // lblMainStatusContentUpdate
            // 
            this.lblMainStatusContentUpdate.ForeColor = System.Drawing.Color.Red;
            this.lblMainStatusContentUpdate.Name = "lblMainStatusContentUpdate";
            this.lblMainStatusContentUpdate.Size = new System.Drawing.Size(401, 17);
            this.lblMainStatusContentUpdate.Text = "Content Update (Slides will not be available offline until update completes)";
            this.lblMainStatusContentUpdate.Visible = false;
            // 
            // pbContentUpdate
            // 
            this.pbContentUpdate.Name = "pbContentUpdate";
            this.pbContentUpdate.Size = new System.Drawing.Size(200, 16);
            this.pbContentUpdate.Visible = false;
            // 
            // toolStripStatusLabelResume
            // 
            this.toolStripStatusLabelResume.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripStatusLabelResume.IsLink = true;
            this.toolStripStatusLabelResume.Name = "toolStripStatusLabelResume";
            this.toolStripStatusLabelResume.Size = new System.Drawing.Size(136, 17);
            this.toolStripStatusLabelResume.Text = "Resume Content Update";
            this.toolStripStatusLabelResume.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.toolStripStatusLabelResume.Click += new System.EventHandler(this.toolStripStatusLabelResume_Click);
            // 
            // clkWait
            // 
            this.clkWait.Interval = 1000;
            // 
            // exportPptBuilder
            // 
            this.exportPptBuilder.AsposeSlideLicensePath = null;
            this.exportPptBuilder.DiagnosticOutputFile = null;
            this.exportPptBuilder.SupportsDiagnosticOutput = false;
            this.exportPptBuilder.WorkerReportsProgress = true;
            this.exportPptBuilder.WorkerSupportsCancellation = true;
            this.exportPptBuilder.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.exportPptBuilder_ProgressChanged);
            this.exportPptBuilder.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.exportPptBuilder_RunWorkerCompleted);
            // 
            // playPptBuilder
            // 
            this.playPptBuilder.AsposeSlideLicensePath = null;
            this.playPptBuilder.DiagnosticOutputFile = null;
            this.playPptBuilder.SupportsDiagnosticOutput = false;
            this.playPptBuilder.WorkerReportsProgress = true;
            this.playPptBuilder.WorkerSupportsCancellation = true;
            this.playPptBuilder.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.playPptBuilder_ProgressChanged);
            this.playPptBuilder.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.playPptBuilder_RunWorkerCompleted);
            // 
            // printPptBuilder
            // 
            this.printPptBuilder.AsposeSlideLicensePath = null;
            this.printPptBuilder.DiagnosticOutputFile = null;
            this.printPptBuilder.SupportsDiagnosticOutput = false;
            this.printPptBuilder.WorkerReportsProgress = true;
            this.printPptBuilder.WorkerSupportsCancellation = true;
            this.printPptBuilder.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.printPptBuilder_ProgressChanged);
            this.printPptBuilder.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.printPptBuilder_RunWorkerCompleted);
            // 
            // webPrinter
            // 
            this.webPrinter.AllowWebBrowserDrop = false;
            this.webPrinter.IsWebBrowserContextMenuEnabled = false;
            this.webPrinter.Location = new System.Drawing.Point(933, 694);
            this.webPrinter.MinimumSize = new System.Drawing.Size(20, 20);
            this.webPrinter.Name = "webPrinter";
            this.webPrinter.ScrollBarsEnabled = false;
            this.webPrinter.Size = new System.Drawing.Size(20, 20);
            this.webPrinter.TabIndex = 3;
            this.webPrinter.TabStop = false;
            this.webPrinter.Visible = false;
            this.webPrinter.WebBrowserShortcutsEnabled = false;
            this.webPrinter.Navigated += new System.Windows.Forms.WebBrowserNavigatedEventHandler(this.webPrinter_Navigated);
            // 
            // clkAppUpdate
            // 
            this.clkAppUpdate.Interval = 600000;
            this.clkAppUpdate.Tick += new System.EventHandler(this.clkAppUpdate_Tick);
            // 
            // backgroundWorkerPreviewThumbnails
            // 
            this.backgroundWorkerPreviewThumbnails.WorkerReportsProgress = true;
            this.backgroundWorkerPreviewThumbnails.WorkerSupportsCancellation = true;
            this.backgroundWorkerPreviewThumbnails.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerPreviewThumbnails_DoWork);
            this.backgroundWorkerPreviewThumbnails.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorkerPreviewThumbnails_ProgressChanged);
            this.backgroundWorkerPreviewThumbnails.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerPreviewThumbnails_RunWorkerCompleted);
            // 
            // usageLoggingWorker
            // 
            this.usageLoggingWorker.Interval = 30000;
            this.usageLoggingWorker.StartIn = 45000;
            this.usageLoggingWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.usageLoggingWorker_DoWork);
            // 
            // serviceLoggingWorker
            // 
            this.serviceLoggingWorker.Interval = 130000;
            this.serviceLoggingWorker.StartIn = 15000;
            this.serviceLoggingWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.serviceLoggingWorker_DoWork);
            // 
            // tmr
            // 
            this.tmr.Interval = 10000;
            this.tmr.Tick += new System.EventHandler(this.tmr_Tick);
            // 
            // tmr2
            // 
            this.tmr2.Interval = 10000;
            this.tmr2.Tick += new System.EventHandler(this.tmr2_Tick);
            // 
            // printPreviewDialog1
            // 
            this.printPreviewDialog1.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.ClientSize = new System.Drawing.Size(400, 300);
            this.printPreviewDialog1.Enabled = true;
            this.printPreviewDialog1.Icon = ((System.Drawing.Icon)(resources.GetObject("printPreviewDialog1.Icon")));
            this.printPreviewDialog1.Name = "printPreviewDialog1";
            this.printPreviewDialog1.Visible = false;
            // 
            // IpgMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.ClientSize = new System.Drawing.Size(992, 716);
            this.Controls.Add(this.webPrinter);
            this.Controls.Add(this.stsMain);
            this.Controls.Add(this.toolStripContainer1);
            this.Controls.Add(this.splForm);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "IpgMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "HP Printing Sales Guide";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.IpgMain_FormClosing);
            this.Load += new System.EventHandler(this.IpgMain_Load);
            this.Shown += new System.EventHandler(this.IpgMain_Shown);
            this.Layout += new System.Windows.Forms.LayoutEventHandler(this.IpgMain_Layout);
            this.tsPreview.ResumeLayout(false);
            this.tsPreview.PerformLayout();
            this.tsMySlides.ResumeLayout(false);
            this.tsMySlides.PerformLayout();
            this.splForm.Panel1.ResumeLayout(false);
            this.splForm.Panel2.ResumeLayout(false);
            this.splForm.ResumeLayout(false);
            this.navigationPaneLeftPane.ResumeLayout(false);
            this.navigationPanePageSlideLibrary.ResumeLayout(false);
            this.navigationPanePageMyPresentations.ResumeLayout(false);
            this.cmnMyPresentations.ResumeLayout(false);
            this.navigationPanePageAdvancedSearch.ResumeLayout(false);
            this.navigationPanePageAdvancedSearch.PerformLayout();
            this.navigationPanePageFilters.ResumeLayout(false);
            this.gradientPanel1.ResumeLayout(false);
            this.splViewers.Panel1.ResumeLayout(false);
            this.splViewers.Panel2.ResumeLayout(false);
            this.splViewers.ResumeLayout(false);
            this.splPreview.Panel1.ResumeLayout(false);
            this.splPreview.Panel1.PerformLayout();
            this.splPreview.Panel2.ResumeLayout(false);
            this.splPreview.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.splPreviewType.Panel1.ResumeLayout(false);
            this.splPreviewType.Panel2.ResumeLayout(false);
            this.splPreviewType.Panel2.PerformLayout();
            this.splPreviewType.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.cmnSlideLibraryPreview.ResumeLayout(false);
            this.tsRegionStatus.ResumeLayout(false);
            this.tsRegionStatus.PerformLayout();
            this.tsbSlideLibraryPreviewNav.ResumeLayout(false);
            this.tsbSlideLibraryPreviewNav.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picPptScrollHider)).EndInit();
            this.splViewerBottom.Panel1.ResumeLayout(false);
            this.splViewerBottom.Panel2.ResumeLayout(false);
            this.splViewerBottom.ResumeLayout(false);
            this.splMySlides.Panel1.ResumeLayout(false);
            this.splMySlides.Panel1.PerformLayout();
            this.splMySlides.Panel2.ResumeLayout(false);
            this.splMySlides.Panel2.PerformLayout();
            this.splMySlides.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.cmnMySlides.ResumeLayout(false);
            this.splSearch.Panel1.ResumeLayout(false);
            this.splSearch.Panel1.PerformLayout();
            this.splSearch.Panel2.ResumeLayout(false);
            this.splSearch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.ContentPanel.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.tsMain.ResumeLayout(false);
            this.tsMain.PerformLayout();
            this.stsMain.ResumeLayout(false);
            this.stsMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SplitContainer splForm;
        private System.Windows.Forms.SplitContainer splViewers;
        private System.Windows.Forms.SplitContainer splMySlides;
        private System.Windows.Forms.SplitContainer splPreview;
        private System.Windows.Forms.Label lblMySlides;
        private System.Windows.Forms.Label lblPreview;
		private System.Windows.Forms.ListView lstMySlides;
        private System.Windows.Forms.ImageList imlSlideThumbs;
        private System.Windows.Forms.ToolStrip tsPreview;
		private System.Windows.Forms.SplitContainer splPreviewType;
        private System.Windows.Forms.ToolStrip tsMySlides;
        private System.Windows.Forms.ToolStripButton tsbMySlideRemoveSlide;
        private System.Windows.Forms.ToolStripButton tsbSlideLibraryAddToMySlides;
        private System.Windows.Forms.StatusStrip statusMySlides;
        private System.Windows.Forms.ToolStripStatusLabel statusMySlidesCount;
        private System.Windows.Forms.ToolStripStatusLabel statusMySlidesQuantity;
        private System.Windows.Forms.ToolStripButton tsbMySlidesSave;
        private System.Windows.Forms.ToolStripButton tsbMySlidesSaveAs;
        private System.Windows.Forms.ToolStripButton tsbSlideLibrarySelectAll;
        private System.Windows.Forms.ContextMenuStrip cmnMySlides;
        private System.Windows.Forms.ToolStripMenuItem miMySlidesRemove;
        private System.Windows.Forms.ToolStripMenuItem miMySlidesSave;
        private System.Windows.Forms.ToolStripMenuItem miMySlidesSaveAs;
        private System.Windows.Forms.ToolStripButton tsbMySlidesSelectAll;
        private System.Windows.Forms.ToolStripButton tsbMySlidesPlay;
        private System.Windows.Forms.ToolStripButton tsbMySlidesClear;
        private System.Windows.Forms.ToolStripMenuItem miMySlidesPlay;
        private System.Windows.Forms.ToolStripMenuItem miMySlidesSelectAll;
        private System.Windows.Forms.ToolStripMenuItem miMySlidesClear;
        private System.Windows.Forms.ContextMenuStrip cmnSlideLibraryPreview;
        private System.Windows.Forms.ToolStripMenuItem miSlideLibraryPreviewAddToMySlides;
        private System.Windows.Forms.ToolStripMenuItem miSlideLibraryPreviewSelectAll;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ContextMenuStrip cmnMyPresentations;
        private System.Windows.Forms.ToolStripMenuItem miMyPresentationsDelete;
        private System.Windows.Forms.ToolStripButton tsbMySlidesExportPpt;
        private System.Windows.Forms.ToolStrip tsbSlideLibraryPreviewNav;
        private System.Windows.Forms.ToolStripButton tsbSlideLibraryPreviewBack;
        private System.Windows.Forms.ToolStripButton tsbSlideLibraryPreviewForward;
        private System.Windows.Forms.ToolStripMenuItem miMySlidesExportPpt;
        private System.Windows.Forms.ToolStripButton tsbSlideLibraryExportPpt;
        private System.Windows.Forms.ToolStripButton tsbSlideLibraryPlay;
        private System.Windows.Forms.ImageList imlTinyThumbs;
        private System.Windows.Forms.ToolStripButton tsbMySlidesPrint;
        private System.Windows.Forms.PrintDialog diaPrint;
        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private System.Windows.Forms.ToolStrip tsMain;
        private System.Windows.Forms.ToolStripButton tsbMainBuildMode;
        private System.Windows.Forms.ToolStripButton tsbMainBrowseMode;
        private System.Windows.Forms.ToolStripButton tsbMainHideNavigation;
        private System.Windows.Forms.ToolStripButton tsbMainShowNavigation;
        private System.Windows.Forms.ToolStripButton tsbSlideLibraryPrint;
        private System.Windows.Forms.PictureBox picPptScrollHider;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripButton tsbSlideLibraryPreviewHome;
        private System.Windows.Forms.ToolStripSplitButton tsbMainUpdates;
        private System.Windows.Forms.ToolStripMenuItem tsbMainUpdateApplication;
        private System.Windows.Forms.ToolStripMenuItem tsbMainUpdateData;
        private System.Windows.Forms.ToolStripSplitButton tsbMainHelp;
        private System.Windows.Forms.ToolStripMenuItem tsbMainHelpManual;
        private System.Windows.Forms.ToolStripMenuItem tsbMainHelpAbout;
        private PlugIns.Helpers.PptBuilder exportPptBuilder;
        private System.Windows.Forms.SaveFileDialog diaExportSave;
        private System.Windows.Forms.StatusStrip stsMain;
        private System.Windows.Forms.ToolStripStatusLabel lblMainStatusMessage;
        private System.Windows.Forms.ToolStripProgressBar pbMainStatusProgress;
        private PlugIns.Helpers.PptBuilder playPptBuilder;
        private PlugIns.Helpers.PptBuilder printPptBuilder;
        private System.Windows.Forms.Timer clkWait;
        private System.Windows.Forms.ToolStripMenuItem miMySlidesPrint;
        private System.Windows.Forms.WebBrowser webPrinter;
        private System.Windows.Forms.Timer clkAppUpdate;
        private System.Windows.Forms.ImageList imlIcons16;
        private System.Windows.Forms.ToolStripSplitButton tsbMainSettings;
        private System.Windows.Forms.ToolStripLabel tslSlideLibraryPreviewSlideRegion;
        private System.Windows.Forms.ToolStrip tsRegionStatus;
        private System.Windows.Forms.ToolStripLabel tslRegionStatus;
        private System.Windows.Forms.ToolStripStatusLabel lblMainStatusContentUpdate;
		private System.Windows.Forms.ToolStripProgressBar pbContentUpdate;
		private Ascend.Windows.Forms.NavigationPane navigationPaneLeftPane;
        private Ascend.Windows.Forms.NavigationPanePage navigationPanePageSlideLibrary;
		private Ascend.Windows.Forms.NavigationPanePage navigationPanePageFilters;
        private Ascend.Windows.Forms.NavigationPanePage navigationPanePageMyPresentations;
		private Client.BLL.CategorySlideTreeView categorySlideTreeViewPresentations;
		public System.Windows.Forms.ListView lstPreview;
        public System.Windows.Forms.WebBrowser webSlidePreview;
        private System.Windows.Forms.TabControl tabRegionLanguageTrees;
        private System.Windows.Forms.TreeView tvwMySlideDecks;
        private System.Windows.Forms.ToolStripMenuItem tsbMyRegionLanguages;
        private Ascend.Windows.Forms.GradientCaption gradientCaption4;
        private System.Windows.Forms.Button btnApplyFilterSettings;
        private System.Windows.Forms.TabControl tabFilterGroups;
        private Ascend.Windows.Forms.GradientPanel gradientPanel1;
        private System.ComponentModel.BackgroundWorker backgroundWorkerPreviewThumbnails;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelThumbnail;
		private System.Windows.Forms.ToolStripProgressBar toolStripProgressBarThumbnail;
		private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelResume;
		private System.Windows.Forms.ToolStripMenuItem registerContentIssuesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registerSoftwareIssuesToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton tsbShowAnnouncements;
        private System.Windows.Forms.ToolStripMenuItem supportFAQToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem profileToolStripMenuItem;
        private PlugIns.Helpers.IntervalInvokingWorker usageLoggingWorker;
        private PlugIns.Helpers.IntervalInvokingWorker serviceLoggingWorker;
        private System.Windows.Forms.ProgressBar pbIndex;
        private System.Windows.Forms.Label lblIndex;
        private System.Windows.Forms.Timer tmr;
        private System.Windows.Forms.Timer tmr2;
        private System.Windows.Forms.Button btnSearchIndex;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.SplitContainer splViewerBottom;
        private System.Windows.Forms.ToolStripButton tsbSearchShow;
        private System.Windows.Forms.ToolStripButton tsbSearchHide;
        private System.Windows.Forms.SplitContainer splSearch;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListView lvSearch;
        private System.Windows.Forms.ColumnHeader Thumbnail;
        private System.Windows.Forms.ColumnHeader Title;
        private System.Windows.Forms.ColumnHeader Category;
        private System.Windows.Forms.ColumnHeader ModifiedDate;
        private Ascend.Windows.Forms.NavigationPanePage navigationPanePageAdvancedSearch;
        private System.Windows.Forms.Button btnAdvancedSearchSubmit;
        private System.Windows.Forms.CheckBox cbAdvancedSearchDate;
        private System.Windows.Forms.DateTimePicker dtFrom;
        private System.Windows.Forms.Label lblTo;
        private System.Windows.Forms.DateTimePicker dtTo;
        private System.Windows.Forms.Label lblFrom;
        private System.Windows.Forms.CheckedListBox cbCategories;
        private System.Windows.Forms.CheckBox cbAdvancedSearchCategories;
        private System.Windows.Forms.Label lblSearchString;
        private System.Windows.Forms.TextBox txtSearchBox;
        private System.Windows.Forms.Label lblSearchResultsCount;
        private System.Windows.Forms.ToolStripMenuItem tsddIndexing;
        private System.Windows.Forms.ToolStripMenuItem tsmIndexAll;
        private System.Windows.Forms.ToolStripMenuItem tsmTurnOffIndex;
        private System.Windows.Forms.ToolStripMenuItem tsmTurnOnIndex;
        private System.Windows.Forms.PrintPreviewDialog printPreviewDialog1;
        private System.Windows.Forms.ColumnHeader RegionLang;
        private System.Windows.Forms.ColumnHeader TopCategory;
        private System.Windows.Forms.Button btnSearchFAQ;
        private System.Windows.Forms.Button btnASearchFAQ;
        private System.Windows.Forms.LinkLabel llRegistryFix;
        private System.Windows.Forms.Button btnregistryFixClose;
        private System.Windows.Forms.ToolTip toolTip1;

    }
}