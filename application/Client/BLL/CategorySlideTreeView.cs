using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Data;

namespace Client.BLL
{
	class CategorySlideTreeView : Wirestone.WinForms.Controls.CategoryEntryTreeview.CategoryEntryTreeView
	{
		#region Properties

		private IpgMain _IpgMainForm;

		private IpgMain IpgMainForm
		{
			get { return _IpgMainForm; }
			set { _IpgMainForm = value; }
		}

        private int _DescendantSlides;

        private int DescendantSlides
        {
            get { return _DescendantSlides; }
            set { _DescendantSlides = value; }
        }

        private List<TreeNode> _NodesToDelete;

        private List<TreeNode> NodesToDelete
        {
            get { return _NodesToDelete; }
            set { _NodesToDelete = value; }
        }

        //private List<wsIpgSlideUpdater.SlideLibrary.SlideRow> _CurrentDescendantSlides;

        //private List<wsIpgSlideUpdater.SlideLibrary.SlideRow> CurrentDescendantSlides
        //{
        //    get { return _CurrentDescendantSlides; }
        //    set { _CurrentDescendantSlides = value; }
        //}

        private System.Collections.ArrayList _CurrentDescendantSlides;

        private System.Collections.ArrayList CurrentDescendantSlides
        {
            get { return _CurrentDescendantSlides; }
            set { _CurrentDescendantSlides = value; }
        }



	

		#endregion

		public CategorySlideTreeView() : base()
		{
			// Hook events
			EntryNodeLeftClick += new EntryNodeLeftClickDelegate(OnEntryNodeLeftClick);
            //*SS Removing the right click delegates
			//EntryNodeRightClick += new EntryNodeRightClickDelegate(OnEntryNodeRightClick);
			//CategoryNodeRightClick += new CategoryNodeRightClickDelegate(OnCategoryNodeRightClick);
			CategoryNodeLeftClick += new CategoryNodeLeftClickDelegate(OnCategoryNodeLeftClick);

            //CurrentDescendantSlides = new List<Client.wsIpgSlideUpdater.SlideLibrary.SlideRow>();
            CurrentDescendantSlides = new System.Collections.ArrayList();

            this.HideSelection = false;
		}

		public void FillCategorySlideData(wsIpgSlideUpdater.SlideLibrary dsSlideLibrary)
		{
			this.FillWithImages(dsSlideLibrary.Category, 
                "CategoryID", 
                "Category", 
                typeof(wsIpgSlideUpdater.SlideLibrary.CategoryRow), 
                "ParentCategoryID",
				dsSlideLibrary.Slide, 
                "SlideID", 
                "Title",
				dsSlideLibrary.SlideCategory, 
                typeof(wsIpgSlideUpdater.SlideLibrary.SlideCategoryRow),
				"Icon", 
                true, 
                "single-slide.ico", 
                false);


			// Get the current instance of the AdminMain
			IpgMainForm = ((IpgMain)this.FindForm());
			
			// Expand the first node. This is the same behavior as the original treeview.
			Nodes[0].Expand();


            NodesToDelete = new List<TreeNode>();
            foreach (TreeNode catNode in this.Nodes)
            {
                DeleteEmptyCategoryNode(catNode);
            }
            foreach (TreeNode deleteNode in NodesToDelete)
            {
                deleteNode.Remove();
            }
		}

        #region helper methods for removing empty category nodes

        private void DeleteEmptyCategoryNode(TreeNode node)
        {
            string nodeText = node.Text;
            if (node.Tag.GetType() == CategoryNodeType)
            {
                if (!NodeHasDescendantSlides(node))
                {
                    //node.Remove();
                    NodesToDelete.Add(node);

                    //return;
                }
                else
                {
                    int numNodes = node.Nodes.Count;
                    for (int i = 0; i < numNodes; i++)
                    {
                        if (node.Nodes.Count <= i)
                        {
                            break;
                        }
                        DeleteEmptyCategoryNode(node.Nodes[i]);


                    }
                    //foreach (TreeNode childNode in node.Nodes)
                    //{
                    //    DeleteEmptyCategoryNode(childNode);

                    //}
                }
            }
        }

        private bool NodeHasDescendantSlides(TreeNode node)
        {
            DescendantSlides = 0;
            for (int i = 0; i < node.Nodes.Count; i++)
            {
                TraverseChildNodesForSlides(node.Nodes[i]);
                if (DescendantSlides > 0)
                {
                    return true;
                }
                //else
                //{
                //    return false;
                //}
            }
            return false;

        }

        private void TraverseChildNodesForSlides(TreeNode node)
        {
            string test = node.Tag.GetType().ToString();

            //if its a slide, add it
            if (node.Tag.GetType() == typeof(wsIpgSlideUpdater.SlideLibrary.SlideCategoryRow))
            {
                //wsIpgSlideUpdater.SlideLibrary.SlideRow slide = dsSlideLibrary.Slide.FindBySlideID(((wsIpgSlideUpdater.SlideLibrary.SlideCategoryRow)node.Tag).SlideID);
                DescendantSlides++;
                return;
            }
            // Iterate through the child nodes of the parent node.
            for (int i = 0; i < node.Nodes.Count; i++)
            {
                TraverseChildNodesForSlides(node.Nodes[i]);
            }

        } 

        #endregion

        public void GetDescendantSlides(TreeNode node)
        {
            if (CurrentDescendantSlides.Count > 0)
            {
                this.CurrentDescendantSlides.Clear();
            }
            for (int i = 0; i < node.Nodes.Count; i++)
            {
                PerambulateChildNodesForSlides(node.Nodes[i]);
            }
        }

        public void GetChildSlides(TreeNode node)
        {
            if (CurrentDescendantSlides.Count > 0)
            {
                CurrentDescendantSlides.Clear();
            }
            foreach (TreeNode childNode in node.Nodes)
            {
                //if its a slide, add it
                if (childNode.Tag.GetType() == typeof(wsIpgSlideUpdater.SlideLibrary.SlideCategoryRow))
                {
                    wsIpgSlideUpdater.SlideLibrary.SlideCategoryRow categoryRow = (wsIpgSlideUpdater.SlideLibrary.SlideCategoryRow)childNode.Tag;
                    //wsIpgSlideUpdater.SlideLibrary.SlideRow slide = ((wsIpgSlideUpdater.SlideLibrary.SlideCategoryRow)childNode.Tag).SlideRow;
                    wsIpgSlideUpdater.SlideLibrary.SlideRow slide = ((wsIpgSlideUpdater.SlideLibrary.SlideDataTable)TreeData.EntryTable).FindBySlideID(categoryRow.SlideID);
                    slide.CategoryID = categoryRow.CategoryID;
                    CurrentDescendantSlides.Add(slide);
                }
            }
        }

        private void PerambulateChildNodesForSlides(TreeNode node)
        {
            string test = node.Tag.GetType().ToString();

            //if its a slide, add it
            if (node.Tag.GetType().ToString() == "Client.wsIpgSlideUpdater.SlideLibrary+SlideCategoryRow")
            {
                wsIpgSlideUpdater.SlideLibrary.SlideCategoryRow categoryRow = (wsIpgSlideUpdater.SlideLibrary.SlideCategoryRow)node.Tag;
                //wsIpgSlideUpdater.SlideLibrary.SlideRow slide = ((wsIpgSlideUpdater.SlideLibrary.SlideCategoryRow)node.Tag).SlideRow;
                wsIpgSlideUpdater.SlideLibrary.SlideRow slide = ((wsIpgSlideUpdater.SlideLibrary.SlideDataTable)TreeData.EntryTable).FindBySlideID(categoryRow.SlideID);
                slide.CategoryID = categoryRow.CategoryID;
                CurrentDescendantSlides.Add(slide);
            }
            // Iterate through the child nodes of the parent node.
            for (int i = 0; i < node.Nodes.Count; i++)
            {
                PerambulateChildNodesForSlides(node.Nodes[i]);
            }

        }

		protected override void OnEntryNodeLeftClick(TreeNodeMouseClickEventArgs e, object entryRow)
		{            
		}

		protected override void OnCategoryNodeLeftClick(TreeNodeMouseClickEventArgs e, object categoryRow)
		{
            this.SelectedNode = e.Node;  
		}

        protected override void OnAfterSelect(TreeViewEventArgs e)
        {
            if (e.Node.Tag.GetType() == typeof(wsIpgSlideUpdater.SlideLibrary.CategoryRow))
            {
                IpgMainForm.CurrentChildSlides.Clear();
                IpgMainForm.lstPreview.Items.Clear();
                IpgMainForm.CurrentBrowsedSlideSet = Client.IpgMain.BrowsedSlideSet.Library.ToString();

                if (((wsIpgSlideUpdater.SlideLibrary.CategoryRow)e.Node.Tag).ShowDescendantSlides)
                {
                    GetDescendantSlides(e.Node);
                }
                else
                {
                    GetChildSlides(e.Node);
                }


                if (CurrentDescendantSlides.Count > 0)
                {
                    IpgMainForm.CurrentChildSlides.Clear();
                    IpgMainForm.CurrentChildSlides.AddRange(CurrentDescendantSlides);
                    // disable logging for folder click if a folder contains slide nodes
                    IpgMainForm.SetLoggingEnabled(false);
                    IpgMainForm.LoadPreviewSlidePreviewThumbsAsync();
                    //IpgMainForm.PopulatePreviewSlidesList();
                    //IpgMainForm.ActivatePreviewListItem(0);
                    //IpgMainForm.SetupButtons();
                }
                else
                {
                    IpgMainForm.webSlidePreview.Navigate("");
                    IpgMainForm.DisableSlideLibraryButtons();
                }
            }
            else if (e.Node.Tag.GetType() == typeof(wsIpgSlideUpdater.SlideLibrary.SlideCategoryRow))
            {
                // enable usage logging for a slide click
                IpgMainForm.SetLoggingEnabled(true);
                // Clear thumbnails
                IpgMainForm.CurrentChildSlides.Clear();
                IpgMainForm.lstPreview.Items.Clear();
                wsIpgSlideUpdater.SlideLibrary.SlideCategoryRow categoryRow = (wsIpgSlideUpdater.SlideLibrary.SlideCategoryRow)e.Node.Tag;
                // Get the slide row
                wsIpgSlideUpdater.SlideLibrary.SlideRow slideRow = ((wsIpgSlideUpdater.SlideLibrary.SlideDataTable)TreeData.EntryTable).FindBySlideID(categoryRow.SlideID);
                slideRow.CategoryID = categoryRow.CategoryID;
                // Add the slide row
                IpgMainForm.CurrentChildSlides.Add(slideRow);

                // Set thumbsnail
                IpgMainForm.LoadPreviewSlidePreviewThumbsAsync();
                //IpgMainForm.PopulatePreviewSlidesList();
                //IpgMainForm.ActivatePreviewListItem(0);

                //IpgMainForm.SetupButtons();
            }
                //base.OnAfterSelect(e);
        }
        
        protected override void OnAfterExpand(TreeViewEventArgs e)
        {
            //base.OnAfterExpand(e);
            e.Node.TreeView.SelectedNode = e.Node;
           
        }
	}
}
