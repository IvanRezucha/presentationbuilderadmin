﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Client.BLL
{
    class SearchItem
    {
        #region Properties
        string _filename;
        string _title;
        string _category;
        int _categoryid;
        int _slideid;
        int _regionlangid;
        string _regionlang;
        DateTime _modified_date;
        string _top_category;

        public string FileName
        {
            get
            {
                return _filename;
            }
            set
            {
                _filename = value;
            }
        }
        public string Title
        {
            get
            {
                return _title;
            }
            set
            {
                _title = value;
            }
        }
        public string Category
        {
            get
            {
                return _category;
            }
            set
            {
                _category = value;
            }
        }
        public int CategoryID
        {
            get
            {
                return _categoryid;
            }
            set
            {
                _categoryid = value;
            }
        }
        public int RegionLangID
        {
            get
            {
                return _regionlangid;
            }
            set
            {
                _regionlangid = value;
            }
        }
        public string RegionLang
        {
            get
            {
                return _regionlang;
            }
            set
            {
                _regionlang = value;
            }
        }
        public int SlideID
        {
            get
            {
                return _slideid;
            }
            set
            {
                _slideid = value;
            }
        }
        public DateTime ModifiedDate
        {
            get
            {
                return _modified_date;
            }
            set
            {
                _modified_date = value;
            }
        }
        public string Top_Category
        {
            get
            {
                return _top_category;
            }
            set
            {
                _top_category = value;
            }
        }
        #endregion
    }
}
