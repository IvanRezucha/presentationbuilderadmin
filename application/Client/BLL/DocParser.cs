using System;
using System.IO;
using iTextSharp.text.pdf;
using System.Text;

namespace DocToText
{
    /// <summary>
    /// Parses a Doc file and extracts the text from it.
    /// </summary>
    public class DocParser
    {
        public string extractDocText(string fl){
            string m_Content;
            try
            {           
                Microsoft.Office.Interop.Word.ApplicationClass wordApp = new Microsoft.Office.Interop.Word.ApplicationClass();
                object file = fl;
                object nullobj = System.Reflection.Missing.Value;
                Microsoft.Office.Interop.Word.Document doc = wordApp.Documents.Open(ref file,
                    ref nullobj, ref nullobj,ref nullobj, ref nullobj, ref nullobj,
                    ref nullobj, ref nullobj, ref nullobj, ref nullobj, ref nullobj, 
                    ref nullobj, ref nullobj, ref nullobj, ref nullobj, ref nullobj);

                Microsoft.Office.Interop.Word.Document doc1 = wordApp.ActiveDocument;
                m_Content = doc1.Content.Text;

                doc.Close(ref nullobj, ref nullobj, ref nullobj);
            }
            catch (Exception)
            {

                m_Content = "";
            }


            return m_Content;
            
        }
    }
}
