﻿using System;
using System.Collections.Generic;
using System.Text;  
using Analyzer = Lucene.Net.Analysis.Analyzer;
using StandardAnalyzer = Lucene.Net.Analysis.Standard.StandardAnalyzer;
using Document = Lucene.Net.Documents.Document;
using QueryParser = Lucene.Net.QueryParsers.QueryParser;
using Hits = Lucene.Net.Search.Hits;
using IndexSearcher = Lucene.Net.Search.IndexSearcher;
using Query = Lucene.Net.Search.Query;
using Searcher = Lucene.Net.Search.Searcher;
using Field = Lucene.Net.Documents.Field;
using DateTools = Lucene.Net.Documents.DateTools;
using Filter = Lucene.Net.Search.Filter;
using System.IO;
using System.Runtime.InteropServices;
using Aspose.Slides;
using System.Threading;
using System.Collections;
using System.Windows.Forms;

namespace Client.BLL
{
    class LuceneSearch
    {
        #region Class Variables
        DirectoryInfo DirToIndex = new System.IO.DirectoryInfo(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),Properties.Settings.Default.AppDataDirectory.ToString() + @"\Resources\Ppt"));
        DirectoryInfo INDEX_DIR = new System.IO.DirectoryInfo(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),Properties.Settings.Default.AppDataDirectory.ToString() + @"\Index"));

        //int fCount = 0;
        //int IndexCount = 0;
        //System.ComponentModel.BackgroundWorker _BackgroundWorkerFullIndex;
        //System.ComponentModel.BackgroundWorker _BackgroundWorkerUpdateIndex;
        private DataSets.MyRegionLanguages _MyRegionLanguages;
        #endregion

        #region Properties
        List<SearchItem> _search_items = new List<SearchItem>();
        int _hits;
        string _query;
        string _error;

        public List<SearchItem> SearchItems
        {
            get
            {
                return _search_items;
            }
        }
        public string Query
        {
            get
            {
                return _query;
            }
        }
        public int ResultHits
        {
            get
            {
                return _hits;
            }
        }
        public string ErrorMsg
        {
            get
            {
                return _error;
            }
        }

        #endregion

        #region Constructor
        public LuceneSearch()
        {
        }
        #endregion

        #region Public Methods
        public LuceneSearch Basic_Search(string search_string)
        {
            //Instantiate new LucenSearch object to hold results
            LuceneSearch ls = new LuceneSearch();
            LoadMyRegionLanguages();
            wsIpgSlideUpdater.SlideLibrary sld = Get_XML_For_Category_Search();
            try
            {              
                
                //Declare new SearchItem object so that we can add this to the LucenSearch object later.
                SearchItem si;

                Lucene.Net.Store.Directory dir = Lucene.Net.Store.FSDirectory.GetDirectory(INDEX_DIR.FullName, false);

                Searcher searcher = new IndexSearcher(INDEX_DIR.FullName);
                Analyzer analyzer = new StandardAnalyzer();

                if (search_string.Length > 0)
                {
                    Query query = new QueryParser("contents", analyzer).Parse(search_string);
                    
                    ls._query = query.ToString();
                    Hits hits = searcher.Search(query);
                    //set how many hits we received from this query.
                    ls._hits = hits.Length();

                    int end = hits.Length();
                    for (int i = 0; i < end; i++)
                    {
                        //Instantiate the new searchitem object so we can add the results of the query doc 
                        //to this object
                        si = new SearchItem();

                        Document doc = hits.Doc(i);
                        String path = doc.Get("path");
                        if (path != null)
                        {
                            
                            try
                            {
                                si.CategoryID = Convert.ToInt32(doc.Get("categoryid"));
                            }
                            catch (Exception)
                            {
                                si.CategoryID = 0;
                            }
                            try
                            {
                                if (doc.Get("path").ToString().Substring(doc.Get("path").ToString().LastIndexOf(".")).ToLower() == ".pot" ||
                                    doc.Get("path").ToString().Substring(doc.Get("path").ToString().LastIndexOf(".")).ToLower() == ".ppt")
                                {
                                    si.Category = Get_Category(sld, Convert.ToInt32(doc.Get("categoryid")));
                                }
                                else
                                {
                                    si.Category = "";
                                }
                            }
                            catch (Exception)
                            {
                                si.Category = "";
                            }
                            try
                            {
                                si.SlideID = Convert.ToInt32(doc.Get("slideid"));
                            }
                            catch (Exception)
                            {
                                si.SlideID = 0;
                            }
                            si.FileName = doc.Get("path").ToString();
                            //si.ModifiedDate =  DateTime.Parse(doc.Get("modified").ToString());
                            si.ModifiedDate = DateTime.Parse(DateTools.StringToDate(doc.Get("modified").ToString()).ToString());                            
                            si.Title = doc.Get("title").ToString();
                            si.RegionLangID = Convert.ToInt32(doc.Get("regionlang"));
                            try
                            {
                                si.RegionLang = Get_RegionLang(sld, Convert.ToInt32(doc.Get("regionlang")));
                            }
                            catch (Exception)
                            {
                                si.RegionLang = "";
                            }
                            
                        }
                        else
                        {
                            System.String url = doc.Get("url");
                            if (url != null)
                            {
                                
                                try
                                {
                                    si.CategoryID = Convert.ToInt32(doc.Get("categoryid"));
                                }
                                catch (Exception)
                                {
                                    si.CategoryID = 0;
                                }
                                try
                                {
                                    if (doc.Get("url").ToString().Substring(doc.Get("url").ToString().LastIndexOf(".")).ToLower() == ".pot" ||
                                        doc.Get("url").ToString().Substring(doc.Get("url").ToString().LastIndexOf(".")).ToLower() == ".ppt")
                                    {
                                        si.Category = Get_Category(sld, Convert.ToInt32(doc.Get("categoryid")));
                                    }
                                    else
                                    {
                                        si.Category = "";
                                    }
                                }
                                catch (Exception)
                                {
                                    si.Category = "";
                                }
                                try
                                {
                                    si.SlideID = Convert.ToInt32(doc.Get("slideid"));
                                }
                                catch (Exception)
                                {
                                    si.SlideID = 0;
                                }
                                si.FileName = doc.Get("url").ToString();
                                si.ModifiedDate = DateTime.Parse(DateTools.StringToDate(doc.Get("modified").ToString()).ToString());     
                                si.Title = doc.Get("title").ToString();
                                si.RegionLangID = Convert.ToInt32(doc.Get("regionlang"));
                                try
                                {
                                    si.RegionLang = Get_RegionLang(sld, Convert.ToInt32(doc.Get("regionlang")));
                                }
                                catch (Exception)
                                {
                                    si.RegionLang = "";
                                }
                            }
                        }
                        ls._search_items.Add(si);
                    }

                }
                searcher.Close();
            }
            catch (System.Exception ex)
            {
                ls._error = " caught a " + ex.GetType() + "\n with message: " + ex.Message;
            }
            return ls;
        }
        public LuceneSearch Advanced_Search(List<string> categories, string content, string filename, string title, DateTime begin_date, DateTime end_date)
        {
            //Instantiate new LucenSearch object to hold results
            LuceneSearch ls = new LuceneSearch();
            LoadMyRegionLanguages();
            
            try
            {

               

                Lucene.Net.Store.Directory dir = Lucene.Net.Store.FSDirectory.GetDirectory(INDEX_DIR.FullName, false);

                Searcher searcher = new IndexSearcher(INDEX_DIR.FullName);
                Analyzer analyzer = new StandardAnalyzer();

                //Lucene.Net.Search.Query query = new Lucene.Net.Search.TermQuery(new Lucene.Net.Index.Term("contents", content));
                
                //Create a Boolean query
                Lucene.Net.Search.BooleanQuery bq = new Lucene.Net.Search.BooleanQuery();
                Lucene.Net.Search.BooleanQuery bqText = new Lucene.Net.Search.BooleanQuery();
                Lucene.Net.Search.BooleanQuery bqCats = new Lucene.Net.Search.BooleanQuery();
                Lucene.Net.Search.BooleanQuery bqRegs = new Lucene.Net.Search.BooleanQuery();

                //Declare the search terms and filters
                Query qContent;
                Query qCategory;
                Query qFilename;
                Query qTitle;
                Query qRegions;
                Filter fDateRange;


                //Flags
                bool useText = false;
                bool useDate = false;
                bool useCats = false;

                //Create the query for the search terms - if a file has the search term in either contents, path or title then it should be returned.
                if (!string.IsNullOrEmpty(content))
                {

                    string[] contentSplit = content.Split(' ');
                    foreach (string s in contentSplit)
                    {
                        qContent = new Lucene.Net.Search.TermQuery(new Lucene.Net.Index.Term("contents", s));
                        bqText.Add(qContent, Lucene.Net.Search.BooleanClause.Occur.SHOULD);
                        useText = true;

                    }
                    
                }                
                if (!string.IsNullOrEmpty(filename))
                {
                    qFilename = new Lucene.Net.Search.TermQuery(new Lucene.Net.Index.Term("path", filename));
                    bqText.Add(qFilename, Lucene.Net.Search.BooleanClause.Occur.SHOULD);
                    useText = true;
                }
                if (!string.IsNullOrEmpty(title))
                {
                    string[] titleSplit = title.Split(' ');
                    foreach (string s in titleSplit)
                    {
                        qTitle = new Lucene.Net.Search.TermQuery(new Lucene.Net.Index.Term("title", s));
                        bqText.Add(qTitle, Lucene.Net.Search.BooleanClause.Occur.SHOULD);
                        useText = true;
                    }
                }
                //Create the categories query - If a file has any of the top level categories selected as it's top level category then it should be shown.
                if (categories.Count > 0)
                {
                    foreach (string cat in categories)
                    {
                        qCategory = new Lucene.Net.Search.TermQuery(new Lucene.Net.Index.Term("categoryparent", cat));
                        bqCats.Add(qCategory, Lucene.Net.Search.BooleanClause.Occur.SHOULD);
                        useCats = true;
                    }
                    
                }

                //Only search region languages that the user is subscribed too
                if (_MyRegionLanguages._MyRegionLanguages.Count > 0)
                {
                    foreach (Client.DataSets.MyRegionLanguages.MyRegionLanguagesRow mlr in _MyRegionLanguages._MyRegionLanguages)
                    {
                        qRegions = new Lucene.Net.Search.TermQuery(new Lucene.Net.Index.Term("regionlang", mlr.RegionLanguageID.ToString()));
                        bqRegs.Add(qRegions, Lucene.Net.Search.BooleanClause.Occur.SHOULD);
                    }
                }

                //Create the query for date range. - The file must have a modified date within the date range.
                Lucene.Net.Index.Term beginDate = new Lucene.Net.Index.Term("modified", DateTools.TimeToString(begin_date.Date.Ticks, DateTools.Resolution.MINUTE));
                Lucene.Net.Index.Term endDate = new Lucene.Net.Index.Term("modified", DateTools.TimeToString(end_date.AddSeconds(59 - end_date.Second).AddMinutes(59 - end_date.Minute).AddHours(23 - end_date.Hour).Ticks, DateTools.Resolution.MINUTE));
                Lucene.Net.Search.RangeQuery rQuery = new Lucene.Net.Search.RangeQuery(beginDate, endDate, true);

                //Add the sub queries to the original query.
                bq.Add(rQuery, Lucene.Net.Search.BooleanClause.Occur.MUST);
                //Add in the text queries
                if(useText)
                    bq.Add(bqText, Lucene.Net.Search.BooleanClause.Occur.MUST);
                //Add the categories query to the original query
                if(useCats)
                    bq.Add(bqCats, Lucene.Net.Search.BooleanClause.Occur.MUST);
                //Add Region Filter to query
                bq.Add(bqRegs, Lucene.Net.Search.BooleanClause.Occur.MUST);

                SearchItem si;
                ls._query = bq.ToString();
                Hits hits = searcher.Search(bq);

                //Load the results
                ls = Load_Search_Results(hits);

                searcher.Close();
                
            }
            catch (System.Exception ex)
            {
                ls._error = " caught a " + ex.GetType() + "\n with message: " + ex.Message;
            }
            return ls;              

                    
        }

        public bool is_index_valid()
        {
            bool retVal = true;



            //Text Search criteria
            string strContent = "hp";
            string strFileName = "";
            string strTitle = "";
            DateTime beginDate = DateTime.Parse("1/1/1972");
            DateTime endDate = DateTime.Now;
            
            //Category Search Criteria
            List<string> strCats = new List<string>();

            //Search is built now run search and populate the lucenesearch object with the results
            LuceneSearch ls = new LuceneSearch();
            ls = ls.Advanced_Search(strCats, strContent, strFileName, strTitle, beginDate, endDate);

            if (ls.ResultHits < 1)
                retVal = false;
            return retVal;
        }
        private LuceneSearch Load_Search_Results(Hits hits)
        {
            LuceneSearch ls = new LuceneSearch();
            SearchItem si;
            wsIpgSlideUpdater.SlideLibrary sld = Get_XML_For_Category_Search();
            ls._hits = hits.Length();
            int end = hits.Length();
            for (int i = 0; i < end; i++)
            {
                //Instantiate the new searchitem object so we can add the results of the query doc 
                //to this object
                si = new SearchItem();

                Document doc = hits.Doc(i);
                String path = doc.Get("path");
                if (path != null)
                {

                    try
                    {
                        si.CategoryID = Convert.ToInt32(doc.Get("categoryid"));
                    }
                    catch (Exception)
                    {
                        si.CategoryID = 0;
                    }
                    try
                    {
                        if (doc.Get("path").ToString().Substring(doc.Get("path").ToString().LastIndexOf(".")).ToLower() == ".pot" ||
                            doc.Get("path").ToString().Substring(doc.Get("path").ToString().LastIndexOf(".")).ToLower() == ".ppt")
                        {
                            si.Category = Get_Category(sld, Convert.ToInt32(doc.Get("categoryid")));
                        }
                        else
                        {
                            si.Category = "";
                        }
                    }
                    catch (Exception)
                    {
                        si.Category = "";
                    }
                    try
                    {
                        si.SlideID = Convert.ToInt32(doc.Get("slideid"));
                    }
                    catch (Exception)
                    {
                        si.SlideID = 0;
                    }
                    si.FileName = doc.Get("path").ToString();
                    //si.ModifiedDate =  DateTime.Parse(doc.Get("modified").ToString());
                    si.ModifiedDate = DateTime.Parse(DateTools.StringToDate(doc.Get("modified").ToString()).ToString());
                    si.Title = doc.Get("title").ToString();
                    si.RegionLangID = Convert.ToInt32(doc.Get("regionlang"));
                    try
                    {
                        si.RegionLang = Get_RegionLang(sld, Convert.ToInt32(doc.Get("regionlang")));
                    }
                    catch (Exception)
                    {
                        si.RegionLang = "";
                    }
                    si.Top_Category = doc.Get("categoryparent");

                }
                else
                {
                    System.String url = doc.Get("url");
                    if (url != null)
                    {

                        try
                        {
                            si.CategoryID = Convert.ToInt32(doc.Get("categoryid"));
                        }
                        catch (Exception)
                        {
                            si.CategoryID = 0;
                        }
                        try
                        {
                            if (doc.Get("url").ToString().Substring(doc.Get("url").ToString().LastIndexOf(".")).ToLower() == ".pot" ||
                                doc.Get("url").ToString().Substring(doc.Get("url").ToString().LastIndexOf(".")).ToLower() == ".ppt")
                            {
                                si.Category = Get_Category(sld, Convert.ToInt32(doc.Get("categoryid")));
                            }
                            else
                            {
                                si.Category = "";
                            }
                        }
                        catch (Exception)
                        {
                            si.Category = "";
                        }
                        try
                        {
                            si.SlideID = Convert.ToInt32(doc.Get("slideid"));
                        }
                        catch (Exception)
                        {
                            si.SlideID = 0;
                        }
                        si.FileName = doc.Get("url").ToString();
                        si.ModifiedDate = DateTime.Parse(DateTools.StringToDate(doc.Get("modified").ToString()).ToString());
                        si.Title = doc.Get("title").ToString();
                        si.RegionLangID = Convert.ToInt32(doc.Get("regionlang"));
                        try
                        {
                            si.RegionLang = Get_RegionLang(sld, Convert.ToInt32(doc.Get("regionlang")));
                        }
                        catch (Exception)
                        {
                            si.RegionLang = "";
                        }
                        si.Top_Category = doc.Get("categoryparent");
                    }
                }
                ls._search_items.Add(si);
            }
            //int end = hits.Length();
            //for (int i = 0; i < end; i++)
            //{
            //    //Instantiate the new searchitem object so we can add the results of the query doc 
            //    //to this object
            //    si = new SearchItem();

            //    Document doc = hits.Doc(i);
            //    String path = doc.Get("path");
            //    if (path != null)
            //    {
            //        si.Category = doc.Get("categoryname").ToString();
            //        try
            //        {
            //            si.CategoryID = Convert.ToInt32(doc.Get("categoryid"));
            //        }
            //        catch (Exception)
            //        {
            //            si.CategoryID = 0;
            //        }
            //        try
            //        {
            //            si.SlideID = Convert.ToInt32(doc.Get("slideid"));
            //        }
            //        catch (Exception)
            //        {
            //            si.SlideID = 0;
            //        }
            //        si.FileName = doc.Get("path").ToString();
            //        //si.ModifiedDate =  DateTime.Parse(doc.Get("modified").ToString());
            //        si.ModifiedDate = DateTime.Parse(DateTools.StringToDate(doc.Get("modified").ToString()).ToString());
            //        si.Title = doc.Get("title").ToString();
            //        si.RegionLangID = Convert.ToInt32(doc.Get("regionlang"));
            //        try
            //        {
            //            si.RegionLang = Get_RegionLang(sld, Convert.ToInt32(doc.Get("regionlang")));
            //        }
            //        catch (Exception)
            //        {
            //            si.RegionLang = "";
            //        }
            //    }
            //    else
            //    {
            //        System.String url = doc.Get("url");
            //        if (url != null)
            //        {
            //            si.Category = doc.Get("categoryname").ToString();
            //            try
            //            {
            //                si.CategoryID = Convert.ToInt32(doc.Get("categoryid"));
            //            }
            //            catch (Exception)
            //            {
            //                si.CategoryID = 0;
            //            }
            //            try
            //            {
            //                si.SlideID = Convert.ToInt32(doc.Get("slideid"));
            //            }
            //            catch (Exception)
            //            {
            //                si.SlideID = 0;
            //            }
            //            si.FileName = doc.Get("url").ToString();
            //            si.ModifiedDate = DateTime.Parse(DateTools.StringToDate(doc.Get("modified").ToString()).ToString());
            //            si.Title = doc.Get("title").ToString();
            //            si.RegionLangID = Convert.ToInt32(doc.Get("regionlang"));
            //            try
            //            {
            //                si.RegionLang = Get_RegionLang(sld, Convert.ToInt32(doc.Get("regionlang")));
            //            }
            //            catch (Exception)
            //            {
            //                si.RegionLang = "";
            //            }
            //        }
            //    }
            //    ls._search_items.Add(si);
            //}
            
            return ls;

        }

        #endregion


        private string Get_Category(wsIpgSlideUpdater.SlideLibrary sld, int cat_id)
        {
            string retCat = "";

            //Add filter of Filename and get all the rows for that file.
            string filter = "CategoryID=" + cat_id;
            List<wsIpgSlideUpdater.SlideLibrary.CategoryRow> catr = new List<Client.wsIpgSlideUpdater.SlideLibrary.CategoryRow>((wsIpgSlideUpdater.SlideLibrary.CategoryRow[])sld.Category.Select(filter));
            if (catr.Count > 0)
            {
                retCat = catr[0].Category;
            }


            return retCat;
        }

        private string Get_RegionLang(wsIpgSlideUpdater.SlideLibrary sld, int reg_lang_id)
        {
            string retRL = "";

            Client.wsIpgSlideUpdater.SlideLibrary.RegionLanguageRow rlRow = sld.RegionLanguage.FindByRegionLanguageID(reg_lang_id);
            if (rlRow != null)
            {
                retRL = String.Format("{0}/{1}",
                    rlRow.RegionRow.ShortName,
                     rlRow.LanguageRow.LongName);                
                //retRL = String.Format("{0}/{1}",
                //    sld.RegionLanguage.FindByRegionLanguageID(reg_lang_id).RegionRow.ShortName,
                //    sld.RegionLanguage.FindByRegionLanguageID(reg_lang_id).LanguageRow.LongName);
            }


            return retRL;
        }

        private wsIpgSlideUpdater.SlideLibrary Get_XML_For_Category_Search()
        {

            wsIpgSlideUpdater.SlideLibrary dsTempSlideLibrary = new Client.wsIpgSlideUpdater.SlideLibrary();
            string mainData = Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, AppSettings.LocalFiles.Default.MainData);
            try
            {
                dsTempSlideLibrary.ReadXml(mainData);
            }
            catch
            {
                HandleCorruptedFile(mainData);
            }
            return dsTempSlideLibrary;
            
        }

        private void LoadMyRegionLanguages()
        {
            //get my languages
            _MyRegionLanguages = new Client.DataSets.MyRegionLanguages();
            _MyRegionLanguages.ReadXml(Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, AppSettings.LocalFiles.Default.MyRegionLangauges));
        }

        private void HandleCorruptedFile(string file)
        {
            MessageBox.Show("The system encountered a corrupted file.  Please restart the application to refresh the data.  The application will now close.");
            try
            {
                File.Delete(file);
            }
            catch
            {
                //Fail Silently
            }
            Application.Exit();
        }


    }
}
