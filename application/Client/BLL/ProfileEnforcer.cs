﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Client.BLL
{
    public class ProfileEnforcer
    {
        
        private wsUserRegistration.UserRegistrationService userService;

        public ProfileEnforcer()
        {
             userService = new Client.wsUserRegistration.UserRegistrationService();
        }
        public wsUserRegistration.UserRegistrationDS.UserProfileDataTable GetProfile(string email)
        {
            return userService.GetUserByEmail(email);
        }
        public void Enforce()
        {
            wsUserRegistration.UserRegistrationDS.UserProfileDataTable profile = GetProfile(Properties.Settings.Default.UserRegistrationEmail);
            if (HasInvalidProperties(profile))
            {
                Profile profileForm = new Profile("Please complete your profile information before proceeding.");
                profileForm.Show();
            }
        }

        private bool HasInvalidProperties(wsUserRegistration.UserRegistrationDS.UserProfileDataTable profile)
        {
            if (profile.Rows.Count < 1)
                return false;

            wsUserRegistration.UserRegistrationDS.UserProfileRow row = profile.Rows[0] as wsUserRegistration.UserRegistrationDS.UserProfileRow;
            bool result = false;
            if (
                row.IsEmailNull() || String.IsNullOrEmpty(row.Email) ||
                row.IsFirstNameNull() || String.IsNullOrEmpty(row.FirstName) ||
                row.IsLastNameNull() || String.IsNullOrEmpty(row.LastName) ||
                row.IsPhoneNull() || String.IsNullOrEmpty(row.Phone) ||
                row.IsCountryNull() || String.IsNullOrEmpty(row.Country) ||
                row.IsRegionIdNull() || row.RegionId.Equals(0) ||
                row.IsCompanyNameNull() || String.IsNullOrEmpty(row.CompanyName) ||
                row.IsCompanySizeNull() || row.CompanySize.Equals(0) ||
                row.IsJobTitleNull() || String.IsNullOrEmpty(row.JobTitle) ||
                row.IsReferenceSourceNull() || String.IsNullOrEmpty(row.ReferenceSource)
               )
            {
                result = true;
            }
            bool hasIncorrectJobTitle = HasIncorrectJobTitle(row);
            return result || hasIncorrectJobTitle;
        }

        private bool HasIncorrectJobTitle(wsUserRegistration.UserRegistrationDS.UserProfileRow row)
        {
            bool hasIncorrectJobTitle = true;
            if (!row.IsJobTitleNull())
            {
                string jobTitle = row.JobTitle;
                wsUserRegistration.UserRegistrationDS.JobTitlesDataTable jobTitles = userService.GetJobTitles();
                foreach (wsUserRegistration.UserRegistrationDS.JobTitlesRow jobTitlesRow in jobTitles)
                {
                    if (jobTitlesRow.Code == jobTitle)
                    {
                        hasIncorrectJobTitle = false;
                        break;
                    }
                }
                if (hasIncorrectJobTitle)
                {
                    row.SetJobTitleNull();
                }
            }
            return hasIncorrectJobTitle;
        }
    }
}
