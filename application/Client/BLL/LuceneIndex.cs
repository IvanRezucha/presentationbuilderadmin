﻿using System;
using System.Collections.Generic;
using System.Text;  
using Analyzer = Lucene.Net.Analysis.Analyzer;
using StandardAnalyzer = Lucene.Net.Analysis.Standard.StandardAnalyzer;
using Document = Lucene.Net.Documents.Document;
using QueryParser = Lucene.Net.QueryParsers.QueryParser;
using Hits = Lucene.Net.Search.Hits;
using IndexSearcher = Lucene.Net.Search.IndexSearcher;
using Query = Lucene.Net.Search.Query;
using Searcher = Lucene.Net.Search.Searcher;
using IndexWriter = Lucene.Net.Index.IndexWriter;
using Field = Lucene.Net.Documents.Field;
using DateTools = Lucene.Net.Documents.DateTools;
//using Microsoft.Office.Interop.Word;
using System.IO;
using System.Runtime.InteropServices;
using Aspose.Slides;
using System.Threading;
using Client.wsUsageLogging;

namespace Client.BLL
{
    class LuceneIndex
    {
        #region Class Variables
        DirectoryInfo DirToIndex = new System.IO.DirectoryInfo(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),Properties.Settings.Default.AppDataDirectory.ToString() + @"\Resources\Ppt"));
        DirectoryInfo INDEX_DIR = new System.IO.DirectoryInfo(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),Properties.Settings.Default.AppDataDirectory.ToString() + @"\Index"));
        DirectoryInfo TBI_DIR = new System.IO.DirectoryInfo(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), Properties.Settings.Default.AppDataDirectory.ToString() + @"\Resources\"));
        int fCount = 0;
        int IndexCount = 0;
        System.ComponentModel.BackgroundWorker _BackgroundWorkerFullIndex;
        System.ComponentModel.BackgroundWorker _BackgroundWorkerUpdateIndex;
        private DataSets.MyRegionLanguages _MyRegionLanguages;
        List<string> errorList = new List<string>();
        private bool stop_and_clear_index = false;
        //private List<string> fullIndexList = new List<string>();
        //private int errCount = 0;
        //private int regCount = 0;
        #endregion

        #region Constructor
        public LuceneIndex()
        {
        }
        #endregion

        # region Threads
        private void _BackgroundWorkerFullIndex_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            IndexUpdateCompleted();
            Update_Index_Files((List<string>)e.Result);
        }
        private void _BackgroundWorkerFullIndex_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            IndexUpdateProgressReported(e.ProgressPercentage);
        }
        private void _BackgroundWorkerFullIndex_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {            
        }
        private void _BackgroundWorkerUpdateIndex_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            IndexUpdateCompleted();
        }
        private void _BackgroundWorkerUpdateIndex_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            IndexUpdateProgressReported(e.ProgressPercentage);
        }
        private void _BackgroundWorkerUpdateIndex_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            //Starting index process. Set flags so the program will know and it can be continued later.
            Client.Properties.Settings.Default.IndexComplete = false;
            Client.Properties.Settings.Default.IndexInProgress = true;
            Client.Properties.Settings.Default.Save();

            //Call Index Setup
            Lucene.Net.Index.IndexWriter writer = Index_Setup(false);
            //Load regions so that we can update the xml with the new index date.
            LoadMyRegionLanguages();
            FileInfo fi;
            int rCount = 0;
            List<string> fis = (List<string>)e.Argument;
            //create another list to manipulate and save to the filesystem in case this process stops and we want to restart
            List<string> tbi = new List<string>(fis);
            List<string> indexList = new List<string>(fis);
            foreach (string f in indexList)
            {
                
                if (!stop_and_clear_index)
                {
                    rCount++;
                    fi = new FileInfo(f);
                    update_file_in_index(fi, writer);
                    //UpdateIndexDate(fi.Name);
                    _BackgroundWorkerUpdateIndex.ReportProgress((int)Math.Round(((float)rCount / (float)fis.Count) * 100), "Indexing..." + rCount + "/" + fis.Count);
                    tbi.RemoveAt(0);
                    Save_TBI_File(tbi);
                }
            }
            Close_Index(writer);
            SendErrors();
            if (stop_and_clear_index)
            {
                Clear_Index();
                Clear_TBI_File();
               Client.Properties.Settings.Default.IndexComplete = false;
               Client.Properties.Settings.Default.IndexInProgress = false;
               Client.Properties.Settings.Default.Save();
            }
            else
            {
               Client.Properties.Settings.Default.IndexComplete = true;
               Client.Properties.Settings.Default.IndexInProgress = false;
               Client.Properties.Settings.Default.Save();
            }
        }
        #endregion

        #region Private Index Methods
        private List<string> Build_Full_Dir_List(System.IO.DirectoryInfo root, List<string> fis)
        {
            int rCount = 0;
            System.IO.FileInfo[] files = null;
            System.IO.DirectoryInfo[] subDirs = null;

            // First, process all the files directly under this folder
            try
            {
                files = root.GetFiles("*.*");
            }
            // This is thrown if even one of the files requires permissions greater
            // than the application provides.
            catch (UnauthorizedAccessException ex)
            {
            }

            catch (System.IO.DirectoryNotFoundException ex)
            {
            }

            if (files != null)
            {
                foreach (System.IO.FileInfo fi in files)
                {
                    if (!stop_and_clear_index)
                    {
                        try
                        {
                            fis.Add(fi.FullName);
                            //iw.AddDocument(StartIndexDocument(fi));
                            //UpdateIndexDate(fi.Name);
                        }
                        catch (Exception)
                        {
                        }
                        finally
                        {
                            rCount++;
                            //_BackgroundWorkerFullIndex.ReportProgress((int)Math.Round(((float)rCount / (float)fCount) * 100), "Indexing..." + rCount + "/" + fCount);
                        }
                    }

                }

                // Now find all the subdirectories under this directory.
                subDirs = root.GetDirectories();

                foreach (System.IO.DirectoryInfo dirInfo in subDirs)
                {
                    // Resursive call for each subdirectory.
                    //GetDirectoryTree(dirInfo, iw);
                    Build_Full_Dir_List(dirInfo, fis);
                }
            }
            return fis;
        }
        private void GetDirectoryTree(System.IO.DirectoryInfo root, IndexWriter iw)
        {
            //System.Windows.Forms.MessageBox.Show("Dir Tree:" + root.ToString());
            int rCount = 0;
            System.IO.FileInfo[] files = null;
            System.IO.DirectoryInfo[] subDirs = null;

            // First, process all the files directly under this folder
            try
            {
                //System.Windows.Forms.MessageBox.Show("Dir Tree: Get Files");
                files = root.GetFiles("*.*");
            }
            // This is thrown if even one of the files requires permissions greater
            // than the application provides.
            catch (UnauthorizedAccessException ex)
            {
                //lblMsg.Text = ex.Message;
            }

            catch (System.IO.DirectoryNotFoundException ex)
            {
                //lblMsg.Text = ex.Message;
            }

            if (files != null)
            {
                //System.Windows.Forms.MessageBox.Show("files is not null");
                //System.Windows.Forms.MessageBox.Show("Dir Tree: Get Files");
                foreach (System.IO.FileInfo fi in files)
                {
                    if (!stop_and_clear_index)
                    {
                        //indexFile(fi.FullName, iw);
                        try
                        {
                            //if(rCount == 0 || rCount % 25 == 0)
                            //System.Windows.Forms.MessageBox.Show(rCount.ToString());

                            iw.AddDocument(StartIndexDocument(fi));
                            //UpdateIndexDate(fi.Name);

                        }
                        catch (Exception)
                        {

                        }
                        finally
                        {
                            rCount++;
                            //step_progress();
                            //frm.UpdateIndexProgress((rCount / fCount) * 100);
                            //IndexUpdateProgressReported((int)Math.Round(((float)rCount / (float)fCount) * 100));
                            _BackgroundWorkerFullIndex.ReportProgress((int)Math.Round(((float)rCount / (float)fCount) * 100), "Indexing..." + rCount + "/" + fCount);
                        }
                    }

                }

                // Now find all the subdirectories under this directory.
                subDirs = root.GetDirectories();

                foreach (System.IO.DirectoryInfo dirInfo in subDirs)
                {
                    // Resursive call for each subdirectory.
                    GetDirectoryTree(dirInfo, iw);
                }
            }
        }
        private void Clear_Index()
        {
            Directory.Delete(INDEX_DIR.FullName,true);
            stop_and_clear_index = false;
        }
        private Document StartIndexDocument(System.IO.FileInfo f)
        {
            // Make a new, empty document
            Document doc = new Document();

            //Load the xml and get the record so that we can get the info we need out of it.
            wsIpgSlideUpdater.SlideLibrary.SlideCategoryRow XMLRecord;
            XMLRecord = Load_XML_For_File(f.Name);
            wsIpgSlideUpdater.SlideLibrary XML_SL;
            XML_SL = Get_XML_Slide_Library();
            int catID = Get_Category_ID(XMLRecord);

            //Add all the fields to the index
            doc.Add(new Field("categoryid", catID.ToString(), Field.Store.YES, Field.Index.UN_TOKENIZED));
            doc.Add(new Field("categoryname", Get_Category_Name(XMLRecord).ToString(), Field.Store.YES, Field.Index.UN_TOKENIZED));
            doc.Add(new Field("regionlang", Get_Reg_Lang_ID(XMLRecord).ToString(), Field.Store.YES, Field.Index.UN_TOKENIZED));
            doc.Add(new Field("categoryparent", Get_Category_Parent(XML_SL,catID), Field.Store.YES, Field.Index.UN_TOKENIZED));
            doc.Add(new Field("title", Get_Title(XMLRecord).ToString(), Field.Store.YES, Field.Index.UN_TOKENIZED));
            doc.Add(new Field("slideid", Get_Slide_ID(XMLRecord).ToString(), Field.Store.YES, Field.Index.UN_TOKENIZED));
            doc.Add(new Field("path", f.FullName, Field.Store.YES,Field.Index.UN_TOKENIZED));
            doc.Add(new Field("modified",DateTools.TimeToString(f.LastWriteTime.Ticks,DateTools.Resolution.MINUTE),Field.Store.YES, Field.Index.UN_TOKENIZED));
            //adding contents field
            if (f.Extension.ToLower() == ".doc" || f.Extension.ToLower() == ".docx")
            {
                string tmp = ReadDOC(f.FullName);
                if (tmp != null)
                {
                    Lucene.Net.Documents.Field fldContent = new Lucene.Net.Documents.Field("contents", tmp, Lucene.Net.Documents.Field.Store.NO, Lucene.Net.Documents.Field.Index.TOKENIZED, Lucene.Net.Documents.Field.TermVector.YES);
                    doc.Add(fldContent);
                }
            }
            else if (f.Extension.ToLower() == ".ppt" || f.Extension.ToLower() == ".pot")
            {
                string tmp = ReadPPT(f.FullName);
                if (tmp != null)
                {
                    Lucene.Net.Documents.Field fldContent = new Lucene.Net.Documents.Field("contents", tmp, Lucene.Net.Documents.Field.Store.NO, Lucene.Net.Documents.Field.Index.TOKENIZED, Lucene.Net.Documents.Field.TermVector.YES);
                    doc.Add(fldContent);
                }
            }
            else if (f.Extension.ToLower() == ".pdf" || f.Extension.ToLower() == ".pdf")
            {
                string tmp = ReadPDF(f.FullName);
                if (tmp != null)
                {
                    Lucene.Net.Documents.Field fldContent = new Lucene.Net.Documents.Field("contents", tmp, Lucene.Net.Documents.Field.Store.NO, Lucene.Net.Documents.Field.Index.TOKENIZED, Lucene.Net.Documents.Field.TermVector.YES);
                    doc.Add(fldContent);
                }
            }
            else
            {
                doc.Add(new Field("contents", new System.IO.StreamReader(f.FullName, System.Text.Encoding.Default)));
            }
            // Return the document
            return doc;
        }
        private wsIpgSlideUpdater.SlideLibrary Get_XML_Slide_Library()
        {

            wsIpgSlideUpdater.SlideLibrary dsTempSlideLibrary = new Client.wsIpgSlideUpdater.SlideLibrary();
            string mutexName = AppSettings.LocalFiles.Default.MainData;
            Mutex m = new Mutex(false, mutexName);
            m.WaitOne();
            dsTempSlideLibrary.ReadXml(Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, AppSettings.LocalFiles.Default.MainData));
            m.ReleaseMutex();

            return dsTempSlideLibrary;

        }        
        private wsIpgSlideUpdater.SlideLibrary.SlideCategoryRow Load_XML_For_File(string fName)
        {
            wsIpgSlideUpdater.SlideLibrary.SlideCategoryRow retXML = null;
            //LoadMyRegionLanguages();
            List<int> myRegLangIds = new List<int>();
            foreach (DataSets.MyRegionLanguages.MyRegionLanguagesRow myRegLang in _MyRegionLanguages._MyRegionLanguages.Rows)
            {//Iterate through Regions
                myRegLangIds.Add(myRegLang.RegionLanguageID);
                //Get XMl file for Region
                string localizedDataFile = Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, String.Format("{0}{1}", myRegLang.RegionLanguageID.ToString(), AppSettings.LocalFiles.Default.SlideDataFilePart));
                if (File.Exists(localizedDataFile))
                {
                    wsIpgSlideUpdater.SlideLibrary _SlideData = new Client.wsIpgSlideUpdater.SlideLibrary();
                    _SlideData.EnforceConstraints = false;
                    string mutexName = String.Format("{0}{1}", myRegLang.RegionLanguageID.ToString(), AppSettings.LocalFiles.Default.SlideDataFilePart);
                    Mutex m = new Mutex(false, mutexName);
                    m.WaitOne(); 
                    _SlideData.ReadXml(localizedDataFile);
                    m.ReleaseMutex();

                    //Add filter of Filename and get all the rows for that file.
                    string filter = "FileName='" + fName + "'";
                    List<wsIpgSlideUpdater.SlideLibrary.SlideRow> sld = new List<Client.wsIpgSlideUpdater.SlideLibrary.SlideRow>((wsIpgSlideUpdater.SlideLibrary.SlideRow[])_SlideData.Slide.Select(filter));
                    if (sld.Count > 0)
                    {
                        //Add filter of Filename and get all the rows for that file.
                        filter = "SlideID=" + sld[0].SlideID.ToString();
                        List<wsIpgSlideUpdater.SlideLibrary.SlideCategoryRow> sldC = new List<Client.wsIpgSlideUpdater.SlideLibrary.SlideCategoryRow>((wsIpgSlideUpdater.SlideLibrary.SlideCategoryRow[])_SlideData.SlideCategory.Select(filter));
                        if (sldC.Count > 0)
                        {
                            retXML = sldC[0];
                        }
                    }
                }
            }

            return retXML;
        }
        private int Get_Category_ID(wsIpgSlideUpdater.SlideLibrary.SlideCategoryRow xmlRecord)
        {
            int retCat = 0;
            try
            {
                retCat = xmlRecord.CategoryID;
            }
            catch (Exception)
            {
            }
            return retCat;
        }
        private int Get_Reg_Lang_ID(wsIpgSlideUpdater.SlideLibrary.SlideCategoryRow xmlRecord)
        {
            int retCat = 0;
            try
            {
                retCat = xmlRecord.RegionLanguageID;
            }
            catch (Exception)
            {
            }
            return retCat;
        }
        private string Get_Category_Name(wsIpgSlideUpdater.SlideLibrary.SlideCategoryRow xmlRecord)
        {
            string retCat = "";
            try
            {
                retCat = xmlRecord.CategoryRow.Category;
            }
            catch (Exception)
            {
            }
            return retCat;
        }        
        private string Get_Category_Parent(wsIpgSlideUpdater.SlideLibrary sld, int cat_id)
        {
            string retCat = "";
            if (cat_id > 0)
            {                
                try
                {
                    //Add filter of Filename and get all the rows for that file.
                    string filter = "CategoryID=" + cat_id.ToString();
                    List<wsIpgSlideUpdater.SlideLibrary.CategoryRow> catr = new List<Client.wsIpgSlideUpdater.SlideLibrary.CategoryRow>((wsIpgSlideUpdater.SlideLibrary.CategoryRow[])sld.Category.Select(filter));
                    if (catr.Count > 0)
                    {
                        if (string.IsNullOrEmpty(catr[0].ParentCategoryID.ToString()) || catr[0].ParentCategoryID == 21)
                            retCat = catr[0].Category;
                        else
                            retCat = Get_Category_Parent(sld, catr[0].ParentCategoryID);
                    }
                }
                catch (Exception)
                {
                }
            }
            return retCat;
        }       
        private int Get_Slide_ID(wsIpgSlideUpdater.SlideLibrary.SlideCategoryRow xmlRecord)
        {
            int retCat = 0;
            try
            {
                retCat = xmlRecord.SlideID;
            }
            catch (Exception)
            {
            }
            return retCat;
        }
        private string Get_Title(wsIpgSlideUpdater.SlideLibrary.SlideCategoryRow xmlRecord)
        {
            string retCat = "";
            try
            {
                retCat = xmlRecord.SlideRow.Title;
            }
            catch (Exception)
            {
            }            

            return retCat;
        }
        private Lucene.Net.Index.IndexWriter Index_Setup(bool bCreateIndex)
        {
            //Delete index files if they exist
            if (bCreateIndex && INDEX_DIR.Exists)
            {
                INDEX_DIR.Delete(true);
            }
            else if (!bCreateIndex && !INDEX_DIR.Exists)
            {
                bCreateIndex = true;
            }
            //System.Windows.Forms.MessageBox.Show(INDEX_DIR.FullName);
            //state the file location of the index
            Lucene.Net.Store.Directory dir = Lucene.Net.Store.FSDirectory.GetDirectory(INDEX_DIR.FullName, bCreateIndex);
            //System.Windows.Forms.MessageBox.Show(dir.ToString());

            //create an analyzer to process the text
            Lucene.Net.Analysis.Analyzer analyzer = new
            Lucene.Net.Analysis.Standard.StandardAnalyzer();

            //create the index writer with the directory and analyzer defined.
            Lucene.Net.Index.IndexWriter indexWriter = new Lucene.Net.Index.IndexWriter(dir, analyzer, bCreateIndex);

            return indexWriter;
        }
        private void Close_Index(Lucene.Net.Index.IndexWriter writer)
        {
            //optimize and close the writer
            writer.Optimize();
            writer.Close();
        }
        private string ReadPDF(string path)
        {
            PdfToText.PDFParser pdfParser = new PdfToText.PDFParser();


            // extract the text
            String result = pdfParser.ExtractText(path);

            return result;
        }
        private string ReadDOC(string path)
        {
            DocToText.DocParser docParser = new DocToText.DocParser();

            // extract the text
            String result = docParser.extractDocText(path);

            return result;
        }
        private string ReadPPT(string path)
        {
            //Aspose.Slides.License lic = new Aspose.Slides.License();
            //lic.SetLicense("Aspose.Slides.lic");

            string strTemp = "";
            Aspose.Slides.Presentation tempPresentation;
            try
            {
                tempPresentation = GetPresentationObject(path);
            }
            catch (Exception ex)
            {
                Add_Error(ex.Message, path);
                return null;
            }
            
            //if (tempPresentation == null)
                //return null;
            try
            {


                Slide sld = tempPresentation.GetSlideByPosition(1);
                foreach (Aspose.Slides.Shape shp in sld.Shapes)
                {
                    if (shp is Aspose.Slides.Rectangle)
                    {
                        TextFrame tf = shp.TextFrame;
                        strTemp += tf.Text + Environment.NewLine;

                    }
                    else if (shp.IsTextHolder)
                    {
                        TextHolder th = shp.Placeholder as TextHolder;
                        //Console.WriteLine(th.Text);
                        if(th != null)
                        strTemp += th.Text + Environment.NewLine;
                        //th.Text = th.Text + " changed";
                    }
                    else
                    {
                        if (shp.TextFrame != null)
                        {
                            Aspose.Slides.TextFrame tf = shp.TextFrame;
                            //Console.WriteLine(tf.Text);
                            //tf.Text = tf.Text + " changed";
                            strTemp += tf.Text + Environment.NewLine;
                        }
                        else
                        {
                            if (shp.GetType() == typeof(Aspose.Slides.Table))
                            {
                                //System.out.println("i m in");
                                Aspose.Slides.Table tab = (Aspose.Slides.Table)shp;
                                //strTemp += tab.GetCell(0, 0).TextFrame;//getTextFrame().getParagraphs().get(0).getPortions().get(0).getText();

                                //Aspose.Slides.TextFrame tf = tab.GetCell(0, 0).TextFrame;
                                //foreach(Aspose.Slides.Table r in tab.row
                                //If text frame is not null then add some text to the cell

                                for (int r = 0; r < tab.RowsNumber; r++)
                                {
                                    for (int c = 0; c < tab.ColumnsNumber; c++)
                                    {
                                        if (tab.GetCell(c, r).TextFrame != null)
                                        {
                                            foreach (Aspose.Slides.Paragraph par in tab.GetCell(c, r).TextFrame.Paragraphs)
                                            {
                                                strTemp += par.Text + Environment.NewLine;
                                            }
                                        }
                                    }
                                }


                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Add_Error(ex.Message, path);
                return null;
            }
            return strTemp;
        }
        private static Aspose.Slides.Presentation GetPresentationObject(string presentationFilePath)
        {
            //try
            //{
                FileStream presStream = new FileStream(presentationFilePath, FileMode.Open, FileAccess.Read);
                Aspose.Slides.Presentation tempPresentation = new Aspose.Slides.Presentation(presStream);
                presStream.Close();
                return tempPresentation;

            //}
            //catch (Exception ex)
            //{
                
            //    return null;
            //}

        }       
        /*
        private void UpdateIndexDate(string fn)
        {
            //LoadMyRegionLanguages();
            List<int> myRegLangIds = new List<int>();
            foreach (DataSets.MyRegionLanguages.MyRegionLanguagesRow myRegLang in _MyRegionLanguages._MyRegionLanguages.Rows)
            {//Iterate through Regions
                myRegLangIds.Add(myRegLang.RegionLanguageID);
                //Get XMl file for Region
                string localizedDataFile = Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, String.Format("{0}{1}", myRegLang.RegionLanguageID.ToString(), AppSettings.LocalFiles.Default.SlideDataFilePart));
                if (File.Exists(localizedDataFile))
                {//If xml file exists then load it into Slide library
                    wsIpgSlideUpdater.SlideLibrary dsLocalizedSlideData = new Client.wsIpgSlideUpdater.SlideLibrary();
                    dsLocalizedSlideData.EnforceConstraints = false;
                    string mutexName = String.Format("{0}{1}", myRegLang.RegionLanguageID.ToString(), AppSettings.LocalFiles.Default.SlideDataFilePart);
                    Mutex m = new Mutex(false, mutexName);
                    m.WaitOne();
                    dsLocalizedSlideData.ReadXml(localizedDataFile);
                    m.ReleaseMutex();
                    
                    //Add filter of Filename and get all the rows for that file.
                    string filter = "FileName='" + fn + "'";
                    List<wsIpgSlideUpdater.SlideLibrary.SlideRow> sld = new List<Client.wsIpgSlideUpdater.SlideLibrary.SlideRow>((wsIpgSlideUpdater.SlideLibrary.SlideRow[])dsLocalizedSlideData.Slide.Select(filter));
                    if (sld.Count > 0)
                    {//If the file is in the xml then update the Index date and save the xml.
                        //sld[0].BeginEdit();
                        if (sld[0].Table.Columns["DateIndexed"].ReadOnly)
                            sld[0].Table.Columns["DateIndexed"].ReadOnly = false;
                        sld[0].DateIndexed = DateTime.Now;
                        sld[0].AcceptChanges();

                        string mutexName2 = String.Format("{0}{1}", myRegLang.RegionLanguageID.ToString(), AppSettings.LocalFiles.Default.SlideDataFilePart);
                        Mutex m2 = new Mutex(false, mutexName2);
                        m2.WaitOne();
                        dsLocalizedSlideData.WriteXml(Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, String.Format("{0}{1}", myRegLang.RegionLanguageID.ToString(), AppSettings.LocalFiles.Default.SlideDataFilePart)));
                        m2.ReleaseMutex();
                    }
                }
            }
        }
       */
        private void remove_file_from_index(FileInfo fi)
        {
            
            Lucene.Net.Index.IndexReader ir = Lucene.Net.Index.IndexReader.Open(INDEX_DIR.FullName);
            //ir.DeleteDocument(1);
            ir.DeleteDocuments(new Lucene.Net.Index.Term("path", fi.FullName));
            ir.Close();
        }
        private void update_file_in_index(FileInfo fi, Lucene.Net.Index.IndexWriter writer)
        {
            try
            {
                //regCount++;
                //IndexWriter writer = new IndexWriter(INDEX_DIR.FullName, new StandardAnalyzer(), false);            
                Lucene.Net.Index.IndexReader ir = Lucene.Net.Index.IndexReader.Open(INDEX_DIR.FullName);
                //ir.DeleteDocument(1);
                ir.DeleteDocuments(new Lucene.Net.Index.Term("path", fi.FullName));
                writer.AddDocument(StartIndexDocument(fi));
                ir.Close();
            }
            catch (Exception)
            {
                //errCount++;
            }
            
        }
        #endregion

        #region Public Methods
        public void Stop_and_Clear_Index()
        {
            stop_and_clear_index = true;
        }
        public void Clean_up_Index()
        {
            try
            {
                Clear_Index();
                Clear_TBI_File();
                Client.Properties.Settings.Default.IndexComplete = false;
                Client.Properties.Settings.Default.IndexInProgress = false;
                Client.Properties.Settings.Default.Save();      
            }
            catch (Exception)
            {
            }
                

        }
        public void index_all()
        {
            ////Create new list toBeIndexed
            //List<string> fis = new List<string>();

            ////starts a thread and calls the do work operation to handle the work int he background.
            ////sets the background thread
            //_BackgroundWorkerFullIndex = new System.ComponentModel.BackgroundWorker();
            //_BackgroundWorkerFullIndex.DoWork += new System.ComponentModel.DoWorkEventHandler(_BackgroundWorkerFullIndex_DoWork);
            //_BackgroundWorkerFullIndex.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(_BackgroundWorkerFullIndex_ProgressChanged);
            //_BackgroundWorkerFullIndex.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(_BackgroundWorkerFullIndex_RunWorkerCompleted);
            //_BackgroundWorkerFullIndex.WorkerReportsProgress = true;
            //_BackgroundWorkerFullIndex.RunWorkerAsync(fis);
            
            //Now send 
            //Update_Index_Files(fullIndexList);

            //This should just build a list of files to index and send to the Update_Index_Files method to process.
            List<string> fis = new List<string>();

            //Iterate full directory tree to build index list.
            fis = Build_Full_Dir_List(DirToIndex, fis);

            Update_Index_Files(fis);
        }       
        public void Update_Index_Files(List<string> fis)
        {
            //starts a thread and calls the do work operation to handle the work int he background.
            //sets the background thread
            _BackgroundWorkerUpdateIndex = new System.ComponentModel.BackgroundWorker();
            _BackgroundWorkerUpdateIndex.DoWork += new System.ComponentModel.DoWorkEventHandler(_BackgroundWorkerUpdateIndex_DoWork);
            _BackgroundWorkerUpdateIndex.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(_BackgroundWorkerUpdateIndex_ProgressChanged);
            _BackgroundWorkerUpdateIndex.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(_BackgroundWorkerUpdateIndex_RunWorkerCompleted);
            _BackgroundWorkerUpdateIndex.WorkerReportsProgress = true;
            _BackgroundWorkerUpdateIndex.RunWorkerAsync(fis);

        }
        public void Remove_Index_Files(List<string> fis)
        {
            FileInfo fi;
            foreach (string f in fis)
            {
                fi = new FileInfo(f);
                remove_file_from_index(fi);
            }
        }
        public bool Index_Exists()
        {
            //Lucene.Net.Store.Directory dir = Lucene.Net.Store.FSDirectory.GetDirectory(INDEX_DIR.FullName,false);
            return Lucene.Net.Index.IndexReader.IndexExists(INDEX_DIR.FullName);
        }
        public void Continue_Index()
        {
            
            try
            {
                Update_Index_Files(Read_TBI_File());
            }
            catch (Exception ex)
            {
            }   
        }
        #endregion

        private void SendErrors()
        {
            //UsageLoggingService service = new UsageLoggingService();
            //service.SaveIndexingErrorsAsync(errorList.ToArray());
            errorList.Clear();
        }

        #region Helper Methods
        private int GetFileCount(System.IO.DirectoryInfo root)
        {
            System.IO.FileInfo[] files = null;
            System.IO.DirectoryInfo[] subDirs = null;

            // First, process all the files directly under this folder
            try
            {
                files = root.GetFiles("*.*");
            }
            // This is thrown if even one of the files requires permissions greater
            // than the application provides.
            catch (UnauthorizedAccessException ex)
            {
                //lblMsg.Text = ex.Message;
            }

            catch (System.IO.DirectoryNotFoundException ex)
            {
                //lblMsg.Text = ex.Message;
            }



            if (files != null)
            {
                foreach (System.IO.FileInfo fi in files)
                {
                    //indexFile(fi.FullName, iw);
                    IndexCount++;
                }

                // Now find all the subdirectories under this directory.
                subDirs = root.GetDirectories();

                foreach (System.IO.DirectoryInfo dirInfo in subDirs)
                {
                    // Resursive call for each subdirectory.
                    GetFileCount(dirInfo);
                }
            }

            return IndexCount;
        }        
        private void LoadMyRegionLanguages()
        {
            //get my languages
            _MyRegionLanguages = new Client.DataSets.MyRegionLanguages();
            string mutexName = AppSettings.LocalFiles.Default.MyRegionLangauges;
            Mutex m = new Mutex(false, mutexName);
            m.WaitOne();
            _MyRegionLanguages.ReadXml(Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, AppSettings.LocalFiles.Default.MyRegionLangauges));
            m.ReleaseMutex();
            
        }
        private void Add_Error(string ex, string s)
        {
            string filename = System.IO.Path.GetFileName(s);
            errorList.Add(filename + ";!;" + s + ";!;Exception: " + ex);
        }
        private void Save_TBI_File(List<string> fis)
        {
            string mutexName = "tbi";
            Mutex m = new Mutex(false, mutexName);
            m.WaitOne();
            File.WriteAllLines(TBI_DIR.FullName + "tbi.txt", fis.ToArray());
            m.ReleaseMutex();
             
        }
        private List<string> Read_TBI_File()
        {
            List<string> fis = new List<string>();
            string mutexName = "tbi";
            Mutex m = new Mutex(false, mutexName);
            m.WaitOne();
            using (StreamReader r = new StreamReader(TBI_DIR.FullName + "tbi.txt"))
            {
                // Use while != null pattern for loop
                string line;
                while ((line = r.ReadLine()) != null)
                {
                    fis.Add(line);
                }
            }
            m.ReleaseMutex();
            

            return fis;
        }
        public List<string> Merge_New_TBI(List<string> newfis)
        {
            List<string> oldfis = new List<string>();
            string mutexName = "tbi";
            Mutex m = new Mutex(false, mutexName);
            m.WaitOne();
            using (StreamReader r = new StreamReader(TBI_DIR.FullName + "tbi.txt"))
            {
                // Use while != null pattern for loop
                string line;
                while ((line = r.ReadLine()) != null)
                {
                    oldfis.Add(line);
                }
            }
            m.ReleaseMutex();
            oldfis.AddRange(newfis);

            Save_TBI_File(oldfis);

            return oldfis;
        }
        private void Clear_TBI_File()
        {
            string mutexName = "tbi";
            Mutex m = new Mutex(false, mutexName);
            m.WaitOne();
            File.Delete(TBI_DIR.FullName + "tbi.txt");
            m.ReleaseMutex();
        }
        #endregion         

        #region events and event delegates
        public delegate void IndexUpdateCompletedDelegate();
        public event IndexUpdateCompletedDelegate IndexUpdateCompleted;
        public delegate void IndexUpdateProgressReportedDelegate(int progressPercent);
        public event IndexUpdateProgressReportedDelegate IndexUpdateProgressReported;
        #endregion

    }
}
