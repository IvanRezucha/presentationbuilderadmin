﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.IO;

namespace Client.BLL
{
    public class RegionLanaguageAccessor
    {
        public RegionLanaguageAccessor()
        {
        }
        public wsIpgSlideUpdater.SlideLibrary GetRegionLanguagesDataSet()
        {
            wsIpgSlideUpdater.SlideLibrary dsRegionLanguages = new Client.wsIpgSlideUpdater.SlideLibrary();
            dsRegionLanguages.EnforceConstraints = false;
            //always try to update this file
            //there may be changes to the RegLangs
            try
            {
                string serverRegLangFileURI = String.Format("{0}{1}/{2}", Properties.Settings.Default.DataUpdateBaseURI, "LocalizedUpdateData", "RegionLanguages.xml");
                dsRegionLanguages.ReadXml(serverRegLangFileURI);
                dsRegionLanguages.WriteXml(Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, "RegionLanguages.xml"));
            }
            catch (Exception ex)
            {
                //do nothing
            }
            finally
            {
                dsRegionLanguages.Clear();
            }

            //if the file exists the ContentUpdater has already downloaded this file
            //if not, go get it from the server--this most likely means a new install
            if (File.Exists(Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, "RegionLanguages.xml")))
            {
                dsRegionLanguages.ReadXml(Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, "RegionLanguages.xml"));
            }
            else
            {
                string serverRegLangFileURI = String.Format("{0}{1}/{2}", Properties.Settings.Default.DataUpdateBaseURI, "LocalizedUpdateData", "RegionLanguages.xml");
                dsRegionLanguages.ReadXml(serverRegLangFileURI);
                dsRegionLanguages.WriteXml(Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, "RegionLanguages.xml"));
            }
            
            return dsRegionLanguages;
        }

        public int GetRegionIdFromMyRegionLanaguages(int myRegionLanguage)
        {
            wsIpgSlideUpdater.SlideLibrary dsRegionLanguages = GetRegionLanguagesDataSet();
            foreach (wsIpgSlideUpdater.SlideLibrary.RegionLanguageRow row in dsRegionLanguages.RegionLanguage)
            {
                if(row.RegionLanguageID.Equals(myRegionLanguage))
                    return row.RegionID;
            }

            return 0;
        }
    }
}
