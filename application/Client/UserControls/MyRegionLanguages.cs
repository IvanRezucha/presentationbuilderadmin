using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Client.UserControls
{
    public partial class MyRegionLanguages : Form
    {
        public MyRegionLanguages(string xmlDirectory)
        {
            InitializeComponent();
            wsIpgSlideUpdater.SlideLibrary dsMainData = new Client.wsIpgSlideUpdater.SlideLibrary();
            dsMainData.ReadXml(mainDataFile);
            regionLanguageGroupGrid.FillRegionLanguages(dsMainData);
        }

        private void MyRegionLanguages_Load(object sender, EventArgs e)
        {

        }


    }
}