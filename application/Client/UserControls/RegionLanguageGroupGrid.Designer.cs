namespace Client.UserControls
{
    partial class RegionLanguageGroupGrid
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ggRegionLanguages = new Wirestone.WinForms.GroupGrid.GroupGrid();
            ((System.ComponentModel.ISupportInitialize)(this.ggRegionLanguages)).BeginInit();
            this.SuspendLayout();
            // 
            // ggRegionLanguages
            // 
            this.ggRegionLanguages.AllowUserToAddRows = false;
            this.ggRegionLanguages.AllowUserToDeleteRows = false;
            this.ggRegionLanguages.AllowUserToResizeColumns = false;
            this.ggRegionLanguages.BackgroundColor = System.Drawing.Color.White;
            this.ggRegionLanguages.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ggRegionLanguages.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.ggRegionLanguages.CollapseIcon = global::Client.Properties.Resources.CollapsePNG;
            this.ggRegionLanguages.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ggRegionLanguages.ColumnHeadersVisible = false;
            this.ggRegionLanguages.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ggRegionLanguages.ExpandIcon = global::Client.Properties.Resources.ExpandPNG;
            this.ggRegionLanguages.Location = new System.Drawing.Point(0, 0);
            this.ggRegionLanguages.Name = "ggRegionLanguages";
            this.ggRegionLanguages.RowHeadersVisible = false;
            this.ggRegionLanguages.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ggRegionLanguages.Size = new System.Drawing.Size(257, 314);
            this.ggRegionLanguages.TabIndex = 1;
            // 
            // RegionLanguageGroupGrid
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.ggRegionLanguages);
            this.Name = "RegionLanguageGroupGrid";
            this.Size = new System.Drawing.Size(257, 314);
            ((System.ComponentModel.ISupportInitialize)(this.ggRegionLanguages)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Wirestone.WinForms.GroupGrid.GroupGrid ggRegionLanguages;
    }
}
