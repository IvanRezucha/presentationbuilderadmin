namespace Client.UserControls
{
    partial class MyRegionLanguages
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.regionLanguageGroupGrid = new Client.UserControls.RegionLanguageGroupGrid();
            this.SuspendLayout();
            // 
            // regionLanguageGroupGrid
            // 
            this.regionLanguageGroupGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.regionLanguageGroupGrid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.regionLanguageGroupGrid.Location = new System.Drawing.Point(-1, -1);
            this.regionLanguageGroupGrid.Name = "regionLanguageGroupGrid";
            this.regionLanguageGroupGrid.Size = new System.Drawing.Size(430, 357);
            this.regionLanguageGroupGrid.TabIndex = 0;
            // 
            // MyRegionLanguages
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(428, 476);
            this.Controls.Add(this.regionLanguageGroupGrid);
            this.Name = "MyRegionLanguages";
            this.Text = "My Regions and Languages";
            this.Load += new System.EventHandler(this.MyRegionLanguages_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private RegionLanguageGroupGrid regionLanguageGroupGrid;
    }
}