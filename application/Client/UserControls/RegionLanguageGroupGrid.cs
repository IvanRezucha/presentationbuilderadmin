using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Client.BLL;

namespace Client.UserControls
{
    public partial class RegionLanguageGroupGrid : Wirestone.WinForms.GroupGrid.GroupGrid
    {        
        public const int CHECK_COLUMN_INDEX = 7;
        //private const int DISPLAY_COLUMN_INDEX = 6;
        RegionLanguageControlDS _RegionLanguageDS;

        private DataSets.MyRegionLanguages _MyRegLangs;
		private DataSets.MyRegionLanguages _LocalRegLangs;
        List<int> _OriginalRegionLanguageIds;

        private bool _IsNewRegionLanguagesSelected;

        public bool IsNewRegionLanguagesSelected
        {
            get { return _IsNewRegionLanguagesSelected; }
            private set { _IsNewRegionLanguagesSelected = value; }
        }


        public RegionLanguageGroupGrid()
        {   
            this.CellBorderStyle = DataGridViewCellBorderStyle.None;
            this.AllowUserToAddRows = false;
            this.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            this.RowHeadersVisible = false;
            this.BackgroundColor = Color.White;
            this.AutoGenerateColumns = false;
            this.ColumnHeadersVisible = false;
            _MyRegLangs = new Client.DataSets.MyRegionLanguages();
            _OriginalRegionLanguageIds = new List<int>();


            //FillRegionLanguageGroupGrid();
            //LoadMyRegionLanguages();

        }

        #region public region/language methods
        
        /// <summary>
        /// Fills the region languages.
        /// </summary>
        public void FillRegionLanguages(wsIpgSlideUpdater.SlideLibrary slideLibrary)
        {
            //grid columns
            //0-RegionLanguageID
            //1-RegionID
            //2-LanguageID
            //3-Region
            //4-Language
            //5-RegionSortOrder
            //6-LanguageSortOrder
            //7-CheckBox
            //8-blank/filler

            //fill the RegionLanguages DataSet
            _RegionLanguageDS = new RegionLanguageControlDS();
            foreach (wsIpgSlideUpdater.SlideLibrary.RegionLanguageRow regLang in slideLibrary.RegionLanguage.Rows)
            {
                RegionLanguageControlDS.RegionLanguageRow rlRow = _RegionLanguageDS.RegionLanguage.NewRegionLanguageRow();
                rlRow.RegionID = regLang.RegionID;
                rlRow.LanguageID = regLang.LanguageID;
                rlRow.RegionLanguageID = regLang.RegionLanguageID;
                rlRow.Region = regLang.RegionRow.RegionName;
                rlRow.Language = regLang.LanguageRow.LongName;
                rlRow.RegionSortOrder = regLang.RegionRow.SortOrder;
                rlRow.LanguageSortOrder = regLang.LanguageRow.SortOrder;
                _RegionLanguageDS.RegionLanguage.AddRegionLanguageRow(rlRow);
            }
			
            //bind data to group grid
            this.BindData(_RegionLanguageDS, _RegionLanguageDS.RegionLanguage.TableName);
			
            //this sets up the grouping--3 is the Region column
            this.GroupTemplate = new Wirestone.WinForms.GroupGrid.GroupGridDefaultGroup();
            this.GroupTemplate.Column = this.Columns[3];
            this.Sort(new Wirestone.WinForms.GroupGrid.GroupGridDataRowComparer(5, ListSortDirection.Ascending));

            //hide the primary and foreign key columns
            this.Columns[0].Visible = false;
            this.Columns[1].Visible = false;
            this.Columns[2].Visible = false;
            this.Columns[3].Visible = false;

            this.ExpandAll();

            //add a checkbox column
            DataGridViewCheckBoxColumn col1 = new DataGridViewCheckBoxColumn();
            col1.HeaderText = "";
            this.Columns.Add(col1);

            ////add a checkbox column
            //DataGridViewCheckBoxColumn col2 = new DataGridViewCheckBoxColumn();
            //col2.HeaderText = "Display";
            //this.Columns.Add(col2);

            //add a blank column to fill the blank grid area
            DataGridViewTextBoxColumn col3 = new DataGridViewTextBoxColumn();
            col3.HeaderText = "";
            col3.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            this.Columns.Add(col3);

            //setup additional column properties
            this.Columns[4].ReadOnly = true;
            this.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            this.Columns[5].Visible = false;
            this.Columns[6].Visible = false;
            this.Columns[7].ReadOnly = false;
            this.Columns[8].ReadOnly = true;

            ////setup additional column properties
            //this.Columns[4].ReadOnly = true;
            //this.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            //this.Columns[5].ReadOnly = false;
            //this.Columns[6].ReadOnly = false;
            ////this.Columns[7].ReadOnly = true;

			string localLanguagesPath = Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, AppSettings.LocalFiles.Default.LocallyAvailableLanguages);

            //let's try to fit more of these into view
            foreach (Wirestone.WinForms.GroupGrid.GroupGridRow ggRow in this.Rows)
            {
				//if this is a CD install we have to check and limit the region/languages in the selection
				//but only if this is the initial install
				try
				{
					if (Properties.Settings.Default.InstallType == PlugIns.Definitions.InstallationType.CD &&
						this.Parent.Parent.Parent.Parent.Name.Equals("LocalizedContentSetupWizardCdVersion"))
					{
						_LocalRegLangs = new Client.DataSets.MyRegionLanguages();
						if (File.Exists(localLanguagesPath))
						{
							_LocalRegLangs.ReadXml(localLanguagesPath);
						}

						//check to see if each RegionLanguage is not locally available, if it's not, disable it
						if (!ggRow.IsGroupRow)
						{
							if (_LocalRegLangs._MyRegionLanguages.FindByRegionLanguageID(Convert.ToInt32(ggRow.Cells[0].Value)) == null)
							{
								ggRow.ReadOnly = true;
								ggRow.DefaultCellStyle.BackColor = Color.Gray;
							}
							else
							{
								ggRow.ReadOnly = false;
								ggRow.DefaultCellStyle.BackColor = Color.White;
							}
						}
					}
				}
				catch(NullReferenceException ex)
				{
					//do nothing
				}
                if (!ggRow.IsGroupRow)
                {
                    ggRow.Height = 23;
                }
				ggRow.Resizable = DataGridViewTriState.False;
            }

        }

        /// <summary>
        /// Gets the selected download region language I ds.
        /// </summary>
        /// <returns></returns>
        public List<int> GetSelectedRegionLanguageIDs()
        {
            return GetSelectedRegionLanguageIDs(CHECK_COLUMN_INDEX);
        }

        /// <summary>
        /// Checks the download slide region languages.
        /// </summary>
        /// <param name="regionLanguageIDs">The region language I ds.</param>
        public void CheckSlideRegionLanguages(List<int> regionLanguageIDs)
        {
            CheckSlideRegionLanguages(regionLanguageIDs, CHECK_COLUMN_INDEX);
        }

        /// <summary>
        /// Checks all download slide region languages.
        /// </summary>
        public void CheckAllSlideRegionLanguages()
        {
            CheckAllSlideRegionLanguages(CHECK_COLUMN_INDEX);
        }

        /// <summary>
        /// Uncheck all downloadslide region languages.
        /// </summary>
        private void UnCheckAllSlideRegionLanguages()
        {
            UnCheckAllSlideRegionLanguages(CHECK_COLUMN_INDEX);
        }

        ///// <summary>
        ///// Gets the selected display region language I ds.
        ///// </summary>
        ///// <returns></returns>
        //public List<int> GetSelectedDisplayRegionLanguageIDs()
        //{
        //    return GetSelectedRegionLanguageIDs(DISPLAY_COLUMN_INDEX);
        //}

        ///// <summary>
        ///// Checks the display slide region languages.
        ///// </summary>
        ///// <param name="regionLanguageIDs">The region language I ds.</param>
        //public void CheckDisplaySlideRegionLanguages(List<int> regionLanguageIDs)
        //{
        //    CheckSlideRegionLanguages(regionLanguageIDs, DISPLAY_COLUMN_INDEX);
        //}

        ///// <summary>
        ///// Checks all display slide region languages.
        ///// </summary>
        //public void CheckAllDisplaySlideRegionLanguages()
        //{
        //    CheckAllSlideRegionLanguages(DISPLAY_COLUMN_INDEX);
        //}

        ///// <summary>
        ///// Uncheck all display slide region languages.
        ///// </summary>
        //private void UnCheckDisplayAllSlideRegionLanguages()
        //{
        //    UnCheckAllSlideRegionLanguages(DISPLAY_COLUMN_INDEX);
        //}

        /// <summary>
        /// Collapses all groups.
        /// </summary>
        public void CollapseAllGroups()
        {
            this.CollapseAll();
        }

        /// <summary>
        /// Expands all groups.
        /// </summary>
        public void ExpandAllGroups()
        {
            this.ExpandAll();
        }

        #endregion

        #region private methods

        /// <summary>
        /// Gets the selected region language IDs.
        /// </summary>
        /// <returns></returns>
        private List<int> GetSelectedRegionLanguageIDs(int columnIndex)
        {
            List<int> selectedIDs = new List<int>();
            foreach (Wirestone.WinForms.GroupGrid.GroupGridRow row in this.Rows)
            {
                if (!row.IsGroupRow)
                {
                    if (Convert.ToBoolean(row.Cells[CHECK_COLUMN_INDEX].Value) == true)
                    {
                        selectedIDs.Add(Convert.ToInt32(row.Cells[0].Value));
                    }
                }
            }

            return selectedIDs;
        }

        /// <summary>
        /// Checks the slide region languages.
        /// </summary>
        /// <param name="regionLanguageIDs">The region language I ds.</param>
        private void CheckSlideRegionLanguages(List<int> regionLanguageIDs, int columnIndex)
        {
            foreach (Wirestone.WinForms.GroupGrid.GroupGridRow row in this.Rows)
            {
                if (!row.IsGroupRow)
                {
                    if (regionLanguageIDs.Contains(Convert.ToInt32(row.Cells[0].Value)))
                    {
                        row.Cells[5].Value = true;
                    }
                }
            }
        }

        /// <summary>
        /// Checks all slide region languages.
        /// </summary>
        private void CheckAllSlideRegionLanguages(int columnIndex)
        {
            foreach (Wirestone.WinForms.GroupGrid.GroupGridRow row in this.Rows)
            {
                if (!row.IsGroupRow)
                {
                    row.Cells[5].Value = true;
                }
            }
        }

        /// <summary>
        /// Checks all slide region languages.
        /// </summary>
        private void UnCheckAllSlideRegionLanguages(int columnIndex)
        {
            foreach (Wirestone.WinForms.GroupGrid.GroupGridRow row in this.Rows)
            {
                if (!row.IsGroupRow)
                {
                    row.Cells[5].Value = false;
                }
            }
        }

        #endregion

        public bool SaveMyRegionLanguages()
        {

            if (ValdiateForm())
            {
                _MyRegLangs._MyRegionLanguages.Clear();
                foreach (int selectedRegLangId in this.GetSelectedRegionLanguageIDs())
                {
                    _MyRegLangs._MyRegionLanguages.AddMyRegionLanguagesRow(selectedRegLangId);

                }
                _MyRegLangs.WriteXml(Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, AppSettings.LocalFiles.Default.MyRegionLangauges));
                UpdateIsNewRegionLanguagesSelectedStatus();
                return true;
            }
            else
            {
                MessageBox.Show("Minimum Required", "You must select at least one Region/Language.", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }

        }

        public void LoadMyRegionLanguages()
        {
            _MyRegLangs = new Client.DataSets.MyRegionLanguages();
            if (File.Exists(Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, AppSettings.LocalFiles.Default.MyRegionLangauges)))
            {
                _MyRegLangs.ReadXml(Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, AppSettings.LocalFiles.Default.MyRegionLangauges));
            }


            //check to see if each RegionLanguage is a MyRegionLanguage
            //if it is, check it
            foreach (Wirestone.WinForms.GroupGrid.GroupGridRow row in this.Rows)
            {
                if (!row.IsGroupRow)
                {
                    if (_MyRegLangs._MyRegionLanguages.FindByRegionLanguageID(Convert.ToInt32(row.Cells[0].Value)) != null)
                    {
                        row.Cells[CHECK_COLUMN_INDEX].Value = true;
                    }
                }
            }

            //we'll use this list later to see if the user selected any new RegLangs
            _OriginalRegionLanguageIds = this.GetSelectedRegionLanguageIDs();
            IsNewRegionLanguagesSelected = false;

        }

        private void UpdateIsNewRegionLanguagesSelectedStatus()
        {
            foreach (int regLangId in this.GetSelectedRegionLanguageIDs())
            {
                if (!_OriginalRegionLanguageIds.Contains(regLangId))
                {
                    IsNewRegionLanguagesSelected = true;
                }
            }
            //IsNewRegionLanguagesSelected = false;
        }

        private bool ValdiateForm()
        {
            if (this.GetSelectedRegionLanguageIDs().Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void FillRegionLanguageGroupGrid()
        {

            RegionLanaguageAccessor regionLanguageAccessor = new RegionLanaguageAccessor();
            wsIpgSlideUpdater.SlideLibrary dsRegionLanguages = regionLanguageAccessor.GetRegionLanguagesDataSet();

            //we don't want to show any RegionLanguages where the Region IsClientDefault
            //put'em in a list
            List<int> isClientDefaultRegionIds = new List<int>();
            foreach (wsIpgSlideUpdater.SlideLibrary.RegionLanguageRow regLang in dsRegionLanguages.RegionLanguage.Rows)
            {
                if (regLang.RegionRow.IsClientDefault)
                {
                    isClientDefaultRegionIds.Add(regLang.RegionLanguageID);
                }
            }
            //now delete'em
            foreach (int regLangId in isClientDefaultRegionIds)
            {
                dsRegionLanguages.RegionLanguage.FindByRegionLanguageID(regLangId).Delete();
            }

            dsRegionLanguages.AcceptChanges();

            this.FillRegionLanguages(dsRegionLanguages);
        }
    }
}
