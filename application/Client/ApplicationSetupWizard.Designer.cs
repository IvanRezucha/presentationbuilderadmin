namespace Client
{
    partial class ApplicationSetupWizard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ApplicationSetupWizard));
            this.tabPageWelcome = new System.Windows.Forms.TabPage();
            this.btnWelcomeNext = new System.Windows.Forms.Button();
            this.panelWelcome = new System.Windows.Forms.Panel();
            this.lblWelcomMessage = new System.Windows.Forms.Label();
            this.richTextBoxWelcomeTitle = new System.Windows.Forms.RichTextBox();
            this.pictureBoxBigIcon = new System.Windows.Forms.PictureBox();
            this.tabPageRegionLanguages = new System.Windows.Forms.TabPage();
            this.btnRegLangNext = new System.Windows.Forms.Button();
            this.panelRegionLanguages = new System.Windows.Forms.Panel();
            this.regionLanguageGroupGrid = new Client.UserControls.RegionLanguageGroupGrid();
            this.lblRegLangMessage = new System.Windows.Forms.Label();
            this.tabPageContentUpdateProgress = new System.Windows.Forms.TabPage();
            this.btnFinished = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblFinished = new System.Windows.Forms.Label();
            this.lblWait = new System.Windows.Forms.Label();
            this.lblWorkerStatus = new System.Windows.Forms.Label();
            this.progressBarContentUpdate = new System.Windows.Forms.ProgressBar();
            this.tabWizard.SuspendLayout();
            this.tabPageWelcome.SuspendLayout();
            this.panelWelcome.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBigIcon)).BeginInit();
            this.tabPageRegionLanguages.SuspendLayout();
            this.panelRegionLanguages.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.regionLanguageGroupGrid)).BeginInit();
            this.tabPageContentUpdateProgress.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // gradientCaptionWizardTitle
            // 
            this.gradientCaptionWizardTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.gradientCaptionWizardTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gradientCaptionWizardTitle.Location = new System.Drawing.Point(0, 0);
            this.gradientCaptionWizardTitle.Name = "gradientCaptionWizardTitle";
            this.gradientCaptionWizardTitle.Size = new System.Drawing.Size(550, 65);
            this.gradientCaptionWizardTitle.TabIndex = 4;
            this.gradientCaptionWizardTitle.Text = "Application Setup Wizard";
            // 
            // tabWizard
            // 
            this.tabWizard.Controls.Add(this.tabPageWelcome);
            this.tabWizard.Controls.Add(this.tabPageRegionLanguages);
            this.tabWizard.Controls.Add(this.tabPageContentUpdateProgress);
            // 
            // tabPageWelcome
            // 
            this.tabPageWelcome.Controls.Add(this.btnWelcomeNext);
            this.tabPageWelcome.Controls.Add(this.panelWelcome);
            this.tabPageWelcome.Location = new System.Drawing.Point(4, 22);
            this.tabPageWelcome.Name = "tabPageWelcome";
            this.tabPageWelcome.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageWelcome.Size = new System.Drawing.Size(552, 405);
            this.tabPageWelcome.TabIndex = 1;
            this.tabPageWelcome.Text = "tabPage1";
            this.tabPageWelcome.UseVisualStyleBackColor = true;
            // 
            // btnWelcomeNext
            // 
            this.btnWelcomeNext.Location = new System.Drawing.Point(465, 363);
            this.btnWelcomeNext.Name = "btnWelcomeNext";
            this.btnWelcomeNext.Size = new System.Drawing.Size(75, 23);
            this.btnWelcomeNext.TabIndex = 1;
            this.btnWelcomeNext.Text = "Next >";
            this.btnWelcomeNext.UseVisualStyleBackColor = true;
            this.btnWelcomeNext.Click += new System.EventHandler(this.btnWelcomeNext_Click);
            // 
            // panelWelcome
            // 
            this.panelWelcome.BackColor = System.Drawing.Color.White;
            this.panelWelcome.Controls.Add(this.lblWelcomMessage);
            this.panelWelcome.Controls.Add(this.richTextBoxWelcomeTitle);
            this.panelWelcome.Controls.Add(this.pictureBoxBigIcon);
            this.panelWelcome.Location = new System.Drawing.Point(0, 3);
            this.panelWelcome.Name = "panelWelcome";
            this.panelWelcome.Size = new System.Drawing.Size(552, 336);
            this.panelWelcome.TabIndex = 0;
            // 
            // lblWelcomMessage
            // 
            this.lblWelcomMessage.Location = new System.Drawing.Point(6, 144);
            this.lblWelcomMessage.Name = "lblWelcomMessage";
            this.lblWelcomMessage.Size = new System.Drawing.Size(540, 64);
            this.lblWelcomMessage.TabIndex = 2;
            this.lblWelcomMessage.Text = "Use the Application Setup Wizard to configure settings for the application.";
            // 
            // richTextBoxWelcomeTitle
            // 
            this.richTextBoxWelcomeTitle.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBoxWelcomeTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBoxWelcomeTitle.Location = new System.Drawing.Point(107, 19);
            this.richTextBoxWelcomeTitle.Name = "richTextBoxWelcomeTitle";
            this.richTextBoxWelcomeTitle.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            this.richTextBoxWelcomeTitle.Size = new System.Drawing.Size(433, 86);
            this.richTextBoxWelcomeTitle.TabIndex = 1;
            this.richTextBoxWelcomeTitle.Text = "";
            // 
            // pictureBoxBigIcon
            // 
            this.pictureBoxBigIcon.Image = global::Client.Properties.Resources.ProgramIcon48PNG;
            this.pictureBoxBigIcon.InitialImage = null;
            this.pictureBoxBigIcon.Location = new System.Drawing.Point(6, 6);
            this.pictureBoxBigIcon.Name = "pictureBoxBigIcon";
            this.pictureBoxBigIcon.Size = new System.Drawing.Size(95, 86);
            this.pictureBoxBigIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxBigIcon.TabIndex = 0;
            this.pictureBoxBigIcon.TabStop = false;
            // 
            // tabPageRegionLanguages
            // 
            this.tabPageRegionLanguages.Controls.Add(this.btnRegLangNext);
            this.tabPageRegionLanguages.Controls.Add(this.panelRegionLanguages);
            this.tabPageRegionLanguages.Location = new System.Drawing.Point(4, 22);
            this.tabPageRegionLanguages.Name = "tabPageRegionLanguages";
            this.tabPageRegionLanguages.Size = new System.Drawing.Size(552, 405);
            this.tabPageRegionLanguages.TabIndex = 2;
            this.tabPageRegionLanguages.Text = "tabPage1";
            this.tabPageRegionLanguages.UseVisualStyleBackColor = true;
            // 
            // btnRegLangNext
            // 
            this.btnRegLangNext.Location = new System.Drawing.Point(465, 371);
            this.btnRegLangNext.Name = "btnRegLangNext";
            this.btnRegLangNext.Size = new System.Drawing.Size(75, 23);
            this.btnRegLangNext.TabIndex = 3;
            this.btnRegLangNext.Text = "Next >";
            this.btnRegLangNext.UseVisualStyleBackColor = true;
            this.btnRegLangNext.Click += new System.EventHandler(this.btnRegLangNext_Click);
            // 
            // panelRegionLanguages
            // 
            this.panelRegionLanguages.BackColor = System.Drawing.Color.White;
            this.panelRegionLanguages.Controls.Add(this.regionLanguageGroupGrid);
            this.panelRegionLanguages.Controls.Add(this.lblRegLangMessage);
            this.panelRegionLanguages.Location = new System.Drawing.Point(0, 3);
            this.panelRegionLanguages.Name = "panelRegionLanguages";
            this.panelRegionLanguages.Size = new System.Drawing.Size(552, 336);
            this.panelRegionLanguages.TabIndex = 2;
            // 
            // regionLanguageGroupGrid
            // 
            this.regionLanguageGroupGrid.AllowUserToAddRows = false;
            this.regionLanguageGroupGrid.BackgroundColor = System.Drawing.Color.White;
            this.regionLanguageGroupGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.regionLanguageGroupGrid.CollapseIcon = null;
            this.regionLanguageGroupGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.regionLanguageGroupGrid.ColumnHeadersVisible = false;
            this.regionLanguageGroupGrid.ExpandIcon = null;
            this.regionLanguageGroupGrid.Location = new System.Drawing.Point(0, 0);
            this.regionLanguageGroupGrid.Name = "regionLanguageGroupGrid";
            this.regionLanguageGroupGrid.RowHeadersVisible = false;
            this.regionLanguageGroupGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.regionLanguageGroupGrid.Size = new System.Drawing.Size(306, 336);
            this.regionLanguageGroupGrid.TabIndex = 2;
            // 
            // lblRegLangMessage
            // 
            this.lblRegLangMessage.Location = new System.Drawing.Point(305, 3);
            this.lblRegLangMessage.Name = "lblRegLangMessage";
            this.lblRegLangMessage.Size = new System.Drawing.Size(244, 97);
            this.lblRegLangMessage.TabIndex = 1;
            this.lblRegLangMessage.Text = "Select the Region/Language(s) you would like to be able to download content for. " +
                "\r\n\r\nThe more you select, the longer the download will take. \r\n\r\nYou can change y" +
                "our settings later if you need to.";
            // 
            // tabPageContentUpdateProgress
            // 
            this.tabPageContentUpdateProgress.Controls.Add(this.btnFinished);
            this.tabPageContentUpdateProgress.Controls.Add(this.panel1);
            this.tabPageContentUpdateProgress.Location = new System.Drawing.Point(4, 22);
            this.tabPageContentUpdateProgress.Name = "tabPageContentUpdateProgress";
            this.tabPageContentUpdateProgress.Size = new System.Drawing.Size(552, 405);
            this.tabPageContentUpdateProgress.TabIndex = 3;
            this.tabPageContentUpdateProgress.Text = "tabPage1";
            this.tabPageContentUpdateProgress.UseVisualStyleBackColor = true;
            // 
            // btnFinished
            // 
            this.btnFinished.Enabled = false;
            this.btnFinished.Location = new System.Drawing.Point(465, 363);
            this.btnFinished.Name = "btnFinished";
            this.btnFinished.Size = new System.Drawing.Size(75, 23);
            this.btnFinished.TabIndex = 3;
            this.btnFinished.Text = "Finished";
            this.btnFinished.UseVisualStyleBackColor = true;
            this.btnFinished.Click += new System.EventHandler(this.btnFinished_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.lblFinished);
            this.panel1.Controls.Add(this.lblWait);
            this.panel1.Controls.Add(this.lblWorkerStatus);
            this.panel1.Controls.Add(this.progressBarContentUpdate);
            this.panel1.Location = new System.Drawing.Point(0, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(552, 336);
            this.panel1.TabIndex = 2;
            // 
            // lblFinished
            // 
            this.lblFinished.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFinished.Location = new System.Drawing.Point(22, 25);
            this.lblFinished.Name = "lblFinished";
            this.lblFinished.Size = new System.Drawing.Size(472, 49);
            this.lblFinished.TabIndex = 3;
            this.lblFinished.Text = "Content files will be downloaded when the application starts.";
            this.lblFinished.Visible = false;
            // 
            // lblWait
            // 
            this.lblWait.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWait.Location = new System.Drawing.Point(22, 23);
            this.lblWait.Name = "lblWait";
            this.lblWait.Size = new System.Drawing.Size(473, 51);
            this.lblWait.TabIndex = 2;
            this.lblWait.Text = "Content files will be downloaded when the application starts.";
            // 
            // lblWorkerStatus
            // 
            this.lblWorkerStatus.AutoSize = true;
            this.lblWorkerStatus.Location = new System.Drawing.Point(12, 304);
            this.lblWorkerStatus.Name = "lblWorkerStatus";
            this.lblWorkerStatus.Size = new System.Drawing.Size(0, 13);
            this.lblWorkerStatus.TabIndex = 1;
            // 
            // progressBarContentUpdate
            // 
            this.progressBarContentUpdate.Location = new System.Drawing.Point(12, 268);
            this.progressBarContentUpdate.Name = "progressBarContentUpdate";
            this.progressBarContentUpdate.Size = new System.Drawing.Size(528, 23);
            this.progressBarContentUpdate.TabIndex = 0;
            this.progressBarContentUpdate.Visible = false;
            // 
            // ApplicationSetupWizard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(550, 461);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(556, 493);
            this.Name = "ApplicationSetupWizard";
            this.Text = global::Client.Properties.Settings.Default.AppNameFull;
            this.Load += new System.EventHandler(this.LocalizedContentSetupWizard_Load);
            this.tabWizard.ResumeLayout(false);
            this.tabPageWelcome.ResumeLayout(false);
            this.panelWelcome.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBigIcon)).EndInit();
            this.tabPageRegionLanguages.ResumeLayout(false);
            this.panelRegionLanguages.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.regionLanguageGroupGrid)).EndInit();
            this.tabPageContentUpdateProgress.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage tabPageWelcome;
        private System.Windows.Forms.Button btnWelcomeNext;
        private System.Windows.Forms.Panel panelWelcome;
        private System.Windows.Forms.Label lblWelcomMessage;
        private System.Windows.Forms.RichTextBox richTextBoxWelcomeTitle;
        private System.Windows.Forms.PictureBox pictureBoxBigIcon;
        private System.Windows.Forms.TabPage tabPageRegionLanguages;
        private System.Windows.Forms.Button btnRegLangNext;
        private System.Windows.Forms.Panel panelRegionLanguages;
        private Client.UserControls.RegionLanguageGroupGrid regionLanguageGroupGrid;
        private System.Windows.Forms.Label lblRegLangMessage;
        private System.Windows.Forms.TabPage tabPageContentUpdateProgress;
        private System.Windows.Forms.Button btnFinished;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblWorkerStatus;
        private System.Windows.Forms.ProgressBar progressBarContentUpdate;
        private System.Windows.Forms.Label lblFinished;
        private System.Windows.Forms.Label lblWait;
    }
}