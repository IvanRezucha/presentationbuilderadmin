using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.IO;

namespace Client
{
    public partial class SearchFAQ : Form
    {
        public SearchFAQ()
        {
            InitializeComponent();
        }

        private void Faq_Load(object sender, EventArgs e)
        {
            string faqFile = Path.Combine(Application.StartupPath, Properties.Settings.Default.SearchFAQFile);
            if (File.Exists(faqFile))
            {
                webFaq.Navigate(faqFile);
            }
            else
            {
                MessageBox.Show("Could not find the help file.", "Missing Help File", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}