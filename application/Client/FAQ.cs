using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.IO;

namespace Client
{
    public partial class FAQ : Form
    {
        public FAQ()
        {
            InitializeComponent();
        }

        private void Faq_Load(object sender, EventArgs e)
        {
            string faqFile = Path.Combine(Application.StartupPath, Properties.Settings.Default.FAQFile);
            if (File.Exists(faqFile))
            {
                webFaq.Navigate(faqFile);
            }
            else
            {
                MessageBox.Show("Could not find the help file.", "Missing Help File", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}