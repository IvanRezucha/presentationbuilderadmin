﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Deployment.Application;

namespace Client
{
	public partial class ManualInstall : Form
	{
		#region Form events

		public ManualInstall()
		{
			InitializeComponent();
		}

		private void ManualInstall_Load(object sender, EventArgs e)
		{
			CheckForUpdates();
		}
		
		#endregion

		#region Controllers

		void CheckForUpdates()
		{
			switch (Components.AppUpdater.CheckForAppUpdate())
			{
				case Client.Components.UpdateAvailable.Yes:
					ApplicationDeployment appUpdate = ApplicationDeployment.CurrentDeployment;
					appUpdate.UpdateCompleted += new AsyncCompletedEventHandler(UpdateCompleted);
					appUpdate.UpdateAsync();
					break;

				default:
					DisplayFailure();
					break;
			}
		}

		void UpdateCompleted(object sender, AsyncCompletedEventArgs e)
		{
			if (e.Cancelled || e.Error != null)
			{
				DisplayFailure();
			}
			else
			{
				Application.Restart();
			}
		}

		void DisplayFailure()
		{
			if (DialogResult.Yes == MessageBox.Show(string.Format("We're sorry, but {0} cannot continue to install because it cannot find an available update.  Please check your internet connection.  Would you like to try again?", Properties.Settings.Default.AppNameFull), "No update available", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation))
			{
				CheckForUpdates();
			}
			else
			{
				Close();
			}
		}
		
		#endregion
	}
}
