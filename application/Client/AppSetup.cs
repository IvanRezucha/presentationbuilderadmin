using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Microsoft.Win32;
using System.Windows.Forms;

namespace Client
{
    public class AppSetup
    {
        /// <summary>
        /// Registries the setup.
        /// </summary>
        public void RegistrySetup()
        {
            string officeVersionKey = (string)Registry.GetValue(@"HKEY_CLASSES_ROOT\PowerPoint.Application\CurVer", "", null);

            if (officeVersionKey == null)
            {
                ////Office Not Installed or Registry Issues
                //MessageBox.Show("Microsoft PowerPoint does not appear to be installed. " + Properties.Settings.Default.AppNameFull.ToString() + "will now close.", "Warning");
                //Application.Exit();
                return;
            }
            else if (officeVersionKey.Equals("PowerPoint.Application.14"))
            {
                //Office 2010
                Registry.ClassesRoot.CreateSubKey(@"PowerPoint.Show.8").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                Registry.ClassesRoot.CreateSubKey(@"PowerPoint.SlideShow.8").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                Registry.ClassesRoot.CreateSubKey(@"PowerPoint.Template.8").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                Registry.ClassesRoot.CreateSubKey(@"PowerPoint.Presentation.12").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                Registry.ClassesRoot.CreateSubKey(@"PowerPoint.Template.12").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                Registry.ClassesRoot.CreateSubKey(@"PowerPoint.Show.12").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                Registry.ClassesRoot.CreateSubKey(@"PowerPoint.TemplateMacroEnabled.12").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                Registry.ClassesRoot.CreateSubKey(@"PowerPoint.ShowMacroEnabled.12").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                Registry.ClassesRoot.CreateSubKey(@"PowerPoint.MacroEnabled.Presentation.12").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                Registry.ClassesRoot.CreateSubKey(@"PowerPoint.MacroEnabled.Show.12").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                Registry.ClassesRoot.CreateSubKey(@"PowerPoint.MacroEnabled.Template.12").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                RegistryKey regKeyAppRoot = Registry.CurrentUser.CreateSubKey(@"Software\Microsoft\Windows\Shell\AttachmentExecute\{0002DF01-0000-0000-C000-000000000046}");
                regKeyAppRoot.SetValue("PowerPoint.Show.8", 0, 0);
            }
            else if (officeVersionKey.Equals("PowerPoint.Application.12"))
            {
                //MessageBox.Show("BROWSER FLAGS SET HERE");
                //Office 2007
                // try // removing try-catch block since callers of this method are catching exceptions and trying to react to them.
                // { 
                //Make/edit registry keys necessary to cause ppts to open in browser instead of application
                Registry.ClassesRoot.CreateSubKey(@"PowerPoint.Show.8").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                Registry.ClassesRoot.CreateSubKey(@"PowerPoint.SlideShow.8").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                Registry.ClassesRoot.CreateSubKey(@"PowerPoint.Template.8").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                Registry.ClassesRoot.CreateSubKey(@"PowerPoint.Presentation.12").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                Registry.ClassesRoot.CreateSubKey(@"PowerPoint.Template.12").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                Registry.ClassesRoot.CreateSubKey(@"PowerPoint.Show.12").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                Registry.ClassesRoot.CreateSubKey(@"PowerPoint.TemplateMacroEnabled.12").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                Registry.ClassesRoot.CreateSubKey(@"PowerPoint.ShowMacroEnabled.12").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                Registry.ClassesRoot.CreateSubKey(@"PowerPoint.MacroEnabled.Presentation.12").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                Registry.ClassesRoot.CreateSubKey(@"PowerPoint.MacroEnabled.Show.12").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                Registry.ClassesRoot.CreateSubKey(@"PowerPoint.MacroEnabled.Template.12").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                RegistryKey regKeyAppRoot = Registry.CurrentUser.CreateSubKey(@"Software\Microsoft\Windows\Shell\AttachmentExecute\{0002DF01-0000-0000-C000-000000000046}");
                regKeyAppRoot.SetValue("PowerPoint.Show.8", 0, 0);
                //}
                //catch (System.Security.SecurityException)
                //{
                //    //MessageBox.Show("You have inadequate permissions to use " + Properties.Settings.Default.AppNameFull.ToString() +
                //    //        ". " + Properties.Settings.Default.AppNameFull.ToString() + "will now close.","Warning");
                //    //Application.Exit();
                //}
                //catch (UnauthorizedAccessException)
                //{
                //    // allow this to be picked up by the IpgMain
                //}
                //MessageBox.Show("DONE SETTING BROWSER FLAGS");
            }
            else if (officeVersionKey.Equals("PowerPoint.Application.11"))
            {
                //Office 2003
                // try
                //{
                //writes registry setting to suppress powerpoint open/save dialog box when loading ppt to browser
                RegistryKey regKeyAppRoot = Registry.CurrentUser.CreateSubKey(@"Software\Microsoft\Windows\Shell\AttachmentExecute\{0002DF01-0000-0000-C000-000000000046}");
                regKeyAppRoot.SetValue("PowerPoint.Show.8", 0, 0);

                //}
                //catch (System.Security.SecurityException)
                //{
                //    //MessageBox.Show("You have inadequate permissions to use " + Properties.Settings.Default.AppNameFull.ToString() +
                //    //        ". " + Properties.Settings.Default.AppNameFull.ToString() + "will now close.","Warning");
                //    //Application.Exit();
                //}
                //catch (UnauthorizedAccessException)
                //{

                //}
            }
            else
            {
                //Unknown version of Office
                //MessageBox.Show("The currently installed version of Microsoft PowerPoint is not optimized for " + Properties.Settings.Default.AppNameFull.ToString() + ", this may cause the program to behave in an unexpected manner.","Warning");
                RegistryKey regKeyAppRoot = Registry.CurrentUser.CreateSubKey(@"Software\Microsoft\Windows\Shell\AttachmentExecute\{0002DF01-0000-0000-C000-000000000046}");
                regKeyAppRoot.SetValue("PowerPoint.Show.8", 0, 0);
            }
        }

        public void CreateDesktopShortcut()
        {
            //Create desktop shortcut icon
            string appRefFilePath = StartMenuShortcut;
            if (File.Exists(appRefFilePath))
            {
                File.Copy(appRefFilePath, DesktopShortcut, true);
            }
        }

        public bool DoesDesktopShortcutExist
        {
            get
            {
                return File.Exists(DesktopShortcut);
            }
        }

        private string DesktopShortcut
        {
            get
            {
                string desktopPath = System.Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                return Path.Combine(desktopPath, Properties.Settings.Default.AppNameFull + ".APPREF-MS");
            }
        }

        private string StartMenuShortcut
        {
            get
            {
                string appRefPath = Path.Combine(System.Environment.GetFolderPath(Environment.SpecialFolder.Programs), Properties.Settings.Default.PublisherName);
                return Path.Combine(appRefPath, Properties.Settings.Default.AppNameFull + ".APPREF-MS");
            }
        }
    }
}
