namespace Client
{
    partial class FAQ
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FAQ));
            this.webFaq = new System.Windows.Forms.WebBrowser();
            this.SuspendLayout();
            // 
            // webFaq
            // 
            this.webFaq.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webFaq.Location = new System.Drawing.Point(0, 0);
            this.webFaq.MinimumSize = new System.Drawing.Size(20, 20);
            this.webFaq.Name = "webFaq";
            this.webFaq.Size = new System.Drawing.Size(614, 440);
            this.webFaq.TabIndex = 0;
            // 
            // FAQ
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(614, 440);
            this.Controls.Add(this.webFaq);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FAQ";
            this.Text = "FAQ";
            this.Load += new System.EventHandler(this.Faq_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.WebBrowser webFaq;
    }
}