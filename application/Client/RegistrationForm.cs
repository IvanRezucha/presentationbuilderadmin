﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace Client
{
	public partial class RegistrationForm : MasterWizardForm
	{
		#region Constructer and Vars
		private int _remainingUses = 0;
		private wsUserRegistration.UserRegistrationDS.UserProfileRow _userInfoRow = null;
		private wsUserRegistration.UserRegistrationDS.UserProfileDataTable _dtUserRegistration = null;
		private bool _userInfoRowIsNew = true;
        List<Country> countries;
        List<JobTitle> jobTitles;
        wsUserRegistration.UserRegistrationService urs;

		public RegistrationForm(int remainingUses)
		{
			_remainingUses = remainingUses;
			InitializeComponent();
		}

		#endregion

		#region Form Events

		private void RegistrationForm_Load(object sender, EventArgs e)
		{
			//set up the text telling them how many uses they get
			StringBuilder emailWarning = new StringBuilder("It appears this copy of ");
			emailWarning.Append(Properties.Settings.Default.AppNameFull);
			emailWarning.Append(" is unregistered. ");
			emailWarning.Append("Please enter your email address to begin the registration process.");
			emailWarning.Append(Environment.NewLine);
			emailWarning.Append(Environment.NewLine);
			emailWarning.Append("If you choose not to register, you will ");

			if (_remainingUses > 0)
				emailWarning.Append(String.Format("have {0} more uses before you are required to register.", _remainingUses));
			else
				emailWarning.Append(String.Format("not be able to use the {0} tool.",Properties.Settings.Default.AppNameFull));

			txtEmailDetails.Text = emailWarning.ToString();
            urs = new Client.wsUserRegistration.UserRegistrationService();
		}

		private void RegistrationForm_FormClosing(object sender, FormClosingEventArgs e)
		{
			//if they didn't register
			if (!Properties.Settings.Default.UserHasRegistered)
			{
				StringBuilder cancelMessage = new StringBuilder("If you choose not to register you will be ");
				if (Properties.Settings.Default.RemainingFreeUses <= 0)
				{
					cancelMessage.Append(String.Format("unable to use the {0}.", Properties.Settings.Default.AppNameFull));
				}
				else
				{
					cancelMessage.Append(String.Format("able to use the {0} ",Properties.Settings.Default.AppNameFull));
					cancelMessage.Append(Properties.Settings.Default.RemainingFreeUses);
					cancelMessage.Append(" more times before registration will be required.");
				}
				cancelMessage.Append(Environment.NewLine);
				cancelMessage.Append("Are you sure you want to cancel the registration process?");
				DialogResult cancelResult = MessageBox.Show(this, cancelMessage.ToString(), "Cancel?", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
				if (cancelResult == DialogResult.No)
				{
					e.Cancel = true;
				}
			}
		} 

		#endregion

		#region ButtonClicks

		private void btnNext_Click(object sender, EventArgs e)
		{
			if(String.IsNullOrEmpty(txtRegistrationEmail.Text))
			{
				errorProvider.SetError(txtRegistrationEmail, "Please enter an email address");
				return;
			}
			else if(!Regex.IsMatch(txtRegistrationEmail.Text, "^(([A-Za-z0-9]+_+)|([A-Za-z0-9]+\\-+)|([A-Za-z0-9]+\\.+)|([A-Za-z0-9]+\\++))*[A-Za-z0-9]+@((\\w+\\-+)|(\\w+\\.))*\\w{1,63}\\.[a-zA-Z]{2,6}$"))
			{
				errorProvider.SetError(txtRegistrationEmail, "Please enter a valid email address");
				return;
			}
			else
			{
				errorProvider.SetError(txtRegistrationEmail, "");
			}

			wsUserRegistration.UserRegistrationService urs = new Client.wsUserRegistration.UserRegistrationService();
			if(_dtUserRegistration != null)
				_dtUserRegistration.Rows.Clear();
			_dtUserRegistration = urs.GetUserDataTableByEmail(txtRegistrationEmail.Text);

            List<JobTitle> jobTitles = GetJobTitles();
            List<Country> countries = GetCountries();
            this.profileControl1.PopulateCountryDropDown(countries);
            this.profileControl1.PopulateJobTitleDropDown(jobTitles);

			if (_dtUserRegistration.Rows.Count > 1)
			{
				throw new Exception("DB Error: Duplicate email values returned for " + txtRegistrationEmail.Text);
			} 
			else if (_dtUserRegistration.Rows.Count == 1)//user is already registered at least with web
			{
				_userInfoRow = ((wsUserRegistration.UserRegistrationDS.UserProfileRow)_dtUserRegistration.Rows[0]);
                this.profileControl1.Populate(_userInfoRow);
				_userInfoRowIsNew = false;

                if (this.profileControl1.IsProfileValid())
                {
                    MessageBox.Show(this, String.Format("Thank you for registering this copy of {0}", Properties.Settings.Default.AppNameFull), "Thank you", MessageBoxButtons.OK, MessageBoxIcon.None);
                    Properties.Settings.Default.UserHasRegistered = true;
                    Properties.Settings.Default.Save();
                    Close();

                }
                /*
				if (!_userInfoRow.IsCompanyNameNull() && !_userInfoRow.IsCompanySizeNull() && !_userInfoRow.IsCountryNull() && !_userInfoRow.IsReferenceSourceNull())
				{//if they've already filled in the fields to complete registration let them go
					MessageBox.Show(this, String.Format("Thank you for registering this copy of {0}",Properties.Settings.Default.AppNameFull), "Thank you", MessageBoxButtons.OK, MessageBoxIcon.None);
					Properties.Settings.Default.UserHasRegistered = true;
					Properties.Settings.Default.Save();
					Close();
				}
                */
			}
			else if (_dtUserRegistration.Rows.Count < 1)
			{
				_userInfoRowIsNew = true;
				_userInfoRow = _dtUserRegistration.NewUserProfileRow();
				_userInfoRow.Email = txtRegistrationEmail.Text;
				_userInfoRow.IsInSurvey = false;
				_userInfoRow.InstallDate = DateTime.UtcNow;
			}

			//if they get here, they're at least missing some registration info so fill in what we've got and
			//prompt them for what we don't at the next tab of the wizard
			LoadFields();
            this.profileControl1.ValidateControls();
            Properties.Settings.Default.UserRegistrationEmail = _userInfoRow.Email;
            Properties.Settings.Default.Save();
			tabWizard.SelectedTab = tabPage2;
		}

		private void btnDontRegister_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void btnBack_Click(object sender, EventArgs e)
		{
			tabWizard.SelectedTab = tabPage1;
		}

		private void btnFinish_Click(object sender, EventArgs e)
		{
            if (!this.profileControl1.IsProfileValid())
            {
                this.profileControl1.ValidateControls();
                return;
            }

            _userInfoRow.FirstName = this.profileControl1.FirstName;
            _userInfoRow.LastName = this.profileControl1.LastName;
            _userInfoRow.Phone = this.profileControl1.PhoneNumber;
            _userInfoRow.Country = this.profileControl1.Country;
            _userInfoRow.RegionId = this.profileControl1.RegionProperty;
            _userInfoRow.CompanyName = this.profileControl1.CompanyNameProperty;
            _userInfoRow.CompanySize = this.profileControl1.CompanySize;
            _userInfoRow.JobTitle = this.profileControl1.JobTitle;
            _userInfoRow.ReferenceSource = this.profileControl1.Referrer;

			if (_userInfoRowIsNew)
				_dtUserRegistration.Rows.Add(_userInfoRow);

			urs.SubmitNewUserData(_dtUserRegistration);

			Properties.Settings.Default.UserHasRegistered = true;
			Properties.Settings.Default.Save();

            wsUserRegistration.UserRegistrationDS.UserProfileRow userRow = (wsUserRegistration.UserRegistrationDS.UserProfileRow)_dtUserRegistration.Rows[0];
            if (userRow.UserID != 0)
            {
                wsUserRegistration.UserRegistrationDS.UserProfileDataTable dt = urs.GetUserByEmail(Properties.Settings.Default.UserRegistrationEmail);
                if (dt.Rows.Count > 0)
                {
                    userRow = (wsUserRegistration.UserRegistrationDS.UserProfileRow)dt.Rows[0];
                }
            }
            Properties.Settings.Default.UserID = userRow.UserID;
            Properties.Settings.Default.Save();

			MessageBox.Show(this, String.Format("Thank you for registering this copy of {0}",Properties.Settings.Default.AppNameFull), "Thank you", MessageBoxButtons.OK, MessageBoxIcon.None);
			Close();
		}

		#endregion

		#region HelperMethods

        private List<Country> GetCountries()
        {
            List<Country> countryList = new List<Country>();
            wsUserRegistration.UserRegistrationDS.CountriesDataTable countriesTable = urs.GetCountries();
            foreach (wsUserRegistration.UserRegistrationDS.CountriesRow row in countriesTable)
                countryList.Add(new Country(row.Name, row.Code));
            return countryList;
        }
        private List<JobTitle> GetJobTitles()
        {
            List<JobTitle> jobTitles = new List<JobTitle>();
            wsUserRegistration.UserRegistrationDS.JobTitlesDataTable jobTitlesTable = urs.GetJobTitles();
            foreach (wsUserRegistration.UserRegistrationDS.JobTitlesRow row in jobTitlesTable)
                jobTitles.Add(new JobTitle(row.Name, row.Code));
            return jobTitles;
        }

		private void LoadFields()
		{
            if (!_userInfoRowIsNew)
            {
                this.profileControl1.FirstName = _userInfoRow.IsFirstNameNull() ? string.Empty : _userInfoRow.FirstName;
                this.profileControl1.LastName = _userInfoRow.IsLastNameNull() ? string.Empty : _userInfoRow.LastName;
                this.profileControl1.PhoneNumber = _userInfoRow.IsPhoneNull() ? string.Empty : _userInfoRow.Phone;
                this.profileControl1.JobTitle = _userInfoRow.IsJobTitleNull() ? string.Empty : _userInfoRow.JobTitle;
                this.profileControl1.CompanyNameProperty = _userInfoRow.IsCompanyNameNull() ? string.Empty : _userInfoRow.CompanyName;
                this.profileControl1.CompanySize = _userInfoRow.IsCompanySizeNull() ? 0 :_userInfoRow.CompanySize;
                this.profileControl1.Referrer = _userInfoRow.IsReferenceSourceNull() ? string.Empty : _userInfoRow.ReferenceSource;
                this.profileControl1.Country = _userInfoRow.IsCountryNull() ? string.Empty : _userInfoRow.Country;
                this.profileControl1.RegionProperty = _userInfoRow.IsRegionIdNull() ? 0 : _userInfoRow.RegionId;
            }
		}

		#endregion

	}
}
