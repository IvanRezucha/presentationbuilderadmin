using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Client
{
    public partial class LocalizedContentAdapterWizard : Client.MasterWizardForm
    {
        //class variables
        private wsIpgSlideUpdater.SlideLibrary _OldSlideLibrary;
        //these datasets are for localize content
        private wsIpgSlideUpdater.SlideLibrary _NewMainData;
        private SortedList<int, wsIpgSlideUpdater.SlideLibrary> _NewLocalizedSlideData;
        private wsIpgSlideUpdater.SlideLibrary _NewMergedData;
        private DataSets.MyRegionLanguages _MyRegionLanguages;
        private List<string> _FilesToDownload;
        private List<string> _FilesToDelete;
        private wsIpgSlideUpdater.SupportFileDS _NewSupportFiles;
        private wsIpgSlideUpdater.SupportFileDS _OldSupportFiles;

        public LocalizedContentAdapterWizard()
        {
            InitializeComponent();
        }

        private void LocalizedContentAdapterWizard_Load(object sender, EventArgs e)
        {
            string welcomeMessage;
            welcomeMessage = String.Format("Welcome to the {0} Localized Content Conversion Wizard", Properties.Settings.Default.AppNameFull);
            richTextBoxWelcomeTitle.Text = welcomeMessage;
        }

        #region Next Button Click Events

        private void btnRegLangNext_Click(object sender, EventArgs e)
        {
            if (!regionLanguageGroupGrid.SaveMyRegionLanguages())
            {
                return;     
            }
            tabWizard.SelectTab(tabPageContentUpdateProgress);
            backgroundWorkerContentAdapter.RunWorkerAsync();
        }

        private void btnWelcomeNext_Click(object sender, EventArgs e)
        {
            regionLanguageGroupGrid.FillRegionLanguageGroupGrid();
            tabWizard.SelectTab(tabPageRegionLanguages);
        } 

        #endregion

        #region Content Updating Stuff

        private void LoadOldData()
        {
            _OldSlideLibrary = new Client.wsIpgSlideUpdater.SlideLibrary();
            _OldSlideLibrary.EnforceConstraints = false;
            _OldSlideLibrary.ReadXml(Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, AppSettings.LocalFiles.Default.SlideLibraryData));
            _OldSupportFiles = new Client.wsIpgSlideUpdater.SupportFileDS();
            //this is for backwards compatibility. the orignal updating scheme did not include a SupportFileData.xml file.
            string currentSupportFileDataFile = Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, AppSettings.LocalFiles.Default.SupportFilesData);
            if (!File.Exists(currentSupportFileDataFile))
            {
                _OldSupportFiles.WriteXml(currentSupportFileDataFile);
            }
            _OldSupportFiles.ReadXml(currentSupportFileDataFile);
        }

        private void LoadNewLocalizedData()
        {
            //get the main data file (tables that aren't subject to localization)
            _NewMainData = new Client.wsIpgSlideUpdater.SlideLibrary();
            _NewMainData.EnforceConstraints = false;
            string mainDataFileURI = String.Format("{0}{1}/{2}", Properties.Settings.Default.DataUpdateBaseURI, "LocalizedUpdateData", AppSettings.LocalFiles.Default.MainData);
            _NewMainData.ReadXml(mainDataFileURI);

            //we'll merge this with the localized data
            _NewMergedData = new Client.wsIpgSlideUpdater.SlideLibrary();
            _NewMergedData.Merge(_NewMainData);

            //get my languages
            _MyRegionLanguages = new Client.DataSets.MyRegionLanguages();
            _MyRegionLanguages.ReadXml(Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, AppSettings.LocalFiles.Default.MyRegionLangauges));

            _NewLocalizedSlideData = new SortedList<int, Client.wsIpgSlideUpdater.SlideLibrary>();

            //get localized slide data for each RegionLanguage that the user has selected to be updated
            List<int> myRegLangIds = new List<int>();
            foreach (DataSets.MyRegionLanguages.MyRegionLanguagesRow myRegLangRow in _MyRegionLanguages._MyRegionLanguages.Rows)
            {
                myRegLangIds.Add(myRegLangRow.RegionLanguageID);
                wsIpgSlideUpdater.SlideLibrary dsLocalizedSlideData = new Client.wsIpgSlideUpdater.SlideLibrary();
                dsLocalizedSlideData.EnforceConstraints = false;
                dsLocalizedSlideData.ReadXml(String.Format("{0}{1}/{2}", Properties.Settings.Default.DataUpdateBaseURI, "LocalizedUpdateData", String.Format("{0}{1}", myRegLangRow.RegionLanguageID, AppSettings.LocalFiles.Default.SlideDataFilePart)));
                _NewMergedData.Merge(dsLocalizedSlideData);
                _NewLocalizedSlideData.Add(myRegLangRow.RegionLanguageID, dsLocalizedSlideData);
            }

            //support files
            _NewSupportFiles = new Client.wsIpgSlideUpdater.SupportFileDS();
            _NewSupportFiles.ReadXml(String.Format("{0}{1}", Properties.Settings.Default.DataUpdateBaseURI, AppSettings.LocalFiles.Default.SupportFilesData));
        }

        private void DetermineFilesToDownload()
        {
            _FilesToDownload = new List<string>();

            //get the slides to download
            foreach (wsIpgSlideUpdater.SlideLibrary.SlideCategoryRow newSlide in _NewMergedData.SlideCategory.Rows)
            {
                if (!_FilesToDownload.Contains(newSlide.SlideRow.FileName))
                {
                    string filter = "SlideID=" + newSlide.SlideID.ToString();
                    List<wsIpgSlideUpdater.SlideLibrary.SlideCategoryRow> oldSlides = new List<Client.wsIpgSlideUpdater.SlideLibrary.SlideCategoryRow>((wsIpgSlideUpdater.SlideLibrary.SlideCategoryRow[])_OldSlideLibrary.SlideCategory.Select(filter));
                    if (oldSlides.Count == 0)
                    {
                        _FilesToDownload.Add(newSlide.SlideRow.FileName);
                    }
                    else
                    {
                        if (newSlide.SlideRow.DateModified > oldSlides[0].SlideRow.DateModified)
                        {
                            _FilesToDownload.Add(newSlide.SlideRow.FileName);
                        }
                    }
                }
            }

            //get the support files to download
            foreach (wsIpgSlideUpdater.SupportFileDS.SupportFileRow newSupportFile in _NewSupportFiles.SupportFile.Rows)
            {
                wsIpgSlideUpdater.SupportFileDS.SupportFileRow oldSupportFile = _OldSupportFiles.SupportFile.FindBySupportFileID(newSupportFile.SupportFileID);
                if (oldSupportFile == null)
                {
                    _FilesToDownload.Add(newSupportFile.FileName);
                }
                else
                {
                    if (newSupportFile.ModifyDate > oldSupportFile.ModifyDate)
                    {
                        _FilesToDownload.Add(newSupportFile.FileName);
                    }
                }
            }
        }

        private void DetermineFilesToDelete()
        {
            _FilesToDelete = new List<string>();

            //get the slides to delete
            foreach (wsIpgSlideUpdater.SlideLibrary.SlideCategoryRow oldSlide in _OldSlideLibrary.SlideCategory.Rows)
            {

                if (!_FilesToDelete.Contains(oldSlide.SlideRow.FileName))
                {
                    string filter = "SlideID=" + oldSlide.SlideID.ToString();
                    if (_NewMergedData.SlideCategory.Select(filter).Length == 0)
                    {
                        _FilesToDelete.Add(oldSlide.SlideRow.FileName);
                    }
                }
            }
        }

        private void DownloadFiles()
        {
            System.Net.WebClient wc = new System.Net.WebClient();
            //_FilesToDownload.ForEach(AppendWebPath);

            int filesDownloaded = 0;
            double percentDownloaded = 0;
            foreach (string file in _FilesToDownload)
            {
                Uri uri = new Uri(Properties.Settings.Default.ServerSlideBaseURI + file);
                string filePath = Path.Combine(AppSettings.LocalPaths.Default.SlideDirectory, file);
                try
                {
                    wc.DownloadFile(uri, filePath);
                }
                catch (Exception ex)
                {
                    //do nothing
                }
                //if the file is a slide, download the image
                if (uri.AbsoluteUri.EndsWith(".pot"))
                {
                    Uri imageUri = new Uri(Properties.Settings.Default.ServerImageBaseURI + file.Replace(".pot", ".jpg"));
                    filePath = Path.Combine(AppSettings.LocalPaths.Default.ThumbnailDirectory, file.Replace(".pot", ".jpg"));
                    try
                    {
                        wc.DownloadFile(imageUri, filePath);
                    }
                    catch (Exception ex)
                    {
                        //do nothing
                    }
                }

                //increment and report progress
                filesDownloaded++;
                percentDownloaded = (Convert.ToDouble(filesDownloaded) / Convert.ToDouble(_FilesToDownload.Count)) * 100.00;
                backgroundWorkerContentAdapter.ReportProgress(Convert.ToInt32(percentDownloaded), String.Format("Downloading file {0} of {1}.", filesDownloaded, _FilesToDownload.Count));

            }
        }

        private void DeleteOldFiles()
        {
            foreach (string file in _FilesToDelete)
            {
                try
                {
                    File.Delete(Path.Combine(AppSettings.LocalPaths.Default.SlideDirectory, file));
                    if (file.EndsWith(".pot"))
                    {
                        File.Delete(Path.Combine(AppSettings.LocalPaths.Default.ThumbnailDirectory, file.Replace(".pot", ".jpg")));
                    }
                }
                catch (Exception ex)
                {
                    //TODO: Handle exception
                }
            }
        }

        private void UpdateContentVersionSetting()
        {
            wsIpgSlideUpdater.SlideUpdater wsSlideUpdater = new Client.wsIpgSlideUpdater.SlideUpdater();
            int? newContentID = wsSlideUpdater.GetMostRecentLocalizedContentUpdateId();


            Properties.Settings.Default.LastLocalizedContentID = newContentID.Value;
            Properties.Settings.Default.Save();

        }

        private void backgroundWorkerContentAdapter_DoWork(object sender, DoWorkEventArgs e)
        {
            backgroundWorkerContentAdapter.ReportProgress(0, "Initializing content conversion...");
            LoadOldData();
            LoadNewLocalizedData();
            backgroundWorkerContentAdapter.ReportProgress(0, "Determining files to download...");
            DetermineFilesToDownload();
            DetermineFilesToDelete();
            DownloadFiles();
            DeleteOldFiles();

            //write the updated data to disk
            _NewMainData.WriteXml(Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, AppSettings.LocalFiles.Default.MainData));

            foreach (KeyValuePair<int, wsIpgSlideUpdater.SlideLibrary> localizedSlideData in _NewLocalizedSlideData)
            {
                localizedSlideData.Value.WriteXml(Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, String.Format("{0}{1}", localizedSlideData.Key.ToString(), AppSettings.LocalFiles.Default.SlideDataFilePart)));
            }

            _NewSupportFiles.WriteXml(Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, AppSettings.LocalFiles.Default.SupportFilesData));


            UpdateContentVersionSetting();
        }

        private void backgroundWorkerContentAdapter_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            lblWorkerStatus.Text = e.UserState.ToString();
            progressBarContentUpdate.Value = e.ProgressPercentage;
        }
        
        private void backgroundWorkerContentAdapter_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Properties.Settings.Default.IsAppLocalizedContentReady = true;
            Properties.Settings.Default.IsFirstRun = false;
            Properties.Settings.Default.Save();
            lblWorkerStatus.Text = "Content conversion complete.";
            btnFinished.Enabled = true;
            lblWait.Visible = false;
            lblFinished.Visible = true;

            //update upgrade settings
            Upgrades.UpgradeSettings.Default.IsReadyFor_1_9 = true;
            Upgrades.UpgradeSettings.Default.Save();

        }

        #endregion


        private void btnFinished_Click(object sender, EventArgs e)
        {

            this.Close();
        }


    }
}

