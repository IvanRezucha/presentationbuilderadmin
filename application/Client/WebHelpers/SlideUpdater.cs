using System;
using System.Collections.Generic;
using System.Text;
using System.Net;

namespace Client.wsIpgSlideUpdater
{
    partial class SlideUpdater
    {
        protected override System.Net.WebRequest GetWebRequest(Uri uri)
        {
            //this is for the users who have old school proxy
            HttpWebRequest httpWebRequest = (HttpWebRequest)base.GetWebRequest(uri);
            httpWebRequest.KeepAlive = false;
            httpWebRequest.ProtocolVersion = HttpVersion.Version10;

            //this if for the users who's proxy requires credentials
            IWebProxy iWebProxy = WebRequest.DefaultWebProxy;
            string webServiceUrl = uri.ToString();
            string proxyAddress = iWebProxy.GetProxy(new Uri(webServiceUrl)).ToString();
            if (!proxyAddress.Equals(webServiceUrl))
            {
                WebProxy webProxy = new WebProxy();
                webProxy.Address = new Uri(proxyAddress);
                webProxy.Credentials = CredentialCache.DefaultCredentials;
                Proxy = webProxy;
            }

            return httpWebRequest;

        }

       
    }
}
