using System;
using System.Collections.Generic;
using System.Text;
using System.Net;

namespace Client.WebHelpers
{
    partial class WebClientHelper : System.Net.WebClient
    {
        protected override WebRequest GetWebRequest(Uri address)
        {
            HttpWebRequest httpWebRequest = (HttpWebRequest)base.GetWebRequest(address);
            httpWebRequest.KeepAlive = false;
            httpWebRequest.ProtocolVersion = HttpVersion.Version10;
            return httpWebRequest;
        }

    }
}
