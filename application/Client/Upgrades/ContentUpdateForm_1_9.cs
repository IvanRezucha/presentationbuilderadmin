using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Deployment.Application;

namespace Client.Upgrades
{
    public partial class ContentUpdateForm_1_9 : MasterForm
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="ContentUpdateForm"/> class.
        /// </summary>
        /// <param name="updateType">Type of the update.</param>
        public ContentUpdateForm_1_9()
        {
            InitializeComponent();
        }

        #region control events

        /// <summary>
        /// Handles the Click event of the btnCancel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void btnCancel_Click(object sender, EventArgs e)
        {

            this.Close();
        }

        /// <summary>
        /// Handles the Click event of the btnOK control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void btnOK_Click(object sender, EventArgs e)
        {
            btnOK.Enabled = false;
            lblUpdateStatus.Visible = true;
            lblUpdateStatus.Text = "Preparing to download content...";
            backgroundWorkerGitUrDone.RunWorkerAsync();

        } 
        #endregion

        private void backgroundWorkerGitUrDone_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {   
                //we'll use these regardless of support for content regionalization
                //don't enforce contraints because we've changed the SlideLibrary dataset schema

                wsIpgSlideUpdater.SlideLibrary dsOldSlideLibrary = new Client.wsIpgSlideUpdater.SlideLibrary();
                dsOldSlideLibrary.EnforceConstraints = false;

                wsIpgSlideUpdater.SlideLibrary dsNewSlideLibrary = new Client.wsIpgSlideUpdater.SlideLibrary();
                dsNewSlideLibrary.EnforceConstraints = false;


                if (Properties.Settings.Default.IsContentLocalizationSupported)
                {
                    DataSets.MyRegionLanguages dsMyRegLangs = new Client.DataSets.MyRegionLanguages();
                    dsMyRegLangs.ReadXml(Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, AppSettings.LocalFiles.Default.MyRegionLangauges));

                    //load up the old data//////////////////////////////////////////////

                    dsOldSlideLibrary.ReadXml(Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, "MainData.xml"));

                    foreach (DataSets.MyRegionLanguages.MyRegionLanguagesRow myRegLang in dsMyRegLangs._MyRegionLanguages.Rows)
                    {
                        string regLangSlideDataFile = Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, String.Format("{0}-LocalizedSlideData.xml", myRegLang.RegionLanguageID.ToString()));
                        if (File.Exists(regLangSlideDataFile))
                        {
                            wsIpgSlideUpdater.SlideLibrary dsTempSL = new Client.wsIpgSlideUpdater.SlideLibrary();
                            dsTempSL.EnforceConstraints = false;
                            dsTempSL.ReadXml(regLangSlideDataFile);
                            dsOldSlideLibrary.Merge(dsTempSL);
                        }
                    }

                    string commonSlidesFile = Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, "1-LocalizedSlideData.xml");
                    if (File.Exists(commonSlidesFile))
                    {
                        wsIpgSlideUpdater.SlideLibrary dsTempSL = new Client.wsIpgSlideUpdater.SlideLibrary();
                        dsTempSL.EnforceConstraints = false;
                        dsTempSL.ReadXml(commonSlidesFile);
                        dsOldSlideLibrary.Merge(dsTempSL);
                    }

                    //get the new data//////////////////////////////////////////////////////////

                    dsNewSlideLibrary.ReadXml(String.Format("{0}LocalizedUpdateData/{1}", Properties.Settings.Default.DataUpdateBaseURI, AppSettings.LocalFiles.Default.MainData));

                    //write new xml locally
                    dsNewSlideLibrary.WriteXml(Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, AppSettings.LocalFiles.Default.MainData));

                    foreach (DataSets.MyRegionLanguages.MyRegionLanguagesRow myRegLang in dsMyRegLangs._MyRegionLanguages.Rows)
                    {
                        string regLangSlideDataFile = String.Format("{0}LocalizedUpdateData/{1}{2}", Properties.Settings.Default.DataUpdateBaseURI, myRegLang.RegionLanguageID.ToString(), AppSettings.LocalFiles.Default.SlideDataFilePart);
                        wsIpgSlideUpdater.SlideLibrary dsTempSL = new Client.wsIpgSlideUpdater.SlideLibrary();
                        dsTempSL.EnforceConstraints = false;
                        dsTempSL.ReadXml(regLangSlideDataFile);
                        //write new xml locally
                        dsTempSL.WriteXml(Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, String.Format("{0}{1}", myRegLang.RegionLanguageID.ToString(), AppSettings.LocalFiles.Default.SlideDataFilePart)));

                        dsNewSlideLibrary.Merge(dsTempSL);

                    } 
                }
                else
                {
                    //write code for non-localized conversion
                    DataSets.MyRegionLanguages dsMyRegLangs = new Client.DataSets.MyRegionLanguages();
                    wsIpgSlideUpdater.SlideLibrary dsOnlineRegLangs = new Client.wsIpgSlideUpdater.SlideLibrary();
                    string serverRegLangFileURI = String.Format("{0}{1}/{2}", Properties.Settings.Default.DataUpdateBaseURI, "LocalizedUpdateData", "RegionLanguages.xml");
                    dsOnlineRegLangs.ReadXml(serverRegLangFileURI);
                    //this will give them the fake RegLang
                    int fakeyRegLangID = ((wsIpgSlideUpdater.SlideLibrary.RegionLanguageRow)dsOnlineRegLangs.RegionLanguage.Rows[0]).RegionLanguageID;
                    dsMyRegLangs._MyRegionLanguages.AddMyRegionLanguagesRow(fakeyRegLangID);
                    ///write the fake MyRegLang file
                    dsMyRegLangs.WriteXml(Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, AppSettings.LocalFiles.Default.MyRegionLangauges));

                    //load old slide library///////////////////////////////////////////////////
                    dsOldSlideLibrary.ReadXml(Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, "SlideLibraryData.xml"));


                    ///load new slide library//////////////////////////////////////////////////////
                    dsNewSlideLibrary.ReadXml(String.Format("{0}LocalizedUpdateData/{1}", Properties.Settings.Default.DataUpdateBaseURI, AppSettings.LocalFiles.Default.MainData));

                    //write new xml locally
                    dsNewSlideLibrary.WriteXml(Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, AppSettings.LocalFiles.Default.MainData));

                    string regLangSlideDataFile = String.Format("{0}LocalizedUpdateData/{1}{2}", Properties.Settings.Default.DataUpdateBaseURI, fakeyRegLangID, AppSettings.LocalFiles.Default.SlideDataFilePart);
                    wsIpgSlideUpdater.SlideLibrary dsTempSL = new Client.wsIpgSlideUpdater.SlideLibrary();
                    dsTempSL.EnforceConstraints = false;
                    dsTempSL.ReadXml(regLangSlideDataFile);
                    //write new xml locally
                    dsTempSL.WriteXml(Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, String.Format("{0}{1}", fakeyRegLangID, AppSettings.LocalFiles.Default.SlideDataFilePart)));

                    dsNewSlideLibrary.Merge(dsTempSL);                    
                }

                //support files///////////////////////////////////////////////////////////////////
                //old
                wsIpgSlideUpdater.SupportFileDS dsOldSupportFiles = new Client.wsIpgSlideUpdater.SupportFileDS();
                string currentSupportFileDataFile = Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, AppSettings.LocalFiles.Default.SupportFilesData);
                if (!File.Exists(currentSupportFileDataFile))
                {
                    dsOldSupportFiles.WriteXml(currentSupportFileDataFile);
                }
                dsOldSupportFiles.ReadXml(currentSupportFileDataFile);

                //new
                Client.wsIpgSlideUpdater.SupportFileDS dsNewSupportFiles = new Client.wsIpgSlideUpdater.SupportFileDS();
                dsNewSupportFiles.ReadXml(String.Format("{0}{1}", Properties.Settings.Default.DataUpdateBaseURI, AppSettings.LocalFiles.Default.SupportFilesData));
                //write new xml locally
                dsNewSupportFiles.WriteXml(Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, AppSettings.LocalFiles.Default.SupportFilesData));
                ///////////////////determine files to download///////////////////////

                List<string> filesToDownload = new List<string>();

                //get the slides to download
                foreach (wsIpgSlideUpdater.SlideLibrary.SlideCategoryRow newSlide in dsNewSlideLibrary.SlideCategory.Rows)
                {
                    if (!filesToDownload.Contains(newSlide.SlideRow.FileName))
                    {
                        string filter = "SlideID=" + newSlide.SlideID.ToString();
                        List<wsIpgSlideUpdater.SlideLibrary.SlideCategoryRow> oldSlides = new List<Client.wsIpgSlideUpdater.SlideLibrary.SlideCategoryRow>((wsIpgSlideUpdater.SlideLibrary.SlideCategoryRow[])dsOldSlideLibrary.SlideCategory.Select(filter));
                        if (oldSlides.Count == 0)
                        {
                            filesToDownload.Add(newSlide.SlideRow.FileName);
                        }
                        else
                        {
                            if (newSlide.SlideRow.DateModified > oldSlides[0].SlideRow.DateModified)
                            {
                                filesToDownload.Add(newSlide.SlideRow.FileName);
                            }
                        }
                    }
                }

                //get the support files to download
                foreach (wsIpgSlideUpdater.SupportFileDS.SupportFileRow newSupportFile in dsNewSupportFiles.SupportFile.Rows)
                {
                    wsIpgSlideUpdater.SupportFileDS.SupportFileRow oldSupportFile = dsOldSupportFiles.SupportFile.FindBySupportFileID(newSupportFile.SupportFileID);
                    if (oldSupportFile == null)
                    {
                        filesToDownload.Add(newSupportFile.FileName);
                    }
                    else
                    {
                        if (newSupportFile.ModifyDate > oldSupportFile.ModifyDate)
                        {
                            filesToDownload.Add(newSupportFile.FileName);
                        }
                    }
                }

                ///////////determine files to delete////
                List<string> filesToDelete = new List<string>();

                //get the slides to delete
                foreach (wsIpgSlideUpdater.SlideLibrary.SlideCategoryRow oldSlide in dsOldSlideLibrary.SlideCategory.Rows)
                {

                    if (!filesToDelete.Contains(oldSlide.SlideRow.FileName))
                    {
                        string filter = "SlideID=" + oldSlide.SlideID.ToString();
                        if (dsNewSlideLibrary.SlideCategory.Select(filter).Length == 0)
                        {
                            filesToDelete.Add(oldSlide.SlideRow.FileName);
                        }
                    }
                }

                /////download//////////////////////////
                System.Net.WebClient wc = new System.Net.WebClient();

                int filesDownloaded = 0;
                double percentDownloaded = 0;
                foreach (string file in filesToDownload)
                {
                    Uri uri = new Uri(Properties.Settings.Default.ServerSlideBaseURI + file);
                    string filePath = Path.Combine(AppSettings.LocalPaths.Default.SlideDirectory, file);
                    try
                    {
                        wc.DownloadFile(uri, filePath);
                    }
                    catch (Exception ex)
                    {
                        //do nothing
                    }
                    //if the file is a slide, download the image
                    if (uri.AbsoluteUri.EndsWith(".pot"))
                    {
                        Uri imageUri = new Uri(Properties.Settings.Default.ServerImageBaseURI + file.Replace(".pot", ".jpg"));
                        filePath = Path.Combine(AppSettings.LocalPaths.Default.ThumbnailDirectory, file.Replace(".pot", ".jpg"));
                        try
                        {
                            wc.DownloadFile(imageUri, filePath);
                        }
                        catch (Exception ex)
                        {
                            //do nothing
                        }
                    }

                    //increment and report progress
                    filesDownloaded++;
                    percentDownloaded = (Convert.ToDouble(filesDownloaded) / Convert.ToDouble(filesToDownload.Count)) * 100.00;
                    backgroundWorkerGitUrDone.ReportProgress(Convert.ToInt32(percentDownloaded), String.Format("Downloading file {0} of {1}.", filesDownloaded, filesToDownload.Count));

                }

                /////delete////////////////////////
                foreach (string file in filesToDelete)
                {
                    try
                    {
                        File.Delete(Path.Combine(AppSettings.LocalPaths.Default.SlideDirectory, file));
                        if (file.EndsWith(".pot"))
                        {
                            File.Delete(Path.Combine(AppSettings.LocalPaths.Default.ThumbnailDirectory, file.Replace(".pot", ".jpg")));
                        }
                    }
                    catch (Exception ex)
                    {
                        //TODO: Handle exception
                    }
                }
            }
            catch (Exception ex)
            {
                //TODO: Handle exception
            }
        }

        private void backgroundWorkerGitUrDone_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            lblUpdateStatus.Visible = true;
            pbStatus.Visible = true;
            lblUpdateStatus.Text = e.UserState.ToString();
            pbStatus.Value = e.ProgressPercentage;
        }

        private void backgroundWorkerGitUrDone_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Upgrades.UpgradeSettings.Default.IsReadyFor_1_9 = true;
            Upgrades.UpgradeSettings.Default.Save();

            try
            {
                wsIpgSlideUpdater.SlideUpdater wsSlideUpdater = new Client.wsIpgSlideUpdater.SlideUpdater();
                wsSlideUpdater.ConnectionGroupName = "upgrader";
                int? newContentID = wsSlideUpdater.GetMostRecentLocalizedContentUpdateId();


                Properties.Settings.Default.LastLocalizedContentID = newContentID.Value;
                Properties.Settings.Default.Save();
            }
            catch (Exception ex)
            {
                
            }

            lblUpdateStatus.Text = "Content update complete.";

            this.Close();
        }

        private void ContentUpdateForm_1_9_Shown(object sender, EventArgs e)
        {
            this.BringToFront();
        }
    }

}