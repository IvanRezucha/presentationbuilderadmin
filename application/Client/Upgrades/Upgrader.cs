using System;
using System.Data;
using System.Configuration;
using System.Deployment.Application;
using System.IO;
using System.Collections.Generic;

namespace Client.Upgrades
{
    public class Upgrader
    {


        public void DoNecessaryUpgrades()
        {
            string currentVersion = String.Empty;
            if (ApplicationDeployment.IsNetworkDeployed)
            {
                currentVersion = ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString();
            }

            //if we didn't get an app version, don't do anything
            if (currentVersion.Equals(String.Empty))
            {
                return;
            }

            if (currentVersion.StartsWith("1.9"))
            {
                if (!Upgrades.UpgradeSettings.Default.IsReadyFor_1_9)
                {
                    SplashScreen.SplashScreen.CloseForm();
                    //if (Properties.Settings.Default.IsContentLocalizationSupported)
                    //{
                        if (Properties.Settings.Default.IsAppLocalizedContentReady)
                        {
                            DoLocalized_1_9_Upgrade();
                        }
                        else
                        {
                            //if we the app is configured to support content localization and the user hasn't yet configure
                            //their reg/langs, prompt them to do so.
                            LocalizedContentAdapterWizard wiz = new LocalizedContentAdapterWizard();
                            wiz.ShowDialog();
                        }
                    //}
                    //else
                    //{
                    //    DoNonLocalized_1_9_Upgrade();
                    //}
                }
            }
        }

        private void DoLocalized_1_9_Upgrade()
        {
            ContentUpdateForm_1_9 formy = new ContentUpdateForm_1_9();
            formy.ShowDialog();

        }

    }


}
