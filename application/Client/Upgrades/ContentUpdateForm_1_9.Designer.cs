namespace Client.Upgrades
{
    partial class ContentUpdateForm_1_9
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblHeading = new System.Windows.Forms.Label();
            this.pnlTop = new System.Windows.Forms.Panel();
            this.pnlBody = new System.Windows.Forms.Panel();
            this.labelMainMessage = new System.Windows.Forms.Label();
            this.lblUpdateStatus = new System.Windows.Forms.Label();
            this.pbStatus = new System.Windows.Forms.ProgressBar();
            this.btnOK = new System.Windows.Forms.Button();
            this.backgroundWorkerGitUrDone = new System.ComponentModel.BackgroundWorker();
            this.pnlTop.SuspendLayout();
            this.pnlBody.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblHeading
            // 
            this.lblHeading.AutoSize = true;
            this.lblHeading.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeading.Location = new System.Drawing.Point(12, 18);
            this.lblHeading.Name = "lblHeading";
            this.lblHeading.Size = new System.Drawing.Size(140, 24);
            this.lblHeading.TabIndex = 0;
            this.lblHeading.Text = "Content Update";
            // 
            // pnlTop
            // 
            this.pnlTop.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pnlTop.Controls.Add(this.lblHeading);
            this.pnlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTop.Location = new System.Drawing.Point(0, 0);
            this.pnlTop.Name = "pnlTop";
            this.pnlTop.Size = new System.Drawing.Size(382, 57);
            this.pnlTop.TabIndex = 4;
            // 
            // pnlBody
            // 
            this.pnlBody.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlBody.BackColor = System.Drawing.Color.White;
            this.pnlBody.Controls.Add(this.labelMainMessage);
            this.pnlBody.Controls.Add(this.lblUpdateStatus);
            this.pnlBody.Controls.Add(this.pbStatus);
            this.pnlBody.Location = new System.Drawing.Point(0, 57);
            this.pnlBody.Name = "pnlBody";
            this.pnlBody.Size = new System.Drawing.Size(382, 223);
            this.pnlBody.TabIndex = 5;
            // 
            // labelMainMessage
            // 
            this.labelMainMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMainMessage.Location = new System.Drawing.Point(16, 13);
            this.labelMainMessage.Name = "labelMainMessage";
            this.labelMainMessage.Size = new System.Drawing.Size(328, 54);
            this.labelMainMessage.TabIndex = 6;
            this.labelMainMessage.Text = "The application has detected that a mandatory content update is necessary. Click " +
                "\'OK\' to begin this content update.";
            // 
            // lblUpdateStatus
            // 
            this.lblUpdateStatus.AutoSize = true;
            this.lblUpdateStatus.Location = new System.Drawing.Point(16, 194);
            this.lblUpdateStatus.Name = "lblUpdateStatus";
            this.lblUpdateStatus.Size = new System.Drawing.Size(0, 13);
            this.lblUpdateStatus.TabIndex = 5;
            this.lblUpdateStatus.Visible = false;
            // 
            // pbStatus
            // 
            this.pbStatus.Location = new System.Drawing.Point(15, 164);
            this.pbStatus.Name = "pbStatus";
            this.pbStatus.Size = new System.Drawing.Size(355, 23);
            this.pbStatus.TabIndex = 4;
            this.pbStatus.Visible = false;
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(295, 286);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 6;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // backgroundWorkerGitUrDone
            // 
            this.backgroundWorkerGitUrDone.WorkerReportsProgress = true;
            this.backgroundWorkerGitUrDone.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerGitUrDone_DoWork);
            this.backgroundWorkerGitUrDone.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerGitUrDone_RunWorkerCompleted);
            this.backgroundWorkerGitUrDone.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorkerGitUrDone_ProgressChanged);
            // 
            // ContentUpdateForm_1_9
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(382, 316);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.pnlBody);
            this.Controls.Add(this.pnlTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ContentUpdateForm_1_9";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = global::Client.Properties.Settings.Default.AppNameFull;
            this.Shown += new System.EventHandler(this.ContentUpdateForm_1_9_Shown);
            this.pnlTop.ResumeLayout(false);
            this.pnlTop.PerformLayout();
            this.pnlBody.ResumeLayout(false);
            this.pnlBody.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblHeading;
        private System.Windows.Forms.Panel pnlTop;
        private System.Windows.Forms.Panel pnlBody;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Label lblUpdateStatus;
        private System.Windows.Forms.ProgressBar pbStatus;
        private System.ComponentModel.BackgroundWorker backgroundWorkerGitUrDone;
        private System.Windows.Forms.Label labelMainMessage;
    }
}