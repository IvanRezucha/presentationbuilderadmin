using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Deployment.Application;
using System.Net.NetworkInformation; //**SS Network Check

namespace Client
{
    public partial class ContentUpdateForm : MasterForm
    {
        ContentUpdater.UpdateType _UpdateType;
        ClosingAction _ClosingAction;
		ContentUpdater _ContentUpdater = new ContentUpdater();
        public bool CancelClicked = false;
        //BLL.LuceneIndex ind = new BLL.LuceneIndex();
        private bool isFirstRun;
        
		
        /// <summary>
        /// Initializes a new instance of the <see cref="ContentUpdateForm"/> class.
        /// </summary>
        /// <param name="updateType">Type of the update.</param>
        public ContentUpdateForm(ContentUpdater.UpdateType updateType)
        {
            Init(updateType);
        }
        public ContentUpdateForm(ContentUpdater.UpdateType updateType, bool isFirstRun)
        {
            this.isFirstRun = isFirstRun;
            Init(updateType);
        }
        public void Init(ContentUpdater.UpdateType updateType)
        {
            InitializeComponent();
            NetworkChange.NetworkAvailabilityChanged += new NetworkAvailabilityChangedEventHandler(ApplicationNetworkAvailabilityChanged);
            _UpdateType = updateType;
            _ClosingAction = ClosingAction.Close;
        }

        void ApplicationNetworkAvailabilityChanged(object sender, NetworkAvailabilityEventArgs networkAvailabilityEventArgs)
        {
           // check to see if the interface is avilable
           string adpStatus = NetworkInterface.GetIsNetworkAvailable() ? "Connected" : "Disconnected";
           if (adpStatus == "Disconnected")
           {
               MessageBox.Show("You have lost your network connection. If you have not completed your content download, you may not be able to view all files offline.");
           }
        }

       

		/// <summary>
		/// Pauses the current content update, but only if the update is Regular or Partial.
		/// </summary>
		public void PauseContentUpdate()
		{
			if (_UpdateType == ContentUpdater.UpdateType.LocalizedRegular || _UpdateType == ContentUpdater.UpdateType.LocalizedPartial)
			{
				_ContentUpdater.PauseLocalizeContentUpdateAsync();
			}
		}

        #region form setup

		/// <summary>
        /// Handles the Load event of the ContentUpdateForm control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void ContentUpdateForm_Load(object sender, EventArgs e)
        {
			// If the Resume Update button is displayed hide it since the application is currently doing an update.
			((IpgMain)Owner).ShowResumeUpdateButton = false;

            //*SS Update the IPG progress bar
            ((IpgMain)this.Owner).UpdateIPGContentProgress(0);

			_ContentUpdater.ContentUpdateProgressReported += new ContentUpdater.ContentUpdateProgressReportedDelegate(contentUpdater_ContentUpdateProgressReported);
			_ContentUpdater.ContentUpdateCompleted += new ContentUpdater.ContentUpdateCompletedDelegate(contentUpdater_ContentUpdateCompleted);
			_ContentUpdater.CanPauseProcess += new ContentUpdater.CanPauseProcessDelegate(contentUpdater_CanPauseProcess);
			_ContentUpdater.ContentUpdatePaused += new ContentUpdater.ContentUpdatePausedDelegate(contentUpdater_ContentUpdatePaused);
            _ContentUpdater.UpdateToBeIndexed += new ContentUpdater.UpdateToBeIndexedDelegate(indexUpdater_UpdateToBeIndexed);
            _ContentUpdater.UpdateRemoveIndex += new ContentUpdater.UpdateRemoveIndexDelegate(indexUpdater_UpdateRemoveIndex);

            PopulateUpdateTypeList();

			if (_UpdateType == ContentUpdater.UpdateType.LocalizedRegular || _UpdateType == ContentUpdater.UpdateType.LocalizedPartial)
			{
				btnPauseUpdate.Visible = true;
			}
			else
			{
				btnPauseUpdate.Visible = false;
			}
        }

        /// <summary>
        /// Populates the update type list.
        /// </summary>
        private void PopulateUpdateTypeList()
        {
            List<ComboItem> comboItems = new List<ComboItem>();
			
            if (_UpdateType == ContentUpdater.UpdateType.LocalizedNew)
            {
                ComboItem ci = new ComboItem();
                ci.ComboItemText = "Download all content";
                ci.ComboUpdateType = ContentUpdater.UpdateType.LocalizedNew;
                comboItems.Add(ci);
            }
            else if (_UpdateType == ContentUpdater.UpdateType.LocalizedRegular)
            {
                ComboItem ci = new ComboItem();
                ci.ComboItemText = "Download updated content";
                ci.ComboUpdateType = ContentUpdater.UpdateType.LocalizedRegular;
                comboItems.Add(ci);
            }
            else if (_UpdateType == ContentUpdater.UpdateType.LocalizedFull)
            {
                ComboItem ci = new ComboItem();
                ci.ComboItemText = "Download all content";
                ci.ComboUpdateType = ContentUpdater.UpdateType.LocalizedFull;
                comboItems.Add(ci);
            }
			else if (_UpdateType == ContentUpdater.UpdateType.LocalizedPartial)
			{
				ComboItem ci = new ComboItem();
				ci.ComboItemText = "Download remaining content";
				ci.ComboUpdateType = ContentUpdater.UpdateType.LocalizedPartial;
				comboItems.Add(ci);
			}
          
            cboUpdateType.DataSource = comboItems;
            cboUpdateType.DisplayMember = "ComboItemText";
            cboUpdateType.ValueMember = "ComboUpdateType";

            cboUpdateType.SelectedValue = _UpdateType;

        } 

	    #endregion

        #region content updater events
        
		/// <summary>
        /// Contents the updater_ content update completed.
        /// </summary>
        private void contentUpdater_ContentUpdateCompleted()
        {
            this.Refresh();
            ((IpgMain)this.Owner).PopulateLocalizedTreeviews();

            //*SS Update the IPG progress bar
            ((IpgMain)this.Owner).UpdateIPGContentProgressComplete();

            if (_UpdateType == ContentUpdater.UpdateType.LocalizedFull || _UpdateType == ContentUpdater.UpdateType.LocalizedNew || (_UpdateType == ContentUpdater.UpdateType.LocalizedRegular && isFirstRun))
                ((IpgMain)this.Owner)._fullIndex = true;

            wsIpgSlideUpdater.SlideUpdater wsSlideUpdater = new Client.wsIpgSlideUpdater.SlideUpdater();
            try
            {
                wsSlideUpdater.SendClientDataUpdateInfoAsync(Properties.Settings.Default.LastLocalizedContentID,
                    ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString(),
                    Environment.OSVersion.ToString());
            }
            catch (Exception ex)
            {
                //don't worry about it, this is trasparent to the user
            }


            CancelClicked = true;
            this.Close();
        }

        /// <summary>
        /// Contents the updater_ content update progress reported.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="progressPercent">The progress percent.</param>
        private void contentUpdater_ContentUpdateProgressReported(string message, int progressPercent)
        {
            this.Refresh();
            lblUpdateStatus.Text = message;
            pbStatus.Value = progressPercent;

            //*SS Update the IPG progress bar
            ((IpgMain)this.Owner).UpdateIPGContentProgress(progressPercent);
        }

		private void contentUpdater_CanPauseProcess()
		{
			btnPauseUpdate.Enabled = true;
		}

		private void contentUpdater_ContentUpdatePaused()
		{
			this.Visible = false;

			string msg = "Content update has been paused.  Select the Resume Content Update button on the status bar.";
			MessageBox.Show(this.ParentForm, msg, "Content Update Paused", MessageBoxButtons.OK, MessageBoxIcon.Information);
			
			this.Close();
		}

        #endregion

        //#region Index updater events

        ///// <summary>
        ///// Contents the updater_ content update completed.
        ///// </summary>
        //private void indexUpdater_ContentUpdateCompleted()
        //{

        //    //*SS Update the IPG progress bar
        //    ((IpgMain)this.Owner).UpdateIndexProgressComplete();
           
        //}

        ///// <summary>
        ///// Contents the updater_ content update progress reported.
        ///// </summary>
        ///// <param name="message">The message.</param>
        ///// <param name="progressPercent">The progress percent.</param>
        //private void indexUpdater_ContentUpdateProgressReported(int progressPercent)
        //{
        //    this.Refresh();

        //    //*SS Update the IPG progress bar
        //    ((IpgMain)this.Owner).UpdateIndexProgress(progressPercent);
        //}        

        //#endregion

        #region form events

        /// <summary>
        /// Handles the SelectedValueChanged event of the cboUpdateType control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void cboUpdateType_SelectedValueChanged(object sender, EventArgs e)
        {
            if (cboUpdateType.SelectedItem != null)
            {            
                ComboItem ci = (ComboItem)cboUpdateType.SelectedItem;
                _UpdateType = ci.ComboUpdateType;
                string message = "";
                switch (_UpdateType)
                {
                    case ContentUpdater.UpdateType.LocalizedNew:
                        message = String.Format("The {0} has detected that this is the initial content is should be downloaded. The {0} will download content based on you My Region/Languages selection.",
                            Properties.Settings.Default.AppNameFull);
                        break;
                    case ContentUpdater.UpdateType.LocalizedRegular:
                        message = String.Format("The {0} has detected there is new and/or updated content available.{1}{1}Press 'OK' to download the new content.{1}{1}Press 'Cancel' if you would like to download the new content at a later time.",
                            Properties.Settings.Default.AppNameFull, System.Environment.NewLine);
                        break;
                    case ContentUpdater.UpdateType.LocalizedFull:
                        message = String.Format("If you believe the {0} is missing content you may choose to download all of the available content.{1}{1}Press 'OK' to downoad all of the content.{1}{1}Press 'Cancel' if you would like to download the content at a later time.",
                           Properties.Settings.Default.AppNameFull, System.Environment.NewLine);
                        break;
                    default:
                        message = String.Format("If you believe the {0} is missing content you may choose to download all of the available content.{1}{1}Press 'OK' to downoad all of the content.{1}{1}Press 'Cancel' if you would like to download the content at a later time.",
                            Properties.Settings.Default.AppNameFull, System.Environment.NewLine);
                        break;
                }
                lblMessage.Text = message;

            }

        }

        /// <summary>
        /// Handles the Click event of the btnCancel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void btnCancel_Click(object sender, EventArgs e)
        {
            //if (_UpdateType == ContentUpdater.UpdateType.New)
            //{
            //    _ClosingAction = ClosingAction.Quit;
            //}
			if (_UpdateType == ContentUpdater.UpdateType.LocalizedPartial)
				((IpgMain)Owner).ShowResumeUpdateButton = true;

            NotDoneMessage();

            CancelClicked = true;

            this.Close();
        }

        /// <summary>
        /// Handles the Click event of the btnOK control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void btnOK_Click(object sender, EventArgs e)
        {
            //disable controls during content download
            btnCancel.Enabled = false;
            btnOK.Enabled = false;
            cboUpdateType.Enabled = false;

            lblUpdateStatus.Visible = true;
            pbStatus.Visible = true;
            _ContentUpdater.IsFirstRun = isFirstRun;
			_ContentUpdater.UpdateLocalizeContentAsync(_UpdateType);
        }

		/// <summary>
		/// Allows the user to pause the update process and continue at a later time.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		private void btnPauseUpdate_Click(object sender, EventArgs e)
		{
			_ContentUpdater.PauseLocalizeContentUpdateAsync();
			((IpgMain)Owner).ShowResumeUpdateButton = true;

            CancelClicked = true;
		}

		/// <summary>
		/// Handles the FormClosing event of the ContentUpdateForm control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Windows.Forms.FormClosingEventArgs"/> instance containing the event data.</param>
		private void ContentUpdateForm_FormClosing(object sender, FormClosingEventArgs e)
		{
            if (CancelClicked)
            {
                if ((_UpdateType == ContentUpdater.UpdateType.LocalizedPartial || _UpdateType == ContentUpdater.UpdateType.LocalizedRegular) && _ContentUpdater.IsWorkInProgress)
                {
                    _ContentUpdater.PauseLocalizeContentUpdateAsync();
                    ((IpgMain)Owner).ShowResumeUpdateButton = true;
                }


                if (_ClosingAction == ClosingAction.Quit)
                {
                    Application.Exit();
                }
            }
            else
            {
                MessageBox.Show("Please use the cancel button to close this form.");
                e.Cancel = true;
            }
            
		}

        private void NotDoneMessage()
        {
            MessageBox.Show("If you have not run a content update, not all files will be available in offline mode.");
        }

        

        

        private void ContentUpdateForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (_UpdateType == ContentUpdater.UpdateType.LocalizedPartial || _UpdateType == ContentUpdater.UpdateType.LocalizedRegular)
            {
                ((IpgMain)Owner)._UpdateIndex = true;
            }
        }

        private void indexUpdater_UpdateToBeIndexed(string f)
        {
            ((IpgMain)Owner)._toBeIndexed.Add(f);
        }

        private void indexUpdater_UpdateRemoveIndex(string f)
        {
            ((IpgMain)Owner)._removeIndex.Add(f);
        }

        #endregion
    }

    public class ComboItem
        {
            private string _ComboItemText;

            public string ComboItemText
            {
                get { return _ComboItemText; }
                set { _ComboItemText = value; }
            }

        private ContentUpdater.UpdateType _ComboUpdateType;

        public ContentUpdater.UpdateType ComboUpdateType
            {
                get { return _ComboUpdateType; }
                set { _ComboUpdateType = value; }
            }

        }

    public enum ClosingAction
    { 
        Close,
        Quit
    }

   
}