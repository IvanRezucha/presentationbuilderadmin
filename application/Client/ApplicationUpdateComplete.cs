using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Client
{
	public partial class ApplicationUpdateComplete : MasterForm
	{
		public ApplicationUpdateComplete()
		{
			InitializeComponent();
		}

		private void btnRestart_Click(object sender, EventArgs e)
		{
            Application.Restart();
		}

		private void btnContinue_Click(object sender, EventArgs e)
		{
            this.Close();
		}
	}
}

