using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Client
{
    public partial class ApplicationSetupWizard : MasterWizardForm
    {
        //class variables
        ContentUpdater _ContentUpdater;

        public ApplicationSetupWizard()
        {
            InitializeComponent();
            
        }

        private void LocalizedContentSetupWizard_Load(object sender, EventArgs e)
        {
            AppSetup appSetup = new AppSetup();
            try
            {
                appSetup.RegistrySetup();
            }
            catch (Exception)
            {

            }
            appSetup.CreateDesktopShortcut();
            string welcomeMessage;
            welcomeMessage = String.Format("Welcome to the {0} Application Setup Wizard", Properties.Settings.Default.AppNameFull);
            richTextBoxWelcomeTitle.Text = welcomeMessage;

        }

        private void btnWelcomeNext_Click(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.IsContentLocalizationSupported)
            {
                regionLanguageGroupGrid.FillRegionLanguageGroupGrid();
                tabWizard.SelectTab(tabPageRegionLanguages); 
            }
            else
            {
                string regLangUri = String.Format("{0}LocalizedUpdateData/RegionLanguages.xml", Properties.Settings.Default.DataUpdateBaseURI);
                wsIpgSlideUpdater.SlideLibrary dsRegLangs = new Client.wsIpgSlideUpdater.SlideLibrary();
                dsRegLangs.EnforceConstraints = false;
                dsRegLangs.RegionLanguage.ReadXml(regLangUri);

                DataSets.MyRegionLanguages dsMyRegLangs = new Client.DataSets.MyRegionLanguages();
                dsMyRegLangs._MyRegionLanguages.AddMyRegionLanguagesRow(((wsIpgSlideUpdater.SlideLibrary.RegionLanguageRow)dsRegLangs.RegionLanguage.Rows[0]).RegionLanguageID);
                dsMyRegLangs.WriteXml(Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, AppSettings.LocalFiles.Default.MyRegionLangauges));
                StartGettingContent();
                tabWizard.SelectTab(tabPageContentUpdateProgress);
            }
        }

        private void btnRegLangNext_Click(object sender, EventArgs e)
        {
            regionLanguageGroupGrid.SaveMyRegionLanguages();
            tabWizard.SelectTab(tabPageContentUpdateProgress);
            StartGettingContent();
            _ContentUpdater_ContentUpdateCompleted();
        }

        private void StartGettingContent()
        {
            //*SS Commented out this code as we no longer want to download content at this stage.

            //start downloading content
            _ContentUpdater = new ContentUpdater();
            //_ContentUpdater.ContentUpdateProgressReported += new ContentUpdater.ContentUpdateProgressReportedDelegate(_ContentUpdater_ContentUpdateProgressReported);
           //_ContentUpdater.ContentUpdateCompleted += new ContentUpdater.ContentUpdateCompletedDelegate(_ContentUpdater_ContentUpdateCompleted);
            //_ContentUpdater.UpdateLocalizeContentAsync(ContentUpdater.UpdateType.LocalizedNew);

            
        }

        private void _ContentUpdater_ContentUpdateCompleted()
        {            
            Properties.Settings.Default.IsFirstRun = false;
			Properties.Settings.Default.IsSupportFilesLocalized = true;
            if (Properties.Settings.Default.IsContentLocalizationSupported)
            {
                Properties.Settings.Default.IsAppLocalizedContentReady = true;
            }
            
            Properties.Settings.Default.Save();

            //update upgrade settings
            Upgrades.UpgradeSettings.Default.IsReadyFor_1_9 = true;
            Upgrades.UpgradeSettings.Default.Save();

            lblWorkerStatus.Text = "";
            btnFinished.Enabled = true;
            lblWait.Visible = false;
            lblFinished.Visible = true;
        }

        private void _ContentUpdater_ContentUpdateProgressReported(string message, int progressPercent)
        {
            lblWorkerStatus.Text = message;
            progressBarContentUpdate.Value = progressPercent;
        }

        private void btnFinished_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}