namespace Client
{
    partial class MasterWizardForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gradientCaptionWizardTitle = new Ascend.Windows.Forms.GradientCaption();
            this.tabWizard = new System.Windows.Forms.TabControl();
            this.SuspendLayout();
            // 
            // gradientCaptionWizardTitle
            // 
            this.gradientCaptionWizardTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.gradientCaptionWizardTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gradientCaptionWizardTitle.Location = new System.Drawing.Point(0, 0);
            this.gradientCaptionWizardTitle.Name = "gradientCaptionWizardTitle";
            this.gradientCaptionWizardTitle.Size = new System.Drawing.Size(552, 65);
            this.gradientCaptionWizardTitle.TabIndex = 4;
            this.gradientCaptionWizardTitle.Text = "Application Setup Wizard";
            // 
            // tabWizard
            // 
            this.tabWizard.Location = new System.Drawing.Point(-4, 40);
            this.tabWizard.Name = "tabWizard";
            this.tabWizard.SelectedIndex = 0;
            this.tabWizard.Size = new System.Drawing.Size(560, 431);
            this.tabWizard.TabIndex = 3;
            // 
            // MasterWizardForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(552, 469);
            this.Controls.Add(this.gradientCaptionWizardTitle);
            this.Controls.Add(this.tabWizard);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "MasterWizardForm";
            this.Load += new System.EventHandler(this.MasterWizardForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        public Ascend.Windows.Forms.GradientCaption gradientCaptionWizardTitle;
        public System.Windows.Forms.TabControl tabWizard;

    }
}