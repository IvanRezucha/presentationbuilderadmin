using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Data;
using System.Windows.Forms;
using System.Threading;

namespace Client
{
    public class ContentUpdater
    {
        #region class variables
        private wsIpgSlideUpdater.SupportFileDS _OldSupportFiles;
        private wsIpgSlideUpdater.SupportFileDS _NewSupportFiles;
        private SortedList<int, wsIpgSlideUpdater.SupportFileDS> _NewLocalizedSupportFileData;
        private wsIpgSlideUpdater.SlideLibrary _NewMainData;
        private SortedList<int, wsIpgSlideUpdater.SlideLibrary> _NewLocalizedSlideData;
        private wsIpgSlideUpdater.SlideLibrary _NewMergedData;
        private wsIpgSlideUpdater.SlideLibrary _OldMergedData;
        private DataSets.MyRegionLanguages _MyRegionLanguages;

        private List<string> _FilesToDownload;
        private List<string> _FilesToDelete;
        System.ComponentModel.BackgroundWorker _BackgroundWorkerFullLocalized;
        System.ComponentModel.BackgroundWorker _BackgroundWorkerRegularLocalized;
        System.ComponentModel.BackgroundWorker _BackgroundWorkerPartialLocalized;

        private bool _IsSilentCheck;
        public bool IsFirstRun;
        private int? _CurrentLocalizedUpdateID;

        #endregion

        #region properties

        private bool _IsWorkInProgress;
        private bool _IsUpdatePaused;
        private UpdateType _currentUpdateType;
        private wsUserRegistration.UserRegistrationService _userRegService = new Client.wsUserRegistration.UserRegistrationService();

        /// <summary>
        /// Gets a value indicating whether this instance is work in progress.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is work in progress; otherwise, <c>false</c>.
        /// </value>
        public bool IsWorkInProgress
        {
            get { return _IsWorkInProgress; }
        }

        /// <summary>
        /// Gets a value indicating whether the update process has been put on hold.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is put on pause; otherwise, <c>false</c>.
        /// </value>
        public bool IsUpdatePaused
        {
            get { return _IsUpdatePaused; }
            private set { _IsUpdatePaused = value; }
        }

        /// <summary>
        /// Maintains the current update type
        /// </summary>
        private UpdateType CurrentUpdateType
        {
            get { return _currentUpdateType; }
            set { _currentUpdateType = value; }
        }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="ContentUpdater"/> class.
        /// </summary>
        public ContentUpdater()
        {
            _IsWorkInProgress = false;
        }

        #region public methods

        public void CheckForLocalizedContentUpdateAsync(bool isSilent)
        {
            _IsWorkInProgress = true;
            _IsSilentCheck = isSilent;

            wsIpgSlideUpdater.SlideUpdater wsSlideUpdater = new Client.wsIpgSlideUpdater.SlideUpdater();

            wsSlideUpdater.IsLocalizedContentAvailableCompleted += new Client.wsIpgSlideUpdater.IsLocalizedContentAvailableCompletedEventHandler(wsSlideUpdater_IsLocalizedContentAvailableCompleted);

            try
            {
                wsSlideUpdater.IsServiceAvailable();
            }
            catch (Exception)
            {
                //if we got here, we aren't online and can't do an update
                _IsWorkInProgress = false;
                return;
            }

            wsSlideUpdater.IsLocalizedContentAvailableAsync(Properties.Settings.Default.LastLocalizedContentID);
        }

        /// <summary>
        /// Updates the localize content async.
        /// </summary>
        /// <param name="localContentDirectory">The local content directory.</param>
        /// <param name="localThumbnailDirectory">The local thumbnail directory.</param>
        /// <param name="currentSupportFileDataFile">The current support file data file.</param>
        /// <param name="myRegionLanguagesFile">My region languages file.</param>
        /// <param name="mainDataFile">The main data file.</param>
        /// <param name="updateType">Type of the update.</param>
        public void UpdateLocalizeContentAsync(UpdateType updateType)
        {
            _IsWorkInProgress = true;
            CurrentUpdateType = updateType;

            Properties.Settings.Default.ContentUpdateSessionStartDate = DateTime.UtcNow;
            Properties.Settings.Default.ContentUpdateType = updateType.ToString();
            Properties.Settings.Default.Save();

            //run ther appropriate BackgroundWorker
            if (updateType == UpdateType.LocalizedRegular)
            {
                _BackgroundWorkerRegularLocalized = new System.ComponentModel.BackgroundWorker();
                _BackgroundWorkerRegularLocalized.DoWork += new System.ComponentModel.DoWorkEventHandler(_BackgroundWorkerRegularLocalized_DoWork);
                _BackgroundWorkerRegularLocalized.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(_BackgroundWorkerRegularLocalized_ProgressChanged);
                _BackgroundWorkerRegularLocalized.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(_BackgroundWorkerRegularLocalized_RunWorkerCompleted);
                _BackgroundWorkerRegularLocalized.WorkerReportsProgress = true;
                _BackgroundWorkerRegularLocalized.WorkerSupportsCancellation = true;
                _BackgroundWorkerRegularLocalized.RunWorkerAsync();
            }
            else if (updateType == UpdateType.LocalizedFull || updateType == UpdateType.LocalizedNew)
            {
                _BackgroundWorkerFullLocalized = new System.ComponentModel.BackgroundWorker();
                _BackgroundWorkerFullLocalized.DoWork += new System.ComponentModel.DoWorkEventHandler(_BackgroundWorkerFullLocalized_DoWork);
                _BackgroundWorkerFullLocalized.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(_BackgroundWorkerFullLocalized_ProgressChanged);
                _BackgroundWorkerFullLocalized.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(_BackgroundWorkerFullLocalized_RunWorkerCompleted);
                _BackgroundWorkerFullLocalized.WorkerReportsProgress = true;
                _BackgroundWorkerFullLocalized.RunWorkerAsync();
            }
            else if (updateType == UpdateType.LocalizedPartial)
            {
                _BackgroundWorkerPartialLocalized = new System.ComponentModel.BackgroundWorker();
                _BackgroundWorkerPartialLocalized.DoWork += new System.ComponentModel.DoWorkEventHandler(_BackgroundWorkerPartialLocalized_DoWork);
                _BackgroundWorkerPartialLocalized.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(_BackgroundWorkerPartialLocalized_ProgressChanged);
                _BackgroundWorkerPartialLocalized.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(_BackgroundWorkerPartialLocalized_RunWorkerCompleted);
                _BackgroundWorkerPartialLocalized.WorkerReportsProgress = true;
                _BackgroundWorkerPartialLocalized.WorkerSupportsCancellation = true;
                _BackgroundWorkerPartialLocalized.RunWorkerAsync();
            }
        }

        public void PauseLocalizeContentUpdateAsync()
        {
            Properties.Settings.Default.ContentUpdateDuration += Convert.ToInt32(DateTime.UtcNow.Subtract(Properties.Settings.Default.ContentUpdateSessionStartDate).TotalSeconds);
            Properties.Settings.Default.Save();

            if (CurrentUpdateType == UpdateType.LocalizedRegular)
            {
                _BackgroundWorkerRegularLocalized.CancelAsync();
            }
            else
            {
                _BackgroundWorkerPartialLocalized.CancelAsync();
            }
        }

        #endregion

        #region private methods

        /// <summary>
        /// Reports the region languages the user has to the server for tracking
        /// </summary>
        private void ReportCurrentRegionLanguagesToServer()
        {
            wsUserRegistration.UserRegistrationService userRegService = new Client.wsUserRegistration.UserRegistrationService();

            List<int> regionLanguages = new List<int>();
            foreach (DataSets.MyRegionLanguages.MyRegionLanguagesRow row in _MyRegionLanguages._MyRegionLanguages.Rows)
            {
                regionLanguages.Add(row.RegionLanguageID);
            }

            userRegService.SubmitUserRegionLanguagesForUserAsync(Properties.Settings.Default.UserRegistrationEmail, regionLanguages.ToArray());
        }

        /// <summary>
        /// Deletes the old files.
        /// </summary>
        private void DeleteOldFiles()
        {
            foreach (string file in _FilesToDelete)
            {
                try
                {
                    File.Delete(Path.Combine(AppSettings.LocalPaths.Default.SlideDirectory, file));
                    if (file.EndsWith(".pot"))
                    {
                        File.Delete(Path.Combine(AppSettings.LocalPaths.Default.ThumbnailDirectory, file.Replace(".pot", ".jpg")));
                        
                        
                    }
                    //*SS Add to the RemoveIndex list so we can remove this file from the index later
                    UpdateRemoveIndex(file);   
                }
                catch (Exception ex)
                {
                    //TODO: Handle exception
                }
            }
        }

        /// <summary>
        /// The initial download downloads all files uninterruptedly by not giving the user the ability to pause the file download.
        /// </summary>
        private void DownloadFilesInitial()
        {
            System.Net.WebClient wc = new System.Net.WebClient();
            int filesDownloaded = 0;
            double percentDownloaded = 0;

            foreach (string file in _FilesToDownload)
            {
                Uri uri = new Uri(Properties.Settings.Default.ServerSlideBaseURI + file);
                string filePath = Path.Combine(AppSettings.LocalPaths.Default.SlideDirectory, file);
                try
                {
                    if (File.Exists(filePath))
                    {
                        FileInfo fi = new FileInfo(filePath);
                        fi.Attributes = FileAttributes.Normal;
                    }
                    wc.DownloadFile(uri, filePath);
                }
                catch (Exception ex)
                {
                    //do nothing
                }
                //if the file is a slide, download the image
                if (uri.AbsoluteUri.EndsWith(".pot"))
                {
                    Uri imageUri = new Uri(Properties.Settings.Default.ServerImageBaseURI + file.Replace(".pot", ".jpg"));
                    filePath = Path.Combine(AppSettings.LocalPaths.Default.ThumbnailDirectory, file.Replace(".pot", ".jpg"));
                    try
                    {
                        if (File.Exists(filePath))
                        {
                            FileInfo fi = new FileInfo(filePath);
                            fi.Attributes = FileAttributes.Normal;
                        }
                        wc.DownloadFile(imageUri, filePath);
                    }
                    catch (Exception ex)
                    {
                        //do nothing
                    }
                }

                filesDownloaded++;
                percentDownloaded = (Convert.ToDouble(filesDownloaded) / Convert.ToDouble(_FilesToDownload.Count)) * 100.00;

                _BackgroundWorkerFullLocalized.ReportProgress(Convert.ToInt32(percentDownloaded), String.Format("Downloading file {0} of {1}.", filesDownloaded, _FilesToDownload.Count));
            }
        }

        /// <summary>
        /// Download content files and has the ability to pause a file download and continue at a later time.
        /// </summary>
        /// <param name="bgWorker">Current background worker performing the content update</param>
        private void DownloadFiles(System.ComponentModel.BackgroundWorker bgWorker)
        {
            System.Net.WebClient wc = new System.Net.WebClient();
            DataSets.ContentFiles contentFiles = new Client.DataSets.ContentFiles();
            string contentFilesDataSetPath = Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, AppSettings.LocalFiles.Default.ContentFiles);
            bool resumePreviousUpdate = (CurrentUpdateType == UpdateType.LocalizedPartial && File.Exists(contentFilesDataSetPath));
            int filesDownloaded = 0;
            double percentDownloaded = 0;

            // Resume a previous update
            if (resumePreviousUpdate)
            {
                string mutexName = AppSettings.LocalFiles.Default.ContentFiles;
                Mutex m = new Mutex(false, mutexName);
                m.WaitOne();
                contentFiles.ReadXml(contentFilesDataSetPath);
                m.ReleaseMutex();

                _FilesToDownload = new List<string>();

                foreach (DataSets.ContentFiles.DownloadContentRow contentFile in contentFiles.DownloadContent.Rows)
                {
                    if (!contentFile.IsFileDownloaded)
                    {
                        _FilesToDownload.Add(contentFile.FileName);
                    }
                }
            }
            else
            {
                // There isn't a previous content update, copy the list of files to the DownloadContent datatable to get a current view of all the download content.  Later
                // as each file gets downloaded we'll update the DownloadContent datatable to indicate that it has been successfully downloaded.  The datatable gets saved
                // only if the user decides to interrupt the download process and that becomes the list of files yet to be downloaded.
                foreach (string fileName in _FilesToDownload.ToArray())
                {
                    contentFiles.DownloadContent.AddDownloadContentRow(fileName, false);
                }
            }

            // Download each file in the _FilesToDownload's list
            foreach (string file in _FilesToDownload)
            {
                Uri uri = new Uri(Properties.Settings.Default.ServerSlideBaseURI + file);
                string filePath = Path.Combine(AppSettings.LocalPaths.Default.SlideDirectory, file);

				try
				{
					if (File.Exists(filePath))
					{
						FileInfo fi = new FileInfo(filePath);
						fi.Attributes = FileAttributes.Normal;
					}
					wc.DownloadFile(uri, filePath);
                    //*SS Add this file to the toBeIndexed list so that we can index this file later.
                    UpdateToBeIndexed(filePath);   
				}
				catch (Exception ex)
				{
					//do nothing
				}

                //if the file is a slide, download the image
                if (uri.AbsoluteUri.EndsWith(".pot"))
                {
                    Uri imageUri = new Uri(Properties.Settings.Default.ServerImageBaseURI + file.Replace(".pot", ".jpg"));
                    filePath = Path.Combine(AppSettings.LocalPaths.Default.ThumbnailDirectory, file.Replace(".pot", ".jpg"));
                    try
                    {
                        if (File.Exists(filePath))
                        {
                            FileInfo fi = new FileInfo(filePath);
                            fi.Attributes = FileAttributes.Normal;
                        }
                        wc.DownloadFile(imageUri, filePath);
                    }
                    catch (Exception ex)
                    {
                        //do nothing
                    }
                }

                filesDownloaded++;
                percentDownloaded = (Convert.ToDouble(filesDownloaded) / Convert.ToDouble(_FilesToDownload.Count)) * 100.00;

                // Keep a list of files to be downloaded
                contentFiles.DownloadContent.FindByFileName(file).Delete();
                contentFiles.AcceptChanges();

                // Cancel the file download
                if (bgWorker.CancellationPending)
                {
                    string mutexName = AppSettings.LocalFiles.Default.ContentFiles;
                    Mutex m = new Mutex(false, mutexName);
                    m.WaitOne();
                    contentFiles.WriteXml(Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, AppSettings.LocalFiles.Default.ContentFiles));
                    m.ReleaseMutex();
                    
                    return;
                }

                bgWorker.ReportProgress(Convert.ToInt32(percentDownloaded), String.Format("Downloading file {0} of {1}.", filesDownloaded, _FilesToDownload.Count));
            }
        }

        /// <summary>
        /// Appends the web path.
        /// </summary>
        /// <param name="file">The file.</param>
        private static void AppendWebPath(string file)
        {
            file = file.Insert(0, Properties.Settings.Default.ServerSlideBaseURI);
        }

        private void DetermineLocalizedFilesToDelete()
        {

            _FilesToDelete = new List<string>();

            //get the slides to delete
            foreach (wsIpgSlideUpdater.SlideLibrary.SlideCategoryRow oldSlide in _OldMergedData.SlideCategory.Rows)
            {

                if (!_FilesToDelete.Contains(oldSlide.SlideRow.FileName))
                {
                    string filter = "SlideID=" + oldSlide.SlideID.ToString();
                    if (_NewMergedData.SlideCategory.Select(filter).Length == 0)
                    {
                        _FilesToDelete.Add(oldSlide.SlideRow.FileName);
                    }
                }
            }

            //get the support files to delete
            foreach (wsIpgSlideUpdater.SupportFileDS.SupportFileRow supportFile in _OldSupportFiles.SupportFile.Rows)
            {
                if (!_FilesToDelete.Contains(supportFile.FileName))
                {
                    string filter = "SupportFileID=" + supportFile.SupportFileID.ToString();
                    if (_NewSupportFiles.SupportFile.Select(filter).Length == 0)
                    {
                        _FilesToDelete.Add(supportFile.FileName);
                    }
                }
            }
        }

        private void LoadAllFilesForFirstRun()
        {
            _FilesToDownload = new List<string>();

            foreach (wsIpgSlideUpdater.SlideLibrary.SlideCategoryRow newSlide in _NewMergedData.SlideCategory.Rows)
            {
                if (!_FilesToDownload.Contains(newSlide.SlideRow.FileName))
                    _FilesToDownload.Add(newSlide.SlideRow.FileName);
            }

            foreach (wsIpgSlideUpdater.SupportFileDS.SupportFileRow newSupportFile in _NewSupportFiles.SupportFile.Rows)
            {
                if (!_FilesToDownload.Contains(newSupportFile.FileName))
                    _FilesToDownload.Add(newSupportFile.FileName);
            }

            Properties.Settings.Default.IsSupportFilesLocalized = true;
            Properties.Settings.Default.Save();
        }

        private void DetermineLocalizedFilesToDownload()
        {
            _FilesToDownload = new List<string>();

            //get the slides to download
            foreach (wsIpgSlideUpdater.SlideLibrary.SlideCategoryRow newSlide in _NewMergedData.SlideCategory.Rows)
            {
                if (!_FilesToDownload.Contains(newSlide.SlideRow.FileName))
                {
                    string filter = "SlideID=" + newSlide.SlideID.ToString();
                    List<wsIpgSlideUpdater.SlideLibrary.SlideCategoryRow> oldSlides = new List<Client.wsIpgSlideUpdater.SlideLibrary.SlideCategoryRow>((wsIpgSlideUpdater.SlideLibrary.SlideCategoryRow[])_OldMergedData.SlideCategory.Select(filter));
                    if (oldSlides.Count == 0)
                    {
                        _FilesToDownload.Add(newSlide.SlideRow.FileName);
                                        
                    }
                    else
                    {
                        if (newSlide.SlideRow.DateModified > oldSlides[0].SlideRow.DateModified)
                        {
                            _FilesToDownload.Add(newSlide.SlideRow.FileName);
                              
                        }
                    }
                }
            }

            if (Properties.Settings.Default.IsSupportFilesLocalized)
            {
                //get the support files to download
                foreach (wsIpgSlideUpdater.SupportFileDS.SupportFileRow newSupportFile in _NewSupportFiles.SupportFile.Rows)
                {
                    if (!_FilesToDownload.Contains(newSupportFile.FileName))
                    {
                        string filter = "SupportFileID=" + newSupportFile.SupportFileID.ToString();
                        List<wsIpgSlideUpdater.SupportFileDS.SupportFileRow> oldSupportFiles = new List<Client.wsIpgSlideUpdater.SupportFileDS.SupportFileRow>((wsIpgSlideUpdater.SupportFileDS.SupportFileRow[])_OldSupportFiles.SupportFile.Select(filter));
                        if (oldSupportFiles.Count == 0)
                        {
                            _FilesToDownload.Add(newSupportFile.FileName);
                        }
                        else
                        {
                            if (newSupportFile.ModifyDate > oldSupportFiles[0].ModifyDate)
                            {
                                _FilesToDownload.Add(newSupportFile.FileName);
                            }
                        }
                    }
                }
            }
            else
            {
                foreach (wsIpgSlideUpdater.SupportFileDS.SupportFileRow newSupportFile in _NewSupportFiles.SupportFile.Rows)
                {
                    wsIpgSlideUpdater.SupportFileDS.SupportFileRow oldSupportFile = _OldSupportFiles.SupportFile.FindBySupportFileID(newSupportFile.SupportFileID);
                    if (!_FilesToDownload.Contains(newSupportFile.FileName))
                    {
                        if (oldSupportFile == null)
                        {
                            _FilesToDownload.Add(newSupportFile.FileName);
                        }
                        else
                        {
                            if (newSupportFile.ModifyDate > oldSupportFile.ModifyDate)
                            {
                                _FilesToDownload.Add(newSupportFile.FileName);
                            }
                        }
                    }

                }
                Properties.Settings.Default.IsSupportFilesLocalized = true;
                Properties.Settings.Default.Save();
            }

        }

        ///// <summary>
        ///// Determines the missing files.
        ///// </summary>
        ///// <param name="localContentDirectory">The local content directory.</param>
        //private void DetermineMissingFiles()
        //{
        //    _FilesToDownload = new List<string>();

        //    //get the missing slides
        //    foreach (wsIpgSlideUpdater.SlideLibrary.SlideCategoryRow slideCat in _OldSlideLibrary.SlideCategory.Rows)
        //    {
        //        string slideFile = Path.Combine(AppSettings.LocalPaths.Default.SlideDirectory, slideCat.SlideRow.FileName);
        //        if (!File.Exists(slideFile))
        //        {
        //            if (!_FilesToDownload.Contains(slideCat.SlideRow.FileName))
        //            {
        //                _FilesToDownload.Add(slideCat.SlideRow.FileName);
        //            }
        //        }
        //    }

        //    //get missing support files
        //    foreach (wsIpgSlideUpdater.SupportFileDS.SupportFileRow supportFile in _OldSupportFiles.SupportFile.Rows)
        //    {
        //        string supportFilePath = Path.Combine(AppSettings.LocalPaths.Default.SlideDirectory, supportFile.FileName);
        //        if (!File.Exists(supportFilePath))
        //        {
        //            if (!_FilesToDownload.Contains(supportFile.FileName))
        //            {
        //                _FilesToDownload.Add(supportFile.FileName);
        //            }
        //        }
        //    }


        //}

        private void LoadNewLocalizedData()
        {
            //get the main data file (tables that aren't subject to localization)
            _NewMainData = new Client.wsIpgSlideUpdater.SlideLibrary();
            _NewMainData.EnforceConstraints = false;
            string mainDataFileURI = String.Format("{0}{1}/{2}", Properties.Settings.Default.DataUpdateBaseURI, "LocalizedUpdateData", AppSettings.LocalFiles.Default.MainData);
            string mutexName = String.Format(AppSettings.LocalFiles.Default.MainData);
            Mutex m = new Mutex(false, mutexName);
            m.WaitOne();
            _NewMainData.ReadXml(mainDataFileURI);
            m.ReleaseMutex();
            

            //we'll merge this with the localized data
            _NewMergedData = new Client.wsIpgSlideUpdater.SlideLibrary();
            //_NewMergedData.EnforceConstraints = false;
            _NewMergedData.Merge(_NewMainData);

            _NewSupportFiles = new Client.wsIpgSlideUpdater.SupportFileDS();
            _NewSupportFiles.Merge(_NewMainData);
            _NewLocalizedSupportFileData = new SortedList<int, Client.wsIpgSlideUpdater.SupportFileDS>();

            _NewLocalizedSlideData = new SortedList<int, Client.wsIpgSlideUpdater.SlideLibrary>();

            //get localized slide data for each RegionLanguage that the user has selected to be updated
            List<int> myRegLangIds = new List<int>();
            foreach (DataSets.MyRegionLanguages.MyRegionLanguagesRow myRegLangRow in _MyRegionLanguages._MyRegionLanguages.Rows)
            {
                myRegLangIds.Add(myRegLangRow.RegionLanguageID);
                wsIpgSlideUpdater.SlideLibrary dsLocalizedSlideData = new Client.wsIpgSlideUpdater.SlideLibrary();
                dsLocalizedSlideData.EnforceConstraints = false;
                string s = String.Format("{0}{1}/{2}", Properties.Settings.Default.DataUpdateBaseURI, "LocalizedUpdateData",
                    String.Format("{0}{1}", myRegLangRow.RegionLanguageID, AppSettings.LocalFiles.Default.SlideDataFilePart));
                string mutexName2 = String.Format("{0}{1}", myRegLangRow.RegionLanguageID, AppSettings.LocalFiles.Default.SlideDataFilePart);
                Mutex m2 = new Mutex(false, mutexName2);
                m2.WaitOne();
                dsLocalizedSlideData.ReadXml(s);
                m2.ReleaseMutex();
                
                try
                {
                    _NewMergedData.Merge(dsLocalizedSlideData);
                }
                catch (ConstraintException ce)
                {
                    string errorstring = "";
                    // this reveals the ConstraintException error details for debugging
                    foreach (DataTable t in _NewMergedData.Tables)
                    {
                        foreach (DataRow row in t.Rows)
                        {
                            string errorString = row.RowError;
                            if (!String.IsNullOrEmpty(errorString))
                            {
                                // this is error(s)
                                errorstring += "Error found in Table " + t.TableName + ": " + errorString;
                            }
                        }
                    }
                    throw new Exception(errorstring, ce);
                }
                _NewLocalizedSlideData.Add(myRegLangRow.RegionLanguageID, dsLocalizedSlideData);
            }

            //support files
            foreach (int regLangId in myRegLangIds)
            {
                wsIpgSlideUpdater.SupportFileDS dsLocalizedSupportFileData = new Client.wsIpgSlideUpdater.SupportFileDS();
                dsLocalizedSupportFileData.EnforceConstraints = false;
                string mutexName2 = String.Format("{0}{1}", regLangId, AppSettings.LocalFiles.Default.SupportFileDataFilePart);
                Mutex m2 = new Mutex(false, mutexName2);
                m2.WaitOne();
                dsLocalizedSupportFileData.ReadXml(String.Format("{0}{1}/{2}", Properties.Settings.Default.DataUpdateBaseURI, "LocalizedUpdateData",
                     String.Format("{0}{1}", regLangId, AppSettings.LocalFiles.Default.SupportFileDataFilePart)));
                m2.ReleaseMutex();
                _NewSupportFiles.Merge(dsLocalizedSupportFileData);
                _NewLocalizedSupportFileData.Add(regLangId, dsLocalizedSupportFileData);
            }
        }

        private List<int> DetermineClientDefaultRegionLanguages(List<int> myRegLangIds)
        {
            //we also need to get any default RegLangs
            //put these ids in a list
            List<int> myClientDefaultRegLangIds = new List<int>();
            foreach (int myRegLangId in myRegLangIds)
            {
                int currentMyRegLangLangID = _NewMainData.RegionLanguage.FindByRegionLanguageID(myRegLangId).LanguageID;

                foreach (wsIpgSlideUpdater.SlideLibrary.RegionLanguageRow regLangRow in _NewMainData.Language.FindByLanguageID(currentMyRegLangLangID).GetRegionLanguageRows())
                {
                    if (regLangRow.RegionRow.IsClientDefault)
                    {
                        //don't put it in the list if it is already in there, we only want to download once
                        if (!myClientDefaultRegLangIds.Contains(regLangRow.RegionLanguageID))
                        {
                            myClientDefaultRegLangIds.Add(regLangRow.RegionLanguageID);
                        }
                    }
                }
            }
            return myClientDefaultRegLangIds;
        }

        private void LoadMyRegionLanguages()
        {
            //get my languages
            _MyRegionLanguages = new Client.DataSets.MyRegionLanguages();            
            string mutexName = AppSettings.LocalFiles.Default.MyRegionLangauges;
            Mutex m = new Mutex(false, mutexName);
            m.WaitOne();
            _MyRegionLanguages.ReadXml(Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, AppSettings.LocalFiles.Default.MyRegionLangauges));
            m.ReleaseMutex();
            
        }

        private void LoadOldLocalizedData()
        {
            _OldMergedData = new Client.wsIpgSlideUpdater.SlideLibrary();
            _OldSupportFiles = new Client.wsIpgSlideUpdater.SupportFileDS();
            _OldMergedData.EnforceConstraints = false;
            _OldSupportFiles.EnforceConstraints = false;
            //put in the data that isn't subject to localization
            string mainDataFile = Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, AppSettings.LocalFiles.Default.MainData);
            try
            {
                string mutexName = AppSettings.LocalFiles.Default.MainData;
                Mutex m = new Mutex(false, mutexName);
                m.WaitOne();
                _OldMergedData.ReadXml(mainDataFile);
                m.ReleaseMutex();
               
            }
            catch
            {
                HandleCorruptedFile(mainDataFile);
            }
            string mutexName2 = AppSettings.LocalFiles.Default.MainData;
            Mutex m2 = new Mutex(false, mutexName2);
            m2.WaitOne();
            _OldSupportFiles.ReadXml(Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, AppSettings.LocalFiles.Default.MainData));
            m2.ReleaseMutex();
            
            //now drop in data from each localized slide data file

            List<int> myRegLangIds = new List<int>();
            foreach (DataSets.MyRegionLanguages.MyRegionLanguagesRow myRegLang in _MyRegionLanguages._MyRegionLanguages.Rows)
            {
                myRegLangIds.Add(myRegLang.RegionLanguageID);
                string localizedDataFile = Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, String.Format("{0}{1}", myRegLang.RegionLanguageID.ToString(), AppSettings.LocalFiles.Default.SlideDataFilePart));
                if (File.Exists(localizedDataFile))
                {
                    wsIpgSlideUpdater.SlideLibrary dsLocalizedSlideData = new Client.wsIpgSlideUpdater.SlideLibrary();
                    dsLocalizedSlideData.EnforceConstraints = false;
                    try
                    {
                        string mutexName = String.Format("{0}{1}", myRegLang.RegionLanguageID.ToString(), AppSettings.LocalFiles.Default.SlideDataFilePart);
                        Mutex m = new Mutex(false, mutexName);
                        m.WaitOne();
                        dsLocalizedSlideData.ReadXml(localizedDataFile);
                        m.ReleaseMutex();
                    }
                    catch
                    {
                        HandleCorruptedFile(localizedDataFile);
                    }
                    _OldMergedData.Merge(dsLocalizedSlideData);
                }
            }

            if (Properties.Settings.Default.IsSupportFilesLocalized)
            {
                foreach (DataSets.MyRegionLanguages.MyRegionLanguagesRow myRegLang in _MyRegionLanguages._MyRegionLanguages.Rows)
                {
                    string localizedDataFile = Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, string.Format("{0}{1}", myRegLang.RegionLanguageID.ToString(), AppSettings.LocalFiles.Default.SupportFileDataFilePart));
                    if (File.Exists(localizedDataFile))
                    {
                        wsIpgSlideUpdater.SupportFileDS dsLocalizedSupportFileData = new Client.wsIpgSlideUpdater.SupportFileDS();
                        dsLocalizedSupportFileData.EnforceConstraints = false;
                        string mutexName = string.Format("{0}{1}", myRegLang.RegionLanguageID.ToString(), AppSettings.LocalFiles.Default.SupportFileDataFilePart);
                        Mutex m = new Mutex(false, mutexName);
                        m.WaitOne();
                        dsLocalizedSupportFileData.ReadXml(localizedDataFile);
                        m.ReleaseMutex();
                        
                        _OldSupportFiles.Merge(dsLocalizedSupportFileData);
                    }
                }
            }
            else
            {
                _OldSupportFiles = new Client.wsIpgSlideUpdater.SupportFileDS();
                //this is for backwards compatibility. the orignal updating scheme did not include a SupportFileData.xml file.
                string mutexName = AppSettings.LocalFiles.Default.SupportFilesData;
                Mutex m = new Mutex(false, mutexName);
                m.WaitOne();               
                if (!File.Exists(Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, AppSettings.LocalFiles.Default.SupportFilesData)))
                {
                    _OldSupportFiles.WriteXml(Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, AppSettings.LocalFiles.Default.SupportFilesData));
                }
                _OldSupportFiles.ReadXml(Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, AppSettings.LocalFiles.Default.SupportFilesData));
                m.ReleaseMutex();
            }



        }

        private void UpdateLocalizedContentVersionSetting()
        {
            try
            {
                if (!_CurrentLocalizedUpdateID.HasValue)
                {
                    wsIpgSlideUpdater.SlideUpdater wsSlideUpdater = new Client.wsIpgSlideUpdater.SlideUpdater();
                    _CurrentLocalizedUpdateID = wsSlideUpdater.GetMostRecentLocalizedContentUpdateId();
                }
                Properties.Settings.Default.LastLocalizedContentID = _CurrentLocalizedUpdateID.Value;
                Properties.Settings.Default.Save();

            }
            catch (Exception ex)
            {
                File.WriteAllText(Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, "LocalizedContentUpdatedLog.txt"), ex.Message + System.Environment.NewLine + System.Environment.NewLine +
                                                             ex.StackTrace + System.Environment.NewLine + System.Environment.NewLine +
                                                             ex.InnerException.ToString());
            }
        }
        #endregion

        #region Localized Regular BG events

        private void _BackgroundWorkerRegularLocalized_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            _IsWorkInProgress = false;
            if (e.Error != null)
            {
                //TODO: handle this
                MessageBox.Show("An error occured while updating content.  This may be solvable by the PSG administrators.  The specific error found was: " + e.Error.Message);
                Properties.Settings.Default.IsUpdatePaused = false;
                Properties.Settings.Default.Save();
                ContentUpdateCompleted();
            }
            else if (e.Cancelled)
            {
                // Fire the content paused event
                Properties.Settings.Default.IsUpdatePaused = true;
                Properties.Settings.Default.Save();
                ContentUpdatePaused();
            }
            else
            {
                Properties.Settings.Default.IsUpdatePaused = false;
                Properties.Settings.Default.Save();
                ContentUpdateProgressReported("Content update complete.", 100);
                ContentUpdateCompleted();
                int totalContentUpdateDuration = Convert.ToInt32(Properties.Settings.Default.ContentUpdateDuration +
                    DateTime.UtcNow.Subtract(Properties.Settings.Default.ContentUpdateSessionStartDate).TotalSeconds);
                _userRegService.RecordUserContentUpdateDurationAsync(Properties.Settings.Default.UserRegistrationEmail, totalContentUpdateDuration);
                Properties.Settings.Default.ContentUpdateDuration = 0;
                Properties.Settings.Default.Save();
            }
        }

        private void _BackgroundWorkerRegularLocalized_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            if (e.UserState.ToString() == "CanPause")
            {
                CanPauseProcess();
            }
            else
            {
                ContentUpdateProgressReported((string)e.UserState, e.ProgressPercentage);
            }
        }

        private void _BackgroundWorkerRegularLocalized_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            _BackgroundWorkerRegularLocalized.ReportProgress(0, "Preparing to download content...");

            LoadMyRegionLanguages();

            ReportCurrentRegionLanguagesToServer();

            LoadNewLocalizedData();

            LoadOldLocalizedData();

            if (IsFirstRun)
                LoadAllFilesForFirstRun();
            else
                DetermineLocalizedFilesToDownload();

            DetermineLocalizedFilesToDelete();

            DeleteOldFiles();

            // Fire CanPauseProcess event
            _BackgroundWorkerRegularLocalized.ReportProgress(0, "CanPause");

            DownloadFiles(_BackgroundWorkerRegularLocalized);

            UpdateLocalizedContentVersionSetting();

            if (_BackgroundWorkerRegularLocalized.CancellationPending)
            {
                e.Cancel = true;
                return;
            }

            WriteNewLocalizedDataFiles();

            WriteNewLocalizedSupportFiles();
        }

        #endregion

        #region Localized Full BG events

        /// <summary>
        /// Handles the RunWorkerCompleted event of the _BackgroundWorkerFullLocalized control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.RunWorkerCompletedEventArgs"/> instance containing the event data.</param>
        private void _BackgroundWorkerFullLocalized_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            _IsWorkInProgress = false;
            ContentUpdateProgressReported("Content update complete.", 100);
            ContentUpdateCompleted();

            if (Properties.Settings.Default.ContentUpdateType == UpdateType.LocalizedNew.ToString())
            {
                int totalContentUpdateDuration = Convert.ToInt32(DateTime.UtcNow.Subtract(Properties.Settings.Default.ContentUpdateSessionStartDate).TotalSeconds);
                _userRegService.RecordUserContentInstallDurationAsync(Properties.Settings.Default.UserRegistrationEmail, totalContentUpdateDuration);
            }
            else
            {
                int totalContentUpdateDuration = Convert.ToInt32(Properties.Settings.Default.ContentUpdateDuration +
                    DateTime.UtcNow.Subtract(Properties.Settings.Default.ContentUpdateSessionStartDate).TotalSeconds);

                _userRegService.RecordUserContentUpdateDurationAsync(Properties.Settings.Default.UserRegistrationEmail, totalContentUpdateDuration);
                Properties.Settings.Default.ContentUpdateDuration = 0;
                Properties.Settings.Default.Save();
            }
        }

        /// <summary>
        /// Handles the ProgressChanged event of the _BackgroundWorkerFullLocalized control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.ProgressChangedEventArgs"/> instance containing the event data.</param>
        private void _BackgroundWorkerFullLocalized_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            ContentUpdateProgressReported((string)e.UserState, e.ProgressPercentage);
        }

        /// <summary>
        /// Handles the DoWork event of the _BackgroundWorkerFullLocalized control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.DoWorkEventArgs"/> instance containing the event data.</param>
        private void _BackgroundWorkerFullLocalized_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            _BackgroundWorkerFullLocalized.ReportProgress(0, "Preparing to download content...");

            LoadMyRegionLanguages();

            ReportCurrentRegionLanguagesToServer();

            LoadNewLocalizedData();

            //get all the slides to download
            _FilesToDownload = new List<string>();
            foreach (wsIpgSlideUpdater.SlideLibrary.SlideCategoryRow slideCat in _NewMergedData.SlideCategory.Rows)
            {
                //try
                //{
                if (!_FilesToDownload.Contains(slideCat.SlideRow.FileName))
                {
                    _FilesToDownload.Add(slideCat.SlideRow.FileName);
                }
                //}
                //catch (Exception ex)
                //{
                //    //do nothing
                //}
            }

            //get all support files to download
            foreach (wsIpgSlideUpdater.SupportFileDS.SupportFileRow supportFile in _NewSupportFiles.SupportFile.Rows)
            {
                if (!_FilesToDownload.Contains(supportFile.FileName))
                {
                    _FilesToDownload.Add(supportFile.FileName);
                }
            }

            DownloadFilesInitial();

            WriteNewLocalizedDataFiles();

            WriteNewLocalizedSupportFiles();

            UpdateLocalizedContentVersionSetting();

        }
        #endregion

        private void WriteNewLocalizedDataFiles()
        {
            //write the updated data to disk
            Mutex mainMutex = new Mutex(false, "MainData");
            mainMutex.WaitOne();
            _NewMainData.WriteXml(Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, AppSettings.LocalFiles.Default.MainData));
            mainMutex.ReleaseMutex();

            foreach (KeyValuePair<int, wsIpgSlideUpdater.SlideLibrary> localizedSlideData in _NewLocalizedSlideData)
            {
                string mutexName = String.Format("{0}{1}", localizedSlideData.Key.ToString(), AppSettings.LocalFiles.Default.SlideDataFilePart);
                Mutex m = new Mutex(false, mutexName);
                m.WaitOne();
                string localizedSlideDataFile = Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, String.Format("{0}{1}", localizedSlideData.Key.ToString(), AppSettings.LocalFiles.Default.SlideDataFilePart));
                localizedSlideData.Value.WriteXml(localizedSlideDataFile);
                m.ReleaseMutex();
            }
            
        }

        private void WriteNewLocalizedSupportFiles()
        {
            foreach (KeyValuePair<int, wsIpgSlideUpdater.SupportFileDS> localizedSupportData in _NewLocalizedSupportFileData)
            {
                string mutexName = String.Format("{0}{1}", localizedSupportData.Key.ToString(), AppSettings.LocalFiles.Default.SupportFileDataFilePart);
                Mutex m = new Mutex(false, mutexName);
                m.WaitOne();
                localizedSupportData.Value.WriteXml(Path.Combine(AppSettings.LocalPaths.Default.XmlDirectory, String.Format("{0}{1}", localizedSupportData.Key.ToString(), AppSettings.LocalFiles.Default.SupportFileDataFilePart)));
                m.ReleaseMutex();
                
            }
        }
        

        #region Localized Partial BG events

        void _BackgroundWorkerPartialLocalized_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            _IsWorkInProgress = false;
            if (e.Error != null)
            {
                //TODO: handle this
                MessageBox.Show("An error occured while updating content.  This may be solvable by the PSG administrators.  The specific error found was: " + e.Error.Message, "Error");
                Properties.Settings.Default.IsUpdatePaused = false;
                Properties.Settings.Default.Save();
                ContentUpdateCompleted();
            }
            else if (e.Cancelled)
            {
                // Fire the content paused event
                Properties.Settings.Default.IsUpdatePaused = true;
                Properties.Settings.Default.Save();
                ContentUpdatePaused();
            }
            else
            {
                Properties.Settings.Default.IsUpdatePaused = false;
                Properties.Settings.Default.Save();
                ContentUpdateProgressReported("Content update complete.", 100);
                ContentUpdateCompleted();
                int totalContentUpdateDuration = Convert.ToInt32(Properties.Settings.Default.ContentUpdateDuration +
                    DateTime.UtcNow.Subtract(Properties.Settings.Default.ContentUpdateSessionStartDate).TotalSeconds);
                _userRegService.RecordUserContentUpdateDurationAsync(Properties.Settings.Default.UserRegistrationEmail, totalContentUpdateDuration);
                Properties.Settings.Default.ContentUpdateDuration = 0;
                Properties.Settings.Default.Save();
            }
        }

        void _BackgroundWorkerPartialLocalized_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            if (e.UserState.ToString() == "CanPause")
            {
                CanPauseProcess();
            }
            else
            {
                ContentUpdateProgressReported((string)e.UserState, e.ProgressPercentage);
            }
        }

        void _BackgroundWorkerPartialLocalized_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            // Fire CanPauseProcess event
            _BackgroundWorkerPartialLocalized.ReportProgress(0, "CanPause");

            DownloadFiles(_BackgroundWorkerPartialLocalized);

            UpdateLocalizedContentVersionSetting();

            if (_BackgroundWorkerPartialLocalized.CancellationPending)
            {
                e.Cancel = true;
                return;
            }

            LoadMyRegionLanguages();

            ReportCurrentRegionLanguagesToServer();

            LoadNewLocalizedData();

            WriteNewLocalizedDataFiles();

            WriteNewLocalizedSupportFiles();
        }

        #endregion

        #region Webservice Async Completed

        /// <summary>
        /// Handles the IsLocalizedContentAvailableCompleted event of the wsSlideUpdater control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Client.wsIpgSlideUpdater.IsLocalizedContentAvailableCompletedEventArgs"/> instance containing the event data.</param>
        private void wsSlideUpdater_IsLocalizedContentAvailableCompleted(object sender, Client.wsIpgSlideUpdater.IsLocalizedContentAvailableCompletedEventArgs e)
        {
            _IsWorkInProgress = false;
            ContentUpdateCheckResult result;

            if (e.Error != null)
            {
                result = ContentUpdateCheckResult.Error;
            }
            else if (e.Cancelled)
            {
                result = ContentUpdateCheckResult.Cancelled;
            }
            else
            {
                if (e.Result)
                {
                    result = ContentUpdateCheckResult.UpdateAvailable;
                    wsIpgSlideUpdater.SlideUpdater wsSlideUpdater = new Client.wsIpgSlideUpdater.SlideUpdater();
                    //since we'll probably going to get an update, go get this value.
                    //we'll need it when we are done with the update
                    //getting it now is better than waiting for the content update to complete because the ws connection may timeout
                    _CurrentLocalizedUpdateID = wsSlideUpdater.GetMostRecentLocalizedContentUpdateId();
                }
                else
                {
                    if (Properties.Settings.Default.IsUpdatePaused)
                    {
                        // The content update was paused, resume the update
                        result = ContentUpdateCheckResult.ResumeUpdate;
                    }
                    else
                    {
                        result = ContentUpdateCheckResult.ContentIsCurrent;
                    }
                }
            }
            CheckForUpdateCompleted(result, _IsSilentCheck);
        }

        private void HandleCorruptedFile(string file)
        {
            MessageBox.Show("The system encountered a corrupted file.  Please restart the application to refresh the data.  The application will now close.");
            try
            {
                File.Delete(file);
            }
            catch
            {
                //Fail Silently
            }
            Application.Exit();
        }

        #endregion

        #region events and event delegates

        public delegate void CheckForUpdateCompletedDelegate(ContentUpdateCheckResult result, bool isSilentlyChecked);

        public event CheckForUpdateCompletedDelegate CheckForUpdateCompleted;

        public delegate void ContentUpdateCompletedDelegate();

        public event ContentUpdateCompletedDelegate ContentUpdateCompleted;

        public delegate void ContentUpdateProgressReportedDelegate(string message, int progressPercent);

        public event ContentUpdateProgressReportedDelegate ContentUpdateProgressReported;

        public delegate void CanPauseProcessDelegate();

        public event CanPauseProcessDelegate CanPauseProcess;

        public delegate void ContentUpdatePausedDelegate();

        public event ContentUpdatePausedDelegate ContentUpdatePaused;

        public delegate void UpdateToBeIndexedDelegate(string f);
        public event UpdateToBeIndexedDelegate UpdateToBeIndexed;

        public delegate void UpdateRemoveIndexDelegate(string f);
        public event UpdateRemoveIndexDelegate UpdateRemoveIndex;

        #endregion

        #region enumerations

        /// <summary>
        /// Result of checking for content update
        /// </summary>
        public enum ContentUpdateCheckResult
        {
            UpdateAvailable,
            ContentIsCurrent,
            ResumeUpdate,
            Error,
            Cancelled
        }

        /// <summary>
        /// The type of update
        /// </summary>
        public enum UpdateType
        {
            LocalizedNew,
            LocalizedRegular,
            LocalizedFull,
            LocalizedPartial
        }

        private enum BgTypeRunning
        {
            LocalizedRegular,
            LocalizedFull,
            LocalizedMissing,
            LocalizedPartial
        }

        #endregion
    }
}
