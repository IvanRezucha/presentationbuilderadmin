using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Client
{
    public partial class SaveDialog : Form
    {
        int presentationID;
        string presentationName;
        public SaveDialog(int presID, string presName)
        {
            InitializeComponent();
            presentationID = presID;
            if (presentationID == -1)
            {
                txtPresentationName.Text = "Untitled";
            }
            else 
            {
                txtPresentationName.Text = presName;
            }
            presentationName = txtPresentationName.Text;
            txtPresentationName.SelectAll();
        }

        private void SaveDialog_Load(object sender, EventArgs e)
        {

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (presentationName == txtPresentationName.Text && presentationName != "Untitled")
            {
                //save existing
                ((Client.IpgMain)this.Owner).SavePptXml(presentationID, presentationName);
            }
            else
            {
                //save new
                ((Client.IpgMain)this.Owner).SavePptXml(-1, txtPresentationName.Text);
            }
                this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}