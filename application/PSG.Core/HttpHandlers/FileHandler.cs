﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Configuration;
using PSG.Core.Files;
using System.IO;

namespace PSG.Core.HttpHandlers
{
    public class FileHandler : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            string fileName = string.Empty;
            string rawUrl = context.Request.RawUrl;

            if (rawUrl.Contains("ResourceFiles"))
            {
                fileName = GetFileName(rawUrl);
                string filePath = GetFilePath(fileName);

                if (!File.Exists(filePath))
                    context.Response.Redirect("~/Web/FileNotFound.htm");
                else
                {
                    context.Response.ContentType = GetContentType(fileName);
                    context.Response.WriteFile(filePath);
                }

                context.Response.End();
            }
            else
            {
                context.Response.Clear();
                context.Response.ContentType = GetContentType(context.Request.PhysicalPath); 
                context.Response.WriteFile(context.Request.PhysicalPath);
                context.Response.End();
            }
        }

        public string GetFilePath(string fileName)
        {
            string resourceUrl = WebConfigurationManager.AppSettings["ResourceFilesDirectory"].ToString();
            FileLocation location = new FileLocation(fileName);
            string filePath = Path.Combine(resourceUrl, location.ShortPath);
            return filePath;
        }

        private string GetFileName(string rawUrl)
        {
            string[] splitter = { @"/" };
            string[] urlParts = rawUrl.Split(splitter, StringSplitOptions.None);
            string fileName = urlParts[urlParts.Length - 1];
            return fileName;
        }

        string GetContentType(String path)
        {
            switch (Path.GetExtension(path))
            {
                case ".jpg": return "Image/jpeg";
                case ".pdf": return "application/pdf";
                case ".rtf": return "application/rtf";
                case ".pot": return "application/vnd.ms-powerpoint";
                case ".ppt": return "application/vnd.ms-powerpoint";
                default: break;
            }
            return "";
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
