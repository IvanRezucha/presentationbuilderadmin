﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace PSG.Core.Files
{
    public class SHAHashLocationAlgorithm : ILocationAlgorithm
    {
        private const int RANGE = 600;
        public SHAHashLocationAlgorithm()
        {
        }
        public string GetDirectory(string fileName)
        {
            int lowerValue = 0;
            int upperValue = RANGE - 1;

            int value = GetSHAHash(fileName);
            string valStr = value.ToString();
            double sum = 0.0;
            int Aj;
            double Tj;
            double Pj;
            for (int j = 0; j < valStr.Length; j++)
            {
                Aj = (int)valStr[j];
                Tj = (Math.PI * (1 + j % 5) / 2);
                Pj = Math.Pow(Aj, Tj);
                sum += Math.Round((Math.Ceiling(Pj) - Pj) * RANGE) * Tj;
            }
            int result = ((int)(sum % RANGE)) + lowerValue;
            string resultString = result.ToString() + @"\";
            return resultString;
        }
        public int GetSHAHash(string fileName)
        {
            SHA1CryptoServiceProvider hash = new SHA1CryptoServiceProvider();
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(fileName);
            byte[] hashBytes = hash.ComputeHash(plainTextBytes);
            return BitConverter.ToInt32(hashBytes, 0);
        }
    }
}
