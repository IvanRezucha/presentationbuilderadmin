﻿using System.Text;

namespace PSG.Core.Files
{
    public class HashCodeLocationAlgorithm : ILocationAlgorithm
    {
        public HashCodeLocationAlgorithm()
        {
        }
        public string GetDirectory(string fileName)
        {
            StringBuilder sb = new StringBuilder();
            int value = GetHashCodeValue(fileName);
            string valStr = value.ToString();
            foreach (char c in valStr)
            {
                sb.Append(c.ToString());
                sb.Append(@"\");
            }
            return sb.ToString();
            
        }
        private int GetHashCodeValue(string fileName)
        {
            return fileName.GetHashCode();
        }
    }
}
