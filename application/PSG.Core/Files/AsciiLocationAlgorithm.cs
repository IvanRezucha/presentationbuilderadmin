﻿using System;
using System.Text;

namespace PSG.Core.Files
{
    public class AsciiLocationAlgorithm : ILocationAlgorithm
    {
        public AsciiLocationAlgorithm()
        {
        }
        public string GetDirectory(string fileName)
        {
            StringBuilder sb = new StringBuilder();
            int value = GetAsciiValue(fileName);
            string valStr = value.ToString();
            foreach (char c in valStr)
            {
                sb.Append(c.ToString());
                sb.Append(@"\");
            }
            return sb.ToString();
        }

        //option1
        private int GetAsciiValue(string fileName)
        {
            int value = 0;
            foreach (char c in fileName)
                value += Convert.ToInt32(c);
          
            return value;
        }
    }
}
