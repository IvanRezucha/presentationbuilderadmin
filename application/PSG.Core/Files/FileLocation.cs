﻿using System.Diagnostics;
using System.Text;
using System.IO;


namespace PSG.Core.Files
{
    public class FileLocation
    {
        private string fileName = string.Empty;
        //private string filePath = string.Empty;
        private string directory = string.Empty;
        private string shortPath = string.Empty;
        private ILocationAlgorithm locationAlgorithm;
        //public static string DIRECTORY_ROOT = string.Empty;

        public FileLocation(string fileName)
        {
            locationAlgorithm = new SHAHashLocationAlgorithm();
            this.fileName = fileName;
            this.SetFilePath();
        }

        public string FileName
        {
            get { return fileName; }
        }
        
        /*
        public string FilePath
        {
            get { return filePath; }
        }
        */
        public string Directory
        {
            get { return directory; }
        }
        public string ShortPath
        {
            get { return shortPath; }
        }

        public void SetFilePath()
        {
            string directoryPart = locationAlgorithm.GetDirectory(fileName);
            this.shortPath = Path.Combine(directoryPart, fileName);
            ///this.directory = Path.Combine(DIRECTORY_ROOT, directoryPart);
            this.directory = directoryPart;
            //this.filePath = Path.Combine(this.directory, fileName);
        }
    }
}
