﻿using System;
using System.IO;

namespace PSG.Core.Files
{
    public class FileAccessor
    {
        private string directory = string.Empty;

        public FileAccessor(string directory)
        {
            this.directory = directory;
        }

        public byte[] GetFile(FileLocation location)
        {
            try
            {
                string path = Path.Combine(this.directory, location.ShortPath);
                return ReadFile(path);
            }
            catch (Exception ex)
            {
                byte[] byteArray = new byte[1];
                byteArray[0] = Convert.ToByte(false);
                return byteArray;
            }
        }

        public byte[] GetFile(string fileName)
        {
            FileLocation location = new FileLocation(fileName);
            return GetFile(location);
        }

        public bool SaveFile(byte[] buffer, FileLocation fileLocation)
        {
            try
            {
                CheckDirectory(fileLocation);
                string path = Path.Combine(this.directory, fileLocation.ShortPath);
                WriteFile(buffer, path);
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public bool SaveFile(byte[] buffer, string fileName)
        {
            FileLocation fileLocation = new FileLocation(fileName);
            return SaveFile(buffer, fileLocation);
        }

        public void CheckDirectory(FileLocation fileLocation)
        {
            string directory = Path.Combine(this.directory, fileLocation.Directory);
            DirectoryInfo di = new DirectoryInfo(directory);
            if (!di.Exists)
                di.Create();
        }

        public static bool WriteFile(byte[] buffer, string path)
        {
            try
            {
				File.WriteAllBytes(path, buffer);
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public static byte[] ReadFile(string path)
        {
			return File.ReadAllBytes(path);
        }

        public static bool DeleteFile(string path)
        {
            try
            {
                File.Delete(path);
            }
            catch (Exception ex)
            {
                return false;
            }

            FileInfo file = new FileInfo(path);

            if (!file.Exists)
                return true;
            else
                return false;
        }

        public static FileInfo[] GetFileList(string path)
        {
            DirectoryInfo directoryInfo = new DirectoryInfo(path);
            return directoryInfo.GetFiles();
        } 
    }
}
