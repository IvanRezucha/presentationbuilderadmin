﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;

namespace PSG.Core.Notifiers
{
    public class EmailNotifier
    {
        private string to;
        private string from;
        private string subject;
        private string body;

        public EmailNotifier(string to, string from, string subject, string body)
        {
            this.to = to;
            this.from = from;
            this.subject = subject;
            this.body = body;
        }

        public void Send()
        {
            SmtpClient smtpClient = new SmtpClient();
            MailMessage mailMessage = new MailMessage(from, to);
            mailMessage.Subject = subject;
            mailMessage.Body = body;
            smtpClient.Send(mailMessage);
        }
        public void SendHTML()
        {
            SmtpClient smtpClient = new SmtpClient();
            MailMessage mailMessage = new MailMessage(from, to);
            mailMessage.Subject = subject;
            mailMessage.Body = body;
            mailMessage.IsBodyHtml = true;
            smtpClient.Send(mailMessage);
        }
    }
}
