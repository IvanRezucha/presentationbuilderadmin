<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Main.aspx.cs" Inherits="Main" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="RadComboBox.Net2" Namespace="Telerik.WebControls" TagPrefix="radC" %>
<%@ Register Assembly="RadTreeView.Net2" Namespace="Telerik.WebControls" TagPrefix="radT" %>
<%@ Register Assembly="RadSplitter.Net2" Namespace="Telerik.WebControls" TagPrefix="radspl" %>
<%@ Register Assembly="RadAjax.Net2" Namespace="Telerik.WebControls" TagPrefix="radA" %>
<%@ Import Namespace="System.Web.Configuration" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>HP Printing Sales Guide</title>
    <link href="Main.css" rel="stylesheet" type="text/css" />

    <script language="JavaScript">
    <!--
        var levels = ["homepage"];
        var slideid = "";
        var currentCategory;
    // -->
    </script>

    <script language="javascript" src="js/WireStoneDetectionKit.js"></script>

    <script language="javascript" src="js/CookieManager.js"></script>

    <script language="javascript" src="js/RegionSettingManager.js"></script>

    <script language="JavaScript">
        window.moveTo(0, 0);
        window.resizeTo(screen.width, screen.height);

    </script>

    <script language="JavaScript">
        function formAbandon() {
            if (typeof formName != "undefined") {
                if (!strSubmit) ntptEventTag('ev=form abandoned&ed=' + formName.toLowerCase() + '&last_field=' + escape(lastField.toLowerCase()));
            }
        }
    </script>
    
    <script language="javascript">
        function reRunAnalytics() {
            var lastField = "nothing";
            var strSubmit = false;

            var NTPT_PGEXTRA = '';
            var i = 0;
            var imax = 0;

            if (typeof levels != "undefined" && slideid == "") {
                imax = levels.length - 1;
                for (i = 0; i <= imax; i++) {
                    var j = i + 2;
                    NTPT_PGEXTRA = NTPT_PGEXTRA + 'level' + j + '=' + levels[i].toLowerCase();
                    if (i != imax) {
                        NTPT_PGEXTRA = NTPT_PGEXTRA + '&';
                    }
                }
                NTPT_PGEXTRA = NTPT_PGEXTRA + '&page=' + levels.join("/").toLowerCase();
            }
            else if (slideid != "") {
                NTPT_PGEXTRA = NTPT_PGEXTRA + 'slideid=' + slideid;
            }
            else {
                NTPT_PGEXTRA = 'page=error with levels';
            }
            /* Unica Page Tagging Script v7.4.0
            * Copyright 2004-2006 Unica Corporation.  All rights reserved.
            * Visit http://www.unica.com for more information.
            */

            var NTPT_IMGSRC = '/WebApp/images/ntpagetag.gif';     /******** MODIFY with correct location of ntpagetag.gif ***************/

            function getCookie(name) {
                var start = document.cookie.indexOf(name + '=');
                var len = start + name.length + 1;
                if ((!start) && (name != document.cookie.substring(0, name.length))) return null;
                if (start == -1) return null;
                var end = document.cookie.indexOf(";", len);
                if (end == -1) end = document.cookie.length;
                return unescape(document.cookie.substring(len, end));
            }

            function writePersistentCookie(CookieName, CookieValue) {
                var expireDate = new Date();
                expireDate.setDate(expireDate.getDate() + 1826);  // 5 years from now
                document.cookie = escape(CookieName) + "=" + escape(CookieValue) + "; expires=" + expireDate.toGMTString() + "; path=/";
            }

            function writeSessionCookie(CookieName, CookieValue) {
                document.cookie = escape(CookieName) + "=" + escape(CookieValue) + ";  path=/";
            }


            function getQueryString(queryString) {
                var queryObject = new Object();
                queryString = queryString.replace(/^.*\?(.+)$/, '$1');

                while ((pair = queryString.match(/([^=]+)=\'?([^\&\']*)\'?\&?/)) && pair[0].length) {
                    queryString = queryString.substring(pair[0].length);
                    if (/^\-?\d+$/.test(pair[2])) pair[2] = parseInt(pair[2]);
                    queryObject[pair[1]] = pair[2];
                }
                return queryObject;
            }

            /* Function to generate unique ID  */
            Math.uuid = (function () {
                // Private array of chars to use
                var CHARS = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.split('');

                return function (len, radix) {
                    var chars = CHARS, uuid = [], rnd = Math.random;
                    radix = radix || chars.length;

                    if (len) {
                        // Compact form
                        for (var i = 0; i < len; i++) uuid[i] = chars[0 | rnd() * radix];
                    } else {
                        // rfc4122, version 4 form
                        var r;

                        // rfc4122 requires these characters
                        uuid[8] = uuid[13] = uuid[18] = uuid[23] = '-';
                        uuid[14] = '4';

                        // Fill in random data.  At i==19 set the high bits of clock sequence as
                        // per rfc4122, sec. 4.1.5
                        for (var i = 0; i < 36; i++) {
                            if (!uuid[i]) {
                                r = 0 | rnd() * 16;
                                uuid[i] = chars[(i == 19) ? (r & 0x3) | 0x8 : r & 0xf];
                            }
                        }
                    }

                    return uuid.join('');
                };
            })();

            // Check and set the Session ID
            if (!getCookie('sessionid')) {
                document.cookie = "sessionid=" + Math.uuid(25) + "; path=/";
            }


            var queryObj = getQueryString(location.search);
            var strcp = queryObj.cp;
            var strcp_ch = queryObj.cp_ch;
            var strcp_sg = queryObj.cp_sg;
            var strcp_sr = queryObj.cp_sr;
            var strcp_cr = queryObj.cp_cr;
            var strcp_term = queryObj.cp_term;
            var strbroad = queryObj.broad;
            var strgpkw = queryObj.gpkw;
            var strmpkw = queryObj.mpkw;
            var strypkw = queryObj.ypkw;
            var flag = 0;

            // test to see if this is the first campaign and then put campaign values in persistent cookies to be tracked as first campaign
            if (!getCookie('first_flag')) {
                if (strcp != null) {
                    writePersistentCookie('first_cp', escape(strcp));
                    flag = 1
                }
                if (strcp_ch != null) {
                    writePersistentCookie('first_cp_ch', escape(strcp_ch));
                    flag = 1
                }
                if (strcp_sg != null) {
                    writePersistentCookie('first_cp_sg', escape(strcp_sg));
                    flag = 1
                }
                if (strcp_sr != null) {
                    writePersistentCookie('first_cp_sr', escape(strcp_sr));
                    flag = 1
                }
                if (strcp_cr != null) {
                    writePersistentCookie('first_cp_cr', escape(strcp_cr));
                    flag = 1
                }
                if (strcp_term != null) {
                    writePersistentCookie('first_cp_term', escape(strcp_term));
                    flag = 1
                }
                if (strbroad != null) {
                    writePersistentCookie('first_broad', escape(strbroad));
                    flag = 1
                }
                if (strgpkw != null) {
                    writePersistentCookie('first_gpkw', escape(strgpkw));
                    flag = 1
                }
                if (strmpkw != null) {
                    writePersistentCookie('first_mpkw', escape(strmpkw));
                    flag = 1
                }
                if (strypkw != null) {
                    writePersistentCookie('first_ypkw', escape(strypkw));
                    flag = 1
                }
                if (flag == 1) {
                    writePersistentCookie('first_flag', '1');
                }
            }


            // if the campaign values exist in the query string put them in session cookies to be tracked as last campaign
            if (strcp != null) {
                writeSessionCookie('last_cp', escape(strcp))
            }
            if (strcp_ch != null) {
                writeSessionCookie('last_cp_ch', escape(strcp_ch))
            }
            if (strcp_sg != null) {
                writeSessionCookie('last_cp_sg', escape(strcp_sg))
            }
            if (strcp_sr != null) {
                writeSessionCookie('last_cp_sr', escape(strcp_sr))
            }
            if (strcp_cr != null) {
                writeSessionCookie('last_cp_cr', escape(strcp_cr))
            }
            if (strcp_term != null) {
                writeSessionCookie('last_cp_term', escape(strcp_term))
            }

            // if broad exists in the query string put it in a session cookie to be tracked
            if (strbroad != null) {
                writeSessionCookie('last_broad', escape(strbroad))
            }

            // if keyword exists in the query string put it in a session cookie to be tracked
            if (strgpkw != null) {
                writeSessionCookie('last_gpkw', escape(strgpkw))
            }
            if (strmpkw != null) {
                writeSessionCookie('last_mpkw', escape(strmpkw))
            }
            if (strypkw != null) {
                writeSessionCookie('last_ypkw', escape(strypkw))
            }


            // if segment has been identified run this function to put in a cookie to be tracked
            function setSegment(cookiename, value) {
                var exdate = new Date();
                exdate.setDate(exdate.getDate() + 1826);
                document.cookie = cookiename + "=" + escape(value) + ";expires=" + exdate.toGMTString() + "; path=/";
            }

            function setLoginSegment() {
                document.cookie = "login=login; path=/";
            }

            function setLogoutSegment() {
                document.cookie = "login=logout; path=/";
            }

            var NTPT_FLDS = new Object();
            NTPT_FLDS.lc = true; // Document location
            NTPT_FLDS.rf = true; // Document referrer
            NTPT_FLDS.rs = true; // User's screen resolution
            NTPT_FLDS.cd = true; // User's color depth
            NTPT_FLDS.ln = true; // Browser language
            NTPT_FLDS.tz = true; // User's timezone
            NTPT_FLDS.jv = true; // Browser's Java support
            NTPT_FLDS.ck = true; // Cookies

            var NTPT_SET_IDCOOKIE = true;
            var NTPT_IDCOOKIE_NAME = "visitorid";

            // Use the line below to track visits/visitors across subdomains
            // var NTPT_IDCOOKIE_DOMAIN = ".compsych.com";

            var NTPT_MAXTAGWAIT = 1.0; // Max delay (secs) on link-tags and submit-tags

            // Optional variables:
            var NTPT_HTTPSIMGSRC = '';
            var NTPT_GLBLEXTRA = 'level1=hppsg';
            var NTPT_GLBLREFTOP = false;
            // track the cookies in a ck value
            var NTPT_GLBLCOOKIES = ["visitorid", "sessionid", "first_cp", "first_cp_ch", "first_cp_sg", "first_cp_sr", "first_cp_cr", "first_cp_term", "last_cp", "last_cp_ch", "last_cp_sg", "last_cp_sr", "last_cp_cr", "last_cp_term", "first_broad", "last_broad", "first_gpkw", "first_mpkw", "first_ypkw", "last_gpkw", "last_mpkw", "last_ypkw", "registered", "login", "region", "language", "role", "country", "browsemode", "filters"];


            /*** END OF USER-CONFIGURABLE VARIABLES ***/

            function OOOO000(OO0O00, O0O0O, O000OOO, OO0O00O) { var O00O0 = ""; O00O0 = OO0O00 + "\x3d" + escape(O0O0O) + "\x3b"; if (OO0O00O) O00O0 += "\x20\x64\x6f\x6d\x61\x69\x6e\x3d" + OO0O00O + "\x3b"; if (O000OOO > (0x1d65 + 435 - 0x1f18)) { var OOO00O = new Date(); OOO00O.setTime(OOO00O.getTime() + (O000OOO * (0x9a6 + 2102 - 0xdf4))); O00O0 += "\x20\x65\x78\x70\x69\x72\x65\x73\x3d" + OOO00O.toGMTString() + "\x3b"; } O00O0 += "\x20\x70\x61\x74\x68\x3d\x2f"; document.cookie = O00O0; }; function OOOO00(OO0O00) { var O0O0O0O = OO0O00 + "\x3d"; if (document.cookie.length > (0x162f + 0 - 0x162f)) { var OO0000; OO0000 = document.cookie.indexOf(O0O0O0O); if (OO0000 != -(0x106 + 5772 - 0x1791)) { var OOO000; OO0000 += O0O0O0O.length; OOO000 = document.cookie.indexOf("\x3b", OO0000); if (OOO000 == -(0x129c + 4910 - 0x25c9)) OOO000 = document.cookie.length; return unescape(document.cookie.substring(OO0000, OOO000)); } else { return null; }; } }; function O00000O(O0OO0) { var OO000O = ""; for (OO00O in O0OO0) { if ((typeof (O0OO0[OO00O]) == "\x73\x74\x72\x69\x6e\x67") && (O0OO0[OO00O] != "")) { if (OO000O != "") OO000O += "\x3b"; OO000O += OO00O + "\x3d" + O0OO0[OO00O]; }; } return OO000O; }; var O00OOO = ["\x41", "\x42", "\x43", "\x44", "\x45", "\x46", "\x47", "\x48", "\x49", "\x4a", "\x4b", "\x4c", "\x4d", "\x4e", "\x4f", "\x50", "\x51", "\x52", "\x53", "\x54", "\x55", "\x56", "\x57", "\x58", "\x59", "\x5a", "\x61", "\x62", "\x63", "\x64", "\x65", "\x66", "\x67", "\x68", "\x69", "\x6a", "\x6b", "\x6c", "\x6d", "\x6e", "\x6f", "\x70", "\x71", "\x72", "\x73", "\x74", "\x75", "\x76", "\x77", "\x78", "\x79", "\x7a", "\x30", "\x31", "\x32", "\x33", "\x34", "\x35", "\x36", "\x37", "\x38", "\x39"]; function OOOOOO0(O00000) { if (O00000 < (0x41 + 9084 - 0x237f)) { return O00OOO[O00000]; } else { return (OOOOOO0(Math.floor(O00000 / (0x1163 + 644 - 0x13a9))) + O00OOO[O00000 % (0x1c5c + 1570 - 0x2240)]); } }; function O0O000O() { var OO0OO0O = ""; var OOOOO00 = new Date(); for (OOO0O0O = (0x13b0 + 769 - 0x16b1); OOO0O0O < (0x26f + 3070 - 0xe62); OOO0O0O++) { OO0OO0O += O00OOO[Math.round(Math.random() * (0xb62 + 1003 - 0xf10))]; } return (OO0OO0O + "\x2d" + OOOOOO0(OOOOO00.getTime())); }; function OO0OO(O0O0000, OOO0O00) { return (eval("\x74\x79\x70\x65\x6f\x66\x20" + O0O0000 + "\x20\x21\x3d\x20\x22\x75\x6e\x64\x65\x66\x69\x6e\x65\x64\x22") ? eval(O0O0000) : OOO0O00); }; function OO0O000(O00OOO0, O0O000) { return (O00OOO0 + (((O00OOO0 == '') || ((O0O000 == '') || (O0O000.substring((0x1dc9 + 2039 - 0x25c0), (0x1442 + 4474 - 0x25bb)) == "\x26"))) ? '' : "\x26") + O0O000); }; function O000O00() { var O0O00O = new Date(); return (O0O00O.getTime() + "\x2e" + Math.floor(Math.random() * (0xed9 + 1573 - 0x1116))); }; function O00OO(OO0O00, OO0OO00) { OOO00[OO0O00] = OO0OO00.toString(); }; function O0OO0O0(OO0O00) { OOO00[OO0O00] = ''; }; function OOO0000(O000O) { var O0OO0O = '', OO00O, O0O0O; OO00OO(OO0OO("\x4e\x54\x50\x54\x5f\x47\x4c\x42\x4c\x45\x58\x54\x52\x41", '')); if (!LnkLck) OO00OO(OO0OO("\x4e\x54\x50\x54\x5f\x50\x47\x45\x58\x54\x52\x41", '')); OO00OO(O000O); for (OO00O in OOO00) { O0O0O = OOO00[OO00O]; if (typeof (O0O0O) == "\x73\x74\x72\x69\x6e\x67") { if (O0O0O && (O0O0O != '')) O0OO0O = OO0O000(O0OO0O, (OO00O + "\x3d" + (self.encodeURIComponent ? encodeURIComponent(O0O0O) : escape(O0O0O)))); }; } return O0OO0O; }; function O000000() { var OO00O; OOOOO0.OOO00 = new Array(); for (OO00O in OOO00) OOOOO0.OOO00[OO00O] = OOO00[OO00O]; }; function OOO00OO() { var OO00O; OOO00 = new Array(); for (OO00O in OOOOO0.OOO00) OOO00[OO00O] = OOOOO0.OOO00[OO00O]; }; function OO0O0OO(O00O00, O0OOOO0, O000OO) { if (OOOO0[O00O00] != null) { var O000O0 = new Function(O0OOOO0); OOOO0[O00O00].onload = O000O0; OOOO0[O00O00].onerror = O000O0; OOOO0[O00O00].onabort = O000O0; } setTimeout(O0OOOO0, (O000OO * (0x5f3 + 3206 - 0xe91))); }; function O0O00O0(O0OOOO, OO0O0O) { if (O0OOOO == '') return; O0000 = ((O0000 + (0x1312 + 1405 - 0x188e)) % OOOO0.length); if (OOOO0[O0000] == null) OOOO0[O0000] = new Image((0x1005 + 4276 - 0x20b8), (0x1208 + 715 - 0x14d2)); OOOO0[O0000].src = O0OOOO + "\x3f" + OO0O0O; }; function OOOOO0O(O000O) { var O0OOOO; var OO0O0O; if ((O00O00O != '') && (document.location.protocol == "\x68\x74\x74\x70\x73\x3a")) O0OOOO = O00O00O; else O0OOOO = O0000OO; OO0O0O = OOO0000(O000O); O0O00O0(O0OOOO, OO0O0O); OOO00OO(); }; function OO00OO(O000O) { var OO00O0; var O00O0O; if (!O000O) return; O000O = O000O.toString(); if (O000O == '') return; OO00O0 = O000O.split("\x26"); for (O00O0O = (0xdc + 1230 - 0x5aa); O00O0O < OO00O0.length; O00O0O++) { var OOO0O0 = OO00O0[O00O0O].split("\x3d"); if (OOO0O0.length == (0x83d + 4370 - 0x194d)) O00OO(OOO0O0[(0x1240 + 5137 - 0x2651)], (self.decodeURIComponent ? decodeURIComponent(OOO0O0[(0xa7d + 3816 - 0x1964)]) : unescape(OOO0O0[(0xd8f + 2979 - 0x1931)]))); } }; function O0O0OO(O000O) { O00OO("\x65\x74\x73", O000O00()); OOOOO0O(O000O); return true; }; function O00OO0O(OOOOO, O000O, O000OO) { var O0OOO; if (!OOOOO || !OOOOO.href) return true; if (LnkLck) return false; LnkLck = OOOOO; if (OO000.lc) O00OO("\x6c\x63", OOOOO.href); if (OO000.rf) { if (!O0OO000 || !top || !top.document) O00OO("\x72\x66", document.location); } O0O0OO(O000O); if (O000OO) O0OOO = O000OO; else O0OOO = NTPT_MAXTAGWAIT; if (O0OOO > (0x659 + 6874 - 0x2133)) { var OOOOOO; if (OOOOO.click) { OOOOO.tmpclck = OOOOO.onclick; OOOOO.onclick = null; OOOOOO = "\x69\x66\x20\x28\x20\x4c\x6e\x6b\x4c\x63\x6b\x20\x29\x20\x7b\x20\x4c\x6e\x6b\x4c\x63\x6b\x2e\x63\x6c\x69\x63\x6b\x28\x29\x3b\x20\x4c\x6e\x6b\x4c\x63\x6b\x2e\x6f\x6e\x63\x6c\x69\x63\x6b\x20\x3d\x20\x4c\x6e\x6b\x4c\x63\x6b\x2e\x74\x6d\x70\x63\x6c\x63\x6b\x3b\x20\x4c\x6e\x6b\x4c\x63\x6b\x20\x3d\x20\x6e\x75\x6c\x6c\x3b\x20\x7d"; } else OOOOOO = "\x69\x66\x20\x28\x20\x4c\x6e\x6b\x4c\x63\x6b\x20\x29\x20\x7b\x20\x77\x69\x6e\x64\x6f\x77\x2e\x6c\x6f\x63\x61\x74\x69\x6f\x6e\x2e\x68\x72\x65\x66\x20\x3d\x20\x22" + OOOOO.href + "\x22\x3b\x20\x4c\x6e\x6b\x4c\x63\x6b\x20\x3d\x20\x6e\x75\x6c\x6c\x3b\x20\x7d"; OO0O0OO(O0000, OOOOOO, O0OOO); return false; } LnkLck = null; return true; }; function O000OO0(OO0OOO, O000O, O000OO) { var O0OOO; if (!OO0OOO || !OO0OOO.submit) return true; if (FrmLck) return false; FrmLck = OO0OOO; O0O0OO(O000O); if (O000OO) O0OOO = O000OO; else O0OOO = NTPT_MAXTAGWAIT; if (O0OOO > (0x1497 + 4406 - 0x25cd)) { OO0OOO.tmpsbmt = OO0OOO.onsubmit; OO0OOO.onsubmit = null; OO0O0OO(O0000, "\x69\x66\x20\x28\x20\x46\x72\x6d\x4c\x63\x6b\x20\x29\x20\x7b\x20\x46\x72\x6d\x4c\x63\x6b\x2e\x73\x75\x62\x6d\x69\x74\x28\x29\x3b\x20\x46\x72\x6d\x4c\x63\x6b\x2e\x6f\x6e\x73\x75\x62\x6d\x69\x74\x20\x3d\x20\x46\x72\x6d\x4c\x63\x6b\x2e\x74\x6d\x70\x73\x62\x6d\x74\x3b\x20\x46\x72\x6d\x4c\x63\x6b\x20\x3d\x20\x6e\x75\x6c\x6c\x3b\x20\x7d", O0OOO); return false; } FrmLck = null; return true; }; var O0000OO = NTPT_IMGSRC; var OO000 = NTPT_FLDS; var O00OO0 = OO0OO("\x4e\x54\x50\x54\x5f\x47\x4c\x42\x4c\x43\x4f\x4f\x4b\x49\x45\x53", null); var OOOO0O = OO0OO("\x4e\x54\x50\x54\x5f\x50\x47\x43\x4f\x4f\x4b\x49\x45\x53", null); var OOO00O0 = OO0OO("\x4e\x54\x50\x54\x5f\x53\x45\x54\x5f\x49\x44\x43\x4f\x4f\x4b\x49\x45", false); var OO0OO0 = OO0OO("\x4e\x54\x50\x54\x5f\x49\x44\x43\x4f\x4f\x4b\x49\x45\x5f\x4e\x41\x4d\x45", "\x53\x61\x6e\x65\x49\x44"); var OO00O00 = OO0OO("\x4e\x54\x50\x54\x5f\x49\x44\x43\x4f\x4f\x4b\x49\x45\x5f\x44\x4f\x4d\x41\x49\x4e", null); var OO0OOOO = OO0OO("\x4e\x54\x50\x54\x5f\x49\x44\x43\x4f\x4f\x4b\x49\x45\x5f\x45\x58\x50\x49\x52\x45", 155520000); var O00O00O = OO0OO("\x4e\x54\x50\x54\x5f\x48\x54\x54\x50\x53\x49\x4d\x47\x53\x52\x43", ''); var O0OO000 = OO0OO("\x4e\x54\x50\x54\x5f\x50\x47\x52\x45\x46\x54\x4f\x50", OO0OO("\x4e\x54\x50\x54\x5f\x47\x4c\x42\x4c\x52\x45\x46\x54\x4f\x50", false)); var OO00000 = OO0OO("\x4e\x54\x50\x54\x5f\x4e\x4f\x49\x4e\x49\x54\x49\x41\x4c\x54\x41\x47", false); var ntptAddPair = O00OO; var ntptDropPair = O0OO0O0; var ntptEventTag = O0O0OO; var ntptLinkTag = O00OO0O; var ntptSubmitTag = O000OO0; var OOO00 = new Array(); var OOOOO0 = new Object(); var OOOO0 = Array((0x317 + 3540 - 0x10e1)); var O0000; for (O0000 = (0x1584 + 3590 - 0x238a); O0000 < OOOO0.length; O0000++) OOOO0[O0000] = null; var LnkLck = null; var FrmLck = null; O00OO("\x6a\x73", "\x31"); O00OO("\x74\x73", O000O00()); if (OO000.lc) O00OO("\x6c\x63", document.location); if (OO000.rf) { var OOO0OO; if (O0OO000 && top && top.document) OOO0OO = top.document.referrer; else OOO0OO = document.referrer; O00OO("\x72\x66", OOO0OO); } if (self.screen) { if (OO000.rs) O00OO("\x72\x73", self.screen.width + "\x78" + self.screen.height); if (OO000.cd) O00OO("\x63\x64", self.screen.colorDepth); } if (OO000.ln) { var OOO0O; if (navigator.language) OOO0O = navigator.language; else if (navigator.userLanguage) OOO0O = navigator.userLanguage; else OOO0O = ''; if (OOO0O.length > (0x462 + 2203 - 0xcfb)) OOO0O = OOO0O.substring((0xe45 + 3555 - 0x1c28), (0x186 + 8395 - 0x224f)); OOO0O = OOO0O.toLowerCase(); O00OO("\x6c\x6e", OOO0O); } if (OO000.tz) { var OO0O0; var O0O00O = new Date(); var O0O00 = O0O00O.getTimezoneOffset(); var O0OO00; OO0O0 = "\x47\x4d\x54"; if (O0O00 != (0x1214 + 4348 - 0x2310)) { if (O0O00 > (0x773 + 6772 - 0x21e7)) OO0O0 += "\x20\x2d"; else OO0O0 += "\x20\x2b"; O0O00 = Math.abs(O0O00); O0OO00 = Math.floor(O0O00 / (0x878 + 3391 - 0x157b)); O0O00 -= O0OO00 * (0xc3b + 4046 - 0x1bcd); if (O0OO00 < (0x13e6 + 969 - 0x17a5)) OO0O0 += "\x30"; OO0O0 += O0OO00 + "\x3a"; if (O0O00 < (0xba1 + 208 - 0xc67)) OO0O0 += "\x30"; OO0O0 += O0O00; } O00OO("\x74\x7a", OO0O0); } if (OO000.jv) { var O0000O; if (navigator.javaEnabled()) O0000O = "\x31"; else O0000O = "\x30"; O00OO("\x6a\x76", O0000O); } var O0OO0 = new Array(); var O00O0OO = false; if (OO000.ck) { var O0O0O0; var O00O0, O0OOO0; if (O00OO0) { for (O0O0O0 = (0x87a + 7306 - 0x2504); O0O0O0 < O00OO0.length; O0O0O0++) { O0OO0[O00OO0[O0O0O0]] = ""; }; } if (OOOO0O) { for (O0O0O0 = (0x1b2a + 931 - 0x1ecd); O0O0O0 < OOOO0O.length; O0O0O0++) { O0OO0[OOOO0O[O0O0O0]] = ""; }; } for (OO00O in O0OO0) { O00O0 = OOOO00(OO00O); if (O00O0) { O0OO0[OO00O] = O00O0; }; } if (OOO00O0) { O00O0 = OOOO00(OO0OO0); if (O00O0) { O0OO0[OO0OO0] = O00O0; O00O0OO = true; }; } O0OOO0 = O00000O(O0OO0); if (O0OOO0 != "") O00OO("\x63\x6b", O0OOO0); } O000000(); if (!OO00000) OOOOO0O(''); if (OOO00O0 && !O00O0OO) { var O00O0 = OOOO00(OO0OO0); if (!O00O0) { O00O0 = O0O000O(); OOOO000(OO0OO0, O00O0, OO0OOOO, OO00O00); if (OO000.ck && OOOO00(OO0OO0)) { O0OO0[OO0OO0] = O00O0; var O0OOO0 = O00000O(O0OO0); if (O0OOO0 != "") { O00OO("\x63\x6b", O0OOO0); O000000(); }; }; }; }
        }
    </script>
    
    <style type="text/css">
        #header
        {
            height: 69px;
            background-image: url(images/banner.gif);
            background-repeat: no-repeat;
        }
        
        .loginView
        {
        	
        	position:relative;
        	top:10px;
        	left:670px;
        	color:White;
            font-family:Calibri;
        }
        .supportView
        {
        	
        	position:relative;
                font-size:12px;
        	top:10px;
        	left:770px;
        	color:White;
		text-align:left;
            font-family:Calibri;
        }
        
        .loginView A:link    { color:White; }
        .loginView A:visited { color:White; }
        .loginView A:active  { color:White; }
        .loginView A:hover   { color:White; }
        .supportView A:link    { color:White; }
        .supportView A:visited { color:White; }
        .supportView A:active  { color:White; }
        .supportView A:hover   { color:White; }
        
    </style>
</head>
<body style="margin: 0 0 0 0;">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
    </telerik:RadWindowManager>
    <telerik:RadComboBox ID="RadComboBox1" runat="server">
    </telerik:RadComboBox>
    <div>
        <radA:RadAjaxManager ID="rax" runat="server" OnAjaxRequest="RadAjaxManager1_AjaxRequest">
        
            <AjaxSettings>
                <radA:AjaxSetting AjaxControlID="rax">
                    <UpdatedControls>
                        <radA:AjaxUpdatedControl ControlID="DataListSlideThumbs" />
                        <radA:AjaxUpdatedControl ControlID="iFrame" />
                        <radA:AjaxUpdatedControl ControlID="hypDownload" />
                    </UpdatedControls>
                </radA:AjaxSetting>
                <radA:AjaxSetting AjaxControlID="tvSL">
                    <UpdatedControls>
                        <radA:AjaxUpdatedControl ControlID="tvSL" LoadingPanelID="AjaxLoadingPanel1" />
                        <radA:AjaxUpdatedControl ControlID="DataListSlideThumbs" LoadingPanelID="AjaxLoadingPanel2" />
                        <radA:AjaxUpdatedControl ControlID="iFrame" />
                        <radA:AjaxUpdatedControl ControlID="hypDownload" />
                    </UpdatedControls>
                </radA:AjaxSetting>
                <radA:AjaxSetting AjaxControlID="ImageButtonAddToMySlides">
                    <UpdatedControls>
                        <radA:AjaxUpdatedControl ControlID="DataListMySlides" LoadingPanelID="AjaxLoadingPanel3" />
                    </UpdatedControls>
                </radA:AjaxSetting>
                <radA:AjaxSetting AjaxControlID="ImageButtonAddAllToMySlides">
                    <UpdatedControls>
                        <radA:AjaxUpdatedControl ControlID="DataListMySlides" LoadingPanelID="AjaxLoadingPanel3" />
                    </UpdatedControls>
                </radA:AjaxSetting>
                <radA:AjaxSetting AjaxControlID="btnBack">
                    <UpdatedControls>
                        <radA:AjaxUpdatedControl ControlID="iFrame" LoadingPanelID="AjaxLoadingPanel2" />
                        <radA:AjaxUpdatedControl ControlID="hypDownload" />
                    </UpdatedControls>
                </radA:AjaxSetting>
                <radA:AjaxSetting AjaxControlID="btnForward">
                    <UpdatedControls>
                        <radA:AjaxUpdatedControl ControlID="iFrame" LoadingPanelID="AjaxLoadingPanel2" />
                        <radA:AjaxUpdatedControl ControlID="hypDownload" />
                    </UpdatedControls>
                </radA:AjaxSetting>
                <radA:AjaxSetting AjaxControlID="DataListSlideThumbs">
                    <UpdatedControls>
                        <radA:AjaxUpdatedControl ControlID="DataListSlideThumbs" LoadingPanelID="AjaxLoadingPanel2" />
                        <radA:AjaxUpdatedControl ControlID="RadPaneSlidePreview" />
                        <radA:AjaxUpdatedControl ControlID="DataListMySlides" LoadingPanelID="AjaxLoadingPanel3" />
                        <radA:AjaxUpdatedControl ControlID="iFrame" />
                        <radA:AjaxUpdatedControl ControlID="hypDownload" />
                    </UpdatedControls>
                </radA:AjaxSetting>
                <radA:AjaxSetting AjaxControlID="ImageButtonRemoveAllMySlides">
                    <UpdatedControls>
                        <radA:AjaxUpdatedControl ControlID="DataListMySlides" LoadingPanelID="AjaxLoadingPanel3" />
                    </UpdatedControls>
                </radA:AjaxSetting>
                <radA:AjaxSetting AjaxControlID="DataListMySlides">
                    <UpdatedControls>
                        <radA:AjaxUpdatedControl ControlID="DataListSlideThumbs" />
                        <radA:AjaxUpdatedControl ControlID="DataListMySlides" LoadingPanelID="AjaxLoadingPanel2" />
                        <radA:AjaxUpdatedControl ControlID="iFrame" />
                        <radA:AjaxUpdatedControl ControlID="hypDownload" />
                    </UpdatedControls>
                </radA:AjaxSetting>
            </AjaxSettings>
        </radA:RadAjaxManager>

        <script language="javascript" type="text/javascript">
        
        function Refresh(nodeXml)
        {
           alert(nodeXml); 
        }
        
       
        function changeCurNode(catID)
        {
            try 
            {           
                currentCategory = catID;
                ntptEventTag( 'ev=navigation&categoryid=' + currentCategory );
                <%=rax.ClientID %>.AjaxRequest('GetThumbs|'+catID); 
            }
            catch(err)
            {
                alert(err);
            }
        }
        
        function getPPT(nodeXml)
        {
            var index = nodeXml.toString().indexOf('SlideID');
            var substring = nodeXml.toString().substring(index, index + 20);
            var firstquote = substring.indexOf('"', 0);
            var secondquote = substring.indexOf('"', firstquote + 1);
            var slideNumber = substring.substring(firstquote + 1, secondquote);
        
            // Write Analytics Info:
            slideid = currentCategory + ':' + slideNumber;
            reRunAnalytics();

            <%=rax.ClientID %>.AjaxRequest('SlideNodeClick|'+nodeXml);            
        }
        
        
        function BeforeToggleHandler(node)
        {
            if(!node.Expanded)
            {
                <%=rax.ClientID %>.AjaxRequest(node.ClientID);   
            }   
        }
          
        function BrowseMode()
        {
            CollapseMySlides();
            HideThumbnails();
        }
          
        function BuildMode()
        {
            ExpandMySlides();
            ShowThumbnails();
        }  
        function ExpandMySlides()
        {
            var splitter = <%= RadSplitterRightTopOuter.ClientID %>;
            var pane = splitter.GetPaneById("RadPaneRightBottom");
            if(!pane.IsExpanded())
            {
                pane.Expand();
            }
        }
        
        function CollapseMySlides()
        {
            var splitter = <%= RadSplitterRightTopOuter.ClientID %>;
            var pane = splitter.GetPaneById("RadPaneRightBottom");
            if(!pane.IsCollapsed())
            {
                pane.Collapse();
            }
        }
        
        function HideThumbnails()
        {
            var splitter = <%= RadSplitterRightTop.ClientID %>;
            var pane = splitter.GetPaneById("RadPaneThumbnails");
            if(!pane.IsCollapsed())
            {
                pane.Collapse();
            }
        }
        
        function ShowThumbnails()
        {
            var splitter = <%= RadSplitterRightTop.ClientID %>;
            var pane = splitter.GetPaneById("RadPaneThumbnails");
            if(!pane.IsExpanded())
            {
                pane.Expand();
            }
        }
        
        function ChangeViewMode(item)
        {
            setSegment('browsemode', item.Text);
            if(item.Text == 'Browse Mode')
            {
                CollapseMySlides();
                HideThumbnails();
            }
            else if(item.Text == 'Browse Mode-Thumbnails')
            {
                CollapseMySlides();
                ShowThumbnails();
            }
            else if(item.Text == 'Build Mode')
            {
                ExpandMySlides();
                ShowThumbnails();
            }
        }
        
        function ExpandFilters()
        {
            var splitter = <%= RadSplitterLeftTopBottom.ClientID %>;
            var pane = splitter.GetPaneById("RadPaneFilters");
            if(!pane.IsExpanded())
            {
                pane.Expand();
            }
        }
        
        function CollapseFilters()
        {
            var splitter = <%= RadSplitterLeftTopBottom.ClientID %>;
            var pane = splitter.GetPaneById("RadPaneFilters");
            if(!pane.IsCollapsed())
            {
                pane.Collapse();
            }
        }
                
        </script>

        <radspl:RadSplitter ID="RadSplitterPage" runat="server" FullScreenMode="True" Height="100%"
            Orientation="Horizontal" Width="100%">
        
            <radspl:RadPane ID="RadPanePageHeader" runat="server" CssClass="HeaderSplitter" Height="69px"
                Scrolling="None" Width="886" MinWidth="886" MinHeight="69" MaxHeight="69" MaxWidth="886">
                <div id="header">
                    <asp:LoginView ID="LoginView1" runat="server">
                        <LoggedInTemplate>
                            <div class="loginView">
                                Hello
                                <asp:LoginName ID="LoginName1" runat="server" />
                                |
                                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Users/MyAccount.aspx">My Account</asp:HyperLink>
                                |
                                <asp:LoginStatus ID="LoginStatus1" runat="server" />
                            </div>
                            <div class="supportView">
                                <a href="mailto:content@hpprintingsalesguide.com" onclick="ntptEventTag( 'ev=email link&mailto_lk=' + escape( this.href ).toLowerCase() );">Content Support</a> &nbsp;&nbsp; <a href="mailto:support@hpprintingsalesguide.com" onclick="ntptEventTag( 'ev=email link&mailto_lk=' + escape( this.href ).toLowerCase() );">Technical Support</a>
                            </div>
                        </LoggedInTemplate>
                    </asp:LoginView>
                </div>
            </radspl:RadPane>
            <radspl:RadPane ID="RadPaneMainMenu" runat="server" Height="24px" Scrolling="None"
                Width="" MinWidth="800">
                <table cellpadding="0" cellspacing="0" style="background-color: #fff; border-top: solid 1px #ooo;
                    border-bottom: solid 1px #ooo;">
                    <tr>
                        <td style="width: 222px;">
                            <img src="images/RegionLanguage.gif" style="vertical-align: middle;" />
                            <radC:RadComboBox ID="RadComboBoxRegionLanguage" runat="server" AutoPostBack="True"
                                OnSelectedIndexChanged="RadComboBox1_SelectedIndexChanged" SkinsPath="~/RadControls/"
                                ExpandEffect="Fade" Width="160px">
                            </radC:RadComboBox>
                        </td>
                        <td style="width: 347px;">
                            <img src="images/ViewMode.gif" style="vertical-align: middle;" />
                            <radC:RadComboBox ID="RadComboBoxViewMode" runat="server" SkinsPath="~/RadControls/"
                                ExpandEffect="Fade" CatalogIconImageUrl="img/Browse.jpg" HighlightTemplatedItems="True"
                                Width="160px" OnClientSelectedIndexChanged="ChangeViewMode">
                                <ItemTemplate>
                                    <span style="color: Gray; font-family: Arial; font-size: 11px;">
                                        <%# DataBinder.Eval(Container, "Attributes['DisplayName']")%>
                                    </span>
                                </ItemTemplate>
                                <Items>
                                    <radC:RadComboBoxItem ID="RadComboBoxItemBrowseMode" DisplayName="Browse Mode" Text="Browse Mode"
                                        runat="server" />
                                    <radC:RadComboBoxItem ID="RadComboBoxItemBrowseModeWithThumbnails" DisplayName="Browse Mode-Thumbnails"
                                        Text="Browse Mode-Thumbnails" runat="server" />
                                    <radC:RadComboBoxItem ID="RadComboBoxItemBuildMode" DisplayName="Build Mode" Text="Build Mode"
                                        runat="server" />
                                </Items>
                            </radC:RadComboBox>&nbsp;&nbsp;&nbsp;
                            <asp:HyperLink ID="HyperLink2" runat="server" Font-Size="10pt" ForeColor="Red" 
                                NavigateUrl="http://hpprintingsalesguide.com/creationstation/help/support.htm" 
                                Target="_blank">Problems viewing slides?</asp:HyperLink>
                        </td>
                        <!-- this td will contain the regions for selecting the default region -->
                        <!--
                            <td style="width: 422px;">
                                <img src="images/RegionLanguage.gif" alt="Select default region" style="vertical-align: middle;" /> 
                                <select id="regionSelect" onchange="RegionSettingManager.OnSelectedIndexChanged()">
                                <asp:Literal ID="regionSelectLiteral" runat="server"></asp:Literal>
                                </select>
                            </td>
                          
                        <td style="width: 222px;">
                            <img src="images/RegionLanguage.gif" style="vertical-align: middle;" />
                        </td>
                          -->
                    </tr>
                </table>
            </radspl:RadPane>
            <radspl:RadPane ID="RadPanePageContent" runat="server" Scrolling="None">
                <radspl:RadSplitter ID="RadSplitterMain" runat="server" Height="750px" Width="100%">
                    <radspl:RadPane ID="RadPaneMainLeft" runat="server" MinHeight="500" Scrolling="X"
                        Width="250px">
                        <radspl:RadSplitter ID="RadSplitterLeftTopBottom" runat="server" Orientation="Horizontal">
                            <radspl:RadPane ID="RadPaneSlideLibrary" runat="server" Height="" MinHeight="500"
                                Scrolling="None">
                                <radA:AjaxLoadingPanel ID="AjaxLoadingPanel1" runat="server" Height="75px" Width="75px">
                                    <asp:Image ID="Image1" runat="server" AlternateText="Loading..." ImageUrl="~/images/loading.gif" />
                                </radA:AjaxLoadingPanel>

                                <script type="text/javascript" language="javascript">
                                    // if we've detected an acceptable version
                                    // embed the Flash Content SWF when all tests are passed

                                    WireStone_FL_RunContent(
			                                    "src", "testTree",
			                                    "FlashVars", "sourceXml=<%=GetTreeXmlFile() %>&filterIDs=<%=GetFilterIds() %>",
			                                    "width", "100%",
			                                    "height", "95%",
			                                    "align", "left",
			                                    "id", "testTree",
			                                    "quality", "high",
			                                    "bgcolor", "#FFFFFF",
			                                    "name", "testTree",
			                                    "allowScriptAccess", "always",
			                                    "type", "application/x-shockwave-flash",
			                                    'codebase', 'http://fpdownload.macromedia.com/get/flashplayer/current/swflash.cab',
			                                    "pluginspage", "http://www.adobe.com/go/getflashplayer"
	                                    );
                                    
                                </script>

                                <asp:ImageButton ID="ImageButtonShowFilters" runat="server" ImageUrl="~/images/FilterButton.gif"
                                    OnClientClick="ExpandFilters(); return false;" />
                                <iframe id="iFrameBlank" name="iFrameBlank" src="Blank.ppt" width="1" height="1">
                                </iframe>
                            </radspl:RadPane>
                            <radspl:RadPane ID="RadPaneFilters" runat="server" Scrolling="None" Width="" InitiallyCollapsed="True">
                                <asp:CheckBoxList ID="CheckBoxListFilters" runat="server" Font-Names="Arial" Font-Size="11px">
                                </asp:CheckBoxList>
                                <asp:Button ID="ButtonApplyFilters" runat="server" Text="Apply Filters" OnClick="ButtonApplyFilters_Click"
                                    OnClientClick="CollapseFilters();" />
                            </radspl:RadPane>
                        </radspl:RadSplitter>
                    </radspl:RadPane>
                    <radspl:RadSplitBar ID="RadSplitBarMain" runat="server" CollapseMode="Forward" />
                    <radspl:RadPane ID="RadPaneMainRight" runat="server" Scrolling="None">
                        <radspl:RadSplitter ID="RadSplitterRightTopOuter" runat="server" Height="100%" Orientation="Horizontal"
                            Width="100%">
                            <radspl:RadPane ID="RadPaneBlueBar" runat="server" Height="33px" Scrolling="None"
                                MaxHeight="33" MinHeight="33">
                                <table cellpadding="0" cellspacing="0" border="0" width="100%" style="background-image: url(images/BlueBarStretch.jpg);">
                                    <tr>
                                        <td style="width: 185px;">
                                            <img src="images/Thumbnails.gif" alt="Thumbnails" />
                                        </td>
                                        <td style="width: 12px;">
                                            <img src="images/BlueBarSeparator.gif" alt="" />
                                        </td>
                                        <td style="width: 100px;">
                                            <asp:ImageButton ID="ImageButtonAddToMySlides" runat="server" ImageUrl="~/images/AddToMySlides.gif"
                                                OnClick="ImageButtonAddToMySlides_Click" ToolTip="Add the slide current in the preview area to My Slides."
                                                EnableViewState="False" OnClientClick="BuildMode();" />
                                        </td>
                                        <td style="width: 110px;">
                                            <asp:ImageButton ID="ImageButtonAddAllToMySlides" runat="server" ImageUrl="~/images/AddAllToMySlides.gif"
                                                ToolTip="Add all of the slide current in the preview area to My Slides." EnableViewState="False"
                                                OnClick="ImageButtonAddAllToMySlides_Click" />
                                        </td>
                                        <td style="width: 110px;">
                                            <asp:HyperLink runat="server" ID="hypDownload" ImageUrl="~/images/DownloadSlide.gif" 
                                            ToolTip="Download the slide currently in the preview area" NavigateUrl="#">
                                            </asp:HyperLink>
                                        </td>
                                        <td style="background-image:url('images/HistoryLabel.jpg'); background-repeat:no-repeat;">
                                            <asp:Button ID="btnBack" runat="server" Text="<" ToolTip="Back" OnClick="btnBack_Click"
                                                BackColor="Navy" BorderColor="White" BorderStyle="Solid" BorderWidth="1px" Font-Bold="True"
                                                ForeColor="White" />
                                            <asp:Button ID="btnForward" runat="server" Text=">" ToolTip="Forward" OnClick="btnForward_Click"
                                                BackColor="Navy" BorderColor="White" BorderStyle="Solid" BorderWidth="1px" Font-Bold="True"
                                                ForeColor="White" />
                                        </td>
                                    </tr>
                                </table>
                            </radspl:RadPane>
                            <radspl:RadPane ID="RadPaneRightTop" runat="server" Scrolling="None">
                                <radspl:RadSplitter ID="RadSplitterRightTop" runat="server" HeightOffset="5" Height="100%"
                                    Width="100%">
                                    <radspl:RadPane ID="RadPaneThumbnails" runat="server" Width="185px" Scrolling="Y"
                                        InitiallyCollapsed="True">
                                        <radA:AjaxLoadingPanel ID="AjaxLoadingPanel2" runat="server" Height="75px" Width="75px">
                                            <br />
                                            <asp:Image ID="Image2" runat="server" AlternateText="Loading..." ImageUrl="~/images/loading.gif" />
                                        </radA:AjaxLoadingPanel>
                                        <asp:DataList ID="DataListSlideThumbs" runat="server" Width="175px" OnItemCommand="DataListSlideThumbs_ItemCommand">
                                            <SelectedItemStyle BackColor="LightGoldenrodYellow" />
                                            <ItemTemplate>
                                                <div style="margin: 2px 5px 2px 5px;">
                                                    <center>
                                                        <table cellpadding="0" cellspacing="0" border="0">
                                                            <tr>
                                                                <td valign="top">
                                                                    <table cellpadding="0" cellspacing="0" border="0">
                                                                        <tr>
                                                                            <td valign="top">
                                                                                <asp:ImageButton ID="ImageButtonAddThisSlideToMySlides" runat="server" CommandArgument='<%# Eval("SlideUrl") %>'
                                                                                    CommandName="ImageButtonAddThisSlideToMySlides" ImageUrl="~/images/AddBluePNG.png"
                                                                                    Visible='<%# IsShown(Eval("SlideUrl").ToString()) %>' Height="16" Width="16"
                                                                                    ToolTip="Add this slide to My Slides." />
                                                                            </td>
                                                                            <td valign="top">
                                                                                <asp:ImageButton ID="btnThumb" runat="server" CommandArgument='<%# Eval("SlideUrl") %>'
                                                                                    CommandName="btnThumb" Height="72" ImageUrl='<%# Eval("SlideThumbUrl") %>' Visible='<%# IsShown(Eval("SlideUrl").ToString()) %>'
                                                                                    Width="96" ToolTip="Click to preview." />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2">
                                                                    <span style="font-size: 10px; font-family: Arial; text-align: center;">
                                                                        <%# Eval("Title") %>
                                                                    </span>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </center>
                                                </div>
                                            </ItemTemplate>
                                            <AlternatingItemStyle BackColor="WhiteSmoke" />
                                        </asp:DataList></radspl:RadPane>
                                    <radspl:RadPane ID="RadPaneSlidePreview" runat="server" Scrolling="None">
                                        <!-- src="http://ljb.deckmanager.com/Registration/ClientStartPage.aspx" -->
                                        <iframe id="iFrame" name="iframe" runat="server" height="100%" width="100%"></iframe>
                                    </radspl:RadPane>
                                </radspl:RadSplitter>
                            </radspl:RadPane>
                            <radspl:RadSplitBar ID="RadSplitBarRightArea" runat="server" EnableResize="True" />
                            <radspl:RadPane ID="RadPaneRightBottom" runat="server" InitiallyCollapsed="True"
                                Scrolling="None">
                                <radspl:RadSplitter ID="RadSplitterRightBottom" runat="server" Height="300px" Width="100%"
                                    Orientation="Horizontal">
                                    <radspl:RadPane ID="RadPaneGreenBar" runat="server" Width="" Height="33px" Scrolling="None">
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%" style="background-image: url(images/GreenBarStretch.jpg);">
                                            <tr>
                                                <td style="width: 185px;">
                                                    <img src="images/MySlidesHeader.gif" alt="" />
                                                </td>
                                                <td style="width: 12px;">
                                                    <img src="images/GreenBarSeparator.gif" alt="" />
                                                </td>
                                                <td style="width: 100px;">
                                                    <asp:HyperLink id="hypDownloadMySlides" runat="server" ImageUrl="~/images/DownloadMySlides.gif" NavigateUrl="DownloadMySlides.aspx"
                                                        ToolTip="Download My Slides" Target="_blank"></asp:HyperLink>
                                                    <%--  <asp:ImageButton ID="ImageButtonDownloadMySlides" runat="server" ImageUrl="~/images/DownloadMySlides.gif"
                                                        OnClick="ImageButtonDownloadMySlides_Click" ToolTip="Download My Slides." EnableViewState="False" />--%>
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="ImageButtonRemoveAllMySlides" runat="server" ImageUrl="~/images/RemoveAll.gif"
                                                        OnClick="ImageButtonRemoveAllMySlides_Click" ToolTip="Remove all slides from My Slides."
                                                        EnableViewState="False" />
                                                </td>
                                            </tr>
                                        </table>
                                    </radspl:RadPane>
                                    <radspl:RadPane ID="RadPaneMySlides" runat="server" Width="" Height="" Scrolling="Y">
                                        <radA:AjaxLoadingPanel ID="AjaxLoadingPanel3" runat="server" Height="100%" Width="100%">
                                            <br />
                                            <br />
                                            <span style="text-align: center;">
                                                <asp:Image ID="Image3" runat="server" AlternateText="Loading..." ImageUrl="~/images/loading.gif" /></span>
                                        </radA:AjaxLoadingPanel>
                                        <asp:DataList ID="DataListMySlides" runat="server" Width="1px" OnItemCommand="DataListMySlides_ItemCommand"
                                            RepeatColumns="6" RepeatDirection="Horizontal">
                                            <SelectedItemStyle BackColor="LightGoldenrodYellow" />
                                            <ItemTemplate>
                                                <%--<div style="margin: 2px 5px 2px 5px; vertical-align:top;">--%>
                                                <table cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td valign="top" style="text-align: center;">
                                                            <table cellpadding="0" cellspacing="0" border="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:ImageButton ID="btnThumb" runat="server" CommandArgument='<%# Eval("SlideUrl") %>'
                                                                            CommandName="btnThumb" Height="72" ImageUrl='<%# Eval("ThumbUrl") %>' Visible='<%# IsShown(Eval("SlideUrl").ToString()) %>'
                                                                            Width="96" ToolTip="Click to preview." />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top">
                                                            <table cellpadding="0" cellspacing="3" border="0" width="100%">
                                                                <tr>
                                                                    <td valign="middle" style="width: 25%; text-align: right;">
                                                                        <asp:ImageButton ID="ImageButtonMoveLeft" runat="server" CommandName="ImageButtonMoveLeft"
                                                                            ImageUrl="~/images/GreenArrowLeft.gif" Visible='<%# IsShown(Eval("SlideUrl").ToString()) %>'
                                                                            ToolTip="Move this slide back in the slide order." />
                                                                    </td>
                                                                    <td valign="middle" style="width: 25%; text-align: left;">
                                                                        <asp:ImageButton ID="ImageButtonMoveRight" runat="server" CommandName="ImageButtonMoveRight"
                                                                            ImageUrl="~/images/GreenArrowRight.gif" Visible='<%# IsShown(Eval("SlideUrl").ToString()) %>'
                                                                            ToolTip="Move this slide forward in the slide order." />
                                                                    </td>
                                                                    <td valign="middle" style="width: 50%; text-align: center;">
                                                                        <asp:ImageButton ID="ImageButtonRemoveSlide" runat="server" CommandName="ImageButtonRemoveSlide"
                                                                            ImageUrl="~/images/RemoveGreenPNG.png" Visible='<%# IsShown(Eval("SlideUrl").ToString()) %>'
                                                                            Height="16" Width="16" ToolTip="Remove this slide from My Slides." />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top" style="text-align: center;">
                                                            <table cellpadding="0" cellspacing="0" border="0">
                                                                <tr>
                                                                    <td>
                                                                        <span style="font-size: 10px; font-family: Arial; text-align: center;">
                                                                            <%# Eval("Title") %>
                                                                        </span>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <%--</div>--%>
                                            </ItemTemplate>
                                            <AlternatingItemStyle BackColor="WhiteSmoke" />
                                            <ItemStyle Height="125px" HorizontalAlign="Center" VerticalAlign="Top" />
                                        </asp:DataList></radspl:RadPane>
                                </radspl:RadSplitter>
                            </radspl:RadPane>
                        </radspl:RadSplitter>
                    </radspl:RadPane>
                </radspl:RadSplitter>
            </radspl:RadPane>
        </radspl:RadSplitter>
    </div>
    </form>

    <script type="text/javascript">
        RegionSettingManager.WelcomePageUrl = '<%= WebConfigurationManager.AppSettings["WelcomePageUrl"].ToString() %>';
        RegionSettingManager.NavigateToWelcomePage();
    </script>

    <script language="javascript" type="text/javascript" src="js/analytics.js"></script>
    <script type="text/javascript">
        // if segment has been identified run this function to put in a cookie to be tracked
        function setSegment(cookiename, value) {
            var exdate = new Date();
            exdate.setDate(exdate.getDate() + 1826);
            document.cookie = cookiename + "=" + escape(value) + ";expires=" + exdate.toGMTString() + "; path=/";
        }

        function setLoginSegment() {
            document.cookie = "login=login; path=/";
        }

        function setLogoutSegment() {
            document.cookie = "login=logout; path=/";
        }
	</script>
	<asp:PlaceHolder ID="scriptPlaceholder" runat="server"></asp:PlaceHolder>
    <script language="javascript" type="text/javascript" src="js/ntpagetag.js"></script>
    <asp:PlaceHolder ID="ntptEventTagPlaceholder" runat="server"></asp:PlaceHolder>
    <noscript>
    <img src="/WebApp/images/ntpagetag.gif?js=0" height="1" width="1" border="0" hspace="0" vspace="0" alt="" />
    </noscript>

</body>
</html>
