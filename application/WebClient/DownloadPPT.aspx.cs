﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Net;

public partial class DownloadPPT : System.Web.UI.Page
{


    // Taken from http://www.yoda.arachsys.com/csharp/readbinary.html

    /// <summary>
    /// Reads data from a stream until the end is reached. The
    /// data is returned as a byte array. An IOException is
    /// thrown if any of the underlying IO calls fail.
    /// </summary>
    /// <param name="stream">The stream to read data from</param>
    /// <param name="initialLength">The initial buffer length</param>   
    /// 


    public static byte[] ReadFully(Stream stream, int initialLength)
    {
        // If we've been passed an unhelpful initial length, just
        // use 32K.
        if (initialLength < 1)
        {
            initialLength = 32768;
        }

        byte[] buffer = new byte[initialLength];
        int read = 0;

        int chunk;
        while ((chunk = stream.Read(buffer, read, buffer.Length - read)) > 0)
        {
            read += chunk;

            // If we've reached the end of our buffer, check to see if there's
            // any more information
            if (read == buffer.Length)
            {
                int nextByte = stream.ReadByte();

                // End of stream? If so, we're done
                if (nextByte == -1)
                {
                    return buffer;
                }

                // Nope. Resize the buffer, put in the byte we've just
                // read, and continue
                byte[] newBuffer = new byte[buffer.Length * 2];
                Array.Copy(buffer, newBuffer, buffer.Length);
                newBuffer[read] = (byte)nextByte;
                buffer = newBuffer;
                read++;
            }
        }
        // Buffer is now too big. Shrink it.
        byte[] ret = new byte[read];
        Array.Copy(buffer, ret, read);
        return ret;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        string filename = Request.QueryString["filename"];
        if (filename == "" || filename == null)
        {
            return;
        }
        if (filename.Contains("..") || filename.Contains(":") || filename.Contains("\\"))
        {
            return;
        }

        HttpWebRequest hr = WebRequest.Create(String.Format("{0}{1}", ConfigurationManager.AppSettings["SlideLocation"], filename)) as HttpWebRequest;
        HttpWebResponse resp = hr.GetResponse() as HttpWebResponse;
        Stream s = resp.GetResponseStream();
        byte[] pptfile = ReadFully(s, (int)resp.ContentLength);

        /*string slideFullPath = ConfigurationManager.AppSettings["SlidePhysicalPath"] + filename;

        FileInfo file = new FileInfo(slideFullPath);
        File f = File.*/

        Response.Clear();
        Response.AddHeader("Content-Disposition", "attachment; filename=" + filename.Replace(".pot", ".ppt"));
        Response.AddHeader("Content-Length", resp.ContentLength.ToString());
        Response.AddHeader("Cache-Control", "public");
        Response.ContentType = "application/octet-stream";
        Response.BinaryWrite(pptfile);

        //Response.WriteFile(file.FullName);
        Response.End();
        //rax.ResponseScripts.Add(String.Format(@"window.location.href = ""{0}"";", "URL HERE"));

    }
}
