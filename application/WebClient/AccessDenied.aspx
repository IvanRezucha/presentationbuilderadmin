<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AccessDenied.aspx.cs" Inherits="AccessDenied" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body style="margin: 0 0 0 0;">
    <form id="form1" runat="server">
    <div>
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td style="background-image:url('images/BannerStretch.gif')"><img src="images/banner.gif" alt="Deck Manager" /></td>
            </tr>
        </table>
    </div>
        <br />
        <span style="font-family: Arial"></span><span style="font-family: Arial; font-size: 16pt; color: navy;">
            &nbsp;Access Denied<br />
        </span>
        <br />
        <span style="font-size: 10pt; font-family: Arial">&nbsp;You have not been authorized to use this application. </span>   
    </form>
</body>
</html>
