using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for SlideNode
/// </summary>
public class SlideNode : Telerik.WebControls.RadTreeNode
{

    private string _SlideUrl;

    public string SlideUrl
    {
        get { return _SlideUrl; }
        set { _SlideUrl = value; }
    }

    
	public SlideNode()
	{
		//
		// TODO: Add constructor logic here
		//
	}
}
