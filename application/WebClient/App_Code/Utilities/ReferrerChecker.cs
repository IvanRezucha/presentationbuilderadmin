using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for ReferrerChecker
/// </summary>
public class ReferrerChecker
{
	public ReferrerChecker()
	{
	}


    //public ReferrerCheckResult IsReferrerInList(string referrer, string referrerListKey)
    public ReferrerCheckResult IsReferrerInList(Uri referrer, string referrerListKey)
    {
        if (ConfigurationManager.AppSettings[referrerListKey] == null)
        {
            return ReferrerCheckResult.NoReferrerListKeyFound;
        }

        if (ConfigurationManager.AppSettings[referrerListKey].ToString().Length <= 0)
        {
            return ReferrerCheckResult.BlankReferrerList;
        }
        
        if (referrer == null)
        {
    		return ReferrerCheckResult.NullReferrer; 
        }

        System.Collections.Generic.List<string> allowedReferrers = new System.Collections.Generic.List<string>(ConfigurationManager.AppSettings[referrerListKey].Split(','));
        string actualReferrer = referrer.ToString();
        
        foreach (string allowedReferrer in allowedReferrers)
        {
            if (actualReferrer.Contains(allowedReferrer))
            {
        		 return ReferrerCheckResult.InList;
            }
        }

        return ReferrerCheckResult.NotInList;
            
        
    }

    public enum ReferrerCheckResult
    {
        InList,
        NotInList,
        NullReferrer,
        NoReferrerListKeyFound,
        BlankReferrerList
    }
}
