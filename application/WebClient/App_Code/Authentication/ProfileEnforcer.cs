﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
/// <summary>
/// Summary description for ProfileEnforcer
/// </summary>
public class ProfileEnforcer
{
    PsgMembershipProvider provider;
    PsgProfile profile;
    System.Web.UI.Page requestedPage;
    public ProfileEnforcer(System.Web.UI.Page page)
    {
        requestedPage = page;
        string email = page.User.Identity.Name;
        provider = (PsgMembershipProvider)Membership.Provider;
        profile = provider.GetUserProfile(email);
    }
    public void Enforce()
    {
        if (profile.HasEmptyProperites())
            requestedPage.Response.Redirect("~/Users/MyAccount.aspx?pe=t");
        
    }
}
