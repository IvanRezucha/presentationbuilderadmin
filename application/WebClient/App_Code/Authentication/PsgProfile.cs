﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class PsgProfile
{
    private string email;
    private string firstName;
    private string lastName;
    private string phoneNumber;
    private string country;
    private int region;
    private string companyName;
    private int companySize;
    private string jobTitle;
    private string referrer;
    private bool temporaryPasswordInUse;    

    public string Email
    {
        get { return email; }
        set { email = value; }
    }
    public string FirstName
    {
        get { return firstName; }
        set { firstName = value; }
    }
    public string LastName
    {
        get { return lastName; }
        set { lastName = value; }
    }
    public string PhoneNumber
    {
        get { return phoneNumber; }
        set { phoneNumber = value; }
    }
    public string Country
    {
        get { return country; }
        set { country = value; }
    }
    public int Region
    {
        get { return region; }
        set { region = value; }
    }
    public string CompanyName
    {
        get { return companyName; }
        set { companyName = value; }
    }
    public int CompanySize
    {
        get { return companySize; }
        set { companySize = value; }
    }
    public string JobTitle
    {
        get { return jobTitle; }
        set { jobTitle = value; }
    }
    public string Referrer
    {
        get { return referrer; }
        set { referrer = value; }
    }
    public bool TemporaryPasswordInUse
    {
        get { return temporaryPasswordInUse; }
        set { temporaryPasswordInUse = value; }
    }

    public PsgProfile() { }   
    public PsgProfile(string email, string firstName, string lastName, string phoneNumber, string country, int region, string companyName, int companySize, string jobTitle, string referrer, bool temporaryPasswordInUse)
    {
        this.Email = email;
        this.FirstName = firstName;
        this.LastName = lastName;
        this.PhoneNumber = phoneNumber;
        this.Country = country;
        this.Region = region;
        this.CompanyName = companyName;
        this.CompanySize = companySize;
        this.JobTitle = jobTitle;
        this.Referrer = referrer;
        this.TemporaryPasswordInUse = temporaryPasswordInUse;

    }

    public bool HasEmptyProperites()
    {
        bool result = false;
        if (
            string.IsNullOrEmpty(this.Email) ||
            string.IsNullOrEmpty(this.FirstName) ||
            string.IsNullOrEmpty(this.LastName) ||
            string.IsNullOrEmpty(this.PhoneNumber) ||
            string.IsNullOrEmpty(this.Country) ||
            this.Region.Equals(0) ||
            string.IsNullOrEmpty(this.CompanyName) ||
            this.CompanySize.Equals(0) ||
            string.IsNullOrEmpty(this.JobTitle) ||
            string.IsNullOrEmpty(this.Referrer)
           )
        {
            result = true;
        }

        return result;
    }
}
