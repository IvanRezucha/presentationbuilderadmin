﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Configuration;
using System.Security.Cryptography;
using System.Text;

public delegate void UserValidatedEventHandler(object sender, UserValidatedEventArgs e);
public class UserValidatedEventArgs : EventArgs
{
    private PsgProfile profile;

    public UserValidatedEventArgs(PsgProfile profile)
    {
        this.profile = profile;
    }

    public PsgProfile Profile
    {
        get
        {
            return profile;
        }
    }
}

public class PsgMembershipProvider : MembershipProvider
{
    private UserWebService.UserRegistrationService userService;
    private UserVisitService.UserVisitService visitService;
    public event UserValidatedEventHandler UserValidated;
    bool eirg = false;
    public PsgMembershipProvider()
    {
    }

    public override bool EnablePasswordRetrieval
    {
        get { return true; }
    }

    public override bool EnablePasswordReset
    {
        get { return true; }
    }

    public override string ApplicationName
    {
        get
        {
            return "WebClient";
        }
        set
        {
            return;
        }
    }

    public override bool RequiresQuestionAndAnswer
    {
        get { return false; }
    }

    public override int MaxInvalidPasswordAttempts
    {
        get { return 3; }
    }

    public override int PasswordAttemptWindow
    {
        get { return 10; }
    }

    public override bool RequiresUniqueEmail
    {
        get { return true; }
    }

    public override MembershipPasswordFormat PasswordFormat
    {
        get { return MembershipPasswordFormat.Hashed; }
    }

    public override int MinRequiredPasswordLength
    {
        get { return 8; }
    }

    public override string Description
    {
        get
        {
            return base.Description;
        }
    }

    public override bool ChangePassword(string username, string oldPassword, string newPassword)
    {
        bool result = userService.ChangePassword(username, oldPassword, newPassword);
        return result;
    }

    public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer)
    {
        throw new Exception("The method or operation is not implemented.");
    }

    public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
    {
        status = (MembershipCreateStatus)userService.CreateUser(username, password, "sellhpprinters");
        //put some webservice stuff here to actually save the new user;
        return new MembershipUser("PsgMembershipProvider", username, new object(), username, string.Empty, string.Empty, true, false, DateTime.Now, DateTime.Now, DateTime.Now, DateTime.Now, DateTime.Now);
    }

    public void eirg_reg(string userEmail, DateTime dateTime)
    {
        userService.eirg_reg(userEmail, dateTime);        
    }

    //public override void eirg_reg(int userId, DateTime dateTime)
    //{
    //   //userService.CreateUser(username, password, "sellhpprinters");
    //    //put some webservice stuff here to actually save the new user;
    //    //return new MembershipUser("PsgMembershipProvider", username, new object(), username, string.Empty, string.Empty, true, false, DateTime.Now, DateTime.Now, DateTime.Now, DateTime.Now, DateTime.Now);
    //}

    protected override byte[] DecryptPassword(byte[] encodedPassword)
    {
        return base.DecryptPassword(encodedPassword);
    }

    public override bool DeleteUser(string username, bool deleteAllRelatedData)
    {
        throw new Exception("The method or operation is not implemented.");
    }

    protected override byte[] EncryptPassword(byte[] password)
    {
        return base.EncryptPassword(password);
    }

    public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
    {
        throw new Exception("The method or operation is not implemented.");
    }

    public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
    {
        throw new Exception("The method or operation is not implemented.");
    }

    //This method will be called after login is attempted.
    public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config)
    {
        base.Initialize(name, config);
        userService = new UserWebService.UserRegistrationService();
        visitService = new UserVisitService.UserVisitService();
    }

    public override MembershipUser GetUser(string username, bool userIsOnline)
    {
        MembershipUser user = new MembershipUser("PsgMembershipProvider", username, new object(), username, string.Empty, string.Empty, true, false, DateTime.Now, DateTime.Now, DateTime.Now, DateTime.Now, DateTime.Now);
        return user;
    }

    public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
    {
        throw new Exception("The method or operation is not implemented.");
    }

    public override int GetNumberOfUsersOnline()
    {
        throw new Exception("The method or operation is not implemented.");
    }

    public override int MinRequiredNonAlphanumericCharacters
    {
        get { return 0; }
    }

    public override string PasswordStrengthRegularExpression
    {
        get { throw new Exception("The method or operation is not implemented."); }
    }

    public override string GetPassword(string username, string answer)
    {
        throw new Exception("The method or operation is not implemented.");
    }

    public override string ResetPassword(string username, string answer)
    {
        throw new Exception("The method or operation is not implemented.");
    }

    public override void UpdateUser(MembershipUser user)
    {
        throw new Exception("The method or operation is not implemented.");
    }
    

    public override bool ValidateUser(string username, string password)
    {
        //string hashedPassword = HashPassword(password);
        string strApp = "WebClient";
        if (eirg) { strApp = "WebClient-eirg"; }

        bool isValidated = userService.ValidateUser(username, password, strApp);
        if (isValidated)
        {
            PsgProfile profile = GetUserProfile(username);
            OnUserValidated(new UserValidatedEventArgs(profile));
        }
        return isValidated;
    }

    public bool AutoValidateUser(string email)
    {
        UserWebService.UserRegistrationDS.UserProfileDataTable pt = userService.GetUserDataTableByEmail(email);
        eirg = true;
        return ValidateUser(pt[0].Email, pt[0].Password);

        //bool isValidated = userService.ValidateUser(pt[0].Email, pt[0].Password, "WebClient");
        //if (isValidated)
        //{
        //    PsgProfile profile = GetUserProfile(pt[0].Email);
        //    OnUserValidated(new UserValidatedEventArgs(profile));
        //}
        //return isValidated;
    }

    public string getUserinfoForPT(string email)
    {
        UserWebService.UserRegistrationDS.UserProfileDataTable pt = userService.GetUserDataTableByEmail(email);
        return pt[0].Password;

        //string hashedPassword = HashPassword(password);

        //bool isValidated = userService.ValidateUser(pt[0].Email, pt[0].Password, "WebClient");
        //if (isValidated)
        //{
        //    PsgProfile profile = GetUserProfile(pt[0].Email);
        //    OnUserValidated(new UserValidatedEventArgs(profile));
        //}
        //return isValidated;
    }

    public override string GetUserNameByEmail(string email)
    {
        throw new Exception("The method or operation is not implemented.");
    }

    public override bool UnlockUser(string userName)
    {
        throw new Exception("The method or operation is not implemented.");
    }

    public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
    {
        throw new Exception("The method or operation is not implemented.");
    }

    public string HashPassword(string password)
    {
        SHA1CryptoServiceProvider hash = new SHA1CryptoServiceProvider();
        byte[] plainTextBytes = Encoding.UTF8.GetBytes(password);
        byte[] hashBytes = hash.ComputeHash(plainTextBytes);
        int hashCode = BitConverter.ToInt32(hashBytes, 0);
        return hashCode.ToString();
    }

    public PsgProfile GetUserProfile(string email)
    {
        UserWebService.UserRegistrationDS.UserProfileDataTable profileTable = userService.GetUserByEmail(email);
        if (profileTable.Rows.Count > 0)
        {
            UserWebService.UserRegistrationDS.UserProfileRow profileRow = profileTable.Rows[0] as UserWebService.UserRegistrationDS.UserProfileRow;
            return ConvertProfileRowToPsgProfile(profileRow);
        }
        else
            return new PsgProfile();
    }

    private PsgProfile ConvertProfileRowToPsgProfile(UserWebService.UserRegistrationDS.UserProfileRow profileRow)
    {
        string email = profileRow.IsEmailNull() ? string.Empty : profileRow.Email;
        string firstName = profileRow.IsFirstNameNull() ? string.Empty : profileRow.FirstName;
        string lastName = profileRow.IsLastNameNull() ? string.Empty : profileRow.LastName;
        string phone = profileRow.IsPhoneNull() ? string.Empty : profileRow.Phone;
        string country = profileRow.IsCountryNull() ? string.Empty : profileRow.Country;
        int regionId = profileRow.IsRegionIdNull() ? 0 : profileRow.RegionId;
        string companyName = profileRow.IsCompanyNameNull() ? string.Empty : profileRow.CompanyName;
        int companySize = profileRow.IsCompanySizeNull() ? 0 : profileRow.CompanySize;
        string jobTitle = profileRow.IsJobTitleNull() ? string.Empty : profileRow.JobTitle;
        string referenceSource = profileRow.IsReferenceSourceNull() ? string.Empty : profileRow.ReferenceSource;
        bool temporaryPasswordInUse = profileRow.IsTemporaryPasswordInUseNull() ? false : profileRow.TemporaryPasswordInUse;

        PsgProfile psgProfile = new PsgProfile(email, firstName, lastName, phone, country, regionId, companyName, companySize, jobTitle, referenceSource, temporaryPasswordInUse);
        //PsgProfile psgProfile = new PsgProfile(profileRow.Email, profileRow.FirstName, profileRow.LastName, profileRow.Phone, profileRow.Country, profileRow.RegionId, profileRow.CompanyName, profileRow.CompanySize, profileRow.JobTitle, profileRow.ReferenceSource, profileRow.TemporaryPasswordInUse);
        return psgProfile;
    }

    public MembershipCreateStatus UpdateProfileInformation(PsgProfile psgProfile)
    {
        MembershipCreateStatus status = (MembershipCreateStatus)userService.UpdateProfileInformation(psgProfile.Email, psgProfile.FirstName, psgProfile.LastName, psgProfile.PhoneNumber, psgProfile.Country, psgProfile.Region, psgProfile.CompanyName, psgProfile.CompanySize, psgProfile.JobTitle, psgProfile.Referrer);
        return status;
    }

    public virtual void OnUserValidated(UserValidatedEventArgs e)
    {
        UserValidated(this, e);
    }

    public string CreateTempPassword(string email)
    {
        return userService.CreateTempPassword(email);
    }

    public string SetandGetTempPassword(string email)
    {
        return userService.SetandGetTempPassword(email);
    }

    public void LogVisit(string email, string visitorID)
    {
        //TODO
        UserWebService.UserRegistrationDS.UserProfileDataTable table = userService.GetUserByEmail(email);
        if (table.Rows.Count > 0)
        {
            UserWebService.UserRegistrationDS.UserProfileRow row = table.Rows[table.Rows.Count - 1] as UserWebService.UserRegistrationDS.UserProfileRow;
            UserVisitService.UserVisitDS.UserVisitDataTable visitTable = new UserVisitService.UserVisitDS.UserVisitDataTable();
            visitTable.AddUserVisitRow(row.UserID, visitorID, DateTime.Now);
//            visitService.SubmitVisitInformation(visitTable);

            //TODO James Cooper: 04/18/2012  I commented this out because it was breaking and I don't know why.  This shouldn't break.
        }
    }

    public void send_gen_email(string email, string subject, string body)
    {
        userService.send_gen_email(email, subject, body);
    }
}

