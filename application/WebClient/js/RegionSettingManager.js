﻿RegionSettingManager = function() { }
RegionSettingManager.RegionComboBox = null;
RegionSettingManager.WelcomePageUrl = "";

RegionSettingManager.OnSelectedIndexChanged = function(ddlList) {
    CookieManager.CreateCookie("regionId", ddlList.value, 1000);
}

RegionSettingManager.NavigateToWelcomePage = function() {
    var value = CookieManager.ReadCookie("regionId");
    var date = new Date();
    var dateString = date.getMonth() + date.getDay() + date.getFullYear() + date.getHours() + date.getMinutes() + date.getSeconds() + date.getMilliseconds();
    document.getElementById("iFrame").src = RegionSettingManager.WelcomePageUrl + '?regionId=' + value + '&date=' + dateString;
    document.getElementById("hypDownload").href = "#";
    document.getElementById("hypDownload").target = "";
}

/*
// james removing for region removal
RegionSettingManager.SetSelectedIndex = function() {
var cookieValue = CookieManager.ReadCookie("regionId");
if (RegionSettingManager.RegionComboBox != null) {
var item = RegionSettingManager.RegionComboBox.FindItemByValue(cookieValue);
if (item != null)
item.Select();
else
CookieManager.CreateCookie("regionId", RegionSettingManager.RegionComboBox.Items[0].Value, 1000);
}
RegionSettingManager.NavigateToWelcomePage();
}
var globalItem;
RegionSettingManager.OnSelectedIndexChanged = function(item) {
globalItem = item;
CookieManager.CreateCookie("regionId", item.SelectedValue, 1000);
RegionSettingManager.NavigateToWelcomePage();
}

RegionSettingManager.SetSelectedIndex = function() {
    
var regionSelect = document.getElementById("regionSelect");
var regionSelectOptionsLength = regionSelect.options.length;
if (cookieValue != null) {
for (var i = 0; i < regionSelectOptionsLength; i++) {
if (regionSelect.options[i].value == cookieValue) {
regionSelect.selectedIndex = i;
break;
}
}
}
else {
var value = regionSelect.options[0].value;
CookieManager.CreateCookie("regionId", value, 1000);
}
RegionSettingManager.NavigateToWelcomePage();
}

RegionSettingManager.OnSelectedIndexChanged = function() {
var regionSelect = document.getElementById("regionSelect");
var regionSelectOptionsLength = regionSelect.options.length;
for (var i = 0; i < regionSelectOptionsLength; i++) {
if (regionSelect.options[i].selected == true) {
CookieManager.CreateCookie("regionId", regionSelect.options[i].value, 1000);
break;
}
}
RegionSettingManager.NavigateToWelcomePage();
}
*/

