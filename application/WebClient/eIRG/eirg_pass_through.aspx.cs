﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Text;

public partial class eIRG_eirg_pass_through : System.Web.UI.Page
{
    PsgMembershipProvider provider = (PsgMembershipProvider)Membership.Provider;
    bool isUsingTempPassword = false;
    string visitorId = String.Empty;
    PsgProfile prof;

    string pFName="";
    string pLName = "";
    string pEmail = "";
    int pReg = 0;
    string pOrg = "N/A";
    string pEIrg = "";
    string pCountry = "US";
    string pPhone = "5555555555";
    string pCompany = "N/A";
    int pCompanySize = 0;


    protected void Page_Load(object sender, EventArgs e)
    {
        /**************************
         *      TODO
         *      
         * -Need to log the Visit
         * -provider.eirg_reg usues dummy info for user id **DONE**
         * 
         * 
         * *************************/



        //Response.Write("<script type='text/javascript'>alert ('in Pass Through');</script>");
        /*
         *Validation method to make sure user is from eIRG
         *   if not send to login page
         *   
         *Get user info that is passed in 
         *      email
         *      first and last name
         *      region
         *      country
         *      org
         *  If info is not valid then send to registration/login
         *  
         *Check to see if user is already registered
         *  if they are log them in and send them on their way
         *      Need to set flags that the user is from eIRG
         *  if not then automatically register them and then log them in and on their way
         *      send email with temp password and login info after registering.  
        */

        //Check browser - if not IE7 or greater than show eror page
        if (!Check_Browser())
            Response.Redirect("eirg_error.aspx?er=14ie1_7");
        provider.UserValidated += new UserValidatedEventHandler(provider_UserValidated);
        set_parameters();
        if(validate_sender(pEIrg))
        {//Validated that this request came from eIRG - or someone is pretty smart and cracked the code. ARRGGH!

            //Need to check to see if the user is already registered.            
            prof = provider.GetUserProfile(pEmail);
            if (string.IsNullOrEmpty(prof.Email))
            {//Not registered yet. Need to create the user.
                create_user();                
            }
            //Log the user in
            log_user_in();            

        }else{
            //Response.Write("<script type='text/javascript'>alert ('Going to Login Page');</script>");
            Response.Redirect("~/Users/Login.aspx");
        }

    }

    protected void provider_UserValidated(object sender, UserValidatedEventArgs e)
    {
        //Level3Tag = "confirmation";
    }

    private void set_parameters()
    {
        pFName = Request.QueryString["fname"];
        pLName = Request.QueryString["lname"];
        pEmail = Request.QueryString["email"];
        pCountry = Request.QueryString["country"];
        pReg = Convert.ToInt32(Request.QueryString["region"]);
        pOrg = Request.QueryString["org"];
        pEIrg = Request.QueryString["eirg"];
        pPhone = Request.QueryString["p"];
        pCompany = Request.QueryString["cmp"];
        pCompanySize = Convert.ToInt32(Request.QueryString["cs"]);
    }

    private bool validate_sender(string req_send)
    {
        bool bRet = false;

        try
        {
            //This grabs the code that was sen from eIRG and strips out everything but the last 3 digits which is the 
            //Julian date doubled. So I grab that date and divide it by 2.
            int req_jday = ((Convert.ToInt32(req_send.Substring(req_send.Length - 3))) / 2);
            //Grab the current UTC Julian date
            int cur_jday = DateTime.UtcNow.DayOfYear;
            //I am just validating that the Julian date that is sent is within +/- 5 days to account for server discrepancies.
            if (cur_jday - 5 <= req_jday && cur_jday + 5 >= req_jday)
            {
                bRet = true;
            }
        }
        catch (Exception)
        {
            bRet = false;
        }

        return bRet;
    }

    private bool is_user_registered()
    {
        bool retVal = false;



        return retVal;
    }

    private void log_user_in()
    {       
        

        bool bt = provider.AutoValidateUser(prof.Email);
        //Check to see if the login was a success
        if (bt)
        {//Login succeeded.
            //Set the authentication cookie
            FormsAuthentication.SetAuthCookie(prof.Email, true);

            //Set the profile to the session so it can be used in the app.
            Session["LgnProfile"] = prof;
            // Add visitorID cookie value to Database...
            if (Request.Cookies["visitorid"] != null)
                provider.LogVisit(prof.Email, Request.Cookies["visitorid"].Value);

            //Session["JustRegistered"] = true;
            //send them to the main page.
            //Response.Write("<script language=JavaScript> alert('User Logged in - Now redirecting to main.aspx'); </script>");
            Response.Redirect("~/Main.aspx");
        }
        else
        {//Login failed. Let's send them to the login page
            //Response.Write("<script type='text/javascript'>alert ('Login Failed -Going to Login Page');</script>");
            Response.Redirect("~/Users/Login.aspx");
        }
    }

    private void create_user()
    {
        MembershipCreateStatus mcs;
        System.Object puk = new Object();
        //puk = Guid.NewGuid();
        provider.CreateUser(pEmail,"w&t3$t",pEmail,"","",true,puk,out mcs);        
        //Response.Write("<script language=JavaScript> alert('User Created'); </script>");

       prof = new PsgProfile(pEmail,
            pFName,
            pLName,
            pPhone,
            pCountry,
            pReg,
            pCompany,
            pCompanySize,
            "N/A",
            "eirg",
            true);

       provider.UpdateProfileInformation(prof);
        //Log the registration as from eIRG
        //TODO FIX THIS- No dummy data
       //provider.eirg_reg(pEmail, DateTime.Now);
        //Need to get a temp password
       string pw = provider.SetandGetTempPassword(pEmail);
        //Email the user
        string body = build_email(pw);
        provider.send_gen_email(pEmail, "Welcome to the HP Printing Sales Guide.", body); 
        
       //Response.Write("<script language=JavaScript> alert('User profile updated.'); </script>");
    }

    private string build_email(string pw)
    {
        StringBuilder em = new StringBuilder();
        em.Append("");
         em.Append("<table width='697' border='0' cellpadding='0' cellspacing='0'>");
        em.Append("<tr>");
         em.Append("<td width='697'><img src='http://www.hpprintingsalesguide.com/webapp/eIrg/images/HP_Black_Logo_R.jpg' width='697px'/></td>");
        em.Append("</tr>");
      em.Append("<tr>");
         em.Append("<td width='697'>&nbsp;</td>");
      em.Append("</tr>");
      em.Append("<tr>");
         em.Append("<td>");
             em.Append("<table width='697' border='0' cellpadding='0' cellspacing='0'>");
             em.Append("<tr>");
                 em.Append("<td width='106'></td>");
                 em.Append("<td width='591'><img src='http://www.hpprintingsalesguide.com/webapp/eIrg/images/HP_Welcom_Header_Email.jpg' width='591px'/></td>");
             em.Append("</tr>");
             em.Append("<tr>");
                 em.Append("<td width='106'></td>");
                 em.Append("<td width='591'>&nbsp;</td>");
             em.Append("</tr>");
             em.Append("<tr>");
                 em.Append("<td width='106'></td>");
                 em.Append("<td width='591'><img src='http://www.hpprintingsalesguide.com/webapp/eIrg/images/HP_Tag_Email.jpg' width='591px'/></td>");
             em.Append("</tr>");
             em.Append("</table>");
         em.Append("</td>");
      em.Append("</tr>");
      em.Append("<tr>");
      em.Append("<td width='697'>&nbsp;</td>");
      em.Append("</tr>");
      em.Append("<tr>");
         em.Append("<td>");
             em.Append("<table width='697' border='0' cellpadding='0' cellspacing='0'>");
             em.Append("<tr>");
                 em.Append("<td>");
                     em.Append("<table width='697' border='0' cellpadding='0' cellspacing='0'>");
                         em.Append("<tr>");
                             em.Append("<td width='106'>&nbsp;</td>");
                             em.Append("<td width='561'>");
                                 em.Append("<span style='font-family:Arial; font-size:12px; color:#2a99d3; font-weight:bold'>You’re registered!</span>");
                                 em.Append("<br /><br />");
                                 em.Append("<span style='font-family:Arial; font-size:12px;'>Now you have full access to the online HP Printing Sales Guide. Here you can find the latest information on HP Imaging and Printing products to help you increase attach rates and improve cross-sell and up-sell opportunities. Information is organized on pre-formatted slides so you can quickly create and customize your own presentations for customer sales opportunities.</span>");
                                 em.Append("<br /><br />");
                                 em.Append("<span style='font-family:Arial; font-size:12px;'>Use the dynamic navigational software to move easily through the guide to find Microsoft® PowerPoint® slides for product features, benefits, comparisons, technical specifications and customer case studies. Then select, mix and match slides to build your own customized presentation.</span>");
                                 em.Append("<br /><br />");
                                 em.Append("<span style='font-family:Arial; font-size:12px; font-weight:bold'>Download the desktop client.</span>");
                                 em.Append("<br />");
                                 em.Append("<span style='font-family:Arial; font-size:12px;'>You can also access the HP Printing Sales Guide directly from your desktop, without going through the eIRG. By downloading and installing the client application, you can select your own region and language to auto-download regional marketing assets. You can take them with you on the road, into customer settings or training environments.</span> ");
                                 em.Append("<a style='font-family:Arial; font-size:12px; color:#2a99d3; text-decoration:none;' href='http://ljb.deckmanager.com/client2011/publish.htm'>Download now</a>.");
                                 em.Append("<br /><br />");
                                 em.Append("<span style='font-family:Arial; font-size:12px; font-weight:bold'>Access the HP Printing Sales Guide from any computer.</span>");
                                 em.Append("<br />");
                                 em.Append("<span style='font-family:Arial; font-size:12px;'>Now that you’re a registered user, you can also access the guide from any computer just by typing <a style='font-family:Arial; font-size:14px; color:#2a99d3; text-decoration:none;' href='http://www.hpprintingsalesguide.com/webapp/main.aspx'>http://www.hpprintingsalesguide.com/webapp/main.aspx</a> into an Internet Explorer® browser. Again without going through the eIRG. Bookmark it for future access.</span>");
                                 em.Append("<br /><br />");
                                 em.Append("<span style='font-family:Arial; font-size:12px; font-weight:bold'>Change your password.</span>");
                                 em.Append("<br />");
                                 em.Append("<span style='font-family:Arial; font-size:12px;'>We've registered you on the HP Printing Sales Guide with the temporary password ");
                                 em.Append(pw);
                                 em.Append(". For increased security, please create your own password <a style='font-family:Arial; font-size:14px; color:#2a99d3; text-decoration:none;' href='http://www.hpprintingsalesguide.com/webapp/Users/ResetPassword.aspx'>here</a>.</span>");
                                 em.Append("<br /><br /><br />");
                                 em.Append("<span style='font-family:Arial; font-size:12px;'>If you are having trouble reading this e-mail, click <a style='font-family:Arial; font-size:14px; color:#2a99d3; text-decoration:none;' href='mailto:support@hpprintingsalesguide.com'>here</a>.</span>");
                                 em.Append("<br/>");
                                 em.Append("<span style='font-family:Arial; font-size:12px;'>If you experience any problems accessing the HP Printing Sales Guide, please e-mail technical support at <a style='font-family:Arial; font-size:14px; color:#2a99d3; text-decoration:none;' href='mailto:support@hpprintingsalesguide.com'>support@hpprintingsalesguide.com</a>.</span>");
                                 em.Append("<br /><br />");
                                 em.Append("<span style='font-family:Arial; font-size:8px;'>Microsoft, Internet Explorer and PowerPoint are U.S. registered trademarks of Microsoft Corporation.</span>");
                                 em.Append("<br /><br />");
                                 em.Append("<span style='font-family:Arial; font-size:8px;'>© Copyright 2011 Hewlett-Packard Development Company, L.P. The information contained herein is subject to change without notice. The only warranties for HP products and services are set forth in the express limited warranty statements accompanying such products and services. Nothing herein should be construed as constituting an additional warranty. HP shall not be liable for technical or editorial errors or omissions contained herein. For more information on our legal policy please click <a style='font-family:Arial; font-size:8px; color:#2a99d3; text-decoration:none;' href='http://welcome.hp.com/country/us/en/privacy.html'>here.</span>");
                             em.Append("</td>");
                             em.Append("<td width='30'>&nbsp;</td>");
                         em.Append("</tr>");
                     em.Append("</table>");
                 em.Append("</td>");
             em.Append("</tr>");
             em.Append("</table>");
         em.Append("</td>");
      em.Append("</tr>");
      em.Append("</table>");











        //em.Append("You’re registered!<br /><br />Now you have full access to the online HP Printing Sales Guide. Here you can find the latest information on HP Imaging and Printing products to help you increase attach rates and improve cross-sell and up-sell opportunities. Information is organized on pre-formatted slides so you can quickly create and customize your own presentations for customer sales opportunities.");
        //em.Append("<br /><br />Use the dynamic navigational software to move easily through the guide to find Microsoft® PowerPoint® slides for product features, benefits, comparisons, technical specifications and customer case studies. Then select, mix and match slides to build your own customized presentation.");
        //em.Append("<br /><br /><b>Download the desktop client.</b><br />");
        //em.Append("You can also access the HP Printing Sales Guide directly from your desktop, without going through the eIRG. By downloading and installing the client application, you can select your own region and language to auto-download regional marketing assets. You can take them with you on the road, into customer settings or training environments. <a href='http://ljb.deckmanager.com/client2011/publish.htm'>Download now</a>.");
        //em.Append("<br /><br /><b>Access the HP Printing Sales Guide from any computer.</b><br />");
        //em.Append("Now that you’re a registered user, you can also access the guide from any computer just by typing <a href='http://ljb.deckmanager.com/WebApp'>http://ljb.deckmanager.com/WebApp</a> into an Internet Explorer® browser. Again without going through the eIRG. Bookmark it for future access.");
        //em.Append("<br /><br /><b>Change your password.</b> <br />");
        //em.Append("We’ve registered you on the HP Printing Sales Guide with the temporary password ");
        //em.Append(pw);
        //em.Append(". For increased security, please create your own password <a href='http://localhost:2714/WebClient/Users/ResetPassword.aspx'>here</a>.");
        //em.Append("<br /><br /><br />If you’re having trouble reading this e-mail, click <a href='mailto:support@hpprintingsalesguide.com'>here</a>.<br/>");
        //em.Append("If you experience any problems accessing the HP Printing Sales Guide, please e-mail technical support at <a href='mailto:support@hpprintingsalesguide.com'>support@hpprintingsalesguide.com</a>.");
        //em.Append("<br /><br />Microsoft, Internet Explorer and PowerPoint are registered trademarks of Microsoft Corporation.");

        return em.ToString();
    }

    private bool Check_Browser()
    {
        bool retVal = false;

        if (!(IsPostBack))
        {
            if (Request.Browser.Browser == "IE")
            {
                if (Request.Browser.MajorVersion > 6)
                {
                    retVal =true;
                }                
            }
        }

        return retVal;
    }
    
}