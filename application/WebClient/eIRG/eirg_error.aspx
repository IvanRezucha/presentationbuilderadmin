﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="eirg_error.aspx.cs" Inherits="eIRG_eirg_error" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
        <head id="Head2" runat="server">
            <title>HP Printing Sales Guide</title> 
                <style type="text/css">
        #header
        {
            height: 69px;
            background-image: url(images/banner.gif);
            background-repeat: no-repeat;
        }
        
</style>
        </head>
        <body>
            <form id="form2" runat="server">

            <div id="header">
            
            </div>
                <table width="800px" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td><br />&nbsp;<br />&nbsp;</td>
                        <td>
                            <p><br /><strong>Error!</strong></p>
                            <p><asp:Literal ID="lError" runat="server"></asp:Literal></p>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><br /><br />&nbsp;</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td> <span style="font-size:8pt; font-family:Arial;">                            
                            <a href="http://welcome.hp.com/country/us/en/privacy.html">Privacy statement</a>&nbsp;&nbsp;|&nbsp;&nbsp;Using this site means you accept <a href="http://welcome.hp.com/country/us/en/termsofuse.html">its terms</a>.&nbsp;&nbsp;|&nbsp;&nbsp;<a href="mailto:support@hpprintingsalesguide.com">HP Sales Guide Support</a><br />
                            © Copyright 2010 Hewlett-Packard Development Company, L.P. The information contained herein is subject to change without notice.
                            </span></td>
                    </tr>
                </table>               
        </form>
    </body>
</html>

