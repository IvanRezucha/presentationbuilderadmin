﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class eIRG_eirg_error : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        switch (Request.QueryString["er"].ToLower())
        {
            case "14ie1_7":
                lError.Text = "Internet Explorer 7 or greater is required to use HP Printing Sales Guide.<br/><br/><a href='http://ljb.deckmanager.com/webapp/'>Login Here</a>";
                break;
            default:
                break;
        }
    }
}