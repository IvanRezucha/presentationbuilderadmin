using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Authenticate : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string refer = "null";
        if (Request.UrlReferrer != null)
        {
            refer = Request.UrlReferrer.ToString();
        }
        string referrer = String.Format("{0}: {1}{2}", DateTime.Now.ToString(), refer, System.Environment.NewLine);

        System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
        smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
        smtp.Host = "localhost";
        smtp.UseDefaultCredentials = true;


        ReferrerChecker rc = new ReferrerChecker();

        ReferrerChecker.ReferrerCheckResult rcResult = rc.IsReferrerInList(Request.UrlReferrer, "allowedReferrers");

        switch (rcResult)
        {
            case ReferrerChecker.ReferrerCheckResult.InList:
                smtp.Send("curtis@fcbcmail.org", "curtis.glesmann@wirestone.com", "LJB Web-InList", referrer);
                Session["Authenticated"] = "true";
                Response.Redirect("Main.aspx");
                break;
            case ReferrerChecker.ReferrerCheckResult.NotInList:
                smtp.Send("curtis@fcbcmail.org", "curtis.glesmann@wirestone.com", "LJB Web-NotInList", referrer);
                Response.Redirect("AccessDenied.aspx");
                break;
            case ReferrerChecker.ReferrerCheckResult.NullReferrer:
                smtp.Send("curtis@fcbcmail.org", "curtis.glesmann@wirestone.com", "LJB Web-NullReferrer", referrer);
                Response.Redirect("AccessDenied.aspx");
                break;
            case ReferrerChecker.ReferrerCheckResult.NoReferrerListKeyFound:
                smtp.Send("curtis@fcbcmail.org", "curtis.glesmann@wirestone.com", "LJB Web-NoReferrerListKeyFound", referrer);
                Response.Redirect("AccessDenied.aspx");
                break;
            case ReferrerChecker.ReferrerCheckResult.BlankReferrerList:
                smtp.Send("curtis@fcbcmail.org", "curtis.glesmann@wirestone.com", "LJB Web-BlankReferrerList", referrer);
                Response.Redirect("AccessDenied.aspx");
                break;
            default:
                smtp.Send("curtis@fcbcmail.org", "curtis.glesmann@wirestone.com", "LJB Web-default", referrer);
                Response.Redirect("AccessDenied.aspx");
                break;
        }
    }
}
