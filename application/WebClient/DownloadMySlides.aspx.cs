﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using PSG.Core.Files;

public partial class DownloadMySlides : System.Web.UI.Page
{

	private string GetSlidePath(string url)
	{
		//string iFrameSrc = iFrame.Attributes["src"].ToString();
		string[] splitter = { @"/" };
		//string[] urlParts = iFrameSrc.Split(splitter, StringSplitOptions.None);
		string[] urlParts = url.Split(splitter, StringSplitOptions.None);
		string fileName = urlParts[urlParts.Length - 1];

		if (fileName.EndsWith(".ppt"))
			fileName.Replace(".ppt", ".pot");

		FileLocation location = new FileLocation(fileName);
		string slideDirectory = Path.Combine(ConfigurationManager.AppSettings["SlidePhysicalPath"], location.Directory);
		string slideFullPath = Path.Combine(slideDirectory, fileName);

		return slideFullPath;
	}

	protected void Page_Load(object sender, EventArgs e)
	{
		if (Session["MySlides"] != null)
		{
			try
			{
				Aspose.Slides.License lic = new Aspose.Slides.License();
				lic.SetLicense(Server.MapPath("Aspose.Slides.lic"));
				Aspose.Slides.Presentation mySlides = new Aspose.Slides.Presentation();
				MySlidesDS.MySlidesDataTable dtMySlides = (MySlidesDS.MySlidesDataTable)Session["MySlides"];
				DataTable dtTemp = dtMySlides.DefaultView.ToTable();
				dtMySlides.DefaultView.Sort = "SortOrder ASC";

				foreach (DataRow dr in dtTemp.Rows)
				{
					System.Collections.SortedList sortedList = new System.Collections.SortedList();
					//string slideFilePath = dr["SlideURL"].ToString().Replace(ConfigurationManager.AppSettings["SlideLocation"], ConfigurationManager.AppSettings["SlidePhysicalPath"]);
					string slideFilePath = GetSlidePath(dr["SlideURL"].ToString());
					if (File.Exists(slideFilePath))
					{
						FileStream fileStream = new FileStream(slideFilePath, FileMode.Open, FileAccess.Read);
						Aspose.Slides.Presentation importPres = new Aspose.Slides.Presentation(fileStream);
						Aspose.Slides.Slide slide = importPres.GetSlideByPosition(1);
						importPres.CloneSlide(slide, mySlides.Slides.LastSlidePosition + 1, mySlides, sortedList);
					}
				}

				//get rid of the blank first slide
				mySlides.Slides.RemoveAt(0);

				//get a temp file name
				string tempFile = Path.Combine(ConfigurationManager.AppSettings["TempDirectory"].ToString(), Path.GetRandomFileName());
				tempFile = Path.Combine(Path.GetDirectoryName(tempFile), Path.GetFileNameWithoutExtension(tempFile) + ".ppt");
				mySlides.Write(tempFile);

				//download it
				System.IO.FileInfo file = new System.IO.FileInfo(tempFile);

				Response.Clear();
				Response.AddHeader("Content-Disposition", "attachment; filename=" + Path.GetFileNameWithoutExtension(file.Name) + ".ppt");
				Response.AddHeader("Content-Length", file.Length.ToString());
				Response.ContentType = "application/octet-stream";
				Response.WriteFile(file.FullName);
				Response.End();
			}
			catch (Exception ex)
			{
				//probably couldn't find the file
				Response.Write("Exception: " + ex.ToString());
			}

		}
		else
		{
			Response.Write("No MySlides found in Session.");
		}

	}
}
