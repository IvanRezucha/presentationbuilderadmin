﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login"
    MasterPageFile="~/Users/UserPages.master" %>

<asp:Content runat="server" ContentPlaceHolderID="placeholderTitle">
    HP Printing Sales Guide
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="headPlaceholder">
    <style type="text/css">
        #rightColumn
        {
            float: left;
        }
        #leftColumn
        {
            float: left;
            margin-right: 5px;
        }
        #spacer
        {
            clear: left;
            height: 40px;
        }
    </style>
    
    <script language="JavaScript">
    <!--
        var levels = ["login"];
        var slideid = "";
    // -->
    </script>


</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="bodyPlaceholder">
    <div id="leftColumn">
        <div style="width: 303px">
            <table>
                <tr>
                    <td style="font-weight: bold;">
                        The HP Printing Sales Guide is an easy-to-use, centralized, automated tool that
                        provides one access point for everything you need to sell HP printing solutions.
                        <br />
                        <br />
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold;">
                        Instant access from the Web
                    </td>
                </tr>
                <tr>
                    <td>
                        The Web Version of the HP Printing Sales Guide can be accessed easily and instantly
                        from your browser when you are connected to the internet. No need to install on
                        your laptop or worry with downloads - great tool for the sales floor! The web version
                        features the same slide content as the Portable version, but does not support all
                        hyperlinking or allow you to save your slide presentations within the Guide. You
                        can still download slides to your computer.  
                        <br />
                        <br />
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold;">
                        Support
                    </td>
                </tr>
                <tr>
                    <td>
                        Content Support:&nbsp;<a href="mailto:content@hpprintingsalesguide.com" onclick="ntptEventTag( 'ev=email link&mailto_lk=' + escape( this.href ).toLowerCase() );">content@hpprintingsalesguide.com</a><br />
                        Technical&nbsp;Support:&nbsp;<a href="mailto:support@hpprintingsalesguide.com" onclick="ntptEventTag( 'ev=email link&mailto_lk=' + escape( this.href ).toLowerCase() );">support@hpprintingsalesguide.com</a><br />
                        <br />
                        <br />
                    </td>
                </tr>
                <!--
                <tr>
                    <td style="font-weight: bold; font-size: 14px">
                        REGISTRATION NOW REQUIRED
                    </td>
                </tr>
                <tr>
                    <td>
                        Registration takes only a minute and consists of a few questions. <b>You will need
                        the password/passcode. To obtain, please email <a href="mailto:support@hpprintingsalesguide.com">
                            support@hpprintingsalesguide.com</a></b>
                    </td>
                </tr> -->
            </table>
        </div>
    </div>
    <div id="rightColumn" style="border: 1px solid #E6E2D8">
        <div style="background-color: #F9F9F9; padding: 10px; width: 255px;">
            <asp:LoginView ID="LoginView1" runat="server">
                <AnonymousTemplate>
                    <asp:Login ID="Login1" runat="server" OnLoggedIn="OnLoggedIn">
                        <LayoutTemplate>
                            <table style="width: 235px">
                                <tr>
                                    <td>
                                        <span style="font-weight: bold; font-size: 14px">Sign in to the HP Printing Sales Guide!</span><br />
                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Email:<br />
                                        <asp:TextBox ID="UserName" runat="server" Style="width: 225px"></asp:TextBox><br />
                                        <asp:RequiredFieldValidator Display="Dynamic" ID="UserNameRequired" runat="server"
                                            ControlToValidate="UserName" Text="Email is required."></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator Display="Dynamic" ID="RegularExpressionValidator1"
                                            runat="server" ControlToValidate="UserName" Text="Email is not a valid email address."
                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Password:<br />
                                        <asp:TextBox ID="Password" runat="server" TextMode="Password" Style="width: 225px"></asp:TextBox><br />
                                        <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password"
                                            Text="Password is required."></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="FailureText" runat="server" ForeColor="Red"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="RememberMe" runat="server" Text="Keep me signed in" Font-Size="13px"
                                            Font-Bold="true"></asp:CheckBox>
                                    </td>
                                </tr>
                            </table>
                            <table style="width: 235px">
                                <tr>
                                    <td style="text-align: right">
                                        <asp:Button ID="Login" CommandName="Login" runat="server" Text="Sign In"></asp:Button>
                                    </td>
                                </tr>
                            </table>
                            <table style="width: 235px">
                                <tr>
                                    <td>
                                        <a href="GetPassword.aspx">Forgot your password?</a>
                                        <br />
                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <a href="GetPassword.aspx">I've Already Registered, I Need a Password</a>
                                        <br />
                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="border-top: 1px solid #E6E2D8">
                                        <br />
                                        <span style="font-weight: bold">Not signed up?</span><br />
                                        Sign up to take advantage of all the resources the sales guide has to offer.
                                        <br />
                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: right; font-size: 14px;">
                                        <a href="NewAccount.aspx">Sign Up</a>
                                    </td>
                                </tr>
                            </table>
                        </LayoutTemplate>
                    </asp:Login>
                </AnonymousTemplate>
                <LoggedInTemplate>
                    You are logged in as
                    <asp:LoginName ID="LoginName1" runat="server" />
                    <br />
                   <a href="../Main.aspx">Access the Printing Sales Guide</a>
                </LoggedInTemplate>
            </asp:LoginView>
            <%-- 
        <asp:LoginStatus ID="LoginStatus1" runat="server" />
        <br />
        Not signed up yet?<a href="Register.aspx">Click here to register!</a>
        --%>
        </div>
    </div>

    <script language="javascript" type="text/javascript" src="../js/analytics.js"></script>
    <script language="javascript" type="text/javascript" src="../js/ntpagetag.js"></script>

</asp:Content>
