﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

public partial class Users_GetPassword : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void UpdateView(string result)
    {
        if (result.Equals("PasswordSent"))
        {
            mainPanel.Visible = false;
            successPanel.Visible = true;
        }
        else if (result.Equals("UserNotFound"))
        {
            lblResult.Text = "We were unable to find the email address provided.<br />";
        }
        else if (result.Equals("UserNotCreatedYet"))
        {
            lblResult.Text = "There is no account created for this email.  Please create an account and password to proceed.";
        }
        else
        {
            lblResult.Text = "Unspecified Error.  Please try creating an account.";
            //lblResult.Text = "Too bad sucka.  No login for you!";
        }
    }

    protected void GetPassword_Click(object sender, EventArgs e)
    {
        PsgMembershipProvider provider = Membership.Provider as PsgMembershipProvider;
        string result = provider.CreateTempPassword(UserName.Text);

        UpdateView(result);
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Users/Login.aspx");
    }

    protected void btnMainPanelBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Users/Login.aspx");
    }
}
