﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

public partial class Users_ResetPassword : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        UserName.Text = Page.User.Identity.Name;
        FailureText.Text = "";
        if (!Page.IsPostBack)
        {
            if (Request.QueryString["rpr"] != null)
            {
                tempPasswordLabel.Visible = true;
            }
        }
        Level3Tag = "home";
    }

    public string Level3Tag
    {
        get;
        set;
    }

    protected void ChangePassword_Click(object sender, EventArgs e)
    {
        PsgMembershipProvider provider = Membership.Provider as PsgMembershipProvider;
        bool success = provider.ChangePassword(UserName.Text, CurrentPassword.Text, NewPassword.Text);
        UpdateView(success);
    }

    protected void Continue_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Main.aspx");
    }

    protected void Cancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Main.aspx");
    }

    protected void UpdateView(bool success)
    {
        if (success)
        {
            Level3Tag = "confirmation";
            successTemplate.Visible = true;
            changePasswordTemplate.Visible = false;
        }
        else
        {
            FailureText.Text = "The current password is incorrect or the new password is invalid."; 
        }

    }
}
