﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Text;

public partial class NewAccount : System.Web.UI.Page
{
    PsgMembershipProvider provider = (PsgMembershipProvider)Membership.Provider;

    protected void Page_Load(object sender, EventArgs e)
    {
        provider.UserValidated += new UserValidatedEventHandler(provider_UserValidated);
        Level3Tag = "form";

        Literal literalBody = (Literal)Master.FindControl("bodyLiteral");
        literalBody.Text = "onunload='javascript:formAbandon();'";

        BottomScript = "var formName = 'form:registration';";
    }

    protected override void Render(HtmlTextWriter writer)
    {
        base.Render(writer);

        if (Session["newRegistration"] != null && (bool)(Session["newRegistration"]))
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<script type='text/javascript'>strSubmit=true; setSegment('registered', 'registered'); ");
            sb.Append("setSegment('country', '" + Session["Country"].ToString() + "'); ");
            sb.Append("setSegment('region', '" + Session["Region"].ToString() + "'); ");
            sb.Append("setSegment('role', '" + Session["Role"].ToString() + "'); ");
            sb.Append("ntptEventTag('ev=confirmation:registration');</script>");

            writer.WriteLine(sb.ToString());
            Session["newRegistration"] = false;
        }
    }

    public string Level3Tag
    {
        get;
        set;
    }

    public string BottomScript { get; set; }

    protected void provider_UserValidated(object sender, UserValidatedEventArgs e)
    {
        Level3Tag = "confirmation";
    }
    protected void CreateUserWizard1_CreatedUser(object sender, EventArgs e)
    {
        TextBox EmailAddress = (TextBox)CreateUserWizardStep1.ContentTemplateContainer.FindControl("UserName");
        Users_ProfileInformation profileInfo = (Users_ProfileInformation)CreateUserWizardStep1.ContentTemplateContainer.FindControl("ProfileInformationUserControl");

        PsgProfile psgProfile = new PsgProfile(EmailAddress.Text,
            profileInfo.FirstName,
            profileInfo.LastName,
            profileInfo.PhoneNumber,
            profileInfo.Country,
            profileInfo.Region,
            profileInfo.CompanyName,
            profileInfo.CompanySize,
            profileInfo.JobTitle,
            profileInfo.Referrer,
            false);
        
        PsgMembershipProvider provider = Membership.Provider as PsgMembershipProvider;
        provider.UpdateProfileInformation(psgProfile);

        // Add visitorID cookie value to Database...
        if (Request.Cookies["visitorid"] != null)
            provider.LogVisit(EmailAddress.Text, Request.Cookies["visitorid"].Value);
        
        // NetInsight Tracking
        Session["newRegistration"] = true;
        BottomScript = "";
        Session["Country"] = profileInfo.Country;
        Session["Region"] = profileInfo.RegionText;
        Session["Role"] = profileInfo.JobTitle;

    }
    protected void CreateUserWizard1_CreateUserError(object sender, CreateUserErrorEventArgs e)
    {
        Level3Tag = "form";
        if (e.CreateUserError.Equals(MembershipCreateStatus.UserRejected))
        {
            Label lblErrorMessage = (Label)CreateUserWizardStep1.ContentTemplateContainer.FindControl("lblErrorMessage");
            lblErrorMessage.Text = "The passcode you entered was not correct. Please try again.";
        }
        if (e.CreateUserError.Equals(MembershipCreateStatus.DuplicateEmail))
        {
            Label lblErrorMessage = (Label)CreateUserWizardStep1.ContentTemplateContainer.FindControl("lblErrorMessage");
            lblErrorMessage.Text = "An account has already been created with this account.";
        }
    }

    protected void btnContinue_Click(object sender, EventArgs e)
    {
        Session["JustRegistered"] = true;
        Response.Redirect("~/Main.aspx");
    }
}
