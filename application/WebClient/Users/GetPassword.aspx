﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="GetPassword.aspx.cs" Inherits="Users_GetPassword"
    MasterPageFile="~/Users/UserPages.master" %>

<asp:Content runat="server" ContentPlaceHolderID="headPlaceholder">
    <title>HP Printing Sales Guide - Get Password</title>

    <script language="JavaScript">
    <!--
        var levels = ["forgot your password"];
        var slideid = "";
    // -->
    </script>

</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="bodyPlaceholder">
    <div class="containers">
        <span style="font-weight: bold; font-size: 14px">Get New Password</span>
    </div>
    <asp:Panel runat="server" ID="mainPanel" Visible="true">
        <div class="containers">
            <asp:Label runat="server" ID="lblResult" ForeColor="Red"></asp:Label><br />
            <span>Please enter your e-mail address and a temporary password will be e-mailed to
                you. You will be prompted to reset this password the next time you login.</span>
            <table>
                <tr>
                    <td class="tableLabels">
                        Email:
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="UserName" Style="width: 225px;" ValidationGroup="ProfileValidation"
                            CssClass="textboxes" />
                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator11" Display="Dynamic"
                            ControlToValidate="UserName" ErrorMessage="*" ValidationGroup="ProfileValidation" /><br />
                        <asp:RegularExpressionValidator Display="Dynamic" ID="RegularExpressionValidator1"
                            runat="server" ControlToValidate="UserName" ValidationGroup="ProfileValidation"
                            ErrorMessage="Email address is not valid" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                    </td>
                </tr>
            </table>
        </div>
        <div class="containers">
            <table border="0" cellspacing="5" style="width: 100%; height: 100%;">
                <tr align="left">
                    <td align="left" colspan="0">
                        <asp:Button ID="GetPassword" runat="server" BackColor="#FFFBFF" BorderColor="#CCCCCC"
                            BorderStyle="Solid" BorderWidth="1px" Font-Names="Verdana" ForeColor="#284775"
                            Text="Get New Password" ValidationGroup="ProfileValidation" OnClick="GetPassword_Click" />
                        <asp:Button ID="btnMainPanelBack" runat="server" BackColor="#FFFBFF" BorderColor="#CCCCCC"
                            BorderStyle="Solid" BorderWidth="1px" Font-Names="Verdana" ForeColor="#284775"
                            Text="Back to login" OnClick="btnMainPanelBack_Click" />
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    <asp:Panel runat="server" ID="successPanel" Visible="false">
        <div class="containers">
            <span>Your temporary password has been e-mailed to you.</span>
        </div>
        <div class="containers">
            <table border="0" cellspacing="5" style="width: 100%; height: 100%;">
                <tr align="left">
                    <td align="left" colspan="0">
                        <asp:Button ID="btnBack" runat="server" BackColor="#FFFBFF" BorderColor="#CCCCCC"
                            BorderStyle="Solid" BorderWidth="1px" Font-Names="Verdana" ForeColor="#284775"
                            Text="Back to Login" OnClick="btnBack_Click" />
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>

    <script language="javascript" type="text/javascript" src="../js/analytics.js"></script>
    <script language="javascript" type="text/javascript" src="../js/ntpagetag.js"></script>
</asp:Content>
