﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

public partial class Login : System.Web.UI.Page
{
    PsgMembershipProvider provider = (PsgMembershipProvider)Membership.Provider;
    bool isUsingTempPassword = false;
    string visitorId = String.Empty;
    PsgProfile prof;

    protected void Page_Load(object sender, EventArgs e)
    {
        HideLoginStatus();
        provider.UserValidated += new UserValidatedEventHandler(provider_UserValidated);
        try
        {
            visitorId = HttpContext.Current.Request.Cookies["visitorid"].Value;
        }
        catch (Exception ex)
        {
        }
    }

    private void provider_UserValidated(object sender, UserValidatedEventArgs e)
    {
        prof = e.Profile;
        if (e.Profile.TemporaryPasswordInUse)
            isUsingTempPassword = true;
    }
    public void OnLoggedIn(object src, EventArgs e)
    {
        Session["LgnProfile"] = prof;
        if (visitorId != null)
            provider.LogVisit(prof.Email, visitorId);
        if (isUsingTempPassword)
            Page.Response.Redirect("~/Users/ResetPassword.aspx?rpr=t");
    }
    public void HideLoginStatus()
    {
        LoginStatus loginStatus = (LoginStatus)this.Master.FindControl("LoginStatus1");
        if (loginStatus != null)
            loginStatus.Visible = false;
    }

}
