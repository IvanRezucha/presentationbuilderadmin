﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MyAccount.aspx.cs" Inherits="MyAccount"
    MasterPageFile="~/Users/UserPages.master" %>

<%@ Register TagPrefix="psg" TagName="ProfileInformation" Src="~/Users/ProfileInformation.ascx" %>
<asp:Content runat="server" ContentPlaceHolderID="headPlaceholder">
    <script language="JavaScript">
    <!--
        var levels = ["my account", "<%=Level3Tag%>"];
        var slideid = "";
    // -->

        function ClearFormName() {
            formName = '';
        }
    </script>
    <title>HP Printing Sales Guide - My Profile</title>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="bodyPlaceholder">
    <div class="containers">
        <span style="font-weight: bold; font-size: 14px">Your Profile Information -- </span><span>
        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Users/ResetPassword.aspx">Click here to reset your password.</asp:HyperLink></span><br />
        <asp:Label runat="server" ID="lblSuccess" ForeColor="Green" Text="Your profile has been updated." Visible="false" />
        <asp:Label runat="server" ID="lblFailure" ForeColor="Red" Text="There was an error saving your profile." Visible="false" />
        <asp:Label runat="server" ID="lblProfileAlert" ForeColor="Red" Text="New Profile Information is required.  Please complete this form." Visible="false" />
    </div>
    <div class="containers">
        <psg:ProfileInformation runat="server" ID="profileInformation" />
    </div>
    <div class="containers">
        <table border="0" cellspacing="5" style="width: 100%; height: 100%;">
            <tr align="left">
                <td align="left" colspan="0">
                    <asp:Button ID="StepNextButton" runat="server" BackColor="#FFFBFF" BorderColor="#CCCCCC"
                        BorderStyle="Solid" BorderWidth="1px" OnClick="btnUpdate_Click" Font-Names="Verdana"
                        ForeColor="#284775" Text="Update Profile" ValidationGroup="ProfileValidation" OnClientClick="ClearFormName();" />
                    <asp:Button ID="btnBack" runat="server" BackColor="#FFFBFF" BorderColor="#CCCCCC"
                        BorderStyle="Solid" BorderWidth="1px" OnClick="btnBack_Click" Font-Names="Verdana"
                        ForeColor="#284775" Text="Back to viewer" ValidationGroup="ProfileValidation" />
                </td>
            </tr>
        </table>
    </div>

    <script language="javascript" type="text/javascript" src="../js/analytics.js"></script>
    <script language="javascript" type="text/javascript" src="../js/ntpagetag.js"></script>
    <script language="javascript" type="text/javascript">
        function formAbandon() {
            if (typeof formName != "undefined" && formName != '') {
                if (!strSubmit) ntptEventTag('ev=form abandoned&ed=' + formName.toLowerCase() + '&last_field=' + escape(lastField.toLowerCase()));
            }
        }
    </script>
    <script type="text/javascript">
        <%=BottomScript%>
    </script>

</asp:Content>
