﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Configuration;


public partial class Users_ProfileInformation : System.Web.UI.UserControl
{
    UserWebService.UserRegistrationDS.CountriesDataTable countriesTable;
    UserWebService.UserRegistrationDS.JobTitlesDataTable jobTitlesTable;

    protected void Page_Init(object sender, EventArgs e)
    {
        UserWebService.UserRegistrationService userService = new UserWebService.UserRegistrationService();
        countriesTable = userService.GetCountries();
        jobTitlesTable = userService.GetJobTitles();

        if (!Page.IsPostBack)
        {
            PopulateRegionDropDown();
            PopulateCountryDropDown();
            PopulateJobTitlesDropDown();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void PopulateRegionDropDown()
    {
        wsWebAppData.SlideLibrary dsRegions = new wsWebAppData.SlideLibrary();
        dsRegions.ReadXml(String.Format("{0}RegionLanguages.xml", WebConfigurationManager.AppSettings["DataURI"].ToString()));

        ddlRegion.Items.Add(new ListItem("Please Select...", ""));
        foreach (wsWebAppData.SlideLibrary.RegionRow region in dsRegions.Region.Rows)
        {
            if (!region.RegionName.Equals("Common"))
                ddlRegion.Items.Add(new ListItem(region.RegionName, region.RegionID.ToString()));
        }
    }

    protected void PopulateCountryDropDown()
    {
        ddlCountries.Items.Add(new ListItem("Please Select...", ""));
        foreach (UserWebService.UserRegistrationDS.CountriesRow row in countriesTable)
            ddlCountries.Items.Add(new ListItem(row.Name, row.Code));
    }
    protected void PopulateJobTitlesDropDown()
    {
        ddlJobTitles.Items.Add(new ListItem("Please Select...", ""));
        foreach (UserWebService.UserRegistrationDS.JobTitlesRow row in jobTitlesTable)
            ddlJobTitles.Items.Add(new ListItem(row.Name, row.Code));
    }

    public void Populate(PsgProfile psgProfile)
    {
        this.FirstName = psgProfile.FirstName;
        this.LastName = psgProfile.LastName;
        this.PhoneNumber = psgProfile.PhoneNumber;
        this.Country = psgProfile.Country;
        this.Region = psgProfile.Region;
        this.CompanyName = psgProfile.CompanyName;
        this.CompanySize = psgProfile.CompanySize;
        this.JobTitle = psgProfile.JobTitle;
        this.Referrer = psgProfile.Referrer;
    }

    public string FirstName
    {
        get
        {
            return txtFirstName.Text;
        }
        set
        {
            txtFirstName.Text = value;
        }
    }
    public string LastName
    {
        get
        {
            return txtLastName.Text;
        }
        set
        {
            txtLastName.Text = value;
        }
    }
    public string PhoneNumber
    {
        get
        {
            return txtPhoneNumber.Text;
        }
        set
        {
            txtPhoneNumber.Text = value;
        }
    }
    public string Country
    {
        get
        {
            return ddlCountries.SelectedValue;
        }
        set
        {
            int index = 0;
            foreach (ListItem item in ddlCountries.Items)
            {
                if (item.Value == value)
                {
                    ddlCountries.SelectedIndex = index;
                    break;
                }
                index++;
            }
        }
    }
    public int Region
    {
        get
        {
            return Convert.ToInt32(ddlRegion.SelectedValue.ToString());
        }
        set
        {
            int index = 0;
            foreach (ListItem item in ddlRegion.Items)
            {
                if (item.Value == value.ToString())
                {
                    ddlRegion.SelectedIndex = index;
                    break;
                }
                index++;
            }
        }
    }
    public string CompanyName
    {
        get
        {
            return txtCompanyName.Text;
        }
        set
        {
            txtCompanyName.Text = value;
        }
    }
    public int CompanySize
    {
        get
        {
            int size = 0;
            Int32.TryParse(txtCompanySize.Text, out size);
            return size;
        }
        set
        {
            txtCompanySize.Text = value.ToString();
        }
    }
    public string JobTitle
    {
        get
        {
            return ddlJobTitles.SelectedValue;
        }
        set
        {
            int index = 0;
            foreach (ListItem item in ddlJobTitles.Items)
            {
                if (item.Value == value)
                {
                    ddlJobTitles.SelectedIndex = index;
                    break;
                }
                index++;
            }
        }
    }
    public string Referrer
    {
        get
        {
            return txtReferrer.Text;
        }
        set
        {
            txtReferrer.Text = value;
        }
    }

    public string RegionText
    {
        get
        {
            return ddlRegion.SelectedItem.Text;
        }
    }
}
