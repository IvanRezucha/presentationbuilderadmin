﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Text;

public partial class MyAccount : System.Web.UI.Page
{
    PsgMembershipProvider provider = (PsgMembershipProvider)Membership.Provider;

    protected override void Render(HtmlTextWriter writer)
    {
        base.Render(writer);

        if (Session["justUpdated"] != null && (bool)(Session["justUpdated"]))
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<script type='text/javascript'>strSubmit=true; ");
            sb.Append("ntptEventTag('ev=confirmation:my account');</script>");

            writer.WriteLine(sb.ToString());
            Session["justUpdated"] = false;
        }

        if (Session["UpdatedRegion"] != null)
        {
            writer.Write("<script type='text/javascript'>setSegment('region', '" + Session["UpdatedRegion"].ToString() + "');</script>");
            Session["UpdatedRegion"] = null;
        }
        if (Session["UpdatedCountry"] != null)
        {
            writer.Write("<script type='text/javascript'>setSegment('country', '" + Session["UpdatedCountry"].ToString() + "');</script>");
            Session["UpdatedCountry"] = null;
        }
        if (Session["UpdatedJobTitle"] != null)
        {
            writer.Write("<script type='text/javascript'>setSegment('role', '" + Session["UpdatedJobTitle"].ToString() + "');</script>");
            Session["UpdatedJobTitle"] = null;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            if (Request.QueryString["pe"] != null)
                lblProfileAlert.Visible = true;

            PopulateProfileInformation();
        }
        Level3Tag = "form";

        Literal literalBody = (Literal)Master.FindControl("bodyLiteral");
        literalBody.Text = "onunload='javascript:formAbandon();'";

        BottomScript = "var formName = 'my account';";
    }

    public string Level3Tag
    {
        get;
        set;
    }

    public string BottomScript { get; set; }

    protected void PopulateProfileInformation()
    {
        string email = Page.User.Identity.Name; 
        PsgProfile psgProfile = provider.GetUserProfile(email);
        this.profileInformation.Populate(psgProfile);
        Session["Region"] = profileInformation.RegionText;
        Session["Country"] = psgProfile.Country;
        Session["JobTitle"] = psgProfile.JobTitle;
    }
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        string email = Page.User.Identity.Name;
        PsgProfile psgProfile = new PsgProfile(email,
            this.profileInformation.FirstName,
            this.profileInformation.LastName,
            this.profileInformation.PhoneNumber,
            this.profileInformation.Country,
            this.profileInformation.Region,
            this.profileInformation.CompanyName,
            this.profileInformation.CompanySize,
            this.profileInformation.JobTitle,
            this.profileInformation.Referrer,
            false);
        MembershipCreateStatus status = provider.UpdateProfileInformation(psgProfile);
        // set the region cookie for the homepage
        HttpCookie regionCookie = Request.Cookies["regionId"];
        if (regionCookie == null)
        {
            regionCookie = new HttpCookie("regionId", "");
            regionCookie.Expires = DateTime.Today.AddDays(1000d);
            Request.Cookies.Add(regionCookie);
        }
        regionCookie.Value = this.profileInformation.Region.ToString();
        Response.SetCookie(regionCookie);
        if (status.Equals(MembershipCreateStatus.Success))
        {
            Level3Tag = "confirmation";
            lblSuccess.Visible = true;
            lblProfileAlert.Visible = false;
            if ((string)(Session["Region"]) != profileInformation.RegionText)
                Session["UpdatedRegion"] = profileInformation.RegionText;
                //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "regionChange", "<script type='text/javascript'>setSegment('region', '" + this.profileInformation.Region + "');</script>", false);
            if ((string)(Session["Country"]) != psgProfile.Country)
                Session["UpdatedCountry"] = psgProfile.Country;
                //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "countryChange", "<script type='text/javascript'>setSegment('country', '" + this.profileInformation.Country + "');</script>", false);
            if ((string)(Session["JobTitle"]) != psgProfile.JobTitle)
                Session["UpdatedJobTitle"] = psgProfile.JobTitle;
                //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "roleChange", "<script type='text/javascript'>setSegment('role', '" + this.profileInformation.JobTitle + "');</script>", false);

            Session["justUpdated"] = true;
            BottomScript = "";
        }
        else
            lblFailure.Visible = true;
            
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Main.aspx");
    }
}
