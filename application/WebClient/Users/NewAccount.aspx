﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="NewAccount.aspx.cs" Inherits="NewAccount"
    MasterPageFile="~/Users/UserPages.master" %>

<%@ Register TagPrefix="psg" TagName="ProfileInformation" Src="~/Users/ProfileInformation.ascx" %>

<asp:Content runat="server" ContentPlaceHolderID="headPlaceholder">

    <script language="JavaScript">
    <!--
        var levels = ["registration", "<%=Level3Tag%>"];
        var slideid = "";
    // -->

        function ClearFormName() {
            formName = '';
        }
    </script>

    <title>HP Printing Sales Guide - New Account</title>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="bodyPlaceholder">
    <asp:CreateUserWizard ID="CreateUserWizard1" runat="server" Width="585px" OnCreatedUser="CreateUserWizard1_CreatedUser"
        OnCreateUserError="CreateUserWizard1_CreateUserError">
        <SideBarStyle BackColor="#5D7B9D" BorderWidth="0px" Font-Size="0.9em" VerticalAlign="Top" />
        <SideBarButtonStyle BorderWidth="0px" Font-Names="Verdana" ForeColor="White" />
        <ContinueButtonStyle BackColor="#FFFBFF" BorderColor="#CCCCCC" BorderStyle="Solid"
            BorderWidth="1px" Font-Names="Verdana" ForeColor="#284775" />
        <NavigationButtonStyle BackColor="#FFFBFF" BorderColor="#CCCCCC" BorderStyle="Solid"
            BorderWidth="1px" Font-Names="Verdana" ForeColor="#284775" />
        <HeaderStyle BackColor="#5D7B9D" BorderStyle="Solid" Font-Bold="True" Font-Size="0.9em"
            ForeColor="White" HorizontalAlign="Center" />
        <CreateUserButtonStyle BackColor="#FFFBFF" BorderColor="#CCCCCC" BorderStyle="Solid"
            BorderWidth="1px" Font-Names="Verdana" ForeColor="#284775" />
        <TitleTextStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <StepStyle BorderWidth="0px" />
        <WizardSteps>
            <asp:CreateUserWizardStep ID="CreateUserWizardStep1" runat="server">
                <ContentTemplate>
                    <div class="containers">
                        <span style="font-weight: bold; font-size: 14px">New Account Registration -- </span>
                        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Users/GetPassword.aspx">Forget Password?  Click Here.</asp:HyperLink></span><br />
                    </div>
                    <div class="containers">
                        <span>Please provide the following login information.</span>
                        <table>
                            <tr>
                                <td class="tableLabels">
                                    Email:
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="UserName" Style="width: 225px;" ValidationGroup="ProfileValidation"
                                        CssClass="textboxes" />
                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator11" Display="Dynamic"
                                        ControlToValidate="UserName" ErrorMessage="*" ValidationGroup="ProfileValidation" />
                                    <asp:RegularExpressionValidator Display="Dynamic" ID="RegularExpressionValidator1"
                                        runat="server" ControlToValidate="UserName" ValidationGroup="ProfileValidation"
                                        ErrorMessage="Email address is not valid" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="tableLabels">
                                    Password:
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="Password" Style="width: 225px;" TextMode="Password"
                                        ValidationGroup="ProfileValidation" CssClass="textboxes" />
                                    <asp:RequiredFieldValidator runat="server" Display="Dynamic" ID="RequiredFieldValidator10"
                                        ControlToValidate="Password" ValidationGroup="ProfileValidation" ErrorMessage="*" />
                                </td>
                            </tr>
                            <tr>
                                <td class="tableLabels">
                                    Confirm Password:
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="ConfirmPassword" Style="width: 225px;" TextMode="Password"
                                        ValidationGroup="ProfileValidation" CssClass="textboxes" />
                                    <asp:RequiredFieldValidator runat="server" Display="Dynamic" ID="RequiredFieldValidator13"
                                        ControlToValidate="ConfirmPassword" ValidationGroup="ProfileValidation" ErrorMessage="*" />
                                </td>
                            </tr>
                            <tr runat="server" visible="false">
                                <td class="tableLabels">
                                    Passcode:
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="Answer" Style="width: 225px;" TextMode="SingleLine"
                                        ValidationGroup="ProfileValidation" CssClass="textboxes" />
                                    <asp:RequiredFieldValidator runat="server" Display="Dynamic" ID="RequiredFieldValidator1"
                                        ControlToValidate="Answer" ValidationGroup="ProfileValidation" ErrorMessage="*" />
                                </td>
                            </tr>
                            <tr runat="server" visible="false">
                                <td colspan="2" style="text-align: center;">
                                    NOTE: To obtain the Passcode, please send a request to <a href="mailto:support@hpprintingsalesguide.com" onclick="ntptEventTag( 'ev=email link&mailto_lk=' + escape( this.href ).toLowerCase() );">
                                        support@hpprintingsalesguide.com</a>.
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:CompareValidator ID="PasswordCompare" runat="server" ControlToCompare="Password"
                                        ValidationGroup="ProfileValidation" ControlToValidate="ConfirmPassword" Display="Dynamic"
                                        ErrorMessage="The Password and Confirmation Password must match."></asp:CompareValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox ID="Email" runat="server" Visible="false" ValidationGroup="ProfileValidation"
                                        CssClass="textboxes"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:Label runat="server" ID="lblErrorMessage" EnableViewState="false" ForeColor="Red"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="containers">
                        <span>Please provide the following profile information.</span>
                        <psg:ProfileInformation ID="ProfileInformationUserControl" runat="server" />
                    </div>
                </ContentTemplate>
                <CustomNavigationTemplate>
                    <div class="containers">
                        <table border="0" cellspacing="5" style="width: 100%; height: 100%;">
                            <tr align="left">
                                <td align="left" colspan="0">
                                    <asp:Button ID="StepNextButton" runat="server" BackColor="#FFFBFF" BorderColor="#CCCCCC"
                                        BorderStyle="Solid" BorderWidth="1px" CommandName="MoveNext" Font-Names="Verdana"
                                        ForeColor="#284775" Text="Create Account" ValidationGroup="ProfileValidation" OnClientClick="strSubmit=true; ClearFormName();" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </CustomNavigationTemplate>
            </asp:CreateUserWizardStep>
            <asp:CompleteWizardStep runat="server">
                <ContentTemplate>
                    <div class="containers">
                        <span>Thank you for signing up to use the HP Printing Sales Guide. Your profile has
                            been saved. Click continue to proceed to the viewer.</span>
                    </div>
                    <div class="containers">
                        <table border="0" cellspacing="5" style="width: 100%; height: 100%;">
                            <tr align="left">
                                <td align="left" colspan="0">
                                    <asp:Button ID="btnBack" runat="server" BackColor="#FFFBFF" BorderColor="#CCCCCC"
                                        BorderStyle="Solid" BorderWidth="1px" Font-Names="Verdana" ForeColor="#284775"
                                        Text="Continue" OnClick="btnContinue_Click" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </ContentTemplate>
            </asp:CompleteWizardStep>
        </WizardSteps>
    </asp:CreateUserWizard>

    <script language="javascript" type="text/javascript" src="../js/analytics.js"></script>
    <script language="javascript" type="text/javascript" src="../js/ntpagetag.js"></script>
    <script language="javascript" type="text/javascript">
        function formAbandon() {
            if (typeof formName != "undefined" && formName != '') {
                if (!strSubmit) ntptEventTag('ev=form abandoned&ed=' + formName.toLowerCase() + '&last_field=' + escape(lastField.toLowerCase()));
            }
        }
    </script>
    <script type="text/javascript">
        <%=BottomScript%>
    </script>
</asp:Content>
