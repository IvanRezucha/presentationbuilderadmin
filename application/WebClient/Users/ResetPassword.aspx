﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ResetPassword.aspx.cs" Inherits="Users_ResetPassword"
    MasterPageFile="~/Users/UserPages.master" %>

<asp:Content runat="server" ContentPlaceHolderID="headPlaceholder">
    <script language="JavaScript">
    <!--
        var levels = ["reset password", "<%=Level3Tag%>"];
        var slideid = "";
    // -->
    </script>
    <title>HP Printing Sales Guide - Reset Password</title>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="bodyPlaceholder">
    <div class="containers">
        <span style="font-weight: bold; font-size: 14px">Reset Password</span>
    </div>
    <asp:Panel ID="successTemplate" runat="server" Visible="false">
        <div class="containers">
            <table border="0" cellpadding="4">
                <tr>
                    <td>
                        <span style="font-weight: bold">Password reset complete.</span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span style="font-weight: bold">Your password has been changed!</span>
                    </td>
                </tr>
            </table>
        </div>
        <div class="containers">
            <table border="0" cellspacing="5" style="width: 100%; height: 100%;">
                <tr align="left">
                    <td align="left" colspan="0">
                        <asp:Button ID="Button1" runat="server" BackColor="#FFFBFF" BorderColor="#CCCCCC"
                            BorderStyle="Solid" BorderWidth="1px" Font-Names="Verdana" ForeColor="#284775"
                            Text="Continue" OnClick="Continue_Click" />
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    <asp:Panel ID="changePasswordTemplate" runat="server" Visible="true">
        <div class="containers">
            <asp:Label runat="server" ID="tempPasswordLabel" ForeColor="Red" Visible="false">You are currently using a temporary password.</asp:Label><br /> 
            <span>Please provide the following login information to reset your password.</span>
            <table>
                <tr>
                    <td class="tableLabels">
                        Email:
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="UserName" Style="width: 225px;" ValidationGroup="ProfileValidation"
                            CssClass="textboxes" />
                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator11" Display="Dynamic"
                            ControlToValidate="UserName" ErrorMessage="*" ValidationGroup="ProfileValidation" />
                        <asp:RegularExpressionValidator Display="Dynamic" ID="RegularExpressionValidator1"
                            runat="server" ControlToValidate="UserName" ValidationGroup="ProfileValidation"
                            ErrorMessage="Email address is not valid" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td class="tableLabels">
                        Current Password:
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="CurrentPassword" Style="width: 225px;" TextMode="Password"
                            ValidationGroup="ProfileValidation" CssClass="textboxes" />
                        <asp:RequiredFieldValidator runat="server" Display="Dynamic" ID="RequiredFieldValidator10"
                            ControlToValidate="CurrentPassword" ValidationGroup="ProfileValidation" ErrorMessage="*" />
                    </td>
                </tr>
                <tr>
                    <td class="tableLabels">
                        New Password:
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="NewPassword" Style="width: 225px;" TextMode="Password"
                            ValidationGroup="ProfileValidation" CssClass="textboxes" />
                        <asp:RequiredFieldValidator runat="server" Display="Dynamic" ID="RequiredFieldValidator13"
                            ControlToValidate="NewPassword" ValidationGroup="ProfileValidation" ErrorMessage="*" />
                    </td>
                </tr>
                <tr>
                    <td class="tableLabels">
                        Confirm New Password:
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="ConfirmPassword" Style="width: 225px;" TextMode="Password"
                            ValidationGroup="ProfileValidation" CssClass="textboxes" />
                        <asp:RequiredFieldValidator runat="server" Display="Dynamic" ID="RequiredFieldValidator1"
                            ControlToValidate="ConfirmPassword" ValidationGroup="ProfileValidation" ErrorMessage="*" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:CompareValidator ID="PasswordCompare" runat="server" ControlToCompare="NewPassword"
                            ValidationGroup="ProfileValidation" ControlToValidate="ConfirmPassword" Display="Dynamic"
                            ErrorMessage="The Password and Confirmation Password must match."></asp:CompareValidator>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="color: Red;">
                        <asp:Literal EnableViewState="False" ID="FailureText" runat="server">
                        </asp:Literal>
                    </td>
                </tr>
            </table>
        </div>
        <div class="containers">
            <table border="0" cellspacing="5" style="width: 100%; height: 100%;">
                <tr align="left">
                    <td align="left" colspan="0">
                        <asp:Button ID="ChangePassword" runat="server" BackColor="#FFFBFF" BorderColor="#CCCCCC"
                            BorderStyle="Solid" BorderWidth="1px" Font-Names="Verdana" ForeColor="#284775"
                            Text="Reset Password" ValidationGroup="ProfileValidation" OnClick="ChangePassword_Click"  />
                        <asp:Button ID="Cancel" runat="server" BackColor="#FFFBFF" BorderColor="#CCCCCC"
                            BorderStyle="Solid" BorderWidth="1px" Font-Names="Verdana" ForeColor="#284775"
                            Text="Cancel" OnClick="Cancel_Click" />
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>

    <script language="javascript" type="text/javascript" src="../js/analytics.js"></script>
    <script language="javascript" type="text/javascript" src="../js/ntpagetag.js"></script>

</asp:Content>
