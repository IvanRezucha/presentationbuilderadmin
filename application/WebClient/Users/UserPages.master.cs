﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Users_UserPages : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Let's set up some Analytics Tracking!
        Control ctrlLogout = LoginView1.FindControl("LoginStatus1");
        if (ctrlLogout != null)
        {
            LoginStatus statusLink = ctrlLogout as LoginStatus;
            statusLink.Attributes.Add("onclick", "setLogoutSegment(); ntptEventTag( 'ev=logout' );");
        }
    }
}
