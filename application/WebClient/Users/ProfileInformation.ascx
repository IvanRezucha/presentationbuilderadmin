﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProfileInformation.ascx.cs"
    Inherits="Users_ProfileInformation" %>
    <script  type="text/javascript">
        function isNumber(field) {
            var re = /^[0-9]*$/;
            if (!re.test(field.value)) {
                field.value = field.value.replace(/[^0-9]/g, "");
            }
        }
</script>
<table>
    <tr>
        <td class="tableLabels">First Name:</td>
        <td>
            
            <asp:TextBox runat="server" ID="txtFirstName" Style="width:225px;" MaxLength="50" ValidationGroup="ProfileValidation" CssClass="textboxes" />
            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtFirstName"
                ErrorMessage="*" ValidationGroup="ProfileValidation" />
        </td>
    </tr>
    <tr>
        <td class="tableLabels">Last Name:</td>
        <td>
            
            <asp:TextBox runat="server" ID="txtLastName" Style="width:225px;" MaxLength="50"  ValidationGroup="ProfileValidation"  CssClass="textboxes"/>
            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="txtLastName" ValidationGroup="ProfileValidation"
                ErrorMessage="*" />
        </td>
    </tr>
    <tr>
        <td class="tableLabels">Phone Number:</td>
        <td>
            <asp:TextBox runat="server" ID="txtPhoneNumber" Style="width:225px;" MaxLength="25" ValidationGroup="ProfileValidation" CssClass="textboxes" />
            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator7" ControlToValidate="txtPhoneNumber" ValidationGroup="ProfileValidation"
                ErrorMessage="*" />
        </td>
    </tr>
    <tr>
        <td class="tableLabels">Country you reside in:</td>
        <td>
            <asp:DropDownList ID="ddlCountries" Style="width:230px;" CssClass="textboxes" runat="server">
            </asp:DropDownList>
            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ControlToValidate="ddlCountries" ValidationGroup="ProfileValidation"
                ErrorMessage="*" />
            <%--
            <asp:TextBox runat="server" ID="txtCountry"  Style="width:225px;" MaxLength="50"  ValidationGroup="ProfileValidation" CssClass="textboxes"/>
            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator8" ControlToValidate="txtCountry" ValidationGroup="ProfileValidation"
                ErrorMessage="*" />
            --%>
        </td>
    </tr>
    <tr>
        <td class="tableLabels">HP region you belong to:</td>
        <td>
            <asp:DropDownList ID="ddlRegion" Style="width:230px;" CssClass="textboxes" runat="server">
            </asp:DropDownList>
            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator6" ControlToValidate="ddlRegion" ValidationGroup="ProfileValidation"
                ErrorMessage="*" />
            <%-- 
            <asp:TextBox runat="server" ID="txtRegion"  Style="width:225px;" MaxLength="50" ValidationGroup="ProfileValidation" CssClass="textboxes"/>
            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator9" ControlToValidate="txtRegion" ValidationGroup="ProfileValidation"
                ErrorMessage="*" />
            --%>
        </td>
    </tr>
    <tr>
        <td class="tableLabels">Company Name:</td>
        <td>
            <asp:TextBox runat="server" ID="txtCompanyName"  Style="width:225px;" MaxLength="50" ValidationGroup="ProfileValidation" CssClass="textboxes"/>
            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" ControlToValidate="txtCompanyName" ValidationGroup="ProfileValidation"
                ErrorMessage="*" />
        </td>
    </tr>
    <tr>
        <td class="tableLabels">Company Size:</td>
        <td>
            <asp:TextBox runat="server" ID="txtCompanySize" Style="width:225px;" MaxLength="50" ValidationGroup="ProfileValidation" CssClass="textboxes" onkeyup="isNumber(this)"/>
            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator5" ControlToValidate="txtCompanySize" ValidationGroup="ProfileValidation"
                ErrorMessage="*" />  
            <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtCompanySize" ErrorMessage="Must Be Numeric" Type="Integer" MinimumValue="1" MaximumValue="1000000" ValidationGroup="ProfileValidation"></asp:RangeValidator>
        </td>
    </tr>
    <tr>
        <td class="tableLabels">Organization:</td>
        <td>
            <asp:DropDownList ID="ddlJobTitles" Style="width:230px;" CssClass="textboxes" runat="server">
            </asp:DropDownList>
            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator8" ControlToValidate="ddlJobTitles" ValidationGroup="ProfileValidation"
                ErrorMessage="*" />
            <%-- 
            <asp:TextBox runat="server" ID="txtJobTitle" Style="width:225px;" MaxLength="50" ValidationGroup="ProfileValidation" CssClass="textboxes"/>
            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator6" ControlToValidate="txtJobTitle" ValidationGroup="ProfileValidation"
                ErrorMessage="*" />
            --%>
        </td>
    </tr>
    <tr>
        <td class="tableLabels">How were you referred to this site?</td>
        <td>
            <asp:TextBox runat="server" ID="txtReferrer" Style="width:225px;" MaxLength="50" ValidationGroup="ProfileValidation" CssClass="textboxes"/>
            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator10" ControlToValidate="txtReferrer" ValidationGroup="ProfileValidation"
                ErrorMessage="*" />
        </td>
    </tr>
</table>
