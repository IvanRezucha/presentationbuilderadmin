using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.Configuration;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Net;
using PSG.Core.Files;

public partial class Main : System.Web.UI.Page
{
    //here is a link to the rad controls web site.
    //http://www.telerik.com/help/aspnet/combobox/combo_client_model.html
    //There is a difference between telerik.WebControls.RadComboBox and telerik.Web.UI.RadComboBox. 
    //The latter is the newest version.  That client api's are different for each of them.

    private const string ROOT_NODE_LEVEL = "3";

    protected void Page_Load(object sender, EventArgs e)
    {

        this.Unload += new EventHandler(Main_Unload);

        if (!Page.IsPostBack)
        {
            //we need a tricky backdoor into the system, so we'll check to see if this QUeryString key is present
            //if (Request.QueryString["tricky"] == null)
            //{
            //    //if we aren't being tricky, check to see if this session was authenticated on the Authenticate.aspx page
            //    if (Session["Authenticated"] == null || Session["Authenticated"].ToString() != "true")
            //    {
            //        Response.Redirect("AccessDenied.aspx");
            //    }
            //}
            ProfileEnforcer enforcer = new ProfileEnforcer(this);
            enforcer.Enforce();


            SlideThumbHack();

            MySlidesHack();

            PopulateRegionLanguageList();
            string[] pieces = RadComboBoxRegionLanguage.Items[0].Text.Split(new char[] { '-' });
            Session["SelectedRegion"] = pieces[0];
            Session["SelectedLanguage"] = pieces[1];

            GetSlideLibraryData();
            //PopulateInitialNodes();
            PopulateFilters();
            Session["SelectedFilters"] = GetFilterIds();

            HistorySetup();

            // Let's set up some Analytics Tracking!
            ImageButtonAddToMySlides.Attributes.Add("onclick", "ntptEventTag( 'ev=add slide' );");
            ImageButtonAddAllToMySlides.Attributes.Add("onclick", "ntptEventTag( 'ev=add deck' );");
            hypDownload.Attributes.Add("onclick", "ntptEventTag( 'ev=download slide' );");
            hypDownloadMySlides.Attributes.Add("onclick", "ntptEventTag( 'ev=download deck' );");
            Control ctrlLogout = LoginView1.FindControl("LoginStatus1");
            if (ctrlLogout != null)
            {
                LoginStatus statusLink = ctrlLogout as LoginStatus;
                statusLink.Attributes.Add("onclick", "setLogoutSegment(); ntptEventTag( 'ev=logout' );");
            }
        }

        WriteJs();
    }

    void Main_Unload(object sender, EventArgs e)
    {
        // Reset.
        Session["LgnProfile"] = null;

        Session["RegionChanged"] = false;
        Session["LanguageChanged"] = false;
        Session["FiltersChanged"] = false;

    }

    protected void WriteJs()
    {
        StringBuilder content = new StringBuilder();
        string nptEvent = string.Empty;

        if (Session["LgnProfile"] != null)
        {
            PsgProfile profile = (PsgProfile)(Session["LgnProfile"]);
            content.Append("<script type='text/javascript'>setLoginSegment(); " +
                "setSegment('region', '" + Session["SelectedRegion"] + "');" +
                "setSegment('language', '" + Session["SelectedLanguage"] + "');" +
                "setSegment('role', '" + profile.JobTitle + "');" +
                "setSegment('country', '" + profile.Country + "');" +
                "setSegment('browsemode', '" + RadComboBoxViewMode.Text + "');" +
                "setSegment('filters', '" + GetFilterIds() + "');" +
                "</script>");
            Session["LgnProfile"] = null;

            nptEvent = "<script type='text/javascript'>ntptEventTag('ev=login');</script> ";
        }

        if (Session["RegionChanged"] != null && (bool)Session["RegionChanged"])
            content.Append("<script type='text/javascript'>setSegment('region', '" + Session["SelectedRegion"].ToString() + "');</script>");
        if (Session["LanguageChanged"] != null && (bool)Session["LanguageChanged"])
            content.Append("<script type='text/javascript'>setSegment('language', '" + Session["SelectedLanguage"].ToString() + "');</script>");
        if (Session["FiltersChanged"] != null && (bool)Session["FiltersChanged"])
            content.Append("<script type='text/javascript'>setSegment('filters', '" + Session["SelectedFilters"].ToString() + "');</script>");

        Literal literal = new Literal();
        literal.Text = content.ToString();
        scriptPlaceholder.Controls.Add(literal);
        Literal nptLiteral = new Literal();
        nptLiteral.Text = nptEvent;
        ntptEventTagPlaceholder.Controls.Add(nptLiteral);
    }

    #region Flash Load Methods

    public string GetTreeXmlFile()
    {
        string xmlFile = WebConfigurationManager.AppSettings["DataURI"].ToString() + String.Format("{0}-LocalizedTreeData.xml", RadComboBoxRegionLanguage.SelectedValue);
        //xmlFile = Path.Combine(WebConfigurationManager.AppSettings["DataURI"].ToString(), String.Format("{0}-LocalizedTreeData.xml", RadComboBoxRegionLanguage.SelectedValue));
        //string xmlFile = "http://localhost:2714/WebClient/13-LocalizedTreeData.xml";
        return xmlFile;
    }

    public string GetFilterIds()
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        int i = 0;
        foreach (ListItem li in CheckBoxListFilters.Items)
        {
            i++;
            if (li.Selected)
            {
                sb.Append(li.Value);
                if (i < CheckBoxListFilters.Items.Count)
                {
                    sb.Append(",");
                }
            }
        }
        return sb.ToString();
    }

    #endregion

    #region History

    private void HistorySetup()
    {
        Session["History"] = new List<HistoryItem>();
    }

    private List<HistoryItem> HistoryGet()
    {
        return (List<HistoryItem>)Session["History"];
    }

    private void HistorySave(List<HistoryItem> history)
    {
        Session["History"] = history;
    }

    private void HistoryAdd(string historyItemText)
    {
        //make a new item
        HistoryItem newItem = new HistoryItem();
        newItem.IsCurrentItem = true;
        newItem.Text = historyItemText;  //iFrame.Attributes["src"].ToString();

        //get current history and current item
        List<HistoryItem> history = HistoryGet();
        HistoryItem historyItem = history.Find(HistoryIsCurrentItem);



        if (historyItem != null)
        {
            //get the current item index
            int currentHistoryItemIndex = history.IndexOf(historyItem);

            //the newItem is going to become the current item, so the other can't be
            history.ForEach(delegate(HistoryItem histItem)
            {
                histItem.IsCurrentItem = false;
            });

            //insert the newItem after the old current item
            history.Insert(currentHistoryItemIndex + 1, newItem);
        }
        else
        {
            history.Add(newItem);
        }

        HistorySave(history);

    }

    private void HistoryBack()
    {
        List<HistoryItem> history = HistoryGet();
        HistoryItem currentItem = history.Find(HistoryIsCurrentItem);
        if (currentItem != null)
        {
            //get the current item index
            int currentHistoryItemIndex = history.IndexOf(currentItem);
            //can't go back if the current items is first in the list
            if (currentHistoryItemIndex > 0)
            {
                //browse to the previous item
                SetIframe(history[currentHistoryItemIndex - 1].Text);

                //keep track of the current item
                history[currentHistoryItemIndex].IsCurrentItem = false;
                history[currentHistoryItemIndex - 1].IsCurrentItem = true;
                HistorySave(history);
            }
        }
    }

    private void HistoryForward()
    {
        List<HistoryItem> history = HistoryGet();
        HistoryItem currentItem = history.Find(HistoryIsCurrentItem);
        if (currentItem != null)
        {
            //get the current item index
            int currentHistoryItemIndex = history.IndexOf(currentItem);
            //if the current item is last in the list, we can't go forward
            if (currentHistoryItemIndex < history.Count - 1)
            {
                //browse to the next item
                SetIframe(history[currentHistoryItemIndex + 1].Text);

                //keep track of the current item
                history[currentHistoryItemIndex].IsCurrentItem = false;
                history[currentHistoryItemIndex + 1].IsCurrentItem = true;
                HistorySave(history);
            }
        }
    }

    private static bool HistoryIsCurrentItem(HistoryItem historyItem)
    {
        if (historyItem.IsCurrentItem)
        {
            return true;
        }
        return false;
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        HistoryBack();
    }
    protected void btnForward_Click(object sender, EventArgs e)
    {
        HistoryForward();
    }

    #endregion

    #region Setup Hacks

    private void SlideThumbHack()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("SlideUrl", Type.GetType("System.String"));
        dt.Columns.Add("SlideThumbUrl", Type.GetType("System.String"));
        dt.Columns.Add("Title", Type.GetType("System.String"));

        DataRow dr = dt.NewRow();
        dr["SlideUrl"] = "blah";
        dr["SlideThumbUrl"] = "blah";

        dt.Rows.Add(dr);
        DataListSlideThumbs.DataSource = dt;
        DataListSlideThumbs.DataBind();
    }

    private void MySlidesHack()
    {
        MySlidesDS.MySlidesDataTable dtMySlides = new MySlidesDS.MySlidesDataTable();
        dtMySlides.AddMySlidesRow(0, "", "blah", "blah", 0);
        DataListMySlides.DataSource = dtMySlides;
        DataListMySlides.DataBind();
    }

    public bool IsShown(string slideURL)
    {
        if (slideURL == "blah")
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    #endregion

    #region Initialization

    private void PopulateRegionLanguageList()
    {
        wsWebAppData.SlideLibrary dsRegLangs = new wsWebAppData.SlideLibrary();
        dsRegLangs.ReadXml(String.Format("{0}RegionLanguages.xml", WebConfigurationManager.AppSettings["DataURI"].ToString()));

        foreach (wsWebAppData.SlideLibrary.RegionLanguageRow regLang in dsRegLangs.RegionLanguage.Rows)
        {
            if (!regLang.RegionRow.IsClientDefault)
            {
                Telerik.WebControls.RadComboBoxItem li = new Telerik.WebControls.RadComboBoxItem(String.Format("{0}-{1}", regLang.RegionRow.RegionName, regLang.LanguageRow.LongName), regLang.RegionLanguageID.ToString());
                RadComboBoxRegionLanguage.Items.Add(li);
            }

        }

        RadComboBoxRegionLanguage.SelectedIndex = 0;
    }

    //private void PopulateRegionList()
    //{
    //    wsWebAppData.SlideLibrary dsRegions = new wsWebAppData.SlideLibrary();
    //    dsRegions.ReadXml(String.Format("{0}RegionLanguages.xml", WebConfigurationManager.AppSettings["DataURI"].ToString()));
    //    StringBuilder sb = new StringBuilder();

    //    foreach (wsWebAppData.SlideLibrary.RegionRow region in dsRegions.Region.Rows)
    //    {
    //        if (!region.RegionName.Equals("Common"))
    //            RegionComboBox.Items.Add(new Telerik.WebControls.RadComboBoxItem(region.RegionName, region.RegionID.ToString()));
    //    }

    //}

    private void GetSlideLibraryData()
    {

        string xmlFile = WebConfigurationManager.AppSettings["DataURI"].ToString() + String.Format("ds{0}-LocalizedTreeData.xml", RadComboBoxRegionLanguage.SelectedValue);
        wsWebAppData.WebTreeData dsWebTreeData = new wsWebAppData.WebTreeData();
        dsWebTreeData.EnforceConstraints = false;
        dsWebTreeData.ReadXml(xmlFile);

        Cache[xmlFile] = dsWebTreeData;
    }

    private void PopulateFilters()
    {
        wsWebAppData.SlideLibrary dsFilters = new wsWebAppData.SlideLibrary();
        string xmlFile = WebConfigurationManager.AppSettings["DataURI"].ToString() + String.Format("Main.xml");
        dsFilters.ReadXml(xmlFile);
        foreach (wsWebAppData.SlideLibrary.FilterRow filterRow in dsFilters.Filter.Rows)
        {
            ListItem filterItem = new ListItem();
            filterItem.Text = filterRow.FilterName;
            filterItem.Value = filterRow.FilterID.ToString();
            filterItem.Selected = true;
            CheckBoxListFilters.Items.Add(filterItem);
        }
    }

    #endregion

    #region AJAX

    protected void RadAjaxManager1_AjaxRequest(object sender, Telerik.WebControls.AjaxRequestEventArgs e)
    {
        List<string> ajaxParams = new List<string>(e.Argument.Split('|'));

        if (ajaxParams[0].Equals("GetThumbs"))
        {
            int catId = Int32.Parse(ajaxParams[1]);
            PopulateThumbnails(catId);
        }
        else if (ajaxParams[0].Equals("SlideNodeClick"))
        {
            SlideNodeClicked(ajaxParams[1]);
        }
    }

    private void PopulateThumbnails(int catID)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("SlideUrl", Type.GetType("System.String"));
        dt.Columns.Add("SlideThumbUrl", Type.GetType("System.String"));
        dt.Columns.Add("Title", Type.GetType("System.String"));


        string xmlFile = WebConfigurationManager.AppSettings["DataURI"].ToString() + String.Format("ds{0}-LocalizedTreeData.xml", RadComboBoxRegionLanguage.SelectedValue);
        wsWebAppData.WebTreeData dsWebTreeData = new wsWebAppData.WebTreeData();
        dsWebTreeData.EnforceConstraints = false;
        dsWebTreeData.ReadXml(xmlFile);

        wsWebAppData.WebTreeData.CatRow catRow = dsWebTreeData.Cat.FindBylabelID(catID);

        if (catRow != null)
        {
            wsWebAppData.WebTreeData.SlideCatInfoRow[] slideCats = catRow.GetSlideCatInfoRows();

            List<string> selectedFilterIds = new List<string>(GetFilterIds().Split(','));
            List<string> slideCatFilterIds = new List<string>();
            foreach (wsWebAppData.WebTreeData.SlideCatInfoRow slideCat in slideCats)
            {
                slideCatFilterIds.Clear();
                slideCatFilterIds.AddRange(slideCat.filterIDs.Split(','));

                foreach (string filterId in slideCatFilterIds)
                {
                    if (selectedFilterIds.Contains(filterId))
                    {
                        DataRow dr = dt.NewRow();
                        dr["SlideUrl"] = slideCat.FileName;
                        string thumb = WebConfigurationManager.AppSettings["ThumbnailLocation"].ToString() + slideCat.FileName.Replace(".pot", ".jpg");
                        dr["SlideThumbUrl"] = thumb;
                        dr["Title"] = slideCat.label;
                        dt.Rows.Add(dr);
                        break;
                    }
                }
            }
        }

        DataListSlideThumbs.Visible = true;
        DataListSlideThumbs.DataSource = dt;
        DataListSlideThumbs.DataBind();

        //iFrame should browse to the first slide in the list
        if (dt.Rows.Count > 0)
        {
            SetIframe(dt.Rows[0]["SlideUrl"].ToString());

            HistoryAdd(dt.Rows[0]["SlideUrl"].ToString());
        }


    }

    private void SlideNodeClicked(string nodeXml)
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        sb.Append("<root>");
        sb.Append(nodeXml);
        sb.Append("</root>");

        System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();
        xmlDoc.LoadXml(sb.ToString());

        System.Xml.XmlNodeList nodes = xmlDoc.GetElementsByTagName("SlideCatInfo");

        //iFrame should browse to the first slide in the list
        if (nodes.Count > 0)
        {
            SetIframe(nodes[0].Attributes["FileName"].Value);

            HistoryAdd(nodes[0].Attributes["FileName"].Value);
        }
    }

    #endregion

    private void SetIframe(string filename)
    {
        iFrame.Attributes["src"] = String.Format("{0}{1}", ConfigurationManager.AppSettings["SlideLocation"], filename);
        hypDownload.NavigateUrl = "DownloadPPT.aspx?filename=" + filename;
        hypDownload.Target = "_blank";
    }

    #region DataList Events



    protected void DataListSlideThumbs_ItemCommand(object source, DataListCommandEventArgs e)
    {
        if (e.CommandName == "btnThumb")
        {
            SetIframe(e.CommandArgument.ToString());
            HistoryAdd(e.CommandArgument.ToString());
            //RadPaneSlidePreview.ContentUrl = e.CommandArgument.ToString();
            DataListSlideThumbs.SelectedIndex = e.Item.ItemIndex;
            DataListMySlides.SelectedIndex = -1;
        }
        else if (e.CommandName == "ImageButtonAddThisSlideToMySlides")
        {
            AddToMySlides(e.CommandArgument.ToString());
        }

    }

    protected void DataListMySlides_ItemCommand(object source, DataListCommandEventArgs e)
    {
        if (e.CommandName == "btnThumb")
        {
            SetIframe(e.CommandArgument.ToString());
            HistoryAdd(e.CommandArgument.ToString());
            DataListMySlides.SelectedIndex = e.Item.ItemIndex;
        }
        else if (e.CommandName == "ImageButtonMoveLeft")
        {

            if (e.Item.ItemIndex != 0)
            {
                MySlidesDS.MySlidesDataTable dtMySlides = (MySlidesDS.MySlidesDataTable)Session["MySlides"];
                foreach (MySlidesDS.MySlidesRow mySlideRow in dtMySlides.Rows)
                {
                    if (mySlideRow.SortOrder == e.Item.ItemIndex - 1)
                    {
                        mySlideRow.SortOrder += 1;
                    }
                    else if (mySlideRow.SortOrder == e.Item.ItemIndex)
                    {
                        mySlideRow.SortOrder -= 1;
                    }
                }
                SaveAndBindMySlides(dtMySlides);

                DataListMySlides.SelectedIndex = e.Item.ItemIndex - 1;
                DataListSlideThumbs.SelectedIndex = -1;
            }
        }
        else if (e.CommandName == "ImageButtonMoveRight")
        {
            if (e.Item.ItemIndex != DataListMySlides.Items.Count - 1)
            {
                MySlidesDS.MySlidesDataTable dtMySlides = (MySlidesDS.MySlidesDataTable)Session["MySlides"];
                foreach (MySlidesDS.MySlidesRow mySlideRow in dtMySlides.Rows)
                {
                    if (mySlideRow.SortOrder == e.Item.ItemIndex + 1)
                    {
                        mySlideRow.SortOrder -= 1;
                    }
                    else if (mySlideRow.SortOrder == e.Item.ItemIndex)
                    {
                        mySlideRow.SortOrder += 1;
                    }
                }
                SaveAndBindMySlides(dtMySlides);
                DataListMySlides.SelectedIndex = e.Item.ItemIndex + 1;
                DataListSlideThumbs.SelectedIndex = -1;
            }
        }
        else if (e.CommandName == "ImageButtonRemoveSlide")
        {
            MySlidesDS.MySlidesDataTable dtMySlides = (MySlidesDS.MySlidesDataTable)Session["MySlides"];
            MySlidesDS.MySlidesRow mySlideRowToDelete = null;
            foreach (MySlidesDS.MySlidesRow mySlideRow in dtMySlides.Rows)
            {
                if (mySlideRow.SortOrder == e.Item.ItemIndex)
                {
                    mySlideRowToDelete = mySlideRow;
                }
                else if (mySlideRow.SortOrder > e.Item.ItemIndex)
                {
                    mySlideRow.SortOrder -= 1;
                }
            }
            if (mySlideRowToDelete != null)
            {
                mySlideRowToDelete.Delete();
            }

            dtMySlides.AcceptChanges();
            SaveAndBindMySlides(dtMySlides);
        }
    }

    #endregion

    #region Buttons

    protected void ImageButtonAddToMySlides_Click(object sender, ImageClickEventArgs e)
    {
        if (iFrame.Attributes["src"] != null)
        {
            string slideFile = iFrame.Attributes["src"].ToString();
            AddToMySlides(slideFile);
        }
    }

    protected void ImageButtonAddAllToMySlides_Click(object sender, ImageClickEventArgs e)
    {
        foreach (DataListItem slideThumbItem in DataListSlideThumbs.Items)
        {
            ImageButton btnThumb = (ImageButton)slideThumbItem.FindControl("btnThumb");
            if (btnThumb != null)
            {
                AddToMySlides(btnThumb.CommandArgument);
            }
        }
    }

    private string GetSlidePath(string url)
    {
        //string iFrameSrc = iFrame.Attributes["src"].ToString();
        string[] splitter = { @"/" };
        //string[] urlParts = iFrameSrc.Split(splitter, StringSplitOptions.None);
        string[] urlParts = url.Split(splitter, StringSplitOptions.None);
        string fileName = urlParts[urlParts.Length - 1];

        if (fileName.EndsWith(".ppt"))
            fileName.Replace(".ppt", ".pot");

        FileLocation location = new FileLocation(fileName);
        string slideDirectory = Path.Combine(ConfigurationManager.AppSettings["SlidePhysicalPath"], location.Directory);
        string slideFullPath = Path.Combine(slideDirectory, fileName);

        return slideFullPath;
    }


    protected void ImageButtonRemoveAllMySlides_Click(object sender, ImageClickEventArgs e)
    {
        if (Session["MySlides"] != null)
        {
            MySlidesDS.MySlidesDataTable dtMySlides = (MySlidesDS.MySlidesDataTable)Session["MySlides"];
            dtMySlides.Clear();
            dtMySlides.AddMySlidesRow(0, "", "blah", "blah", 0);
            Session.Remove("MySlides");
            DataListMySlides.DataSource = dtMySlides;
            DataListMySlides.DataBind();
        }
    }

    protected void ButtonApplyFilters_Click(object sender, EventArgs e)
    {
        Session["FiltersChanged"] = true;
        Session["SelectedFilters"] = GetFilterIds();

        SlideThumbHack();
    }

    #endregion

    protected void RadComboBox1_SelectedIndexChanged(object o, Telerik.WebControls.RadComboBoxSelectedIndexChangedEventArgs e)
    {
        string oldRegion = Session["SelectedRegion"].ToString();
        string oldLanguage = Session["SelectedLanguage"].ToString();

        string[] newPieces = RadComboBoxRegionLanguage.Text.Split(new char[] { '-' });

        Session["RegionChanged"] = (oldRegion != newPieces[0]);
        Session["LanguageChanged"] = (oldLanguage != newPieces[1]);
        Session["SelectedRegion"] = newPieces[0];
        Session["SelectedLanguage"] = newPieces[1];

        SlideThumbHack();

    }

    #region Helpers


    private List<int> GetSelectedFilterIds()
    {
        List<int> selectedFilterIds = new List<int>();
        foreach (ListItem filterItem in CheckBoxListFilters.Items)
        {
            if (filterItem.Selected)
            {
                selectedFilterIds.Add(Int32.Parse(filterItem.Value));
            }
        }
        return selectedFilterIds;
    }

    private void DownloadPptFile(System.IO.FileInfo file)
    {
        try
        {
            Response.Clear();
            Response.AddHeader("Content-Disposition", "attachment; filename=" + Path.GetFileNameWithoutExtension(file.Name) + ".ppt");
            Response.AddHeader("Content-Length", file.Length.ToString());
            Response.ContentType = "application/octet-stream";
            Response.WriteFile(file.FullName);
            Response.End();
        }
        catch (Exception ex)
        {
            //probably couldn't find the file
        }
    }

    private void AddToMySlides(string slideFile)
    {
        RadPaneMySlides.Height = Unit.Pixel(200);
        MySlidesDS.MySlidesDataTable dtMySlides;
        if (Session["MySlides"] == null)
        {
            dtMySlides = new MySlidesDS.MySlidesDataTable();
        }
        else
        {
            dtMySlides = (MySlidesDS.MySlidesDataTable)Session["MySlides"];
        }

        wsWebAppData.WebTreeData.SlideCatInfoRow slide = GetSlideByFileName(slideFile.Replace(ConfigurationManager.AppSettings["SlideLocation"].ToString(), ""));

        if (slide != null)
        {
            dtMySlides.AddMySlidesRow(slide.SlideID, slide.label, String.Format("{0}{1}", ConfigurationManager.AppSettings["SlideLocation"].ToString(), slide.FileName), String.Format("{0}{1}", ConfigurationManager.AppSettings["ThumbnailLocation"].ToString(), slide.FileName).Replace(".pot", ".jpg"), dtMySlides.Count);
            SaveAndBindMySlides(dtMySlides);
        }
    }

    private wsWebAppData.WebTreeData.SlideCatInfoRow GetSlideByFileName(string fileName)
    {

        string xmlFile = WebConfigurationManager.AppSettings["DataURI"].ToString() + String.Format("ds{0}-LocalizedTreeData.xml", RadComboBoxRegionLanguage.SelectedValue);
        wsWebAppData.WebTreeData dsWebTreeData = new wsWebAppData.WebTreeData();
        dsWebTreeData.EnforceConstraints = false;
        dsWebTreeData.ReadXml(xmlFile);
        DataRow[] slideRows = dsWebTreeData.SlideCatInfo.Select(String.Format("FileName='{0}'", fileName));
        if (slideRows.Length > 0)
        {
            return (wsWebAppData.WebTreeData.SlideCatInfoRow)slideRows[0];
        }
        else
        {
            return null;
        }

    }

    private void SaveAndBindMySlides(MySlidesDS.MySlidesDataTable dtMySlides)
    {
        dtMySlides.DefaultView.Sort = "SortOrder ASC";
        Session["MySlides"] = dtMySlides;
        DataListMySlides.DataSource = dtMySlides.DefaultView;
        DataListMySlides.DataBind();
    }


    #endregion


}

public class HistoryItem
{
    public bool IsCurrentItem;
    public string Text;
}