<%@ Page Language="C#" AutoEventWireup="true" CodeFile="NotFound.aspx.cs" Inherits="NotFound" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>HP Printing Sales Guide</title>
</head>
<body style="margin: 5px;">
    <form id="form1" runat="server">
    <img src="images/banner.jpg" alt="Deck Manager" />
    <br /><br />
    <span style="color: #696969; font-weight: bold; font-family: Calibri,Tahoma,Verdana,Arial;">
        The resource you are looking for cannot be found or is unavailable at this time. 
    </span>
    </form>
</body>
</html>
