using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Admin
{
    public partial class SupportFile : MasterForm
    {
        public SupportFile()
        {
            InitializeComponent();
        }

        private void tsbMainNewFile_Click(object sender, EventArgs e)
        {
            SupportFileSave supportSave = new SupportFileSave(SupportFileSave.Mode.New, -1);
            supportSave.Owner = this;
            supportSave.ShowDialog();
        }

        private void SupportFile_Load(object sender, EventArgs e)
        {
			RefreshSupportFiles();
            GetSupportFiles();
            PopulateSupportFileList();
		}

		private void RefreshSupportFiles()
		{
			DataSets.MyRegionLanguages dsMyRegLangs = new Admin.DataSets.MyRegionLanguages();
			dsMyRegLangs = ((Admin.AdminMain)this.Owner).GetMyRegionLanguages();

			DataTable dtMyRegLangsWithNames = new DataTable();
			dtMyRegLangsWithNames.Columns.Add("RegionLanguageID", typeof(int));
			dtMyRegLangsWithNames.Columns.Add("RegionLanguageName", typeof(string));

			wsIpgAdmin.SupportFileDS dsSupportFile = admin.RegionsAndLanguagesGetForSupportFiles();
			
			
			//get the RegionLanguages
			dtMyRegLangsWithNames.Rows.Add(new object[] { -1, "Show All from Selected Region/Languages" });
			foreach (DataSets.MyRegionLanguages.MyRegionLanguagesRow myRegLang in dsMyRegLangs._MyRegionLanguages.Rows)
			{
				string regLangName = dsSupportFile.RegionLanguage.FindByRegionLanguageID(myRegLang.RegionLanguageID).RegionRow.RegionName + " - " +
				dsSupportFile.RegionLanguage.FindByRegionLanguageID(myRegLang.RegionLanguageID).LanguageRow.LongName;
				dtMyRegLangsWithNames.Rows.Add(new object[] { myRegLang.RegionLanguageID, regLangName });
			}
			toolStripComboBoxRegionLanguage.ComboBox.DataSource = dtMyRegLangsWithNames;
			toolStripComboBoxRegionLanguage.ComboBox.ValueMember = dtMyRegLangsWithNames.Columns[0].ToString();
			toolStripComboBoxRegionLanguage.ComboBox.DisplayMember = dtMyRegLangsWithNames.Columns[1].ToString();
			
		}

        private void GetSupportFiles()
        {
			if (toolStripComboBoxRegionLanguage.SelectedIndex == 0)
			{
				dsSupportFile = admin.SupportFileGetAll();
				bindFiles.DataSource = dsSupportFile.SupportFile;
			}
			else
			{
				int regionLanguageID = (int)toolStripComboBoxRegionLanguage.ComboBox.SelectedValue;
				wsIpgAdmin.Admin admin = new Admin.wsIpgAdmin.Admin();
				wsIpgAdmin.SupportFileDS supportFilesFilteredByRegionLanguage = admin.SupportFilesGetByRegionLanguageID(regionLanguageID);
				bindFiles.DataSource = supportFilesFilteredByRegionLanguage.SupportFile;
			}
        }

        private void PopulateSupportFileList()
        {
            bindFiles.DataSource = dsSupportFile.SupportFile;
            if (dsSupportFile.SupportFile.Rows.Count > 0)
            {
                //we don't want to set the sort property if there is nothing to sort
                //if we do an error will be thrown when the binding source is disposed
                bindFiles.Sort = "FileName ASC";
            }

			// Display the default text
			webFilePreview.DocumentText = "<font face='arial' size='3'><b>To view a support file, right click and select &quot;Show&quot;. Large files may take a while to load.</font></b>";
		}

        private void tsbMainUpdateFile_Click(object sender, EventArgs e)
        {
            if (dgvFiles.SelectedRows.Count == 1)
            {
                int supportFileID = Convert.ToInt32(dgvFiles.SelectedRows[0].Cells[0].Value.ToString());

                SupportFileSave supportFileSaveForm = new SupportFileSave(SupportFileSave.Mode.Edit, supportFileID);
                supportFileSaveForm.Owner = this;
                supportFileSaveForm.ShowDialog();
            }
        }

        public void RefreshForm()
        {
            GetSupportFiles();
            PopulateSupportFileList(); 
        }

        private void tsbMainDeleteFile_Click(object sender, EventArgs e)
        {
            if (dgvFiles.SelectedRows.Count == 1)
            {
                int supportFileID = Convert.ToInt32(dgvFiles.SelectedRows[0].Cells[0].Value.ToString());
                DialogResult result;
                result = MessageBox.Show("Are you sure you want to delete this file?", "Confirm Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    admin.SupportFileDelete(supportFileID, dgvFiles.SelectedRows[0].Cells[1].Value.ToString());
                    if (Properties.Settings.Default.IsNetStorageUtilized)
                    {
                        Utilities.FTP.FTPclient ftp = new Utilities.FTP.FTPclient(Properties.Settings.Default.NetStorageFtp, Properties.Settings.Default.NetStorageUserName, Properties.Settings.Default.NetStoragePassword);
                        ftp.FtpDelete(String.Format("/31505/Ppt/{0}", dgvFiles.SelectedRows[0].Cells[1].Value.ToString()));
                    }
                    RefreshForm();
                }
            }
        }

        public void BrowseToFile(string fileName)
        {
			webFilePreview.Refresh(WebBrowserRefreshOption.Completely);
            string navUri = Properties.Settings.Default.SlideNavigateLocation + fileName;
            webFilePreview.Navigate(navUri);
		}

		/// <summary>
		/// The mouse down event is used to display the context menu for the datagrid view rows and to select the
		/// row that was right clicked.
		/// </summary>
		private void dgvFiles_MouseDown(object sender, MouseEventArgs e)
		{
			DataGridView.HitTestInfo info = dgvFiles.HitTest(e.X, e.Y);

			// Dynamically show the context menu for the slide list
			if ((info.Type == DataGridViewHitTestType.Cell) && ((e.Button & MouseButtons.Right) != 0))
			{
				// Switch selection
				DataGridViewRow selectedRow = dgvFiles.Rows[info.RowIndex];
				selectedRow.Selected = true;

				// Show the context menu
				cmnHelpFiles.Show(dgvFiles, e.X, e.Y);
			}
		}

		/// <summary>
		/// Displays the support file
		/// </summary>
		private void tsbMainShowFile_Click(object sender, EventArgs e)
		{
			// Display the selected file
			BrowseToFile(dgvFiles.SelectedRows[0].Cells[1].Value.ToString());
		}

		private void toolStripComboBoxRegionLanguage_SelectedIndexChanged(object sender, EventArgs e)
		{
			GetSupportFiles();
		}
    }
}