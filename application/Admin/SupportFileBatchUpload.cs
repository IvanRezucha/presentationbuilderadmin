﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Linq;
using Admin.UserControls;

namespace Admin
{
    public partial class SupportFileBatchUpload : MasterForm
    {
        #region properties

        public List<SupportFileItem> SupportFiles { get; set; }

        public wsIpgAdmin.SupportFileDS DsSupportFiles { get; set; }

        #endregion

        public SupportFileBatchUpload()
        {
            InitializeComponent();
            SupportFiles = new List<SupportFileItem>();
            SetupRegionLanguageControl();

        }

        #region intialization
        
        private void SetupRegionLanguageControl()
        {
            wsIpgAdmin.Admin wsAdmin = new Admin.wsIpgAdmin.Admin();
            wsIpgAdmin.SlideLibrary dsSlideLibrary = new Admin.wsIpgAdmin.SlideLibrary();

            dsSlideLibrary = wsAdmin.RegionsAndLanguagesGet();
            regionLanguageGroupGridForSupportFiles.FillRegionLanguages(dsSlideLibrary);
        }

        #endregion

        #region tab page navigation

        private void buttonGoToRegionLanguages_Click(object sender, EventArgs e)
        {
            NavigateToRegionLangaugeTab();
        }

        private void NavigateToRegionLangaugeTab()
        {
            if (SupportFiles.Count > 0)
            {
                tabControlWizard.SelectedTab = tabPageRegionLanguages;
            }
            else
            {
                MessageBox.Show("You must select at least 1 file to upload.", "Minimum file requirement", MessageBoxButtons.OK, MessageBoxIcon.Information);
                tabControlWizard.SelectedTab = tabPageSelectFiles;
            }
        }

        private void buttonGoToUpload_Click(object sender, EventArgs e)
        {
            NavigateToUploadTab();    
        }

        private void NavigateToUploadTab()
        {
            if (regionLanguageGroupGridForSupportFiles.GetSelectedRegionLanguageIDs().Count > 0)
            {
                tabControlWizard.SelectedTab = tabPageUploadProgress;
            }
            else
            {
                MessageBox.Show("You must select at least 1 region/language.", "Minimum region language requirement", MessageBoxButtons.OK, MessageBoxIcon.Information);
                tabControlWizard.SelectedTab = tabPageRegionLanguages;
            }
        } 

        #endregion

        #region add/remove files

        private void toolStripButtonSelectFiles_Click(object sender, EventArgs e)
        {
            OpenFileDialog selectFilesDialog = new OpenFileDialog();
            selectFilesDialog.Multiselect = true;
            if (selectFilesDialog.ShowDialog() == DialogResult.OK)
            {
                AddFilesToCollection(new List<string>(selectFilesDialog.FileNames));
                BindSupportFilesToGrid();
                SetFilesSelectLabel();
            }
        }

        private void toolStripButtonRemoveFiles_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem lvi in listViewSupportFiles.SelectedItems)
            {
                SupportFiles.RemoveAll(sf => sf.Filename == lvi.Text);
            }
            BindSupportFilesToGrid();
            SetFilesSelectLabel();
        }

        private void BindSupportFilesToGrid()
        {
            listViewSupportFiles.Items.Clear();

            foreach (SupportFileItem supportFileItem in SupportFiles)
            {
                ListViewItem listViewItem = new ListViewItem(supportFileItem.Filename);
                listViewItem.SubItems.Add(supportFileItem.Filepath);
                listViewSupportFiles.Items.Add(listViewItem);
            }
        }

        private void AddFilesToCollection(List<string> filepaths)
        {
            foreach (string filepath in filepaths)
            {
                //we only want unique filenames
                if (!SupportFiles.Exists(s => s.Filename == Path.GetFileName(filepath)))
                {
                    SupportFiles.Add(new SupportFileItem { Filepath = filepath });
                }
            }
        }

        private void RemoveFileFromCollection(string filename)
        {
            SupportFiles.Remove(SupportFiles.Find(s => s.Filename == filename));
        }

        private void SetFilesSelectLabel()
        {
            labelFilesSelectCount.Text = String.Format("{0} files selected", SupportFiles.Count);
        }

        #endregion        

        #region manage status actions

        private Admin.UserControls.StatusReport.ActionItem BuildUploadActionItem(string actionItemText, string message,
            Admin.UserControls.StatusReport.ActionItem.StatusResultItems statusResult,
            Admin.UserControls.StatusReport.ActionItem.ActionItemImageItems image)
        {
            Admin.UserControls.StatusReport.ActionItem actionItem = new Admin.UserControls.StatusReport.ActionItem();
            actionItem.Message = message;
            actionItem.StatusResult = statusResult;
            actionItem.ActionItemImage = image;
            actionItem.ActionItemText = actionItemText;
            return actionItem;

        }

        private Admin.UserControls.StatusReport.ActionItem UpdateUploadActionItem(Admin.UserControls.StatusReport.ActionItem actionItem, string message,
            Admin.UserControls.StatusReport.ActionItem.StatusResultItems statusResult,
            Admin.UserControls.StatusReport.ActionItem.ActionItemImageItems image)
        {
            actionItem.Message = message;
            actionItem.StatusResult = statusResult;
            actionItem.ActionItemImage = image;
            return actionItem;
        } 
        #endregion
     
        private void buttonUpload_Click(object sender, EventArgs e)
        {
            buttonUpload.Enabled = false;
            statusReportUpload.Visible = true;
            backgroundWorkerUpload.RunWorkerAsync();
        }

        #region backgroundWorkerUpload

        private void backgroundWorkerUpload_DoWork(object sender, DoWorkEventArgs e)
        {
            //set up some stuff we'll need
            StatusReport.ActionItem setupAction = BuildUploadActionItem("Preparing to upload", "Preparing to upload in progress", StatusReport.ActionItem.StatusResultItems.None, StatusReport.ActionItem.ActionItemImageItems.Updating);
            backgroundWorkerUpload.ReportProgress(0, setupAction);
            wsIpgAdmin.Admin adminService = new Admin.wsIpgAdmin.Admin();
            DsSupportFiles = adminService.SupportFileGetAll();
            List<int> regLangIds = regionLanguageGroupGridForSupportFiles.GetSelectedRegionLanguageIDs();
            System.Net.WebClient wc = new System.Net.WebClient();
            setupAction = UpdateUploadActionItem(setupAction, "Upload preparation complete", StatusReport.ActionItem.StatusResultItems.Success, StatusReport.ActionItem.ActionItemImageItems.Success);
            backgroundWorkerUpload.ReportProgress(0, setupAction);

            foreach (SupportFileItem supportFileItem in SupportFiles)
            {
                bool successOnUpload = false;
                StatusReport.ActionItem statusItem = BuildUploadActionItem(supportFileItem.Filename, String.Format("Uploading {0}", supportFileItem.Filename),
                    StatusReport.ActionItem.StatusResultItems.None, StatusReport.ActionItem.ActionItemImageItems.Updating);
                backgroundWorkerUpload.ReportProgress(0, statusItem);

                //attempt to upload the file
                try
                {
                    string temp = Properties.Settings.Default.SlideUploadURI;
                    wc.UploadFile(Properties.Settings.Default.SlideUploadURI, supportFileItem.Filepath);

                    if (Properties.Settings.Default.IsNetStorageUtilized)
                    {
                        Utilities.FTP.FTPclient ftp = new Utilities.FTP.FTPclient(Properties.Settings.Default.NetStorageFtp, Properties.Settings.Default.NetStorageUserName, Properties.Settings.Default.NetStoragePassword);
                        ftp.Upload(supportFileItem.Filepath, String.Format("/31505/Ppt/{0}", System.IO.Path.GetFileName(supportFileItem.Filepath)));
                    }
                    successOnUpload = true;
                }
                catch (Exception ex)
                {
                    successOnUpload = false;
                    statusItem = UpdateUploadActionItem(statusItem, String.Format("Upload of {0} failed.", supportFileItem.Filename), StatusReport.ActionItem.StatusResultItems.Failure, StatusReport.ActionItem.ActionItemImageItems.Failure);
                    backgroundWorkerUpload.ReportProgress(0, statusItem);
                }

                //if the upload was successfull, we need to update the database
                if (successOnUpload)
                {
                    wsIpgAdmin.SupportFileDS.SupportFileRow drSupportFile;

                    if (DsSupportFiles.SupportFile.Select(String.Format("FileName='{0}'", supportFileItem.Filename)).Length == 0)//we have a new file
                    {
                        drSupportFile = DsSupportFiles.SupportFile.NewSupportFileRow();
                        drSupportFile.FileName = supportFileItem.Filename;
                        drSupportFile.ModifyDate = DateTime.UtcNow;
                        DsSupportFiles.SupportFile.AddSupportFileRow(drSupportFile);
                    }
                    else//this one was already in the db
                    {
                        drSupportFile = (wsIpgAdmin.SupportFileDS.SupportFileRow)DsSupportFiles.SupportFile.Select(String.Format("FileName='{0}'", supportFileItem.Filename))[0];
                        drSupportFile.ModifyDate = DateTime.UtcNow;
                    }

                    //remove all reg langs
                    foreach (wsIpgAdmin.SupportFileDS.SupportFileRegionLanguageRow supportFileRegLang in drSupportFile.GetSupportFileRegionLanguageRows())
                    {
                        supportFileRegLang.Delete();
                    }

                    //add the selected reg langs
                    foreach (int regLangId in regLangIds)
                    {
                        DsSupportFiles.SupportFileRegionLanguage.AddSupportFileRegionLanguageRow(drSupportFile, DsSupportFiles.RegionLanguage.FindByRegionLanguageID(regLangId));
                    }

                    statusItem = UpdateUploadActionItem(statusItem, String.Format("Upload of {0} completed successfully.", supportFileItem.Filename), StatusReport.ActionItem.StatusResultItems.Success, StatusReport.ActionItem.ActionItemImageItems.Success);
                    backgroundWorkerUpload.ReportProgress(0, statusItem);
                }
            }

            StatusReport.ActionItem dbActionItem = BuildUploadActionItem("Update Database", "Updating database with uploaded files", StatusReport.ActionItem.StatusResultItems.None, StatusReport.ActionItem.ActionItemImageItems.Updating);
            backgroundWorkerUpload.ReportProgress(0, dbActionItem);
            try
            {
                //update the db
                adminService.SupportFileSaveMultiple((wsIpgAdmin.SupportFileDS)DsSupportFiles.GetChanges());
                UpdateUploadActionItem(dbActionItem, "Database update complete", StatusReport.ActionItem.StatusResultItems.Success, StatusReport.ActionItem.ActionItemImageItems.Success);
                backgroundWorkerUpload.ReportProgress(0, dbActionItem);
            }
            catch (Exception ex)
            {
                UpdateUploadActionItem(dbActionItem, "Database update failed", StatusReport.ActionItem.StatusResultItems.Failure, StatusReport.ActionItem.ActionItemImageItems.Failure);
                backgroundWorkerUpload.ReportProgress(0, dbActionItem);
            }
        }

        private void backgroundWorkerUpload_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (((StatusReport.ActionItem)e.UserState).StatusResult == StatusReport.ActionItem.StatusResultItems.None)
            {
                statusReportUpload.AddActionItem((StatusReport.ActionItem)e.UserState);
            }
            else
            {
                statusReportUpload.UpdateActionItem((StatusReport.ActionItem)e.UserState);
            }
        }

        private void backgroundWorkerUpload_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            statusReportUpload.AllItemsCompleted();
            buttonUploadAnotherBatch.Visible = true;
            buttonFinished.Visible = true;
            
        } 

        #endregion

        private void buttonFinished_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonUploadAnotherBatch_Click(object sender, EventArgs e)
        {
            SupportFileBatchUpload uploadForm = new SupportFileBatchUpload();
            uploadForm.Owner = this.Owner;
            this.Hide();
            uploadForm.ShowDialog();
            this.Close();
        }
    }

    public class SupportFileItem
    {
        public string Filepath { get; set; }
        public string Filename 
        {
            get 
            {
                return Path.GetFileName(Filepath);
            }
        }
    }
}
