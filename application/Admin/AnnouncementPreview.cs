﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Admin
{
    public partial class AnnouncementPreview : Form
    {
        private string navigateUrl;
        private int announcementId;

        public AnnouncementPreview()
        {
            InitializeComponent();
        }

        public AnnouncementPreview(string navigateUrl, int announcementId)
        {
            InitializeComponent();
            this.navigateUrl = navigateUrl;
            this.announcementId = announcementId;
            webBrowser1.Navigate(this.navigateUrl);
        }

        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {

        }

        private void AnnouncementPreview_FormClosed(object sender, FormClosedEventArgs e)
        {
            Admin.wsIpgAdmin.Admin announcementWebService = new Admin.wsIpgAdmin.Admin();
            announcementWebService.DeleteAnnouncementByAnnouncementIdAsync(announcementId);
        }
    }
}
