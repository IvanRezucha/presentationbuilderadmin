using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Net;
using Admin.UserControls;

namespace Admin
{
	public partial class SlideFileDropDialog : MasterForm
	{
		#region Fields

		private List<string> _files = new List<string>();
		private List<string> _thumbnails;
		private List<int> _slideIDs = new List<int>();
		private HpDm.DAL.Slide _slideDAL;
		private HpDm.BLL.FileUploader _fileUploader = new HpDm.BLL.FileUploader();
		private HpDm.BLL.FileUploader _thumbnailUploader = new HpDm.BLL.FileUploader();
		private wsIpgAdmin.SlideLibrary _slideLibrary = new Admin.wsIpgAdmin.SlideLibrary();
		private Admin.UserControls.SlideFiltersAndLanguagesFillType _filterAndRegLangFillType;
		private int _outstandingAsyncCalls;
		private readonly int _slideUploadLimit = 4000;
		private int _regionLanguage;
		private StatusReport.ActionItem _dataUpdateAction = new StatusReport.ActionItem("Update Slide Data");
		private StatusReport.ActionItem _uploadSlidesAction = new StatusReport.ActionItem("Upload Slide Files");
		private StatusReport.ActionItem _uploadThumbnailsAction = new StatusReport.ActionItem("Upload Thumbnail Files");
		private StatusReport.ActionItem _ftpSlidesNetStorageAction = new StatusReport.ActionItem("Distributing Slide Files");
		private StatusReport.ActionItem _ftpThumbnailsNetStorageAction = new StatusReport.ActionItem("Distributing Thumbnail Files");
		private StatusReport.ActionItem _compressSlidesAction = new StatusReport.ActionItem("Compressing Slides");
		private bool _criticalError = false;

		#endregion

		#region Properties
		
		public List<int> SlideIDs
		{
			get { return _slideIDs; }
			set { _slideIDs = value; }
		}
		public List<string> Files
		{
			get { return _files; }
			set { _files = value; }
		}
		public List<string> Thumbnails
		{
			get { return _thumbnails; }
			set { _thumbnails = value; }
		}
		public HpDm.DAL.Slide SlideDAL
		{
			get { return _slideDAL; }
			set { _slideDAL = value; }
		}
		public HpDm.BLL.FileUploader FileUploader
		{
			get { return _fileUploader; }
			set { _fileUploader = value; }
		}
		public HpDm.BLL.FileUploader ThumbnailUploader
		{
			get { return _thumbnailUploader; }
			set { _thumbnailUploader = value; }
		}
		public wsIpgAdmin.SlideLibrary SlideLibrary
		{
			get { return _slideLibrary; }
			set { _slideLibrary = value; }
		}
		public SlideFiltersAndLanguagesFillType FilterAndRegLangFillType
		{
			get { return _filterAndRegLangFillType; }
			set { _filterAndRegLangFillType = value; }
		}
		public int OutstandingAsyncCalls
		{
			get { return _outstandingAsyncCalls; }
			set { _outstandingAsyncCalls = value; }
		}
		public int SlideUploadLimit
		{
			get { return _slideUploadLimit; }
		}
		public int RegionLanguage
		{
			get { return _regionLanguage; }
			set { _regionLanguage = value; }
		}
		public StatusReport.ActionItem DataUpdateAction
		{
			get { return _dataUpdateAction; }
			set { _dataUpdateAction = value; }
		}
		public StatusReport.ActionItem UploadSlidesAction
		{
			get { return _uploadSlidesAction; }
			set { _uploadSlidesAction = value; }
		}
		public StatusReport.ActionItem FtpSlidesNetStorageAction
		{
			get { return _ftpSlidesNetStorageAction; }
			set { _ftpSlidesNetStorageAction = value; }
		}
		public StatusReport.ActionItem CompressSlidesAction
		{
			get { return _compressSlidesAction; }
			set { _compressSlidesAction = value; }
		}
		public StatusReport.ActionItem FtpThumbnailsNetStorageAction
		{
			get { return _ftpThumbnailsNetStorageAction; }
			set { _ftpThumbnailsNetStorageAction = value; }
		}
		public StatusReport.ActionItem UploadThumbnailsAction
		{
			get { return _uploadThumbnailsAction; }
			set { _uploadThumbnailsAction = value; }
		}
		public bool CriticalError
		{
			get { return _criticalError; }
			set { _criticalError = value; }
		}

		#endregion
		
		public SlideFileDropDialog(List<string> files, int regionLanguage)
		{
			InitializeComponent();
			
			// Initialize class properties
			Files = files;

			RegionLanguage = regionLanguage;
			SlideDAL = new HpDm.DAL.Slide();
			spliterForm.Panel2Collapsed = true;
			Height -= pnUploadStatus.Height;

			// Initialize async completed and progress events
			SlideDAL.GetByFileNamesCompleted += new HpDm.DAL.Slide.GetByFileNamesCompletedDelegate(SlideDAL_GetByFileNamesCompleted);
			SlideDAL.UpdateCompleted += new HpDm.DAL.Slide.UpdateCompletedDelegate(SlideDAL_UpdateCompleted);
			FileUploader.UploadSlideFilesCompleted += new HpDm.BLL.FileUploader.UploadSlideFilesCompletedDelegate(FileUploader_UploadSlideFilesCompleted);
			ThumbnailUploader.UploadSlideFilesCompleted += new HpDm.BLL.FileUploader.UploadSlideFilesCompletedDelegate(ThumbnailUploader_UploadSlideFilesCompleted);
		}

		private void SlideFileDropDialog_Load(object sender, EventArgs e)
		{

			StringBuilder delimitedFileNameList = new StringBuilder(0, 4000);
			
			// Convert the generic list into a comma delimited list.
			foreach (string fileName in Files)
			{
				delimitedFileNameList.Append(Path.GetFileName(fileName) + ',');
			}

			// The limit the form to a maximum number of slides.
			if (delimitedFileNameList.Length >= SlideUploadLimit)
			{
				string msg = "The maximum number of slides allowed to upload at once has been exceeded.  Please upload sets of slides in smaller batches.";
				MessageBox.Show("Upload maximum exceeded", msg, MessageBoxButtons.OK, MessageBoxIcon.Information);
			}

			// Pass in our list of files to the webservice and see which ones are a match
			SlideDAL.GetByFileNamesAsync(delimitedFileNameList.ToString(0, delimitedFileNameList.Replace(".ppt", ".pot").Length - 1));

		}

		#region Async Completed and Progress Events

		private void SlideDAL_GetByFileNamesCompleted(bool sucess, Admin.wsIpgAdmin.SlideLibrary slideLibrary)
		{
			if (!sucess)
			{
				return;
			}

			// Set the SlideLibrary property
			SlideLibrary.Merge(slideLibrary);

			// Separate new files and existing files in to two datagridviews.
			foreach (string file in Files)
			{
				string fileName = Path.GetFileNameWithoutExtension(file) + ".pot";
				wsIpgAdmin.SlideLibrary.SlideRow[] drSlide = (wsIpgAdmin.SlideLibrary.SlideRow[])SlideLibrary.Slide.Select("FileName = '" + fileName + "'");

				if (drSlide.Length > 0)
				{
					dgvExists.Rows.Add(new object[2] {drSlide[0].Title, drSlide[0].FileName});
					drSlide[0].DateModified = DateTime.Now;
				}
				else
				{
					string fileTitle = PlugIns.Helpers.PptUtils.GetSlideTitle(file, 1, @"lic\Aspose.Slides.lic");
					SlideLibrary.Slide.AddSlideRow(fileTitle, fileName, DateTime.Now, 0,0,DateTime.Now);
					dgvNewSlides.Rows.Add(new object[2] {fileTitle, fileName});
				}
			}

			// Add the SlideRegionLanguageRow that is associated to the the region/lanuage that we drag/dropped on to.
			foreach (wsIpgAdmin.SlideLibrary.SlideRow slideRow in SlideLibrary.Slide.Rows)
			{
				wsIpgAdmin.SlideLibrary.SlideRegionLanguageRow srlRow = SlideLibrary.SlideRegionLanguage.FindBySlideIDRegionLanguageID(slideRow.SlideID, RegionLanguage);

				if (srlRow == null)
				{
					srlRow = SlideLibrary.SlideRegionLanguage.NewSlideRegionLanguageRow();

					srlRow.SlideID = slideRow.SlideID;
					srlRow.RegionLanguageID = RegionLanguage;

					SlideLibrary.SlideRegionLanguage.AddSlideRegionLanguageRow(srlRow);
				}
			}

			// Fill slideLibrary dataset with the region and language and filters.
			CustomFeaturesSetup();

			// Make sure that both grids are not selected.
			SetSelectedSlideSettings();

		}

		private void SlideDAL_UpdateCompleted(bool success, Admin.wsIpgAdmin.SlideLibrary slideLibrary)
		{
			// Capture the updated slide IDs and store them in the SlideIDs property which gets used by the CategorySlideTreeView
			foreach (wsIpgAdmin.SlideLibrary.SlideRow slideRow in slideLibrary.Slide.Rows)
			{
				SlideIDs.Add(slideRow.SlideID);
			}

			btnClose.Enabled = true;


			if (success)
			{
				DataUpdateAction.ActionItemImage = StatusReport.ActionItem.ActionItemImageItems.Success;
				DataUpdateAction.Message = string.Empty;
				DataUpdateAction.StatusResult = StatusReport.ActionItem.StatusResultItems.Success;
			}
			else
			{
				DataUpdateAction.ActionItemImage = StatusReport.ActionItem.ActionItemImageItems.Failure;
				DataUpdateAction.Message = "Data update failed. Slides did not upload.";
				DataUpdateAction.StatusResult = StatusReport.ActionItem.StatusResultItems.Failure;
			}

			statusReport.UpdateActionItem(DataUpdateAction);
			statusReport.AllItemsCompleted();
		}

		private void FileUploader_UploadSlideFilesCompleted(List<string> failedUploadFiles)
		{
			if (failedUploadFiles.Count > 0)
			{
				UploadSlidesAction.ActionItemImage = StatusReport.ActionItem.ActionItemImageItems.Failure;
				UploadSlidesAction.Message = failedUploadFiles.Count + "slide(s) failed to upload.";
				UploadSlidesAction.StatusResult = StatusReport.ActionItem.StatusResultItems.Failure;
				statusReport.UpdateActionItem(UploadSlidesAction);
				CriticalError = true;
				return;
			}

			// Success
			UploadSlidesAction.ActionItemImage = StatusReport.ActionItem.ActionItemImageItems.Success;
			UploadSlidesAction.StatusResult = StatusReport.ActionItem.StatusResultItems.Success;
			statusReport.UpdateActionItem(UploadSlidesAction);
			
			UpdateData();
		}

		private void ThumbnailUploader_UploadSlideFilesCompleted(List<string> failedUploadFiles)
		{
			if (failedUploadFiles.Count > 0)
			{
				UploadThumbnailsAction.ActionItemImage = StatusReport.ActionItem.ActionItemImageItems.Failure;
				UploadThumbnailsAction.Message = failedUploadFiles.Count + "thumbnail(s) failed to upload.";
				UploadThumbnailsAction.StatusResult = StatusReport.ActionItem.StatusResultItems.Failure;
				statusReport.UpdateActionItem(UploadThumbnailsAction);
				CriticalError = true;
				return;
			}

			// Success
			UploadThumbnailsAction.ActionItemImage = StatusReport.ActionItem.ActionItemImageItems.Success;
			UploadThumbnailsAction.StatusResult = StatusReport.ActionItem.StatusResultItems.Success;
			statusReport.UpdateActionItem(UploadThumbnailsAction);

			UpdateData();
		}

		private void workerFileUploadNetStorage_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			if (e.Error != null || (e.Result != null && e.Result.GetType() == typeof(Exception)))
			{
				FtpSlidesNetStorageAction.ActionItemImage = StatusReport.ActionItem.ActionItemImageItems.Failure;
				FtpSlidesNetStorageAction.Message = ((Exception)e.Result).Message; 
				FtpSlidesNetStorageAction.StatusResult = StatusReport.ActionItem.StatusResultItems.Failure;
				statusReport.UpdateActionItem(FtpSlidesNetStorageAction);
				CriticalError = true;
				return;
			}

			// Success
			FtpSlidesNetStorageAction.ActionItemImage = StatusReport.ActionItem.ActionItemImageItems.Success;
			FtpSlidesNetStorageAction.StatusResult = StatusReport.ActionItem.StatusResultItems.Success;
			statusReport.UpdateActionItem(FtpSlidesNetStorageAction);

			UpdateData();
		}

		private void workerThumbnailUploadNetStorage_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			if (e.Error != null || (e.Result != null && e.Result.GetType() == typeof(Exception)))
			{
				FtpThumbnailsNetStorageAction.ActionItemImage = StatusReport.ActionItem.ActionItemImageItems.Failure;
				FtpThumbnailsNetStorageAction.Message = ((Exception)e.Result).Message; 
				FtpThumbnailsNetStorageAction.StatusResult = StatusReport.ActionItem.StatusResultItems.Failure;
				statusReport.UpdateActionItem(FtpThumbnailsNetStorageAction);
				CriticalError = true;
				return;
			}

			// Success
			FtpThumbnailsNetStorageAction.ActionItemImage = StatusReport.ActionItem.ActionItemImageItems.Success;
			FtpThumbnailsNetStorageAction.StatusResult = StatusReport.ActionItem.StatusResultItems.Success;
			statusReport.UpdateActionItem(FtpThumbnailsNetStorageAction);

			UpdateData();
		}

		#endregion

		/// <summary>
		/// Initializes both grids
		/// </summary>
		private void SetSelectedSlideSettings()
		{

			// Setup grids to not allow user to add rows.
			dgvNewSlides.AllowUserToAddRows = false;
			dgvExists.AllowUserToAddRows = false;

			// Only one grid can be selected at a time.  Determine which grid gets selected and then setup the filter and region/language settings.
			if (dgvNewSlides.Rows.Count == 0)
			{
				dgvExists.Rows[0].Selected = true;
				SetFilterAndRegionLanguages(dgvExists["FileNameExist", 0].Value.ToString());
			}
			else
			{
				dgvNewSlides.Rows[0].Selected = true;
				SetFilterAndRegionLanguages(dgvNewSlides["FileName", 0].Value.ToString());
				
				// There are rows in the existing datagrid deselect that row so that there is only one row selected.
				if (dgvExists.SelectedRows.Count > 0)
				{
					dgvExists.Rows[0].Selected = false;
				}
			}
		}

		/// <summary>
		/// Sets up configurable features.
		/// </summary>
		private void CustomFeaturesSetup()
		{
			if (Properties.Settings.Default.IsFilterManagementEnabled || Properties.Settings.Default.IsRegionManagmentEnabled)
			{
				FilterAndRegLangFillType = SlideFiltersAndLanguagesFillType.FiltersAndRegionLanguages;

				if (Properties.Settings.Default.IsFilterManagementEnabled && !Properties.Settings.Default.IsRegionManagmentEnabled)
				{
					FilterAndRegLangFillType = SlideFiltersAndLanguagesFillType.FiltersOnly;
				}
				else if (!Properties.Settings.Default.IsFilterManagementEnabled && Properties.Settings.Default.IsRegionManagmentEnabled)
				{
					FilterAndRegLangFillType = SlideFiltersAndLanguagesFillType.RegionLanguagesOnly;
				}

				slideFiltersAndLanguages.Fill(SlideLibrary, FilterAndRegLangFillType);
			}
			else
			{
				Height -= slideFiltersAndLanguages.Height;
				spliterUpload.Panel2Collapsed = true;
			}
		}

		/// <summary>
		/// Sets the filter and region/language settings for a particular slide.
		/// </summary>
		/// <param name="fileName">The primary key used to determine the associated filter and region/language settings</param>
		private void SetFilterAndRegionLanguages(string fileName)
		{
			// Reset filters and region/languages
            slideFiltersAndLanguages.filterTabs.CheckAllFilters(); //*SS Changed from UnCheckAllFilters to CheckAllGIlters
			slideFiltersAndLanguages.regionLanguageGroupGrid.UnCheckAllSlideRegionLanguages();

			int slideID = ((wsIpgAdmin.SlideLibrary.SlideRow[])SlideLibrary.Slide.Select("FileName = '" + fileName + "'"))[0].SlideID;
			List<int> slideFilterIDs = new List<int>();
			List<int> regionLangIDs = new List<int>();
			DataRow[] slideFilterArray = SlideLibrary.SlideFilter.Select("SlideID  = " + slideID);
			DataRow[] regionLangArray = SlideLibrary.SlideRegionLanguage.Select("SlideID  = " + slideID);

			// Get a list of all the associated filterIDs
			foreach (wsIpgAdmin.SlideLibrary.SlideFilterRow slideFilterRow in (wsIpgAdmin.SlideLibrary.SlideFilterRow[])slideFilterArray)
			{
				slideFilterIDs.Add(slideFilterRow.FilterID);
			}

			// Get a list of all the associated RegionLanguageIDs
			foreach (wsIpgAdmin.SlideLibrary.SlideRegionLanguageRow slideRegLangRow in (wsIpgAdmin.SlideLibrary.SlideRegionLanguageRow[])regionLangArray)
			{
				regionLangIDs.Add(slideRegLangRow.RegionLanguageID);
			}

			slideFiltersAndLanguages.filterTabs.CheckAllFilters();
			slideFiltersAndLanguages.regionLanguageGroupGrid.CheckSlideRegionLanguages(regionLangIDs);

		}

		/// <summary>
		/// Saves the slide title, filter settings, and region/language settings to the local dataset
		/// </summary>
		/// <param name="title">The slide title</param>
		/// <param name="fileName">The slide file name</param>
		private void SaveSlideSettings(string title, string fileName)
		{
			if (title.Length > 0 && fileName.Length > 0)
			{
				List<int> filterIDs = slideFiltersAndLanguages.filterTabs.GetSelectedFilterIDs();
				List<int> regionLanguageIDs = slideFiltersAndLanguages.regionLanguageGroupGrid.GetSelectedRegionLanguageIDs();
				wsIpgAdmin.SlideLibrary.SlideRow slideRow = (wsIpgAdmin.SlideLibrary.SlideRow)(SlideLibrary.Slide.Select("FileName = '" + fileName + "'")[0]);

				// Save slide title
				slideRow.Title = title;

				// Delete all filter relationship records for this group of slides
				foreach (wsIpgAdmin.SlideLibrary.SlideFilterRow sfRow in slideRow.GetSlideFilterRows())
				{
					sfRow.Delete();
				}

				// Add new slide filter records
				foreach (int filterID in filterIDs)
				{
					SlideLibrary.SlideFilter.Rows.Add(new object[2] { filterID, slideRow.SlideID });
				}

				// Delete all region/language relationship records for this group of slides
				foreach (wsIpgAdmin.SlideLibrary.SlideRegionLanguageRow srlRow in slideRow.GetSlideRegionLanguageRows())
				{
					srlRow.Delete();
				}

				// Add new slide region/language records
				foreach (int regionLanguageID in regionLanguageIDs)
				{
					SlideLibrary.SlideRegionLanguage.Rows.Add(new object[2] { slideRow.SlideID, regionLanguageID });
				}
			}
		}

		/// <summary>
		/// Valids the business rules of the form like each slide must have an associated filter and region/language.
		/// </summary>
		/// <returns>True if valid</returns>
		private bool ValidForm()
		{
			bool areAllSlidesAssociatedWithDropCategory = true;
			bool matchRegionLanguage;
			
			errorProvider.Clear();

			// Check each slide to see if it has associated filters or region/languages
			foreach (wsIpgAdmin.SlideLibrary.SlideRow slideRow in SlideLibrary.Slide.Rows)
			{
				wsIpgAdmin.SlideLibrary.SlideFilterRow[] slideFilterRows = slideRow.GetSlideFilterRows();
				wsIpgAdmin.SlideLibrary.SlideRegionLanguageRow[] slideRegioLangRows = slideRow.GetSlideRegionLanguageRows();

				// Error checking for filters
				if (slideFilterRows.Length == 0)
				{
					string msg = "Every slide needs to be associated to at least one filter.  The slide titled '" + slideRow.FileName + "' currently does not have an associated filter.";
					errorProvider.SetError(slideFiltersAndLanguages, msg);

					// Select the row in question
					SelectGridRowByFileName(slideRow.FileName);
					return false;
				}

				// Error checking for region/languages
				if (slideRegioLangRows.Length == 0)
				{
					string msg = "Every slide needs to be associated to at least one region/language.  The slide titled '" + slideRow.FileName + "' currently does not have an associated region/language.";
					errorProvider.SetError(slideFiltersAndLanguages, msg);

					// Select the row in question
					SelectGridRowByFileName(slideRow.FileName);
					return false;
				}
			}

			return true;
		}

		/// <summary>
		/// Selects the datagridview row, from either grid, by filename.
		/// </summary>
		/// <param name="fileName">Key value - filename</param>
		private void SelectGridRowByFileName(string fileName)
		{
			// New slides grid
			foreach (DataGridViewRow gridRow in dgvNewSlides.Rows)
			{
				if (gridRow.Cells[1].Value.ToString() == fileName)
				{
					dgvNewSlides.Focus();
					dgvNewSlides.CurrentCell = gridRow.Cells[0];
					gridRow.Selected = true;
					
					dgvNewSlides_Click(null, null);

					return;
				}
			}

			// Existing slides grid
			foreach (DataGridViewRow gridRow in dgvExists.Rows)
			{
				if (gridRow.Cells[1].Value.ToString() == fileName)
				{
					dgvExists.Focus();
					dgvExists.CurrentCell = gridRow.Cells[0];
					gridRow.Selected = true;
					
					dgvExists_Click(null, null);

					return;
				}
			}
		}

		/// <summary>
		/// Once all the asynchronous calls have completed examine the results and upload the data.
		/// </summary>
		private void UpdateData()
		{
			// Decrement number of outanding async calls
			OutstandingAsyncCalls--;

			if (CriticalError)
			{
				DataUpdateAction.ActionItemImage = StatusReport.ActionItem.ActionItemImageItems.Warning;
				DataUpdateAction.StatusResult = StatusReport.ActionItem.StatusResultItems.Warning;
				DataUpdateAction.Message = "This process was not completed due to an error.";
				statusReport.UpdateActionItem(DataUpdateAction);

				btnClose.DialogResult = DialogResult.Cancel;
				btnClose.Enabled = true;
				
				return;
			}

			if (OutstandingAsyncCalls == 0)
			{
				DataUpdateAction.ActionItemImage = StatusReport.ActionItem.ActionItemImageItems.Updating;
				statusReport.UpdateActionItem(DataUpdateAction);

				SlideDAL.UpdateAsync(SlideLibrary);
			}
		}

		#region Form Events

		private void btnClose_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void btnUploadFiles_Click(object sender, EventArgs e)
		{
			string slideFileName = string.Empty;
			string title = string.Empty;

			#region Save any settings that have not been updated to the local dataset

			if (dgvExists.SelectedRows.Count > 0)
			{
				int rowIndex = dgvExists.SelectedRows[0].Index;
				dgvExists.Rows[rowIndex].Selected = false;

				// Commit any changes from the datagrid
				dgvExists.EndEdit();
				slideFileName = dgvExists["FileNameExist", rowIndex].Value.ToString();
				title = dgvExists["SlideTitleExist", rowIndex].Value.ToString();
			}
			else if (dgvNewSlides.SelectedRows.Count > 0)
			{
				int rowIndex = dgvNewSlides.SelectedRows[0].Index;
				dgvNewSlides.Rows[rowIndex].Selected = false;

				// Commit any changes from the datagrid
				dgvNewSlides.EndEdit();
				slideFileName = dgvNewSlides["FileName", rowIndex].Value.ToString();
				title = dgvNewSlides["SlideTitle", rowIndex].Value.ToString();
			}

			SaveSlideSettings(title, slideFileName);

			#endregion

			if (!ValidForm())
			{
				return;
			}

			// Declarations
			Thumbnails = new List<string>(_files.Count);
			OutstandingAsyncCalls = Properties.Settings.Default.IsNetStorageUtilized ? 4 : 2;

			// Hide upload button and spliterForm.panel1
			btnClose.Enabled = false;
			spliterForm.Panel1Collapsed = true;
			btnUploadFiles.Visible = false;
			spliterForm.Panel2Collapsed = false;
	
			#region Compress Slides
			
			if (chkCompress.Checked)
			{
				// Add action item to status report
				CompressSlidesAction.ActionItemImage = StatusReport.ActionItem.ActionItemImageItems.Updating;
				CompressSlidesAction = statusReport.AddActionItem(CompressSlidesAction);

				try
				{
					PlugIns.Helpers.PowerPointCompressor compressor = new PlugIns.Helpers.PowerPointCompressor();
					List<string> tempFiles = new List<string>();

					foreach (string file in Files)
					{
						compressor.CompressPresentation(file, ((AdminMain)this.Owner).AppTempDirectory, true, true, 9, true);
						//replace the slide file path with the path to the compressed slide
						tempFiles.Add(Path.Combine(((AdminMain)this.Owner).AppTempDirectory, Path.GetFileName(file)));
					}

					Files.Clear();
					Files.AddRange(tempFiles);

					CompressSlidesAction.ActionItemImage = StatusReport.ActionItem.ActionItemImageItems.Success;
					CompressSlidesAction.StatusResult = StatusReport.ActionItem.StatusResultItems.Success;
					statusReport.UpdateActionItem(CompressSlidesAction);
				}
				catch (Exception ex)
				{
					CompressSlidesAction.ActionItemImage = StatusReport.ActionItem.ActionItemImageItems.Warning;
					CompressSlidesAction.StatusResult = StatusReport.ActionItem.StatusResultItems.Warning;
					CompressSlidesAction.Message = ex.Message;
					statusReport.UpdateActionItem(CompressSlidesAction);
				}
			}

			#endregion

			// Upload slides to Rackspace
			UploadSlidesAction.ActionItemImage = StatusReport.ActionItem.ActionItemImageItems.Updating;
			UploadSlidesAction = statusReport.AddActionItem(UploadSlidesAction);
			
			FileUploader.UploadSlideFilesAsync(Files, Admin.Properties.Settings.Default.SlideUploadURI, false);
			
			#region Create Thumbnails

			foreach (string file in Files)
			{
				string fileName = Path.GetFileName(file);

				// Add the thumbnail file to the list of thumbnail files
				Thumbnails.Add(Path.Combine(((AdminMain)this.Owner).AppTempDirectory, fileName.Replace(".ppt", ".jpg").Replace(".pot", ".jpg")));

				// Create the thumbnail
				if (File.Exists(file))
				{
					PlugIns.Helpers.PptUtils.WriteSlideImage(file,
						Path.Combine(((AdminMain)this.Owner).AppTempDirectory, fileName.Replace(".ppt", ".jpg").Replace(".pot", ".jpg")),
						0, 96, 72, System.Drawing.Imaging.ImageFormat.Jpeg, Path.Combine(Application.StartupPath, @"lic\Aspose.Slides.lic"));
				}
			}

			#endregion

			UploadThumbnailsAction.ActionItemImage = StatusReport.ActionItem.ActionItemImageItems.Updating;
			UploadThumbnailsAction = statusReport.AddActionItem(UploadThumbnailsAction);
			
			// Upload thumbnails to Rackspace
			ThumbnailUploader.UploadSlideFilesAsync(Thumbnails, Admin.Properties.Settings.Default.ThumbnailUploadURI, false);

			#region NetStorage

			if (Properties.Settings.Default.IsNetStorageUtilized)
			{
				FtpThumbnailsNetStorageAction.ActionItemImage = StatusReport.ActionItem.ActionItemImageItems.Updating;
				FtpThumbnailsNetStorageAction = statusReport.AddActionItem(FtpThumbnailsNetStorageAction);

				// Upload slides to NetStorage
				workerThumbnailUploadNetStorage.RunWorkerAsync();

				FtpSlidesNetStorageAction.ActionItemImage = StatusReport.ActionItem.ActionItemImageItems.Updating;
				FtpSlidesNetStorageAction = statusReport.AddActionItem(FtpSlidesNetStorageAction);

				// Upload thumbnails to NetStorage
				workerFileUploadNetStorage.RunWorkerAsync();
			}

			#endregion

			DataUpdateAction = statusReport.AddActionItem(DataUpdateAction);
		}

		private void dgvNewSlides_Click(object sender, EventArgs e)
		{

			// Both grids should not show selected records at the same time.
			if (dgvExists.SelectedRows.Count > 0)
			{
				int rowIndex = dgvExists.SelectedRows[0].Index;
				dgvExists.Rows[rowIndex].Selected = false;

				// Commit any changes from the datagrid
				dgvExists.EndEdit();
				string fileName = dgvExists["FileNameExist", rowIndex].Value.ToString();
				string title = dgvExists["SlideTitleExist", rowIndex].Value.ToString();

				SaveSlideSettings(title, fileName);
			}

			SetFilterAndRegionLanguages(dgvNewSlides.CurrentRow.Cells[1].Value.ToString());
		}

		private void dgvExists_Click(object sender, EventArgs e)
		{

			// Both grids should not show selected records at the same time.
			if (dgvNewSlides.SelectedRows.Count > 0)
			{
				int rowIndex = dgvNewSlides.SelectedRows[0].Index;
				dgvNewSlides.Rows[rowIndex].Selected = false;

				// Commit any changes from the datagrid
				dgvNewSlides.EndEdit();
				string fileName = dgvNewSlides["FileName", rowIndex].Value.ToString();
				string title = dgvNewSlides["SlideTitle", rowIndex].Value.ToString();

				SaveSlideSettings(title, fileName);
			}

			SetFilterAndRegionLanguages(dgvExists.CurrentRow.Cells[1].Value.ToString());
		}

		private void workerFileUploadNetStorage_DoWork(object sender, DoWorkEventArgs e)
		{
			Utilities.FTP.FTPclient ftp = new Utilities.FTP.FTPclient(Properties.Settings.Default.NetStorageFtp, Properties.Settings.Default.NetStorageUserName, Properties.Settings.Default.NetStoragePassword);
			List<string> failedUploadFiles = new List<string>();

			foreach (string file in Files)
			{
				if (!ftp.Upload(file, String.Format("/31505/Ppt/{0}.pot", Path.GetFileNameWithoutExtension(file))))
				{
					failedUploadFiles.Add(file);
				}
			}

			if (failedUploadFiles.Count > 0)
			{
				e.Result = new Exception(failedUploadFiles.Count + " files failed to upload.");
			}
		}

		private void workerThumbnailUploadNetStorage_DoWork(object sender, DoWorkEventArgs e)
		{
			Utilities.FTP.FTPclient ftp = new Utilities.FTP.FTPclient(Properties.Settings.Default.NetStorageFtp, Properties.Settings.Default.NetStorageUserName, Properties.Settings.Default.NetStoragePassword);
			List<string> failedUploadFiles = new List<string>();

			foreach (string thumb in Thumbnails)
			{
				if (!ftp.Upload(thumb, String.Format("/31505/Images/ThumbSlide/{0}", Path.GetFileName(thumb))))
				{
					failedUploadFiles.Add(thumb);
				}
			}

			if (failedUploadFiles.Count > 0)
			{
				e.Result = new Exception(failedUploadFiles.Count + " files failed to upload.");
			}
		}

		private void dgvNewSlides_RowLeave(object sender, DataGridViewCellEventArgs e)
		{
			if (dgvNewSlides.SelectedRows.Count > 0)
			{
				int rowIndex = dgvNewSlides.SelectedRows[0].Index;

				// Commit any changes from the datagrid
				dgvNewSlides.EndEdit();
				string fileName = dgvNewSlides["FileName", rowIndex].Value.ToString();
				string title = dgvNewSlides["SlideTitle", rowIndex].Value.ToString();

				SaveSlideSettings(title, fileName);
			}
		}

		private void dgvExists_RowLeave(object sender, DataGridViewCellEventArgs e)
		{
			if (dgvExists.SelectedRows.Count > 0)
			{
				int rowIndex = dgvExists.SelectedRows[0].Index;

				// Commit any changes from the datagrid
				dgvExists.EndEdit();
				string fileName = dgvExists["FileNameExist", rowIndex].Value.ToString();
				string title = dgvExists["SlideTitleExist", rowIndex].Value.ToString();

				SaveSlideSettings(title, fileName);
			}
		}

		#endregion
	}
}