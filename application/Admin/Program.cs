using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Admin
{
    static class Program
    {
		public static ApplicationContext _applicationContext;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
			
			_applicationContext = new ApplicationContext();
			bool loginSuccessful = false;

			using (Login login = new Login())
			{
				for (int i = 0; i < 3; i++)
				{
					loginSuccessful = LoginSuccessful(login);

					if (loginSuccessful)
					{
						AdminMain am = new AdminMain(login.AdminTicket);
						am.Show();
						break;
					}
				}

				if (!loginSuccessful)
				{
					Application.Exit();
					return;
				}
			}

			Application.DoEvents();
			Application.Run(_applicationContext); //new AdminMain()
        }

		private static bool LoginSuccessful(Login login)
		{
			if (login.ShowDialog().Equals(DialogResult.Yes))
			{
				return true;
			}
			return false;
		}
    }
}