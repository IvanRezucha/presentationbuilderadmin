namespace Admin
{
    partial class SupportFile
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                //total hack for ms bug
                bindFiles.Sort = "";

                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SupportFile));
			this.tsMain = new System.Windows.Forms.ToolStrip();
			this.tsbMainNewFile = new System.Windows.Forms.ToolStripButton();
			this.tsbMainUpdateFile = new System.Windows.Forms.ToolStripButton();
			this.tsbMainDeleteFile = new System.Windows.Forms.ToolStripButton();
			this.tsbMainShowFile = new System.Windows.Forms.ToolStripButton();
			this.toolStripComboBoxRegionLanguage = new System.Windows.Forms.ToolStripComboBox();
			this.stsBottom = new System.Windows.Forms.StatusStrip();
			this.splMain = new System.Windows.Forms.SplitContainer();
			this.dgvFiles = new System.Windows.Forms.DataGridView();
			this.supportFileIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.fileNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.modifyDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.cmnHelpFiles = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.showToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.updateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.bindFiles = new System.Windows.Forms.BindingSource(this.components);
			this.dsSupportFile = new Admin.wsIpgAdmin.SupportFileDS();
			this.webFilePreview = new System.Windows.Forms.WebBrowser();
			this.wcFileUpload = new System.Net.WebClient();
			this.admin = new Admin.wsIpgAdmin.Admin();
			this.tsMain.SuspendLayout();
			this.splMain.Panel1.SuspendLayout();
			this.splMain.Panel2.SuspendLayout();
			this.splMain.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvFiles)).BeginInit();
			this.cmnHelpFiles.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.bindFiles)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dsSupportFile)).BeginInit();
			this.SuspendLayout();
			// 
			// tsMain
			// 
			this.tsMain.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
			this.tsMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbMainNewFile,
            this.tsbMainUpdateFile,
            this.tsbMainDeleteFile,
            this.tsbMainShowFile,
            this.toolStripComboBoxRegionLanguage});
			this.tsMain.Location = new System.Drawing.Point(0, 0);
			this.tsMain.Name = "tsMain";
			this.tsMain.Size = new System.Drawing.Size(732, 25);
			this.tsMain.TabIndex = 0;
			// 
			// tsbMainNewFile
			// 
			this.tsbMainNewFile.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.tsbMainNewFile.Image = global::Admin.Properties.Resources.NewFilePNG;
			this.tsbMainNewFile.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.tsbMainNewFile.Name = "tsbMainNewFile";
			this.tsbMainNewFile.Size = new System.Drawing.Size(23, 22);
			this.tsbMainNewFile.ToolTipText = "Upload New File";
			this.tsbMainNewFile.Click += new System.EventHandler(this.tsbMainNewFile_Click);
			// 
			// tsbMainUpdateFile
			// 
			this.tsbMainUpdateFile.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.tsbMainUpdateFile.Image = global::Admin.Properties.Resources.UpdateFile24PNG;
			this.tsbMainUpdateFile.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.tsbMainUpdateFile.Name = "tsbMainUpdateFile";
			this.tsbMainUpdateFile.Size = new System.Drawing.Size(23, 22);
			this.tsbMainUpdateFile.ToolTipText = "Update File";
			this.tsbMainUpdateFile.Click += new System.EventHandler(this.tsbMainUpdateFile_Click);
			// 
			// tsbMainDeleteFile
			// 
			this.tsbMainDeleteFile.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.tsbMainDeleteFile.Image = global::Admin.Properties.Resources.DeleteFile48PNG;
			this.tsbMainDeleteFile.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.tsbMainDeleteFile.Name = "tsbMainDeleteFile";
			this.tsbMainDeleteFile.Size = new System.Drawing.Size(23, 22);
			this.tsbMainDeleteFile.ToolTipText = "Delete File";
			this.tsbMainDeleteFile.Click += new System.EventHandler(this.tsbMainDeleteFile_Click);
			// 
			// tsbMainShowFile
			// 
			this.tsbMainShowFile.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.tsbMainShowFile.Image = ((System.Drawing.Image)(resources.GetObject("tsbMainShowFile.Image")));
			this.tsbMainShowFile.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.tsbMainShowFile.Name = "tsbMainShowFile";
			this.tsbMainShowFile.Size = new System.Drawing.Size(23, 22);
			this.tsbMainShowFile.ToolTipText = "Show File";
			this.tsbMainShowFile.Click += new System.EventHandler(this.tsbMainShowFile_Click);
			// 
			// toolStripComboBoxRegionLanguage
			// 
			this.toolStripComboBoxRegionLanguage.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
			this.toolStripComboBoxRegionLanguage.Name = "toolStripComboBoxRegionLanguage";
			this.toolStripComboBoxRegionLanguage.Size = new System.Drawing.Size(225, 25);
			this.toolStripComboBoxRegionLanguage.SelectedIndexChanged += new System.EventHandler(this.toolStripComboBoxRegionLanguage_SelectedIndexChanged);
			// 
			// stsBottom
			// 
			this.stsBottom.Location = new System.Drawing.Point(0, 530);
			this.stsBottom.Name = "stsBottom";
			this.stsBottom.Size = new System.Drawing.Size(732, 22);
			this.stsBottom.TabIndex = 1;
			this.stsBottom.Text = "statusStrip1";
			// 
			// splMain
			// 
			this.splMain.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splMain.Location = new System.Drawing.Point(0, 25);
			this.splMain.Name = "splMain";
			// 
			// splMain.Panel1
			// 
			this.splMain.Panel1.Controls.Add(this.dgvFiles);
			// 
			// splMain.Panel2
			// 
			this.splMain.Panel2.Controls.Add(this.webFilePreview);
			this.splMain.Size = new System.Drawing.Size(732, 505);
			this.splMain.SplitterDistance = 275;
			this.splMain.TabIndex = 2;
			// 
			// dgvFiles
			// 
			this.dgvFiles.AllowUserToAddRows = false;
			this.dgvFiles.AllowUserToDeleteRows = false;
			this.dgvFiles.AllowUserToResizeRows = false;
			this.dgvFiles.AutoGenerateColumns = false;
			this.dgvFiles.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
			this.dgvFiles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvFiles.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.supportFileIDDataGridViewTextBoxColumn,
            this.fileNameDataGridViewTextBoxColumn,
            this.modifyDateDataGridViewTextBoxColumn});
			this.dgvFiles.ContextMenuStrip = this.cmnHelpFiles;
			this.dgvFiles.DataSource = this.bindFiles;
			this.dgvFiles.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dgvFiles.Location = new System.Drawing.Point(0, 0);
			this.dgvFiles.MultiSelect = false;
			this.dgvFiles.Name = "dgvFiles";
			this.dgvFiles.ReadOnly = true;
			this.dgvFiles.RowHeadersWidth = 10;
			this.dgvFiles.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.dgvFiles.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dgvFiles.Size = new System.Drawing.Size(275, 505);
			this.dgvFiles.TabIndex = 0;
			this.dgvFiles.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dgvFiles_MouseDown);
			// 
			// supportFileIDDataGridViewTextBoxColumn
			// 
			this.supportFileIDDataGridViewTextBoxColumn.DataPropertyName = "SupportFileID";
			this.supportFileIDDataGridViewTextBoxColumn.HeaderText = "SupportFileID";
			this.supportFileIDDataGridViewTextBoxColumn.Name = "supportFileIDDataGridViewTextBoxColumn";
			this.supportFileIDDataGridViewTextBoxColumn.ReadOnly = true;
			// 
			// fileNameDataGridViewTextBoxColumn
			// 
			this.fileNameDataGridViewTextBoxColumn.DataPropertyName = "FileName";
			this.fileNameDataGridViewTextBoxColumn.HeaderText = "FileName";
			this.fileNameDataGridViewTextBoxColumn.Name = "fileNameDataGridViewTextBoxColumn";
			this.fileNameDataGridViewTextBoxColumn.ReadOnly = true;
			// 
			// modifyDateDataGridViewTextBoxColumn
			// 
			this.modifyDateDataGridViewTextBoxColumn.DataPropertyName = "ModifyDate";
			this.modifyDateDataGridViewTextBoxColumn.HeaderText = "ModifyDate";
			this.modifyDateDataGridViewTextBoxColumn.Name = "modifyDateDataGridViewTextBoxColumn";
			this.modifyDateDataGridViewTextBoxColumn.ReadOnly = true;
			// 
			// cmnHelpFiles
			// 
			this.cmnHelpFiles.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showToolStripMenuItem,
            this.updateToolStripMenuItem,
            this.deleteToolStripMenuItem});
			this.cmnHelpFiles.Name = "cmnHelpFiles";
			this.cmnHelpFiles.Size = new System.Drawing.Size(121, 70);
			// 
			// showToolStripMenuItem
			// 
			this.showToolStripMenuItem.Name = "showToolStripMenuItem";
			this.showToolStripMenuItem.Size = new System.Drawing.Size(120, 22);
			this.showToolStripMenuItem.Text = "Show";
			this.showToolStripMenuItem.Click += new System.EventHandler(this.tsbMainShowFile_Click);
			// 
			// updateToolStripMenuItem
			// 
			this.updateToolStripMenuItem.Name = "updateToolStripMenuItem";
			this.updateToolStripMenuItem.Size = new System.Drawing.Size(120, 22);
			this.updateToolStripMenuItem.Text = "Update";
			this.updateToolStripMenuItem.Click += new System.EventHandler(this.tsbMainUpdateFile_Click);
			// 
			// deleteToolStripMenuItem
			// 
			this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
			this.deleteToolStripMenuItem.Size = new System.Drawing.Size(120, 22);
			this.deleteToolStripMenuItem.Text = "Delete";
			this.deleteToolStripMenuItem.Click += new System.EventHandler(this.tsbMainDeleteFile_Click);
			// 
			// bindFiles
			// 
			this.bindFiles.DataMember = "SupportFile";
			this.bindFiles.DataSource = this.dsSupportFile;
			// 
			// dsSupportFile
			// 
			this.dsSupportFile.DataSetName = "SupportFileDS";
			this.dsSupportFile.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
			// 
			// webFilePreview
			// 
			this.webFilePreview.Dock = System.Windows.Forms.DockStyle.Fill;
			this.webFilePreview.Location = new System.Drawing.Point(0, 0);
			this.webFilePreview.MinimumSize = new System.Drawing.Size(20, 20);
			this.webFilePreview.Name = "webFilePreview";
			this.webFilePreview.Size = new System.Drawing.Size(453, 505);
			this.webFilePreview.TabIndex = 0;
			// 
			// wcFileUpload
			// 
			this.wcFileUpload.BaseAddress = "";
			this.wcFileUpload.CachePolicy = null;
			this.wcFileUpload.Credentials = null;
			this.wcFileUpload.Encoding = ((System.Text.Encoding)(resources.GetObject("wcFileUpload.Encoding")));
			this.wcFileUpload.Headers = ((System.Net.WebHeaderCollection)(resources.GetObject("wcFileUpload.Headers")));
			this.wcFileUpload.QueryString = ((System.Collections.Specialized.NameValueCollection)(resources.GetObject("wcFileUpload.QueryString")));
			this.wcFileUpload.UseDefaultCredentials = false;
			// 
			// admin
			// 
			this.admin.Credentials = null;
			this.admin.Url = global::Admin.Properties.Settings.Default.HpDmLaserJetAdmin_wsIpgAdmin_Admin;
			this.admin.UseDefaultCredentials = false;
			// 
			// SupportFile
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(732, 552);
			this.Controls.Add(this.splMain);
			this.Controls.Add(this.stsBottom);
			this.Controls.Add(this.tsMain);
			this.Name = "SupportFile";
			this.Text = "Support File Manager";
			this.Load += new System.EventHandler(this.SupportFile_Load);
			this.tsMain.ResumeLayout(false);
			this.tsMain.PerformLayout();
			this.splMain.Panel1.ResumeLayout(false);
			this.splMain.Panel2.ResumeLayout(false);
			this.splMain.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dgvFiles)).EndInit();
			this.cmnHelpFiles.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.bindFiles)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dsSupportFile)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip tsMain;
        private System.Windows.Forms.StatusStrip stsBottom;
        private System.Windows.Forms.SplitContainer splMain;
        private System.Windows.Forms.DataGridView dgvFiles;
        private System.Windows.Forms.WebBrowser webFilePreview;
        private System.Windows.Forms.ToolStripButton tsbMainNewFile;
        private System.Windows.Forms.ToolStripButton tsbMainUpdateFile;
        private System.Windows.Forms.ToolStripButton tsbMainDeleteFile;
        private System.Windows.Forms.BindingSource bindFiles;
        private System.Net.WebClient wcFileUpload;
		private Admin.wsIpgAdmin.Admin admin;
		private System.Windows.Forms.ContextMenuStrip cmnHelpFiles;
		private System.Windows.Forms.ToolStripButton tsbMainShowFile;
		private System.Windows.Forms.ToolStripMenuItem showToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem updateToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
		private System.Windows.Forms.DataGridViewTextBoxColumn supportFileIDDataGridViewTextBoxColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn fileNameDataGridViewTextBoxColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn modifyDateDataGridViewTextBoxColumn;
		private System.Windows.Forms.ToolStripComboBox toolStripComboBoxRegionLanguage;
		public Admin.wsIpgAdmin.SupportFileDS dsSupportFile;
    }
}