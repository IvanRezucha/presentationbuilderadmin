
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

namespace Admin
{
	public partial class SlideEdit : Form
	{
		#region Class variables
		
		private List<string> _file = new List<string>();
		private List<string> _thumbnail = new List<string>();
		private List<string> _errorList = new List<string>();
		private HpDm.DAL.Slide _slide;
		private int _slideID;
		private Mode _mode;
		private bool _isDataUpdateComplete = false;
		private bool _isFileUploadComplete = false;
		private bool _isThumbNailUploadComplete = false;
		
		#endregion

        #region properties

            public int SelectedTab
	        {
		        get 
		        {
			        return slideFiltersAndLanguages.tabMain.SelectedIndex; 
		        }
		        set 
		        {
			        if (value <= slideFiltersAndLanguages.tabMain.TabPages.Count - 1)
			        {
				        slideFiltersAndLanguages.tabMain.SelectedIndex = value;
			        }
		        }
	        }

        #endregion

        public SlideEdit(SlideEdit.Mode mode, int slideID)
		{
			Cursor = Cursors.WaitCursor;

			InitializeComponent();

			// Fill slideLibrary dataset with the region and language and filters
			CustomFeaturesSetup();
			_slideID = slideID;
			_mode = mode;

			// Fill slideLibrary dataset with slide information
			FillForm();

			Cursor = Cursors.Default;

            
		}

        #region control events

		/// <summary>
		/// Handles the Click event of the btnClose control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		private void btnClose_Click(object sender, EventArgs e)
		{
			Close();
		}

		/// <summary>
		/// Handles the Click event of the btnFile control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		private void btnFile_Click(object sender, EventArgs e)
		{
			DialogResult result = _diaOpenFile.ShowDialog();

			if (result == DialogResult.OK)
			{
				txtFile.Text = _diaOpenFile.FileName;

				_file.Clear();

				_file.Add(_diaOpenFile.FileName);

				chkTitle.Enabled = true;
                chkCompress.Enabled = true;
                chkCompress.Checked = true;

				// Get the slide title
				//txtTitle.Text = PlugIns.Helpers.PptUtils.GetSlideTitle(_diaOpenFile.FileName, 1, @"lic\Aspose.Slides.lic"); ;
			}
		}

		/// <summary>
		/// Handles the Click event of the btnSave control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		private void btnSave_Click(object sender, EventArgs e)
		{

			// Is the form valid?
			if (!ValidateForm())
			{
				return;
			}

			string fileName = string.Empty; 

			#region Upload Slide and Thumbnail
			
			// If we have a file to upload then...
			if (_file.Count > 0)
			{
                fileName = Path.GetFileNameWithoutExtension(_file[0]) + ".pot";

                if (chkCompress.Checked)
                {
					DeleteTempFiles(); 
					try
					{
						CompressSlide();
						//replace the slide file path with the path to the compressed slide
						_file[0] = Path.Combine(((AdminMain)this.Owner).AppTempDirectory, Path.GetFileName(_file[0]));
					}
					catch (System.Runtime.InteropServices.COMException ex)
					{
						
					}
                }
				HpDm.BLL.FileUploader thumbnailUploader = new HpDm.BLL.FileUploader();
				HpDm.BLL.FileUploader fileUploader = new HpDm.BLL.FileUploader();

				// Update the status
				_statusLabel.Text = "Uploading Slides...";
                
                
				// Upload the file
				fileUploader.UploadSlideFilesCompleted += new HpDm.BLL.FileUploader.UploadSlideFilesCompletedDelegate(fileUploader_UploadSlideFilesCompleted);
				fileUploader.UploadSlideFilesAsync(_file, Admin.Properties.Settings.Default.SlideUploadURI, true);

				// Update the status
				_statusLabel.Text = "Uploading Thumbnails...";

				// Add the thumbnail file to the list of thumbnail files
                _thumbnail.Add(Path.Combine(((AdminMain)this.Owner).AppTempDirectory, fileName.Replace(".ppt", ".jpg").Replace(".pot", ".jpg")));
			
				// Create the thumbnail
				if (File.Exists(_file[0]))
				{
					try
					{
						PlugIns.Helpers.PptUtils.WriteSlideImage(_file[0], _thumbnail[0], 0, 96, 72, System.Drawing.Imaging.ImageFormat.Jpeg, Path.Combine(Application.StartupPath, @"lic\Aspose.Slides.lic"));
					}
					catch (Aspose.Slides.PptException ex)
					{
						Properties.Resources.NoPreviewThumb.Save(_thumbnail[0],System.Drawing.Imaging.ImageFormat.Jpeg);
					}	
                }

				// Upload thumbnails
				thumbnailUploader.UploadSlideFilesCompleted += new HpDm.BLL.FileUploader.UploadSlideFilesCompletedDelegate(thumbnailUploader_UploadSlideFilesCompleted);
				thumbnailUploader.UploadSlideFilesAsync(_thumbnail, Admin.Properties.Settings.Default.ThumbnailUploadURI);
                
                //upload to NetStorage if configured to do so
                if (Properties.Settings.Default.IsNetStorageUtilized)
                {
                    Utilities.FTP.FTPclient ftp = new Utilities.FTP.FTPclient(Properties.Settings.Default.NetStorageFtp, Properties.Settings.Default.NetStorageUserName, Properties.Settings.Default.NetStoragePassword);
                    ftp.Upload(_file[0], String.Format("/31505/Ppt/{0}.pot", Path.GetFileNameWithoutExtension(_file[0])));
                    ftp.Upload(_thumbnail[0], String.Format("/31505/Images/ThumbSlide/{0}", Path.GetFileName(_thumbnail[0])));
                }
			}
			else
			{
				// This is an update and a slide file is not necessary
				_isFileUploadComplete = true;
				_isThumbNailUploadComplete = true;
			}
			#endregion

			#region Update Data

			string originalFileName = string.Empty;

			// Get the original filename
			if (_slideLibrary.Slide.Rows.Count > 0)
			{
				originalFileName = _slideLibrary.Slide[0].FileName;
			}
			
			DataRow[] dataRowArray = _slideLibrary.Slide.Select("FileName = '" + originalFileName + "'");
            List<int> filterIDs = slideFiltersAndLanguages.filterTabs.GetSelectedFilterIDs();
			List<int> regionLanguageIDs = slideFiltersAndLanguages.regionLanguageGroupGrid.GetSelectedRegionLanguageIDs();
			wsIpgAdmin.SlideLibrary.SlideRow slideRow = _slideLibrary.Slide.NewSlideRow();
			wsIpgAdmin.SlideLibrary.SlideRegionLanguageRow slideRegionLanguageRow;
			wsIpgAdmin.SlideLibrary.SlideFilterRow slideFilterRow;
			
			// Update the status
			_statusLabel.Text = "Data Update Starting...";

			// Update
			if (dataRowArray.Length > 0)
			{
				slideRow = (wsIpgAdmin.SlideLibrary.SlideRow)dataRowArray[0];
				try
				{
					slideRow.Title = chkTitle.Enabled && chkTitle.Checked ? PlugIns.Helpers.PptUtils.GetSlideTitle(_file[0], 1, @"lic\Aspose.Slides.lic") : txtTitle.Text; 
				}
				catch (Aspose.Slides.PptException ex)
				{
					slideRow.Title = _diaOpenFile.SafeFileName;
					MessageBox.Show("Unable to render " + _diaOpenFile.SafeFileName + ". " + Environment.NewLine +
						"The slide name will be " + _diaOpenFile.SafeFileName + " and there will be no slide thumbnail. If you would like to change the slide name please use the update function.");
				}
				slideRow.DateModified = System.DateTime.UtcNow;
				slideRow.FileName = fileName.Length > 0 && File.Exists(_diaOpenFile.FileName) ? fileName : slideRow.FileName;
			}
			// Insert
			else
			{
				// Add a new slide 
				try
				{
					slideRow.Title = chkTitle.Checked ? PlugIns.Helpers.PptUtils.GetSlideTitle(_file[0], 1, @"lic\Aspose.Slides.lic") : txtTitle.Text;
				}
				catch (Aspose.Slides.PptException ex)
				{
					slideRow.Title = _diaOpenFile.SafeFileName;
					MessageBox.Show("Unable to render " + _diaOpenFile.SafeFileName + ". " + Environment.NewLine +
						"The slide name will be " + _diaOpenFile.SafeFileName + " and there will be no slide thumbnail. If you would like to change the slide name please use the update function.");
				}
				slideRow.FileName = fileName;
				slideRow.DateModified = System.DateTime.UtcNow;
				_slideLibrary.Slide.AddSlideRow(slideRow);
			}

			// Delete all region/language relationship records for this group of slides
			foreach (wsIpgAdmin.SlideLibrary.SlideRegionLanguageRow srlRow in slideRow.GetSlideRegionLanguageRows())
			{
				srlRow.Delete();
			}

			// Delete all filter relationship records for this group of slides
			foreach (wsIpgAdmin.SlideLibrary.SlideFilterRow sfRow in slideRow.GetSlideFilterRows())
			{
				sfRow.Delete();
			}

            //if we're using RegLangs get the ones the user selected, otherwise put in the default
            if (Properties.Settings.Default.IsRegionManagmentEnabled)
            {
			    // Add new slide region/language records
			    foreach (int regionLanguageID in regionLanguageIDs)
			    {
				    slideRegionLanguageRow = _slideLibrary.SlideRegionLanguage.NewSlideRegionLanguageRow();
				    slideRegionLanguageRow.SlideID = slideRow.SlideID;
				    slideRegionLanguageRow.RegionLanguageID = regionLanguageID;
				    _slideLibrary.SlideRegionLanguage.Rows.Add(slideRegionLanguageRow);
			    }
            }
            else
            {
                //if we aren't using RegLangs put in the default ID
                slideRegionLanguageRow = _slideLibrary.SlideRegionLanguage.NewSlideRegionLanguageRow();
                slideRegionLanguageRow.SlideID = slideRow.SlideID;
                slideRegionLanguageRow.RegionLanguageID = AppSettings.NonLocalized.Default.DefaultRegionLanguageID;
                _slideLibrary.SlideRegionLanguage.Rows.Add(slideRegionLanguageRow);
            }

            if (Properties.Settings.Default.IsFilterManagementEnabled)
            {
			    // Add new slide filter records
			    foreach (int filterID in filterIDs)
			    {
				    slideFilterRow = _slideLibrary.SlideFilter.NewSlideFilterRow();
				    slideFilterRow.SlideID = slideRow.SlideID;
				    slideFilterRow.FilterID = filterID;
				    _slideLibrary.SlideFilter.Rows.Add(slideFilterRow);
			    }
            }




			// Update it
			_slide.UpdateCompleted += new HpDm.DAL.Slide.UpdateCompletedDelegate(_slide_UpdateCompleted);
			_slide.UpdateAsync(_slideLibrary);

			#endregion

			tmrProcessComplete.Enabled = true;
		}

		/// <summary>
		/// Handles the Tick event of the tmrProcessComplete control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		private void tmrProcessComplete_Tick(object sender, EventArgs e)
		{
			// Has everything completed
			if (_isDataUpdateComplete && _isFileUploadComplete && _isThumbNailUploadComplete)
			{
				// Has everything completed successfully
				if (_errorList.Count == 0)
				{
					tmrProcessComplete.Enabled = false;

					// TODO: Remove the old slide and thumbnail if this is an update

					// let the consuming form know we're done
                    SlideSavedEventEventArgs args = new SlideSavedEventEventArgs(_slideLibrary, null, _mode);
                    SlideSaved(args);

					// Close the form
                    //Close();
				}
				// Show the errors
				else
				{
					MessageBox.Show("There was an error processing your request. Your change was not saved.");

					_statusLabel.Text = "Error ";

					foreach (string error in _errorList)
					{
						_statusLabel.Text += error + " ";
					}
				}
			}
		}

        #endregion

        #region form setup

        /// <summary>
		/// Sets up configurable features.
		/// </summary>
		private void CustomFeaturesSetup()
		{
			if (Properties.Settings.Default.IsFilterManagementEnabled || Properties.Settings.Default.IsRegionManagmentEnabled)
			{
                wsIpgAdmin.Admin adminWS = new Admin.wsIpgAdmin.Admin();
                wsIpgAdmin.SlideLibrary dsFilterAndRegLangInfo = new Admin.wsIpgAdmin.SlideLibrary();


				if (Properties.Settings.Default.IsRegionManagmentEnabled)
				{
					dsFilterAndRegLangInfo.Merge(adminWS.RegionsAndLanguagesGet());    
				}

				if (Properties.Settings.Default.IsFilterManagementEnabled)
				{
					dsFilterAndRegLangInfo.Merge(adminWS.FilterGet());
				}

                //we're putting this data into the main dataset so
                //we don't have to disable constraints on the dataset when well put in SlideFilters or SlideRegionLanguages
                //this is kind of a weird spot to do this, but I figure since we've got the data may as well put it in
                _slideLibrary.Merge(dsFilterAndRegLangInfo);

				Admin.UserControls.SlideFiltersAndLanguagesFillType fillType;

				if (Properties.Settings.Default.IsFilterManagementEnabled && Properties.Settings.Default.IsRegionManagmentEnabled)
				{
					fillType = Admin.UserControls.SlideFiltersAndLanguagesFillType.FiltersAndRegionLanguages;
				}
				else if (Properties.Settings.Default.IsFilterManagementEnabled && !Properties.Settings.Default.IsRegionManagmentEnabled)
				{
					fillType = Admin.UserControls.SlideFiltersAndLanguagesFillType.FiltersOnly;
				}
				else if (!Properties.Settings.Default.IsFilterManagementEnabled && Properties.Settings.Default.IsRegionManagmentEnabled)
				{
					fillType = Admin.UserControls.SlideFiltersAndLanguagesFillType.RegionLanguagesOnly;
				}
				else
				{
					fillType = Admin.UserControls.SlideFiltersAndLanguagesFillType.FiltersAndRegionLanguages;
				}

				slideFiltersAndLanguages.Fill(dsFilterAndRegLangInfo, fillType);
			}
			else
			{
				slideFiltersAndLanguages.Visible = false;
				Height = 240;
			}
		}

		/// <summary>
		/// Fills the form.
		/// </summary>
		private void FillForm()
		{
			_slide = new HpDm.DAL.Slide();

			if (_mode == Mode.Edit)
			{
				_slide.GetBySlideIDAsync(_slideID);
			}
			else
			{
				_slide.GetBySlideIDAsync(-1);
			}

			_slide.GetBySlideIDCompleted += new HpDm.DAL.Slide.GetBySlideIDCompletedDelegate(_slide_GetBySlideIDCompleted);
		}

        #endregion

        #region operations

        /// <summary>
		/// Validates the form.
		/// </summary>
		/// <returns>Boolean value based on if the form is valid</returns>
		private bool ValidateForm()
		{
			errValidateForm.Clear();

			// Make sure we have a slide title
			if (txtTitle.Text == "" && !chkTitle.Checked)
			{
				MessageBox.Show("Please enter a slide title", "Required field missing", MessageBoxButtons.OK);
				errValidateForm.SetError(txtTitle, "Please enter a slide title");
				return false;
			}

			// There needs to be a slide file if this is a new slide (Not an update)
			if (Mode.New == _mode && txtFile.Text.Length == 0)
			{
				MessageBox.Show("Please select to power point file", "Required field missing", MessageBoxButtons.OK);
				errValidateForm.SetError(btnFile, "Please select to power point file");
				return false;
			}

            if (_file.Count > 0)
            {
                //no duplicate filenames allowed
                string fileName = Path.GetFileNameWithoutExtension(_file[0]) + ".pot";
                //make sure we aren't uploading a slide that will duplicate a filename
                //DataRow[] slideRows = ((AdminMain)this.Owner).dsSlideLibrary.Slide.Select(String.Format("FileName='{0}'", fileName));
                //int slideID = 0;

                //if (slideRows != null && slideRows.Length > 0)
                //{
                //    slideID = ((wsIpgAdmin.SlideLibrary.SlideRow)slideRows[0]).SlideID;
                //}

                wsIpgAdmin.Admin adminService = new Admin.wsIpgAdmin.Admin();
                wsIpgAdmin.IsSlideFilenameUniqueResult isSlideFilenameUniqueResult = adminService.IsSlideFilenameUnique(fileName);
                
                // Check for dups on new files
                if ((_mode == Mode.New && !isSlideFilenameUniqueResult.IsUnique))
                {
                    MessageBox.Show(String.Format("There is already a slide with the filename '{0}'. Duplicate filenames are not allowed.", fileName, MessageBoxButtons.OK, MessageBoxIcon.Information, fileName));
                    errValidateForm.SetError(btnFile, "Another slide already exists with this filename. You can not over rigtht another slide by doing an update on an existing slide.");
                    return false;
                }

			    // Check for dups on edits (basically what this says is first is it an edit, second did we upload a slide, third is the new filename different from the old filename, and finally are there duplicates using the new filename)
                if (_mode == Mode.Edit && !isSlideFilenameUniqueResult.IsUnique && _slideID != isSlideFilenameUniqueResult.SlideId)
			    {
                    MessageBox.Show("Another slide already exists with this filename. You can not over rigtht another slide by doing an update on an existing slide.", fileName, MessageBoxButtons.OK);
				    errValidateForm.SetError(btnFile, "Another slide already exists with this filename. You can not over rigtht another slide by doing an update on an existing slide.");
				    return false;
			    }
            }


			// If there is a slide to upload make sure it is a power point file
			if (txtFile.Text.Length > 0)
			{
				if (txtFile.Text.Substring(txtFile.Text.Length - 4, 4) != ".pot" && txtFile.Text.Substring(txtFile.Text.Length - 4, 4) != ".ppt")
				{
					MessageBox.Show("Slide file must be a PowerPoint file (.pot or .ppt extension)", "Incorrect file type", MessageBoxButtons.OK);
					errValidateForm.SetError(btnFile, "Slide file must be a PowerPoint file");
					return false;
				}
			}

			// Make sure there is at least one filter selected
            if (Properties.Settings.Default.IsFilterManagementEnabled && slideFiltersAndLanguages.filterTabs.GetSelectedFilterIDs().Count == 0)
			{
				MessageBox.Show("Please select at least one filter for this slide", "Slide error", MessageBoxButtons.OK);
				errValidateForm.SetError(slideFiltersAndLanguages, "Please select at least one filter for this slide");
				return false;
			}

			// Make sure there is at least one region/language selected
			if (Properties.Settings.Default.IsRegionManagmentEnabled && slideFiltersAndLanguages.regionLanguageGroupGrid.GetSelectedRegionLanguageIDs().Count == 0)
			{
				MessageBox.Show("Please select at least one region/language for this slide", "Slide error", MessageBoxButtons.OK);
				errValidateForm.SetError(slideFiltersAndLanguages, "Please select at least one region/language for this slide");
				return false;
			}

			return true;
		}

        private void CompressSlide()
        {
            PlugIns.Helpers.PowerPointCompressor compressor = new PlugIns.Helpers.PowerPointCompressor();

            compressor.CompressPresentation(_file[0], ((AdminMain)this.Owner).AppTempDirectory, true, true, 9, true);

            //string tempFile = Path.Combine(((AdminMain)this.Owner).appTempDirectory, Path.GetFileName(_file[0]));
        }

        /// <summary>
        /// Deletes the temp files.
        /// </summary>
        private void DeleteTempFiles()
        {
            List<string> filesInTemp = new List<string>(Directory.GetFiles(((AdminMain)this.Owner).AppTempDirectory));
            foreach (string file in filesInTemp)
            {
                if (File.Exists(file))
                {
                    try
                    {
                        File.Delete(file);
                    }
                    catch (Exception ex)
                    {
                        //do nothing 
                    }
                }
            }
        }

        #endregion

        #region async completed

        /// <summary>
		/// Get slide by slide ID completed.
		/// </summary>
		/// <param name="success">if set to <c>true</c> [success].</param>
		/// <param name="slideLibrary">The slide library.</param>
		private void _slide_GetBySlideIDCompleted(bool success, Admin.wsIpgAdmin.SlideLibrary slideLibrary)
		{
			// If data was successfully updated
			if (success)
			{
				// Add slide info
				_slideLibrary.Slide.Clear();
				_slideLibrary.Merge(slideLibrary);

				// Fill the form if this is an edit
				if (Mode.Edit == _mode && _slideLibrary.Slide.Rows.Count > 0)
				{
					List<int> FilterIDs = new List<int>();
					List<int> RegionLangIDs = new List<int>();

					chkTitle.Enabled = false;

					// Prefill the title textbox, region/language, and filters if this is an edit
					wsIpgAdmin.SlideLibrary.SlideRow slideRow = (wsIpgAdmin.SlideLibrary.SlideRow)_slideLibrary.Slide.Rows[0];
					txtTitle.Text = slideRow.Title;
					txtFile.Text = slideRow.FileName;

                    // Get all the filterIDs
                    foreach (wsIpgAdmin.SlideLibrary.SlideFilterRow slideFilterRow in slideRow.GetSlideFilterRows())
                    {
                        FilterIDs.Add(slideFilterRow.FilterID);
                    }

                    // Get all the region/language IDs
                    foreach (wsIpgAdmin.SlideLibrary.SlideRegionLanguageRow slideRegionLangRow in slideRow.GetSlideRegionLanguageRows())
                    {
                        RegionLangIDs.Add(slideRegionLangRow.RegionLanguageID);
                    }

					// Prefill the slideFiltersAndLanguages control
                    slideFiltersAndLanguages.filterTabs.CheckSlideFilters(FilterIDs);
					slideFiltersAndLanguages.regionLanguageGroupGrid.CheckSlideRegionLanguages(RegionLangIDs);
				}
			}
			else
			{
				// The form can't continue if we don't have any data
				Close();
			}
		}

		/// <summary>
		/// Update slide completed.
		/// </summary>
		/// <param name="success">if set to <c>true</c> [success].</param>
		/// <param name="slideLibrary">The slide library.</param>
		private void _slide_UpdateCompleted(bool success, Admin.wsIpgAdmin.SlideLibrary slideLibrary)
		{
			if (success)
			{
				// Update the status
				_statusLabel.Text = "Data update successful";
			}
			else
			{
				string error = "Data update failed";
				_errorList.Add(error);
				_statusLabel.Text = error;
			}

			_slideLibrary.Clear();
			_slideLibrary.Merge(slideLibrary);

			_isDataUpdateComplete = true;
		}

		/// <summary>
		/// Upload thumbnail file completed.
		/// </summary>
		/// <param name="failedUploadFiles">The failed upload files.</param>
		private void thumbnailUploader_UploadSlideFilesCompleted(List<string> failedUploadFiles)
		{
			// Delete temp thumbnail file
			if (File.Exists(_thumbnail[0]))
			{
				File.Delete(_thumbnail[0]);
			}

			// Update status
			if (failedUploadFiles.Count == 0)
			{
				_statusLabel.Text = "Thumbnail upload successful";
			}
			else
			{
				string error = "Thumbnail failed to upload";
				_errorList.Add(error);
				_statusLabel.Text = error;
			}

			_isThumbNailUploadComplete = true;
		}

		/// <summary>
		/// Upload slide file completed.
		/// </summary>
		/// <param name="failedUploadFiles">The failed upload files.</param>
		private void fileUploader_UploadSlideFilesCompleted(List<string> failedUploadFiles)
		{
			if (failedUploadFiles.Count == 0)
			{
				// Update status
				_statusLabel.Text = "File upload successful";
			}
			else
			{
				string error = "File failed to upload";
				_errorList.Add(error);
				_statusLabel.Text = error;
			}

			_isFileUploadComplete = true;
		}

        #endregion

        #region enums

        public enum Mode
		{
			New,
			Edit
		}

		public enum Tabs
		{
			Filters,
			RegionLanguages
		}

        #endregion

        public delegate void SlideSavedDelegate(SlideSavedEventEventArgs e);
        public event SlideSavedDelegate SlideSaved;
	}

    public class SlideSavedEventEventArgs : EventArgs
    {
        private wsIpgAdmin.SlideLibrary _NewSlideData;
        public wsIpgAdmin.SlideLibrary NewSlideData
        {
            get { return _NewSlideData; }
            private set { _NewSlideData = value; }
        }

        private Exception _Error;
        public Exception Error
        {
            get { return _Error; }
            private set { _Error = value; }
        }

        private SlideEdit.Mode _SaveType;
        public SlideEdit.Mode SaveType
        {
            get { return _SaveType; }
            private set { _SaveType = value; }
        }


        public SlideSavedEventEventArgs(wsIpgAdmin.SlideLibrary newSlideData, Exception error, SlideEdit.Mode saveType)
        {
            NewSlideData = newSlideData;
            Error = error;
            SaveType = saveType;
        }	
    }

}