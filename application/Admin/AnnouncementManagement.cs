using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Net;

namespace Admin
{
    //There are two controls that are on the this form that are positioned at -500,-500.
    //These controls are txtRegionId and dtpPubDate.
    //They are positioned there, becuase they are not meant to be seen but needed for the 
    //binding navigator and the binding source.  If they are set to visable = false, the binding
    //does not work.  
    public partial class AnnouncementManagement : MasterForm
    {
        wsIpgAdmin.Admin ws;
        private Guid previewGuid;

        public AnnouncementManagement()
        {
            InitializeComponent();
            ws = new Admin.wsIpgAdmin.Admin();
            ws.AnnouncementsGetCompleted += new Admin.wsIpgAdmin.AnnouncementsGetCompletedEventHandler(ws_AnnouncementsGetCompleted);
            ws.AnnouncementsSaveCompleted += new Admin.wsIpgAdmin.AnnouncementsSaveCompletedEventHandler(ws_AnnouncementsSaveCompleted);
            ws.SaveAnnouncementPreviewCompleted += new Admin.wsIpgAdmin.SaveAnnouncementPreviewCompletedEventHandler(ws_SaveAnnouncementPreviewCompleted);
        }

        #region ws async completed events

        private void ws_SaveAnnouncementPreviewCompleted(object sender, Admin.wsIpgAdmin.SaveAnnouncementPreviewCompletedEventArgs e)
        {
            wsIpgAdmin.AnnouncementDS result = e.Result as wsIpgAdmin.AnnouncementDS;
            ShowPreviewForm(result);
        }

        private void ws_AnnouncementsSaveCompleted(object sender, Admin.wsIpgAdmin.AnnouncementsSaveCompletedEventArgs e)
        {
            if (e.Error != null)
                MessageBox.Show(e.Error.Message);
            else if (e.Cancelled)
                MessageBox.Show("Save was cancelled");
            else
            {
                txtTitle.Text = "";
                txtBody.Text = "";
                RefreshAnnouncements(e.Result);

                if (dsAnnouncement.Announcement.Rows.Count <= 0)
                    SetupForAdd();
            }
        }

        private void RefreshAnnouncements(Admin.wsIpgAdmin.AnnouncementDS announcementDS)
        {
            dsAnnouncement.Clear();
            dsAnnouncement.Region.Merge(announcementDS.Region);
            PopulateRegionDropDown();
            dsAnnouncement.Announcement.Merge(announcementDS.Announcement);
        }

        private void ws_AnnouncementsGetCompleted(object sender, Admin.wsIpgAdmin.AnnouncementsGetCompletedEventArgs e)
        {
            if (e.Error != null)
                MessageBox.Show(e.Error.Message);
            else if (e.Cancelled)
                MessageBox.Show("Load Data was cancelled");
            else
            {
                RefreshAnnouncements(e.Result);

                if (dsAnnouncement.Announcement.Rows.Count <= 0)
                    SetupForAdd();
                else
                    bsAnnouncement.Position = 0;
            }
        }

        private void PopulateRegionDropDown()
        {
            cboRegion.Items.Clear();
            foreach (wsIpgAdmin.AnnouncementDS.RegionRow row in dsAnnouncement.Region.Rows)
            {
                if (row.RegionName != "Common")
                    cboRegion.Items.Add(row.RegionName);
            }
        }

        #endregion


        private void AnnouncementManagement_Load(object sender, EventArgs e)
        {
            ws.AnnouncementsGetAsync();
        }

        private bool ValidateForm()
        {
            bool isValid = true;

            //if there aren't any items in the list then we don't need to check the fields
            if (bsAnnouncement.List.Count == 0)
                return isValid;

            if (txtTitle.Text.Length < 1)
                isValid = false;

            if (txtBody.Text.Length < 1)
                isValid = false;

            if (!isValid)
                MessageBox.Show("All fields are required.", "", MessageBoxButtons.OK);

            return isValid;
        }

        private void SetupForAdd()
        {
            bsAnnouncement.EndEdit();
            bsAnnouncement.AddNew();
            dtpPubDate.Value = DateTime.Now;
            txtTitle.Text = "Enter New Announcement";
            txtTitle.Select(0, txtTitle.Text.Length);
            txtTitle.Focus();
            cboRegion.SelectedIndex = 0;
            txtRegionId.Text = LookUpRegionByName(cboRegion.SelectedItem.ToString()).ToString();
        }

        #region BindingNavigator child control events

        private void tsbNavSave_Click(object sender, EventArgs e)
        {
            if (ValidateForm())
            {
                bsAnnouncement.EndEdit();
                if (dsAnnouncement.HasChanges())
                {
                    wsIpgAdmin.AnnouncementDS dsChanges = (wsIpgAdmin.AnnouncementDS)dsAnnouncement.GetChanges();
                    ws.AnnouncementsSaveAsync(dsAnnouncement);
                }
            }
        }

        private void tsbNavAddNewItem_Click(object sender, EventArgs e)
        {
            if (ValidateForm())
                SetupForAdd();
        }

        private void tsbNavMoveFirstItem_Click(object sender, EventArgs e)
        {
            if (ValidateForm())
                bsAnnouncement.MoveFirst();
        }

        private void tsbNavMovePreviousItem_Click(object sender, EventArgs e)
        {
            if (ValidateForm())
                bsAnnouncement.MovePrevious();
        }

        private void tsbNavMoveNextItem_Click(object sender, EventArgs e)
        {
            if (ValidateForm())
                bsAnnouncement.MoveNext();
        }

        private void tsbNavMoveLastItem_Click(object sender, EventArgs e)
        {
            if (ValidateForm())
                bsAnnouncement.MoveLast();
        }

        private void tsbNavDeleteItem_Click(object sender, EventArgs e)
        {
            bsAnnouncement.RemoveCurrent();
        }

        #endregion

        private void btnPreview_Click(object sender, EventArgs e)
        {
            if (ValidateForm())
            {
                previewGuid = Guid.NewGuid();
                //Get the current row being view
                DataRowView rowView = bsAnnouncement.Current as DataRowView;
                Admin.wsIpgAdmin.AnnouncementDS.AnnouncementRow currentRow = rowView.Row as Admin.wsIpgAdmin.AnnouncementDS.AnnouncementRow;
                //Create the preview dataset
                wsIpgAdmin.AnnouncementDS previewAnnouncement = new Admin.wsIpgAdmin.AnnouncementDS();
                DateTime pubDate = dtpPubDate.Value;
                previewAnnouncement.Announcement.AddAnnouncementRow(txtTitle.Text, txtBody.Text, pubDate, currentRow.RegionId, true, previewGuid.ToString());
                //Save Preview
                ws.SaveAnnouncementPreviewAsync(previewAnnouncement);
            }
            else
            {
                MessageBox.Show("Please fill out announcement information to preview.", "Preview Announcement");
            }
        }

        private void ShowPreviewForm(wsIpgAdmin.AnnouncementDS previewDS)
        {
            int previewId = 0;
            if (previewDS.Announcement.Rows.Count == 1)
            {
                wsIpgAdmin.AnnouncementDS.AnnouncementRow row = previewDS.Announcement.Rows[0] as wsIpgAdmin.AnnouncementDS.AnnouncementRow;
                previewId = row.AnnouncementID;
            }
            if (previewId == 0)
            {
                MessageBox.Show("The preview is unable to be shown.  Please try again.", "Preview Announcement");
                return;
            }

            string navigateUrl = Admin.Properties.Settings.Default.StartPage + "?previewId=" + previewId.ToString();
            AnnouncementPreview preview = new AnnouncementPreview(navigateUrl, previewId);
            preview.Show();
        }

        private void dgvAnnouncement_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            for (int i = 0; i < dgvAnnouncement.Rows.Count; i++)
            {
                DataGridViewRow row = dgvAnnouncement.Rows[i];
                DataRowView view = row.DataBoundItem as DataRowView;
                Admin.wsIpgAdmin.AnnouncementDS.AnnouncementRow aRow = view.Row as Admin.wsIpgAdmin.AnnouncementDS.AnnouncementRow;
                int regionId = aRow.RegionId;
                row.Cells["Region"].Value = LookUpRegionById(regionId);
            }
        }

        private string LookUpRegionById(int regionId)
        {
            foreach (Admin.wsIpgAdmin.AnnouncementDS.RegionRow row in dsAnnouncement.Region.Rows)
            {
                if (row.RegionID.Equals(regionId))
                    return row.RegionName;
            }
            return string.Empty;
        }

        private int LookUpRegionByName(string regionName)
        {
            foreach (Admin.wsIpgAdmin.AnnouncementDS.RegionRow row in dsAnnouncement.Region.Rows)
            {
                if (row.RegionName.Equals(regionName))
                    return row.RegionID;
            }
            return 0;
        }

        private void cboRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtRegionId.Text = LookUpRegionByName(cboRegion.SelectedItem.ToString()).ToString();
        }

        private void bsAnnouncement_BindingComplete(object sender, BindingCompleteEventArgs e)
        {
            if (string.IsNullOrEmpty(txtRegionId.Text))
                return;

            for (int index = 0; index < cboRegion.Items.Count; index++)
            {
                string item = cboRegion.Items[index].ToString();
                string bsAnnouncementRegion = LookUpRegionById(Convert.ToInt32(txtRegionId.Text));
                if (item.ToString().Equals(bsAnnouncementRegion))
                {
                    cboRegion.SelectedIndex = index;
                    break;
                }
            }
        }

        private void btnManageAssets_Click(object sender, EventArgs e)
        {
            AnnouncementAssetManagement assetManagement = new AnnouncementAssetManagement();
            assetManagement.Show();
        }
    }
}