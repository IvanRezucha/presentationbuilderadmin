using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Admin
{
	public partial class SlideMultipleDelete : MasterForm
	{
		private wsIpgAdmin.SlideLibrary.SlideRow[] _deletedRows;
		
		public SlideMultipleDelete(wsIpgAdmin.SlideLibrary.SlideRow[] deletedRows)
		{
			InitializeComponent();
			dgvDeletedSlides.DataSource = deletedRows;
			_deletedRows = deletedRows;
			this.DialogResult = DialogResult.No;
		}

		private void btnClose_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void btnDelete_Click(object sender, EventArgs e)
		{
			this.DialogResult = DialogResult.Yes;
		}

		private void dgvDeletedSlides_MouseDown(object sender, MouseEventArgs e)
		{
			DataGridView.HitTestInfo info = dgvDeletedSlides.HitTest(e.X, e.Y);

			if (info.RowIndex >= 0)
            {
				tabTreeviewPaths.SelectedIndex = info.RowIndex;
            }
		}
	}
}