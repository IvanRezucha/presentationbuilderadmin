using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Admin
{
    public partial class FilterManagement : Form
    {
        wsIpgAdmin.Admin _wsAdmin;
        public FilterManagement()
        {
            InitializeComponent();
            _wsAdmin = new Admin.wsIpgAdmin.Admin();
            _wsAdmin.FilterGetCompleted += new Admin.wsIpgAdmin.FilterGetCompletedEventHandler(_wsAdmin_FilterGetCompleted);
            _wsAdmin.FilterSaveCompleted += new Admin.wsIpgAdmin.FilterSaveCompletedEventHandler(_wsAdmin_FilterSaveCompleted);
        }

        void _wsAdmin_FilterSaveCompleted(object sender, Admin.wsIpgAdmin.FilterSaveCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                MessageBox.Show("There was an error saving your changes.");
                this.Close();
            }
            else if (e.Cancelled)
            {
                MessageBox.Show("Save Cancelled");
            }
            else
            {
                dsSlideLibrary.Clear();
                dsSlideLibrary.Merge((wsIpgAdmin.SlideLibrary)e.Result);
                this.Close();
            }
        }

        void _wsAdmin_FilterGetCompleted(object sender, Admin.wsIpgAdmin.FilterGetCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                MessageBox.Show("Load Error");
            }
            else if (e.Cancelled)
            {
                MessageBox.Show("Load Cancelled");
            }
            else
            {
                dsSlideLibrary.Clear();
                dsSlideLibrary.Merge((wsIpgAdmin.SlideLibrary)e.Result);
            }
        }

        private void FilterManagement_Load(object sender, EventArgs e)
        {
            _wsAdmin.FilterGetAsync();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Save();
        }

        private void Save()
        {
            
            wsIpgAdmin.SlideLibrary dsChanges = new Admin.wsIpgAdmin.SlideLibrary();

            dsChanges.FilterGroup.Merge(dsSlideLibrary.FilterGroup);
            if (dsSlideLibrary.Filter.GetChanges((DataRowState.Added | DataRowState.Modified)) != null)
            {
                dsChanges.Filter.Merge(dsSlideLibrary.Filter.GetChanges(DataRowState.Added | DataRowState.Modified));
            }
            if (dsSlideLibrary.FilterGroup.GetChanges(DataRowState.Deleted) == null && dsSlideLibrary.Filter.GetChanges(DataRowState.Deleted) != null)
            {
                dsChanges.Filter.Merge(dsSlideLibrary.Filter.GetChanges(DataRowState.Added | DataRowState.Modified | DataRowState.Deleted));
            }

            _wsAdmin.Shizmo();
            _wsAdmin.FilterSaveAsync((wsIpgAdmin.SlideLibrary)dsChanges);
        }

        private void dgvFilterGroup_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

            if (e.ColumnIndex == 2)
            {
                if (MessageBox.Show("Deleting a filter group will also delete all related filters."+ System.Environment.NewLine + "Deleting a filter will also delete all slide associations with this filter." + System.Environment.NewLine + "Are you sure you wish to delete?", "Confirm Delete", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    dgvFilterGroup.Rows.Remove(dgvFilterGroup.Rows[e.RowIndex]);
                }
            }
        }

        private void btnFilterGroupAdd_Click(object sender, EventArgs e)
        {
            if (txtFilterGroup.Text.Length > 0)
            {
                dsSlideLibrary.FilterGroup.AddFilterGroupRow(txtFilterGroup.Text);
                txtFilterGroup.Text = "";
            }

        }

        private void btnFilterAdd_Click(object sender, EventArgs e)
        {
            if (txtFilter.Text.Length > 0)
            {
                wsIpgAdmin.SlideLibrary.FilterGroupRow filterGroup;
                filterGroup = (wsIpgAdmin.SlideLibrary.FilterGroupRow)dsSlideLibrary.FilterGroup.Rows[dgvFilterGroup.SelectedRows[0].Index];
                dsSlideLibrary.Filter.AddFilterRow(txtFilter.Text, filterGroup);
                txtFilter.Text = "";
            }
            
        }

        private void dgvFilter_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 3)
            {
                if (MessageBox.Show("Deleting a filter will also delete all slide associations with this filter." + System.Environment.NewLine + "Are you sure you wish to delete?", "Confirm Delete", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    dgvFilter.Rows.Remove(dgvFilter.Rows[e.RowIndex]);
                }
            }
        }

        private void dgvFilterGroup_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 1)
            {
                dgvFilterGroup.Rows[e.RowIndex].Cells[e.ColumnIndex].ReadOnly = false;
                btnSave.Enabled = false;
            }
        }

        private void dgvFilter_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 1)
            {
                dgvFilter.Rows[e.RowIndex].Cells[e.ColumnIndex].ReadOnly = false;
                btnSave.Enabled = false;
            }
        }

        private void dgvFilterGroup_CellLeave(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvFilterGroup.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString().Length > 0)
            {
                dgvFilterGroup.Rows[e.RowIndex].Cells[e.ColumnIndex].ReadOnly = true;
            }
            else
            {
                dgvFilterGroup.Rows[e.RowIndex].Cells[e.ColumnIndex].ReadOnly = false;
            }
            
        }

        private void dgvFilter_CellLeave(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvFilter.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString().Length > 0)
            {
                dgvFilter.Rows[e.RowIndex].Cells[e.ColumnIndex].ReadOnly = true;
            }
            else
            {
                dgvFilter.Rows[e.RowIndex].Cells[e.ColumnIndex].ReadOnly = false;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgvFilterGroup_CellStateChanged(object sender, DataGridViewCellStateChangedEventArgs e)
        {
            ToggleSaveButton(e);
        }

        private void dgvFilter_CellStateChanged(object sender, DataGridViewCellStateChangedEventArgs e)
        {
            ToggleSaveButton(e);
        }

        private void ToggleSaveButton(DataGridViewCellStateChangedEventArgs e)
        { 
            if (e.StateChanged == DataGridViewElementStates.ReadOnly)
            {
                if (e.Cell.ReadOnly)
                {
                    btnSave.Enabled = true;
                }
                else
                {
                    btnSave.Enabled = false;
                }
            }
        }
    }
}