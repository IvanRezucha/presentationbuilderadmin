using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace Admin
{
    public partial class ClientDataUpdateInfo : Form
    {
        wsIpgAdmin.Admin wsAdmin;
        private const UInt32 WM_COPY = 0x0301;

        public ClientDataUpdateInfo()
        {
            InitializeComponent();
            wsAdmin = new Admin.wsIpgAdmin.Admin();
            wsAdmin.ClientDataUpdateInfoGetCompleted += new Admin.wsIpgAdmin.ClientDataUpdateInfoGetCompletedEventHandler(wsAdmin_ClientDataUpdateInfoGetCompleted);
        }

        private void ClientDataUpdateInfo_Load(object sender, EventArgs e)
        {
            dtpFrom.Value = Convert.ToDateTime(String.Format("{0}/1/{1}", DateTime.Now.Month.ToString(), DateTime.Now.Year.ToString()));
            wsAdmin.ClientDataUpdateInfoGetAsync();  
        }

        private void wsAdmin_ClientDataUpdateInfoGetCompleted(object sender, Admin.wsIpgAdmin.ClientDataUpdateInfoGetCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                MessageBox.Show("There was an error retrieving the Data Update Information", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else if (e.Cancelled)
            {
                //do nothing
            }
            else
            {
                dsClientDataUpdate.Clear();
                dsClientDataUpdate.Merge((wsIpgAdmin.ClientDataUpdateDS)e.Result);
                Filter();
            }
        }

        private void btnFilter_Click(object sender, EventArgs e)
        {
            Filter();
        }

        private void Filter()
        {
            string filter = String.Format("UpdateDate >= '{0} 0:00:00' AND UpdateDate <= '{1} 23:59:59'", dtpFrom.Value.ToShortDateString(), dtpTo.Value.ToShortDateString());
            bsClientDataUpdate.Filter = filter;
            lblUpdateCount.Text = dgvClientDataUpdateInfo.Rows.Count.ToString();
        }

       

 
    }
}