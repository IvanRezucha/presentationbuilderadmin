namespace Admin
{
	partial class SlideEdit
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this._statusStrip = new System.Windows.Forms.StatusStrip();
            this._statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.grpSlideEdit = new System.Windows.Forms.GroupBox();
            this.chkCompress = new System.Windows.Forms.CheckBox();
            this.chkTitle = new System.Windows.Forms.CheckBox();
            this.txtFile = new System.Windows.Forms.TextBox();
            this.btnFile = new System.Windows.Forms.Button();
            this.lblSlide = new System.Windows.Forms.Label();
            this.lblTitle = new System.Windows.Forms.Label();
            this.txtTitle = new System.Windows.Forms.TextBox();
            this.slideFiltersAndLanguages = new Admin.UserControls.SlideFiltersAndLanguages();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this._slideLibrary = new Admin.wsIpgAdmin.SlideLibrary();
            this._diaOpenFile = new System.Windows.Forms.OpenFileDialog();
            this.errValidateForm = new System.Windows.Forms.ErrorProvider(this.components);
            this.tmrProcessComplete = new System.Windows.Forms.Timer(this.components);
            this._statusStrip.SuspendLayout();
            this.grpSlideEdit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._slideLibrary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errValidateForm)).BeginInit();
            this.SuspendLayout();
            // 
            // _statusStrip
            // 
            this._statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._statusLabel});
            this._statusStrip.Location = new System.Drawing.Point(0, 559);
            this._statusStrip.Name = "_statusStrip";
            this._statusStrip.Size = new System.Drawing.Size(419, 22);
            this._statusStrip.TabIndex = 0;
            // 
            // _statusLabel
            // 
            this._statusLabel.AutoSize = false;
            this._statusLabel.Name = "_statusLabel";
            this._statusLabel.Size = new System.Drawing.Size(200, 17);
            this._statusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // grpSlideEdit
            // 
            this.grpSlideEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grpSlideEdit.Controls.Add(this.chkCompress);
            this.grpSlideEdit.Controls.Add(this.chkTitle);
            this.grpSlideEdit.Controls.Add(this.txtFile);
            this.grpSlideEdit.Controls.Add(this.btnFile);
            this.grpSlideEdit.Controls.Add(this.lblSlide);
            this.grpSlideEdit.Controls.Add(this.lblTitle);
            this.grpSlideEdit.Controls.Add(this.txtTitle);
            this.grpSlideEdit.Controls.Add(this.slideFiltersAndLanguages);
            this.grpSlideEdit.Location = new System.Drawing.Point(12, 12);
            this.grpSlideEdit.Name = "grpSlideEdit";
            this.grpSlideEdit.Size = new System.Drawing.Size(395, 504);
            this.grpSlideEdit.TabIndex = 1;
            this.grpSlideEdit.TabStop = false;
            this.grpSlideEdit.Text = "Slide Edit";
            // 
            // chkCompress
            // 
            this.chkCompress.AutoSize = true;
            this.chkCompress.Enabled = false;
            this.chkCompress.Location = new System.Drawing.Point(68, 105);
            this.chkCompress.Name = "chkCompress";
            this.chkCompress.Size = new System.Drawing.Size(160, 17);
            this.chkCompress.TabIndex = 7;
            this.chkCompress.Text = "Check to compress this slide";
            this.chkCompress.UseVisualStyleBackColor = true;
            // 
            // chkTitle
            // 
            this.chkTitle.AutoSize = true;
            this.chkTitle.Enabled = false;
            this.chkTitle.Location = new System.Drawing.Point(68, 45);
            this.chkTitle.Name = "chkTitle";
            this.chkTitle.Size = new System.Drawing.Size(212, 17);
            this.chkTitle.TabIndex = 6;
            this.chkTitle.Text = "Check to use uploaded slide title as title";
            this.chkTitle.UseVisualStyleBackColor = true;
            // 
            // txtFile
            // 
            this.txtFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFile.Location = new System.Drawing.Point(68, 79);
            this.txtFile.Name = "txtFile";
            this.txtFile.ReadOnly = true;
            this.txtFile.Size = new System.Drawing.Size(273, 20);
            this.txtFile.TabIndex = 5;
            // 
            // btnFile
            // 
            this.btnFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFile.Location = new System.Drawing.Point(347, 77);
            this.btnFile.Name = "btnFile";
            this.btnFile.Size = new System.Drawing.Size(29, 23);
            this.btnFile.TabIndex = 4;
            this.btnFile.Text = "...";
            this.btnFile.UseVisualStyleBackColor = true;
            this.btnFile.Click += new System.EventHandler(this.btnFile_Click);
            // 
            // lblSlide
            // 
            this.lblSlide.AutoSize = true;
            this.lblSlide.Location = new System.Drawing.Point(9, 79);
            this.lblSlide.Name = "lblSlide";
            this.lblSlide.Size = new System.Drawing.Size(30, 13);
            this.lblSlide.TabIndex = 3;
            this.lblSlide.Text = "Slide";
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Location = new System.Drawing.Point(6, 22);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(27, 13);
            this.lblTitle.TabIndex = 2;
            this.lblTitle.Text = "Title";
            // 
            // txtTitle
            // 
            this.txtTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTitle.Location = new System.Drawing.Point(68, 19);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(308, 20);
            this.txtTitle.TabIndex = 1;
            // 
            // slideFiltersAndLanguages
            // 
            this.slideFiltersAndLanguages.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.slideFiltersAndLanguages.Location = new System.Drawing.Point(6, 144);
            this.slideFiltersAndLanguages.Name = "slideFiltersAndLanguages";
            this.slideFiltersAndLanguages.Size = new System.Drawing.Size(370, 354);
            this.slideFiltersAndLanguages.TabIndex = 0;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(332, 526);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 2;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(251, 526);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 3;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // _slideLibrary
            // 
            this._slideLibrary.DataSetName = "SlideLibrary";
            this._slideLibrary.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // _diaOpenFile
            // 
            this._diaOpenFile.DefaultExt = "pot";
            this._diaOpenFile.Filter = "Power Point Presentations (*.pot;*.ppt)|*.pot;*.ppt";
            // 
            // errValidateForm
            // 
            this.errValidateForm.ContainerControl = this;
            // 
            // tmrProcessComplete
            // 
            this.tmrProcessComplete.Interval = 500;
            this.tmrProcessComplete.Tick += new System.EventHandler(this.tmrProcessComplete_Tick);
            // 
            // SlideEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(419, 581);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.grpSlideEdit);
            this.Controls.Add(this._statusStrip);
            this.Name = "SlideEdit";
            this.Text = "Slide Edit";
            this._statusStrip.ResumeLayout(false);
            this._statusStrip.PerformLayout();
            this.grpSlideEdit.ResumeLayout(false);
            this.grpSlideEdit.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._slideLibrary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errValidateForm)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.StatusStrip _statusStrip;
		private System.Windows.Forms.GroupBox grpSlideEdit;
		private System.Windows.Forms.Button btnClose;
		private System.Windows.Forms.Button btnSave;
		private Admin.UserControls.SlideFiltersAndLanguages slideFiltersAndLanguages;
		private System.Windows.Forms.Label lblSlide;
		private System.Windows.Forms.Label lblTitle;
		private System.Windows.Forms.TextBox txtTitle;
		private Admin.wsIpgAdmin.SlideLibrary _slideLibrary;
		private System.Windows.Forms.Button btnFile;
		private System.Windows.Forms.TextBox txtFile;
		private System.Windows.Forms.OpenFileDialog _diaOpenFile;
		private System.Windows.Forms.ErrorProvider errValidateForm;
		private System.Windows.Forms.ToolStripStatusLabel _statusLabel;
		private System.Windows.Forms.Timer tmrProcessComplete;
		private System.Windows.Forms.CheckBox chkTitle;
        private System.Windows.Forms.CheckBox chkCompress;
	}
}