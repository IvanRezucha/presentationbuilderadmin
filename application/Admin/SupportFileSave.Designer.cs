namespace Admin
{
    partial class SupportFileSave
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SupportFileSave));
			this.btnClose = new System.Windows.Forms.Button();
			this.btnSave = new System.Windows.Forms.Button();
			this.btnFile = new System.Windows.Forms.Button();
			this.lblFile = new System.Windows.Forms.Label();
			this.txtFile = new System.Windows.Forms.TextBox();
			this.diaOpenFile = new System.Windows.Forms.OpenFileDialog();
			this.wcFileUpload = new System.Net.WebClient();
			this.regionLanguageGroupGrid = new Admin.UserControls.RegionLanguageGroupGrid();
			this.SuspendLayout();
			// 
			// btnClose
			// 
			this.btnClose.Location = new System.Drawing.Point(300, 53);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new System.Drawing.Size(75, 23);
			this.btnClose.TabIndex = 11;
			this.btnClose.Text = "Close";
			this.btnClose.UseVisualStyleBackColor = true;
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			// 
			// btnSave
			// 
			this.btnSave.Location = new System.Drawing.Point(197, 53);
			this.btnSave.Name = "btnSave";
			this.btnSave.Size = new System.Drawing.Size(75, 23);
			this.btnSave.TabIndex = 10;
			this.btnSave.Text = "Save";
			this.btnSave.UseVisualStyleBackColor = true;
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// btnFile
			// 
			this.btnFile.Location = new System.Drawing.Point(533, 12);
			this.btnFile.Name = "btnFile";
			this.btnFile.Size = new System.Drawing.Size(28, 23);
			this.btnFile.TabIndex = 9;
			this.btnFile.Text = "...";
			this.btnFile.UseVisualStyleBackColor = true;
			this.btnFile.Click += new System.EventHandler(this.btnFile_Click);
			// 
			// lblFile
			// 
			this.lblFile.AutoSize = true;
			this.lblFile.Location = new System.Drawing.Point(7, 12);
			this.lblFile.Name = "lblFile";
			this.lblFile.Size = new System.Drawing.Size(23, 13);
			this.lblFile.TabIndex = 8;
			this.lblFile.Text = "File";
			// 
			// txtFile
			// 
			this.txtFile.BackColor = System.Drawing.Color.LightYellow;
			this.txtFile.Location = new System.Drawing.Point(51, 12);
			this.txtFile.Name = "txtFile";
			this.txtFile.ReadOnly = true;
			this.txtFile.Size = new System.Drawing.Size(476, 20);
			this.txtFile.TabIndex = 7;
			// 
			// wcFileUpload
			// 
			this.wcFileUpload.BaseAddress = "";
			this.wcFileUpload.CachePolicy = null;
			this.wcFileUpload.Credentials = null;
			this.wcFileUpload.Encoding = ((System.Text.Encoding)(resources.GetObject("wcFileUpload.Encoding")));
			this.wcFileUpload.Headers = ((System.Net.WebHeaderCollection)(resources.GetObject("wcFileUpload.Headers")));
			this.wcFileUpload.QueryString = ((System.Collections.Specialized.NameValueCollection)(resources.GetObject("wcFileUpload.QueryString")));
			this.wcFileUpload.UseDefaultCredentials = false;
			this.wcFileUpload.UploadProgressChanged += new System.Net.UploadProgressChangedEventHandler(this.wcFileUpload_UploadProgressChanged);
			// 
			// regionLanguageGroupGrid
			// 
			this.regionLanguageGroupGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.regionLanguageGroupGrid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.regionLanguageGroupGrid.Location = new System.Drawing.Point(12, 97);
			this.regionLanguageGroupGrid.Name = "regionLanguageGroupGrid";
			this.regionLanguageGroupGrid.ShowControlBox = false;
			this.regionLanguageGroupGrid.Size = new System.Drawing.Size(549, 351);
			this.regionLanguageGroupGrid.TabIndex = 12;
			// 
			// SupportFileSave
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(573, 460);
			this.Controls.Add(this.regionLanguageGroupGrid);
			this.Controls.Add(this.btnClose);
			this.Controls.Add(this.btnSave);
			this.Controls.Add(this.btnFile);
			this.Controls.Add(this.lblFile);
			this.Controls.Add(this.txtFile);
			this.Name = "SupportFileSave";
			this.Text = "Support File Upload";
			this.Load += new System.EventHandler(this.SupportFileSave_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnFile;
        private System.Windows.Forms.Label lblFile;
        private System.Windows.Forms.TextBox txtFile;
        private System.Windows.Forms.OpenFileDialog diaOpenFile;
        private System.Net.WebClient wcFileUpload;
		private Admin.UserControls.RegionLanguageGroupGrid regionLanguageGroupGrid;
    }
}