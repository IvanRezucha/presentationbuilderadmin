namespace Admin
{
    partial class DeckUploader
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.BottomToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.TopToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.RightToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.LeftToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.ContentPanel = new System.Windows.Forms.ToolStripContentPanel();
            this.tscMain = new System.Windows.Forms.ToolStripContainer();
            this.statusMain = new System.Windows.Forms.StatusStrip();
            this.lblMainStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.pbMainProgress = new System.Windows.Forms.ToolStripProgressBar();
            this.toolStripStatusLabelSlideUpload = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelSlideUploadStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripProgressBarSlideUpload = new System.Windows.Forms.ToolStripProgressBar();
            this.toolStripStatusLabelThumbnailUpload = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelThumbnailUploadStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripProgressBarThumbnailUpload = new System.Windows.Forms.ToolStripProgressBar();
            this.splForm = new System.Windows.Forms.SplitContainer();
            this.chkCompress = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtSlideFilePrefix = new System.Windows.Forms.TextBox();
            this.lblDeckDirectory = new System.Windows.Forms.Label();
            this.btnChooseDeck = new System.Windows.Forms.Button();
            this.txtDeck = new System.Windows.Forms.TextBox();
            this.splPreview = new System.Windows.Forms.SplitContainer();
            this.lvwSlideThumbs = new System.Windows.Forms.ListView();
            this.imlSlideThumbs = new System.Windows.Forms.ImageList(this.components);
            this.splWebFilterLang = new System.Windows.Forms.SplitContainer();
            this.webSlidePreview = new System.Windows.Forms.WebBrowser();
            this.slideFiltersAndLanguages = new Admin.UserControls.SlideFiltersAndLanguages();
            this.tsSlideSpecs = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.txtSlideTitle = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.lblSlideFileName = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel3 = new System.Windows.Forms.ToolStripLabel();
            this.lblSlideFileSize = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel4 = new System.Windows.Forms.ToolStripLabel();
            this.tsMain = new System.Windows.Forms.ToolStrip();
            this.tsbMainSplitOnly = new System.Windows.Forms.ToolStripButton();
            this.tsbMainUpload = new System.Windows.Forms.ToolStripButton();
            this.tsbMainReset = new System.Windows.Forms.ToolStripButton();
            this.diaPickDeck = new System.Windows.Forms.OpenFileDialog();
            this.bgwSlideFileOperations = new System.ComponentModel.BackgroundWorker();
            this.timerCheckUploadCompleted = new System.Windows.Forms.Timer(this.components);
            this.errValidateForm = new System.Windows.Forms.ErrorProvider(this.components);
            this.bgwNetStorageUpload = new System.ComponentModel.BackgroundWorker();
            this.tscMain.BottomToolStripPanel.SuspendLayout();
            this.tscMain.ContentPanel.SuspendLayout();
            this.tscMain.TopToolStripPanel.SuspendLayout();
            this.tscMain.SuspendLayout();
            this.statusMain.SuspendLayout();
            this.splForm.Panel1.SuspendLayout();
            this.splForm.Panel2.SuspendLayout();
            this.splForm.SuspendLayout();
            this.splPreview.Panel1.SuspendLayout();
            this.splPreview.Panel2.SuspendLayout();
            this.splPreview.SuspendLayout();
            this.splWebFilterLang.Panel1.SuspendLayout();
            this.splWebFilterLang.Panel2.SuspendLayout();
            this.splWebFilterLang.SuspendLayout();
            this.tsSlideSpecs.SuspendLayout();
            this.tsMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errValidateForm)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomToolStripPanel
            // 
            this.BottomToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.BottomToolStripPanel.Name = "BottomToolStripPanel";
            this.BottomToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.BottomToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.BottomToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // TopToolStripPanel
            // 
            this.TopToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.TopToolStripPanel.Name = "TopToolStripPanel";
            this.TopToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.TopToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.TopToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // RightToolStripPanel
            // 
            this.RightToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.RightToolStripPanel.Name = "RightToolStripPanel";
            this.RightToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.RightToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.RightToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // LeftToolStripPanel
            // 
            this.LeftToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.LeftToolStripPanel.Name = "LeftToolStripPanel";
            this.LeftToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.LeftToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.LeftToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // ContentPanel
            // 
            this.ContentPanel.AutoScroll = true;
            this.ContentPanel.Size = new System.Drawing.Size(918, 672);
            // 
            // tscMain
            // 
            // 
            // tscMain.BottomToolStripPanel
            // 
            this.tscMain.BottomToolStripPanel.Controls.Add(this.statusMain);
            // 
            // tscMain.ContentPanel
            // 
            this.tscMain.ContentPanel.Controls.Add(this.splForm);
            this.tscMain.ContentPanel.Size = new System.Drawing.Size(756, 626);
            this.tscMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tscMain.LeftToolStripPanelVisible = false;
            this.tscMain.Location = new System.Drawing.Point(0, 0);
            this.tscMain.Name = "tscMain";
            this.tscMain.RightToolStripPanelVisible = false;
            this.tscMain.Size = new System.Drawing.Size(756, 673);
            this.tscMain.TabIndex = 1;
            this.tscMain.Text = "toolStripContainer1";
            // 
            // tscMain.TopToolStripPanel
            // 
            this.tscMain.TopToolStripPanel.Controls.Add(this.tsMain);
            // 
            // statusMain
            // 
            this.statusMain.Dock = System.Windows.Forms.DockStyle.None;
            this.statusMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblMainStatus,
            this.pbMainProgress,
            this.toolStripStatusLabelSlideUpload,
            this.toolStripStatusLabelSlideUploadStatus,
            this.toolStripProgressBarSlideUpload,
            this.toolStripStatusLabelThumbnailUpload,
            this.toolStripStatusLabelThumbnailUploadStatus,
            this.toolStripProgressBarThumbnailUpload});
            this.statusMain.Location = new System.Drawing.Point(0, 0);
            this.statusMain.Name = "statusMain";
            this.statusMain.Size = new System.Drawing.Size(756, 22);
            this.statusMain.TabIndex = 0;
            // 
            // lblMainStatus
            // 
            this.lblMainStatus.Name = "lblMainStatus";
            this.lblMainStatus.Size = new System.Drawing.Size(0, 17);
            this.lblMainStatus.Visible = false;
            // 
            // pbMainProgress
            // 
            this.pbMainProgress.Name = "pbMainProgress";
            this.pbMainProgress.Size = new System.Drawing.Size(300, 16);
            this.pbMainProgress.Visible = false;
            // 
            // toolStripStatusLabelSlideUpload
            // 
            this.toolStripStatusLabelSlideUpload.Name = "toolStripStatusLabelSlideUpload";
            this.toolStripStatusLabelSlideUpload.Size = new System.Drawing.Size(69, 17);
            this.toolStripStatusLabelSlideUpload.Text = "Slide Upload:";
            this.toolStripStatusLabelSlideUpload.Visible = false;
            // 
            // toolStripStatusLabelSlideUploadStatus
            // 
            this.toolStripStatusLabelSlideUploadStatus.Name = "toolStripStatusLabelSlideUploadStatus";
            this.toolStripStatusLabelSlideUploadStatus.Size = new System.Drawing.Size(19, 17);
            this.toolStripStatusLabelSlideUploadStatus.Text = "...";
            this.toolStripStatusLabelSlideUploadStatus.Visible = false;
            // 
            // toolStripProgressBarSlideUpload
            // 
            this.toolStripProgressBarSlideUpload.Name = "toolStripProgressBarSlideUpload";
            this.toolStripProgressBarSlideUpload.Size = new System.Drawing.Size(100, 16);
            this.toolStripProgressBarSlideUpload.Visible = false;
            // 
            // toolStripStatusLabelThumbnailUpload
            // 
            this.toolStripStatusLabelThumbnailUpload.Name = "toolStripStatusLabelThumbnailUpload";
            this.toolStripStatusLabelThumbnailUpload.Size = new System.Drawing.Size(95, 17);
            this.toolStripStatusLabelThumbnailUpload.Text = "Thumbnail Upload:";
            this.toolStripStatusLabelThumbnailUpload.Visible = false;
            // 
            // toolStripStatusLabelThumbnailUploadStatus
            // 
            this.toolStripStatusLabelThumbnailUploadStatus.Name = "toolStripStatusLabelThumbnailUploadStatus";
            this.toolStripStatusLabelThumbnailUploadStatus.Size = new System.Drawing.Size(19, 17);
            this.toolStripStatusLabelThumbnailUploadStatus.Text = "...";
            this.toolStripStatusLabelThumbnailUploadStatus.Visible = false;
            // 
            // toolStripProgressBarThumbnailUpload
            // 
            this.toolStripProgressBarThumbnailUpload.Name = "toolStripProgressBarThumbnailUpload";
            this.toolStripProgressBarThumbnailUpload.Size = new System.Drawing.Size(100, 16);
            this.toolStripProgressBarThumbnailUpload.Visible = false;
            // 
            // splForm
            // 
            this.splForm.BackColor = System.Drawing.SystemColors.ControlDark;
            this.splForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splForm.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splForm.Location = new System.Drawing.Point(0, 0);
            this.splForm.Name = "splForm";
            this.splForm.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splForm.Panel1
            // 
            this.splForm.Panel1.BackColor = System.Drawing.SystemColors.Control;
            this.splForm.Panel1.Controls.Add(this.chkCompress);
            this.splForm.Panel1.Controls.Add(this.label1);
            this.splForm.Panel1.Controls.Add(this.txtSlideFilePrefix);
            this.splForm.Panel1.Controls.Add(this.lblDeckDirectory);
            this.splForm.Panel1.Controls.Add(this.btnChooseDeck);
            this.splForm.Panel1.Controls.Add(this.txtDeck);
            // 
            // splForm.Panel2
            // 
            this.splForm.Panel2.Controls.Add(this.splPreview);
            this.splForm.Size = new System.Drawing.Size(756, 626);
            this.splForm.SplitterDistance = 99;
            this.splForm.SplitterWidth = 2;
            this.splForm.TabIndex = 0;
            // 
            // chkCompress
            // 
            this.chkCompress.AutoSize = true;
            this.chkCompress.Checked = true;
            this.chkCompress.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkCompress.Location = new System.Drawing.Point(95, 69);
            this.chkCompress.Name = "chkCompress";
            this.chkCompress.Size = new System.Drawing.Size(146, 17);
            this.chkCompress.TabIndex = 8;
            this.chkCompress.Text = "Check to compress slides";
            this.chkCompress.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Slide File Prefix";
            // 
            // txtSlideFilePrefix
            // 
            this.txtSlideFilePrefix.Location = new System.Drawing.Point(95, 43);
            this.txtSlideFilePrefix.Name = "txtSlideFilePrefix";
            this.txtSlideFilePrefix.Size = new System.Drawing.Size(119, 20);
            this.txtSlideFilePrefix.TabIndex = 4;
            // 
            // lblDeckDirectory
            // 
            this.lblDeckDirectory.AutoSize = true;
            this.lblDeckDirectory.Location = new System.Drawing.Point(11, 20);
            this.lblDeckDirectory.Name = "lblDeckDirectory";
            this.lblDeckDirectory.Size = new System.Drawing.Size(33, 13);
            this.lblDeckDirectory.TabIndex = 2;
            this.lblDeckDirectory.Text = "Deck";
            // 
            // btnChooseDeck
            // 
            this.btnChooseDeck.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnChooseDeck.Location = new System.Drawing.Point(687, 16);
            this.btnChooseDeck.Name = "btnChooseDeck";
            this.btnChooseDeck.Size = new System.Drawing.Size(28, 20);
            this.btnChooseDeck.TabIndex = 1;
            this.btnChooseDeck.Text = "...";
            this.btnChooseDeck.UseVisualStyleBackColor = true;
            this.btnChooseDeck.Click += new System.EventHandler(this.btnChooseDeck_Click);
            // 
            // txtDeck
            // 
            this.txtDeck.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDeck.BackColor = System.Drawing.Color.LightYellow;
            this.txtDeck.Location = new System.Drawing.Point(95, 17);
            this.txtDeck.Name = "txtDeck";
            this.txtDeck.ReadOnly = true;
            this.txtDeck.Size = new System.Drawing.Size(586, 20);
            this.txtDeck.TabIndex = 0;
            // 
            // splPreview
            // 
            this.splPreview.BackColor = System.Drawing.SystemColors.ControlDark;
            this.splPreview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splPreview.Location = new System.Drawing.Point(0, 0);
            this.splPreview.Name = "splPreview";
            // 
            // splPreview.Panel1
            // 
            this.splPreview.Panel1.Controls.Add(this.lvwSlideThumbs);
            // 
            // splPreview.Panel2
            // 
            this.splPreview.Panel2.Controls.Add(this.splWebFilterLang);
            this.splPreview.Panel2.Controls.Add(this.tsSlideSpecs);
            this.splPreview.Size = new System.Drawing.Size(756, 525);
            this.splPreview.SplitterDistance = 199;
            this.splPreview.SplitterWidth = 2;
            this.splPreview.TabIndex = 0;
            // 
            // lvwSlideThumbs
            // 
            this.lvwSlideThumbs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvwSlideThumbs.HideSelection = false;
            this.lvwSlideThumbs.LargeImageList = this.imlSlideThumbs;
            this.lvwSlideThumbs.Location = new System.Drawing.Point(0, 0);
            this.lvwSlideThumbs.Name = "lvwSlideThumbs";
            this.lvwSlideThumbs.Size = new System.Drawing.Size(199, 525);
            this.lvwSlideThumbs.TabIndex = 0;
            this.lvwSlideThumbs.UseCompatibleStateImageBehavior = false;
            this.lvwSlideThumbs.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lvwSlideThumbs_MouseClick);
            this.lvwSlideThumbs.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.lvwSlideThumbs_ItemSelectionChanged);
            // 
            // imlSlideThumbs
            // 
            this.imlSlideThumbs.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            this.imlSlideThumbs.ImageSize = new System.Drawing.Size(180, 140);
            this.imlSlideThumbs.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // splWebFilterLang
            // 
            this.splWebFilterLang.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splWebFilterLang.Location = new System.Drawing.Point(0, 0);
            this.splWebFilterLang.Name = "splWebFilterLang";
            // 
            // splWebFilterLang.Panel1
            // 
            this.splWebFilterLang.Panel1.Controls.Add(this.webSlidePreview);
            // 
            // splWebFilterLang.Panel2
            // 
            this.splWebFilterLang.Panel2.Controls.Add(this.slideFiltersAndLanguages);
            this.splWebFilterLang.Size = new System.Drawing.Size(555, 525);
            this.splWebFilterLang.SplitterDistance = 338;
            this.splWebFilterLang.SplitterWidth = 2;
            this.splWebFilterLang.TabIndex = 1;
            // 
            // webSlidePreview
            // 
            this.webSlidePreview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webSlidePreview.Location = new System.Drawing.Point(0, 0);
            this.webSlidePreview.MinimumSize = new System.Drawing.Size(20, 20);
            this.webSlidePreview.Name = "webSlidePreview";
            this.webSlidePreview.Size = new System.Drawing.Size(338, 525);
            this.webSlidePreview.TabIndex = 3;
            this.webSlidePreview.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.webSlidePreview_DocumentCompleted);
            // 
            // slideFiltersAndLanguages
            // 
            this.slideFiltersAndLanguages.Dock = System.Windows.Forms.DockStyle.Fill;
            this.slideFiltersAndLanguages.Location = new System.Drawing.Point(0, 0);
            this.slideFiltersAndLanguages.Name = "slideFiltersAndLanguages";
            this.slideFiltersAndLanguages.Size = new System.Drawing.Size(215, 525);
            this.slideFiltersAndLanguages.TabIndex = 1;
            // 
            // tsSlideSpecs
            // 
            this.tsSlideSpecs.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsSlideSpecs.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel2,
            this.txtSlideTitle,
            this.toolStripSeparator3,
            this.toolStripLabel1,
            this.lblSlideFileName,
            this.toolStripSeparator1,
            this.toolStripLabel3,
            this.lblSlideFileSize,
            this.toolStripLabel4});
            this.tsSlideSpecs.Location = new System.Drawing.Point(0, 0);
            this.tsSlideSpecs.Name = "tsSlideSpecs";
            this.tsSlideSpecs.Size = new System.Drawing.Size(555, 25);
            this.tsSlideSpecs.TabIndex = 0;
            this.tsSlideSpecs.Visible = false;
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(27, 22);
            this.toolStripLabel2.Text = "Title";
            // 
            // txtSlideTitle
            // 
            this.txtSlideTitle.Name = "txtSlideTitle";
            this.txtSlideTitle.Size = new System.Drawing.Size(200, 25);
            this.txtSlideTitle.Leave += new System.EventHandler(this.txtSlideTitle_Leave);
            this.txtSlideTitle.TextChanged += new System.EventHandler(this.txtSlideTitle_TextChanged);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(53, 22);
            this.toolStripLabel1.Text = "File Name";
            // 
            // lblSlideFileName
            // 
            this.lblSlideFileName.Name = "lblSlideFileName";
            this.lblSlideFileName.Size = new System.Drawing.Size(80, 22);
            this.lblSlideFileName.Text = "BlahJet_01.pot";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabel3
            // 
            this.toolStripLabel3.Name = "toolStripLabel3";
            this.toolStripLabel3.Size = new System.Drawing.Size(45, 22);
            this.toolStripLabel3.Text = "File Size";
            // 
            // lblSlideFileSize
            // 
            this.lblSlideFileSize.Name = "lblSlideFileSize";
            this.lblSlideFileSize.Size = new System.Drawing.Size(25, 22);
            this.lblSlideFileSize.Text = "123";
            // 
            // toolStripLabel4
            // 
            this.toolStripLabel4.Name = "toolStripLabel4";
            this.toolStripLabel4.Size = new System.Drawing.Size(19, 22);
            this.toolStripLabel4.Text = "KB";
            // 
            // tsMain
            // 
            this.tsMain.Dock = System.Windows.Forms.DockStyle.None;
            this.tsMain.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbMainSplitOnly,
            this.tsbMainUpload,
            this.tsbMainReset});
            this.tsMain.Location = new System.Drawing.Point(3, 0);
            this.tsMain.Name = "tsMain";
            this.tsMain.Size = new System.Drawing.Size(103, 25);
            this.tsMain.TabIndex = 7;
            this.tsMain.Text = "toolStrip3";
            // 
            // tsbMainSplitOnly
            // 
            this.tsbMainSplitOnly.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbMainSplitOnly.Image = global::Admin.Properties.Resources.SplitPNG;
            this.tsbMainSplitOnly.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbMainSplitOnly.Name = "tsbMainSplitOnly";
            this.tsbMainSplitOnly.Size = new System.Drawing.Size(23, 22);
            this.tsbMainSplitOnly.Text = "toolStripButton2";
            this.tsbMainSplitOnly.ToolTipText = "Split only";
            this.tsbMainSplitOnly.Click += new System.EventHandler(this.tsbMainSplitOnly_Click);
            // 
            // tsbMainUpload
            // 
            this.tsbMainUpload.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbMainUpload.Enabled = false;
            this.tsbMainUpload.Image = global::Admin.Properties.Resources.UploadPNG;
            this.tsbMainUpload.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbMainUpload.Name = "tsbMainUpload";
            this.tsbMainUpload.Size = new System.Drawing.Size(23, 22);
            this.tsbMainUpload.Text = "toolStripButton3";
            this.tsbMainUpload.ToolTipText = "Upload";
            this.tsbMainUpload.Click += new System.EventHandler(this.tsbMainUpload_Click);
            // 
            // tsbMainReset
            // 
            this.tsbMainReset.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbMainReset.Image = global::Admin.Properties.Resources.ResetPNG;
            this.tsbMainReset.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbMainReset.Name = "tsbMainReset";
            this.tsbMainReset.Size = new System.Drawing.Size(23, 22);
            this.tsbMainReset.Text = "toolStripButton1";
            this.tsbMainReset.ToolTipText = "Reset";
            this.tsbMainReset.Click += new System.EventHandler(this.tsbMainReset_Click);
            // 
            // diaPickDeck
            // 
            this.diaPickDeck.Filter = "Power Point Presentations (*.pot;*.ppt)|*.pot;*.ppt";
            this.diaPickDeck.RestoreDirectory = true;
            // 
            // bgwSlideFileOperations
            // 
            this.bgwSlideFileOperations.WorkerReportsProgress = true;
            this.bgwSlideFileOperations.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwSlideFileOperations_DoWork);
            this.bgwSlideFileOperations.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgwSlideFileOperations_RunWorkerCompleted);
            this.bgwSlideFileOperations.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgwSlideFileOperations_ProgressChanged);
            // 
            // timerCheckUploadCompleted
            // 
            this.timerCheckUploadCompleted.Interval = 1000;
            this.timerCheckUploadCompleted.Tick += new System.EventHandler(this.timerCheckUploadCompleted_Tick);
            // 
            // errValidateForm
            // 
            this.errValidateForm.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.errValidateForm.ContainerControl = this;
            // 
            // bgwNetStorageUpload
            // 
            this.bgwNetStorageUpload.WorkerReportsProgress = true;
            this.bgwNetStorageUpload.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwNetStorageUpload_DoWork);
            this.bgwNetStorageUpload.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgwNetStorageUpload_RunWorkerCompleted);
            this.bgwNetStorageUpload.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgwNetStorageUpload_ProgressChanged);
            // 
            // DeckUploader
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(756, 673);
            this.Controls.Add(this.tscMain);
            this.Name = "DeckUploader";
            this.Text = "Deck Uploader";
            this.Shown += new System.EventHandler(this.DeckUploader_Shown);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DeckUploader_FormClosing);
            this.Load += new System.EventHandler(this.DeckUploader_Load);
            this.tscMain.BottomToolStripPanel.ResumeLayout(false);
            this.tscMain.BottomToolStripPanel.PerformLayout();
            this.tscMain.ContentPanel.ResumeLayout(false);
            this.tscMain.TopToolStripPanel.ResumeLayout(false);
            this.tscMain.TopToolStripPanel.PerformLayout();
            this.tscMain.ResumeLayout(false);
            this.tscMain.PerformLayout();
            this.statusMain.ResumeLayout(false);
            this.statusMain.PerformLayout();
            this.splForm.Panel1.ResumeLayout(false);
            this.splForm.Panel1.PerformLayout();
            this.splForm.Panel2.ResumeLayout(false);
            this.splForm.ResumeLayout(false);
            this.splPreview.Panel1.ResumeLayout(false);
            this.splPreview.Panel2.ResumeLayout(false);
            this.splPreview.Panel2.PerformLayout();
            this.splPreview.ResumeLayout(false);
            this.splWebFilterLang.Panel1.ResumeLayout(false);
            this.splWebFilterLang.Panel2.ResumeLayout(false);
            this.splWebFilterLang.ResumeLayout(false);
            this.tsSlideSpecs.ResumeLayout(false);
            this.tsSlideSpecs.PerformLayout();
            this.tsMain.ResumeLayout(false);
            this.tsMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errValidateForm)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStripPanel BottomToolStripPanel;
        private System.Windows.Forms.ToolStripPanel TopToolStripPanel;
        private System.Windows.Forms.ToolStripPanel RightToolStripPanel;
        private System.Windows.Forms.ToolStripPanel LeftToolStripPanel;
        private System.Windows.Forms.ToolStripContentPanel ContentPanel;
        private System.Windows.Forms.ToolStripContainer tscMain;
        private System.Windows.Forms.StatusStrip statusMain;
        private System.Windows.Forms.SplitContainer splForm;
        private System.Windows.Forms.Label lblDeckDirectory;
        private System.Windows.Forms.Button btnChooseDeck;
        private System.Windows.Forms.TextBox txtDeck;
        private System.Windows.Forms.SplitContainer splPreview;
        private System.Windows.Forms.ListView lvwSlideThumbs;
        private System.Windows.Forms.ToolStrip tsSlideSpecs;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripTextBox txtSlideTitle;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripLabel toolStripLabel3;
        private System.Windows.Forms.ToolStripLabel toolStripLabel4;
        private System.Windows.Forms.ToolStripLabel lblSlideFileSize;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripLabel lblSlideFileName;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.WebBrowser webSlidePreview;
        private System.Windows.Forms.OpenFileDialog diaPickDeck;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtSlideFilePrefix;
        private System.Windows.Forms.ToolStrip tsMain;
        private System.Windows.Forms.ToolStripButton tsbMainSplitOnly;
        private System.Windows.Forms.ToolStripButton tsbMainUpload;
        private System.Windows.Forms.ToolStripButton tsbMainReset;
        private System.Windows.Forms.ImageList imlSlideThumbs;
        private System.Windows.Forms.ToolStripStatusLabel lblMainStatus;
        private System.Windows.Forms.ToolStripProgressBar pbMainProgress;
        private System.ComponentModel.BackgroundWorker bgwSlideFileOperations;
        private System.Windows.Forms.SplitContainer splWebFilterLang;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelSlideUpload;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelSlideUploadStatus;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBarSlideUpload;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelThumbnailUpload;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelThumbnailUploadStatus;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBarThumbnailUpload;
        private System.Windows.Forms.Timer timerCheckUploadCompleted;
		private System.Windows.Forms.ErrorProvider errValidateForm;
        private System.Windows.Forms.CheckBox chkCompress;
        private System.ComponentModel.BackgroundWorker bgwNetStorageUpload;
        private Admin.UserControls.SlideFiltersAndLanguages slideFiltersAndLanguages;

    }
}