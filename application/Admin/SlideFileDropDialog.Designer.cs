namespace Admin
{
	partial class SlideFileDropDialog
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.spliterUpload = new System.Windows.Forms.SplitContainer();
			this.spliterSlides = new System.Windows.Forms.SplitContainer();
			this.grpNewSlides = new System.Windows.Forms.GroupBox();
			this.dgvNewSlides = new System.Windows.Forms.DataGridView();
			this.SlideTitle = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.FileName = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.grpExistingSlides = new System.Windows.Forms.GroupBox();
			this.dgvExists = new System.Windows.Forms.DataGridView();
			this.SlideTitleExist = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.FileNameExist = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.lblManditoryMessage = new System.Windows.Forms.Label();
			this.lblManditory = new System.Windows.Forms.Label();
			this.slideFiltersAndLanguages = new Admin.UserControls.SlideFiltersAndLanguages();
			this.grpCompression = new System.Windows.Forms.GroupBox();
			this.chkCompress = new System.Windows.Forms.CheckBox();
			this.pnUploadStatus = new System.Windows.Forms.Panel();
			this.statusReport = new Admin.UserControls.StatusReport();
			this.btnUploadFiles = new System.Windows.Forms.Button();
			this.btnClose = new System.Windows.Forms.Button();
			this.spliterForm = new System.Windows.Forms.SplitContainer();
			this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
			this.workerFileUploadNetStorage = new System.ComponentModel.BackgroundWorker();
			this.workerThumbnailUploadNetStorage = new System.ComponentModel.BackgroundWorker();
			this.spliterUpload.Panel1.SuspendLayout();
			this.spliterUpload.Panel2.SuspendLayout();
			this.spliterUpload.SuspendLayout();
			this.spliterSlides.Panel1.SuspendLayout();
			this.spliterSlides.Panel2.SuspendLayout();
			this.spliterSlides.SuspendLayout();
			this.grpNewSlides.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvNewSlides)).BeginInit();
			this.grpExistingSlides.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvExists)).BeginInit();
			this.grpCompression.SuspendLayout();
			this.pnUploadStatus.SuspendLayout();
			this.spliterForm.Panel1.SuspendLayout();
			this.spliterForm.Panel2.SuspendLayout();
			this.spliterForm.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
			this.SuspendLayout();
			// 
			// spliterUpload
			// 
			this.spliterUpload.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.spliterUpload.Location = new System.Drawing.Point(3, 3);
			this.spliterUpload.Name = "spliterUpload";
			this.spliterUpload.Orientation = System.Windows.Forms.Orientation.Horizontal;
			// 
			// spliterUpload.Panel1
			// 
			this.spliterUpload.Panel1.Controls.Add(this.spliterSlides);
			// 
			// spliterUpload.Panel2
			// 
			this.spliterUpload.Panel2.Controls.Add(this.lblManditoryMessage);
			this.spliterUpload.Panel2.Controls.Add(this.lblManditory);
			this.spliterUpload.Panel2.Controls.Add(this.slideFiltersAndLanguages);
			this.spliterUpload.Panel2.Controls.Add(this.grpCompression);
			this.spliterUpload.Size = new System.Drawing.Size(559, 442);
			this.spliterUpload.SplitterDistance = 234;
			this.spliterUpload.SplitterWidth = 2;
			this.spliterUpload.TabIndex = 0;
			// 
			// spliterSlides
			// 
			this.spliterSlides.Dock = System.Windows.Forms.DockStyle.Fill;
			this.spliterSlides.Location = new System.Drawing.Point(0, 0);
			this.spliterSlides.Name = "spliterSlides";
			// 
			// spliterSlides.Panel1
			// 
			this.spliterSlides.Panel1.Controls.Add(this.grpNewSlides);
			// 
			// spliterSlides.Panel2
			// 
			this.spliterSlides.Panel2.Controls.Add(this.grpExistingSlides);
			this.spliterSlides.Size = new System.Drawing.Size(559, 234);
			this.spliterSlides.SplitterDistance = 277;
			this.spliterSlides.SplitterWidth = 2;
			this.spliterSlides.TabIndex = 0;
			// 
			// grpNewSlides
			// 
			this.grpNewSlides.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.grpNewSlides.Controls.Add(this.dgvNewSlides);
			this.grpNewSlides.Location = new System.Drawing.Point(3, 3);
			this.grpNewSlides.Name = "grpNewSlides";
			this.grpNewSlides.Size = new System.Drawing.Size(271, 228);
			this.grpNewSlides.TabIndex = 0;
			this.grpNewSlides.TabStop = false;
			this.grpNewSlides.Text = "New Slides";
			// 
			// dgvNewSlides
			// 
			this.dgvNewSlides.AllowUserToDeleteRows = false;
			this.dgvNewSlides.AllowUserToResizeRows = false;
			this.dgvNewSlides.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.dgvNewSlides.BackgroundColor = System.Drawing.Color.White;
			this.dgvNewSlides.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvNewSlides.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SlideTitle,
            this.FileName});
			this.dgvNewSlides.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
			this.dgvNewSlides.Location = new System.Drawing.Point(6, 19);
			this.dgvNewSlides.MultiSelect = false;
			this.dgvNewSlides.Name = "dgvNewSlides";
			this.dgvNewSlides.RowHeadersVisible = false;
			this.dgvNewSlides.RowHeadersWidth = 25;
			this.dgvNewSlides.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dgvNewSlides.Size = new System.Drawing.Size(259, 203);
			this.dgvNewSlides.TabIndex = 0;
			this.dgvNewSlides.RowLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvNewSlides_RowLeave);
			this.dgvNewSlides.Click += new System.EventHandler(this.dgvNewSlides_Click);
			// 
			// SlideTitle
			// 
			this.SlideTitle.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			this.SlideTitle.HeaderText = "Slide Title";
			this.SlideTitle.Name = "SlideTitle";
			// 
			// FileName
			// 
			this.FileName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			this.FileName.HeaderText = "File Name";
			this.FileName.Name = "FileName";
			this.FileName.ReadOnly = true;
			// 
			// grpExistingSlides
			// 
			this.grpExistingSlides.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.grpExistingSlides.Controls.Add(this.dgvExists);
			this.grpExistingSlides.Location = new System.Drawing.Point(3, 3);
			this.grpExistingSlides.Name = "grpExistingSlides";
			this.grpExistingSlides.Size = new System.Drawing.Size(274, 228);
			this.grpExistingSlides.TabIndex = 0;
			this.grpExistingSlides.TabStop = false;
			this.grpExistingSlides.Text = "Existing Slides";
			// 
			// dgvExists
			// 
			this.dgvExists.AllowUserToDeleteRows = false;
			this.dgvExists.AllowUserToResizeRows = false;
			this.dgvExists.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.dgvExists.BackgroundColor = System.Drawing.Color.White;
			this.dgvExists.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvExists.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SlideTitleExist,
            this.FileNameExist});
			this.dgvExists.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
			this.dgvExists.Location = new System.Drawing.Point(6, 19);
			this.dgvExists.MultiSelect = false;
			this.dgvExists.Name = "dgvExists";
			this.dgvExists.RowHeadersVisible = false;
			this.dgvExists.RowHeadersWidth = 25;
			this.dgvExists.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dgvExists.Size = new System.Drawing.Size(262, 203);
			this.dgvExists.TabIndex = 1;
			this.dgvExists.RowLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvExists_RowLeave);
			this.dgvExists.Click += new System.EventHandler(this.dgvExists_Click);
			// 
			// SlideTitleExist
			// 
			this.SlideTitleExist.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			this.SlideTitleExist.HeaderText = "Slide Title";
			this.SlideTitleExist.Name = "SlideTitleExist";
			// 
			// FileNameExist
			// 
			this.FileNameExist.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			this.FileNameExist.HeaderText = "File Name";
			this.FileNameExist.Name = "FileNameExist";
			this.FileNameExist.ReadOnly = true;
			// 
			// lblManditoryMessage
			// 
			this.lblManditoryMessage.AutoSize = true;
			this.lblManditoryMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblManditoryMessage.ForeColor = System.Drawing.Color.DimGray;
			this.lblManditoryMessage.Location = new System.Drawing.Point(16, 9);
			this.lblManditoryMessage.Name = "lblManditoryMessage";
			this.lblManditoryMessage.Size = new System.Drawing.Size(283, 12);
			this.lblManditoryMessage.TabIndex = 4;
			this.lblManditoryMessage.Text = "At least one filter and one region/language must be selected per slide";
			// 
			// lblManditory
			// 
			this.lblManditory.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblManditory.ForeColor = System.Drawing.Color.DarkRed;
			this.lblManditory.Location = new System.Drawing.Point(4, 8);
			this.lblManditory.Name = "lblManditory";
			this.lblManditory.Size = new System.Drawing.Size(10, 12);
			this.lblManditory.TabIndex = 3;
			this.lblManditory.Text = "*";
			this.lblManditory.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
			// 
			// slideFiltersAndLanguages
			// 
			this.slideFiltersAndLanguages.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.slideFiltersAndLanguages.Location = new System.Drawing.Point(2, 23);
			this.slideFiltersAndLanguages.Name = "slideFiltersAndLanguages";
			this.slideFiltersAndLanguages.Size = new System.Drawing.Size(272, 180);
			this.slideFiltersAndLanguages.TabIndex = 1;
			// 
			// grpCompression
			// 
			this.grpCompression.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.grpCompression.Controls.Add(this.chkCompress);
			this.grpCompression.Location = new System.Drawing.Point(338, 64);
			this.grpCompression.Name = "grpCompression";
			this.grpCompression.Size = new System.Drawing.Size(147, 89);
			this.grpCompression.TabIndex = 2;
			this.grpCompression.TabStop = false;
			this.grpCompression.Text = "Enable Slide Compression";
			// 
			// chkCompress
			// 
			this.chkCompress.AutoSize = true;
			this.chkCompress.Location = new System.Drawing.Point(21, 40);
			this.chkCompress.Name = "chkCompress";
			this.chkCompress.Size = new System.Drawing.Size(109, 17);
			this.chkCompress.TabIndex = 0;
			this.chkCompress.Text = "Compress Slide(s)";
			this.chkCompress.UseVisualStyleBackColor = true;
			// 
			// pnUploadStatus
			// 
			this.pnUploadStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.pnUploadStatus.Controls.Add(this.statusReport);
			this.pnUploadStatus.Location = new System.Drawing.Point(3, 3);
			this.pnUploadStatus.Name = "pnUploadStatus";
			this.pnUploadStatus.Size = new System.Drawing.Size(559, 273);
			this.pnUploadStatus.TabIndex = 1;
			// 
			// statusReport
			// 
			this.statusReport.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.statusReport.Location = new System.Drawing.Point(2, 3);
			this.statusReport.Name = "statusReport";
			this.statusReport.Size = new System.Drawing.Size(554, 267);
			this.statusReport.TabIndex = 0;
			// 
			// btnUploadFiles
			// 
			this.btnUploadFiles.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnUploadFiles.Location = new System.Drawing.Point(396, 736);
			this.btnUploadFiles.Name = "btnUploadFiles";
			this.btnUploadFiles.Size = new System.Drawing.Size(75, 23);
			this.btnUploadFiles.TabIndex = 2;
			this.btnUploadFiles.Text = "Upload";
			this.btnUploadFiles.UseVisualStyleBackColor = true;
			this.btnUploadFiles.Click += new System.EventHandler(this.btnUploadFiles_Click);
			// 
			// btnClose
			// 
			this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnClose.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnClose.Location = new System.Drawing.Point(477, 736);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new System.Drawing.Size(75, 23);
			this.btnClose.TabIndex = 1;
			this.btnClose.Text = "Close";
			this.btnClose.UseVisualStyleBackColor = true;
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			// 
			// spliterForm
			// 
			this.spliterForm.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.spliterForm.Location = new System.Drawing.Point(1, 1);
			this.spliterForm.Name = "spliterForm";
			this.spliterForm.Orientation = System.Windows.Forms.Orientation.Horizontal;
			// 
			// spliterForm.Panel1
			// 
			this.spliterForm.Panel1.Controls.Add(this.spliterUpload);
			// 
			// spliterForm.Panel2
			// 
			this.spliterForm.Panel2.Controls.Add(this.pnUploadStatus);
			this.spliterForm.Size = new System.Drawing.Size(565, 729);
			this.spliterForm.SplitterDistance = 448;
			this.spliterForm.SplitterWidth = 2;
			this.spliterForm.TabIndex = 3;
			// 
			// errorProvider
			// 
			this.errorProvider.ContainerControl = this;
			// 
			// workerFileUploadNetStorage
			// 
			this.workerFileUploadNetStorage.DoWork += new System.ComponentModel.DoWorkEventHandler(this.workerFileUploadNetStorage_DoWork);
			this.workerFileUploadNetStorage.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.workerFileUploadNetStorage_RunWorkerCompleted);
			// 
			// workerThumbnailUploadNetStorage
			// 
			this.workerThumbnailUploadNetStorage.DoWork += new System.ComponentModel.DoWorkEventHandler(this.workerThumbnailUploadNetStorage_DoWork);
			this.workerThumbnailUploadNetStorage.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.workerThumbnailUploadNetStorage_RunWorkerCompleted);
			// 
			// SlideFileDropDialog
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(568, 774);
			this.Controls.Add(this.spliterForm);
			this.Controls.Add(this.btnUploadFiles);
			this.Controls.Add(this.btnClose);
			this.Name = "SlideFileDropDialog";
			this.Text = "SlideFileDropDialog";
			this.Load += new System.EventHandler(this.SlideFileDropDialog_Load);
			this.spliterUpload.Panel1.ResumeLayout(false);
			this.spliterUpload.Panel2.ResumeLayout(false);
			this.spliterUpload.Panel2.PerformLayout();
			this.spliterUpload.ResumeLayout(false);
			this.spliterSlides.Panel1.ResumeLayout(false);
			this.spliterSlides.Panel2.ResumeLayout(false);
			this.spliterSlides.ResumeLayout(false);
			this.grpNewSlides.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dgvNewSlides)).EndInit();
			this.grpExistingSlides.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dgvExists)).EndInit();
			this.grpCompression.ResumeLayout(false);
			this.grpCompression.PerformLayout();
			this.pnUploadStatus.ResumeLayout(false);
			this.spliterForm.Panel1.ResumeLayout(false);
			this.spliterForm.Panel2.ResumeLayout(false);
			this.spliterForm.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.SplitContainer spliterUpload;
		private System.Windows.Forms.SplitContainer spliterSlides;
		private System.Windows.Forms.GroupBox grpNewSlides;
		private System.Windows.Forms.GroupBox grpExistingSlides;
		private Admin.UserControls.SlideFiltersAndLanguages slideFiltersAndLanguages;
		private System.Windows.Forms.Panel pnUploadStatus;
		private System.Windows.Forms.Button btnUploadFiles;
		private System.Windows.Forms.Button btnClose;
		private System.Windows.Forms.GroupBox grpCompression;
		private System.Windows.Forms.CheckBox chkCompress;
		private System.Windows.Forms.DataGridView dgvNewSlides;
		private System.Windows.Forms.DataGridView dgvExists;
		private System.Windows.Forms.SplitContainer spliterForm;
		private System.Windows.Forms.DataGridViewTextBoxColumn SlideTitle;
		private System.Windows.Forms.DataGridViewTextBoxColumn FileName;
		private System.Windows.Forms.ErrorProvider errorProvider;
		private System.ComponentModel.BackgroundWorker workerFileUploadNetStorage;
		private System.ComponentModel.BackgroundWorker workerThumbnailUploadNetStorage;
		private System.Windows.Forms.DataGridViewTextBoxColumn SlideTitleExist;
		private System.Windows.Forms.DataGridViewTextBoxColumn FileNameExist;
		private Admin.UserControls.StatusReport statusReport;
		private System.Windows.Forms.Label lblManditory;
		private System.Windows.Forms.Label lblManditoryMessage;
	}
}