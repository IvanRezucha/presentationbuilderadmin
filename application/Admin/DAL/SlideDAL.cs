using System;
using System.Collections.Generic;
using System.Text;

namespace HpDm.DAL
{
    public class Slide
    {
		private Admin.wsIpgAdmin.Admin _AdminWS;
		private bool _IsCompleted = false;

        public Slide(){}

		public bool IsCompleted
		{
			get { return _IsCompleted; }
			private set { _IsCompleted = value; }
		}		

		/// <summary>
		/// Update slide changes
		/// </summary>
		/// <param name="slideLibrary">SlideLibrary Dataset</param>
		public void UpdateAsync(Admin.wsIpgAdmin.SlideLibrary slideLibrary)
        {
			_AdminWS = new Admin.wsIpgAdmin.Admin();

			if (slideLibrary != null)
			{
				// Hook the the completed event
				_AdminWS.SlideSaveChangesCompleted += new Admin.wsIpgAdmin.SlideSaveChangesCompletedEventHandler(_AdminWS_SlideSaveChangesCompleted);

				_AdminWS.SlideSaveChangesAsync((Admin.wsIpgAdmin.SlideLibrary)slideLibrary.GetChanges());
			}
			else
			{
				//slideLibrary = new Admin.wsIpgAdmin.SlideLibrary();
				//System.ComponentModel.AsyncCompletedEventArgs e = new System.ComponentModel.AsyncCompletedEventArgs(new Exception("Received a null dataset from the application."), true, "Cancelled");
				//object[] eventArg = new object[3];
				//eventArg[0] = e.Cancelled;
				//eventArg[1] = e.Error;
				//eventArg[2] = e.UserState;
				//_AdminWS_SlideSaveChangesCompleted(null, new Admin.wsIpgAdmin.SlideSaveChangesCompletedEventArgs(eventArg, null, true, null));
				
				//Admin.wsIpgAdmin.SlideSaveChangesCompletedEventArgs empty = (Admin.wsIpgAdmin.SlideSaveChangesCompletedEventArgs)Admin.wsIpgAdmin.SlideSaveChangesCompletedEventArgs.Empty;
				_AdminWS_SlideSaveChangesCompleted(null, null);
			}
		}

		/// <summary>
		/// Returns all slides with related relationship tables
		/// </summary>
		public void GetAllAsync()
		{
			_AdminWS = new Admin.wsIpgAdmin.Admin();
            // Hook the completed event
			_AdminWS.SlideGetAllCompleted += new Admin.wsIpgAdmin.SlideGetAllCompletedEventHandler(_AdminWS_SlideGetAllCompleted);

            _AdminWS.Shizmo();
			_AdminWS.SlideGetAllAsync();
		}

		/// <summary>
		/// Gets a specific slide
		/// </summary>
		public void GetBySlideIDAsync(int slideID)
		{
			_AdminWS = new Admin.wsIpgAdmin.Admin();
			_AdminWS.SlideGetAsync(slideID);

			// Hook the completed event
			_AdminWS.SlideGetCompleted += new Admin.wsIpgAdmin.SlideGetCompletedEventHandler(_AdminWS_SlideGetCompleted);
		}
		
		/// <summary>
		/// Compares the list of filenames passed in and returns the filenames that are a match
		/// </summary>
		/// <param name="fileNames">Comma delimited list of filenames</param>
		public void GetByFileNamesAsync(string fileNames)
		{
			_AdminWS = new Admin.wsIpgAdmin.Admin();

			_AdminWS.SlideGetByFileNamesCompleted += new Admin.wsIpgAdmin.SlideGetByFileNamesCompletedEventHandler(_AdminWS_SlideGetByFileNamesCompleted);
			_AdminWS.SlideGetByFileNamesAsync(fileNames);
		}

		/// <summary>
		/// Helper method that determines if the asynchronous call was successful
		/// </summary>
		/// <param name="e">The completed event argument</param>
		/// <returns>Bool</returns>
		private bool IsResultSuccessful(System.ComponentModel.AsyncCompletedEventArgs e)
		{
			if (e == null || e.Cancelled || e.Error != null)
			{
				return false;
			}

			return true;
		}

		private void _AdminWS_SlideGetByFileNamesCompleted(object sender, Admin.wsIpgAdmin.SlideGetByFileNamesCompletedEventArgs e)
		{
			IsCompleted = true;
			GetByFileNamesCompleted(IsResultSuccessful(e), e.Result);
		}

		private void _AdminWS_SlideGetAllCompleted(object sender, Admin.wsIpgAdmin.SlideGetAllCompletedEventArgs e)
		{
			IsCompleted = true;
			GetAllCompleted(IsResultSuccessful(e), e.Result);
		}

		private void _AdminWS_SlideSaveChangesCompleted(object sender, Admin.wsIpgAdmin.SlideSaveChangesCompletedEventArgs e)
		{
			IsCompleted = true;
			
			if (e == null)
			{
				UpdateCompleted(true, null);
				return;
			}

			UpdateCompleted(IsResultSuccessful(e), e.Result);
		}

		private void _AdminWS_SlideGetCompleted(object sender, Admin.wsIpgAdmin.SlideGetCompletedEventArgs e)
		{
			IsCompleted = true;
			GetBySlideIDCompleted(IsResultSuccessful(e), e.Result);
		}

		public event GetByFileNamesCompletedDelegate GetByFileNamesCompleted;

		public event UpdateCompletedDelegate UpdateCompleted;

		public event GetAllCompletedDelegate GetAllCompleted;

		public event GetBySlideIDCompletedDelegate GetBySlideIDCompleted;

		public delegate void GetByFileNamesCompletedDelegate(bool sucess, Admin.wsIpgAdmin.SlideLibrary slideLibrary);

		public delegate void UpdateCompletedDelegate(bool success, Admin.wsIpgAdmin.SlideLibrary slideLibrary);

		public delegate void GetAllCompletedDelegate(bool success, Admin.wsIpgAdmin.SlideLibrary slideLibrary);

		public delegate void GetBySlideIDCompletedDelegate(bool success, Admin.wsIpgAdmin.SlideLibrary slideLibrary);
    }
}