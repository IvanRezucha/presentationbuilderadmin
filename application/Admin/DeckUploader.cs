using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Admin
{
    public partial class DeckUploader : MasterForm
    {
        #region class variables

        private PlugIns.Helpers.DeckSplitter _DeckSplitter;
        private List<string> _SlideFilePaths;
        private List<SlideFile> _SlideFiles;
        private List<string> _FailedFiles;

        private List<string> _SlidesToUpload;
        private List<string> _ThumbnailsToUpload;

        HpDm.BLL.FileUploader _SlideFileUploader;
        HpDm.BLL.FileUploader _ThumbnailFileUploader;
        wsIpgAdmin.SlideLibrary _SlideLibraryDS;

        private bool _IsNetStorageUploadComplete;

        private PlugIns.Helpers.PowerPointCompressor _Compressor;
        private  PlugIns.Helpers.PowerPointCompressor Compressor
        {
            get { return _Compressor; }
            set { _Compressor = value; }
        }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="DeckUploader"/> class.
        /// </summary>
        public DeckUploader()
        {
            InitializeComponent();
            _SlideLibraryDS = new Admin.wsIpgAdmin.SlideLibrary();
        }

        #region form events

        /// <summary>
        /// Handles the Shown event of the DeckUploader control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void DeckUploader_Shown(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            CustomFeaturesSetup();
            this.Cursor = Cursors.Default;

            //we get this now because this will run in the background while the user is doing stuff
            //GetSlideData();
        }

        /// <summary>
        /// Handles the Load event of the DeckUploader control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void DeckUploader_Load(object sender, EventArgs e)
        {
            splForm.Panel2Collapsed = true;
            this.Height = this.Height - 535;
        }

        #endregion

        #region main menu buttons


        /// <summary>
        /// Handles the Click event of the tsbMainSplitOnly control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void tsbMainSplitOnly_Click(object sender, EventArgs e)
        {
            if (IsReadyForSplitting())
            {
                lblMainStatus.Visible = true;
                pbMainProgress.Visible = true;
                lblMainStatus.Text = "Preparing to split deck...";
                DeleteTempFiles();
                if (chkCompress.Checked)
                {
                    lblMainStatus.Text = "Performing compression...";
                    CompressSourcePresentation();
                    //we are compressing asynchronously, so we can't make the slide files until we are done
                    //the completed event will take care of calling the MakeSlideFiles
                    return;
                }
                MakeSlideFiles();
            }
        }

        /// <summary>
        /// Handles the Click event of the tsbMainReset control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void tsbMainReset_Click(object sender, EventArgs e)
        {
            ResetForm();
        } 
        
        /// <summary>
        /// Handles the Click event of the tsbMainUpload control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void tsbMainUpload_Click(object sender, EventArgs e)
        {
			if (!ValidateForm())
			{
				return;
			}

            if (_SlideFiles.Count > 0)
            {
                UploadFiles();
            }
            else
            {
                MessageBox.Show("There are no slides available to upload", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        #endregion

        private void CompressSourcePresentation()
        {
            if (Compressor == null)
            {
                Compressor = new PlugIns.Helpers.PowerPointCompressor();
                Compressor.RunWorkerCompleted += new RunWorkerCompletedEventHandler(Compressor_RunWorkerCompleted);
            }
            List<string> decks = new List<string>();
            decks.Add(txtDeck.Text);
            Compressor.CompressPresentationsAsync(decks, ((AdminMain)this.Owner).AppTempDirectory, true, true, 7, true);
        }

        private void Compressor_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                MessageBox.Show(e.Error.ToString());
                this.Close();
            }
            else
            {
                lblMainStatus.Text = "Compression complete...";
                MakeSlideFiles();
            }
            
        }

		private bool ValidateForm()
		{
			// Make sure there is at least one filter selected
			if (Properties.Settings.Default.IsFilterManagementEnabled && slideFiltersAndLanguages.filterTabs.GetSelectedFilterIDs().Count == 0)
			{
				MessageBox.Show("Please select at least one filter for this slide", "Slide error", MessageBoxButtons.OK);
				errValidateForm.SetError(slideFiltersAndLanguages, "Please select at least one filter for this slide");
				return false;
			}

			// Make sure there is at least one region/language selected
			if (Properties.Settings.Default.IsRegionManagmentEnabled) 
			{
                slideFiltersAndLanguages.regionLanguageGroupGrid.Refresh();
                if (slideFiltersAndLanguages.regionLanguageGroupGrid.GetSelectedRegionLanguageIDs().Count == 0)
                {
                    MessageBox.Show("Please select at least one region/language for this slide", "Slide error", MessageBoxButtons.OK);
				    errValidateForm.SetError(slideFiltersAndLanguages, "Please select at least one region/language for this slide");
				    return false;
                }

			}

			return true;
		}

        #region other control events

        /// <summary>
        /// Handles the Click event of the btnChooseDeck control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void btnChooseDeck_Click(object sender, EventArgs e)
        {
            if (diaPickDeck.ShowDialog() == DialogResult.OK)
            {
                txtDeck.Text = diaPickDeck.FileName;
            }
        }

        /// <summary>
        /// Handles the MouseClick event of the lvwSlideThumbs control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Forms.MouseEventArgs"/> instance containing the event data.</param>
        private void lvwSlideThumbs_MouseClick(object sender, MouseEventArgs e)
        {
            //ListViewHitTestInfo hitTest = lvwSlideThumbs.HitTest(e.X, e.Y);
            //if (hitTest.Item != null)
            //{
            //    int slideIndex = hitTest.Item.Index;
            //    txtSlideTitle.Text = _SlideFiles[slideIndex].SlideTitle;
            //    lblSlideFileName.Text = Path.GetFileName(_SlideFiles[slideIndex].SlideFilePath);
            //    lblSlideFileSize.Text = _SlideFiles[slideIndex].SlideFileSize;
            //    webSlidePreview.Navigate(_SlideFiles[slideIndex].SlideFilePath);
            //    lvwSlideThumbs.Items[slideIndex].Selected = true;
            //}
        }

        /// <summary>
        /// Handles the DocumentCompleted event of the webSlidePreview control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Forms.WebBrowserDocumentCompletedEventArgs"/> instance containing the event data.</param>
        private void webSlidePreview_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            lvwSlideThumbs.Focus();
        } 

        #endregion

        #region splitting

        /// <summary>
        /// Determines whether [is ready for splitting].
        /// </summary>
        /// <returns>
        /// 	<c>true</c> if [is ready for splitting]; otherwise, <c>false</c>.
        /// </returns>
        private bool IsReadyForSplitting()
        {
			FileStream presStream = new FileStream(diaPickDeck.FileName, FileMode.Open, FileAccess.Read);
			Aspose.Slides.Presentation tempPresentation = new Aspose.Slides.Presentation(presStream);
            
			presStream.Close();
			bool fileExists = false;

			if (txtSlideFilePrefix.Text.Trim().Length == 0 || txtDeck.Text.Trim().Length == 0)
            {
				MessageBox.Show("You must complete Deck and Slide File Prefix before splitting the deck", "Missing Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return false;
            }

            ////check to make sure we aren't going to duplicate any filenames
            //for (int i = 1; i <= tempPresentation.Slides.Count; i++)
            //{
            //    if (_SlideLibraryDS.Slide.Select("FileName = '" + txtSlideFilePrefix.Text.Trim() + "_" + i + ".pot'").Length > 0)
            //    {
            //        fileExists = true;
            //    }
            //}

            wsIpgAdmin.Admin ws = new Admin.wsIpgAdmin.Admin();
            
            if (!ws.IsSlideFilenamePrefixValid(txtSlideFilePrefix.Text))
            {
                MessageBox.Show("Filename(s) with the same prefix already exist", "File(s) Exist", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Makes the slide files and images.
        /// </summary>
        /// <returns></returns>
        private void MakeSlideFiles()
        {

			_DeckSplitter = new PlugIns.Helpers.DeckSplitter();
            _DeckSplitter.WriteSlidesToFilesProgressReported += new PlugIns.Helpers.DeckSplitter.WriteSlidesToFilesProgressReportedDelegate(_DeckSplitter_WriteSlidesToFilesProgressReported);
            _DeckSplitter.WriteSlidesToFilesProgressCompleted += new PlugIns.Helpers.DeckSplitter.WriteSlidesToFilesCompletedDelegate(_DeckSplitter_WriteSlidesToFilesProgressCompleted);

            string fileToSplit = string.Empty;
            if (chkCompress.Checked)
            {
                fileToSplit = Path.Combine(((AdminMain)this.Owner).AppTempDirectory, Path.GetFileName(txtDeck.Text));
            }
            else
            {
                fileToSplit = txtDeck.Text;
            }
            _DeckSplitter.WriteSlidesToFilesAsync(fileToSplit, ((AdminMain)this.Owner).AppTempDirectory, txtSlideFilePrefix.Text, @"lic\Aspose.Slides.lic");
        }

        /// <summary>
        /// _s the deck splitter_ write slides to files progress completed.
        /// </summary>
        /// <param name="newSlideFiles">The new slide files.</param>
        private void _DeckSplitter_WriteSlidesToFilesProgressCompleted(List<string> newSlideFiles)
        {
			_SlideFilePaths = newSlideFiles;
            pbMainProgress.Value = 0;
            lblMainStatus.Text = "Preparing to create slide images...";
            bgwSlideFileOperations.RunWorkerAsync();
        }

        /// <summary>
        /// _s the deck splitter_ write slides to files progress reported.
        /// </summary>
        /// <param name="progressPercent">The progress percent.</param>
        /// <param name="message">The message.</param>
        private void _DeckSplitter_WriteSlidesToFilesProgressReported(int progressPercent, string message)
        {
            lblMainStatus.Text = message;
            pbMainProgress.Value = progressPercent;
        }

        /// <summary>
        /// Does the split complete actions.
        /// </summary>
        private void DoSplitCompleteActions()
        {
            tsSlideSpecs.Visible = true;
            lblMainStatus.Visible = false;
            
			pbMainProgress.Visible = false;

			txtSlideTitle.TextChanged -= txtSlideTitle_TextChanged;
			lvwSlideThumbs.Items[0].Selected = true;
			txtSlideTitle.TextChanged += new EventHandler(txtSlideTitle_TextChanged);
        }

        /// <summary>
        /// Handles the DoWork event of the bgwSlideFileOperations control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.DoWorkEventArgs"/> instance containing the event data.</param>
        private void bgwSlideFileOperations_DoWork(object sender, DoWorkEventArgs e)
        {

            _SlideFiles = new List<SlideFile>();
            int slidesProcessed = 0;
            double percentProcessed = 0;

            foreach (string slideFilePath in _SlideFilePaths)
            {
                SlideFile slideFile = new SlideFile();
                slideFile.SlideFilePath = slideFilePath;

                //get slide file size
                FileInfo fi = new FileInfo(slideFilePath);
                long fileSizeInKB = fi.Length / 1024;
                slideFile.SlideFileSize = Convert.ToString(fileSizeInKB);

                //get slide title
                string slideTitle = PlugIns.Helpers.PptUtils.GetSlideTitle(slideFilePath, 1, @"lic\Aspose.Slides.lic");
                slideFile.SlideTitle = slideTitle;
				Image slideThumb = null;
                //make slide thumb
				try
				{
					slideThumb = PlugIns.Helpers.PptUtils.GetSlideImage(slideFilePath, 0, 96, 72, @"lic\Aspose.Slides.lic");
				}
				catch (Exception ex)
				{
                    //if we can't get the slidethumb from the slide, give it this generic thumbnail
                    slideThumb = Properties.Resources.HpLogoThumbJPG;
				}
				
				slideFile.SlideImage = slideThumb;
                string slideThumbFilePath = slideFilePath.Replace(".pot", ".jpg");
                slideThumb.Save(slideThumbFilePath, System.Drawing.Imaging.ImageFormat.Jpeg);
                slideFile.SlideImagePath = slideThumbFilePath;

                _SlideFiles.Add(slideFile);
                slidesProcessed++;
                percentProcessed = (Convert.ToDouble(slidesProcessed) / Convert.ToDouble(_SlideFilePaths.Count)) * 100.00;
                bgwSlideFileOperations.ReportProgress(Convert.ToInt32(percentProcessed), String.Format("Creating slide image {0} of {1}", slidesProcessed, _SlideFilePaths.Count));

            }
        }

        /// <summary>
        /// Handles the RunWorkerCompleted event of the bgwSlideFileOperations control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.RunWorkerCompletedEventArgs"/> instance containing the event data.</param>
        private void bgwSlideFileOperations_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //if (_IsCompressed)
            //{
            //    SortedList<string, string> sourceAndTargetFiles = new SortedList<string, string>();
            //    foreach (string file in _SlideFilePaths)
            //    {
            //        sourceAndTargetFiles.Add(file, file);
            //    }
            //    PlugIns.Helpers.PowerPointCompressor compressor = new PlugIns.Helpers.PowerPointCompressor();
            //    compressor.ProgressChanged += new ProgressChangedEventHandler(compressor_ProgressChanged);
            //    compressor.RunWorkerCompleted += new RunWorkerCompletedEventHandler(compressor_RunWorkerCompleted);
            //    lblMainStatus.Text = "Initializing split progress...";
            //    lblMainStatus.Visible = true;
            //    pbMainProgress.Value = 0;
            //    pbMainProgress.Visible = true;
            //    //compressor.CompressPresentations(sourceAndTargetFiles,
            //    //    cboMainAllowJpeg.SelectedItem.ToString() == "Yes" ? true : false,
            //    //    cboFlattenEmbeddedObjects.SelectedItem.ToString() == "Yes" ? true : false,
            //    //    Convert.ToInt32(cboMainJpegQuality.SelectedItem.ToString()));
            //}
            //else
            //{
                LoadListView();
                DoSplitCompleteActions();
            //}
            this.Height = this.Height + 550;
            splForm.Panel2Collapsed = false;
            splForm.Panel1.Enabled = false;
            tsbMainSplitOnly.Enabled = false;
            tsbMainUpload.Enabled = true;
        }

        /// <summary>
        /// Handles the ProgressChanged event of the bgwSlideFileOperations control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.ProgressChangedEventArgs"/> instance containing the event data.</param>
        private void bgwSlideFileOperations_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            pbMainProgress.Value = e.ProgressPercentage;
            lblMainStatus.Text = e.UserState.ToString();
        } 

        #endregion        

        #region misc

        /// <summary>
        /// Sets up configurable features.
        /// </summary>
        private void CustomFeaturesSetup()
        {
            wsIpgAdmin.Admin adminWS = new Admin.wsIpgAdmin.Admin();
            wsIpgAdmin.SlideLibrary dsFilterAndRegLangInfo = new Admin.wsIpgAdmin.SlideLibrary();

            //get the approprate data to fill the the SlideFiltersAndLanguagesControl
            if (Properties.Settings.Default.IsRegionManagmentEnabled)
            {
                adminWS.Shizmo();
                dsFilterAndRegLangInfo.Merge(adminWS.RegionsAndLanguagesGet());
            }

            if (Properties.Settings.Default.IsFilterManagementEnabled)
            {
                dsFilterAndRegLangInfo.Merge(adminWS.FilterGet());
            }

            _SlideLibraryDS.Merge(dsFilterAndRegLangInfo);
            //get the appropriate FillType
            Admin.UserControls.SlideFiltersAndLanguagesFillType fillType;
            if (Properties.Settings.Default.IsFilterManagementEnabled && Properties.Settings.Default.IsRegionManagmentEnabled)
            {
                fillType = Admin.UserControls.SlideFiltersAndLanguagesFillType.FiltersAndRegionLanguages;
            }
            else if (Properties.Settings.Default.IsFilterManagementEnabled && !Properties.Settings.Default.IsRegionManagmentEnabled)
            {
                fillType = Admin.UserControls.SlideFiltersAndLanguagesFillType.FiltersOnly;
            }
            else if (!Properties.Settings.Default.IsFilterManagementEnabled && Properties.Settings.Default.IsRegionManagmentEnabled)
            {
                fillType = Admin.UserControls.SlideFiltersAndLanguagesFillType.RegionLanguagesOnly;
            }
            else
            {
                fillType = Admin.UserControls.SlideFiltersAndLanguagesFillType.FiltersAndRegionLanguages;
            }

            //if either of these settings are true we need to show the user this panel and fill the control
            if (Properties.Settings.Default.IsFilterManagementEnabled || Properties.Settings.Default.IsRegionManagmentEnabled)
            {
                splWebFilterLang.Panel2Collapsed = false;
                slideFiltersAndLanguages.Fill(dsFilterAndRegLangInfo, fillType);

            }
            else
            {
                splWebFilterLang.Panel2Collapsed = true;
            }

        }

        /// <summary>
        /// Loads the list view.
        /// </summary>
        /// <param name="newSlideImageFiles">The new slide image files.</param>
        private void LoadListView()
        {
            lvwSlideThumbs.Items.Clear();
            foreach (SlideFile slideFile in _SlideFiles)
            {
                imlSlideThumbs.Images.Add(Path.GetFileNameWithoutExtension(slideFile.SlideImagePath), slideFile.SlideImage);
                ListViewItem lvItem = new ListViewItem();
                lvItem.Text = "Title: " + slideFile.SlideTitle + System.Environment.NewLine + "File: " + Path.GetFileNameWithoutExtension(slideFile.SlideImagePath);
                lvItem.ImageKey = Path.GetFileNameWithoutExtension(slideFile.SlideImagePath);
                lvwSlideThumbs.Items.Add(lvItem);

            }
        }

        /// <summary>
        /// Deletes the temp files.
        /// </summary>
        private void DeleteTempFiles()
        {
            List<string> filesInTemp = new List<string>(Directory.GetFiles(((AdminMain)this.Owner).AppTempDirectory));
            foreach (string file in filesInTemp)
            {
                if (File.Exists(file))
                {
                    try
                    {
                        File.Delete(file);
                    }
                    catch (Exception ex)
                    {
                        //do nothing 
                    }
                }
            }
        }

        /// <summary>
        /// Resets the form.
        /// </summary>
        private void ResetForm()
        {
			KillPowerPoint();
			txtDeck.Text = "";
            txtSlideFilePrefix.Text = "";
            this.Height = 190;
            splForm.Panel2Collapsed = true;
            splForm.Panel1.Enabled = true;
            tsbMainUpload.Enabled = false;
            tsbMainSplitOnly.Enabled = true;
            DeleteTempFiles();
        }

		private void KillPowerPoint()
		{
			System.Diagnostics.Process[] processes = System.Diagnostics.Process.GetProcessesByName("POWERPNT");
			if (processes.Length > 0)
			{
				processes[0].Kill();
			}
		}

        #endregion

        #region uploading

        /// <summary>
        /// Uploads the files.
        /// </summary>
        private void UploadFiles()
        {
            tsbMainUpload.Enabled = false;
            tsbMainReset.Enabled = false;

            //get the slide files and thumbnails to upload
            List<string> slideFilesToUpload = new List<string>();
            List<string> thumbnailFilesToUpload = new List<string>();

            foreach (SlideFile slidey in _SlideFiles)
            {
                slideFilesToUpload.Add(slidey.SlideFilePath);
                thumbnailFilesToUpload.Add(slidey.SlideImagePath);
            }

            _SlidesToUpload = slideFilesToUpload;
            _ThumbnailsToUpload = thumbnailFilesToUpload;

            _SlideFileUploader = new HpDm.BLL.FileUploader();
			_SlideFileUploader.UploadSlideFilesProgressReported += new HpDm.BLL.FileUploader.UploadSlideFilesProgressReportedDelegate(fileUploader_UploadSlideFilesProgressReported);
            _SlideFileUploader.UploadSlideFilesCompleted += new HpDm.BLL.FileUploader.UploadSlideFilesCompletedDelegate(fileUploader_UploadSlideFilesCompleted);

            _ThumbnailFileUploader = new HpDm.BLL.FileUploader();
			_ThumbnailFileUploader.UploadSlideFilesProgressReported += new HpDm.BLL.FileUploader.UploadSlideFilesProgressReportedDelegate(thumbnailFileUploader_UploadSlideFilesProgressReported);
            _ThumbnailFileUploader.UploadSlideFilesCompleted += new HpDm.BLL.FileUploader.UploadSlideFilesCompletedDelegate(thumbnailFileUploader_UploadSlideFilesCompleted);

            //get this ready so we can put the failed files in it when the upload is completed
            _FailedFiles = new List<string>();

            //start this up, we'll stop it when both uploads are complete
            timerCheckUploadCompleted.Start();



            //start uploading
            _SlideFileUploader.UploadSlideFilesAsync(slideFilesToUpload, Properties.Settings.Default.SlideUploadURI, true);
            _ThumbnailFileUploader.UploadSlideFilesAsync(thumbnailFilesToUpload, Properties.Settings.Default.ThumbnailUploadURI, true);

            //upload to NetStorage if congured to do so
            if (Properties.Settings.Default.IsNetStorageUtilized)
            {
                _IsNetStorageUploadComplete = false;
                bgwNetStorageUpload.RunWorkerAsync();
            }

            //show the status controls
            toolStripStatusLabelSlideUpload.Visible = true;
            toolStripStatusLabelSlideUploadStatus.Visible = true;
            toolStripStatusLabelThumbnailUpload.Visible = true;
            toolStripStatusLabelThumbnailUploadStatus.Visible = true;
            toolStripProgressBarSlideUpload.Visible = true;
            toolStripProgressBarThumbnailUpload.Visible = true;
        }

        /// <summary>
        /// Thumbnails the file uploader_ upload slide files completed.
        /// </summary>
        /// <param name="failedUploadFiles">The failed upload files.</param>
        private void thumbnailFileUploader_UploadSlideFilesCompleted(List<string> failedUploadFiles)
        {
            toolStripStatusLabelThumbnailUploadStatus.Text = "Thumbnail upload complete";
            toolStripStatusLabelThumbnailUpload.Visible = false;
            _FailedFiles.AddRange(failedUploadFiles);
        }

        /// <summary>
        /// Thumbnails the file uploader_ upload slide files progress reported.
        /// </summary>
        /// <param name="progressReported">The progress reported.</param>
        /// <param name="message">The message.</param>
        private void thumbnailFileUploader_UploadSlideFilesProgressReported(int progressReported, string message)
        {
            toolStripProgressBarThumbnailUpload.Value = progressReported;
            toolStripStatusLabelThumbnailUploadStatus.Text = message;
        }

        /// <summary>
        /// Files the uploader_ upload slide files completed.
        /// </summary>
        /// <param name="failedUploadFiles">The failed upload files.</param>
        private void fileUploader_UploadSlideFilesCompleted(List<string> failedUploadFiles)
        {
            toolStripStatusLabelSlideUploadStatus.Text = "Slide upload complete";
            toolStripStatusLabelSlideUpload.Visible = false;
            _FailedFiles.AddRange(failedUploadFiles);
        }

        /// <summary>
        /// Files the uploader_ upload slide files progress reported.
        /// </summary>
        /// <param name="progressReported">The progress reported.</param>
        /// <param name="message">The message.</param>
        private void fileUploader_UploadSlideFilesProgressReported(int progressReported, string message)
        {
            toolStripProgressBarSlideUpload.Value = progressReported;
            toolStripStatusLabelSlideUploadStatus.Text = message;
        }

        /// <summary>
        /// Handles the Tick event of the timerCheckUploadCompleted control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void timerCheckUploadCompleted_Tick(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.IsNetStorageUtilized)
            {
                //if they both aren't busy, we're done
                if (!_SlideFileUploader.IsBusy && !_ThumbnailFileUploader.IsBusy && _IsNetStorageUploadComplete) 
                {
                    timerCheckUploadCompleted.Stop();
                    UploadComplete();

                }
            }
            else
            {
                //if they both aren't busy, we're done
                if (!_SlideFileUploader.IsBusy && !_ThumbnailFileUploader.IsBusy)
                {
                    timerCheckUploadCompleted.Stop();
                    UploadComplete();

                }
            }

        }

        /// <summary>
        /// Call this when all of the uploading is done.
        /// </summary>
        private void UploadComplete()
        {
            UpdateSlideData();
            
        } 

        #endregion

        #region data get/save

        /// <summary>
        /// Updates the slide data.
        /// </summary>
        private void UpdateSlideData()
        {
            HpDm.DAL.Slide slideDAL = new HpDm.DAL.Slide();
            slideDAL.UpdateCompleted += new HpDm.DAL.Slide.UpdateCompletedDelegate(slideDAL_UpdateCompleted);
           
            //insert slide rows
            foreach (SlideFile slideFile in _SlideFiles)
            {
                string filter = String.Format("FileName = '{0}'", Path.GetFileName(slideFile.SlideFilePath));
                if (_SlideLibraryDS.Slide.Select(filter).Length == 0)
                {
                    wsIpgAdmin.SlideLibrary.SlideRow slideRow = _SlideLibraryDS.Slide.NewSlideRow();
                    slideRow.FileName = Path.GetFileName(slideFile.SlideFilePath);
                    slideRow.Title = slideFile.SlideTitle;
                    slideRow.DateModified = DateTime.UtcNow;
                    _SlideLibraryDS.Slide.AddSlideRow(slideRow);
                }
            }
            

            List<int> selectedFilterIDs = slideFiltersAndLanguages.filterTabs.GetSelectedFilterIDs();
            List<int> selectedRegionLanguageIDs = slideFiltersAndLanguages.regionLanguageGroupGrid.GetSelectedRegionLanguageIDs();
            //insert SlideFilters and/or SlideRegionLanguage
            foreach (wsIpgAdmin.SlideLibrary.SlideRow slideRow in _SlideLibraryDS.Slide.Rows)
            {
                //there's a whole bunch of slide rows in the dataset...
                //just create SlideFilters and SlideRegionLanguages for the slides we just added
                if (slideRow.RowState == DataRowState.Added)
                {
                    foreach (int filterID in selectedFilterIDs)
                    {
                        wsIpgAdmin.SlideLibrary.SlideFilterRow slideFilterRow = _SlideLibraryDS.SlideFilter.NewSlideFilterRow();
                        slideFilterRow.FilterID = filterID;
                        slideFilterRow.SlideID = slideRow.SlideID;
                        _SlideLibraryDS.SlideFilter.AddSlideFilterRow(slideFilterRow);
                    }

                    foreach (int regLangID in selectedRegionLanguageIDs)
                    {
                        wsIpgAdmin.SlideLibrary.SlideRegionLanguageRow slideRegionLanguageRow = _SlideLibraryDS.SlideRegionLanguage.NewSlideRegionLanguageRow();
                        slideRegionLanguageRow.RegionLanguageID = regLangID;
                        slideRegionLanguageRow.SlideID = slideRow.SlideID;
                        _SlideLibraryDS.SlideRegionLanguage.AddSlideRegionLanguageRow(slideRegionLanguageRow);
                    }
                }
            }


            wsIpgAdmin.SlideLibrary dsChanges = new Admin.wsIpgAdmin.SlideLibrary();
            dsChanges.Merge(_SlideLibraryDS.GetChanges());

            slideDAL.UpdateAsync(dsChanges);
        }

        /// <summary>
        /// Slides the DA l_ update completed.
        /// </summary>
        /// <param name="success">if set to <c>true</c> [success].</param>
        /// <param name="slideLibrary">The slide library.</param>
        private void slideDAL_UpdateCompleted(bool success, Admin.wsIpgAdmin.SlideLibrary slideLibrary)
        {
			if (success)
			{
				// See if there are any slides in the failed file list
				bool containsFailedFiles = _FailedFiles.Count > 0 ? true : false;
				bool foundSuccessfulFile = true;
				string fileName = string.Empty;
				int slideID = -1;

				// If there are failed slides, go through the file list and get the first successful file
				if (containsFailedFiles)
				{
					// Get the slideID and refresh the Slide List and the Slide Browser
					foreach (string successfulFile in _SlideFilePaths)
					{
						// See if the the current file is in the failed file list
						foreach (string failedFile in _FailedFiles)
						{
							if (Path.GetFileName(successfulFile).Equals(failedFile))
							{
								foundSuccessfulFile = false;
								break;
							}
							else
							{
								foundSuccessfulFile = true;
							}
						}

						// Successfully loaded slide, set this slide as the reselect slide after the refresh has completed
						if (foundSuccessfulFile)
						{
							slideID = ((wsIpgAdmin.SlideLibrary.SlideRow[])slideLibrary.Slide.Select("FileName = '" + Path.GetFileName(successfulFile) + "'"))[0].SlideID;
							fileName = Path.GetFileName(successfulFile);
						}
					}
				}
				else
				{
					fileName = Path.GetFileName(_SlideFiles[0].SlideFilePath);
					slideID = ((wsIpgAdmin.SlideLibrary.SlideRow[])slideLibrary.Slide.Select("FileName = '" + fileName + "'"))[0].SlideID;
				}

				// Refresh
                //((Admin.AdminMain)this.Owner).GetSlideLibraryData();
                //((Admin.AdminMain)this.Owner).RefreshSlideListBySlideID(slideID);
                //((Admin.AdminMain)this.Owner).LoadSlideToBrowser(fileName);
                DeckUploadedEventEventArgs args = new DeckUploadedEventEventArgs(slideLibrary, null);
                DeckUploaded(args);

				this.Close();
			}
        }

        /// <summary>
        /// Gets the slide data.
        /// </summary>
        private void GetSlideData()
        {
            HpDm.DAL.Slide slideDAL = new HpDm.DAL.Slide();
            slideDAL.GetAllCompleted += new HpDm.DAL.Slide.GetAllCompletedDelegate(slideDAL_GetAllCompleted);
            slideDAL.GetAllAsync();
        }

        /// <summary>
        /// Slides the DA l_ get all completed.
        /// </summary>
        /// <param name="success">if set to <c>true</c> [success].</param>
        /// <param name="slideLibrary">The slide library.</param>
        private void slideDAL_GetAllCompleted(bool success, Admin.wsIpgAdmin.SlideLibrary slideLibrary)
        {
            
            if (success)
            {
                _SlideLibraryDS = new Admin.wsIpgAdmin.SlideLibrary();
                _SlideLibraryDS.Clear();
                _SlideLibraryDS.Merge(slideLibrary);
            }
        }
        
        #endregion

        private void lvwSlideThumbs_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
			if (lvwSlideThumbs.SelectedItems.Count == 1)
            {
				int slideIndex = lvwSlideThumbs.SelectedItems[0].Index;
				txtSlideTitle.Text = _SlideFiles[slideIndex].SlideTitle;
                lblSlideFileName.Text = Path.GetFileName(_SlideFiles[slideIndex].SlideFilePath);
                lblSlideFileSize.Text = _SlideFiles[slideIndex].SlideFileSize;
                webSlidePreview.Navigate(_SlideFiles[slideIndex].SlideFilePath); 
            }
        }

		/// <summary>
		/// Changes the slide title
		/// </summary>
		private void txtSlideTitle_TextChanged(object sender, EventArgs e)
		{

		}

		private void DeckUploader_FormClosing(object sender, FormClosingEventArgs e)
		{
            DeleteTempFiles();
		}

		private void txtSlideTitle_Leave(object sender, EventArgs e)
		{
			if (lvwSlideThumbs.SelectedItems.Count == 1 && txtSlideTitle.Text.Length > 0)
			{
				int slideFileIndex = lvwSlideThumbs.SelectedItems[0].Index;
				_SlideFiles[slideFileIndex].SlideTitle = txtSlideTitle.Text;

				LoadListView();
			}
		}

        private void bgwNetStorageUpload_DoWork(object sender, DoWorkEventArgs e)
        {
            
            Utilities.FTP.FTPclient ftp = new Utilities.FTP.FTPclient(Properties.Settings.Default.NetStorageFtp, Properties.Settings.Default.NetStorageUserName, Properties.Settings.Default.NetStoragePassword);

            bgwNetStorageUpload.ReportProgress(0, "Uploading to slides to Net Storage...");

            int slidesUploaded = 0;
            foreach (string slideFile in (_SlidesToUpload))
            {
                ftp.Upload(slideFile, String.Format("/31505/Ppt/{0}.pot", Path.GetFileNameWithoutExtension(slideFile)));
                bgwNetStorageUpload.ReportProgress(Convert.ToInt32((Convert.ToDouble(slidesUploaded++) / Convert.ToDouble(_SlidesToUpload.Count)) * 100.00), "Uploading to slides to Net Storage...");
            }

            bgwNetStorageUpload.ReportProgress(0, "Uploading to thumbnails to Net Storage...");
            int thumbsUploaded = 0;
            foreach (string thumbFile in _ThumbnailsToUpload)
            {
                ftp.Upload(thumbFile, String.Format("/31505/Images/ThumbSlide/{0}", Path.GetFileName(thumbFile)));
                bgwNetStorageUpload.ReportProgress(Convert.ToInt32((Convert.ToDouble(thumbsUploaded++) / Convert.ToDouble(_ThumbnailsToUpload.Count)) * 100.00), "Uploading to thumbnails to Net Storage...");
            }
        }

        private void bgwNetStorageUpload_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            _IsNetStorageUploadComplete = true;
        }

        private void bgwNetStorageUpload_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            lblMainStatus.Visible = true;
            pbMainProgress.Visible = true;
            pbMainProgress.Value = e.ProgressPercentage;
            lblMainStatus.Text = e.UserState.ToString();
        }

        public delegate void DeckUploadedDelegate(DeckUploadedEventEventArgs e);
        public event DeckUploadedDelegate DeckUploaded;

    }

    public class SlideFile
    {
        #region properties

        private string _SlideFilePath;

        public string SlideFilePath
        {
            get { return _SlideFilePath; }
            set { _SlideFilePath = value; }
        }

        private string _SlideImagePath;

        public string SlideImagePath
        {
            get { return _SlideImagePath; }
            set { _SlideImagePath = value; }
        }

        private string _SlideTitle;

        public string SlideTitle
        {
            get { return _SlideTitle; }
            set { _SlideTitle = value; }
        }

        private string _SlideFileSize;

        public string SlideFileSize
        {
            get { return _SlideFileSize; }
            set { _SlideFileSize = value; }
        }

        private Image _SlideImage;

        public Image SlideImage
        {
            get { return _SlideImage; }
            set { _SlideImage = value; }
        }


        #endregion
    }

    public class DeckUploadedEventEventArgs : EventArgs
    {
        private wsIpgAdmin.SlideLibrary _NewSlideData;
        public wsIpgAdmin.SlideLibrary NewSlideData
        {
            get { return _NewSlideData; }
            private set { _NewSlideData = value; }
        }

        private Exception _Error;
        public Exception Error
        {
            get { return _Error; }
            private set { _Error = value; }
        }

        public DeckUploadedEventEventArgs(wsIpgAdmin.SlideLibrary newSlideData, Exception error)
        {
            NewSlideData = newSlideData;
            Error = error;
        }
    }
}