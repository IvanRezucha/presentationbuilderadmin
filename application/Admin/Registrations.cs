using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Admin
{
    public partial class Registrations : MasterForm
    {
        wsRegistration.Registration regWS;

        public Registrations()
        {
            InitializeComponent();
        }

        private void Registrations_Load(object sender, EventArgs e)
        {
            //dsReg = new Admin.wsRegistration.RegistrationDS();
            regWS = new Admin.wsRegistration.Registration();
            regWS.GetRegistrantsCompleted += new Admin.wsRegistration.GetRegistrantsCompletedEventHandler(regWS_GetRegistrantsCompleted);

            wsRegistration.RegistrantTypes regType;
            if (Properties.Settings.Default.RegistartionDB == "Ipg")
            {
                regType = Admin.wsRegistration.RegistrantTypes.Ipg;
            }
            else if (Properties.Settings.Default.RegistartionDB == "IpgPartner")
            {
                regType = Admin.wsRegistration.RegistrantTypes.IpgParnter;
            }
            else if (Properties.Settings.Default.RegistartionDB == "Cs")
            {
                regType = Admin.wsRegistration.RegistrantTypes.ConsumerSales;
            }
            else if (Properties.Settings.Default.RegistartionDB == "CsPartner")
            {
                regType = Admin.wsRegistration.RegistrantTypes.ConsumerSalesPartner;
            }
            else if (Properties.Settings.Default.RegistartionDB == "HpPrinting")
            {
                regType = Admin.wsRegistration.RegistrantTypes.HpPrinting;
            }
            else
            {
                ApplicationException ax = new ApplicationException("Invalid registration database configuration value.");
                throw ax;
            }

            regWS.GetRegistrantsAsync(regType);
        }

        private void regWS_GetRegistrantsCompleted(object sender, Admin.wsRegistration.GetRegistrantsCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                MessageBox.Show("Operation cancelled.");
            }
            else if (e.Error != null)
            {
                MessageBox.Show("Error retrieving registrants.");
            }
            else
            {
                dsReg.Clear();
                dsReg.Merge((wsRegistration.RegistrationDS)e.Result);
                if (Properties.Settings.Default.RegistartionDB == "Ipg")
                {
                    lblTotalRegistrantCount.Text = dgvIpg.Rows.Count.ToString();
                    dgvIpg.Visible = true;
                    dgvIpg.Dock = DockStyle.Fill;
                    dgvIpg.BringToFront();
                    bnMain.BindingSource = bsIpg;
                }
                else if (Properties.Settings.Default.RegistartionDB == "IpgPartner")
                {
                    lblTotalRegistrantCount.Text = dgvIpgPartner.Rows.Count.ToString();
                    dgvIpgPartner.Visible = true;
                    dgvIpgPartner.Dock = DockStyle.Fill;
                    dgvIpgPartner.BringToFront();
                    bnMain.BindingSource = bsIpgPartner;
                }
                else if (Properties.Settings.Default.RegistartionDB == "Cs")
                {
                    lblTotalRegistrantCount.Text = dgvConsumerSales.Rows.Count.ToString();
                    dgvConsumerSales.Visible = true;
                    dgvConsumerSales.Dock = DockStyle.Fill;
                    dgvConsumerSales.BringToFront();
                    bnMain.BindingSource = bsConsumnerSales;
                }
                else if (Properties.Settings.Default.RegistartionDB == "CsPartner")
                {
                    lblTotalRegistrantCount.Text = dgvConsumerSalesPartner.Rows.Count.ToString();
                    dgvConsumerSalesPartner.Visible = true;
                    dgvConsumerSalesPartner.Dock = DockStyle.Fill;
                    dgvConsumerSalesPartner.BringToFront();
                    bnMain.BindingSource = bsConsumnerSales;
                }
                else if (Properties.Settings.Default.RegistartionDB == "HpPrinting")
                {
                    lblTotalRegistrantCount.Text = dgvHpPrinting.Rows.Count.ToString();
                    dgvHpPrinting.Visible = true;
                    dgvHpPrinting.Dock = DockStyle.Fill;
                    dgvHpPrinting.BringToFront();
                    bnMain.BindingSource = bsHpPrinting;
                }
                else
                {
                    ApplicationException ax = new ApplicationException("Invalid registration database configuration value.");
                    throw ax;
                }

                //dgvRegistrants.DataSource = dsReg;
                //lblTotalRegistrantCount.Text = dgvRegistrants.Rows.Count.ToString();
            }
        }


    }
}