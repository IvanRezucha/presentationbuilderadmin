namespace Admin
{
	partial class SlideMultipleDelete
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.admin = new Admin.wsIpgAdmin.Admin();
			this.slideLibrary = new Admin.wsIpgAdmin.SlideLibrary();
			this.lblSlidesCaption = new System.Windows.Forms.Label();
			this.slideBindingSource = new System.Windows.Forms.BindingSource(this.components);
			this.dgvDeletedSlides = new System.Windows.Forms.DataGridView();
			this.slideIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.titleDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.fileNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dateModifiedDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.numInLibDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.lblRelationshipCaption = new System.Windows.Forms.Label();
			this.btnClose = new System.Windows.Forms.Button();
			this.btnDelete = new System.Windows.Forms.Button();
			this.tabTreeviewPaths = new System.Windows.Forms.TabControl();
			((System.ComponentModel.ISupportInitialize)(this.slideLibrary)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.slideBindingSource)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dgvDeletedSlides)).BeginInit();
			this.SuspendLayout();
			// 
			// admin
			// 
			this.admin.Credentials = null;
			this.admin.Url = "http://localhost/IpgMediaViewer/Services/Update/Admin.asmx";
			this.admin.UseDefaultCredentials = false;
			// 
			// slideLibrary
			// 
			this.slideLibrary.DataSetName = "SlideLibrary";
			this.slideLibrary.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
			// 
			// lblSlidesCaption
			// 
			this.lblSlidesCaption.Location = new System.Drawing.Point(12, 9);
			this.lblSlidesCaption.Name = "lblSlidesCaption";
			this.lblSlidesCaption.Size = new System.Drawing.Size(523, 29);
			this.lblSlidesCaption.TabIndex = 0;
			this.lblSlidesCaption.Text = "Deleting the following slide(s) will permanetly remove them from the slide list a" +
				"s well as remove all instances from the Slide Library. Do you want to do this?";
			// 
			// slideBindingSource
			// 
			this.slideBindingSource.DataMember = "Slide";
			this.slideBindingSource.DataSource = this.slideLibrary;
			// 
			// dgvDeletedSlides
			// 
			this.dgvDeletedSlides.AllowUserToAddRows = false;
			this.dgvDeletedSlides.AllowUserToDeleteRows = false;
			this.dgvDeletedSlides.AllowUserToResizeColumns = false;
			this.dgvDeletedSlides.AllowUserToResizeRows = false;
			this.dgvDeletedSlides.AutoGenerateColumns = false;
			this.dgvDeletedSlides.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.dgvDeletedSlides.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.slideIDDataGridViewTextBoxColumn,
            this.titleDataGridViewTextBoxColumn,
            this.fileNameDataGridViewTextBoxColumn,
            this.dateModifiedDataGridViewTextBoxColumn,
            this.numInLibDataGridViewTextBoxColumn});
			this.dgvDeletedSlides.DataSource = this.slideBindingSource;
			this.dgvDeletedSlides.Location = new System.Drawing.Point(12, 41);
			this.dgvDeletedSlides.MultiSelect = false;
			this.dgvDeletedSlides.Name = "dgvDeletedSlides";
			this.dgvDeletedSlides.ReadOnly = true;
			this.dgvDeletedSlides.RowHeadersWidth = 4;
			this.dgvDeletedSlides.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.dgvDeletedSlides.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dgvDeletedSlides.Size = new System.Drawing.Size(523, 115);
			this.dgvDeletedSlides.TabIndex = 3;
			this.dgvDeletedSlides.VirtualMode = true;
			this.dgvDeletedSlides.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dgvDeletedSlides_MouseDown);
			// 
			// slideIDDataGridViewTextBoxColumn
			// 
			this.slideIDDataGridViewTextBoxColumn.DataPropertyName = "SlideID";
			this.slideIDDataGridViewTextBoxColumn.HeaderText = "SlideID";
			this.slideIDDataGridViewTextBoxColumn.Name = "slideIDDataGridViewTextBoxColumn";
			this.slideIDDataGridViewTextBoxColumn.ReadOnly = true;
			this.slideIDDataGridViewTextBoxColumn.Visible = false;
			// 
			// titleDataGridViewTextBoxColumn
			// 
			this.titleDataGridViewTextBoxColumn.DataPropertyName = "Title";
			this.titleDataGridViewTextBoxColumn.HeaderText = "Title";
			this.titleDataGridViewTextBoxColumn.Name = "titleDataGridViewTextBoxColumn";
			this.titleDataGridViewTextBoxColumn.ReadOnly = true;
			this.titleDataGridViewTextBoxColumn.Width = 275;
			// 
			// fileNameDataGridViewTextBoxColumn
			// 
			this.fileNameDataGridViewTextBoxColumn.DataPropertyName = "FileName";
			this.fileNameDataGridViewTextBoxColumn.HeaderText = "File Name";
			this.fileNameDataGridViewTextBoxColumn.Name = "fileNameDataGridViewTextBoxColumn";
			this.fileNameDataGridViewTextBoxColumn.ReadOnly = true;
			this.fileNameDataGridViewTextBoxColumn.Width = 170;
			// 
			// dateModifiedDataGridViewTextBoxColumn
			// 
			this.dateModifiedDataGridViewTextBoxColumn.DataPropertyName = "DateModified";
			this.dateModifiedDataGridViewTextBoxColumn.HeaderText = "DateModified";
			this.dateModifiedDataGridViewTextBoxColumn.Name = "dateModifiedDataGridViewTextBoxColumn";
			this.dateModifiedDataGridViewTextBoxColumn.ReadOnly = true;
			this.dateModifiedDataGridViewTextBoxColumn.Visible = false;
			// 
			// numInLibDataGridViewTextBoxColumn
			// 
			this.numInLibDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			this.numInLibDataGridViewTextBoxColumn.DataPropertyName = "NumInLib";
			this.numInLibDataGridViewTextBoxColumn.HeaderText = "Instances";
			this.numInLibDataGridViewTextBoxColumn.Name = "numInLibDataGridViewTextBoxColumn";
			this.numInLibDataGridViewTextBoxColumn.ReadOnly = true;
			// 
			// lblRelationshipCaption
			// 
			this.lblRelationshipCaption.AutoSize = true;
			this.lblRelationshipCaption.Location = new System.Drawing.Point(12, 170);
			this.lblRelationshipCaption.Name = "lblRelationshipCaption";
			this.lblRelationshipCaption.Size = new System.Drawing.Size(154, 13);
			this.lblRelationshipCaption.TabIndex = 4;
			this.lblRelationshipCaption.Text = "Instances from the Slide Library";
			// 
			// btnClose
			// 
			this.btnClose.Location = new System.Drawing.Point(460, 427);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new System.Drawing.Size(75, 23);
			this.btnClose.TabIndex = 6;
			this.btnClose.Text = "Close";
			this.btnClose.UseVisualStyleBackColor = true;
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			// 
			// btnDelete
			// 
			this.btnDelete.Location = new System.Drawing.Point(379, 427);
			this.btnDelete.Name = "btnDelete";
			this.btnDelete.Size = new System.Drawing.Size(75, 23);
			this.btnDelete.TabIndex = 7;
			this.btnDelete.Text = "Delete All";
			this.btnDelete.UseVisualStyleBackColor = true;
			this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
			// 
			// tabTreeviewPaths
			// 
			this.tabTreeviewPaths.Location = new System.Drawing.Point(15, 186);
			this.tabTreeviewPaths.Name = "tabTreeviewPaths";
			this.tabTreeviewPaths.SelectedIndex = 0;
			this.tabTreeviewPaths.Size = new System.Drawing.Size(520, 219);
			this.tabTreeviewPaths.TabIndex = 8;
			// 
			// SlideMultipleDelete
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(547, 462);
			this.Controls.Add(this.tabTreeviewPaths);
			this.Controls.Add(this.btnDelete);
			this.Controls.Add(this.btnClose);
			this.Controls.Add(this.lblRelationshipCaption);
			this.Controls.Add(this.dgvDeletedSlides);
			this.Controls.Add(this.lblSlidesCaption);
			this.Name = "SlideMultipleDelete";
			this.Text = "Delete Slide";
			((System.ComponentModel.ISupportInitialize)(this.slideLibrary)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.slideBindingSource)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dgvDeletedSlides)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private Admin.wsIpgAdmin.Admin admin;
		private Admin.wsIpgAdmin.SlideLibrary slideLibrary;
		private System.Windows.Forms.Label lblSlidesCaption;
		private System.Windows.Forms.BindingSource slideBindingSource;
		private System.Windows.Forms.Label lblRelationshipCaption;
		private System.Windows.Forms.Button btnClose;
		private System.Windows.Forms.DataGridView dgvDeletedSlides;
		private System.Windows.Forms.Button btnDelete;
		public System.Windows.Forms.TabControl tabTreeviewPaths;
		private System.Windows.Forms.DataGridViewTextBoxColumn slideIDDataGridViewTextBoxColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn titleDataGridViewTextBoxColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn fileNameDataGridViewTextBoxColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn dateModifiedDataGridViewTextBoxColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn numInLibDataGridViewTextBoxColumn;
	}
}