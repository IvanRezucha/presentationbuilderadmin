using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Admin
{
    public partial class SupportFileSave : MasterForm
    {
        Mode CurrentMode;
        int CurrentSupportFileID;
        wsIpgAdmin.SupportFileDS dsSupportFile;
		private int _supportFileID;

        public SupportFileSave(SupportFileSave.Mode mode, int supportFileID)
        {
            InitializeComponent();
            CurrentMode = mode;
			Admin.wsIpgAdmin.Admin admin = new Admin.wsIpgAdmin.Admin();
			dsSupportFile = admin.SupportFileGetAll();
			_supportFileID = supportFileID;
        }
		
        public enum Mode
        {
            New,
            Edit
        }

        private void SupportFileSave_Load(object sender, EventArgs e)
        {
			regionLanguageGroupGrid.FillSupportFileRegionLanguages(((SupportFile)this.Owner).dsSupportFile);
			FillForm(_supportFileID);
        }

        private void FillForm(int supportFileID)
        {
            wsIpgAdmin.SupportFileDS.SupportFileRow supportFile;
            if (CurrentMode == Mode.New)
            {
                supportFile = dsSupportFile.SupportFile.NewSupportFileRow();
                supportFile.FileName = "";
                supportFile.ModifyDate = DateTime.Now;
                dsSupportFile.SupportFile.AddSupportFileRow(supportFile);

            }
            else //if(currentMode == Mode.Edit)
            {
                wsIpgAdmin.Admin admin = new Admin.wsIpgAdmin.Admin();
                dsSupportFile = admin.SupportFileGet(supportFileID);
                supportFile = dsSupportFile.SupportFile.FindBySupportFileID(supportFileID);
                txtFile.Text = supportFile.FileName;
				List<int> regionLangIDs = new List<int>();
				foreach (wsIpgAdmin.SupportFileDS.SupportFileRegionLanguageRow dr in dsSupportFile.SupportFileRegionLanguage.Rows)
				{
					regionLangIDs.Add(dr.RegionLanguageID);
				}
				regionLanguageGroupGrid.CheckSlideRegionLanguages(regionLangIDs);
            }
            CurrentSupportFileID = supportFile.SupportFileID;
			regionLanguageGroupGrid.VerticalScroll.Value = regionLanguageGroupGrid.VerticalScroll.Minimum;
        }

        private void btnFile_Click(object sender, EventArgs e)
        {
            DialogResult result = diaOpenFile.ShowDialog();

            if (result == DialogResult.OK)
            {
                if (CurrentMode == Mode.Edit)
                {
                    if (txtFile.Text == System.IO.Path.GetFileName(diaOpenFile.FileName))
                    {
                        txtFile.Text = diaOpenFile.FileName;
                        txtFile.Modified = true;
                    }
                    else
                    {
                        MessageBox.Show("When updating a Supporting File you must upload a file with the same file name.",
                                        "File Name Does Not Match", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }
                }
                else
                {
                    txtFile.Text = diaOpenFile.FileName;
                    txtFile.Modified = true;
                }
                
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            bool valResult = ValidateForm();
            if (valResult)
            {
                this.Cursor = Cursors.WaitCursor;
                Save();
                this.Cursor = Cursors.Default;
            }
        }

        private void Save()
        {
            wsIpgAdmin.Admin admin = new Admin.wsIpgAdmin.Admin();
			
			wsIpgAdmin.SupportFileDS.SupportFileRow supportFile = dsSupportFile.SupportFile.FindBySupportFileID(CurrentSupportFileID);
			supportFile.FileName = System.IO.Path.GetFileName(txtFile.Text);
            supportFile.ModifyDate = DateTime.Now;

			//delete all the existing entries for this ID
			foreach (wsIpgAdmin.SupportFileDS.SupportFileRegionLanguageRow sfrlr in dsSupportFile.SupportFileRegionLanguage.Rows)
			{
				if(sfrlr.SupportFileID == CurrentSupportFileID)
					sfrlr.Delete();
			}

			foreach (int regLangID in regionLanguageGroupGrid.GetSelectedRegionLanguageIDs())
			{
				wsIpgAdmin.SupportFileDS.SupportFileRegionLanguageRow sfrlr = dsSupportFile.SupportFileRegionLanguage.NewSupportFileRegionLanguageRow();
				sfrlr.RegionLanguageID = regLangID;
				sfrlr.SupportFileRow = supportFile;
				dsSupportFile.SupportFileRegionLanguage.AddSupportFileRegionLanguageRow(sfrlr);
			}

			try
            {
				if (txtFile.Modified)
				{
					wcFileUpload.UploadFile(Properties.Settings.Default.SlideUploadURI, txtFile.Text);
					if (Properties.Settings.Default.IsNetStorageUtilized)
					{
						Utilities.FTP.FTPclient ftp = new Utilities.FTP.FTPclient(Properties.Settings.Default.NetStorageFtp, Properties.Settings.Default.NetStorageUserName, Properties.Settings.Default.NetStoragePassword);
						ftp.Upload(txtFile.Text, String.Format("/31505/Ppt/{0}", System.IO.Path.GetFileName(txtFile.Text)));
					}
				}
				admin.SupportFileSave(CurrentSupportFileID, dsSupportFile);
            }
            catch (Exception ex)
            {
                MessageBox.Show("There was an error saving this Support File.", "Save Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            ((SupportFile)this.Owner).RefreshForm();
            this.Close();
        }
		
        private bool ValidateForm()
        {
            bool result = true;

            if (txtFile.Text == "" || txtFile.TextLength == 0)
            {
                MessageBox.Show("Support File is required.", "Required field missing", MessageBoxButtons.OK);
                result = false;
            }
			
			//if support file name has already been taken
			if (CurrentMode == Mode.New)
			{
				wsIpgAdmin.Admin admin = new Admin.wsIpgAdmin.Admin();
				if (admin.SupportFileNameExists(System.IO.Path.GetFileName(txtFile.Text)))
				{
					MessageBox.Show("A support file with this name already exists. If you wish to upload this file, please rename it and try again.", "Save Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
					result = false;
				}
			}

			//if (CurrentMode == Mode.Edit)
			//{
			//    if (txtFile.Modified == false)
			//    {
			//        MessageBox.Show("You have not selected a file to update.", "", MessageBoxButtons.OK);
			//        result = false;
			//    }
			//}
            return result;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void wcFileUpload_UploadProgressChanged(object sender, System.Net.UploadProgressChangedEventArgs e)
        {
            
        }
    }
}