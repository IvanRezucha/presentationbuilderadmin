namespace Admin
{
    partial class CatEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CatEdit));
            this.txtCategory = new System.Windows.Forms.TextBox();
            this.lblCategory = new System.Windows.Forms.Label();
            this.lblCategoryType = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.lblNew = new System.Windows.Forms.Label();
            this.chkNew = new System.Windows.Forms.CheckBox();
            this.lvwCategoryType = new System.Windows.Forms.ListView();
            this.imlIcons16 = new System.Windows.Forms.ImageList(this.components);
            this.grpDefaultSubCategories = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.chkSubCatShowDescendantSlides = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.chkSubCatNew = new System.Windows.Forms.CheckBox();
            this.lvwSubCatIcon = new System.Windows.Forms.ListView();
            this.label2 = new System.Windows.Forms.Label();
            this.tvwDefaultSubCats = new System.Windows.Forms.TreeView();
            this.grpCategoryInfo = new System.Windows.Forms.GroupBox();
            this.lblCategoryShowDescendantSlides = new System.Windows.Forms.Label();
            this.chkCategoryShowDescendantSlides = new System.Windows.Forms.CheckBox();
            this.dsSlideLibrary = new Admin.wsIpgAdmin.SlideLibrary();
            this.grpDefaultSubCategories.SuspendLayout();
            this.grpCategoryInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dsSlideLibrary)).BeginInit();
            this.SuspendLayout();
            // 
            // txtCategory
            // 
            this.txtCategory.Location = new System.Drawing.Point(99, 20);
            this.txtCategory.Name = "txtCategory";
            this.txtCategory.Size = new System.Drawing.Size(361, 20);
            this.txtCategory.TabIndex = 0;
            // 
            // lblCategory
            // 
            this.lblCategory.AutoSize = true;
            this.lblCategory.Location = new System.Drawing.Point(9, 20);
            this.lblCategory.Name = "lblCategory";
            this.lblCategory.Size = new System.Drawing.Size(80, 13);
            this.lblCategory.TabIndex = 2;
            this.lblCategory.Text = "Category Name";
            // 
            // lblCategoryType
            // 
            this.lblCategoryType.AutoSize = true;
            this.lblCategoryType.Location = new System.Drawing.Point(9, 46);
            this.lblCategoryType.Name = "lblCategoryType";
            this.lblCategoryType.Size = new System.Drawing.Size(28, 13);
            this.lblCategoryType.TabIndex = 3;
            this.lblCategoryType.Text = "Icon";
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(323, 656);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(404, 655);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 5;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lblNew
            // 
            this.lblNew.AutoSize = true;
            this.lblNew.Location = new System.Drawing.Point(9, 146);
            this.lblNew.Name = "lblNew";
            this.lblNew.Size = new System.Drawing.Size(35, 13);
            this.lblNew.TabIndex = 7;
            this.lblNew.Text = "New?";
            // 
            // chkNew
            // 
            this.chkNew.AutoSize = true;
            this.chkNew.Location = new System.Drawing.Point(164, 145);
            this.chkNew.Name = "chkNew";
            this.chkNew.Size = new System.Drawing.Size(15, 14);
            this.chkNew.TabIndex = 6;
            this.chkNew.UseVisualStyleBackColor = true;
            // 
            // lvwCategoryType
            // 
            this.lvwCategoryType.HideSelection = false;
            this.lvwCategoryType.Location = new System.Drawing.Point(99, 46);
            this.lvwCategoryType.Name = "lvwCategoryType";
            this.lvwCategoryType.Size = new System.Drawing.Size(151, 94);
            this.lvwCategoryType.SmallImageList = this.imlIcons16;
            this.lvwCategoryType.TabIndex = 9;
            this.lvwCategoryType.UseCompatibleStateImageBehavior = false;
            this.lvwCategoryType.View = System.Windows.Forms.View.SmallIcon;
            // 
            // imlIcons16
            // 
            this.imlIcons16.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imlIcons16.ImageStream")));
            this.imlIcons16.TransparentColor = System.Drawing.Color.Transparent;
            this.imlIcons16.Images.SetKeyName(0, "color-printing.ico");
            this.imlIcons16.Images.SetKeyName(1, "digital-imaging.ico");
            this.imlIcons16.Images.SetKeyName(2, "large-format.ico");
            this.imlIcons16.Images.SetKeyName(3, "MFP-AIO.ico");
            this.imlIcons16.Images.SetKeyName(4, "mono-printing.ico");
            this.imlIcons16.Images.SetKeyName(5, "multi-slides.ico");
            this.imlIcons16.Images.SetKeyName(6, "my-slide-decks.ico");
            this.imlIcons16.Images.SetKeyName(7, "single-slide.ico");
            this.imlIcons16.Images.SetKeyName(8, "Folder.ico");
            this.imlIcons16.Images.SetKeyName(9, "Slide-Folder.ico");
            this.imlIcons16.Images.SetKeyName(10, "Multi-Slide-Folder.ico");
            this.imlIcons16.Images.SetKeyName(11, "my-presentations.ico");
            this.imlIcons16.Images.SetKeyName(12, "slide-library.ico");
            // 
            // grpDefaultSubCategories
            // 
            this.grpDefaultSubCategories.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grpDefaultSubCategories.Controls.Add(this.label1);
            this.grpDefaultSubCategories.Controls.Add(this.chkSubCatShowDescendantSlides);
            this.grpDefaultSubCategories.Controls.Add(this.label3);
            this.grpDefaultSubCategories.Controls.Add(this.chkSubCatNew);
            this.grpDefaultSubCategories.Controls.Add(this.lvwSubCatIcon);
            this.grpDefaultSubCategories.Controls.Add(this.label2);
            this.grpDefaultSubCategories.Controls.Add(this.tvwDefaultSubCats);
            this.grpDefaultSubCategories.Location = new System.Drawing.Point(12, 207);
            this.grpDefaultSubCategories.Name = "grpDefaultSubCategories";
            this.grpDefaultSubCategories.Size = new System.Drawing.Size(467, 441);
            this.grpDefaultSubCategories.TabIndex = 14;
            this.grpDefaultSubCategories.TabStop = false;
            this.grpDefaultSubCategories.Text = "Default Sub-Categories";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 425);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(132, 13);
            this.label1.TabIndex = 17;
            this.label1.Text = "Show Descendant Slides?";
            // 
            // chkSubCatShowDescendantSlides
            // 
            this.chkSubCatShowDescendantSlides.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkSubCatShowDescendantSlides.AutoSize = true;
            this.chkSubCatShowDescendantSlides.Location = new System.Drawing.Point(164, 424);
            this.chkSubCatShowDescendantSlides.Name = "chkSubCatShowDescendantSlides";
            this.chkSubCatShowDescendantSlides.Size = new System.Drawing.Size(15, 14);
            this.chkSubCatShowDescendantSlides.TabIndex = 16;
            this.chkSubCatShowDescendantSlides.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 405);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "New?";
            // 
            // chkSubCatNew
            // 
            this.chkSubCatNew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkSubCatNew.AutoSize = true;
            this.chkSubCatNew.Location = new System.Drawing.Point(164, 404);
            this.chkSubCatNew.Name = "chkSubCatNew";
            this.chkSubCatNew.Size = new System.Drawing.Size(15, 14);
            this.chkSubCatNew.TabIndex = 14;
            this.chkSubCatNew.UseVisualStyleBackColor = true;
            // 
            // lvwSubCatIcon
            // 
            this.lvwSubCatIcon.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lvwSubCatIcon.HideSelection = false;
            this.lvwSubCatIcon.Location = new System.Drawing.Point(99, 300);
            this.lvwSubCatIcon.Name = "lvwSubCatIcon";
            this.lvwSubCatIcon.Size = new System.Drawing.Size(151, 94);
            this.lvwSubCatIcon.SmallImageList = this.imlIcons16;
            this.lvwSubCatIcon.TabIndex = 13;
            this.lvwSubCatIcon.UseCompatibleStateImageBehavior = false;
            this.lvwSubCatIcon.View = System.Windows.Forms.View.SmallIcon;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 300);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(28, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Icon";
            // 
            // tvwDefaultSubCats
            // 
            this.tvwDefaultSubCats.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tvwDefaultSubCats.CheckBoxes = true;
            this.tvwDefaultSubCats.Location = new System.Drawing.Point(6, 19);
            this.tvwDefaultSubCats.Name = "tvwDefaultSubCats";
            this.tvwDefaultSubCats.Size = new System.Drawing.Size(454, 275);
            this.tvwDefaultSubCats.TabIndex = 0;
            // 
            // grpCategoryInfo
            // 
            this.grpCategoryInfo.Controls.Add(this.lblCategoryShowDescendantSlides);
            this.grpCategoryInfo.Controls.Add(this.chkCategoryShowDescendantSlides);
            this.grpCategoryInfo.Controls.Add(this.lvwCategoryType);
            this.grpCategoryInfo.Controls.Add(this.txtCategory);
            this.grpCategoryInfo.Controls.Add(this.lblCategory);
            this.grpCategoryInfo.Controls.Add(this.lblNew);
            this.grpCategoryInfo.Controls.Add(this.lblCategoryType);
            this.grpCategoryInfo.Controls.Add(this.chkNew);
            this.grpCategoryInfo.Location = new System.Drawing.Point(12, 12);
            this.grpCategoryInfo.Name = "grpCategoryInfo";
            this.grpCategoryInfo.Size = new System.Drawing.Size(467, 189);
            this.grpCategoryInfo.TabIndex = 15;
            this.grpCategoryInfo.TabStop = false;
            this.grpCategoryInfo.Text = "Category Information";
            // 
            // lblCategoryShowDescendantSlides
            // 
            this.lblCategoryShowDescendantSlides.AutoSize = true;
            this.lblCategoryShowDescendantSlides.Location = new System.Drawing.Point(9, 166);
            this.lblCategoryShowDescendantSlides.Name = "lblCategoryShowDescendantSlides";
            this.lblCategoryShowDescendantSlides.Size = new System.Drawing.Size(132, 13);
            this.lblCategoryShowDescendantSlides.TabIndex = 11;
            this.lblCategoryShowDescendantSlides.Text = "Show Descendant Slides?";
            // 
            // chkCategoryShowDescendantSlides
            // 
            this.chkCategoryShowDescendantSlides.AutoSize = true;
            this.chkCategoryShowDescendantSlides.Location = new System.Drawing.Point(164, 165);
            this.chkCategoryShowDescendantSlides.Name = "chkCategoryShowDescendantSlides";
            this.chkCategoryShowDescendantSlides.Size = new System.Drawing.Size(15, 14);
            this.chkCategoryShowDescendantSlides.TabIndex = 10;
            this.chkCategoryShowDescendantSlides.UseVisualStyleBackColor = true;
            // 
            // dsSlideLibrary
            // 
            this.dsSlideLibrary.DataSetName = "SlideLibrary";
            this.dsSlideLibrary.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // CatEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(491, 690);
            this.Controls.Add(this.grpCategoryInfo);
            this.Controls.Add(this.grpDefaultSubCategories);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSave);
            this.Name = "CatEdit";
            this.Text = "Category";
            this.Load += new System.EventHandler(this.CatEdit_Load);
            this.grpDefaultSubCategories.ResumeLayout(false);
            this.grpDefaultSubCategories.PerformLayout();
            this.grpCategoryInfo.ResumeLayout(false);
            this.grpCategoryInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dsSlideLibrary)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtCategory;
        private System.Windows.Forms.Label lblCategory;
        private System.Windows.Forms.Label lblCategoryType;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label lblNew;
        private System.Windows.Forms.CheckBox chkNew;
        private Admin.wsIpgAdmin.SlideLibrary dsSlideLibrary;
        private System.Windows.Forms.ListView lvwCategoryType;
		private System.Windows.Forms.ImageList imlIcons16;
        private System.Windows.Forms.GroupBox grpDefaultSubCategories;
		private System.Windows.Forms.GroupBox grpCategoryInfo;
        private System.Windows.Forms.TreeView tvwDefaultSubCats;
        private System.Windows.Forms.ListView lvwSubCatIcon;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chkSubCatShowDescendantSlides;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox chkSubCatNew;
        private System.Windows.Forms.Label lblCategoryShowDescendantSlides;
        private System.Windows.Forms.CheckBox chkCategoryShowDescendantSlides;
    }
}