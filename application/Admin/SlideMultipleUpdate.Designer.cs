namespace Admin
{
	partial class SlideMultipleUpdate
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.grpSlideMulipleUpdate = new System.Windows.Forms.GroupBox();
            this.slideFiltersAndLanguages = new Admin.UserControls.SlideFiltersAndLanguages();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.status = new System.Windows.Forms.ToolStripStatusLabel();
            this.errFormValid = new System.Windows.Forms.ErrorProvider(this.components);
            this.grpSlideMulipleUpdate.SuspendLayout();
            this.statusStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errFormValid)).BeginInit();
            this.SuspendLayout();
            // 
            // grpSlideMulipleUpdate
            // 
            this.grpSlideMulipleUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grpSlideMulipleUpdate.Controls.Add(this.slideFiltersAndLanguages);
            this.grpSlideMulipleUpdate.Location = new System.Drawing.Point(12, 12);
            this.grpSlideMulipleUpdate.Name = "grpSlideMulipleUpdate";
            this.grpSlideMulipleUpdate.Size = new System.Drawing.Size(336, 402);
            this.grpSlideMulipleUpdate.TabIndex = 0;
            this.grpSlideMulipleUpdate.TabStop = false;
            this.grpSlideMulipleUpdate.Text = "Update Slide Region/Languages and Filters";
            // 
            // slideFiltersAndLanguages
            // 
            this.slideFiltersAndLanguages.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.slideFiltersAndLanguages.Location = new System.Drawing.Point(6, 19);
            this.slideFiltersAndLanguages.Name = "slideFiltersAndLanguages";
            this.slideFiltersAndLanguages.Size = new System.Drawing.Size(309, 377);
            this.slideFiltersAndLanguages.TabIndex = 0;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(275, 424);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpdate.Location = new System.Drawing.Point(194, 424);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(75, 23);
            this.btnUpdate.TabIndex = 2;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.Update_Click);
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.status});
            this.statusStrip.Location = new System.Drawing.Point(0, 463);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(363, 22);
            this.statusStrip.TabIndex = 3;
            this.statusStrip.Text = "statusStrip1";
            // 
            // status
            // 
            this.status.Name = "status";
            this.status.Size = new System.Drawing.Size(0, 17);
            // 
            // errFormValid
            // 
            this.errFormValid.ContainerControl = this;
            // 
            // SlideMultipleUpdate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(363, 485);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.grpSlideMulipleUpdate);
            this.Name = "SlideMultipleUpdate";
            this.Text = "Update Slide Filters and/or Regions and Languages";
            this.grpSlideMulipleUpdate.ResumeLayout(false);
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errFormValid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.GroupBox grpSlideMulipleUpdate;
		private Admin.UserControls.SlideFiltersAndLanguages slideFiltersAndLanguages;
		private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnUpdate;
		private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel status;
		private System.Windows.Forms.ErrorProvider errFormValid;

	}
}