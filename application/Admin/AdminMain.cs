using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using Admin.BLL;

namespace Admin
{
    public partial class AdminMain : MasterForm
    {
        //this is a control that we create at run-time
        //it is declared here becauase we need to be able to hook into its events
        TabControl tabRegionLanguageTrees;
        SlideDeleteContainer slideDeleteContainer;
        SlideRemoveContainer slideRemoveContainer;
        List<wsIpgAdmin.SlideLibrary.SlideRow> slidesToDelete = new List<wsIpgAdmin.SlideLibrary.SlideRow>();
        List<SlideRemoveTransport> slidesToRemoveFromCategory = new List<SlideRemoveTransport>();

        #region properties

        private string _AppTempDirectory;
        public string AppTempDirectory
        {
            get { return _AppTempDirectory; }
            set { _AppTempDirectory = value; }
        }

        private string _XmlDirectory;
        public string XmlDirectory
        {
            get { return _XmlDirectory; }
            set { _XmlDirectory = value; }
        }

        public enum TreeViewDisplay
        {
            Main,
            RegionLanguages,
            Both
        }
        private TreeViewDisplay _CurrentTreeViewDisplay;
        public TreeViewDisplay CurrentTreeViewDisplay
        {
            get { return _CurrentTreeViewDisplay; }
            set { _CurrentTreeViewDisplay = value; }
        }

        private bool _LastFilterToggle = false;
        public bool LastFilterToggle
        {
            get { return _LastFilterToggle; }
            set { _LastFilterToggle = value; }
        }

        private int _LastSelectedRegionLangTab;
        private int LastSelectedRegionLangTab
        {
            get { return _LastSelectedRegionLangTab; }
            set { _LastSelectedRegionLangTab = value; }
        }

        private List<int> _SelectedRegionLanguageFilterByIDs;
        /// <summary>
        /// Gets or sets the selected region language filters.
        /// </summary>
        /// <value>The selected region language filters.</value>
        public List<int> SelectedRegionLanguageFilterByIDs
        {
            get { return _SelectedRegionLanguageFilterByIDs; }
            set { _SelectedRegionLanguageFilterByIDs = value; }
        }

        private bool _IsDataFiltered;
        /// <summary>
        /// Gets or sets a value indicating whether this instance's is data filtered.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is data filtered; otherwise, <c>false</c>.
        /// </value>
        public bool IsDataFiltered
        {
            get { return _IsDataFiltered; }
            set { _IsDataFiltered = value; }
        }

        private TreeNode _ReselectNode;
        /// <summary>
        /// Gets or sets the reselect tree node.
        /// </summary>
        /// <value>The reselect tree node.</value>
        public TreeNode ReselectNode
        {
            get { return _ReselectNode; }
            set { _ReselectNode = value; }
        }

        private TreeNode _RightClickNode;
        /// <summary>
        /// Gets or sets the right click node.
        /// </summary>
        /// <value>The right click node.</value>
        public TreeNode RightClickNode
        {
            get { return _RightClickNode; }
            set { _RightClickNode = value; }
        }

        public object _CurrentDraggedItem;
        /// <summary>
        /// Gets or sets the current dragged item.
        /// </summary>
        /// <value>The current dragged item.</value>
        public object CurrentDraggedItem
        {
            get { return _CurrentDraggedItem; }
            set { _CurrentDraggedItem = value; }
        }

        private bool _LastRegionLanguageToggle;
        public bool LastRegionLanguageToggle
        {
            get { return _LastRegionLanguageToggle; }
            set { _LastRegionLanguageToggle = value; }
        }

        private int _SlideSortColumnIndex;
        public int SlideSortColumnIndex
        {
            get { return _SlideSortColumnIndex; }
            set { _SlideSortColumnIndex = value; }
        }

        private SortOrder _SlideSortOrder = 0;
        public SortOrder SlideSortOrder
        {
            get { return _SlideSortOrder; }
            set { _SlideSortOrder = value; }
        }

        private TreeNode _NodeToDelete;
        public TreeNode NodeToDelete
        {
            get { return _NodeToDelete; }
            set { _NodeToDelete = value; }
        }

        private bool _DoSlideListSelectionChange;
        public bool DoSlideListSelectionChange
        {
            get { return _DoSlideListSelectionChange; }
            set { _DoSlideListSelectionChange = value; }
        }

        private int _ReselectSlideID = 0;
        /// <summary>
        /// Gets or sets the reselect slide ID.
        /// </summary>
        /// <value>The reselect slide ID.</value>
        public int ReselectSlideID
        {
            get { return _ReselectSlideID; }
            set { _ReselectSlideID = value; }
        }

        private static string _adminTicket;
        /// <summary>
        /// Gets or sets the admin ticket. Internal reference to the ticket stored on the server to verify logins.
        /// </summary>
        /// <value>The admin ticket.</value>
        public static string AdminTicket
        {
            get { return _adminTicket; }
            set { _adminTicket = value; }
        }

        private SlideEdit _SlideEditForm;
        private SlideEdit SlideEditForm
        {
            get { return _SlideEditForm; }
            set { _SlideEditForm = value; }
        }

        #endregion

        private void SetupSlideDeleteContainer()
        {
            slideDeleteContainer = new SlideDeleteContainer();
            slideDeleteContainer.BackColor = System.Drawing.SystemColors.InactiveBorder;
            slideDeleteContainer.Dock = System.Windows.Forms.DockStyle.Bottom;
            slideDeleteContainer.Location = new System.Drawing.Point(0, 233);
            slideDeleteContainer.Size = new System.Drawing.Size(622, 221);
            slideDeleteContainer.TabIndex = 1;
            slideDeleteContainer.Hide();
            this.splInfo.Panel1.Controls.Add(slideDeleteContainer);
            slideDeleteContainer.DeleteSlides += new SlideDeleteContainerDeleteSlidesEventHandler(slideDeleteContainer_DeleteSlides);
            slideDeleteContainer.ClearSlides += new SlideDeleteContainerClearSlidesEventHandler(slideDeleteContainer_ClearSlides);
            slideDeleteContainer.RemoveSlides += new SlideDeleteContainerRemoveSlideEventHandler(slideDeleteContainer_RemoveSlides);
            slideDeleteContainer.CloseContainer += new SlideDeleteContainerCloseContainerEventHandler(slideDeleteContainer_CloseContainer);
        }

        private void SetupSlideRemoveContainer()
        {
            slideRemoveContainer = new SlideRemoveContainer();
            slideRemoveContainer.BackColor = System.Drawing.SystemColors.InactiveBorder;
            slideRemoveContainer.Dock = System.Windows.Forms.DockStyle.Bottom;
            slideRemoveContainer.Location = new System.Drawing.Point(0, 233);
            slideRemoveContainer.Size = new System.Drawing.Size(622, 221);
            slideRemoveContainer.TabIndex = 1;
            slideRemoveContainer.Hide();
            this.splInfo.Panel1.Controls.Add(slideRemoveContainer);
            slideRemoveContainer.ClearSlides += new SlideRemoveContainerClearSlidesEventHandler(slideRemoveContainer_ClearSlides);
            slideRemoveContainer.RemoveFromCategory += new SlideRemoveContainerRemoveFromCategoryEventHandler(slideRemoveContainer_RemoveFromCategory);
            slideRemoveContainer.RemoveFromList += new SlideRemoveContainerRemoveFromListEventHandler(slideRemoveContainer_RemoveFromList);
            slideRemoveContainer.CloseContainer += new SlideRemoveContainerCloseContainerEventHandler(slideRemoveContainer_CloseContainer);
        }

        public AdminMain(string adminTicket)
        {
            InitializeComponent();
            AdminTicket = adminTicket;
            SetupAppDirectories();
            IsDataFiltered = false;
            SetupSlideDeleteContainer();
            SetupSlideRemoveContainer();
            //dsSlideLibrary.Category.WriteXml(@"C:\xmlDataSet.xml", XmlWriteMode.WriteSchema);

            if (!Properties.Settings.Default.IsVistaRegistryApplied)
            {
                //AppSetup setup = new AppSetup();
                //setup.RegistrySetup();
            }

            if (!Properties.Settings.Default.RegistryHasBeenConfigured)
            {
                AppSetup appSetup = new AppSetup();
                try
                {
                    appSetup.RegistrySetup();
                    Properties.Settings.Default.RegistryHasBeenConfigured = true;
                    Properties.Settings.Default.Save();
                }
                catch (Exception ex1)
                {
                    Process process = new Process();
                    try
                    {
                        process.Exited += new EventHandler(process_Exited);
                        process.EnableRaisingEvents = true;
                        string directoryPath = Path.Combine(Application.StartupPath, @"RegistrySetup");
                        process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                        process.StartInfo.FileName = Path.Combine(directoryPath, "PPTRegistryUpdatev2.exe");
                        process.Start();
                    }
                    catch (Exception ex2)
                    {
                        try { process.Kill(); }
                        catch (Exception) { }
                        MessageBox.Show("Powerpoint settings were unable to be configured due to security settings of this machine." + Environment.NewLine + "Please contact support with questions if this program is not displaying powerpoints correctly.", "Powerpoint Settings");
                    }
                }
            }
        }

        private void process_Exited(object sender, EventArgs e)
        {
            Process process = sender as Process;
            int exitcode = process.ExitCode;
            if (exitcode != 0)
            {
                MessageBox.Show("Powerpoint settings were unable to be configured due to security settings of this machine." + Environment.NewLine + "Please contact support with questions if this program is not displaying powerpoints correctly.", "Powerpoint Settings");
            }
            else
            {
                Properties.Settings.Default.RegistryHasBeenConfigured = true;
                Properties.Settings.Default.Save();
            }
        }
        #region Initialization

        private void SetupAppDirectories()
        {
            //initializes application data directories, creates them if not already existing
            _AppTempDirectory = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                            Properties.Settings.Default.AppDataDirectory.ToString() + @"\Temp");
            if (!Directory.Exists(_AppTempDirectory))
            {
                Directory.CreateDirectory(_AppTempDirectory);
            }
            XmlDirectory = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                        Properties.Settings.Default.AppDataDirectory.ToString() + @"\Resources\Xml");
            if (!Directory.Exists(XmlDirectory))
            {
                Directory.CreateDirectory(XmlDirectory);
            }
        }

        private void CustomFeatureSetup()
        {
            // Setup custom features
            if (Properties.Settings.Default.IsFilterManagementEnabled && Properties.Settings.Default.IsRegionManagmentEnabled)
            {
                miFilterToolStrip.Visible = true;
                miMainSlideUpdateSlideFilterOrRegionLanguages.Visible = true;
                miMainSlideUpdateSlideFilter.Visible = true;
                miMainSlideUpdateSlideRegionLanguages.Visible = true;
                miSlideResetFiltersAndOrRegionLanguages.Visible = true;
                miSlideResetFilters.Visible = true;
                miSlideResetRegionLanguages.Visible = true;

                bindingNavigatorSlideList.Visible = true;
                navigationPanePageFilters.Visible = true;
                navigationPanePageRegionLanguages.Visible = true;
                FillFilters();
                filterTabs.CheckAllFilters();
                FillRegionLanguagesGroupGridAndSlideListComboBox();
                //miMainUpdatePublishContentUpdate.Visible = false;

                if (!File.Exists(Path.Combine(XmlDirectory, "MyRegionLanguages.xml")))
                {
                    navigationPaneLeftPane.SelectNavigationPage(navigationPanePageRegionLanguages);
                    navigationPanePageRegionLanguages.Focus();
                    return;
                }
                else
                {
                    DataSets.MyRegionLanguages dsMyRegLangs = new Admin.DataSets.MyRegionLanguages();
                    dsMyRegLangs.ReadXml(Path.Combine(XmlDirectory, "MyRegionLanguages.xml"));
                    List<int> myRegLangIds = new List<int>();
                    foreach (DataSets.MyRegionLanguages.MyRegionLanguagesRow myRegLang in dsMyRegLangs._MyRegionLanguages.Rows)
                    {
                        myRegLangIds.Add(myRegLang.RegionLanguageID);
                    }
                    regionLanguageGroupGrid.CheckSlideRegionLanguages(myRegLangIds);
                }
            }
            else
            {
                toolStripComboBoxRegionLanguage.Visible = false;
                navigationPaneLeftPane.NavigationPages.Remove(navigationPanePageFilters);
                navigationPaneLeftPane.NavigationPages.Remove(navigationPanePageRegionLanguages);
                miMainUpdatesPublishLocalizedContentUpdate.Visible = false;
            }

            if (!Properties.Settings.Default.IsRegionManagmentEnabled)
            {
                wsIpgAdmin.SlideLibrary.RegionLanguageRow rlRow = (wsIpgAdmin.SlideLibrary.RegionLanguageRow)dsSlideLibrary.RegionLanguage.Rows[0];
                AppSettings.NonLocalized.Default.DefaultRegionLanguageID = rlRow.RegionLanguageID;
            }
        }

        public void GetSlideLibraryData()
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            int i = 0;

            DataSets.MyRegionLanguages dsMyRegLangs = GetMyRegionLanguages();

            foreach (DataSets.MyRegionLanguages.MyRegionLanguagesRow myRegLang in dsMyRegLangs._MyRegionLanguages.Rows)
            {
                sb.Append(myRegLang.RegionLanguageID);
                if (i < dsMyRegLangs._MyRegionLanguages.Rows.Count)
                {
                    sb.Append(",");
                }
                i++;
            }

            wsIpgAdmin.Admin admin = new Admin.wsIpgAdmin.Admin();
            admin.Shizmo();
            dsSlideLibrary = admin.GetSlideLibraryDataByRegionLanguage(sb.ToString());
        }

        public DataSets.MyRegionLanguages GetMyRegionLanguages()
        {
            DataSets.MyRegionLanguages dsMyRegionLanguages = new Admin.DataSets.MyRegionLanguages();

            if (!File.Exists(Path.Combine(XmlDirectory, "MyRegionLanguages.xml")))
            {
                navigationPaneLeftPane.SelectNavigationPage(navigationPanePageRegionLanguages);
                //MessageBox.Show("Please select at least one region/language.");
                return dsMyRegionLanguages;
            }

            dsMyRegionLanguages.ReadXml(Path.Combine(XmlDirectory, "MyRegionLanguages.xml"));

            return dsMyRegionLanguages;

        }

        /// <summary>
        /// Repopulates the slide list and sets the sort order
        /// </summary>
        private void PopulateSlideList()
        {
            //dgvSlides.Rows.Clear();
            DoSlideListSelectionChange = false;
            if (toolStripComboBoxRegionLanguage.SelectedIndex > 0)
            {
                System.Data.DataRowCollection drc = (System.Data.DataRowCollection)toolStripComboBoxRegionLanguage.Tag;
                int regionLanguageID = ((wsIpgAdmin.SlideLibrary.RegionLanguageRow)drc[toolStripComboBoxRegionLanguage.SelectedIndex - 1]).RegionLanguageID;
                wsIpgAdmin.Admin admin = new Admin.wsIpgAdmin.Admin();
                wsIpgAdmin.SlideLibrary slideLibraryFilteredByRegionLanguage = admin.SlidesGetByRegionLanguageID(regionLanguageID);
                bindSlideGrid.DataSource = slideLibraryFilteredByRegionLanguage.Slide;
            }
            else
            {
                bindSlideGrid.DataSource = dsSlideLibrary.Slide;
            }


            bindSlideGrid.Sort = "Title ASC";
            DoSlideListSelectionChange = true;

            // If a reselectSlideID has not been established then set it.
            if (ReselectSlideID == 0 && dgvSlides.Rows.Count > 0)
            {
                ReselectSlideID = (int)dgvSlides.Rows[0].Cells[0].Value;
            }

            if (SlideSortColumnIndex != 0)
            {
                DataGridViewColumn sortColumn = dgvSlides.Columns[SlideSortColumnIndex];
                ListSortDirection sortDirection;
                if (SlideSortOrder == SortOrder.Ascending || SlideSortOrder == SortOrder.None)
                {
                    sortDirection = ListSortDirection.Ascending;
                }
                else// if (slideSortOrder == SortOrder.Descending)
                {
                    sortDirection = ListSortDirection.Descending;
                }
                dgvSlides.Sort(sortColumn, sortDirection);
            }
        }

        #endregion

        #region Form Events

        private void AdminMain_Load(object sender, EventArgs e)
        {
            GetSlideLibraryData();
            PopulateSlideList();
            PopulateRegionLanguageTrees();
            CustomFeatureSetup(); // Adds custom features based on version
            //PopulateSlideLibrary();
            this.WindowState = FormWindowState.Maximized;

            if (!File.Exists(Path.Combine(XmlDirectory, "MyRegionLanguages.xml")))
            {
                navigationPaneLeftPane.SelectNavigationPage(navigationPanePageRegionLanguages);
                MessageBox.Show("Please select at least one region/language.");
            }

            if (dgvSlides.Rows.Count > 0)
            {
                LoadSlideToBrowser(dgvSlides.Rows[0].Cells[2].Value.ToString());
            }

        }

        private void AdminMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            //In the new architecture, we don't have a login form hiding the whole time -- this form is it, so
            //we need to call the exit method when it is closed.
            Application.Exit();
        }

        #endregion

        #region TreeView Methods

        public void PopulateRegionLanguageTrees()
        {
            //In the event that a region language combination was removed from production, we will remove it from my region languages.
            List<int> RemoveRegionLanguageList = new List<int>();

            //get the slides, slidecats, SlideRegionLanguages, and SlideFilters related to this reglangID
            wsIpgAdmin.Admin ws = new Admin.wsIpgAdmin.Admin();
            wsIpgAdmin.SlideLibrary dsRegLangSlides = new Admin.wsIpgAdmin.SlideLibrary();
            DataSets.MyRegionLanguages dsMyRegLangs = new Admin.DataSets.MyRegionLanguages();

            navigationPanePageSlideLibrary.Controls.Clear();
            if (Properties.Settings.Default.IsRegionManagmentEnabled)
            {
                if (!File.Exists(Path.Combine(XmlDirectory, "MyRegionLanguages.xml")))
                {
                    navigationPaneLeftPane.SelectNavigationPage(navigationPanePageRegionLanguages);
                    return;
                }
                dsMyRegLangs.ReadXml(Path.Combine(XmlDirectory, "MyRegionLanguages.xml"));

                tabRegionLanguageTrees = new TabControl();
                navigationPanePageSlideLibrary.Controls.Add(tabRegionLanguageTrees);
                tabRegionLanguageTrees.Dock = DockStyle.Fill;
                tabRegionLanguageTrees.SelectedIndexChanged += new EventHandler(tabRegionLanguageTrees_SelectedIndexChanged);
                tabRegionLanguageTrees.TabPages.Clear();

                foreach (DataSets.MyRegionLanguages.MyRegionLanguagesRow myRegLangRow in dsMyRegLangs._MyRegionLanguages.Rows)
                {
                    System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
                    sw.Start();
                    //convert the string into an int
                    int regionLanguageID = myRegLangRow.RegionLanguageID;
                    wsIpgAdmin.SlideLibrary.RegionLanguageRow rlRow = dsSlideLibrary.RegionLanguage.FindByRegionLanguageID(regionLanguageID);
                    if (rlRow != null)
                    {
                        //make the tab page
                        TabPage regLangTabPage = new TabPage();
                        regLangTabPage.Text = String.Format("{0}/{1}", rlRow.RegionRow.ShortName, rlRow.LanguageRow.LongName);
                        regLangTabPage.Tag = rlRow;
                        tabRegionLanguageTrees.TabPages.Add(regLangTabPage);

                        dsRegLangSlides.Clear();
                        dsRegLangSlides.EnforceConstraints = false;

                        //put all the data into a new dataset
                        dsRegLangSlides.Merge(dsSlideLibrary);
                        //clear out the slide cats
                        dsRegLangSlides.SlideCategory.Clear();
                        //now get a view of the SlideCategory table that has only this RegLang in it
                        dsSlideLibrary.SlideCategory.DefaultView.RowFilter = String.Format("RegionLanguageID={0}", regionLanguageID.ToString());
                        //now merge it
                        dsRegLangSlides.SlideCategory.Merge(dsSlideLibrary.SlideCategory.DefaultView.ToTable());
                        System.Diagnostics.Debug.WriteLine(String.Format("End merge data: {0}", sw.Elapsed.TotalSeconds.ToString()));

                        //make a catSlide tree, fill it, and put it on the tab
                        CategorySlideTreeView catSlideTree = new CategorySlideTreeView();
                        catSlideTree.TreeViewEntryNodeDeleteClick += new TreeViewEntryNodeDeleteEventHandler(catSlideTree_TreeViewEntryNodeDeleteClick);
                        catSlideTree.TreeViewEntryNodeRemoveClick += new TreeViewEntryNodeRemoveEventHandler(catSlideTree_TreeViewEntryNodeRemoveClick);
                        catSlideTree.ImageList = imlIcons16;
                        catSlideTree.AllowDrop = true;
                        catSlideTree.Tag = rlRow;
                        catSlideTree.CategoryContextMenu = cmnCategory;
                        catSlideTree.SlideContextMenu = cmnCategory;
                        catSlideTree.RightClickSelectsNode = true;

                        regLangTabPage.Controls.Add(catSlideTree);

                        catSlideTree.FillCategorySlideData(dsRegLangSlides);
                        System.Diagnostics.Debug.WriteLine(String.Format("End fill tree: {0}", sw.Elapsed.TotalSeconds.ToString()));
                        catSlideTree.Dock = DockStyle.Fill;
                    }
                    else
                    {
                        RemoveRegionLanguageList.Add(regionLanguageID);
                    }
                }

                // Reselect the last select tab page
                if (tabRegionLanguageTrees.TabPages.Count > LastSelectedRegionLangTab)
                {
                    tabRegionLanguageTrees.SelectTab(LastSelectedRegionLangTab);
                }
            }
            else
            {
                dsMyRegLangs._MyRegionLanguages.AddMyRegionLanguagesRow(dsSlideLibrary.RegionLanguage[0].RegionLanguageID);

                //make a catSlide tree, fill it, and put it on the tab
                CategorySlideTreeView catSlideTree = new CategorySlideTreeView();
                catSlideTree.ImageList = imlIcons16;
                catSlideTree.AllowDrop = true;
                catSlideTree.Tag = dsSlideLibrary.RegionLanguage.Rows[0];
                catSlideTree.CategoryContextMenu = cmnCategory;
                catSlideTree.SlideContextMenu = cmnCategory;
                catSlideTree.RightClickSelectsNode = true;

                navigationPanePageSlideLibrary.Controls.Add(catSlideTree);
                catSlideTree.FillCategorySlideData(dsSlideLibrary);
                catSlideTree.Dock = DockStyle.Fill;
            }

            navigationPaneLeftPane.SelectNavigationPage(navigationPanePageSlideLibrary);

            //clear the filter
            dsSlideLibrary.SlideCategory.DefaultView.RowFilter = "";

            if (RemoveRegionLanguageList.Count > 0)
            {
                foreach (int i in RemoveRegionLanguageList)
                {
                    Admin.DataSets.MyRegionLanguages.MyRegionLanguagesRow row = dsMyRegLangs._MyRegionLanguages.FindByRegionLanguageID(i);
                    row.Delete();
                }

                dsMyRegLangs.WriteXml(Path.Combine(XmlDirectory, "MyRegionLanguages.xml"));
            }
        }

        #region tvwSlideLibrary UTILITIES

        /// <summary>
        /// Deletes all slides based on the slideID and parent node
        /// </summary>
        /// <param name="parentNode">Parent Node</param>
        /// <param name="slideID">Slide ID</param>
        private void DeleteAllNodesBySlideID(TreeNode parentNode, int slideID)
        {
            foreach (TreeNode n in parentNode.Nodes)
            {
                if (n.Tag.GetType() == typeof(wsIpgAdmin.SlideLibrary.SlideCategoryRow) && ((wsIpgAdmin.SlideLibrary.SlideCategoryRow)n.Tag).SlideID == slideID)
                {
                    n.Remove();
                }
                else
                {
                    DeleteAllNodesBySlideID(n, slideID);
                }
            }
        }

        private void FindAllNodesBySlideID(TreeNode parentNode, object row)
        {
            foreach (TreeNode n in parentNode.Nodes)
            {
                // This if condition used to read "n.Tag.GetType() == typeof(wsIpgAdmin.SlideLibrary.SlideCategoryRow) && n.Tag == row", but with the
                // implementation of the new treeview n.Tag is never == row.
                if (n.Tag.GetType() == typeof(wsIpgAdmin.SlideLibrary.SlideCategoryRow) &&
                    ((wsIpgAdmin.SlideLibrary.SlideCategoryRow)n.Tag).SlideID == ((wsIpgAdmin.SlideLibrary.SlideCategoryRow)row).SlideID &&
                    ((wsIpgAdmin.SlideLibrary.SlideCategoryRow)n.Tag).CategoryID == ((wsIpgAdmin.SlideLibrary.SlideCategoryRow)row).CategoryID)
                {
                    NodeToDelete = n;
                }
                else
                {
                    FindAllNodesBySlideID(n, row);
                }
            }
        }

        /// <summary>
        /// Takes a tree node and returns its key as an int
        /// </summary>
        /// <param name="node">tree node to get category id out of</param>
        /// <returns>id if node is category node, -1 otherwise</returns>
        private int NodeToCategoryID(TreeNode node)
        {
            if (IsCategoryNode(node))
                return ((wsIpgAdmin.SlideLibrary.CategoryRow)node.Tag).CategoryID;
            else
                return -1;
        }

        /// <summary>
        /// Takes a tree node and returns the category name as a string
        /// </summary>
        /// <param name="node">Tree node to get category out of</param>
        /// <returns>Category if node is category node, zero-length string otherwise</returns>
        private string NodeToCategory(TreeNode node)
        {
            if (IsCategoryNode(node))
                return ((wsIpgAdmin.SlideLibrary.CategoryRow)node.Tag).Category;
            else if (this.IsSlideCategoryNode(node))
                return ((wsIpgAdmin.SlideLibrary.SlideCategoryRow)node.Tag).CategoryRow.Category;
            else
                return "";
        }

        /// <summary>
        /// returns true if node is a category node (i.e. tag is a CategoryRow)
        /// </summary>
        /// <param name="node">node to check</param>
        /// <returns>true if cat, false if not</returns>
        private bool IsCategoryNode(TreeNode node)
        {
            return node.Tag.GetType() == typeof(wsIpgAdmin.SlideLibrary.CategoryRow);
        }

        /// <summary>
        /// returns true if node is a product category (i.e. tag is a ProductCategoryRow)
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        private bool IsSlideCategoryNode(TreeNode node)
        {
            return node.Tag.GetType() == typeof(wsIpgAdmin.SlideLibrary.SlideCategoryRow);
        }

        /// <summary>
        /// Determines whether or not a node has any slide children
        /// NOTE: Only checks children, not all decendants
        /// </summary>
        /// <param name="node">node we are checking</param>
        /// <returns>true/false</returns>
        private bool HasSlideChildren(TreeNode node)
        {
            foreach (TreeNode n in node.Nodes)
            {
                if (n.Tag.GetType() == typeof(wsIpgAdmin.SlideLibrary.SlideCategoryRow))
                {
                    //found a slide
                    return true;
                }
            }
            //no slides in this cat
            return false;
        }

        /// <summary>
        /// Determine the type of node (Category Type or SlideCategory)
        /// </summary>
        /// <param name="node">node we are checking</param>
        /// <returns>string representation of node type</returns>
        public string GetNodeType(TreeNode node)
        {
            string nodeType = "";

            if (node.Tag.GetType() == typeof(wsIpgAdmin.SlideLibrary.CategoryRow))
            {
                //int categoryTypeID = ((wsIpgAdmin.SlideLibrary.CategoryRow)node.Tag).CategoryTypeID;
                //wsIpgAdmin.SlideLibrary.CategoryTypeRow categoryType = dsSlideLibrary.CategoryType.FindByCategoryTypeID(categoryTypeID);
                //if (categoryType != null)
                //{
                //    if (!categoryType.IsCategoryTypeNull())
                //    {
                //        nodeType = categoryType.CategoryType;
                //    }
                //}
            }
            else if (node.Tag.GetType() == typeof(wsIpgAdmin.SlideLibrary.SlideCategoryRow))
            {
                nodeType = "Slide Category";
            }

            return nodeType;
        }

        #endregion

        #endregion

        #region Form Refresh

        /// <summary>
        /// Refreshes the data.
        /// </summary>
        public void RefreshData()
        {
            this.Cursor = Cursors.WaitCursor;
            GetSlideLibraryData();

            PopulateSlideList();
            this.Cursor = Cursors.Default;
            PopulateRegionLanguageTrees();

            FillRegionLanguagesGroupGridAndSlideListComboBox();
        }

        /// <summary>
        /// Refreshes the filtered data.
        /// </summary>
        public void RefreshFilteredData()
        {
            wsIpgAdmin.SlideLibrary tempDS = new Admin.wsIpgAdmin.SlideLibrary();
            wsIpgAdmin.Admin adminWS = new Admin.wsIpgAdmin.Admin();
            StringBuilder selectedFilterIds = new StringBuilder();

            //we're not getting the lookup tables, so let's turn this off
            tempDS.EnforceConstraints = false;

            // Get a comma delimited list
            foreach (int filterId in filterTabs.GetSelectedFilterIDs())
            {
                selectedFilterIds.Append(filterId.ToString() + ',');
            }

            //go get the filtered Slide, SlideCategory, SlideRegionLanguage, and SlideFilter data
            foreach (int regLangID in SelectedRegionLanguageFilterByIDs)
            {
                tempDS.Merge(adminWS.SlidesGetByRegionLanguageIDAndFilterIDs(regLangID, selectedFilterIds.ToString()));
                tempDS.AcceptChanges();
            }

            //can't clear without this
            dsSlideLibrary.EnforceConstraints = false;

            //clear these tables
            dsSlideLibrary.Slide.Clear();
            dsSlideLibrary.SlideCategory.Clear();
            dsSlideLibrary.SlideFilter.Clear();
            dsSlideLibrary.SlideRegionLanguage.Clear();

            //now fill'em up with data according to which RegionLanguages the user selected
            dsSlideLibrary.Slide.Merge(tempDS.Slide);
            dsSlideLibrary.SlideCategory.Merge(tempDS.SlideCategory);
            dsSlideLibrary.SlideFilter.Merge(tempDS.SlideFilter);
            dsSlideLibrary.SlideRegionLanguage.Merge(tempDS.SlideRegionLanguage);

            //should be good now
            //dsSlideLibrary.EnforceConstraints = true;

            //populate the controls, filtered of course
            //PopulateSlideLibrary();
            PopulateSlideList();
            PopulateRegionLanguageTrees();
        }


        /// <summary>
        /// Refreshes the slide list by slide ID.
        /// </summary>
        /// <param name="slideID">The slide ID.</param>
        public void RefreshSlideListBySlideID(int slideID)
        {
            this.Cursor = Cursors.WaitCursor;

            PopulateSlideList();

            // Deselect all slides. This prevents having multiple slides selected after a refresh which is the desired behavior.  
            DeselectDataGridViewItems();

            // Bind to the position
            ReselectSlideID = slideID;
            bindSlideGrid.Position = bindSlideGrid.Find("SlideID", ReselectSlideID);
            bindSlideGrid.ResetItem(bindSlideGrid.Position);
            dgvSlides.Rows[bindSlideGrid.Position].Selected = true;
            dgvSlides.FirstDisplayedScrollingRowIndex = dgvSlides.Rows[bindSlideGrid.Position].Index;

            this.Cursor = Cursors.Default;
        }

        /// <summary>
        /// Refreshes the slide list by position.
        /// </summary>
        /// <param name="slidePosition">The slide position.</param>
        public void RefreshSlideListByPosition(int slidePosition)
        {
            this.Cursor = Cursors.WaitCursor;

            //GetSlideLibraryData();
            PopulateSlideList();

            // Deselect all slides. This prevents having multiple slides selected after a refresh which is the desired behavior.  
            DeselectDataGridViewItems();

            // Bind to the position
            ReselectSlideID = (int)dgvSlides.Rows[slidePosition].Cells[0].Value;
            bindSlideGrid.Position = slidePosition;
            bindSlideGrid.ResetItem(bindSlideGrid.Position);
            dgvSlides.Rows[bindSlideGrid.Position].Selected = true;
            dgvSlides.FirstDisplayedScrollingRowIndex = dgvSlides.Rows[bindSlideGrid.Position].Index;

            this.Cursor = Cursors.Default;
        }

        /// <summary>
        /// Determines the current column and sort order for the slide datagridview
        /// </summary>
        public void GetSlideResortItems()
        {
            SlideSortColumnIndex = dgvSlides.SortedColumn.Index;
            SlideSortOrder = dgvSlides.SortOrder;
        }

        #endregion

        #region dgvSlides

        /// <summary>
        /// determine the row clicked on and get it ready for drag/drop
        /// </summary>
        private void dgvSlides_MouseDown(object sender, MouseEventArgs e)
        {
            DataGridView.HitTestInfo info = dgvSlides.HitTest(e.X, e.Y);

            // Trap the situation when the user sorts the columns. The default action is for the datagridview index to remain
            // the same, which inadvertently selects a new slide. This gives the user the impression that the slide jumps
            // around when sorting columns. This helps disable that.
            if (info.Type.Equals(DataGridViewHitTestType.ColumnHeader))
            {
                // Set doSlideListSelectionChange to false so that the WebBrowser control doesn't load the newly selected slide.
                DoSlideListSelectionChange = false;
                return;
            }

            if (info.RowIndex >= 0)
            {
                DataRowView view = (DataRowView)dgvSlides.Rows[info.RowIndex].DataBoundItem;

                if (view != null)
                {
                    CurrentDraggedItem = view;
                    dgvSlides.DoDragDrop(view, DragDropEffects.Move);
                }
            }

            // Dynamically show the context menu for the slide list
            if ((info.Type == DataGridViewHitTestType.Cell || info.Type == DataGridViewHitTestType.RowHeader) && ((e.Button & MouseButtons.Right) != 0))
            {
                if (!dgvSlides.Rows[info.RowIndex].Selected)
                {
                    // Switch selection
                    ////only deselect if there is one selected row, otherwise this will mess up multi-select
                    //if (dgvSlides.SelectedRows.Count == 1)
                    //{
                    DeselectDataGridViewItems();
                    //)
                    DataGridViewRow selectedRow = dgvSlides.Rows[info.RowIndex];
                    selectedRow.Selected = true;
                }


                // Show the context menu
                cmnSlide.Show(dgvSlides, e.X, e.Y);
            }

        }

        /// <summary>
        /// When the selection has changed, browse to currently selected slide
        /// </summary>
        private void dgvSlides_SelectionChanged(object sender, EventArgs e)
        {
            SlideUpdateButtonToggle();

            //this is set to false before the databind, and then to true after databinding is complete
            if (DoSlideListSelectionChange && dgvSlides.SelectedRows.Count == 1)
            {
                LoadSlideToBrowser(dgvSlides.SelectedRows[0].Cells[2].Value.ToString());
                ReselectSlideID = Convert.ToInt32(dgvSlides.SelectedRows[0].Cells[0].Value);

                System.Collections.ArrayList paths = GetSlidePathList(dgvSlides.SelectedRows[0].Index);
                lstSlideAffiliations.Items.Clear();
                foreach (string path in paths)
                    lstSlideAffiliations.Items.Add(path);
            }
        }

        private void dgvSlides_ColumnSortModeChanged(object sender, DataGridViewColumnEventArgs e)
        {
            DataGridViewColumnSortMode sortMode = e.Column.SortMode;
            int columnIndex = e.Column.Index;
            string columnName = e.Column.Name;
        }

        /// <summary>
        /// Deselects all the datagridview rows that are currently selected
        /// </summary>
        public void DeselectDataGridViewItems()
        {
            if (dgvSlides.SelectedRows.Count > 0)
            {
                int selectedRowsCount = dgvSlides.SelectedRows.Count;

                // Deselect all the rows except the last selected row.
                for (int i = 0; i < selectedRowsCount; i++)
                {
                    dgvSlides.SelectedRows[0].Selected = false;
                }
            }
        }

        #endregion

        #region Slide Actions

        private void NewSlide()
        {
            try
            {
                int slideID = Convert.ToInt32(dgvSlides.SelectedRows[0].Cells[0].Value.ToString());
                ReselectSlideID = slideID;
                GetSlideResortItems();
            }
            catch (Exception ex)
            {
                //TODO
            }

            SlideEditForm = new SlideEdit(SlideEdit.Mode.New, -1);
            SlideEditForm.SlideSaved += new SlideEdit.SlideSavedDelegate(SlideEditForm_SlideSaved);
            SlideEditForm.Owner = this;
            SlideEditForm.ShowDialog();
        }

        private void SlideEditForm_SlideSaved(SlideSavedEventEventArgs e)
        {
            if (e.Error != null)
            {
                MessageBox.Show(e.Error.Message);
            }
            else
            {
                dsSlideLibrary.Merge(e.NewSlideData);
                RefreshSlideListBySlideID(e.NewSlideData.Slide[0].SlideID);
                //if we updated a slide, we need to let the treeview know
                if (e.SaveType == SlideEdit.Mode.Edit)
                {
                    PopulateRegionLanguageTrees();
                }
            }
            SlideEditForm.Close();
        }



        private void UpdateSlide(SlideMultipleUpdateType slideMultipleUpdateType)
        {
            //if (categorySlideTreeViewMain.SelectedNode != null)
            //{
            //    reselectNode = categorySlideTreeViewMain.SelectedNode;
            //}
            int slideID = Convert.ToInt32(dgvSlides.SelectedRows[0].Cells[0].Value.ToString());
            ReselectSlideID = slideID;
            GetSlideResortItems();

            if (dgvSlides.SelectedRows.Count == 1)
            {
                // Just one slide selected, display the SlideEdit form
                SlideEditForm = new SlideEdit(SlideEdit.Mode.Edit, slideID);
                SlideEditForm.SlideSaved += new SlideEdit.SlideSavedDelegate(SlideEditForm_SlideSaved);
                SlideEditForm.Owner = this;
                SlideEditForm.ShowDialog();
            }
            else
            {
                // Multiple slides selected, display the SlideMultipleUpdate form
                List<int> slideIDs = new List<int>();

                // Collect the slide IDs
                foreach (DataGridViewRow slide in dgvSlides.SelectedRows)
                {
                    slideIDs.Add((int)slide.Cells[0].Value);
                }

                SlideMultipleUpdate slideMultipleUpdate = new SlideMultipleUpdate(slideIDs, slideMultipleUpdateType, dsSlideLibrary);
                slideMultipleUpdate.Owner = this;
                slideMultipleUpdate.ShowDialog();
            }
        }

        private void DeleteSlide()
        {
            if (Properties.Settings.Default.IsRegionManagmentEnabled)
            {
                TabControl tabTrees = (TabControl)navigationPanePageSlideLibrary.Controls[0];
                if (tabTrees.SelectedTab != null)
                {
                    LastSelectedRegionLangTab = tabTrees.SelectedIndex;

                    CategorySlideTreeView catSlideTree = (CategorySlideTreeView)tabTrees.SelectedTab.Controls[0];

                    if (catSlideTree.SelectedNode != null)
                    {
                        ReselectNode = catSlideTree.SelectedNode.Parent;
                    }
                }
            }
            else
            {
                CategorySlideTreeView catSlideTree = (CategorySlideTreeView)navigationPanePageSlideLibrary.Controls[0];
                if (catSlideTree.SelectedNode != null)
                {
                    ReselectNode = catSlideTree.SelectedNode.Parent;
                }
            }

            //SlideID to delete
            int slideID = Convert.ToInt32(dgvSlides.SelectedRows[0].Cells[0].Value.ToString());
            int j = 0;

            // Get the current gridview sort order
            GetSlideResortItems();

            // Get an array of deleted rows
            wsIpgAdmin.SlideLibrary.SlideRow[] deletedRowArray = new Admin.wsIpgAdmin.SlideLibrary.SlideRow[dgvSlides.SelectedRows.Count];

            // Get an array of primary keys
            for (int i = 0; i < dgvSlides.SelectedRows.Count; i++)
            {
                j = (dgvSlides.SelectedRows.Count - 1) - i;
                deletedRowArray[j] = (wsIpgAdmin.SlideLibrary.SlideRow)dsSlideLibrary.Slide.Rows.Find((int)dgvSlides.SelectedRows[i].Cells[0].Value);
            }

            SlideMultipleDelete slideMultipleDelete = new SlideMultipleDelete(deletedRowArray);
            slideMultipleDelete.Owner = this;

            // Loop through the list of deleted rows
            foreach (wsIpgAdmin.SlideLibrary.SlideRow slideRow in deletedRowArray)
            {
                // Create the tab
                TabPage slideTab = new TabPage(slideRow.Title);
                ListView slidePathList = new ListView();
                slidePathList.View = View.List;
                slidePathList.Dock = DockStyle.Fill;

                //get all instances of this slide in the Slide Library
                System.Collections.ArrayList slidePathListArray = GetSlideAssignments(slideRow.SlideID);
                foreach (string path in slidePathListArray)
                {
                    ListViewItem slidePath = new ListViewItem(path);
                    slidePathList.Items.Add(slidePath);
                }

                // Add the tab
                slideTab.Controls.Add(slidePathList);
                slideMultipleDelete.tabTreeviewPaths.TabPages.Add(slideTab);
            }

            //System.Diagnostics.Debug.WriteLine(String.Format("Before dialog show: {0}", sw.Elapsed.TotalSeconds.ToString()));
            DialogResult result = slideMultipleDelete.ShowDialog();
            // System.Diagnostics.Debug.WriteLine(String.Format("After dialog show: {0}", sw.Elapsed.TotalSeconds.ToString()));
            if (result == DialogResult.Yes)
            {
                DeleteSlides(deletedRowArray);
            }
        }

        private void DeleteSlides(wsIpgAdmin.SlideLibrary.SlideRow[] deletedRowArray)
        {
            wsIpgAdmin.Admin admin = new Admin.wsIpgAdmin.Admin();
            bool wsResult = false;

            //try to delete, if there's an error result is false
            try
            {
                this.Enabled = false;
                lblStatusMain.Text = "Deleting files...";
                lblStatusMain.Visible = true;
                this.Refresh();
                this.Cursor = Cursors.WaitCursor;


                object[][] deletedRowsObjectArray = new object[deletedRowArray.Length][];
                int i = 0;

                // Build the array that is going to be sent to the webservice
                foreach (wsIpgAdmin.SlideLibrary.SlideRow slideRow in deletedRowArray)
                {
                    deletedRowsObjectArray[i] = new object[] { slideRow.SlideID, slideRow.FileName };
                    i++;
                }
                // Get the result
                wsResult = admin.SlideMultipleDelete(deletedRowsObjectArray);

                this.Cursor = Cursors.Default;
                lblStatusMain.Visible = false;
            }
            catch (Exception ex)
            {
                wsResult = false;
            }
            finally
            {
                this.Enabled = true;
            }
            //if successful refresh the data
            if (wsResult)
            {
                foreach (wsIpgAdmin.SlideLibrary.SlideRow slide in deletedRowArray)
                {
                    dsSlideLibrary.Slide.RemoveSlideRow(slide);
                }
                dsSlideLibrary.AcceptChanges();
                this.Cursor = Cursors.WaitCursor;
                PopulateRegionLanguageTrees();
                this.Cursor = Cursors.Default;

                //RefreshData();

                // Reselect the same spot after the delete
                if (dgvSlides.SelectedRows.Count > 0)
                {
                    int position = bindSlideGrid.Find("SlideID", (int)dgvSlides.SelectedRows[0].Cells[0].Value);

                    if (position == (dgvSlides.Rows.Count - 1))
                        position = dgvSlides.Rows.Count - 2;

                    RefreshSlideListByPosition(position);
                }
            }
            else
            {
                MessageBox.Show("There was an error processing your request. Your change was not saved.");
            }
        }

        /// <summary>
        /// Loads the supplied slide to the browser (via the server)
        /// </summary>
        /// <param name="slide">SlideLibrary.SlideRow--the slide to browse to</param>
        public void LoadSlideToBrowser(string fileName)
        {
            this.Cursor = Cursors.WaitCursor;

            // If we don't have the Refresh method before the Navigate method the webbrowser control could break. For example if the browser returns
            // something other than a found slide, an error of some sort, then the webbrowser control decides to quite unless there is a Refresh
            // method call first. Also, by WebBrowserRefreshOption.Completely we completely clear the cache
            webSlideViewer.Refresh(WebBrowserRefreshOption.Completely);

            string navUri = Properties.Settings.Default.SlideNavigateLocation + fileName;
            // Navigate to the slide
            webSlideViewer.Navigate(navUri);

            // I'm not sure why but by having an additional Refresh method call, after the Navigate method call, the webbrowser control loads the 
            // slide a LOT quicker.
            webSlideViewer.Refresh(WebBrowserRefreshOption.Completely);

            this.Cursor = Cursors.Default;
        }

        /// <summary>
        /// Checks to see if this file is already used in the Slide list
        /// </summary>
        /// <param name="fileName">file name to check</param>
        /// <returns>true/false</returns>
        public bool CheckForDuplicateFilename(string fileName)
        {
            bool result = false;
            try
            {
                DataRow[] slidesFounds = dsSlideLibrary.Slide.Select("FileName='" + fileName + "'");
                //if we found any rows with this filename then it is a duplicate
                if (slidesFounds.Length < 1)
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                result = false;
            }

            return result;
        }

        /// <summary>
        /// Retrieve all instances of the slide in the SlideLibrary
        /// </summary>
        /// <param name="slideID">ID of slide we are looking for</param>
        /// <returns>ArrayList of string paths</returns>
        public System.Collections.ArrayList GetSlideAssignments(int slideID)
        {
            System.Collections.ArrayList slidePaths = new System.Collections.ArrayList();

            //get each SlideCategory with this SlideID
            DataRow[] slideCats = dsSlideLibrary.SlideCategory.Select("SlideID=" + slideID.ToString());

            if (Properties.Settings.Default.IsRegionManagmentEnabled)
            {
                //we need to get the paths to these SlideCat nodes
                //loop through the each tree for each SlideCat row
                TabControl tabTrees = (TabControl)navigationPanePageSlideLibrary.Controls[0];
                //now figure out where each SlideCategory is in the SlideLibrary treeview
                foreach (TabPage tPage in tabRegionLanguageTrees.TabPages)
                {
                    foreach (DataRow row in slideCats)
                    {
                        NodeToDelete = null;

                        int catID = ((wsIpgAdmin.SlideLibrary.SlideCategoryRow)row).CategoryID;
                        int regLangID = ((wsIpgAdmin.SlideLibrary.SlideCategoryRow)row).RegionLanguageID;
                        if (regLangID == ((wsIpgAdmin.SlideLibrary.RegionLanguageRow)tPage.Tag).RegionLanguageID)
                        {
                            wsIpgAdmin.SlideLibrary.SlideCategoryRow slideCatRow = dsSlideLibrary.SlideCategory.FindByRegionLanguageIDCategoryIDSlideID(regLangID, catID, slideID);
                            CategorySlideTreeView catSlideTree = (CategorySlideTreeView)tPage.Controls[0];
                            FindAllNodesBySlideID(catSlideTree.Nodes[0], slideCatRow);

                            if (NodeToDelete != null)
                            {
                                slidePaths.Add(String.Format("{0}--{1}", tPage.Text, NodeToDelete.FullPath));
                            }
                        }
                    }
                }
            }
            else
            {
                foreach (DataRow row in slideCats)
                {
                    NodeToDelete = null;

                    int catID = ((wsIpgAdmin.SlideLibrary.SlideCategoryRow)row).CategoryID;
                    int regLangID = ((wsIpgAdmin.SlideLibrary.SlideCategoryRow)row).RegionLanguageID;
                    wsIpgAdmin.SlideLibrary.SlideCategoryRow slideCatRow = dsSlideLibrary.SlideCategory.FindByRegionLanguageIDCategoryIDSlideID(regLangID, catID, slideID);
                    CategorySlideTreeView catSlideTree = (CategorySlideTreeView)navigationPanePageSlideLibrary.Controls[0];
                    FindAllNodesBySlideID(catSlideTree.Nodes[0], slideCatRow);

                    if (NodeToDelete != null)
                    {
                        slidePaths.Add(NodeToDelete.FullPath);
                    }
                }
            }

            return slidePaths;
        }

        /// <summary>
        /// Capture the Progress Changed event to disable the Datagrid View while the Slide Viewer is loading the given slide. If
        /// the slide is not loaded in time for the user to make another slide selection the Slide Viewer gets confused and stops
        /// responding to user requests. By disabling the Datagrid View we prevent this situation from happening.
        /// </summary>
        private void webSlideViewer_ProgressChanged(object sender, WebBrowserProgressChangedEventArgs e)
        {
            dgvSlides.Enabled = false;
            navigationPanePageSlideLibrary.Enabled = false;

            // Wait to enable the Datagrid View until the slide has loaded.
            if (e.MaximumProgress == e.CurrentProgress)
            {
                dgvSlides.Enabled = true;
                navigationPanePageSlideLibrary.Enabled = true;
            }

        }

        /// <summary>
        /// After a sort event has fired find the original slide in the datagridview and reselect it. That way when a sort is made the user 
        /// will still have the same slide selected. This stops the "jumping effect" from slide to slide during a sort operation.
        /// </summary>
        private void dgvSlides_Sorted(object sender, EventArgs e)
        {
            // Reselect the slide
            bindSlideGrid.Position = bindSlideGrid.Find("SlideID", ReselectSlideID);
            bindSlideGrid.ResetItem(bindSlideGrid.Position);
            if (dgvSlides.Rows.Count > 0)
            {
                dgvSlides.Rows[bindSlideGrid.Position].Selected = true;
                dgvSlides.FirstDisplayedScrollingRowIndex = dgvSlides.Rows[bindSlideGrid.Position].Index;
            }


            // Allow the webbrowser to load slides again after temporarially being disabled in the MouseDown event.
            DoSlideListSelectionChange = true;
        }

        private void SlideUpdateButtonToggle()
        {
            if (dgvSlides.SelectedRows.Count > 1)
            {
                toolStripButtonUpdateSlide.Enabled = false;
                miMainSlideUpdateSlide.Enabled = false;
                miSlideUpdate.Enabled = false;
            }
            else
            {
                toolStripButtonUpdateSlide.Enabled = true;
                miMainSlideUpdateSlide.Enabled = true;
                miSlideUpdate.Enabled = true;
            }
        }

        #endregion

        #region Data Update


        /// <summary>
        /// Fires when the CreateDataUpdateAnync method has completed
        /// </summary>
        private void adminUpdate_CreateDataUpdateCompleted(object sender, Admin.wsIpgAdmin.CreateDataUpdateCompletedEventArgs e)
        {
            //CreateDataUpdate is a boolean method, so the result will be true or false
            //an error could also be returned. we'll treat that as false
            bool wsResult;
            if (e.Result == true)
            {
                wsResult = true;
            }
            else if (e.Result == false)
            {
                wsResult = false;
            }
            else if (e.Error != null)
            {
                wsResult = false;
            }
            else
            {
                wsResult = false;
            }

            this.Enabled = true;
            this.Cursor = Cursors.Default;
            if (wsResult)
            {
                MessageBox.Show("Update was successfully created.", "", MessageBoxButtons.OK);
            }
            else
            {
                MessageBox.Show("There was an error creating the update.", "", MessageBoxButtons.OK);
            }

        }

        #endregion

        #region Main Menu

        private void miMainExit_Click(object sender, EventArgs e)
        {
            DialogResult result;
            result = MessageBox.Show("Are you sure you wish to exit?", "Exit", MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void miMainModulesSupportFiles_Click(object sender, EventArgs e)
        {
            SupportFile supportFileForm = new SupportFile();
            supportFileForm.Owner = this;
            supportFileForm.ShowDialog();
        }

        private void miMainHelp_Click(object sender, EventArgs e)
        {
            Help helper = new Help();
            helper.Owner = this;
            helper.Show();
        }

        private void miFilterManagement_Click(object sender, EventArgs e)
        {
            //FilterEdit filterEdit = new FilterEdit();
            //filterEdit.Owner = this;
            //filterEdit.Show();

            FilterManagement filterMan = new FilterManagement();
            filterMan.Owner = this;
            filterMan.Show();
        }

        private void miMainModulesAnnouncementManagement_Click(object sender, EventArgs e)
        {
            AnnouncementManagement announce = new AnnouncementManagement();
            announce.Owner = this;
            announce.ShowDialog();
        }

        private void miMainUserInfoDataUpdates_Click(object sender, EventArgs e)
        {
            ClientDataUpdateInfo clientDataUpdateInfo = new ClientDataUpdateInfo();
            clientDataUpdateInfo.Owner = this;
            clientDataUpdateInfo.ShowDialog();
        }

        private void miMainUserInfoRegistrants_Click(object sender, EventArgs e)
        {
            Registrations reg = new Registrations();
            reg.Owner = this;
            reg.ShowDialog();
        }

        private void miMainSlideNewSlide_Click(object sender, EventArgs e)
        {
            NewSlide();
        }

        private void miMainSlideUpdateSlide_Click(object sender, EventArgs e)
        {
            UpdateSlide(SlideMultipleUpdateType.RegionLanguageAndFilter);
        }

        private void miMainSlideDeleteSlide_Click(object sender, EventArgs e)
        {
            DeleteSlide();
        }

        private void miMainUpdatePublishContentUpdate_Click(object sender, EventArgs e)
        {
            // ONLY do one or the other.  If the user selected both, the 
            // event handler that's called when the desktop content is updated
            // will call publishLocalizedWebContent() on it's own.
            if (miMainUpdatesPublishLocalizedContentUpdate.Checked)
            {
                // HACK:  There must be a nicer way of doing this.
                // if the user wanted to publish content to the website, 
                // the event handler that catches the completion of the desktop publisher catches it. 
                // jamescooper 09/2010
                publishLocalizedDesktopContent();
            }
            // "else if" is intentional.  See note above.
            else if (miMainUpdatesPublishLocalizedWebContentUpdate.Checked)
            {
                publishLocalizedWebContent();
            }

        }

        private void publishLocalizedDesktopContent()
        {
            wsIpgAdmin.Admin ws = new Admin.wsIpgAdmin.Admin();
            ws.CreateLocalizedContentUpdateCompleted += new Admin.wsIpgAdmin.CreateLocalizedContentUpdateCompletedEventHandler(ws_CreateLocalizedContentUpdateCompleted);
            //ws.Shizmo();
            ws.CreateLocalizedContentUpdateAsync();
            lblStatusMain.Image = Properties.Resources.RedLoadingGif;
            lblStatusMain.DisplayStyle = ToolStripItemDisplayStyle.ImageAndText;
            lblStatusMain.Text = "Creating content update...";
            lblStatusMain.Visible = true;
            splMain.Enabled = false;
        }

        private void publishLocalizedWebContent()
        {
            wsIpgAdmin.Admin ws = new Admin.wsIpgAdmin.Admin();
            ws.CreateLocalizedWebContentUpdateCompleted += new Admin.wsIpgAdmin.CreateLocalizedWebContentUpdateCompletedEventHandler(ws_CreateLocalizedWebContentUpdateCompleted);
            ws.CreateLocalizedWebContentUpdateAsync();
            lblStatusMain.Image = Properties.Resources.RedLoadingGif;
            lblStatusMain.DisplayStyle = ToolStripItemDisplayStyle.ImageAndText;
            lblStatusMain.Text = "Creating web content update...";
            lblStatusMain.Visible = true;
            splMain.Enabled = false;

        }

        private void miMainDataRefreshData_Click(object sender, EventArgs e)
        {
            RefreshData();
        }

        private void miMainUpdatesPublishLocalizedContentUpdate_Click(object sender, EventArgs e)
        {
            //wsIpgAdmin.Admin ws = new Admin.wsIpgAdmin.Admin();
            //ws.CreateLocalizedContentUpdateCompleted += new Admin.wsIpgAdmin.CreateLocalizedContentUpdateCompletedEventHandler(ws_CreateLocalizedContentUpdateCompleted);
            ////ws.Shizmo();
            //ws.CreateLocalizedContentUpdateAsync();
            //lblStatusMain.Image = Properties.Resources.RedLoadingGif;
            //lblStatusMain.DisplayStyle = ToolStripItemDisplayStyle.ImageAndText;
            //lblStatusMain.Text = "Creating content update...";
            //lblStatusMain.Visible = true;
            //splMain.Enabled = false;
        }

        void ws_CreateLocalizedContentUpdateCompleted(object sender, AsyncCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                System.Text.StringBuilder sb = new StringBuilder();
                sb.Append("There was an error creating the content update.");
                sb.AppendLine();
                sb.Append("Error Details:");
                sb.AppendLine();
                sb.AppendLine();
                sb.Append(e.Error.ToString());

                MessageBox.Show(sb.ToString(), "Content Update Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                lblStatusMain.DisplayStyle = ToolStripItemDisplayStyle.Text;
                lblStatusMain.Visible = false;
                splMain.Enabled = true;
            }
            else if (e.Cancelled)
            {
                MessageBox.Show("The content update was cancelled.", "Update Cancellation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                lblStatusMain.DisplayStyle = ToolStripItemDisplayStyle.Text;
                lblStatusMain.Visible = false;
                splMain.Enabled = true;
            }
            else
            {
                lblStatusMain.DisplayStyle = ToolStripItemDisplayStyle.Text;
                lblStatusMain.Text = "";
                lblStatusMain.Visible = false;
                MessageBox.Show("Localized content update has been successfully created.", "Content Update Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                // HACK: Violates encapsulation and couples this event with the gui AND the web update process.
                // There's surely a more professional way.
                // Then again, everything we're talking about here is in the same class, so it's a minor foul, even if it's
                // the kettle calling the pot black.
                //
                // if the user wanted to publish content to the website, now that we've finished doing it for the desktop version, 
                // let's fire it off for the web version.
                // jamescooper 09/2010
                if (miMainUpdatesPublishLocalizedWebContentUpdate.Checked)
                {
                    publishLocalizedWebContent();
                }
                else
                {
                    lblStatusMain.DisplayStyle = ToolStripItemDisplayStyle.Text;
                    lblStatusMain.Visible = false;
                    splMain.Enabled = true;
                }
            }

        }


        private void miMainSlideUpdateSlideFilterOrRegionLanguages_Click(object sender, EventArgs e)
        {
            ResetSlideFiltersAndOrRegionLanguages(SlideMultipleUpdateType.RegionLanguageAndFilter);
        }

        private void miMainSlideUpdateSlideFilter_Click(object sender, EventArgs e)
        {
            ResetSlideFiltersAndOrRegionLanguages(SlideMultipleUpdateType.FilterOnly);
        }

        private void miMainSlideUpdateSlideRegionLanguages_Click(object sender, EventArgs e)
        {
            ResetSlideFiltersAndOrRegionLanguages(SlideMultipleUpdateType.RegionLanguageOnly);
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            DeckUploader deckUploader = new DeckUploader();
            deckUploader.DeckUploaded += new DeckUploader.DeckUploadedDelegate(deckUploader_DeckUploaded);
            deckUploader.Owner = this;
            deckUploader.ShowDialog();
        }

        private void deckUploader_DeckUploaded(DeckUploadedEventEventArgs e)
        {
            if (e.Error != null)
            {
                MessageBox.Show(e.Error.Message);
            }
            else
            {
                dsSlideLibrary.Merge(e.NewSlideData);
                RefreshSlideListBySlideID(e.NewSlideData.Slide[0].SlideID);
                LoadSlideToBrowser(e.NewSlideData.Slide[0].FileName);
            }
        }

        #endregion

        #region Context menus

        #region miSlide
        private void miSlideResetFiltersAndOrRegionLanguages_Click(object sender, EventArgs e)
        {
            ResetSlideFiltersAndOrRegionLanguages(SlideMultipleUpdateType.RegionLanguageAndFilter);
        }

        private void miSlideResetFilters_Click(object sender, EventArgs e)
        {
            ResetSlideFiltersAndOrRegionLanguages(SlideMultipleUpdateType.FilterOnly);
        }

        private void miSlideRegionLanguages_Click(object sender, EventArgs e)
        {
            ResetSlideFiltersAndOrRegionLanguages(SlideMultipleUpdateType.RegionLanguageOnly);
        }
        #endregion

        #region cmnCategory

        #region cmnCategory Events

        private void cmnCategory_Opening(object sender, CancelEventArgs e)
        {
            CategoryMenuSetup(RightClickNode);
        }

        private void miCategoryEdit_Click(object sender, EventArgs e)
        {
            //int catID = ((wsIpgAdmin.SlideLibrary.CategoryRow)tvwSlideLibrary.SelectedNode.Tag).CategoryID;
            //int parentID = ((wsIpgAdmin.SlideLibrary.CategoryRow)tvwSlideLibrary.SelectedNode.Tag).ParentCategoryID;

            int catID = ((wsIpgAdmin.SlideLibrary.CategoryRow)RightClickNode.Tag).CategoryID;
            int parentID = ((wsIpgAdmin.SlideLibrary.CategoryRow)RightClickNode.Tag).ParentCategoryID;

            CatEdit catEdit = new CatEdit(catID, parentID, HasSlideChildren(RightClickNode));
            catEdit.Owner = this;
            catEdit.ShowDialog();

        }

        private void miCategoryDeleteCat_Click(object sender, EventArgs e)
        {
            TreeNode deleteNode = RightClickNode;// tvwSlideLibrary.SelectedNode;
            string CategoryName = this.NodeToCategory(deleteNode);
            if (MessageBox.Show("Are you sure you want to delete the category " + CategoryName + "?", "Category Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.Yes)
            {
                this.CategoryDelete(this.NodeToCategoryID(deleteNode));
            }
        }

        /// <summary>
        /// Remove a slide from the SlideLibrary.
        /// NOTE: this does not delete the Slide. It only removes this instance of the slide form the SlideLibrary
        /// </summary>
        private void miCategoryRemoveSlide_Click(object sender, EventArgs e)
        {
            TreeNode[] nodes = new TreeNode[1];
            nodes[0] = this.RightClickNode;
            RemoveSlidesFromCategory(nodes);
        }

        private void RemoveSlidesFromCategory(TreeNode[] nodesToRemove)
        {
            foreach (TreeNode node in nodesToRemove)
            {
                TreeNode deleteNode = node;
                this.ReselectNode = node.Parent;
                TreeNode deleteNodesParent = deleteNode.Parent;
                TreeNode nodeToPutBack = (TreeNode)deleteNode.Clone();
                int oldNodeIndex = deleteNode.Index;

                wsIpgAdmin.SlideLibrary.SlideCategoryRow deleteSlideCat;

                deleteSlideCat = dsSlideLibrary.SlideCategory.FindByRegionLanguageIDCategoryIDSlideID(((wsIpgAdmin.SlideLibrary.SlideCategoryRow)deleteNode.Tag).RegionLanguageID,
                    ((wsIpgAdmin.SlideLibrary.SlideCategoryRow)deleteNode.Tag).CategoryID,
                    ((wsIpgAdmin.SlideLibrary.SlideCategoryRow)deleteNode.Tag).SlideID);

                deleteSlideCat.Delete();
                deleteNode.Remove();

                foreach (TreeNode currentNode in deleteNodesParent.Nodes)
                {
                    int slideID = ((wsIpgAdmin.SlideLibrary.SlideCategoryRow)currentNode.Tag).SlideID;
                    int catID = ((wsIpgAdmin.SlideLibrary.SlideCategoryRow)currentNode.Tag).CategoryID;
                    int regLangID = ((wsIpgAdmin.SlideLibrary.SlideCategoryRow)currentNode.Tag).RegionLanguageID;

                    wsIpgAdmin.SlideLibrary.SlideCategoryRow currentSlideCat = dsSlideLibrary.SlideCategory.FindByRegionLanguageIDCategoryIDSlideID(regLangID, catID, slideID);
                    currentSlideCat.SortOrder = currentNode.Index;
                }

                wsIpgAdmin.SlideLibrary dsChanges = (wsIpgAdmin.SlideLibrary)dsSlideLibrary.GetChanges();
                wsIpgAdmin.Admin admin = new Admin.wsIpgAdmin.Admin();
                bool wsResult;
                try
                {
                    wsResult = admin.SlideCategoryUpdate(dsChanges);
                }
                catch (Exception ex)
                {
                    wsResult = false;
                }

                if (wsResult)
                {
                    dsSlideLibrary.AcceptChanges();
                    RightClickNode = null;
                }
                else
                {
                    dsSlideLibrary.RejectChanges();
                    deleteNodesParent.Nodes.Insert(oldNodeIndex, nodeToPutBack);
                    MessageBox.Show("There was an error processing your request. Your change was not saved.");
                }
            }
        }

        private void miCategoryCategoryAdd_Click(object sender, EventArgs e)
        {
            int parentID = ((wsIpgAdmin.SlideLibrary.CategoryRow)RightClickNode.Tag).CategoryID;
            CatEdit catEdit = new CatEdit(-1, parentID, HasSlideChildren(RightClickNode));
            catEdit.Owner = this;
            catEdit.ShowDialog();
        }

        #endregion

        /// <summary>
        /// Determines the correct menu items for the cmnCategory menu
        /// </summary>
        /// <param name="selectedNode">the node we need the context menu for</param>
        private void CategoryMenuSetup(TreeNode selectedNode)
        {
            // Set all options to false, we'll turn some back on depending on the node type
            miCategoryCategoryAdd.Visible = false;
            miCategoryEdit.Visible = false;
            miCategoryDeleteCat.Visible = false;
            miCategoryRemoveSlide.Visible = false;

            if (selectedNode == null)
                return;

            // Don't let them do anything to the root node
            //if (categorySlideTreeViewMain.Nodes[0] == selectedNode)
            //{
            //    cmnCategory.Close();
            //    return;
            //}

            if (selectedNode.Tag.GetType() == typeof(wsIpgAdmin.SlideLibrary.CategoryRow))
            {
                miCategoryEdit.Visible = true;
                miCategoryDeleteCat.Visible = true;
                if (!HasSlideChildren(selectedNode))
                {
                    miCategoryCategoryAdd.Visible = true;
                }

            }
            else if (selectedNode.Tag.GetType() == typeof(wsIpgAdmin.SlideLibrary.SlideCategoryRow))
            {
                miCategoryRemoveSlide.Visible = true;
            }



        }

        private void miRegionLanguageCategoryRemoveSlide_Click(object sender, EventArgs e)
        {
            List<int> slideID = new List<int>();
            SlideEdit slideEdit = new SlideEdit(SlideEdit.Mode.Edit, ((wsIpgAdmin.SlideLibrary.SlideCategoryRow)RightClickNode.Tag).SlideID);
            slideEdit.SelectedTab = (int)SlideEdit.Tabs.RegionLanguages;
            slideEdit.ShowDialog(this);
        }

        #endregion

        #endregion

        #region ToolStrips

        #region Slide ToolStrip

        private void toolStripButtonNewSlide_Click(object sender, EventArgs e)
        {
            NewSlide();
        }

        private void toolStripButtonUpdateSlide_Click(object sender, EventArgs e)
        {
            UpdateSlide(SlideMultipleUpdateType.RegionLanguageAndFilter);
        }

        private void toolStripButtonDeleteSlide_Click(object sender, EventArgs e)
        {
            DeleteSlide();
        }

        #endregion

        private void toolStripComboBoxRegionLanguage_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopulateSlideList();
        }


        #endregion

        #region Reg/Lang or Filters

        #region Reg/Lang

        private void FillRegionLanguagesGroupGridAndSlideListComboBox()
        {
            DataSets.MyRegionLanguages dsMyRegLangs = new Admin.DataSets.MyRegionLanguages();
            dsMyRegLangs = GetMyRegionLanguages();

            //get the RegionLanguages
            wsIpgAdmin.Admin adminWS = new Admin.wsIpgAdmin.Admin();
            wsIpgAdmin.SlideLibrary dsFilterAndRegLangInfo = new Admin.wsIpgAdmin.SlideLibrary();
            dsFilterAndRegLangInfo.Merge(adminWS.RegionsAndLanguagesGet());
            regionLanguageGroupGrid.FillRegionLanguages(dsFilterAndRegLangInfo);

            List<int> myRegLangIds = new List<int>();
            foreach (DataSets.MyRegionLanguages.MyRegionLanguagesRow myRegLang in dsMyRegLangs._MyRegionLanguages.Rows)
            {
                myRegLangIds.Add(myRegLang.RegionLanguageID);
            }
            regionLanguageGroupGrid.CheckSlideRegionLanguages(myRegLangIds);

            toolStripComboBoxRegionLanguage.Items.Clear();
            // Add the default "Show All" item
            toolStripComboBoxRegionLanguage.Items.Add("Show All from Selected Region/Languages");
            toolStripComboBoxRegionLanguage.Tag = dsFilterAndRegLangInfo.RegionLanguage.Rows;

            // Add all the region languages to the ComboBox
            foreach (DataSets.MyRegionLanguages.MyRegionLanguagesRow myRegLang in dsMyRegLangs._MyRegionLanguages.Rows)
            {
                toolStripComboBoxRegionLanguage.Items.Add(dsFilterAndRegLangInfo.RegionLanguage.FindByRegionLanguageID(myRegLang.RegionLanguageID).RegionRow.RegionName + " - " + dsFilterAndRegLangInfo.RegionLanguage.FindByRegionLanguageID(myRegLang.RegionLanguageID).LanguageRow.LongName);
            }

            // Preselect the first item
            if (toolStripComboBoxRegionLanguage.Items.Count > 0)
            {
                toolStripComboBoxRegionLanguage.SelectedIndex = 0;
            }
        }

        private void btnToggleRegionLanguages_Click(object sender, EventArgs e)
        {
            if (LastRegionLanguageToggle)
            {
                regionLanguageGroupGrid.UnCheckAllSlideRegionLanguages();
                LastRegionLanguageToggle = false;
            }
            else
            {
                regionLanguageGroupGrid.CheckAllSlideRegionLanguages();
                LastRegionLanguageToggle = true;
            }
        }

        private void btnExpandAllRegions_Click(object sender, EventArgs e)
        {
            regionLanguageGroupGrid.ExpandAllGroups();
        }

        private void btnCollapseAllRegions_Click(object sender, EventArgs e)
        {
            regionLanguageGroupGrid.CollapseAllGroups();
        }

        /// <summary>
        /// Applies the region/language settings as well as the filter 
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void btnApplyRegionLanguageSettings_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;

            // Since the Region Language tabs are dynamic reset the selected tab index when applying
            LastSelectedRegionLangTab = 0;

            DataSets.MyRegionLanguages dsMyRegLangs = new Admin.DataSets.MyRegionLanguages();
            foreach (int selectedRegLangId in regionLanguageGroupGrid.GetSelectedRegionLanguageIDs())
            {
                dsMyRegLangs._MyRegionLanguages.AddMyRegionLanguagesRow(selectedRegLangId);
            }

            dsMyRegLangs.WriteXml(Path.Combine(XmlDirectory, "MyRegionLanguages.xml"));

            //PopulateRegionLanguageTrees();
            //FillRegionLanguagesGroupGridAndSlideListComboBox();
            RefreshData();

            Cursor = Cursors.Default;
        }

        #endregion

        #region Filters

        private void btnToggleFilters_Click(object sender, EventArgs e)
        {
            if (LastFilterToggle)
            {
                filterTabs.CheckAllFilters();
                LastFilterToggle = false;
            }
            else
            {
                filterTabs.UnCheckAllFilters();
                LastFilterToggle = true;
            }
        }

        public void FillFilters()
        {
            filterTabs.Fill(dsSlideLibrary);
        }

        /// <summary>
        /// Handles the Click event of the btnApplyFilterSettings control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void btnApplyFilterSettings_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;


            // Since the Region Language tabs are dynamic reset the selected tab index when applying
            LastSelectedRegionLangTab = 0;

            // Populate the treeviews
            //PopulateSlideLibrary();
            PopulateRegionLanguageTrees();

            Cursor = Cursors.Default;
        }

        #endregion

        private void ResetSlideFiltersAndOrRegionLanguages(SlideMultipleUpdateType slideMultipleUpdateType)
        {
            List<int> slideIDs = new List<int>();

            foreach (DataGridViewRow slideRow in dgvSlides.SelectedRows)
            {
                slideIDs.Add((int)slideRow.Cells[0].Value);
            }

            SlideMultipleUpdate slideMultipleUpdate = new SlideMultipleUpdate(slideIDs, slideMultipleUpdateType, dsSlideLibrary);
            slideMultipleUpdate.Owner = this;
            slideMultipleUpdate.ShowDialog();
        }

        #endregion

        #region Misc

        /// <summary>
        /// Deletes a category 
        /// </summary>
        /// <param name="categoryID">ID of category to delete</param>
        /// <returns></returns>
        private void CategoryDelete(int categoryID)
        {
            bool bResult;//*SS

            wsIpgAdmin.Admin admin = new Admin.wsIpgAdmin.Admin();
            try
            {
                //*SS Added the return bool value so we can show a dialog if the save does not happen.
                bResult = admin.CategoryDelete(categoryID); //this is a boolean web method
                if (bResult)
                    RefreshData();
                else
                    MessageBox.Show("The Category was not able to be deleted.", "Request cancelled", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show("There was an error processing this request.", "Request cancelled", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void tabRegionLanguageTrees_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.LastSelectedRegionLangTab = tabRegionLanguageTrees.SelectedIndex;
        }

        #endregion

        private void miMainUpdatesPublishLocalizedWebContentUpdate_Click(object sender, EventArgs e)
        {
            //wsIpgAdmin.Admin ws = new Admin.wsIpgAdmin.Admin();
            //ws.CreateLocalizedWebContentUpdateCompleted += new Admin.wsIpgAdmin.CreateLocalizedWebContentUpdateCompletedEventHandler(ws_CreateLocalizedWebContentUpdateCompleted);
            //ws.CreateLocalizedWebContentUpdateAsync();
            //lblStatusMain.Image = Properties.Resources.RedLoadingGif;
            //lblStatusMain.DisplayStyle = ToolStripItemDisplayStyle.ImageAndText;
            //lblStatusMain.Text = "Creating web content update...";
            //lblStatusMain.Visible = true;
            //splMain.Enabled = false;
        }

        private void ws_CreateLocalizedWebContentUpdateCompleted(object sender, AsyncCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                System.Text.StringBuilder sb = new StringBuilder();
                sb.Append("There was an error creating the web content update.");
                sb.AppendLine();
                sb.Append("Error Details:");
                sb.AppendLine();
                sb.AppendLine();
                sb.Append(e.Error.ToString());

                MessageBox.Show(sb.ToString(), "Web Content Update Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (e.Cancelled)
            {
                MessageBox.Show("The web content update was cancelled.", "Update Cancellation", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                lblStatusMain.DisplayStyle = ToolStripItemDisplayStyle.Text;
                lblStatusMain.Text = "";
                lblStatusMain.Visible = false;
                MessageBox.Show("Localized web content update has been successfully created.", "Web Content Update Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            lblStatusMain.DisplayStyle = ToolStripItemDisplayStyle.Text;
            lblStatusMain.Visible = false;
            splMain.Enabled = true;
        }

        private void supportFileBatchUploadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SupportFileBatchUpload supportFileBatchUpload = new SupportFileBatchUpload();
            supportFileBatchUpload.Owner = this;
            supportFileBatchUpload.ShowDialog();
        }

        private void dgvSlides_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 4 && e.RowIndex > -1)
            {
                //get all instances of this slide in the Slide Library
                System.Collections.ArrayList slidePathListArray = GetSlidePathList(e.RowIndex);

                dgvSlides[e.ColumnIndex, e.RowIndex].ToolTipText = "";

                foreach (string path in slidePathListArray)
                {
                    ListViewItem slidePath = new ListViewItem(path);
                    dgvSlides[e.ColumnIndex, e.RowIndex].ToolTipText += slidePath.Text + "\n";
                }
            }
        }

        private System.Collections.ArrayList GetSlidePathList(int rowIndex)
        {
            return GetSlideAssignments(Convert.ToInt32(dgvSlides[0, rowIndex].Value));
        }

        #region Multiple Delete Methods

        void slideDeleteContainer_RemoveSlides(object sender, SlideDeleteContainerRemoveSlideEventArgs e)
        {
            slidesToDelete.Remove(e.Slide);
            slideDeleteContainer.Refresh(slidesToDelete);
        }

        void slideDeleteContainer_ClearSlides(object sender, SlideDeleteContainerClearSlidesEventArgs e)
        {
            slidesToDelete.Clear();
            slideDeleteContainer.Refresh(slidesToDelete);
            ManageMiddleUtilityArea(MiddleUtilityAreaShowOption.SlideAffiliationsOption);
        }

        void slideDeleteContainer_DeleteSlides(object sender, SlideDeleteContainerDeleteSlidesEventArgs e)
        {
            DialogResult result = MessageBox.Show("Are you sure you want to delete these slides?  You will not be able to get the slides back once they are deleted.", "Delete Slides Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (result.Equals(DialogResult.Yes))
            {
                DeleteSlides(slidesToDelete.ToArray());
                slidesToDelete.Clear();
                ManageMiddleUtilityArea(MiddleUtilityAreaShowOption.SlideAffiliationsOption);
            }
        }

        void slideDeleteContainer_CloseContainer(object sender, SlideDeleteContainerCloseContainerEventArgs e)
        {
            slidesToDelete.Clear();
            slideDeleteContainer.Refresh(slidesToDelete);
            ManageMiddleUtilityArea(MiddleUtilityAreaShowOption.SlideAffiliationsOption);
        }

        void catSlideTree_TreeViewEntryNodeDeleteClick(object sender, TreeViewEntryNodeDeleteEventArgs e)
        {
            wsIpgAdmin.SlideLibrary.SlideRow row = (wsIpgAdmin.SlideLibrary.SlideRow)dsSlideLibrary.Slide.Rows.Find((int)e.SlideId);

            //System.Collections.ArrayList slidePathListArray = GetSlideAssignments(row.SlideID);

            bool found = false;
            foreach (wsIpgAdmin.SlideLibrary.SlideRow slide in slidesToDelete)
            {
                if (slide.SlideID.Equals(e.SlideId))
                {
                    found = true;
                    break;
                }
            }
            if (found.Equals(false))
                slidesToDelete.Add(row);

            slideDeleteContainer.Refresh(slidesToDelete);
            ManageMiddleUtilityArea(MiddleUtilityAreaShowOption.DeleteSlideOption);
        }
        #endregion

        #region Multiple Remove From Category Methods
        void catSlideTree_TreeViewEntryNodeRemoveClick(object sender, TreeViewEntryNodeRemoveEventArgs e)
        {
            wsIpgAdmin.SlideLibrary.SlideCategoryRow slideCategoryRow = (wsIpgAdmin.SlideLibrary.SlideCategoryRow)e.SelectedNode.Tag;
            wsIpgAdmin.SlideLibrary.CategoryRow categoryRow = (wsIpgAdmin.SlideLibrary.CategoryRow)dsSlideLibrary.Category.Rows.Find((int)slideCategoryRow.CategoryID);
            wsIpgAdmin.SlideLibrary.SlideRow slideRow = (wsIpgAdmin.SlideLibrary.SlideRow)dsSlideLibrary.Slide.Rows.Find((int)slideCategoryRow.SlideID);
            SlideRemoveTransport transport = new SlideRemoveTransport(slideCategoryRow, categoryRow, slideRow, e.SelectedNode);

            if (slidesToRemoveFromCategory.Count < 1)
            {
                slidesToRemoveFromCategory.Add(transport);
            }
            else
            {  
                //must be the same category from the same region language id for it to be added to the list.
                //the first slide added dictates where the others may come from.
                if (slidesToRemoveFromCategory[0].CategoryRow.CategoryID.Equals(categoryRow.CategoryID) && slidesToRemoveFromCategory[0].SlideCategoryRow.RegionLanguageID.Equals(slideCategoryRow.RegionLanguageID))
                {
                    bool found = false;
                    foreach (SlideRemoveTransport srt in slidesToRemoveFromCategory)
                    {
                        if (srt.SlideRow.SlideID.Equals(slideRow.SlideID))
                        {
                            found = true;
                            break;
                        }
                    }
                    if (found.Equals(false))
                    {
                        slidesToRemoveFromCategory.Add(transport);
                    }
                }
                else
                {
                    string message = string.Format("Slide {0} cannot be added because it does not belong to this category: {1}. Please choose a slide from that category to continue.", slideRow.Title, categoryRow.Category);
                    MessageBox.Show(message);
                }
            }

            slideRemoveContainer.Refresh(slidesToRemoveFromCategory);
            ManageMiddleUtilityArea(MiddleUtilityAreaShowOption.RemoveSlideFromCategoryOption);

        }
        void slideRemoveContainer_CloseContainer(object sender, SlideRemoveContainerCloseContainerEventArgs e)
        {
            slidesToRemoveFromCategory.Clear();
            slideRemoveContainer.Refresh(slidesToRemoveFromCategory);
            ManageMiddleUtilityArea(MiddleUtilityAreaShowOption.SlideAffiliationsOption);
        }

        void slideRemoveContainer_RemoveFromList(object sender, SlideRemoveContainerRemoveFromListSlideEventArgs e)
        {
            slidesToRemoveFromCategory.Remove(e.SlideRemoveTransport);
            slideRemoveContainer.Refresh(slidesToRemoveFromCategory);
        }

        void slideRemoveContainer_RemoveFromCategory(object sender, SlideRemoveContainerRemoveFromCategorySlidesEventArgs e)
        {
            TreeNode[] nodes = new TreeNode[slidesToRemoveFromCategory.Count];
            int index = 0;
            foreach (SlideRemoveTransport srt in slidesToRemoveFromCategory)
            {
                nodes[index] = srt.SelectedNode;
                index++;
            }
            RemoveSlidesFromCategory(nodes);
            slidesToRemoveFromCategory.Clear();
            ManageMiddleUtilityArea(MiddleUtilityAreaShowOption.SlideAffiliationsOption);
        }

        void slideRemoveContainer_ClearSlides(object sender, SlideRemoveContainerClearSlidesEventArgs e)
        {
            slidesToRemoveFromCategory.Clear();
            slideRemoveContainer.Refresh(slidesToRemoveFromCategory);
            ManageMiddleUtilityArea(MiddleUtilityAreaShowOption.SlideAffiliationsOption);
        }

        #endregion

        private void ManageMiddleUtilityArea(MiddleUtilityAreaShowOption showOption)
        {
            switch (showOption)
            {
                case MiddleUtilityAreaShowOption.SlideAffiliationsOption:
                    slidesToRemoveFromCategory.Clear();
                    slidesToDelete.Clear();
                    slideRemoveContainer.Hide();
                    slideDeleteContainer.Hide();
                    lstSlideAffiliations.Show();
                    break;
                case MiddleUtilityAreaShowOption.DeleteSlideOption:
                    slidesToRemoveFromCategory.Clear();
                    slideRemoveContainer.Hide();
                    lstSlideAffiliations.Hide();
                    slideDeleteContainer.Show();
                    break;
                case MiddleUtilityAreaShowOption.RemoveSlideFromCategoryOption:
                    slidesToDelete.Clear();
                    slideDeleteContainer.Hide();
                    lstSlideAffiliations.Hide();
                    slideRemoveContainer.Show();
                    break;
            }
        }

        private enum MiddleUtilityAreaShowOption
        {
            SlideAffiliationsOption,
            DeleteSlideOption,
            RemoveSlideFromCategoryOption
        }
    }


}