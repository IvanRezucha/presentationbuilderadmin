namespace Admin
{
    partial class FilterManagement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			this.dsSlideLibrary = new Admin.wsIpgAdmin.SlideLibrary();
			this.dgvFilterGroup = new System.Windows.Forms.DataGridView();
			this.filterGroupIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.filterGroupNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.colFilterGroupDelete = new System.Windows.Forms.DataGridViewImageColumn();
			this.filterGroupBindingSource = new System.Windows.Forms.BindingSource(this.components);
			this.dgvFilter = new System.Windows.Forms.DataGridView();
			this.filterIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.filterGroupIDDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.filterNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.colFilterDelete = new System.Windows.Forms.DataGridViewImageColumn();
			this.fKFilterFilterGroupBindingSource = new System.Windows.Forms.BindingSource(this.components);
			this.btnSave = new System.Windows.Forms.Button();
			this.txtFilterGroup = new System.Windows.Forms.TextBox();
			this.btnFilterGroupAdd = new System.Windows.Forms.Button();
			this.btnFilterAdd = new System.Windows.Forms.Button();
			this.txtFilter = new System.Windows.Forms.TextBox();
			this.btnClose = new System.Windows.Forms.Button();
			this.lblFilterGroup = new System.Windows.Forms.Label();
			this.lblFilter = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.dsSlideLibrary)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dgvFilterGroup)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.filterGroupBindingSource)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dgvFilter)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fKFilterFilterGroupBindingSource)).BeginInit();
			this.SuspendLayout();
			// 
			// dsSlideLibrary
			// 
			this.dsSlideLibrary.DataSetName = "SlideLibrary";
			this.dsSlideLibrary.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
			// 
			// dgvFilterGroup
			// 
			this.dgvFilterGroup.AllowUserToAddRows = false;
			this.dgvFilterGroup.AllowUserToResizeRows = false;
			this.dgvFilterGroup.AutoGenerateColumns = false;
			this.dgvFilterGroup.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.filterGroupIDDataGridViewTextBoxColumn,
            this.filterGroupNameDataGridViewTextBoxColumn,
            this.colFilterGroupDelete});
			this.dgvFilterGroup.DataSource = this.filterGroupBindingSource;
			this.dgvFilterGroup.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
			this.dgvFilterGroup.Location = new System.Drawing.Point(21, 77);
			this.dgvFilterGroup.MultiSelect = false;
			this.dgvFilterGroup.Name = "dgvFilterGroup";
			this.dgvFilterGroup.RowHeadersWidth = 20;
			this.dgvFilterGroup.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dgvFilterGroup.ShowEditingIcon = false;
			this.dgvFilterGroup.Size = new System.Drawing.Size(246, 219);
			this.dgvFilterGroup.TabIndex = 1;
			this.dgvFilterGroup.VirtualMode = true;
			this.dgvFilterGroup.CellLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvFilterGroup_CellLeave);
			this.dgvFilterGroup.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvFilterGroup_CellContentDoubleClick);
			this.dgvFilterGroup.CellStateChanged += new System.Windows.Forms.DataGridViewCellStateChangedEventHandler(this.dgvFilterGroup_CellStateChanged);
			this.dgvFilterGroup.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvFilterGroup_CellContentClick);
			// 
			// filterGroupIDDataGridViewTextBoxColumn
			// 
			this.filterGroupIDDataGridViewTextBoxColumn.DataPropertyName = "FilterGroupID";
			this.filterGroupIDDataGridViewTextBoxColumn.HeaderText = "FilterGroupID";
			this.filterGroupIDDataGridViewTextBoxColumn.Name = "filterGroupIDDataGridViewTextBoxColumn";
			this.filterGroupIDDataGridViewTextBoxColumn.ReadOnly = true;
			this.filterGroupIDDataGridViewTextBoxColumn.Visible = false;
			// 
			// filterGroupNameDataGridViewTextBoxColumn
			// 
			this.filterGroupNameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			this.filterGroupNameDataGridViewTextBoxColumn.DataPropertyName = "FilterGroupName";
			this.filterGroupNameDataGridViewTextBoxColumn.HeaderText = "Filter Group";
			this.filterGroupNameDataGridViewTextBoxColumn.Name = "filterGroupNameDataGridViewTextBoxColumn";
			// 
			// colFilterGroupDelete
			// 
			this.colFilterGroupDelete.Description = "Delete";
			this.colFilterGroupDelete.HeaderText = "";
			this.colFilterGroupDelete.Image = global::Admin.Properties.Resources.DeleteFile48PNG;
			this.colFilterGroupDelete.Name = "colFilterGroupDelete";
			this.colFilterGroupDelete.ToolTipText = "Delete";
			this.colFilterGroupDelete.Width = 25;
			// 
			// filterGroupBindingSource
			// 
			this.filterGroupBindingSource.DataMember = "FilterGroup";
			this.filterGroupBindingSource.DataSource = this.dsSlideLibrary;
			// 
			// dgvFilter
			// 
			this.dgvFilter.AllowUserToAddRows = false;
			this.dgvFilter.AllowUserToResizeRows = false;
			this.dgvFilter.AutoGenerateColumns = false;
			this.dgvFilter.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.filterIDDataGridViewTextBoxColumn,
            this.filterGroupIDDataGridViewTextBoxColumn1,
            this.filterNameDataGridViewTextBoxColumn,
            this.colFilterDelete});
			this.dgvFilter.DataSource = this.fKFilterFilterGroupBindingSource;
			this.dgvFilter.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
			this.dgvFilter.Location = new System.Drawing.Point(304, 77);
			this.dgvFilter.MultiSelect = false;
			this.dgvFilter.Name = "dgvFilter";
			this.dgvFilter.RowHeadersWidth = 20;
			this.dgvFilter.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dgvFilter.ShowEditingIcon = false;
			this.dgvFilter.Size = new System.Drawing.Size(246, 219);
			this.dgvFilter.TabIndex = 2;
			this.dgvFilter.CellLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvFilter_CellLeave);
			this.dgvFilter.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvFilter_CellContentDoubleClick);
			this.dgvFilter.CellStateChanged += new System.Windows.Forms.DataGridViewCellStateChangedEventHandler(this.dgvFilter_CellStateChanged);
			this.dgvFilter.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvFilter_CellContentClick);
			// 
			// filterIDDataGridViewTextBoxColumn
			// 
			this.filterIDDataGridViewTextBoxColumn.DataPropertyName = "FilterID";
			this.filterIDDataGridViewTextBoxColumn.HeaderText = "FilterID";
			this.filterIDDataGridViewTextBoxColumn.Name = "filterIDDataGridViewTextBoxColumn";
			this.filterIDDataGridViewTextBoxColumn.ReadOnly = true;
			this.filterIDDataGridViewTextBoxColumn.Visible = false;
			// 
			// filterGroupIDDataGridViewTextBoxColumn1
			// 
			this.filterGroupIDDataGridViewTextBoxColumn1.DataPropertyName = "FilterGroupID";
			this.filterGroupIDDataGridViewTextBoxColumn1.HeaderText = "FilterGroupID";
			this.filterGroupIDDataGridViewTextBoxColumn1.Name = "filterGroupIDDataGridViewTextBoxColumn1";
			this.filterGroupIDDataGridViewTextBoxColumn1.Visible = false;
			// 
			// filterNameDataGridViewTextBoxColumn
			// 
			this.filterNameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			this.filterNameDataGridViewTextBoxColumn.DataPropertyName = "FilterName";
			this.filterNameDataGridViewTextBoxColumn.HeaderText = "Filter";
			this.filterNameDataGridViewTextBoxColumn.Name = "filterNameDataGridViewTextBoxColumn";
			// 
			// colFilterDelete
			// 
			this.colFilterDelete.HeaderText = "";
			this.colFilterDelete.Image = global::Admin.Properties.Resources.DeleteFile48PNG;
			this.colFilterDelete.Name = "colFilterDelete";
			this.colFilterDelete.ToolTipText = "Delete";
			this.colFilterDelete.Width = 25;
			// 
			// fKFilterFilterGroupBindingSource
			// 
			this.fKFilterFilterGroupBindingSource.DataMember = "FK_Filter_FilterGroup";
			this.fKFilterFilterGroupBindingSource.DataSource = this.filterGroupBindingSource;
			// 
			// btnSave
			// 
			this.btnSave.Location = new System.Drawing.Point(394, 317);
			this.btnSave.Name = "btnSave";
			this.btnSave.Size = new System.Drawing.Size(75, 23);
			this.btnSave.TabIndex = 3;
			this.btnSave.Text = "Save";
			this.btnSave.UseVisualStyleBackColor = true;
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// txtFilterGroup
			// 
			this.txtFilterGroup.Location = new System.Drawing.Point(21, 30);
			this.txtFilterGroup.Name = "txtFilterGroup";
			this.txtFilterGroup.Size = new System.Drawing.Size(186, 20);
			this.txtFilterGroup.TabIndex = 4;
			// 
			// btnFilterGroupAdd
			// 
			this.btnFilterGroupAdd.Location = new System.Drawing.Point(213, 27);
			this.btnFilterGroupAdd.Name = "btnFilterGroupAdd";
			this.btnFilterGroupAdd.Size = new System.Drawing.Size(54, 23);
			this.btnFilterGroupAdd.TabIndex = 5;
			this.btnFilterGroupAdd.Text = "Add";
			this.btnFilterGroupAdd.UseVisualStyleBackColor = true;
			this.btnFilterGroupAdd.Click += new System.EventHandler(this.btnFilterGroupAdd_Click);
			// 
			// btnFilterAdd
			// 
			this.btnFilterAdd.Location = new System.Drawing.Point(496, 27);
			this.btnFilterAdd.Name = "btnFilterAdd";
			this.btnFilterAdd.Size = new System.Drawing.Size(54, 23);
			this.btnFilterAdd.TabIndex = 7;
			this.btnFilterAdd.Text = "Add";
			this.btnFilterAdd.UseVisualStyleBackColor = true;
			this.btnFilterAdd.Click += new System.EventHandler(this.btnFilterAdd_Click);
			// 
			// txtFilter
			// 
			this.txtFilter.Location = new System.Drawing.Point(304, 30);
			this.txtFilter.Name = "txtFilter";
			this.txtFilter.Size = new System.Drawing.Size(186, 20);
			this.txtFilter.TabIndex = 6;
			// 
			// btnClose
			// 
			this.btnClose.Location = new System.Drawing.Point(475, 317);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new System.Drawing.Size(75, 23);
			this.btnClose.TabIndex = 8;
			this.btnClose.Text = "Close";
			this.btnClose.UseVisualStyleBackColor = true;
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			// 
			// lblFilterGroup
			// 
			this.lblFilterGroup.AutoSize = true;
			this.lblFilterGroup.Location = new System.Drawing.Point(18, 9);
			this.lblFilterGroup.Name = "lblFilterGroup";
			this.lblFilterGroup.Size = new System.Drawing.Size(83, 13);
			this.lblFilterGroup.TabIndex = 9;
			this.lblFilterGroup.Text = "Add Filter Group";
			// 
			// lblFilter
			// 
			this.lblFilter.AutoSize = true;
			this.lblFilter.Location = new System.Drawing.Point(304, 8);
			this.lblFilter.Name = "lblFilter";
			this.lblFilter.Size = new System.Drawing.Size(51, 13);
			this.lblFilter.TabIndex = 10;
			this.lblFilter.Text = "Add Filter";
			// 
			// FilterManagement
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(578, 350);
			this.Controls.Add(this.lblFilter);
			this.Controls.Add(this.lblFilterGroup);
			this.Controls.Add(this.btnClose);
			this.Controls.Add(this.btnFilterAdd);
			this.Controls.Add(this.txtFilter);
			this.Controls.Add(this.btnFilterGroupAdd);
			this.Controls.Add(this.txtFilterGroup);
			this.Controls.Add(this.btnSave);
			this.Controls.Add(this.dgvFilter);
			this.Controls.Add(this.dgvFilterGroup);
			this.Name = "FilterManagement";
			this.Text = "Filter Management";
			this.Load += new System.EventHandler(this.FilterManagement_Load);
			((System.ComponentModel.ISupportInitialize)(this.dsSlideLibrary)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dgvFilterGroup)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.filterGroupBindingSource)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dgvFilter)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fKFilterFilterGroupBindingSource)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

		private Admin.wsIpgAdmin.SlideLibrary dsSlideLibrary;
        private System.Windows.Forms.DataGridView dgvFilterGroup;
        //private System.Windows.Forms.BindingSource bsFilter;
        private System.Windows.Forms.DataGridView dgvFilter;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TextBox txtFilterGroup;
        private System.Windows.Forms.Button btnFilterGroupAdd;
        private System.Windows.Forms.Button btnFilterAdd;
        private System.Windows.Forms.TextBox txtFilter;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
		private System.Windows.Forms.DataGridViewTextBoxColumn filterGroupIDDataGridViewTextBoxColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn filterGroupNameDataGridViewTextBoxColumn;
		private System.Windows.Forms.DataGridViewImageColumn colFilterGroupDelete;
		private System.Windows.Forms.BindingSource filterGroupBindingSource;
		private System.Windows.Forms.DataGridViewTextBoxColumn filterIDDataGridViewTextBoxColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn filterGroupIDDataGridViewTextBoxColumn1;
		private System.Windows.Forms.DataGridViewTextBoxColumn filterNameDataGridViewTextBoxColumn;
		private System.Windows.Forms.DataGridViewImageColumn colFilterDelete;
		private System.Windows.Forms.BindingSource fKFilterFilterGroupBindingSource;
		private System.Windows.Forms.Label lblFilterGroup;
		private System.Windows.Forms.Label lblFilter;
    }
}