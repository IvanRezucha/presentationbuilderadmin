using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Reflection;

namespace Admin
{
	public partial class Login : MasterForm
	{
		//"failed parsing the policy document" probably means the WebServiceZipFilter.dll reference went tango uniform.
		//check and see if you need to remove it and add it again.
		private wsAdminAuthentication.AdminAuthentication _adminAuthenticationWse = new wsAdminAuthentication.AdminAuthentication();
        private readonly string _registryKeysLocation = Properties.Settings.Default.RegistryKeysLocation;
		private string _adminTicket;

		public string AdminTicket
		{
			get { return _adminTicket; }
		}

		public Login()
		{
			InitializeComponent();
		}

		private void btnOK_Click(object sender, EventArgs e)
		{
			btnOK.Enabled = false;
			Cursor = Cursors.WaitCursor;
			string adminTicket;
			try
			{
				adminTicket = _adminAuthenticationWse.GetAuthentication(txtEmail.Text, txtPassword.Text);
			}
			catch (System.Web.Services.Protocols.SoapHeaderException ex)
			{
				MessageBox.Show("An error occurred, likely because the WebServiceZipFilter.dll is not in the bin folder on the server.  " +
					"The complete error message is" + Environment.NewLine + Environment.NewLine + ex.ToString());
				return;
			}
			Cursor = Cursors.Default;

			if (adminTicket == null)
			{
				//TODO: clean up the login failure handling...
				MessageBox.Show("Sorry, but we couldn't find a user with these credentials.");
				btnOK.Enabled = true;
				txtEmail.Focus();
			}
			else
			{
				_adminTicket = adminTicket;
				try
				{
					//add the Email and password to the registry
					//this will create or open an existing key
					Microsoft.Win32.RegistryKey regKey = Microsoft.Win32.Registry.CurrentUser.CreateSubKey(_registryKeysLocation);
					if (chkRememberMe.Checked)
					{
						regKey.SetValue("Email", txtEmail.Text.Trim());
						regKey.SetValue("Password", txtPassword.Text.Trim());
					}
					else
					{
						regKey.DeleteValue("Email", false);
						regKey.DeleteValue("Password", false);
					}
					regKey.Close();
				}
				finally
				{
					DialogResult = DialogResult.Yes;
				}
			}
		}

		#region Form Load

		private void Login_Load(object sender, EventArgs e)
		{
			string Email = null;
			string Password = null;

			try
			{
				//try to find the values in the registry
				Microsoft.Win32.RegistryKey regKey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(_registryKeysLocation);
				if (regKey != null)
				{
					Email = (string)regKey.GetValue("Email");
					Password = (string)regKey.GetValue("Password");
					regKey.Close();
				}
			}
			catch
			{
				//do nothing, not a big deal if the registry can't be read from or written to
			}

			if ((Email != null && Email.Length > 0) && (Password != null && Password.Length > 0))
			{
				txtEmail.Text = Email;
				txtPassword.Text = Password;
				chkRememberMe.Checked = true;
			}

			//this.Text = String.Format("Welcome To {0}", AssemblyTitle);
			//this.labelProductName.Text = AssemblyProduct;
			//this.lblVersion.Text = String.Format("Version {0}", AssemblyVersion);
			//this.labelCopyright.Text = AssemblyCopyright;
			//this.labelCompanyName.Text = AssemblyCompany;

			if (System.Deployment.Application.ApplicationDeployment.IsNetworkDeployed)
			{
				lblVersion.Text = string.Format("Version {0}", System.Deployment.Application.ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString());
			}
		}

		#endregion

		#region Assembly Attribute Accessors

		public string AssemblyTitle
		{
			get
			{
				// Get all Title attributes on this assembly
				object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
				// If there is at least one Title attribute
				if (attributes.Length > 0)
				{
					// Select the first one
					AssemblyTitleAttribute titleAttribute = (AssemblyTitleAttribute)attributes[0];
					// If it is not an empty string, return it
					if (titleAttribute.Title != "")
						return titleAttribute.Title;
				}
				// If there was no Title attribute, or if the Title attribute was the empty string, return the .exe name
				return System.IO.Path.GetFileNameWithoutExtension(Assembly.GetExecutingAssembly().CodeBase);
			}
		}

		public string AssemblyVersion
		{
			get
			{
				return Assembly.GetExecutingAssembly().GetName().Version.ToString();
			}
		}

		public string AssemblyDescription
		{
			get
			{
				// Get all Description attributes on this assembly
				object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyDescriptionAttribute), false);
				// If there aren't any Description attributes, return an empty string
				if (attributes.Length == 0)
					return "";
				// If there is a Description attribute, return its value
				return ((AssemblyDescriptionAttribute)attributes[0]).Description;
			}
		}

		public string AssemblyProduct
		{
			get
			{
				// Get all Product attributes on this assembly
				object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyProductAttribute), false);
				// If there aren't any Product attributes, return an empty string
				if (attributes.Length == 0)
					return "";
				// If there is a Product attribute, return its value
				return ((AssemblyProductAttribute)attributes[0]).Product;
			}
		}

		public string AssemblyCopyright
		{
			get
			{
				// Get all Copyright attributes on this assembly
				object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCopyrightAttribute), false);
				// If there aren't any Copyright attributes, return an empty string
				if (attributes.Length == 0)
					return "";
				// If there is a Copyright attribute, return its value
				return ((AssemblyCopyrightAttribute)attributes[0]).Copyright;
			}
		}

		public string AssemblyCompany
		{
			get
			{
				// Get all Company attributes on this assembly
				object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCompanyAttribute), false);
				// If there aren't any Company attributes, return an empty string
				if (attributes.Length == 0)
					return "";
				// If there is a Company attribute, return its value
				return ((AssemblyCompanyAttribute)attributes[0]).Company;
			}
		}

		#endregion
	}
}