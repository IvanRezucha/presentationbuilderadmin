﻿namespace Admin
{
    partial class SupportFileBatchUpload
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SupportFileBatchUpload));
            this.tabControlWizard = new System.Windows.Forms.TabControl();
            this.tabPageSelectFiles = new System.Windows.Forms.TabPage();
            this.labelFilesSelectCount = new System.Windows.Forms.Label();
            this.groupBoxStepOne = new System.Windows.Forms.GroupBox();
            this.labelSelectFilesInstructions = new System.Windows.Forms.Label();
            this.toolStripSelectFiles = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonSelectFiles = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonRemoveFiles = new System.Windows.Forms.ToolStripButton();
            this.buttonGoToRegionLanguages = new System.Windows.Forms.Button();
            this.groupBoxSelectedSupportFiles = new System.Windows.Forms.GroupBox();
            this.listViewSupportFiles = new System.Windows.Forms.ListView();
            this.columnHeaderFileName = new System.Windows.Forms.ColumnHeader();
            this.columnHeaderFilePath = new System.Windows.Forms.ColumnHeader();
            this.tabPageRegionLanguages = new System.Windows.Forms.TabPage();
            this.groupBoxStepTwo = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBoxRegionLanguages = new System.Windows.Forms.GroupBox();
            this.regionLanguageGroupGridForSupportFiles = new Admin.UserControls.RegionLanguageGroupGrid();
            this.buttonGoToUpload = new System.Windows.Forms.Button();
            this.tabPageUploadProgress = new System.Windows.Forms.TabPage();
            this.buttonFinished = new System.Windows.Forms.Button();
            this.buttonUploadAnotherBatch = new System.Windows.Forms.Button();
            this.groupBoxStepThree = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonUpload = new System.Windows.Forms.Button();
            this.groupBoxUploadProgress = new System.Windows.Forms.GroupBox();
            this.statusReportUpload = new Admin.UserControls.StatusReport();
            this.imageListProgressImages = new System.Windows.Forms.ImageList(this.components);
            this.backgroundWorkerUpload = new System.ComponentModel.BackgroundWorker();
            this.tabControlWizard.SuspendLayout();
            this.tabPageSelectFiles.SuspendLayout();
            this.groupBoxStepOne.SuspendLayout();
            this.toolStripSelectFiles.SuspendLayout();
            this.groupBoxSelectedSupportFiles.SuspendLayout();
            this.tabPageRegionLanguages.SuspendLayout();
            this.groupBoxStepTwo.SuspendLayout();
            this.groupBoxRegionLanguages.SuspendLayout();
            this.tabPageUploadProgress.SuspendLayout();
            this.groupBoxStepThree.SuspendLayout();
            this.groupBoxUploadProgress.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControlWizard
            // 
            this.tabControlWizard.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.tabControlWizard.Controls.Add(this.tabPageSelectFiles);
            this.tabControlWizard.Controls.Add(this.tabPageRegionLanguages);
            this.tabControlWizard.Controls.Add(this.tabPageUploadProgress);
            this.tabControlWizard.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlWizard.ItemSize = new System.Drawing.Size(0, 1);
            this.tabControlWizard.Location = new System.Drawing.Point(0, 0);
            this.tabControlWizard.Margin = new System.Windows.Forms.Padding(0);
            this.tabControlWizard.Multiline = true;
            this.tabControlWizard.Name = "tabControlWizard";
            this.tabControlWizard.SelectedIndex = 0;
            this.tabControlWizard.Size = new System.Drawing.Size(738, 683);
            this.tabControlWizard.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControlWizard.TabIndex = 0;
            // 
            // tabPageSelectFiles
            // 
            this.tabPageSelectFiles.Controls.Add(this.labelFilesSelectCount);
            this.tabPageSelectFiles.Controls.Add(this.groupBoxStepOne);
            this.tabPageSelectFiles.Controls.Add(this.toolStripSelectFiles);
            this.tabPageSelectFiles.Controls.Add(this.buttonGoToRegionLanguages);
            this.tabPageSelectFiles.Controls.Add(this.groupBoxSelectedSupportFiles);
            this.tabPageSelectFiles.Location = new System.Drawing.Point(4, 5);
            this.tabPageSelectFiles.Name = "tabPageSelectFiles";
            this.tabPageSelectFiles.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageSelectFiles.Size = new System.Drawing.Size(730, 674);
            this.tabPageSelectFiles.TabIndex = 0;
            this.tabPageSelectFiles.Text = "tabPage1";
            this.tabPageSelectFiles.UseVisualStyleBackColor = true;
            // 
            // labelFilesSelectCount
            // 
            this.labelFilesSelectCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelFilesSelectCount.AutoSize = true;
            this.labelFilesSelectCount.Location = new System.Drawing.Point(8, 624);
            this.labelFilesSelectCount.Name = "labelFilesSelectCount";
            this.labelFilesSelectCount.Size = new System.Drawing.Size(77, 13);
            this.labelFilesSelectCount.TabIndex = 7;
            this.labelFilesSelectCount.Text = "0 files selected";
            // 
            // groupBoxStepOne
            // 
            this.groupBoxStepOne.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxStepOne.Controls.Add(this.labelSelectFilesInstructions);
            this.groupBoxStepOne.Location = new System.Drawing.Point(8, 31);
            this.groupBoxStepOne.Name = "groupBoxStepOne";
            this.groupBoxStepOne.Size = new System.Drawing.Size(714, 56);
            this.groupBoxStepOne.TabIndex = 6;
            this.groupBoxStepOne.TabStop = false;
            this.groupBoxStepOne.Text = "Step One";
            // 
            // labelSelectFilesInstructions
            // 
            this.labelSelectFilesInstructions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSelectFilesInstructions.Location = new System.Drawing.Point(3, 16);
            this.labelSelectFilesInstructions.Name = "labelSelectFilesInstructions";
            this.labelSelectFilesInstructions.Size = new System.Drawing.Size(708, 37);
            this.labelSelectFilesInstructions.TabIndex = 0;
            this.labelSelectFilesInstructions.Text = resources.GetString("labelSelectFilesInstructions.Text");
            // 
            // toolStripSelectFiles
            // 
            this.toolStripSelectFiles.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripSelectFiles.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonSelectFiles,
            this.toolStripButtonRemoveFiles});
            this.toolStripSelectFiles.Location = new System.Drawing.Point(3, 3);
            this.toolStripSelectFiles.Name = "toolStripSelectFiles";
            this.toolStripSelectFiles.Size = new System.Drawing.Size(724, 25);
            this.toolStripSelectFiles.TabIndex = 5;
            this.toolStripSelectFiles.Text = "toolStrip1";
            // 
            // toolStripButtonSelectFiles
            // 
            this.toolStripButtonSelectFiles.Image = global::Admin.Properties.Resources.FileNewPNG;
            this.toolStripButtonSelectFiles.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonSelectFiles.Name = "toolStripButtonSelectFiles";
            this.toolStripButtonSelectFiles.Size = new System.Drawing.Size(80, 22);
            this.toolStripButtonSelectFiles.Text = "Add files...";
            this.toolStripButtonSelectFiles.ToolTipText = "Opens a dialog box to select files";
            this.toolStripButtonSelectFiles.Click += new System.EventHandler(this.toolStripButtonSelectFiles_Click);
            // 
            // toolStripButtonRemoveFiles
            // 
            this.toolStripButtonRemoveFiles.Image = global::Admin.Properties.Resources.FileDeletePNG;
            this.toolStripButtonRemoveFiles.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonRemoveFiles.Name = "toolStripButtonRemoveFiles";
            this.toolStripButtonRemoveFiles.Size = new System.Drawing.Size(88, 22);
            this.toolStripButtonRemoveFiles.Text = "Remove files";
            this.toolStripButtonRemoveFiles.ToolTipText = "Removes selected file(s)";
            this.toolStripButtonRemoveFiles.Click += new System.EventHandler(this.toolStripButtonRemoveFiles_Click);
            // 
            // buttonGoToRegionLanguages
            // 
            this.buttonGoToRegionLanguages.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonGoToRegionLanguages.Image = global::Admin.Properties.Resources.ForwardPNG;
            this.buttonGoToRegionLanguages.Location = new System.Drawing.Point(620, 624);
            this.buttonGoToRegionLanguages.Name = "buttonGoToRegionLanguages";
            this.buttonGoToRegionLanguages.Size = new System.Drawing.Size(102, 42);
            this.buttonGoToRegionLanguages.TabIndex = 4;
            this.buttonGoToRegionLanguages.Text = "Continue";
            this.buttonGoToRegionLanguages.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.buttonGoToRegionLanguages.UseVisualStyleBackColor = true;
            this.buttonGoToRegionLanguages.Click += new System.EventHandler(this.buttonGoToRegionLanguages_Click);
            // 
            // groupBoxSelectedSupportFiles
            // 
            this.groupBoxSelectedSupportFiles.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxSelectedSupportFiles.Controls.Add(this.listViewSupportFiles);
            this.groupBoxSelectedSupportFiles.Location = new System.Drawing.Point(8, 93);
            this.groupBoxSelectedSupportFiles.Name = "groupBoxSelectedSupportFiles";
            this.groupBoxSelectedSupportFiles.Size = new System.Drawing.Size(714, 525);
            this.groupBoxSelectedSupportFiles.TabIndex = 2;
            this.groupBoxSelectedSupportFiles.TabStop = false;
            this.groupBoxSelectedSupportFiles.Text = "Support Files to Upload";
            // 
            // listViewSupportFiles
            // 
            this.listViewSupportFiles.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderFileName,
            this.columnHeaderFilePath});
            this.listViewSupportFiles.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listViewSupportFiles.FullRowSelect = true;
            this.listViewSupportFiles.Location = new System.Drawing.Point(3, 16);
            this.listViewSupportFiles.Name = "listViewSupportFiles";
            this.listViewSupportFiles.Size = new System.Drawing.Size(708, 506);
            this.listViewSupportFiles.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.listViewSupportFiles.TabIndex = 1;
            this.listViewSupportFiles.UseCompatibleStateImageBehavior = false;
            this.listViewSupportFiles.View = System.Windows.Forms.View.Details;
            // 
            // columnHeaderFileName
            // 
            this.columnHeaderFileName.Text = "File Name";
            this.columnHeaderFileName.Width = 175;
            // 
            // columnHeaderFilePath
            // 
            this.columnHeaderFilePath.Text = "File Path";
            this.columnHeaderFilePath.Width = 500;
            // 
            // tabPageRegionLanguages
            // 
            this.tabPageRegionLanguages.Controls.Add(this.groupBoxStepTwo);
            this.tabPageRegionLanguages.Controls.Add(this.groupBoxRegionLanguages);
            this.tabPageRegionLanguages.Controls.Add(this.buttonGoToUpload);
            this.tabPageRegionLanguages.Location = new System.Drawing.Point(4, 5);
            this.tabPageRegionLanguages.Name = "tabPageRegionLanguages";
            this.tabPageRegionLanguages.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageRegionLanguages.Size = new System.Drawing.Size(730, 674);
            this.tabPageRegionLanguages.TabIndex = 1;
            this.tabPageRegionLanguages.Text = "tabPage2";
            this.tabPageRegionLanguages.UseVisualStyleBackColor = true;
            // 
            // groupBoxStepTwo
            // 
            this.groupBoxStepTwo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxStepTwo.Controls.Add(this.label1);
            this.groupBoxStepTwo.Location = new System.Drawing.Point(8, 7);
            this.groupBoxStepTwo.Name = "groupBoxStepTwo";
            this.groupBoxStepTwo.Size = new System.Drawing.Size(714, 56);
            this.groupBoxStepTwo.TabIndex = 7;
            this.groupBoxStepTwo.TabStop = false;
            this.groupBoxStepTwo.Text = "Step Two";
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(3, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(708, 37);
            this.label1.TabIndex = 0;
            this.label1.Text = "Check the Region-Lanuage(s) you wish to associate with the uploaded files.  \r\nThe" +
                " checked region-language(s) will be associated with all files uploaded.";
            // 
            // groupBoxRegionLanguages
            // 
            this.groupBoxRegionLanguages.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxRegionLanguages.Controls.Add(this.regionLanguageGroupGridForSupportFiles);
            this.groupBoxRegionLanguages.Location = new System.Drawing.Point(8, 69);
            this.groupBoxRegionLanguages.Name = "groupBoxRegionLanguages";
            this.groupBoxRegionLanguages.Size = new System.Drawing.Size(714, 549);
            this.groupBoxRegionLanguages.TabIndex = 6;
            this.groupBoxRegionLanguages.TabStop = false;
            this.groupBoxRegionLanguages.Text = "Region Languages";
            // 
            // regionLanguageGroupGridForSupportFiles
            // 
            this.regionLanguageGroupGridForSupportFiles.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.regionLanguageGroupGridForSupportFiles.Dock = System.Windows.Forms.DockStyle.Fill;
            this.regionLanguageGroupGridForSupportFiles.Location = new System.Drawing.Point(3, 16);
            this.regionLanguageGroupGridForSupportFiles.Name = "regionLanguageGroupGridForSupportFiles";
            this.regionLanguageGroupGridForSupportFiles.ShowControlBox = false;
            this.regionLanguageGroupGridForSupportFiles.Size = new System.Drawing.Size(708, 530);
            this.regionLanguageGroupGridForSupportFiles.TabIndex = 0;
            // 
            // buttonGoToUpload
            // 
            this.buttonGoToUpload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonGoToUpload.Image = global::Admin.Properties.Resources.ForwardPNG;
            this.buttonGoToUpload.Location = new System.Drawing.Point(617, 624);
            this.buttonGoToUpload.Name = "buttonGoToUpload";
            this.buttonGoToUpload.Size = new System.Drawing.Size(102, 42);
            this.buttonGoToUpload.TabIndex = 5;
            this.buttonGoToUpload.Text = "Continue";
            this.buttonGoToUpload.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.buttonGoToUpload.UseVisualStyleBackColor = true;
            this.buttonGoToUpload.Click += new System.EventHandler(this.buttonGoToUpload_Click);
            // 
            // tabPageUploadProgress
            // 
            this.tabPageUploadProgress.Controls.Add(this.buttonFinished);
            this.tabPageUploadProgress.Controls.Add(this.buttonUploadAnotherBatch);
            this.tabPageUploadProgress.Controls.Add(this.groupBoxStepThree);
            this.tabPageUploadProgress.Controls.Add(this.buttonUpload);
            this.tabPageUploadProgress.Controls.Add(this.groupBoxUploadProgress);
            this.tabPageUploadProgress.Location = new System.Drawing.Point(4, 5);
            this.tabPageUploadProgress.Name = "tabPageUploadProgress";
            this.tabPageUploadProgress.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageUploadProgress.Size = new System.Drawing.Size(730, 674);
            this.tabPageUploadProgress.TabIndex = 2;
            this.tabPageUploadProgress.Text = "tabPage1";
            this.tabPageUploadProgress.UseVisualStyleBackColor = true;
            // 
            // buttonFinished
            // 
            this.buttonFinished.Image = global::Admin.Properties.Resources.StopPNG;
            this.buttonFinished.Location = new System.Drawing.Point(250, 69);
            this.buttonFinished.Name = "buttonFinished";
            this.buttonFinished.Size = new System.Drawing.Size(115, 42);
            this.buttonFinished.TabIndex = 11;
            this.buttonFinished.Text = "Finished";
            this.buttonFinished.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.buttonFinished.UseVisualStyleBackColor = true;
            this.buttonFinished.Visible = false;
            this.buttonFinished.Click += new System.EventHandler(this.buttonFinished_Click);
            // 
            // buttonUploadAnotherBatch
            // 
            this.buttonUploadAnotherBatch.Image = global::Admin.Properties.Resources.ReloadPNG;
            this.buttonUploadAnotherBatch.Location = new System.Drawing.Point(129, 69);
            this.buttonUploadAnotherBatch.Name = "buttonUploadAnotherBatch";
            this.buttonUploadAnotherBatch.Size = new System.Drawing.Size(115, 42);
            this.buttonUploadAnotherBatch.TabIndex = 10;
            this.buttonUploadAnotherBatch.Text = "Upload another batch";
            this.buttonUploadAnotherBatch.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.buttonUploadAnotherBatch.UseVisualStyleBackColor = true;
            this.buttonUploadAnotherBatch.Visible = false;
            this.buttonUploadAnotherBatch.Click += new System.EventHandler(this.buttonUploadAnotherBatch_Click);
            // 
            // groupBoxStepThree
            // 
            this.groupBoxStepThree.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxStepThree.Controls.Add(this.label2);
            this.groupBoxStepThree.Location = new System.Drawing.Point(8, 7);
            this.groupBoxStepThree.Name = "groupBoxStepThree";
            this.groupBoxStepThree.Size = new System.Drawing.Size(714, 56);
            this.groupBoxStepThree.TabIndex = 9;
            this.groupBoxStepThree.TabStop = false;
            this.groupBoxStepThree.Text = "Step Three";
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(3, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(708, 37);
            this.label2.TabIndex = 0;
            this.label2.Text = "Click the \'Upload\' button to begin uploading support files.  You may view the pro" +
                "gress of each file in the \'Upload Progress\' window.";
            // 
            // buttonUpload
            // 
            this.buttonUpload.Image = global::Admin.Properties.Resources.UploadBigPNG;
            this.buttonUpload.Location = new System.Drawing.Point(8, 69);
            this.buttonUpload.Name = "buttonUpload";
            this.buttonUpload.Size = new System.Drawing.Size(115, 42);
            this.buttonUpload.TabIndex = 8;
            this.buttonUpload.Text = "Upload";
            this.buttonUpload.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.buttonUpload.UseVisualStyleBackColor = true;
            this.buttonUpload.Click += new System.EventHandler(this.buttonUpload_Click);
            // 
            // groupBoxUploadProgress
            // 
            this.groupBoxUploadProgress.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxUploadProgress.Controls.Add(this.statusReportUpload);
            this.groupBoxUploadProgress.Location = new System.Drawing.Point(8, 117);
            this.groupBoxUploadProgress.Name = "groupBoxUploadProgress";
            this.groupBoxUploadProgress.Size = new System.Drawing.Size(714, 549);
            this.groupBoxUploadProgress.TabIndex = 7;
            this.groupBoxUploadProgress.TabStop = false;
            this.groupBoxUploadProgress.Text = "Upload Progress";
            // 
            // statusReportUpload
            // 
            this.statusReportUpload.Dock = System.Windows.Forms.DockStyle.Fill;
            this.statusReportUpload.Location = new System.Drawing.Point(3, 16);
            this.statusReportUpload.Name = "statusReportUpload";
            this.statusReportUpload.Size = new System.Drawing.Size(708, 530);
            this.statusReportUpload.TabIndex = 0;
            this.statusReportUpload.Visible = false;
            // 
            // imageListProgressImages
            // 
            this.imageListProgressImages.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListProgressImages.ImageStream")));
            this.imageListProgressImages.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListProgressImages.Images.SetKeyName(0, "error_sm.gif");
            this.imageListProgressImages.Images.SetKeyName(1, "success_sm.gif");
            this.imageListProgressImages.Images.SetKeyName(2, "updating_sm.gif");
            this.imageListProgressImages.Images.SetKeyName(3, "warning_sm.gif");
            // 
            // backgroundWorkerUpload
            // 
            this.backgroundWorkerUpload.WorkerReportsProgress = true;
            this.backgroundWorkerUpload.WorkerSupportsCancellation = true;
            this.backgroundWorkerUpload.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerUpload_DoWork);
            this.backgroundWorkerUpload.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerUpload_RunWorkerCompleted);
            this.backgroundWorkerUpload.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorkerUpload_ProgressChanged);
            // 
            // SupportFileBatchUpload
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(738, 683);
            this.Controls.Add(this.tabControlWizard);
            this.Name = "SupportFileBatchUpload";
            this.Text = "Support File Batch Upload";
            this.tabControlWizard.ResumeLayout(false);
            this.tabPageSelectFiles.ResumeLayout(false);
            this.tabPageSelectFiles.PerformLayout();
            this.groupBoxStepOne.ResumeLayout(false);
            this.toolStripSelectFiles.ResumeLayout(false);
            this.toolStripSelectFiles.PerformLayout();
            this.groupBoxSelectedSupportFiles.ResumeLayout(false);
            this.tabPageRegionLanguages.ResumeLayout(false);
            this.groupBoxStepTwo.ResumeLayout(false);
            this.groupBoxRegionLanguages.ResumeLayout(false);
            this.tabPageUploadProgress.ResumeLayout(false);
            this.groupBoxStepThree.ResumeLayout(false);
            this.groupBoxUploadProgress.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControlWizard;
        private System.Windows.Forms.TabPage tabPageSelectFiles;
        private System.Windows.Forms.TabPage tabPageRegionLanguages;
        private System.Windows.Forms.ListView listViewSupportFiles;
        private System.Windows.Forms.GroupBox groupBoxSelectedSupportFiles;
        private System.Windows.Forms.ColumnHeader columnHeaderFileName;
        private System.Windows.Forms.ColumnHeader columnHeaderFilePath;
        private System.Windows.Forms.Button buttonGoToRegionLanguages;
        private Admin.UserControls.RegionLanguageGroupGrid regionLanguageGroupGridForSupportFiles;
        private System.Windows.Forms.Button buttonGoToUpload;
        private System.Windows.Forms.TabPage tabPageUploadProgress;
        private System.Windows.Forms.ToolStrip toolStripSelectFiles;
        private System.Windows.Forms.ToolStripButton toolStripButtonSelectFiles;
        private System.Windows.Forms.ToolStripButton toolStripButtonRemoveFiles;
        private System.Windows.Forms.GroupBox groupBoxRegionLanguages;
        private System.Windows.Forms.GroupBox groupBoxStepOne;
        private System.Windows.Forms.Label labelSelectFilesInstructions;
        private System.Windows.Forms.GroupBox groupBoxStepTwo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBoxStepThree;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonUpload;
        private System.Windows.Forms.GroupBox groupBoxUploadProgress;
        private System.Windows.Forms.ImageList imageListProgressImages;
        private Admin.UserControls.StatusReport statusReportUpload;
        private System.ComponentModel.BackgroundWorker backgroundWorkerUpload;
        private System.Windows.Forms.Label labelFilesSelectCount;
        private System.Windows.Forms.Button buttonFinished;
        private System.Windows.Forms.Button buttonUploadAnotherBatch;
    }
}