﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Admin
{
    public partial class AnnouncementAssetManagement : Form
    {
        private byte[] loadedFile;
        private string fileName;
        private DataTable fileInfo;
        private Admin.wsIpgAdmin.Admin adminService;

        public AnnouncementAssetManagement()
        {
            adminService = new Admin.wsIpgAdmin.Admin();
            adminService.SaveAnnouncementAssetCompleted += new Admin.wsIpgAdmin.SaveAnnouncementAssetCompletedEventHandler(adminService_SaveAnnouncementAssetCompleted);
            adminService.DeleteAnnounementAssetsCompleted += new Admin.wsIpgAdmin.DeleteAnnounementAssetsCompletedEventHandler(adminService_DeleteAnnounementAssetsCompleted);
            adminService.GetAnnouncementAssetFileListCompleted += new Admin.wsIpgAdmin.GetAnnouncementAssetFileListCompletedEventHandler(adminService_GetAnnouncementAssetFileListCompleted);
            InitializeComponent();
        }

        //UI Handlers
        private void btnShowOpenFileDialog_Click(object sender, EventArgs e)
        {
            btnUploadFile.Enabled = false;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                loadedFile = LoadFile(openFileDialog1.FileName);
                fileName = openFileDialog1.FileName.Substring(openFileDialog1.FileName.LastIndexOf("\\") + 1);
                txtFile.Text = fileName;
            }

            btnUploadFile.Enabled = true;
        }

        private void btnUploadFile_Click(object sender, EventArgs e)
        {
            lblResult.Text = "Uploading File...";
            adminService.SaveAnnouncementAssetAsync(loadedFile, fileName);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            List<string> filesToDelete = GetFilesToDelete();
            adminService.DeleteAnnounementAssetsAsync(filesToDelete.ToArray());
        }

        private void AnnouncementAssetManagement_Load(object sender, EventArgs e)
        {
            RefreshFileData();
        }
        //Web Service Handlers
        private void adminService_GetAnnouncementAssetFileListCompleted(object sender, Admin.wsIpgAdmin.GetAnnouncementAssetFileListCompletedEventArgs e)
        {
            fileInfo = e.Result;
            RefreshGrid();
        }

        private void adminService_SaveAnnouncementAssetCompleted(object sender, Admin.wsIpgAdmin.SaveAnnouncementAssetCompletedEventArgs e)
        {
            if (e.Result.Equals("fail"))
                lblResult.Text = "Upload Failed";
            else
                lblResult.Text = "Upload Complete";
            RefreshFileData();
        }

        private void adminService_DeleteAnnounementAssetsCompleted(object sender, Admin.wsIpgAdmin.DeleteAnnounementAssetsCompletedEventArgs e)
        {
            if (e.Result)
            {
                RefreshFileData();
                lblDeleteResult.Text = "Delete Succeeded.";
            }
            else
                lblDeleteResult.Text = "Delete Failed.";
        }
        //Helper Methods
        private byte[] LoadFile(string filePath)
        {
            BinaryReader binReader = new BinaryReader(File.Open(filePath, FileMode.Open, FileAccess.Read));
            binReader.BaseStream.Position = 0;
            byte[] binFile = binReader.ReadBytes(Convert.ToInt32(binReader.BaseStream.Length));
            binReader.Close();
            return binFile;
        }

        private void RefreshFileData()
        {
            adminService.GetAnnouncementAssetFileListAsync();
        }

        private void RefreshGrid()
        {
            dgvFiles.Rows.Clear();
            foreach (DataRow tableRow in fileInfo.Rows)
            {
                int rowIndex = dgvFiles.Rows.Add();
                dgvFiles.Rows[rowIndex].Cells["colDelete"].Value = false;
                dgvFiles.Rows[rowIndex].Cells["colFileName"].Value = tableRow["FileName"];
                dgvFiles.Rows[rowIndex].Cells["colRelativePath"].Value = tableRow["RelativePath"];
                dgvFiles.Rows[rowIndex].Cells["colCreatedDate"].Value = tableRow["CreatedDate"];
                dgvFiles.Rows[rowIndex].Tag = tableRow;
            }
        }

        private List<string> GetFilesToDelete()
        {
            List<string> deleteList = new List<string>();
            foreach (DataGridViewRow row in dgvFiles.Rows)
            {
                if ((bool)row.Cells[0].FormattedValue)
                {
                    DataRow tableRow = row.Tag as DataRow;
                    deleteList.Add(tableRow["FileName"].ToString());
                }
            }
            return deleteList;
        }
    }
}
