using System;
using System.Collections.Generic;
using System.Text;

namespace HpDm.BLL
{
    public class FileUploader
    {
        #region class variables

        private System.ComponentModel.BackgroundWorker _UploadFilesWorker;
        private List<string> _FileNames;
        private List<string> _FailedUploadFiles;
        private string _FileRecieverURI;

        #endregion

        #region Properties

        private bool _IsBusy;

        /// <summary>
        /// (Read-only) Gets a value indicating whether this instance is busy.
        /// </summary>
        /// <value><c>true</c> if this instance is busy; otherwise, <c>false</c>.</value>
        public bool IsBusy
        {
            get { return _IsBusy; }
            private set { _IsBusy = value; }
        }

        private bool _UploaderSupportsProgress;

        /// <summary>
        /// Gets or sets a value indicating whether [uploader supports progress].
        /// </summary>
        /// <value>
        /// 	<c>true</c> if [uploader supports progress]; otherwise, <c>false</c>.
        /// </value>
        public bool UploaderSupportsProgress
        {
            get { return _UploaderSupportsProgress; }
            private set { _UploaderSupportsProgress = value; }
        }


        #endregion

        #region constructor and public methods

        public FileUploader()
        {
            IsBusy = false;
            UploaderSupportsProgress = false;
        }

        /// <summary>
        /// Uploads the slide files async.
        /// </summary>
        /// <param name="fileNames">The file names.</param>
        /// <param name="fileRecieverURI">The URI that recieves the file and handles the upload. (Hint: probably an aspx)</param>
        public void UploadSlideFilesAsync(System.Collections.Generic.List<string> fileNames, string fileRecieverURI)
        {
            PreparePrivateMembers(fileNames, fileRecieverURI);
        }

        /// <summary>
        /// Uploads the slide files async.
        /// </summary>
        /// <param name="fileNames">The file names.</param>
        /// <param name="fileRecieverURI">The URI that recieves the file and handles the upload. (Hint: probably an aspx)</param>
        public void UploadSlideFilesAsync(System.Collections.Generic.List<string> fileNames, string fileRecieverURI, bool uploaderSupportsProgress)
        {
            UploaderSupportsProgress = uploaderSupportsProgress;
            PreparePrivateMembers(fileNames, fileRecieverURI);
        }

        private void PreparePrivateMembers(System.Collections.Generic.List<string> fileNames, string fileRecieverURI)
        {
            IsBusy = true;
            _FileNames = fileNames;
            _FileRecieverURI = fileRecieverURI;
            _UploadFilesWorker = new System.ComponentModel.BackgroundWorker();
            //if we don't want progress then don't bother with this stuff
            if (UploaderSupportsProgress)
            {
                _UploadFilesWorker.WorkerReportsProgress = true;
                _UploadFilesWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(_UploadFilesWorker_ProgressChanged); 
            }
            _UploadFilesWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(_UploadFilesWorker_RunWorkerCompleted);
            _UploadFilesWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(_UploadFilesWorker_DoWork);
            _UploadFilesWorker.RunWorkerAsync();
        } 

        #endregion

        #region private methods

        /// <summary>
        /// Uploads the slide file.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        private void UploadSlideFile(string fileName)
        {
            System.Net.WebClient wc = new System.Net.WebClient();
            wc.UploadFile(_FileRecieverURI, fileName);
        }

        /// <summary>
        /// Handles the DoWork event of the _UploadFilesWorker control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.DoWorkEventArgs"/> instance containing the event data.</param>
        private void _UploadFilesWorker_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            _FailedUploadFiles = new List<string>();
            int filesUploaded = 0;
            double percentProcessed = 0.0;
            foreach (string slideFileName in _FileNames)
            {
                //try to upload the file
                //if something bad happens, keep track of which file couldn't be uploaded
                try
                {
                    UploadSlideFile(slideFileName);
                }
                catch (Exception ex)
                {

                    _FailedUploadFiles.Add(slideFileName);
                }

                if (UploaderSupportsProgress)
                {
                    //get progress info and send it
                    filesUploaded++;
                    percentProcessed = (Convert.ToDouble(filesUploaded) / Convert.ToDouble(_FileNames.Count)) * 100.00;
                    _UploadFilesWorker.ReportProgress(Convert.ToInt32(percentProcessed), String.Format("Uploading {0} of {1} files", filesUploaded, _FileNames.Count.ToString()));
                }
            }
        }

        /// <summary>
        /// Handles the RunWorkerCompleted event of the _UploadFilesWorker control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.RunWorkerCompletedEventArgs"/> instance containing the event data.</param>
        private void _UploadFilesWorker_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            IsBusy = false;
            UploadSlideFilesCompleted(_FailedUploadFiles);
        }

        /// <summary>
        /// Handles the ProgressChanged event of the _UploadFilesWorker control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.ProgressChangedEventArgs"/> instance containing the event data.</param>
        private void _UploadFilesWorker_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            //this 'if' will save your butt if you forget to create the event handler in the consuming class
            if (UploadSlideFilesProgressReported != null)
            {
                UploadSlideFilesProgressReported(e.ProgressPercentage, e.UserState.ToString());  
            }
        } 

        #endregion

        #region events and event-delegates

        /// <summary>
        /// Notifies the consuming object of upload progress.
        /// </summary>
        public event UploadSlideFilesProgressReportedDelegate UploadSlideFilesProgressReported;
        /// <summary>
        /// Notifies the consuming object of upload progress. (Delegate)
        /// </summary>
        /// <param name="progressReported">int The percentage of progress.</param>
        /// <param name="message">string Progress message</param>
        public delegate void UploadSlideFilesProgressReportedDelegate(int progressReported, string message);


        /// <summary>
        /// Notifies the consuming object that the upload has been completed.
        /// </summary>
        public event UploadSlideFilesCompletedDelegate UploadSlideFilesCompleted;
        /// <summary>
        /// Notifies the consuming object that the upload has been completed. (Delegate)
        /// </summary>
        /// <param name="failedUploadFiles">The files that failed to upload</param>
        public delegate void UploadSlideFilesCompletedDelegate(List<string> failedUploadFiles);  

        #endregion

    }

    




    

}
