﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Admin.BLL
{

    public partial class SlideRemoveContainer : UserControl
    {
        public event SlideRemoveContainerRemoveFromCategoryEventHandler RemoveFromCategory;
        public event SlideRemoveContainerClearSlidesEventHandler ClearSlides;
        public event SlideRemoveContainerRemoveFromListEventHandler RemoveFromList;
        public event SlideRemoveContainerCloseContainerEventHandler CloseContainer;

        public SlideRemoveContainer()
        {
            InitializeComponent();
            lstSlidesToRemove.DisplayMember = "Title";
        }

        private void Draw(List<SlideRemoveTransport> slides)
        {
            if (slides.Count.Equals(0))
                lblCategory.Text = "";
            else
                lblCategory.Text = slides[0].CategoryRow.Category;

            foreach (SlideRemoveTransport row in slides)
                lstSlidesToRemove.Items.Add(row);
        }

        public void Clear()
        {
            lstSlidesToRemove.Items.Clear();
        }

        public void Refresh(List<SlideRemoveTransport> slides)
        {
            Clear();
            Draw(slides);
        }

        private void btnRemoveFromList_Click(object sender, EventArgs e)
        {
            if (lstSlidesToRemove.SelectedItem != null)
            {
                SlideRemoveTransport slide = (SlideRemoveTransport)lstSlidesToRemove.SelectedItem;
                OnRemoveFromList(new SlideRemoveContainerRemoveFromListSlideEventArgs(slide));
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            OnClearSlides(new SlideRemoveContainerClearSlidesEventArgs());
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            OnCloseContainer(new SlideRemoveContainerCloseContainerEventArgs());
        }

        protected virtual void OnRemoveFromCategory(SlideRemoveContainerRemoveFromCategorySlidesEventArgs e)
        {
            RemoveFromCategory(this, e);
        }

        protected virtual void OnClearSlides(SlideRemoveContainerClearSlidesEventArgs e)
        {
            ClearSlides(this, e);
        }

        protected virtual void OnRemoveFromList(SlideRemoveContainerRemoveFromListSlideEventArgs e)
        {
            RemoveFromList(this, e);
        }

        protected virtual void OnCloseContainer(SlideRemoveContainerCloseContainerEventArgs e)
        {
            CloseContainer(this, e);
        }

        private void btnRemoveFromCategory_Click(object sender, EventArgs e)
        {
            OnRemoveFromCategory(new SlideRemoveContainerRemoveFromCategorySlidesEventArgs());
        }

    }

    public delegate void SlideRemoveContainerRemoveFromCategoryEventHandler(object sender, SlideRemoveContainerRemoveFromCategorySlidesEventArgs e);
    public delegate void SlideRemoveContainerClearSlidesEventHandler(object sender, SlideRemoveContainerClearSlidesEventArgs e);
    public delegate void SlideRemoveContainerRemoveFromListEventHandler(object sender, SlideRemoveContainerRemoveFromListSlideEventArgs e);
    public delegate void SlideRemoveContainerCloseContainerEventHandler(object sender, SlideRemoveContainerCloseContainerEventArgs e);
    public class SlideRemoveContainerRemoveFromCategorySlidesEventArgs : EventArgs
    {
    }
    public class SlideRemoveContainerClearSlidesEventArgs : EventArgs
    {
    }
    public class SlideRemoveContainerRemoveFromListSlideEventArgs : EventArgs
    {
        private SlideRemoveTransport slideRemoveTransport;
        public SlideRemoveContainerRemoveFromListSlideEventArgs(SlideRemoveTransport slideRemoveTransport)
        {
            this.slideRemoveTransport = slideRemoveTransport;
        }
        public SlideRemoveTransport SlideRemoveTransport
        {
            get
            {
                return this.slideRemoveTransport;
            }
        }
    }
    public class SlideRemoveContainerCloseContainerEventArgs : EventArgs
    {
    }
}
