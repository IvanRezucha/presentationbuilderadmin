using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Data;
using System.Drawing;

namespace Admin
{
    public delegate void TreeViewEntryNodeDeleteEventHandler(object sender, TreeViewEntryNodeDeleteEventArgs e);
    public delegate void TreeViewEntryNodeRemoveEventHandler(object sender, TreeViewEntryNodeRemoveEventArgs e);

	class CategorySlideTreeView : Wirestone.WinForms.Controls.CategoryEntryTreeview.CategoryEntryTreeView
	{
		#region Variables

		private TreeNode _RightClickNode;
		private Type _CategoryTableRowType;
		private AdminMain _AdminMainForm;

		#endregion

		#region Properties

		private TreeNode RightClickNode
		{
			get { return _RightClickNode; }
			set { _RightClickNode = value; }
		}
		private Type CategoryTableRowType
		{
			get { return _CategoryTableRowType; }
			set { _CategoryTableRowType = value; }
		}
		private AdminMain AdminMainForm
		{
			get { return _AdminMainForm; }
			set { _AdminMainForm = value; }
		}

        private ContextMenuStrip _CategoryContextMenu;

        public ContextMenuStrip CategoryContextMenu
        {
            get { return _CategoryContextMenu; }
            set { _CategoryContextMenu = value; }
        }

        private ContextMenuStrip _SlideContextMenu;

        public ContextMenuStrip SlideContextMenu
        {
            get { return _SlideContextMenu; }
            set { _SlideContextMenu = value; }
        }

		#endregion

        public event TreeViewEntryNodeDeleteEventHandler TreeViewEntryNodeDeleteClick;
        public event TreeViewEntryNodeRemoveEventHandler TreeViewEntryNodeRemoveClick;

		public CategorySlideTreeView() : base()
		{
			// Hook events
			EntryNodeLeftClick += new EntryNodeLeftClickDelegate(OnEntryNodeLeftClick);
			EntryNodeRightClick += new EntryNodeRightClickDelegate(OnEntryNodeRightClick);
            CategoryNodeRightClick += new CategoryNodeRightClickDelegate(OnCategoryNodeRightClick);
			ItemDrag += new ItemDragEventHandler(CategorySlideTreeView_ItemDrag);
			CategoryNodeDragDrop += new CategoryNodeDragDropDelegate(OnCategoryNodeDragDrop);
			EntryNodeDragDrop += new EntryNodeDragDropDelegate(OnEntryNodeDragDrop);
			DragEnter += new DragEventHandler(CategorySlideTreeView_DragEnter);
			CategoryNodeDragOver += new CategoryNodeDragOverDelegate(OnCategoryNodeDragOver);
			EntryNodeDragOver += new EntryNodeDragOverDelegate(OnEntryNodeDragOver);
			AfterExpand += new TreeViewEventHandler(CategorySlideTreeView_AfterExpand);
            
		}

		public void FillCategorySlideData(wsIpgAdmin.SlideLibrary dsSlideLibrary)
		{
            this.FillWithImages(dsSlideLibrary.Category, "CategoryID", "Category", typeof(wsIpgAdmin.SlideLibrary.CategoryRow), "ParentCategoryID",
                dsSlideLibrary.Slide, "SlideID", "Title",
                dsSlideLibrary.SlideCategory, typeof(wsIpgAdmin.SlideLibrary.SlideCategoryRow),
                "Icon", true, "single-slide.ico", false);
           
		    // Get the current instance of the AdminMain
			AdminMainForm = ((AdminMain)this.FindForm());
            
            ReselectTreeNode();
		}

		/// <summary>
		/// Determins if a node is a leaf node
		/// Leaf Node:  a node that doesn't have any decendants
		/// </summary>
		/// <param name="node">node to check</param>
		/// <returns>true/false</returns>
		private bool IsLeafNode(TreeNode node)
		{
			if (node.Nodes.Count == 0)
			{
				return true;
			}
			else
			{
				foreach (TreeNode childNode in node.Nodes)
				{
					string childNodeType = AdminMainForm.GetNodeType(childNode);
					if (childNodeType != "Slide Category")
					{
						//node has a child that is not a slide category, so it is not a leaf
						return false;
					}
				}
				return true;
			}
		}

		private void ShowDropOnFileDialog(TreeNode droppedOnNode, DragEventArgs e)
		{
            // *SS Filter troubleshooting comment - This is called from OnCategoryNodeDragDrop 
            //MessageBox.Show(String.Format("IN ShowDropOnFileDialog #5"));

			int destinationCategory = ((wsIpgAdmin.SlideLibrary.CategoryRow)droppedOnNode.Tag).CategoryID;
			int regLangId = ((wsIpgAdmin.SlideLibrary.RegionLanguageRow)this.Tag).RegionLanguageID;
			int sortOrder;
			List<string> files = new List<string>((string[])e.Data.GetData(DataFormats.FileDrop));
            files.Sort();
            //***SS Added the lines below as a test
            //MessageBox.Show(String.Format("Before 'new SlideFileDropDialog' #6"));

			SlideFileDropDialog slideFileDropDialog = new SlideFileDropDialog(files, regLangId);

            //***SS Added the lines below as a test
            //MessageBox.Show(String.Format("After 'new SlideFileDropDialog' but before Dialog Result #7"));

			DialogResult result = slideFileDropDialog.ShowDialog(AdminMainForm);

            //***SS Added the lines below as a test
            //MessageBox.Show(String.Format("After Dialog Result #8"));

			if (result == DialogResult.OK)
			{
				AdminMainForm.ReselectNode = droppedOnNode;
				AdminMainForm.ReselectSlideID = slideFileDropDialog.SlideIDs[0];
				AdminMainForm.GetSlideResortItems();

				//we've dragged a slide to a valid category, add it to the category
				wsIpgAdmin.Admin admin = new Admin.wsIpgAdmin.Admin();

				sortOrder = droppedOnNode.Nodes.Count;
				admin.SlideMultipleCategorization(slideFileDropDialog.SlideIDs.ToArray(), destinationCategory, -1, sortOrder, regLangId);

				// Refresh the slide list
				if (AdminMainForm.IsDataFiltered)
				{
					AdminMainForm.RefreshFilteredData();
				}
				else
				{
					AdminMainForm.RefreshData();
				}
			}
		}

		protected override void OnEntryNodeLeftClick(TreeNodeMouseClickEventArgs e, object entryRow)
		{
            //// Take the selected datagrid view row and load it in to the browser.
            //if (GetNodeAt(e.X, e.Y) != null)
            //{
            //    Admin.wsIpgAdmin.SlideLibrary.SlideCategoryRow slideCat = (Admin.wsIpgAdmin.SlideLibrary.SlideCategoryRow)entryRow;
            //    AdminMainForm.LoadSlideToBrowser(((wsIpgAdmin.SlideLibrary.SlideDataTable)this.TreeData.EntryTable).FindBySlideID(slideCat.SlideID).FileName);
            //}

            if (Control.ModifierKeys == Keys.Control)
            {
                wsIpgAdmin.SlideLibrary.SlideCategoryRow row = (wsIpgAdmin.SlideLibrary.SlideCategoryRow)e.Node.Tag;
                int slideId = row.SlideID;
                TreeViewEntryNodeDeleteClick(this, new TreeViewEntryNodeDeleteEventArgs(slideId));
            }

            if (Control.ModifierKeys == Keys.Shift)
            {
                //wsIpgAdmin.SlideLibrary.SlideCategoryRow row = (wsIpgAdmin.SlideLibrary.SlideCategoryRow)e.Node.Tag;
                TreeViewEntryNodeRemoveClick(this, new TreeViewEntryNodeRemoveEventArgs(e.Node));
            }
            
            AdminMainForm.DeselectDataGridViewItems();
            AdminMainForm.bindSlideGrid.Position = AdminMainForm.bindSlideGrid.Find("SlideID", ((wsIpgAdmin.SlideLibrary.SlideCategoryRow)e.Node.Tag).SlideID);
            AdminMainForm.bindSlideGrid.ResetItem(AdminMainForm.bindSlideGrid.Position);
            AdminMainForm.dgvSlides.Rows[AdminMainForm.bindSlideGrid.Position].Selected = true;
            AdminMainForm.dgvSlides.FirstDisplayedScrollingRowIndex = AdminMainForm.dgvSlides.Rows[AdminMainForm.bindSlideGrid.Position].Index;
		}

		protected override void OnEntryNodeRightClick(TreeNodeMouseClickEventArgs e, object entryRow)
		{
            AdminMainForm.RightClickNode = e.Node;
            e.Node.ContextMenuStrip = SlideContextMenu;
		}

        protected override void OnCategoryNodeRightClick(TreeNodeMouseClickEventArgs e, object categoryRow)
        {
            AdminMainForm.RightClickNode = e.Node;
            e.Node.ContextMenuStrip = CategoryContextMenu;
        }

        protected void CategorySlideTreeView_ItemDrag(object sender, ItemDragEventArgs e)
		{
			AdminMainForm.CurrentDraggedItem = (TreeNode)e.Item;
			SelectedNode = (TreeNode)e.Item;
			DoDragDrop(e.Item, DragDropEffects.Move);
		}
        
        protected void CategorySlideTreeView_DragEnter(object sender, DragEventArgs e)
		{
			//for some reason, too much evaluation in this method makes the whole thing hang like crazy.
			//so we'll just tell it to move and figure it out in the over event
			e.Effect = DragDropEffects.Move;
		}
        
        protected void CategorySlideTreeView_AfterExpand(object sender, TreeViewEventArgs e)
		{
			AdminMainForm = ((AdminMain)this.FindForm());
            //AdminMainForm.ReselectNode = e.Node;
			SelectedNode = e.Node;
		}
		
        protected override void OnCategoryNodeDragOver(DragEventArgs e, TreeNode draggedOverNode, object categoryRow)
		{
            OnDragOver(e);
            //by default, drag drop is none
            e.Effect = DragDropEffects.None;

            // The dragged item is getting dragged from outside the application
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                bool arePowerPointFiles = true;
                // Determine if the files are of type "ppt" or "pot"
                List<string> files = new List<string>((string[])e.Data.GetData(DataFormats.FileDrop));

                foreach (string file in files)
                {
                    if (!file.Contains(".pot") && !file.Contains(".ppt"))
                    {
                        arePowerPointFiles = false;
                    }
                }


                // We're dragging an item from the slide list, check to see if we're dragging over a slide group leaf
                if (IsLeafNode(draggedOverNode))
                {
                    if (draggedOverNode.Tag.GetType() != typeof(wsIpgAdmin.SlideLibrary.SlideCategoryRow) && arePowerPointFiles)
                    {
                        // We can drop a slide here
                        e.Effect = DragDropEffects.All;
                    }
                }
            }
            //if the current dragged item is from the Slide datagridview...
            else if (AdminMainForm.CurrentDraggedItem.GetType() == typeof(DataRowView))
            {
                //we're dragging an item from the slide list, check to see if we're dragging over a slide group leaf
                if (IsLeafNode(draggedOverNode))
                {
                    if (draggedOverNode.Tag.GetType() != typeof(wsIpgAdmin.SlideLibrary.SlideCategoryRow))
                    {
                        //we can drop a slide here
                        e.Effect = DragDropEffects.Move;
                    }
                }
            }
            //if the currentDraggedItem is a treenode, determine the action based on the treenode properties
            else if (AdminMainForm.CurrentDraggedItem.GetType() == typeof(TreeNode))
            {
                if (IsPeerNodeAndNotSelf(e, draggedOverNode))
                {
                    e.Effect = DragDropEffects.Move;
                }
            }

		}

		protected override void OnEntryNodeDragOver(DragEventArgs e, TreeNode draggedOverNode, object entryRow)
		{
            OnDragOver(e);
            //by default, drag drop is none
            e.Effect = DragDropEffects.None;

            if (IsPeerNodeAndNotSelf(e, draggedOverNode))
            {
                e.Effect = DragDropEffects.Move;
            }
            else if (AdminMainForm.CurrentDraggedItem.GetType() == typeof(DataRowView))
            {
                e.Effect = DragDropEffects.Move;
            }
            
		}

        private bool IsPeerNodeAndNotSelf(DragEventArgs e, TreeNode draggedOverNode)
        {
            //if the currentDraggedItem is a treenode, determine the action based on the treenode properties
            if (AdminMainForm.CurrentDraggedItem.GetType() == typeof(TreeNode))
            {
                if ((TreeNode)AdminMainForm.CurrentDraggedItem == draggedOverNode)
                {
                    //can't drop on itself
                    return false;
                }
                //we've dragged a node over a node
                //let's see if the hae the same parent
                if (((TreeNode)AdminMainForm.CurrentDraggedItem).Parent == draggedOverNode.Parent)
                {
                    return true;
                }
            }
            return false;
        }

		protected override void OnCategoryNodeDragDrop(DragEventArgs e, TreeNode droppedOnNode, object categoryRow)
		{
            int destinationCategory;
            int[] slideIDs = new int[AdminMainForm.dgvSlides.SelectedRows.Count];
            int sortOrder;

            // If files outside the applicaiton are being dropped on to the treeview show the file dialog form
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                ShowDropOnFileDialog(droppedOnNode, e);
            }
            // Dropping slide from slide list on a leaf node
            else if (IsLeafNode(droppedOnNode) && AdminMainForm.CurrentDraggedItem.GetType() == typeof(DataRowView))
            {
               
                //get all the slide ids of the slides we droppin
                //the DataGridView gives us the order of the selected rows backward (bottom in grid is first in SelectedRows)
                //so...let's use a reverse loop
                int j = 0;
                for (int i = AdminMainForm.dgvSlides.SelectedRows.Count - 1; i >= 0; i--)
                //for (int i = 0; i < AdminMainForm.dgvSlides.SelectedRows.Count; i++)
                {
                    slideIDs[j] = (int)AdminMainForm.dgvSlides.SelectedRows[i].Cells[0].Value;
                    j++;
                }

                //if we're not dropping on the MainTree
                //figure out if these slides match the RegionLanguage of the tree we are dropping onto, if not don't let'em
                foreach (int slideID in slideIDs)
                {
                    wsIpgAdmin.SlideLibrary.SlideRegionLanguageRow[] slideRegLangRows = AdminMainForm.dsSlideLibrary.Slide.FindBySlideID(slideID).GetSlideRegionLanguageRows();

                    List<int> regLandIds = new List<int>();
                    foreach (wsIpgAdmin.SlideLibrary.SlideRegionLanguageRow slideRegionLangRow in slideRegLangRows)
                    {
                        regLandIds.Add(slideRegionLangRow.RegionLanguageID);
                    }

                    if (!regLandIds.Contains(((wsIpgAdmin.SlideLibrary.RegionLanguageRow)this.Tag).RegionLanguageID))

                    {
                        MessageBox.Show(String.Format("The slide(s) you are attempting to assign to this category are not in the {0}-{1} group.", ((wsIpgAdmin.SlideLibrary.RegionLanguageRow)this.Tag).RegionRow.RegionName, ((wsIpgAdmin.SlideLibrary.RegionLanguageRow)this.Tag).LanguageRow.LongName), "Action not allowed", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                } 

                AdminMainForm.ReselectNode = droppedOnNode;
                AdminMainForm.ReselectSlideID = slideIDs[0];
                AdminMainForm.GetSlideResortItems();

                //we've dragged a slide to a valid category, add it to the category
                wsIpgAdmin.Admin admin = new Admin.wsIpgAdmin.Admin();

                destinationCategory = Convert.ToInt32(((wsIpgAdmin.SlideLibrary.CategoryRow)droppedOnNode.Tag).CategoryID.ToString());
                sortOrder = droppedOnNode.Nodes.Count;

                //this treeviews tag has the regLang row in it
                int regLangId = ((wsIpgAdmin.SlideLibrary.RegionLanguageRow)this.Tag).RegionLanguageID;

                admin.SlideMultipleCategorization(slideIDs, destinationCategory, -1, sortOrder, regLangId);

                //AdminMainForm.GetSlideLibraryData();
                //AdminMainForm.PopulateRegionLanguageTrees();  
                AdminMainForm.RefreshData();
                
            }
            // Drop slide from the treeview
            else if (AdminMainForm.CurrentDraggedItem.GetType() == typeof(TreeNode))
            {
                //we've dragged a node over a node
                //let's see if the have the same parent
                if (((TreeNode)AdminMainForm.CurrentDraggedItem).Parent == droppedOnNode.Parent)
                {
                    //reorder categories
                    wsIpgAdmin.Admin adminReorderCat = new Admin.wsIpgAdmin.Admin();
                    int movedCatID = ((wsIpgAdmin.SlideLibrary.CategoryRow)((TreeNode)AdminMainForm.CurrentDraggedItem).Tag).CategoryID;
                    //remove the node from where it was
                    ((TreeNode)AdminMainForm.CurrentDraggedItem).Remove();
                    //drop it into the new category's PARENT
                    //
                    droppedOnNode.Parent.Nodes.Add((TreeNode)AdminMainForm.CurrentDraggedItem);
                    //update the db
                    adminReorderCat.CategoryUpdateParent(movedCatID, ((wsIpgAdmin.SlideLibrary.CategoryRow)droppedOnNode.Parent.Tag).CategoryID);

                    ((TreeNode)AdminMainForm.CurrentDraggedItem).Remove();
                    droppedOnNode.Parent.Nodes.Insert(droppedOnNode.Index, (TreeNode)AdminMainForm.CurrentDraggedItem);
                    //now the db can only move a category up in the hierarchy one step at a time.
                    //so we start looping at the bottom of the node peer group,
                    //updating the db one notch for each loop, until we get to the node moved

                    for (int i = droppedOnNode.Parent.Nodes.Count - 1; i >= 0; i--)
                    {
                        //break out of loop if we reach the node we just moved
                        if (droppedOnNode.Parent.Nodes[i] == (TreeNode)AdminMainForm.CurrentDraggedItem)
                            break;

                        //for each, sort the attribute up one
                        adminReorderCat.CategoryMoveUp(movedCatID);
                    }
                }
            }
		}

		protected override void OnEntryNodeDragDrop(DragEventArgs e, TreeNode droppedOnNode, object entryRow)
		{            

            int destinationCategory;
            int[] slideIDs = new int[AdminMainForm.dgvSlides.SelectedRows.Count];
            int sortOrder;

            // If files outside the applicaiton are being dropped on to the treeview show the file dialog form
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                ShowDropOnFileDialog(droppedOnNode, e);
            }
            // Dropping slide from slide list on to a slide node
            else if (IsLeafNode(droppedOnNode) && AdminMainForm.CurrentDraggedItem.GetType() == typeof(DataRowView))
            {
                
                for (int i = 0; i < AdminMainForm.dgvSlides.SelectedRows.Count; i++)
                {
                    slideIDs[i] = (int)AdminMainForm.dgvSlides.SelectedRows[i].Cells[0].Value;
                }
                
                //this treeview's tag has the regLang row in it
                int regLangId = ((wsIpgAdmin.SlideLibrary.RegionLanguageRow)this.Tag).RegionLanguageID;

                //if we're not dropping on the MainTree
                //figure out if these slides match the RegionLanguage of the tree we are dropping onto, if not don't let'em
                foreach (int slideID in slideIDs)
                {
                    wsIpgAdmin.SlideLibrary.SlideRegionLanguageRow[] slideRegLangRows = AdminMainForm.dsSlideLibrary.Slide.FindBySlideID(slideID).GetSlideRegionLanguageRows();

                    List<int> regLandIds = new List<int>();
                    foreach (wsIpgAdmin.SlideLibrary.SlideRegionLanguageRow slideRegionLangRow in slideRegLangRows)
                    {
                        regLandIds.Add(slideRegionLangRow.RegionLanguageID);
                    }

                    if (!regLandIds.Contains(regLangId))
                    {
                        MessageBox.Show(String.Format("The slide(s) you are attempting to assign to this category are not in the {0}-{1} group.", ((wsIpgAdmin.SlideLibrary.RegionLanguageRow)this.Tag).RegionRow.RegionName, ((wsIpgAdmin.SlideLibrary.RegionLanguageRow)this.Tag).LanguageRow.LongName), "Action not allowed", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }

                AdminMainForm.ReselectNode = droppedOnNode.Parent;
                AdminMainForm.ReselectSlideID = slideIDs[0];
                AdminMainForm.GetSlideResortItems();

                //we've dragged a slide to a valid category, add it to the category
                wsIpgAdmin.Admin admin = new Admin.wsIpgAdmin.Admin();

                destinationCategory = Convert.ToInt32(((wsIpgAdmin.SlideLibrary.CategoryRow)droppedOnNode.Parent.Tag).CategoryID.ToString());
                sortOrder = droppedOnNode.Nodes.Count;

                

                //make new SlideCats for the slides we be droppin
                //We're gonna go backwards because the SelectedRows collection is weird...
                //if the user Shift+Click to selected multiple rows then
                //the SelectedRows collection will be in reverse order compared to how they appear in the DataGridView.
                //if the user Ctrl+Click to selecte multiple rows
                //the SelectedRows collection will be in reverse order compared to the order the rows were selected.

                for (int i = AdminMainForm.dgvSlides.SelectedRows.Count - 1; i >= 0; i--)
                {
                    wsIpgAdmin.SlideLibrary.SlideCategoryRow slideCatRow = AdminMainForm.dsSlideLibrary.SlideCategory.NewSlideCategoryRow();
                    slideCatRow.CategoryID = destinationCategory;
                    slideCatRow.SlideID = (int)AdminMainForm.dgvSlides.SelectedRows[i].Cells[0].Value;
                    slideCatRow.RegionLanguageID = regLangId;
                    slideCatRow.SortOrder = droppedOnNode.Index + AdminMainForm.dgvSlides.SelectedRows.Count - i;
                    AdminMainForm.dsSlideLibrary.SlideCategory.AddSlideCategoryRow(slideCatRow);
                }

                //we need to update the SortOrder for all the nodes below the droppedOnNode
                for (int i = droppedOnNode.Index + 1; i < droppedOnNode.Parent.Nodes.Count; i++)
                {
                    wsIpgAdmin.SlideLibrary.SlideCategoryRow currentSlideCatNode;
                    currentSlideCatNode = AdminMainForm.dsSlideLibrary.SlideCategory.FindByRegionLanguageIDCategoryIDSlideID(((wsIpgAdmin.SlideLibrary.SlideCategoryRow)droppedOnNode.Parent.Nodes[i].Tag).RegionLanguageID,
                        ((wsIpgAdmin.SlideLibrary.SlideCategoryRow)droppedOnNode.Parent.Nodes[i].Tag).CategoryID,
                        ((wsIpgAdmin.SlideLibrary.SlideCategoryRow)droppedOnNode.Parent.Nodes[i].Tag).SlideID);
                    currentSlideCatNode.SortOrder += AdminMainForm.dgvSlides.SelectedRows.Count;
                }

                admin.SlideCategoryUpdate((wsIpgAdmin.SlideLibrary)AdminMainForm.dsSlideLibrary.GetChanges());

                AdminMainForm.dsSlideLibrary.AcceptChanges();
                AdminMainForm.GetSlideLibraryData();
                AdminMainForm.PopulateRegionLanguageTrees();
                //***SS Added the 2 lines below as a test
                //MessageBox.Show(String.Format("IN #1"));
            }
            else if (AdminMainForm.CurrentDraggedItem.GetType() == typeof(TreeNode))
            {
                //we've dragged a node over a node
                //let's see if the have the same parent
                if (((TreeNode)AdminMainForm.CurrentDraggedItem).Parent == droppedOnNode.Parent)
                {
                    //reorder slide-category
                    //get the index of the node we are going to delete in case we have to put it back
                    int oldNodeIndex = 0;
                    oldNodeIndex = ((TreeNode)AdminMainForm.CurrentDraggedItem).Index;
                    //get a copy of the node we are going to delete
                    TreeNode tempNode;
                    tempNode = (TreeNode)AdminMainForm.CurrentDraggedItem;
                    ((TreeNode)AdminMainForm.CurrentDraggedItem).Remove();
                    droppedOnNode.Parent.Nodes.Insert(droppedOnNode.Index + 1, tempNode);

                    //get references to the reordered SlideCategories and give them a new sortorder
                    foreach (TreeNode currentNode in droppedOnNode.Parent.Nodes)
                    {
                        int currentSlideID = ((wsIpgAdmin.SlideLibrary.SlideCategoryRow)currentNode.Tag).SlideID;
                        int categoryID = ((wsIpgAdmin.SlideLibrary.SlideCategoryRow)currentNode.Tag).CategoryID;
                        int regLangID = ((wsIpgAdmin.SlideLibrary.SlideCategoryRow)currentNode.Tag).RegionLanguageID;

                        wsIpgAdmin.SlideLibrary.SlideCategoryRow scRow;
                        scRow = AdminMainForm.dsSlideLibrary.SlideCategory.FindByRegionLanguageIDCategoryIDSlideID(regLangID, categoryID, currentSlideID);
                        scRow.SortOrder = currentNode.Index;
                    }
                    //get the changes
                    wsIpgAdmin.SlideLibrary dsChanges = new Admin.wsIpgAdmin.SlideLibrary();
                    dsChanges = (wsIpgAdmin.SlideLibrary)AdminMainForm.dsSlideLibrary.GetChanges();
                    //send the changes
                    wsIpgAdmin.Admin admin = new Admin.wsIpgAdmin.Admin();
                    bool wsResult = admin.SlideCategoryUpdate(dsChanges);
                    if (wsResult)
                    {
                        //it worked
                        AdminMainForm.dsSlideLibrary.SlideCategory.AcceptChanges();
                        Refresh();
                        
                    }
                }
            }
        }

        private void ReselectTreeNode()
        {
            TreeNode selectedNode = null;
            if (AdminMainForm.ReselectNode != null)
            {
                if (AdminMainForm.ReselectNode.Tag != null && AdminMainForm.ReselectNode.Tag.GetType() == typeof(wsIpgAdmin.SlideLibrary.CategoryRow))
                {

                    wsIpgAdmin.SlideLibrary.CategoryRow catRow = ((wsIpgAdmin.SlideLibrary.CategoryDataTable)this.TreeData.CategoryTable).FindByCategoryID(((wsIpgAdmin.SlideLibrary.CategoryRow)AdminMainForm.ReselectNode.Tag).CategoryID);
                    selectedNode = this.FindNodeByRow(typeof(wsIpgAdmin.SlideLibrary.CategoryRow), catRow, null);
                }
                else if (AdminMainForm.ReselectNode.Tag != null && AdminMainForm.ReselectNode.Tag.GetType() == typeof(wsIpgAdmin.SlideLibrary.SlideCategoryRow))
                {
                    wsIpgAdmin.SlideLibrary.SlideCategoryRow slideCatRow = ((wsIpgAdmin.SlideLibrary.SlideCategoryDataTable)this.TreeData.CategoryEntryTable).FindByRegionLanguageIDCategoryIDSlideID(((wsIpgAdmin.SlideLibrary.SlideCategoryRow)AdminMainForm.ReselectNode.Tag).RegionLanguageID,
                        ((wsIpgAdmin.SlideLibrary.SlideCategoryRow)AdminMainForm.ReselectNode.Tag).SlideID,
                        ((wsIpgAdmin.SlideLibrary.SlideCategoryRow)AdminMainForm.ReselectNode.Tag).CategoryID);
                    selectedNode = this.FindNodeByRow(typeof(Admin.wsIpgAdmin.SlideLibrary.SlideCategoryRow), slideCatRow, null);
                }
            }
            else
            {
                selectedNode = this.Nodes[0];
            }


            if (selectedNode != null)
            {
                // Temporarily removed the event handler for the AfterExpand event in order to execute the next two lines of code which inadvertently trigger that 
                // event. The behavior was that the AfterExpand event would over write the currently selected node and set the selected node as the root node.
                //this.AfterExpand -= this.CategorySlideTreeView_AfterExpand;

                this.AfterExpand -= this.CategorySlideTreeView_AfterExpand;

                this.SelectedNode = selectedNode;
                this.SelectedNode.Expand();

                //this.AfterExpand += new System.Windows.Forms.TreeViewEventHandler(this.CategorySlideTreeView_AfterExpand);

                this.AfterExpand +=new TreeViewEventHandler(CategorySlideTreeView_AfterExpand);
            }
            else
            {
                TreeNode tnParent = AdminMainForm.ReselectNode.Parent;
                tnParent.Expand();
            }
        }

        long m_Ticks;
        protected override void OnDragOver(DragEventArgs drgevent)
        {
            Point pt = PointToClient(new Point(drgevent.X, drgevent.Y));
            TreeNode node = GetNodeAt(pt);
            TimeSpan ts = new TimeSpan(DateTime.Now.Ticks - m_Ticks);
            //scroll up
            if (pt.Y < ItemHeight)
            {
                // if within one node of top, scroll quickly
                if (node.PrevVisibleNode != null)
                {
                    node = node.PrevVisibleNode;
                }
                node.EnsureVisible();
                m_Ticks = DateTime.Now.Ticks;
            }
            else if (pt.Y < (ItemHeight * 2))
            {
                // if within two nodes of the top, scroll slowly
                if (ts.TotalMilliseconds > 250)
                {
                    node = node.PrevVisibleNode;
                    if (node.PrevVisibleNode != null)
                    {
                        node = node.PrevVisibleNode;
                    }
                    node.EnsureVisible();
                    m_Ticks = DateTime.Now.Ticks;
                }
            }


            //scroll down
            if (pt.Y > ItemHeight)
            {
                // if within one node of top, scroll quickly
                if (node.NextVisibleNode != null)
                {
                    node = node.NextVisibleNode;
                }
                node.EnsureVisible();
                m_Ticks = DateTime.Now.Ticks;
            }
            else if (pt.Y > (ItemHeight * 2))
            {
                // if within two nodes of the top, scroll slowly
                if (ts.TotalMilliseconds > 250)
                {
                    node = node.NextVisibleNode;
                    if (node.NextVisibleNode != null)
                    {
                        node = node.NextVisibleNode;
                    }
                    node.EnsureVisible();
                    m_Ticks = DateTime.Now.Ticks;
                }
            }
        }
	}

    public class TreeViewEntryNodeDeleteEventArgs : EventArgs
    {
        private int slideId;
        public TreeViewEntryNodeDeleteEventArgs(int slideId)
        {
            this.slideId = slideId;
        }
        public int SlideId
        {
            get
            {
                return this.slideId;
            }
        }
    }

    public class TreeViewEntryNodeRemoveEventArgs : EventArgs
    {
        //wsIpgAdmin.SlideLibrary.SlideCategoryRow categoryRow

        private TreeNode selectedNode;
        public TreeViewEntryNodeRemoveEventArgs(TreeNode selectedNode)
        {
            this.selectedNode = selectedNode;
        }
        public TreeNode SelectedNode
        {
            get
            {
                return this.selectedNode;
            }
        }
    }
}

    



