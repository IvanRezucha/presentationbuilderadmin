﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Admin.BLL
{
    public class SlideRemoveTransport
    {
        private wsIpgAdmin.SlideLibrary.SlideCategoryRow slideCategoryRow;
        private wsIpgAdmin.SlideLibrary.CategoryRow categoryRow;
        private wsIpgAdmin.SlideLibrary.SlideRow slideRow;
        private TreeNode selectedNode;

        public SlideRemoveTransport(wsIpgAdmin.SlideLibrary.SlideCategoryRow slideCategoryRow,
                                    wsIpgAdmin.SlideLibrary.CategoryRow categoryRow,
                                    wsIpgAdmin.SlideLibrary.SlideRow slideRow,
                                    TreeNode selectedNode)
        {
            this.slideCategoryRow = slideCategoryRow;
            this.categoryRow = categoryRow;
            this.slideRow = slideRow;
            this.selectedNode = selectedNode;
        }

        public string Title
        {
            get { return slideRow.Title; }
        }
        public wsIpgAdmin.SlideLibrary.SlideCategoryRow SlideCategoryRow
        {
            get { return this.slideCategoryRow; }
        }
        public wsIpgAdmin.SlideLibrary.CategoryRow CategoryRow
        {
            get { return this.categoryRow; }
        }
        public wsIpgAdmin.SlideLibrary.SlideRow SlideRow
        {
            get { return this.slideRow; }
        }
        public TreeNode SelectedNode
        {
            get { return this.selectedNode; }
        }
    }
}
