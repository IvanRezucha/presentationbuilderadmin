using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net;

namespace Admin.BLL
{
    public class Ftp
    {
        System.Net.WebClient _Client;

        public Ftp(string userName, string password)
        {
            _Client = new System.Net.WebClient();
            _Client.Credentials = new System.Net.NetworkCredential(userName, password);
            //_Client.CachePolicy = new System.Net.Cache.RequestCachePolicy(System.Net.Cache.RequestCacheLevel.);
        }
       
        public void Upload(string ftpServer, string filename)
        {
            _Client.BaseAddress = Properties.Settings.Default.NetStorageFtp;
            //Uri ftpFileUri = new Uri(ftpServer + "/" + new FileInfo(filename).Name);
            _Client.UploadFile(ftpServer + "/" + new FileInfo(filename).Name, "STOR", filename); 

            //System.Net.FtpWebRequest

            //System.Net.FtpWebRequest ftp;
            //ftp.Method = System.Net.WebRequestMethods.Ftp.UploadFile;
            //ftp.RequestUri = ftpFileUri;
            //ftp.Credentials = new System.Net.NetworkCredential("", "");

                
        }
    }
}
