using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Admin.BLL
{
    public class FtpClient
    {
        const int BUFFER_SIZE = 2048;

        private System.Net.FtpWebRequest _Client;

        public System.Net.FtpWebRequest Client
        {
            get { return _Client; }
            private set { _Client = value; }
        }

        private System.Net.NetworkCredential _Credential;

        public System.Net.NetworkCredential Credential
        {
            get { return _Credential; }
            private set { _Credential = value; }
        }


        public FtpClient(string userName, string password)
        {
            Credential = new System.Net.NetworkCredential(userName, password);
        }

        public void Upload(string ftpLocation, string fileToUpload, string fileExtension)
        {
            FileInfo fi;
            //make sure the file exists
            if (File.Exists(fileToUpload))
            {
                fi = new FileInfo(fileToUpload); 
            }
            else
            {
                FileToUploadNotFoundException ex = new FileToUploadNotFoundException(fileToUpload);
                throw ex;
            }
            
            //create the request
            CreateRequest(ftpLocation, fileToUpload, fileExtension);
            
            Client.Method = System.Net.WebRequestMethods.Ftp.UploadFile;
            Client.UseBinary = true;
            Client.ContentLength = fi.Length;

            byte[] content = new byte[BUFFER_SIZE-1];
            int dataRead;

            using (FileStream fs = fi.OpenRead())
            {
                try
                {
                    using (Stream s = Client.GetRequestStream())
                    {
                        do
                        {
                            dataRead = fs.Read(content, 0, BUFFER_SIZE);
                            s.Write(content, 0, dataRead);
                        } while (dataRead < BUFFER_SIZE);
                        s.Close();
                    }
                }
                catch(Exception ex) { }
                finally
                {
                    fs.Close();
                }
            }

            Client = null; 

        }

        private void CreateRequest(string ftpLocation, string fileToUpload, string fileExtension)
        {
            string uriRequest = String.Format("{0}/{1}.{2}", ftpLocation, Path.GetFileNameWithoutExtension(fileToUpload), fileExtension);
            Client = (System.Net.FtpWebRequest)System.Net.FtpWebRequest.Create(uriRequest);
            Client.Credentials = Credential;
            Client.Proxy = null;
            Client.KeepAlive = false;
        }
    }

    public class FileToUploadNotFoundException : System.IO.FileNotFoundException
    {
        public FileToUploadNotFoundException(string missingFile) 
            : base(String.Format("{0} could not be uploaded. File not found.", missingFile), missingFile)
        {
    
        }

 
    }
}
