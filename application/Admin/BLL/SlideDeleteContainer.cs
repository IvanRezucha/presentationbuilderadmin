﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Admin.BLL
{

    public partial class SlideDeleteContainer : UserControl
    {
        public event SlideDeleteContainerDeleteSlidesEventHandler DeleteSlides;
        public event SlideDeleteContainerClearSlidesEventHandler ClearSlides;
        public event SlideDeleteContainerRemoveSlideEventHandler RemoveSlides;
        public event SlideDeleteContainerCloseContainerEventHandler CloseContainer;

        public SlideDeleteContainer()
        {
            InitializeComponent();
            lstSlidesToDelete.DisplayMember = "Title";
        }

        private void Draw(List<wsIpgAdmin.SlideLibrary.SlideRow> slides)
        {
            foreach (wsIpgAdmin.SlideLibrary.SlideRow row in slides)
                lstSlidesToDelete.Items.Add(row);
        }

        public void Clear()
        {
            lstSlidesToDelete.Items.Clear();
        }

        public void Refresh(List<wsIpgAdmin.SlideLibrary.SlideRow> slides)
        {
            Clear();
            Draw(slides);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            OnDeleteSlides(new SlideDeleteContainerDeleteSlidesEventArgs());
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            OnClearSlides(new SlideDeleteContainerClearSlidesEventArgs());
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (lstSlidesToDelete.SelectedItem != null)
            {
                wsIpgAdmin.SlideLibrary.SlideRow slide = (wsIpgAdmin.SlideLibrary.SlideRow)lstSlidesToDelete.SelectedItem;
                OnRemoveSlide(new SlideDeleteContainerRemoveSlideEventArgs(slide));
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            OnCloseContainer(new SlideDeleteContainerCloseContainerEventArgs());
        }

        protected virtual void OnDeleteSlides(SlideDeleteContainerDeleteSlidesEventArgs e)
        {
            DeleteSlides(this, e);
        }

        protected virtual void OnClearSlides(SlideDeleteContainerClearSlidesEventArgs e)
        {
            ClearSlides(this, e);
        }

        protected virtual void OnRemoveSlide(SlideDeleteContainerRemoveSlideEventArgs e)
        {
            RemoveSlides(this, e);
        }

        protected virtual void OnCloseContainer(SlideDeleteContainerCloseContainerEventArgs e)
        {
            CloseContainer(this, e);
        }
    }

    public delegate void SlideDeleteContainerDeleteSlidesEventHandler(object sender, SlideDeleteContainerDeleteSlidesEventArgs e);
    public delegate void SlideDeleteContainerClearSlidesEventHandler(object sender, SlideDeleteContainerClearSlidesEventArgs e);
    public delegate void SlideDeleteContainerRemoveSlideEventHandler(object sender, SlideDeleteContainerRemoveSlideEventArgs e);
    public delegate void SlideDeleteContainerCloseContainerEventHandler(object sender, SlideDeleteContainerCloseContainerEventArgs e);
    public class SlideDeleteContainerDeleteSlidesEventArgs : EventArgs
    {
    }
    public class SlideDeleteContainerClearSlidesEventArgs : EventArgs
    {
    }
    public class SlideDeleteContainerRemoveSlideEventArgs : EventArgs
    {
        private wsIpgAdmin.SlideLibrary.SlideRow slide;
        public SlideDeleteContainerRemoveSlideEventArgs(wsIpgAdmin.SlideLibrary.SlideRow slide)
        {
            this.slide = slide;
        }
        public wsIpgAdmin.SlideLibrary.SlideRow Slide
        {
            get
            {
                return this.slide;
            }
        }
    }
    public class SlideDeleteContainerCloseContainerEventArgs : EventArgs
    {
    }
}
