﻿namespace Admin.BLL
{
    partial class SlideRemoveContainer
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnRemoveFromCategory = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lstSlidesToRemove = new System.Windows.Forms.ListBox();
            this.btnRemoveFromList = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.lblCategory = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnRemoveFromCategory
            // 
            this.btnRemoveFromCategory.Location = new System.Drawing.Point(34, 184);
            this.btnRemoveFromCategory.Name = "btnRemoveFromCategory";
            this.btnRemoveFromCategory.Size = new System.Drawing.Size(131, 25);
            this.btnRemoveFromCategory.TabIndex = 0;
            this.btnRemoveFromCategory.Text = "Remove From Category";
            this.btnRemoveFromCategory.UseVisualStyleBackColor = true;
            this.btnRemoveFromCategory.Click += new System.EventHandler(this.btnRemoveFromCategory_Click);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(274, 184);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(75, 25);
            this.btnClear.TabIndex = 1;
            this.btnClear.Text = "Clear Slides";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(34, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(315, 16);
            this.label1.TabIndex = 3;
            this.label1.Text = "This will remove these slides from category: ";
            // 
            // lstSlidesToRemove
            // 
            this.lstSlidesToRemove.FormattingEnabled = true;
            this.lstSlidesToRemove.Location = new System.Drawing.Point(37, 44);
            this.lstSlidesToRemove.Name = "lstSlidesToRemove";
            this.lstSlidesToRemove.Size = new System.Drawing.Size(453, 134);
            this.lstSlidesToRemove.TabIndex = 4;
            // 
            // btnRemoveFromList
            // 
            this.btnRemoveFromList.Location = new System.Drawing.Point(357, 184);
            this.btnRemoveFromList.Name = "btnRemoveFromList";
            this.btnRemoveFromList.Size = new System.Drawing.Size(133, 25);
            this.btnRemoveFromList.TabIndex = 5;
            this.btnRemoveFromList.Text = "Remove Slide From List";
            this.btnRemoveFromList.UseVisualStyleBackColor = true;
            this.btnRemoveFromList.Click += new System.EventHandler(this.btnRemoveFromList_Click);
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.Red;
            this.btnClose.Cursor = System.Windows.Forms.Cursors.Default;
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnClose.Location = new System.Drawing.Point(3, 3);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(24, 23);
            this.btnClose.TabIndex = 6;
            this.btnClose.Text = "X";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lblCategory
            // 
            this.lblCategory.AutoSize = true;
            this.lblCategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCategory.Location = new System.Drawing.Point(344, 25);
            this.lblCategory.Name = "lblCategory";
            this.lblCategory.Size = new System.Drawing.Size(0, 16);
            this.lblCategory.TabIndex = 7;
            // 
            // SlideRemoveContainer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.lblCategory);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnRemoveFromList);
            this.Controls.Add(this.lstSlidesToRemove);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnRemoveFromCategory);
            this.Name = "SlideRemoveContainer";
            this.Size = new System.Drawing.Size(503, 221);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnRemoveFromCategory;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox lstSlidesToRemove;
        private System.Windows.Forms.Button btnRemoveFromList;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label lblCategory;
    }
}
