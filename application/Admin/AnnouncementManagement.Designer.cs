namespace Admin
{
    partial class AnnouncementManagement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label announcementIDLabel;
            System.Windows.Forms.Label titleLabel;
            System.Windows.Forms.Label bodyLabel;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AnnouncementManagement));
            this.splForm = new System.Windows.Forms.SplitContainer();
            this.dgvAnnouncement = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Region = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bsAnnouncement = new System.Windows.Forms.BindingSource(this.components);
            this.dsAnnouncement = new Admin.wsIpgAdmin.AnnouncementDS();
            this.btnManageAssets = new System.Windows.Forms.Button();
            this.txtRegionId = new System.Windows.Forms.TextBox();
            this.cboRegion = new System.Windows.Forms.ComboBox();
            this.btnPreview = new System.Windows.Forms.Button();
            this.announcementIDTextBox = new System.Windows.Forms.TextBox();
            this.txtTitle = new System.Windows.Forms.TextBox();
            this.txtBody = new System.Windows.Forms.TextBox();
            this.dtpPubDate = new System.Windows.Forms.DateTimePicker();
            this.bnAnnouncement = new System.Windows.Forms.BindingNavigator(this.components);
            this.tslNavCountItem = new System.Windows.Forms.ToolStripLabel();
            this.tsbNavMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.tsbNavMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.txtNavigatorPosition = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbNavMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.tsbNavMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbNavAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.tsbNavDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.tsbNavSave = new System.Windows.Forms.ToolStripButton();
            this.err = new System.Windows.Forms.ErrorProvider(this.components);
            announcementIDLabel = new System.Windows.Forms.Label();
            titleLabel = new System.Windows.Forms.Label();
            bodyLabel = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            this.splForm.Panel1.SuspendLayout();
            this.splForm.Panel2.SuspendLayout();
            this.splForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAnnouncement)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsAnnouncement)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsAnnouncement)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bnAnnouncement)).BeginInit();
            this.bnAnnouncement.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.err)).BeginInit();
            this.SuspendLayout();
            // 
            // announcementIDLabel
            // 
            announcementIDLabel.AutoSize = true;
            announcementIDLabel.Location = new System.Drawing.Point(899, 292);
            announcementIDLabel.Name = "announcementIDLabel";
            announcementIDLabel.Size = new System.Drawing.Size(96, 13);
            announcementIDLabel.TabIndex = 0;
            announcementIDLabel.Text = "Announcement ID:";
            announcementIDLabel.Visible = false;
            // 
            // titleLabel
            // 
            titleLabel.AutoSize = true;
            titleLabel.Location = new System.Drawing.Point(3, 12);
            titleLabel.Name = "titleLabel";
            titleLabel.Size = new System.Drawing.Size(63, 13);
            titleLabel.TabIndex = 2;
            titleLabel.Text = "Description:";
            // 
            // bodyLabel
            // 
            bodyLabel.AutoSize = true;
            bodyLabel.Location = new System.Drawing.Point(3, 38);
            bodyLabel.Name = "bodyLabel";
            bodyLabel.Size = new System.Drawing.Size(34, 13);
            bodyLabel.TabIndex = 4;
            bodyLabel.Text = "Body:";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(3, 292);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(44, 13);
            label1.TabIndex = 10;
            label1.Text = "Region:";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(899, 326);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(56, 13);
            label2.TabIndex = 12;
            label2.Text = "Region Id:";
            label2.Visible = false;
            // 
            // splForm
            // 
            this.splForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splForm.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splForm.Location = new System.Drawing.Point(0, 0);
            this.splForm.Name = "splForm";
            this.splForm.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splForm.Panel1
            // 
            this.splForm.Panel1.AutoScroll = true;
            this.splForm.Panel1.Controls.Add(this.dgvAnnouncement);
            // 
            // splForm.Panel2
            // 
            this.splForm.Panel2.Controls.Add(this.btnManageAssets);
            this.splForm.Panel2.Controls.Add(label2);
            this.splForm.Panel2.Controls.Add(this.txtRegionId);
            this.splForm.Panel2.Controls.Add(label1);
            this.splForm.Panel2.Controls.Add(this.cboRegion);
            this.splForm.Panel2.Controls.Add(this.btnPreview);
            this.splForm.Panel2.Controls.Add(announcementIDLabel);
            this.splForm.Panel2.Controls.Add(this.announcementIDTextBox);
            this.splForm.Panel2.Controls.Add(titleLabel);
            this.splForm.Panel2.Controls.Add(this.txtTitle);
            this.splForm.Panel2.Controls.Add(bodyLabel);
            this.splForm.Panel2.Controls.Add(this.txtBody);
            this.splForm.Panel2.Controls.Add(this.dtpPubDate);
            this.splForm.Size = new System.Drawing.Size(900, 591);
            this.splForm.SplitterDistance = 192;
            this.splForm.TabIndex = 0;
            // 
            // dgvAnnouncement
            // 
            this.dgvAnnouncement.AllowUserToAddRows = false;
            this.dgvAnnouncement.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvAnnouncement.AutoGenerateColumns = false;
            this.dgvAnnouncement.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.Region});
            this.dgvAnnouncement.DataSource = this.bsAnnouncement;
            this.dgvAnnouncement.Location = new System.Drawing.Point(0, 28);
            this.dgvAnnouncement.Name = "dgvAnnouncement";
            this.dgvAnnouncement.ReadOnly = true;
            this.dgvAnnouncement.RowHeadersWidth = 25;
            this.dgvAnnouncement.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAnnouncement.ShowEditingIcon = false;
            this.dgvAnnouncement.Size = new System.Drawing.Size(897, 161);
            this.dgvAnnouncement.TabIndex = 0;
            this.dgvAnnouncement.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvAnnouncement_RowsAdded);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "AnnouncementID";
            this.dataGridViewTextBoxColumn1.HeaderText = "AnnouncementID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Title";
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridViewTextBoxColumn2.FillWeight = 74.22681F;
            this.dataGridViewTextBoxColumn2.HeaderText = "Description";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Body";
            this.dataGridViewTextBoxColumn3.FillWeight = 33F;
            this.dataGridViewTextBoxColumn3.HeaderText = "Body";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Visible = false;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn4.DataPropertyName = "PubDate";
            this.dataGridViewTextBoxColumn4.FillWeight = 25.7732F;
            this.dataGridViewTextBoxColumn4.HeaderText = "Date";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Visible = false;
            // 
            // Region
            // 
            this.Region.HeaderText = "Region";
            this.Region.Name = "Region";
            this.Region.ReadOnly = true;
            this.Region.Width = 110;
            // 
            // bsAnnouncement
            // 
            this.bsAnnouncement.DataMember = "Announcement";
            this.bsAnnouncement.DataSource = this.dsAnnouncement;
            this.bsAnnouncement.Sort = "PubDate DESC";
            this.bsAnnouncement.BindingComplete += new System.Windows.Forms.BindingCompleteEventHandler(this.bsAnnouncement_BindingComplete);
            // 
            // dsAnnouncement
            // 
            this.dsAnnouncement.DataSetName = "AnnouncementDS";
            this.dsAnnouncement.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // btnManageAssets
            // 
            this.btnManageAssets.Location = new System.Drawing.Point(192, 321);
            this.btnManageAssets.Name = "btnManageAssets";
            this.btnManageAssets.Size = new System.Drawing.Size(118, 23);
            this.btnManageAssets.TabIndex = 13;
            this.btnManageAssets.Text = "Manage Assets";
            this.btnManageAssets.UseVisualStyleBackColor = true;
            this.btnManageAssets.Click += new System.EventHandler(this.btnManageAssets_Click);
            // 
            // txtRegionId
            // 
            this.txtRegionId.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAnnouncement, "RegionId", true));
            this.txtRegionId.Location = new System.Drawing.Point(-500, -500);
            this.txtRegionId.Name = "txtRegionId";
            this.txtRegionId.Size = new System.Drawing.Size(200, 20);
            this.txtRegionId.TabIndex = 11;
            // 
            // cboRegion
            // 
            this.cboRegion.DataBindings.Add(new System.Windows.Forms.Binding("SelectedItem", this.dsAnnouncement, "Region.RegionID", true));
            this.cboRegion.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.dsAnnouncement, "Region.RegionID", true));
            this.cboRegion.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dsAnnouncement, "Region.RegionName", true));
            this.cboRegion.FormattingEnabled = true;
            this.cboRegion.Location = new System.Drawing.Point(88, 284);
            this.cboRegion.Name = "cboRegion";
            this.cboRegion.Size = new System.Drawing.Size(222, 21);
            this.cboRegion.TabIndex = 9;
            this.cboRegion.SelectedIndexChanged += new System.EventHandler(this.cboRegion_SelectedIndexChanged);
            // 
            // btnPreview
            // 
            this.btnPreview.Location = new System.Drawing.Point(88, 321);
            this.btnPreview.Name = "btnPreview";
            this.btnPreview.Size = new System.Drawing.Size(75, 23);
            this.btnPreview.TabIndex = 8;
            this.btnPreview.Text = "Preview";
            this.btnPreview.UseVisualStyleBackColor = true;
            this.btnPreview.Click += new System.EventHandler(this.btnPreview_Click);
            // 
            // announcementIDTextBox
            // 
            this.announcementIDTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAnnouncement, "AnnouncementID", true));
            this.announcementIDTextBox.Location = new System.Drawing.Point(1001, 292);
            this.announcementIDTextBox.Name = "announcementIDTextBox";
            this.announcementIDTextBox.Size = new System.Drawing.Size(200, 20);
            this.announcementIDTextBox.TabIndex = 1;
            this.announcementIDTextBox.Visible = false;
            // 
            // txtTitle
            // 
            this.txtTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTitle.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAnnouncement, "Title", true));
            this.err.SetError(this.txtTitle, "Hey! fill me in");
            this.txtTitle.Location = new System.Drawing.Point(88, 9);
            this.txtTitle.MaxLength = 200;
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(780, 20);
            this.txtTitle.TabIndex = 3;
            // 
            // txtBody
            // 
            this.txtBody.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBody.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAnnouncement, "Body", true));
            this.err.SetError(this.txtBody, "I can\'t be blank");
            this.txtBody.Location = new System.Drawing.Point(88, 35);
            this.txtBody.Multiline = true;
            this.txtBody.Name = "txtBody";
            this.txtBody.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtBody.Size = new System.Drawing.Size(780, 230);
            this.txtBody.TabIndex = 5;
            this.txtBody.WordWrap = false;
            // 
            // dtpPubDate
            // 
            this.dtpPubDate.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.bsAnnouncement, "PubDate", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, null, "d"));
            this.dtpPubDate.Location = new System.Drawing.Point(-500, -500);
            this.dtpPubDate.Name = "dtpPubDate";
            this.dtpPubDate.Size = new System.Drawing.Size(222, 20);
            this.dtpPubDate.TabIndex = 7;
            this.dtpPubDate.Value = new System.DateTime(2009, 1, 14, 0, 0, 0, 0);
            // 
            // bnAnnouncement
            // 
            this.bnAnnouncement.AddNewItem = null;
            this.bnAnnouncement.BindingSource = this.bsAnnouncement;
            this.bnAnnouncement.CountItem = this.tslNavCountItem;
            this.bnAnnouncement.DeleteItem = null;
            this.bnAnnouncement.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.bnAnnouncement.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbNavMoveFirstItem,
            this.tsbNavMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.txtNavigatorPosition,
            this.tslNavCountItem,
            this.bindingNavigatorSeparator1,
            this.tsbNavMoveNextItem,
            this.tsbNavMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.tsbNavAddNewItem,
            this.tsbNavDeleteItem,
            this.tsbNavSave});
            this.bnAnnouncement.Location = new System.Drawing.Point(0, 0);
            this.bnAnnouncement.MoveFirstItem = null;
            this.bnAnnouncement.MoveLastItem = null;
            this.bnAnnouncement.MoveNextItem = null;
            this.bnAnnouncement.MovePreviousItem = null;
            this.bnAnnouncement.Name = "bnAnnouncement";
            this.bnAnnouncement.PositionItem = this.txtNavigatorPosition;
            this.bnAnnouncement.Size = new System.Drawing.Size(900, 25);
            this.bnAnnouncement.TabIndex = 1;
            // 
            // tslNavCountItem
            // 
            this.tslNavCountItem.Name = "tslNavCountItem";
            this.tslNavCountItem.Size = new System.Drawing.Size(36, 22);
            this.tslNavCountItem.Text = "of {0}";
            this.tslNavCountItem.ToolTipText = "Total number of items";
            // 
            // tsbNavMoveFirstItem
            // 
            this.tsbNavMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbNavMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("tsbNavMoveFirstItem.Image")));
            this.tsbNavMoveFirstItem.Name = "tsbNavMoveFirstItem";
            this.tsbNavMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.tsbNavMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.tsbNavMoveFirstItem.Text = "Move first";
            this.tsbNavMoveFirstItem.Click += new System.EventHandler(this.tsbNavMoveFirstItem_Click);
            // 
            // tsbNavMovePreviousItem
            // 
            this.tsbNavMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbNavMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("tsbNavMovePreviousItem.Image")));
            this.tsbNavMovePreviousItem.Name = "tsbNavMovePreviousItem";
            this.tsbNavMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.tsbNavMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.tsbNavMovePreviousItem.Text = "Move previous";
            this.tsbNavMovePreviousItem.Click += new System.EventHandler(this.tsbNavMovePreviousItem_Click);
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // txtNavigatorPosition
            // 
            this.txtNavigatorPosition.AccessibleName = "Position";
            this.txtNavigatorPosition.AutoSize = false;
            this.txtNavigatorPosition.Name = "txtNavigatorPosition";
            this.txtNavigatorPosition.Size = new System.Drawing.Size(50, 21);
            this.txtNavigatorPosition.Text = "0";
            this.txtNavigatorPosition.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // tsbNavMoveNextItem
            // 
            this.tsbNavMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbNavMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("tsbNavMoveNextItem.Image")));
            this.tsbNavMoveNextItem.Name = "tsbNavMoveNextItem";
            this.tsbNavMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.tsbNavMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.tsbNavMoveNextItem.Text = "Move next";
            this.tsbNavMoveNextItem.Click += new System.EventHandler(this.tsbNavMoveNextItem_Click);
            // 
            // tsbNavMoveLastItem
            // 
            this.tsbNavMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbNavMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("tsbNavMoveLastItem.Image")));
            this.tsbNavMoveLastItem.Name = "tsbNavMoveLastItem";
            this.tsbNavMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.tsbNavMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.tsbNavMoveLastItem.Text = "Move last";
            this.tsbNavMoveLastItem.Click += new System.EventHandler(this.tsbNavMoveLastItem_Click);
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // tsbNavAddNewItem
            // 
            this.tsbNavAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbNavAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("tsbNavAddNewItem.Image")));
            this.tsbNavAddNewItem.Name = "tsbNavAddNewItem";
            this.tsbNavAddNewItem.RightToLeftAutoMirrorImage = true;
            this.tsbNavAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.tsbNavAddNewItem.Text = "Add new";
            this.tsbNavAddNewItem.Click += new System.EventHandler(this.tsbNavAddNewItem_Click);
            // 
            // tsbNavDeleteItem
            // 
            this.tsbNavDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbNavDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("tsbNavDeleteItem.Image")));
            this.tsbNavDeleteItem.Name = "tsbNavDeleteItem";
            this.tsbNavDeleteItem.RightToLeftAutoMirrorImage = true;
            this.tsbNavDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.tsbNavDeleteItem.Text = "Delete";
            this.tsbNavDeleteItem.Click += new System.EventHandler(this.tsbNavDeleteItem_Click);
            // 
            // tsbNavSave
            // 
            this.tsbNavSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbNavSave.Image = ((System.Drawing.Image)(resources.GetObject("tsbNavSave.Image")));
            this.tsbNavSave.Name = "tsbNavSave";
            this.tsbNavSave.Size = new System.Drawing.Size(23, 22);
            this.tsbNavSave.Text = "Save Data";
            this.tsbNavSave.Click += new System.EventHandler(this.tsbNavSave_Click);
            // 
            // err
            // 
            this.err.ContainerControl = this;
            this.err.DataSource = this.bsAnnouncement;
            // 
            // AnnouncementManagement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(900, 591);
            this.Controls.Add(this.bnAnnouncement);
            this.Controls.Add(this.splForm);
            this.Name = "AnnouncementManagement";
            this.Text = "Manage Announcements";
            this.Load += new System.EventHandler(this.AnnouncementManagement_Load);
            this.splForm.Panel1.ResumeLayout(false);
            this.splForm.Panel2.ResumeLayout(false);
            this.splForm.Panel2.PerformLayout();
            this.splForm.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAnnouncement)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsAnnouncement)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsAnnouncement)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bnAnnouncement)).EndInit();
            this.bnAnnouncement.ResumeLayout(false);
            this.bnAnnouncement.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.err)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SplitContainer splForm;
        private System.Windows.Forms.DataGridView dgvAnnouncement;
        private System.Windows.Forms.BindingSource bsAnnouncement;
        private Admin.wsIpgAdmin.AnnouncementDS dsAnnouncement;
        private System.Windows.Forms.BindingNavigator bnAnnouncement;
        private System.Windows.Forms.ToolStripButton tsbNavMoveFirstItem;
        private System.Windows.Forms.ToolStripButton tsbNavMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox txtNavigatorPosition;
        private System.Windows.Forms.ToolStripLabel tslNavCountItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton tsbNavMoveNextItem;
        private System.Windows.Forms.ToolStripButton tsbNavMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton tsbNavAddNewItem;
        private System.Windows.Forms.ToolStripButton tsbNavDeleteItem;
        private System.Windows.Forms.ToolStripButton tsbNavSave;
        private System.Windows.Forms.TextBox announcementIDTextBox;
        private System.Windows.Forms.TextBox txtTitle;
        private System.Windows.Forms.TextBox txtBody;
        private System.Windows.Forms.DateTimePicker dtpPubDate;
        private System.Windows.Forms.ErrorProvider err;
        private System.Windows.Forms.Button btnPreview;
        private System.Windows.Forms.ComboBox cboRegion;
        private System.Windows.Forms.TextBox txtRegionId;
        private System.Windows.Forms.Button btnManageAssets;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Region;
    }
}