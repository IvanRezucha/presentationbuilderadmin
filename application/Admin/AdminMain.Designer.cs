namespace Admin
{
    partial class AdminMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AdminMain));
            this.splMain = new System.Windows.Forms.SplitContainer();
            this.navigationPaneLeftPane = new Ascend.Windows.Forms.NavigationPane();
            this.navigationPanePageSlideLibrary = new Ascend.Windows.Forms.NavigationPanePage();
            this.navigationPanePageRegionLanguages = new Ascend.Windows.Forms.NavigationPanePage();
            this.gradientPanel2 = new Ascend.Windows.Forms.GradientPanel();
            this.btnApplyRegionLanguageSettings = new System.Windows.Forms.Button();
            this.btnToggleRegionLanguages = new System.Windows.Forms.Button();
            this.btnCollapseAllRegions = new System.Windows.Forms.Button();
            this.btnExpandAllRegions = new System.Windows.Forms.Button();
            this.gradientCaption3 = new Ascend.Windows.Forms.GradientCaption();
            this.regionLanguageGroupGrid = new Admin.UserControls.RegionLanguageGroupGrid();
            this.navigationPanePageFilters = new Ascend.Windows.Forms.NavigationPanePage();
            this.gradientPanel3 = new Ascend.Windows.Forms.GradientPanel();
            this.btnToggleFilters = new System.Windows.Forms.Button();
            this.gradientCaption4 = new Ascend.Windows.Forms.GradientCaption();
            this.gradientPanel4 = new Ascend.Windows.Forms.GradientPanel();
            this.btnApplyFilterSettings = new System.Windows.Forms.Button();
            this.grpFilterApplyOptions = new System.Windows.Forms.GroupBox();
            this.rdoFilterShowBoth = new System.Windows.Forms.RadioButton();
            this.rdoFilterShowMain = new System.Windows.Forms.RadioButton();
            this.rdoFilterShowRegLang = new System.Windows.Forms.RadioButton();
            this.gradientCaption5 = new Ascend.Windows.Forms.GradientCaption();
            this.filterTabs = new Admin.UserControls.FilterTabs();
            this.splInfo = new System.Windows.Forms.SplitContainer();
            this.dgvSlides = new System.Windows.Forms.DataGridView();
            this.slideIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.titleDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fileNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DateModified = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NumInLib = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DateRemoved = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmnSlideList = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.miSlideListNewSlide = new System.Windows.Forms.ToolStripMenuItem();
            this.bindSlideGrid = new System.Windows.Forms.BindingSource(this.components);
            this.dsSlideLibrary = new Admin.wsIpgAdmin.SlideLibrary();
            this.cmnSlide = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.miSlideUpdate = new System.Windows.Forms.ToolStripMenuItem();
            this.miSlideDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.miSlideResetFiltersAndOrRegionLanguages = new System.Windows.Forms.ToolStripMenuItem();
            this.miSlideResetFilters = new System.Windows.Forms.ToolStripMenuItem();
            this.miSlideResetRegionLanguages = new System.Windows.Forms.ToolStripMenuItem();
            this.lstSlideAffiliations = new System.Windows.Forms.ListBox();
            this.bindingNavigatorSlideList = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripComboBoxRegionLanguage = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripButtonNewSlide = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonUpdateSlide = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonDeleteSlide = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.webSlideViewer = new System.Windows.Forms.WebBrowser();
            this.cmnCategory = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.miCategoryCategoryAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.miCategoryEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.miCategoryDeleteCat = new System.Windows.Forms.ToolStripMenuItem();
            this.miCategoryRemoveSlide = new System.Windows.Forms.ToolStripMenuItem();
            this.imlIcons16 = new System.Windows.Forms.ImageList(this.components);
            this.mnuMain = new System.Windows.Forms.MenuStrip();
            this.miMainFile = new System.Windows.Forms.ToolStripMenuItem();
            this.miMainHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.miMainExit = new System.Windows.Forms.ToolStripMenuItem();
            this.miMainModules = new System.Windows.Forms.ToolStripMenuItem();
            this.miMainModulesSupportFiles = new System.Windows.Forms.ToolStripMenuItem();
            this.supportFileBatchUploadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.miFilterToolStrip = new System.Windows.Forms.ToolStripMenuItem();
            this.miMainModulesAnnouncementManagement = new System.Windows.Forms.ToolStripMenuItem();
            this.miMainUserInfo = new System.Windows.Forms.ToolStripMenuItem();
            this.miMainUserInfoDataUpdates = new System.Windows.Forms.ToolStripMenuItem();
            this.miMainUserInfoRegistrants = new System.Windows.Forms.ToolStripMenuItem();
            this.miMainUpdates = new System.Windows.Forms.ToolStripMenuItem();
            this.miMainUpdatesPublishLocalizedContentUpdate = new System.Windows.Forms.ToolStripMenuItem();
            this.miMainUpdatesPublishLocalizedWebContentUpdate = new System.Windows.Forms.ToolStripMenuItem();
            this.miMainUpdatePublishContentUpdate = new System.Windows.Forms.ToolStripMenuItem();
            this.miMainSlide = new System.Windows.Forms.ToolStripMenuItem();
            this.miMainSlideNewSlide = new System.Windows.Forms.ToolStripMenuItem();
            this.miMainSlideUpdateSlide = new System.Windows.Forms.ToolStripMenuItem();
            this.miMainSlideDeleteSlide = new System.Windows.Forms.ToolStripMenuItem();
            this.miMainSlideUpdateSlideFilterOrRegionLanguages = new System.Windows.Forms.ToolStripMenuItem();
            this.miMainSlideUpdateSlideFilter = new System.Windows.Forms.ToolStripMenuItem();
            this.miMainSlideUpdateSlideRegionLanguages = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.miMainData = new System.Windows.Forms.ToolStripMenuItem();
            this.miMainDataRefreshData = new System.Windows.Forms.ToolStripMenuItem();
            this.stsBottom = new System.Windows.Forms.StatusStrip();
            this.lblStatusMain = new System.Windows.Forms.ToolStripStatusLabel();
            this.cmnRegionLanguageCategory = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.miRegionLanguageCategoryRemoveSlide = new System.Windows.Forms.ToolStripMenuItem();
            this.splMain.Panel1.SuspendLayout();
            this.splMain.Panel2.SuspendLayout();
            this.splMain.SuspendLayout();
            this.navigationPaneLeftPane.SuspendLayout();
            this.navigationPanePageRegionLanguages.SuspendLayout();
            this.gradientPanel2.SuspendLayout();
            this.navigationPanePageFilters.SuspendLayout();
            this.gradientPanel3.SuspendLayout();
            this.gradientPanel4.SuspendLayout();
            this.grpFilterApplyOptions.SuspendLayout();
            this.splInfo.Panel1.SuspendLayout();
            this.splInfo.Panel2.SuspendLayout();
            this.splInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSlides)).BeginInit();
            this.cmnSlideList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindSlideGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsSlideLibrary)).BeginInit();
            this.cmnSlide.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigatorSlideList)).BeginInit();
            this.bindingNavigatorSlideList.SuspendLayout();
            this.cmnCategory.SuspendLayout();
            this.mnuMain.SuspendLayout();
            this.stsBottom.SuspendLayout();
            this.cmnRegionLanguageCategory.SuspendLayout();
            this.SuspendLayout();
            // 
            // splMain
            // 
            this.splMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.splMain.Location = new System.Drawing.Point(0, 27);
            this.splMain.Name = "splMain";
            // 
            // splMain.Panel1
            // 
            this.splMain.Panel1.Controls.Add(this.navigationPaneLeftPane);
            // 
            // splMain.Panel2
            // 
            this.splMain.Panel2.Controls.Add(this.splInfo);
            this.splMain.Size = new System.Drawing.Size(874, 625);
            this.splMain.SplitterDistance = 248;
            this.splMain.TabIndex = 0;
            // 
            // navigationPaneLeftPane
            // 
            this.navigationPaneLeftPane.ButtonActiveGradientHighColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(225)))), ((int)(((byte)(155)))));
            this.navigationPaneLeftPane.ButtonActiveGradientLowColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(165)))), ((int)(((byte)(78)))));
            this.navigationPaneLeftPane.ButtonBorderColor = System.Drawing.SystemColors.MenuHighlight;
            this.navigationPaneLeftPane.ButtonFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.navigationPaneLeftPane.ButtonForeColor = System.Drawing.SystemColors.ControlText;
            this.navigationPaneLeftPane.ButtonGradientHighColor = System.Drawing.SystemColors.ButtonHighlight;
            this.navigationPaneLeftPane.ButtonGradientLowColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.navigationPaneLeftPane.ButtonHighlightGradientHighColor = System.Drawing.Color.White;
            this.navigationPaneLeftPane.ButtonHighlightGradientLowColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(165)))), ((int)(((byte)(78)))));
            this.navigationPaneLeftPane.CaptionBorderColor = System.Drawing.SystemColors.ActiveCaption;
            this.navigationPaneLeftPane.CaptionFont = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold);
            this.navigationPaneLeftPane.CaptionForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.navigationPaneLeftPane.CaptionGradientHighColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.navigationPaneLeftPane.CaptionGradientLowColor = System.Drawing.SystemColors.ActiveCaption;
            this.navigationPaneLeftPane.Controls.Add(this.navigationPanePageSlideLibrary);
            this.navigationPaneLeftPane.Controls.Add(this.navigationPanePageRegionLanguages);
            this.navigationPaneLeftPane.Controls.Add(this.navigationPanePageFilters);
            this.navigationPaneLeftPane.Cursor = System.Windows.Forms.Cursors.Default;
            this.navigationPaneLeftPane.Dock = System.Windows.Forms.DockStyle.Fill;
            this.navigationPaneLeftPane.FooterGradientHighColor = System.Drawing.SystemColors.ButtonHighlight;
            this.navigationPaneLeftPane.FooterGradientLowColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.navigationPaneLeftPane.FooterHeight = 30;
            this.navigationPaneLeftPane.FooterHighlightGradientHighColor = System.Drawing.Color.White;
            this.navigationPaneLeftPane.FooterHighlightGradientLowColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(165)))), ((int)(((byte)(78)))));
            this.navigationPaneLeftPane.Location = new System.Drawing.Point(0, 0);
            this.navigationPaneLeftPane.Name = "navigationPaneLeftPane";
            this.navigationPaneLeftPane.NavigationPages.AddRange(new Ascend.Windows.Forms.NavigationPanePage[] {
            this.navigationPanePageSlideLibrary,
            this.navigationPanePageRegionLanguages,
            this.navigationPanePageFilters});
            this.navigationPaneLeftPane.Size = new System.Drawing.Size(248, 625);
            this.navigationPaneLeftPane.TabIndex = 0;
            this.navigationPaneLeftPane.VisibleButtonCount = 3;
            // 
            // navigationPanePageSlideLibrary
            // 
            this.navigationPanePageSlideLibrary.ActiveGradientHighColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(225)))), ((int)(((byte)(155)))));
            this.navigationPanePageSlideLibrary.ActiveGradientLowColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(165)))), ((int)(((byte)(78)))));
            this.navigationPanePageSlideLibrary.AutoScroll = true;
            this.navigationPanePageSlideLibrary.BackColor = System.Drawing.Color.WhiteSmoke;
            this.navigationPanePageSlideLibrary.ButtonFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.navigationPanePageSlideLibrary.ButtonForeColor = System.Drawing.SystemColors.ControlText;
            this.navigationPanePageSlideLibrary.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F);
            this.navigationPanePageSlideLibrary.GradientHighColor = System.Drawing.SystemColors.ButtonHighlight;
            this.navigationPanePageSlideLibrary.GradientLowColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.navigationPanePageSlideLibrary.HighlightGradientHighColor = System.Drawing.Color.White;
            this.navigationPanePageSlideLibrary.HighlightGradientLowColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(165)))), ((int)(((byte)(78)))));
            this.navigationPanePageSlideLibrary.Image = global::Admin.Properties.Resources.MainPNG;
            this.navigationPanePageSlideLibrary.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.navigationPanePageSlideLibrary.ImageFooter = null;
            this.navigationPanePageSlideLibrary.ImageIndex = -1;
            this.navigationPanePageSlideLibrary.ImageIndexFooter = -1;
            this.navigationPanePageSlideLibrary.ImageKey = "";
            this.navigationPanePageSlideLibrary.ImageKeyFooter = "";
            this.navigationPanePageSlideLibrary.ImageList = null;
            this.navigationPanePageSlideLibrary.ImageListFooter = null;
            this.navigationPanePageSlideLibrary.Key = "SlideLibrary";
            this.navigationPanePageSlideLibrary.Location = new System.Drawing.Point(1, 27);
            this.navigationPanePageSlideLibrary.Name = "navigationPanePageSlideLibrary";
            this.navigationPanePageSlideLibrary.Size = new System.Drawing.Size(246, 464);
            this.navigationPanePageSlideLibrary.TabIndex = 1;
            this.navigationPanePageSlideLibrary.Text = "Slide Library";
            this.navigationPanePageSlideLibrary.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.navigationPanePageSlideLibrary.ToolTipText = null;
            // 
            // navigationPanePageRegionLanguages
            // 
            this.navigationPanePageRegionLanguages.ActiveGradientHighColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(225)))), ((int)(((byte)(155)))));
            this.navigationPanePageRegionLanguages.ActiveGradientLowColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(165)))), ((int)(((byte)(78)))));
            this.navigationPanePageRegionLanguages.AutoScroll = true;
            this.navigationPanePageRegionLanguages.ButtonFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.navigationPanePageRegionLanguages.ButtonForeColor = System.Drawing.SystemColors.ControlText;
            this.navigationPanePageRegionLanguages.Controls.Add(this.gradientPanel2);
            this.navigationPanePageRegionLanguages.Controls.Add(this.gradientCaption3);
            this.navigationPanePageRegionLanguages.Controls.Add(this.regionLanguageGroupGrid);
            this.navigationPanePageRegionLanguages.GradientHighColor = System.Drawing.SystemColors.ButtonHighlight;
            this.navigationPanePageRegionLanguages.GradientLowColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.navigationPanePageRegionLanguages.HighlightGradientHighColor = System.Drawing.Color.White;
            this.navigationPanePageRegionLanguages.HighlightGradientLowColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(165)))), ((int)(((byte)(78)))));
            this.navigationPanePageRegionLanguages.Image = global::Admin.Properties.Resources.RegionLanguagePNG;
            this.navigationPanePageRegionLanguages.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.navigationPanePageRegionLanguages.ImageFooter = null;
            this.navigationPanePageRegionLanguages.ImageIndex = -1;
            this.navigationPanePageRegionLanguages.ImageIndexFooter = -1;
            this.navigationPanePageRegionLanguages.ImageKey = "";
            this.navigationPanePageRegionLanguages.ImageKeyFooter = "";
            this.navigationPanePageRegionLanguages.ImageList = null;
            this.navigationPanePageRegionLanguages.ImageListFooter = null;
            this.navigationPanePageRegionLanguages.Key = "RegionLanguages";
            this.navigationPanePageRegionLanguages.Location = new System.Drawing.Point(1, 27);
            this.navigationPanePageRegionLanguages.Name = "navigationPanePageRegionLanguages";
            this.navigationPanePageRegionLanguages.Size = new System.Drawing.Size(246, 464);
            this.navigationPanePageRegionLanguages.TabIndex = 2;
            this.navigationPanePageRegionLanguages.Text = "Regions && Languages";
            this.navigationPanePageRegionLanguages.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.navigationPanePageRegionLanguages.ToolTipText = null;
            // 
            // gradientPanel2
            // 
            this.gradientPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gradientPanel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.gradientPanel2.Controls.Add(this.btnApplyRegionLanguageSettings);
            this.gradientPanel2.Controls.Add(this.btnToggleRegionLanguages);
            this.gradientPanel2.Controls.Add(this.btnCollapseAllRegions);
            this.gradientPanel2.Controls.Add(this.btnExpandAllRegions);
            this.gradientPanel2.Location = new System.Drawing.Point(-1, 384);
            this.gradientPanel2.Name = "gradientPanel2";
            this.gradientPanel2.Size = new System.Drawing.Size(247, 79);
            this.gradientPanel2.TabIndex = 7;
            // 
            // btnApplyRegionLanguageSettings
            // 
            this.btnApplyRegionLanguageSettings.Location = new System.Drawing.Point(10, 42);
            this.btnApplyRegionLanguageSettings.Name = "btnApplyRegionLanguageSettings";
            this.btnApplyRegionLanguageSettings.Size = new System.Drawing.Size(215, 23);
            this.btnApplyRegionLanguageSettings.TabIndex = 6;
            this.btnApplyRegionLanguageSettings.Text = "Apply Region/Language Settings";
            this.btnApplyRegionLanguageSettings.UseVisualStyleBackColor = true;
            this.btnApplyRegionLanguageSettings.Click += new System.EventHandler(this.btnApplyRegionLanguageSettings_Click);
            // 
            // btnToggleRegionLanguages
            // 
            this.btnToggleRegionLanguages.Image = global::Admin.Properties.Resources.GreenCheckPNG;
            this.btnToggleRegionLanguages.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnToggleRegionLanguages.Location = new System.Drawing.Point(12, 6);
            this.btnToggleRegionLanguages.Name = "btnToggleRegionLanguages";
            this.btnToggleRegionLanguages.Size = new System.Drawing.Size(104, 26);
            this.btnToggleRegionLanguages.TabIndex = 5;
            this.btnToggleRegionLanguages.Text = "Toggle Checks";
            this.btnToggleRegionLanguages.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnToggleRegionLanguages.UseVisualStyleBackColor = true;
            this.btnToggleRegionLanguages.Click += new System.EventHandler(this.btnToggleRegionLanguages_Click);
            // 
            // btnCollapseAllRegions
            // 
            this.btnCollapseAllRegions.Image = global::Admin.Properties.Resources.CollapsePNG;
            this.btnCollapseAllRegions.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCollapseAllRegions.Location = new System.Drawing.Point(166, 6);
            this.btnCollapseAllRegions.Name = "btnCollapseAllRegions";
            this.btnCollapseAllRegions.Size = new System.Drawing.Size(38, 26);
            this.btnCollapseAllRegions.TabIndex = 3;
            this.btnCollapseAllRegions.Text = "All";
            this.btnCollapseAllRegions.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCollapseAllRegions.UseVisualStyleBackColor = true;
            this.btnCollapseAllRegions.Click += new System.EventHandler(this.btnCollapseAllRegions_Click);
            // 
            // btnExpandAllRegions
            // 
            this.btnExpandAllRegions.Image = global::Admin.Properties.Resources.ExpandPNG;
            this.btnExpandAllRegions.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExpandAllRegions.Location = new System.Drawing.Point(122, 6);
            this.btnExpandAllRegions.Name = "btnExpandAllRegions";
            this.btnExpandAllRegions.Size = new System.Drawing.Size(38, 26);
            this.btnExpandAllRegions.TabIndex = 4;
            this.btnExpandAllRegions.Text = "All";
            this.btnExpandAllRegions.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExpandAllRegions.UseVisualStyleBackColor = true;
            this.btnExpandAllRegions.Click += new System.EventHandler(this.btnExpandAllRegions_Click);
            // 
            // gradientCaption3
            // 
            this.gradientCaption3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gradientCaption3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.gradientCaption3.Location = new System.Drawing.Point(0, 360);
            this.gradientCaption3.Name = "gradientCaption3";
            this.gradientCaption3.Size = new System.Drawing.Size(246, 24);
            this.gradientCaption3.TabIndex = 3;
            this.gradientCaption3.Text = "Actions";
            // 
            // regionLanguageGroupGrid
            // 
            this.regionLanguageGroupGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.regionLanguageGroupGrid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.regionLanguageGroupGrid.Location = new System.Drawing.Point(0, 0);
            this.regionLanguageGroupGrid.Name = "regionLanguageGroupGrid";
            this.regionLanguageGroupGrid.ShowControlBox = false;
            this.regionLanguageGroupGrid.Size = new System.Drawing.Size(246, 358);
            this.regionLanguageGroupGrid.TabIndex = 0;
            // 
            // navigationPanePageFilters
            // 
            this.navigationPanePageFilters.ActiveGradientHighColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(225)))), ((int)(((byte)(155)))));
            this.navigationPanePageFilters.ActiveGradientLowColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(165)))), ((int)(((byte)(78)))));
            this.navigationPanePageFilters.AutoScroll = true;
            this.navigationPanePageFilters.ButtonFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.navigationPanePageFilters.ButtonForeColor = System.Drawing.SystemColors.ControlText;
            this.navigationPanePageFilters.Controls.Add(this.gradientPanel3);
            this.navigationPanePageFilters.Controls.Add(this.gradientCaption4);
            this.navigationPanePageFilters.Controls.Add(this.gradientPanel4);
            this.navigationPanePageFilters.Controls.Add(this.gradientCaption5);
            this.navigationPanePageFilters.Controls.Add(this.filterTabs);
            this.navigationPanePageFilters.GradientHighColor = System.Drawing.SystemColors.ButtonHighlight;
            this.navigationPanePageFilters.GradientLowColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.navigationPanePageFilters.HighlightGradientHighColor = System.Drawing.Color.White;
            this.navigationPanePageFilters.HighlightGradientLowColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(165)))), ((int)(((byte)(78)))));
            this.navigationPanePageFilters.Image = global::Admin.Properties.Resources.FilterPNG;
            this.navigationPanePageFilters.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.navigationPanePageFilters.ImageFooter = null;
            this.navigationPanePageFilters.ImageIndex = -1;
            this.navigationPanePageFilters.ImageIndexFooter = -1;
            this.navigationPanePageFilters.ImageKey = "";
            this.navigationPanePageFilters.ImageKeyFooter = "";
            this.navigationPanePageFilters.ImageList = null;
            this.navigationPanePageFilters.ImageListFooter = null;
            this.navigationPanePageFilters.Key = "Filters";
            this.navigationPanePageFilters.Location = new System.Drawing.Point(1, 27);
            this.navigationPanePageFilters.Name = "navigationPanePageFilters";
            this.navigationPanePageFilters.Size = new System.Drawing.Size(246, 464);
            this.navigationPanePageFilters.TabIndex = 3;
            this.navigationPanePageFilters.Text = "Filters";
            this.navigationPanePageFilters.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.navigationPanePageFilters.ToolTipText = null;
            // 
            // gradientPanel3
            // 
            this.gradientPanel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gradientPanel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.gradientPanel3.Controls.Add(this.btnToggleFilters);
            this.gradientPanel3.Location = new System.Drawing.Point(-1, 279);
            this.gradientPanel3.Name = "gradientPanel3";
            this.gradientPanel3.Size = new System.Drawing.Size(247, 36);
            this.gradientPanel3.TabIndex = 11;
            // 
            // btnToggleFilters
            // 
            this.btnToggleFilters.Image = global::Admin.Properties.Resources.GreenCheckPNG;
            this.btnToggleFilters.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnToggleFilters.Location = new System.Drawing.Point(12, 6);
            this.btnToggleFilters.Name = "btnToggleFilters";
            this.btnToggleFilters.Size = new System.Drawing.Size(104, 26);
            this.btnToggleFilters.TabIndex = 5;
            this.btnToggleFilters.Text = "Toggle Checks";
            this.btnToggleFilters.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnToggleFilters.UseVisualStyleBackColor = true;
            this.btnToggleFilters.Click += new System.EventHandler(this.btnToggleFilters_Click);
            // 
            // gradientCaption4
            // 
            this.gradientCaption4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gradientCaption4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.gradientCaption4.Location = new System.Drawing.Point(0, 255);
            this.gradientCaption4.Name = "gradientCaption4";
            this.gradientCaption4.Size = new System.Drawing.Size(246, 24);
            this.gradientCaption4.TabIndex = 10;
            this.gradientCaption4.Text = "Actions";
            // 
            // gradientPanel4
            // 
            this.gradientPanel4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gradientPanel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.gradientPanel4.Controls.Add(this.btnApplyFilterSettings);
            this.gradientPanel4.Controls.Add(this.grpFilterApplyOptions);
            this.gradientPanel4.Location = new System.Drawing.Point(-1, 339);
            this.gradientPanel4.Name = "gradientPanel4";
            this.gradientPanel4.Size = new System.Drawing.Size(246, 125);
            this.gradientPanel4.TabIndex = 9;
            // 
            // btnApplyFilterSettings
            // 
            this.btnApplyFilterSettings.Location = new System.Drawing.Point(4, 96);
            this.btnApplyFilterSettings.Name = "btnApplyFilterSettings";
            this.btnApplyFilterSettings.Size = new System.Drawing.Size(215, 23);
            this.btnApplyFilterSettings.TabIndex = 6;
            this.btnApplyFilterSettings.Text = "Apply Filter Settings";
            this.btnApplyFilterSettings.UseVisualStyleBackColor = true;
            this.btnApplyFilterSettings.Click += new System.EventHandler(this.btnApplyFilterSettings_Click);
            // 
            // grpFilterApplyOptions
            // 
            this.grpFilterApplyOptions.Controls.Add(this.rdoFilterShowBoth);
            this.grpFilterApplyOptions.Controls.Add(this.rdoFilterShowMain);
            this.grpFilterApplyOptions.Controls.Add(this.rdoFilterShowRegLang);
            this.grpFilterApplyOptions.Location = new System.Drawing.Point(4, 6);
            this.grpFilterApplyOptions.Name = "grpFilterApplyOptions";
            this.grpFilterApplyOptions.Size = new System.Drawing.Size(215, 84);
            this.grpFilterApplyOptions.TabIndex = 2;
            this.grpFilterApplyOptions.TabStop = false;
            this.grpFilterApplyOptions.Text = "Apply Options";
            // 
            // rdoFilterShowBoth
            // 
            this.rdoFilterShowBoth.AutoSize = true;
            this.rdoFilterShowBoth.Location = new System.Drawing.Point(6, 65);
            this.rdoFilterShowBoth.Name = "rdoFilterShowBoth";
            this.rdoFilterShowBoth.Size = new System.Drawing.Size(206, 17);
            this.rdoFilterShowBoth.TabIndex = 1;
            this.rdoFilterShowBoth.TabStop = true;
            this.rdoFilterShowBoth.Text = "Show Main and Regions && Languages";
            this.rdoFilterShowBoth.UseVisualStyleBackColor = true;
            // 
            // rdoFilterShowMain
            // 
            this.rdoFilterShowMain.AutoSize = true;
            this.rdoFilterShowMain.Location = new System.Drawing.Point(6, 42);
            this.rdoFilterShowMain.Name = "rdoFilterShowMain";
            this.rdoFilterShowMain.Size = new System.Drawing.Size(78, 17);
            this.rdoFilterShowMain.TabIndex = 0;
            this.rdoFilterShowMain.Text = "Show Main";
            this.rdoFilterShowMain.UseVisualStyleBackColor = true;
            // 
            // rdoFilterShowRegLang
            // 
            this.rdoFilterShowRegLang.AutoSize = true;
            this.rdoFilterShowRegLang.Checked = true;
            this.rdoFilterShowRegLang.Location = new System.Drawing.Point(6, 19);
            this.rdoFilterShowRegLang.Name = "rdoFilterShowRegLang";
            this.rdoFilterShowRegLang.Size = new System.Drawing.Size(159, 17);
            this.rdoFilterShowRegLang.TabIndex = 2;
            this.rdoFilterShowRegLang.TabStop = true;
            this.rdoFilterShowRegLang.Text = "Show Regions && Languages";
            this.rdoFilterShowRegLang.UseVisualStyleBackColor = true;
            // 
            // gradientCaption5
            // 
            this.gradientCaption5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gradientCaption5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.gradientCaption5.Location = new System.Drawing.Point(-1, 315);
            this.gradientCaption5.Name = "gradientCaption5";
            this.gradientCaption5.Size = new System.Drawing.Size(246, 24);
            this.gradientCaption5.TabIndex = 8;
            this.gradientCaption5.Text = "Apply";
            // 
            // filterTabs
            // 
            this.filterTabs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.filterTabs.Location = new System.Drawing.Point(0, 0);
            this.filterTabs.Name = "filterTabs";
            this.filterTabs.Size = new System.Drawing.Size(246, 464);
            this.filterTabs.TabIndex = 0;
            // 
            // splInfo
            // 
            this.splInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splInfo.Location = new System.Drawing.Point(0, 0);
            this.splInfo.Name = "splInfo";
            this.splInfo.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splInfo.Panel1
            // 
            this.splInfo.Panel1.Controls.Add(this.dgvSlides);
            this.splInfo.Panel1.Controls.Add(this.lstSlideAffiliations);
            this.splInfo.Panel1.Controls.Add(this.bindingNavigatorSlideList);
            // 
            // splInfo.Panel2
            // 
            this.splInfo.Panel2.Controls.Add(this.webSlideViewer);
            this.splInfo.Size = new System.Drawing.Size(622, 625);
            this.splInfo.SplitterDistance = 276;
            this.splInfo.TabIndex = 0;
            // 
            // dgvSlides
            // 
            this.dgvSlides.AllowUserToAddRows = false;
            this.dgvSlides.AllowUserToDeleteRows = false;
            this.dgvSlides.AllowUserToResizeRows = false;
            this.dgvSlides.AutoGenerateColumns = false;
            this.dgvSlides.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSlides.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.slideIDDataGridViewTextBoxColumn,
            this.titleDataGridViewTextBoxColumn,
            this.fileNameDataGridViewTextBoxColumn,
            this.DateModified,
            this.NumInLib,
            this.DateRemoved});
            this.dgvSlides.ContextMenuStrip = this.cmnSlideList;
            this.dgvSlides.DataSource = this.bindSlideGrid;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSlides.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvSlides.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvSlides.Location = new System.Drawing.Point(0, 25);
            this.dgvSlides.Name = "dgvSlides";
            this.dgvSlides.ReadOnly = true;
            this.dgvSlides.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.dgvSlides.RowHeadersWidth = 20;
            this.dgvSlides.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvSlides.RowTemplate.ContextMenuStrip = this.cmnSlide;
            this.dgvSlides.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSlides.Size = new System.Drawing.Size(622, 195);
            this.dgvSlides.TabIndex = 1;
            this.dgvSlides.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSlides_CellMouseEnter);
            this.dgvSlides.ColumnSortModeChanged += new System.Windows.Forms.DataGridViewColumnEventHandler(this.dgvSlides_ColumnSortModeChanged);
            this.dgvSlides.SelectionChanged += new System.EventHandler(this.dgvSlides_SelectionChanged);
            this.dgvSlides.Sorted += new System.EventHandler(this.dgvSlides_Sorted);
            this.dgvSlides.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dgvSlides_MouseDown);
            // 
            // slideIDDataGridViewTextBoxColumn
            // 
            this.slideIDDataGridViewTextBoxColumn.DataPropertyName = "SlideID";
            this.slideIDDataGridViewTextBoxColumn.HeaderText = "SlideID";
            this.slideIDDataGridViewTextBoxColumn.Name = "slideIDDataGridViewTextBoxColumn";
            this.slideIDDataGridViewTextBoxColumn.ReadOnly = true;
            this.slideIDDataGridViewTextBoxColumn.Visible = false;
            // 
            // titleDataGridViewTextBoxColumn
            // 
            this.titleDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.titleDataGridViewTextBoxColumn.DataPropertyName = "Title";
            this.titleDataGridViewTextBoxColumn.FillWeight = 33F;
            this.titleDataGridViewTextBoxColumn.HeaderText = "Title";
            this.titleDataGridViewTextBoxColumn.Name = "titleDataGridViewTextBoxColumn";
            this.titleDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // fileNameDataGridViewTextBoxColumn
            // 
            this.fileNameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.fileNameDataGridViewTextBoxColumn.DataPropertyName = "FileName";
            this.fileNameDataGridViewTextBoxColumn.FillWeight = 33F;
            this.fileNameDataGridViewTextBoxColumn.HeaderText = "FileName";
            this.fileNameDataGridViewTextBoxColumn.Name = "fileNameDataGridViewTextBoxColumn";
            this.fileNameDataGridViewTextBoxColumn.ReadOnly = true;
            this.fileNameDataGridViewTextBoxColumn.Width = 76;
            // 
            // DateModified
            // 
            this.DateModified.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.DateModified.DataPropertyName = "DateModified";
            dataGridViewCellStyle1.Format = "g";
            dataGridViewCellStyle1.NullValue = null;
            this.DateModified.DefaultCellStyle = dataGridViewCellStyle1;
            this.DateModified.FillWeight = 17F;
            this.DateModified.HeaderText = "Date Modified";
            this.DateModified.Name = "DateModified";
            this.DateModified.ReadOnly = true;
            this.DateModified.Width = 98;
            // 
            // NumInLib
            // 
            this.NumInLib.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.NumInLib.DataPropertyName = "NumInLib";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.NumInLib.DefaultCellStyle = dataGridViewCellStyle2;
            this.NumInLib.FillWeight = 16F;
            this.NumInLib.HeaderText = "#";
            this.NumInLib.Name = "NumInLib";
            this.NumInLib.ReadOnly = true;
            this.NumInLib.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.NumInLib.Width = 39;
            // 
            // DateRemoved
            // 
            this.DateRemoved.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.DateRemoved.DataPropertyName = "DateRemoved";
            this.DateRemoved.FillWeight = 17F;
            this.DateRemoved.HeaderText = "Removed";
            this.DateRemoved.Name = "DateRemoved";
            this.DateRemoved.ReadOnly = true;
            this.DateRemoved.Width = 78;
            // 
            // cmnSlideList
            // 
            this.cmnSlideList.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miSlideListNewSlide});
            this.cmnSlideList.Name = "cmnSlideList";
            this.cmnSlideList.Size = new System.Drawing.Size(130, 26);
            // 
            // miSlideListNewSlide
            // 
            this.miSlideListNewSlide.Name = "miSlideListNewSlide";
            this.miSlideListNewSlide.Size = new System.Drawing.Size(129, 22);
            this.miSlideListNewSlide.Text = "New  Slide";
            // 
            // bindSlideGrid
            // 
            this.bindSlideGrid.DataMember = "Slide";
            this.bindSlideGrid.DataSource = this.dsSlideLibrary;
            this.bindSlideGrid.Sort = "";
            // 
            // dsSlideLibrary
            // 
            this.dsSlideLibrary.DataSetName = "SlideLibrary";
            this.dsSlideLibrary.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // cmnSlide
            // 
            this.cmnSlide.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miSlideUpdate,
            this.miSlideDelete,
            this.miSlideResetFiltersAndOrRegionLanguages,
            this.miSlideResetFilters,
            this.miSlideResetRegionLanguages});
            this.cmnSlide.Name = "cmnSlide";
            this.cmnSlide.Size = new System.Drawing.Size(294, 114);
            // 
            // miSlideUpdate
            // 
            this.miSlideUpdate.Name = "miSlideUpdate";
            this.miSlideUpdate.Size = new System.Drawing.Size(293, 22);
            this.miSlideUpdate.Text = "Update Slide";
            this.miSlideUpdate.Click += new System.EventHandler(this.miMainSlideUpdateSlide_Click);
            // 
            // miSlideDelete
            // 
            this.miSlideDelete.Name = "miSlideDelete";
            this.miSlideDelete.Size = new System.Drawing.Size(293, 22);
            this.miSlideDelete.Text = "Delete Slide(s)";
            this.miSlideDelete.Click += new System.EventHandler(this.miMainSlideDeleteSlide_Click);
            // 
            // miSlideResetFiltersAndOrRegionLanguages
            // 
            this.miSlideResetFiltersAndOrRegionLanguages.Name = "miSlideResetFiltersAndOrRegionLanguages";
            this.miSlideResetFiltersAndOrRegionLanguages.Size = new System.Drawing.Size(293, 22);
            this.miSlideResetFiltersAndOrRegionLanguages.Text = "Reset Slide Filter and Region/Languages...";
            this.miSlideResetFiltersAndOrRegionLanguages.Visible = false;
            this.miSlideResetFiltersAndOrRegionLanguages.Click += new System.EventHandler(this.miSlideResetFiltersAndOrRegionLanguages_Click);
            // 
            // miSlideResetFilters
            // 
            this.miSlideResetFilters.Name = "miSlideResetFilters";
            this.miSlideResetFilters.Size = new System.Drawing.Size(293, 22);
            this.miSlideResetFilters.Text = "Reset Slide Filter...";
            this.miSlideResetFilters.Visible = false;
            this.miSlideResetFilters.Click += new System.EventHandler(this.miSlideResetFilters_Click);
            // 
            // miSlideResetRegionLanguages
            // 
            this.miSlideResetRegionLanguages.Name = "miSlideResetRegionLanguages";
            this.miSlideResetRegionLanguages.Size = new System.Drawing.Size(293, 22);
            this.miSlideResetRegionLanguages.Text = "Reset Slide Region/Languages...";
            this.miSlideResetRegionLanguages.Visible = false;
            this.miSlideResetRegionLanguages.Click += new System.EventHandler(this.miSlideRegionLanguages_Click);
            // 
            // lstSlideAffiliations
            // 
            this.lstSlideAffiliations.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.lstSlideAffiliations.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lstSlideAffiliations.FormattingEnabled = true;
            this.lstSlideAffiliations.Location = new System.Drawing.Point(0, 220);
            this.lstSlideAffiliations.Name = "lstSlideAffiliations";
            this.lstSlideAffiliations.Size = new System.Drawing.Size(622, 56);
            this.lstSlideAffiliations.TabIndex = 1;
            // 
            // bindingNavigatorSlideList
            // 
            this.bindingNavigatorSlideList.AddNewItem = null;
            this.bindingNavigatorSlideList.BindingSource = this.bindSlideGrid;
            this.bindingNavigatorSlideList.CountItem = this.bindingNavigatorCountItem;
            this.bindingNavigatorSlideList.DeleteItem = null;
            this.bindingNavigatorSlideList.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.bindingNavigatorSlideList.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.toolStripComboBoxRegionLanguage,
            this.toolStripButtonNewSlide,
            this.toolStripButtonUpdateSlide,
            this.toolStripButtonDeleteSlide,
            this.toolStripSeparator1});
            this.bindingNavigatorSlideList.Location = new System.Drawing.Point(0, 0);
            this.bindingNavigatorSlideList.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.bindingNavigatorSlideList.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.bindingNavigatorSlideList.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.bindingNavigatorSlideList.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.bindingNavigatorSlideList.Name = "bindingNavigatorSlideList";
            this.bindingNavigatorSlideList.PositionItem = this.bindingNavigatorPositionItem;
            this.bindingNavigatorSlideList.Size = new System.Drawing.Size(622, 25);
            this.bindingNavigatorSlideList.TabIndex = 0;
            this.bindingNavigatorSlideList.Text = "bindingNavigator1";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 21);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripComboBoxRegionLanguage
            // 
            this.toolStripComboBoxRegionLanguage.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripComboBoxRegionLanguage.Name = "toolStripComboBoxRegionLanguage";
            this.toolStripComboBoxRegionLanguage.Size = new System.Drawing.Size(225, 25);
            this.toolStripComboBoxRegionLanguage.SelectedIndexChanged += new System.EventHandler(this.toolStripComboBoxRegionLanguage_SelectedIndexChanged);
            // 
            // toolStripButtonNewSlide
            // 
            this.toolStripButtonNewSlide.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonNewSlide.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonNewSlide.Image")));
            this.toolStripButtonNewSlide.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonNewSlide.Name = "toolStripButtonNewSlide";
            this.toolStripButtonNewSlide.Size = new System.Drawing.Size(35, 22);
            this.toolStripButtonNewSlide.Text = "New";
            this.toolStripButtonNewSlide.ToolTipText = "New Slide";
            this.toolStripButtonNewSlide.Click += new System.EventHandler(this.toolStripButtonNewSlide_Click);
            // 
            // toolStripButtonUpdateSlide
            // 
            this.toolStripButtonUpdateSlide.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonUpdateSlide.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonUpdateSlide.Image")));
            this.toolStripButtonUpdateSlide.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonUpdateSlide.Name = "toolStripButtonUpdateSlide";
            this.toolStripButtonUpdateSlide.Size = new System.Drawing.Size(49, 22);
            this.toolStripButtonUpdateSlide.Text = "Update";
            this.toolStripButtonUpdateSlide.ToolTipText = "Update Slide";
            this.toolStripButtonUpdateSlide.Click += new System.EventHandler(this.toolStripButtonUpdateSlide_Click);
            // 
            // toolStripButtonDeleteSlide
            // 
            this.toolStripButtonDeleteSlide.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonDeleteSlide.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonDeleteSlide.Image")));
            this.toolStripButtonDeleteSlide.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonDeleteSlide.Name = "toolStripButtonDeleteSlide";
            this.toolStripButtonDeleteSlide.Size = new System.Drawing.Size(44, 22);
            this.toolStripButtonDeleteSlide.Text = "Delete";
            this.toolStripButtonDeleteSlide.ToolTipText = "Delete Slide(s)";
            this.toolStripButtonDeleteSlide.Click += new System.EventHandler(this.toolStripButtonDeleteSlide_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // webSlideViewer
            // 
            this.webSlideViewer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webSlideViewer.Location = new System.Drawing.Point(0, 0);
            this.webSlideViewer.MinimumSize = new System.Drawing.Size(20, 20);
            this.webSlideViewer.Name = "webSlideViewer";
            this.webSlideViewer.Size = new System.Drawing.Size(622, 345);
            this.webSlideViewer.TabIndex = 0;
            this.webSlideViewer.ProgressChanged += new System.Windows.Forms.WebBrowserProgressChangedEventHandler(this.webSlideViewer_ProgressChanged);
            // 
            // cmnCategory
            // 
            this.cmnCategory.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miCategoryCategoryAdd,
            this.miCategoryEdit,
            this.miCategoryDeleteCat,
            this.miCategoryRemoveSlide});
            this.cmnCategory.Name = "cmnCategory";
            this.cmnCategory.Size = new System.Drawing.Size(228, 92);
            this.cmnCategory.Opening += new System.ComponentModel.CancelEventHandler(this.cmnCategory_Opening);
            // 
            // miCategoryCategoryAdd
            // 
            this.miCategoryCategoryAdd.Name = "miCategoryCategoryAdd";
            this.miCategoryCategoryAdd.Size = new System.Drawing.Size(227, 22);
            this.miCategoryCategoryAdd.Text = "Add Category";
            this.miCategoryCategoryAdd.Click += new System.EventHandler(this.miCategoryCategoryAdd_Click);
            // 
            // miCategoryEdit
            // 
            this.miCategoryEdit.Name = "miCategoryEdit";
            this.miCategoryEdit.Size = new System.Drawing.Size(227, 22);
            this.miCategoryEdit.Text = "Edit Selected Category";
            this.miCategoryEdit.Click += new System.EventHandler(this.miCategoryEdit_Click);
            // 
            // miCategoryDeleteCat
            // 
            this.miCategoryDeleteCat.Name = "miCategoryDeleteCat";
            this.miCategoryDeleteCat.Size = new System.Drawing.Size(227, 22);
            this.miCategoryDeleteCat.Text = "Delete Selected Category";
            this.miCategoryDeleteCat.Click += new System.EventHandler(this.miCategoryDeleteCat_Click);
            // 
            // miCategoryRemoveSlide
            // 
            this.miCategoryRemoveSlide.BackColor = System.Drawing.SystemColors.Control;
            this.miCategoryRemoveSlide.Name = "miCategoryRemoveSlide";
            this.miCategoryRemoveSlide.Size = new System.Drawing.Size(227, 22);
            this.miCategoryRemoveSlide.Text = "Remove Slide From Category";
            this.miCategoryRemoveSlide.Click += new System.EventHandler(this.miCategoryRemoveSlide_Click);
            // 
            // imlIcons16
            // 
            this.imlIcons16.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imlIcons16.ImageStream")));
            this.imlIcons16.TransparentColor = System.Drawing.Color.Transparent;
            this.imlIcons16.Images.SetKeyName(0, "Folder.ico");
            this.imlIcons16.Images.SetKeyName(1, "color-printing.ico");
            this.imlIcons16.Images.SetKeyName(2, "digital-imaging.ico");
            this.imlIcons16.Images.SetKeyName(3, "large-format.ico");
            this.imlIcons16.Images.SetKeyName(4, "MFP-AIO.ico");
            this.imlIcons16.Images.SetKeyName(5, "mono-printing.ico");
            this.imlIcons16.Images.SetKeyName(6, "multi-slides.ico");
            this.imlIcons16.Images.SetKeyName(7, "my-slide-decks.ico");
            this.imlIcons16.Images.SetKeyName(8, "single-slide.ico");
            this.imlIcons16.Images.SetKeyName(9, "Slide-Folder.ico");
            this.imlIcons16.Images.SetKeyName(10, "Multi-Slide-Folder.ico");
            this.imlIcons16.Images.SetKeyName(11, "my-presentations.ico");
            this.imlIcons16.Images.SetKeyName(12, "slide-library.ico");
            this.imlIcons16.Images.SetKeyName(13, "tiny_folder_brn.ico");
            this.imlIcons16.Images.SetKeyName(14, "tiny_folder_drk_gry.ico");
            this.imlIcons16.Images.SetKeyName(15, "tiny_folder_drk-blu.ico");
            this.imlIcons16.Images.SetKeyName(16, "tiny_folder_drk-grn.ico");
            this.imlIcons16.Images.SetKeyName(17, "tiny_folder_lgt_gry.ico");
            this.imlIcons16.Images.SetKeyName(18, "tiny_folder_lgt-blu.ico");
            this.imlIcons16.Images.SetKeyName(19, "tiny_folder_lgt-grn.ico");
            this.imlIcons16.Images.SetKeyName(20, "tiny_folder_magenta.ico");
            this.imlIcons16.Images.SetKeyName(21, "tiny_folder_med-blu.ico");
            this.imlIcons16.Images.SetKeyName(22, "tiny_folder_orng.ico");
            this.imlIcons16.Images.SetKeyName(23, "tiny_folder_red.ico");
            this.imlIcons16.Images.SetKeyName(24, "tiny_folder_ylo.ico");
            this.imlIcons16.Images.SetKeyName(25, "tiny_folder_298527.ico");
            this.imlIcons16.Images.SetKeyName(26, "tiny_folder_246147.ico");
            this.imlIcons16.Images.SetKeyName(27, "ylw_242_171_1.ico");
            this.imlIcons16.Images.SetKeyName(28, "brwn_162_60_6.ico");
            this.imlIcons16.Images.SetKeyName(29, "drkblu_83_100_203.ico");
            this.imlIcons16.Images.SetKeyName(30, "drkgrn_36_97_71.ico");
            this.imlIcons16.Images.SetKeyName(31, "ltblu_0_152_246.ico");
            this.imlIcons16.Images.SetKeyName(32, "ltgrn_100_185_0.ico");
            this.imlIcons16.Images.SetKeyName(33, "mdgrn_41_133_39.ico");
            this.imlIcons16.Images.SetKeyName(34, "mdgry_123_123_121.ico");
            this.imlIcons16.Images.SetKeyName(35, "medblu_0_113_180.ico");
            this.imlIcons16.Images.SetKeyName(36, "mgnta_204_0_102.ico");
            this.imlIcons16.Images.SetKeyName(37, "orng_235_95_1.ico");
            this.imlIcons16.Images.SetKeyName(38, "red_176_28_46.ico");
            // 
            // mnuMain
            // 
            this.mnuMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miMainFile,
            this.miMainModules,
            this.miMainUserInfo,
            this.miMainUpdates,
            this.miMainSlide,
            this.miMainData});
            this.mnuMain.Location = new System.Drawing.Point(0, 0);
            this.mnuMain.Name = "mnuMain";
            this.mnuMain.Size = new System.Drawing.Size(874, 24);
            this.mnuMain.TabIndex = 4;
            this.mnuMain.Text = "menuStrip1";
            // 
            // miMainFile
            // 
            this.miMainFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miMainHelp,
            this.miMainExit});
            this.miMainFile.Name = "miMainFile";
            this.miMainFile.Size = new System.Drawing.Size(37, 20);
            this.miMainFile.Text = "File";
            // 
            // miMainHelp
            // 
            this.miMainHelp.Name = "miMainHelp";
            this.miMainHelp.Size = new System.Drawing.Size(99, 22);
            this.miMainHelp.Text = "Help";
            this.miMainHelp.Click += new System.EventHandler(this.miMainHelp_Click);
            // 
            // miMainExit
            // 
            this.miMainExit.Name = "miMainExit";
            this.miMainExit.Size = new System.Drawing.Size(99, 22);
            this.miMainExit.Text = "Exit";
            this.miMainExit.Click += new System.EventHandler(this.miMainExit_Click);
            // 
            // miMainModules
            // 
            this.miMainModules.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miMainModulesSupportFiles,
            this.supportFileBatchUploadToolStripMenuItem,
            this.miFilterToolStrip,
            this.miMainModulesAnnouncementManagement});
            this.miMainModules.Name = "miMainModules";
            this.miMainModules.Size = new System.Drawing.Size(65, 20);
            this.miMainModules.Text = "Modules";
            // 
            // miMainModulesSupportFiles
            // 
            this.miMainModulesSupportFiles.Name = "miMainModulesSupportFiles";
            this.miMainModulesSupportFiles.Size = new System.Drawing.Size(240, 22);
            this.miMainModulesSupportFiles.Text = "Support Files...";
            this.miMainModulesSupportFiles.Click += new System.EventHandler(this.miMainModulesSupportFiles_Click);
            // 
            // supportFileBatchUploadToolStripMenuItem
            // 
            this.supportFileBatchUploadToolStripMenuItem.Name = "supportFileBatchUploadToolStripMenuItem";
            this.supportFileBatchUploadToolStripMenuItem.Size = new System.Drawing.Size(240, 22);
            this.supportFileBatchUploadToolStripMenuItem.Text = "Support File Batch Upload...";
            this.supportFileBatchUploadToolStripMenuItem.Click += new System.EventHandler(this.supportFileBatchUploadToolStripMenuItem_Click);
            // 
            // miFilterToolStrip
            // 
            this.miFilterToolStrip.Name = "miFilterToolStrip";
            this.miFilterToolStrip.Size = new System.Drawing.Size(240, 22);
            this.miFilterToolStrip.Text = "Filter Management...";
            this.miFilterToolStrip.Visible = false;
            this.miFilterToolStrip.Click += new System.EventHandler(this.miFilterManagement_Click);
            // 
            // miMainModulesAnnouncementManagement
            // 
            this.miMainModulesAnnouncementManagement.Name = "miMainModulesAnnouncementManagement";
            this.miMainModulesAnnouncementManagement.Size = new System.Drawing.Size(240, 22);
            this.miMainModulesAnnouncementManagement.Text = "Announcement Management...";
            this.miMainModulesAnnouncementManagement.Click += new System.EventHandler(this.miMainModulesAnnouncementManagement_Click);
            // 
            // miMainUserInfo
            // 
            this.miMainUserInfo.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miMainUserInfoDataUpdates,
            this.miMainUserInfoRegistrants});
            this.miMainUserInfo.Name = "miMainUserInfo";
            this.miMainUserInfo.Size = new System.Drawing.Size(66, 20);
            this.miMainUserInfo.Text = "User Info";
            // 
            // miMainUserInfoDataUpdates
            // 
            this.miMainUserInfoDataUpdates.Name = "miMainUserInfoDataUpdates";
            this.miMainUserInfoDataUpdates.Size = new System.Drawing.Size(153, 22);
            this.miMainUserInfoDataUpdates.Text = "Data Updates...";
            this.miMainUserInfoDataUpdates.Click += new System.EventHandler(this.miMainUserInfoDataUpdates_Click);
            // 
            // miMainUserInfoRegistrants
            // 
            this.miMainUserInfoRegistrants.Name = "miMainUserInfoRegistrants";
            this.miMainUserInfoRegistrants.Size = new System.Drawing.Size(153, 22);
            this.miMainUserInfoRegistrants.Text = "Registrants...";
            this.miMainUserInfoRegistrants.Click += new System.EventHandler(this.miMainUserInfoRegistrants_Click);
            // 
            // miMainUpdates
            // 
            this.miMainUpdates.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miMainUpdatesPublishLocalizedContentUpdate,
            this.miMainUpdatesPublishLocalizedWebContentUpdate,
            this.miMainUpdatePublishContentUpdate});
            this.miMainUpdates.Name = "miMainUpdates";
            this.miMainUpdates.Size = new System.Drawing.Size(58, 20);
            this.miMainUpdates.Text = "Publish";
            // 
            // miMainUpdatesPublishLocalizedContentUpdate
            // 
            this.miMainUpdatesPublishLocalizedContentUpdate.Checked = true;
            this.miMainUpdatesPublishLocalizedContentUpdate.CheckOnClick = true;
            this.miMainUpdatesPublishLocalizedContentUpdate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.miMainUpdatesPublishLocalizedContentUpdate.Name = "miMainUpdatesPublishLocalizedContentUpdate";
            this.miMainUpdatesPublishLocalizedContentUpdate.Size = new System.Drawing.Size(163, 22);
            this.miMainUpdatesPublishLocalizedContentUpdate.Text = "Desktop Content";
            this.miMainUpdatesPublishLocalizedContentUpdate.Click += new System.EventHandler(this.miMainUpdatesPublishLocalizedContentUpdate_Click);
            // 
            // miMainUpdatesPublishLocalizedWebContentUpdate
            // 
            this.miMainUpdatesPublishLocalizedWebContentUpdate.Checked = true;
            this.miMainUpdatesPublishLocalizedWebContentUpdate.CheckOnClick = true;
            this.miMainUpdatesPublishLocalizedWebContentUpdate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.miMainUpdatesPublishLocalizedWebContentUpdate.Name = "miMainUpdatesPublishLocalizedWebContentUpdate";
            this.miMainUpdatesPublishLocalizedWebContentUpdate.Size = new System.Drawing.Size(163, 22);
            this.miMainUpdatesPublishLocalizedWebContentUpdate.Text = "Web Content";
            this.miMainUpdatesPublishLocalizedWebContentUpdate.Click += new System.EventHandler(this.miMainUpdatesPublishLocalizedWebContentUpdate_Click);
            // 
            // miMainUpdatePublishContentUpdate
            // 
            this.miMainUpdatePublishContentUpdate.Name = "miMainUpdatePublishContentUpdate";
            this.miMainUpdatePublishContentUpdate.Size = new System.Drawing.Size(163, 22);
            this.miMainUpdatePublishContentUpdate.Text = "Publish Now";
            this.miMainUpdatePublishContentUpdate.Click += new System.EventHandler(this.miMainUpdatePublishContentUpdate_Click);
            // 
            // miMainSlide
            // 
            this.miMainSlide.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miMainSlideNewSlide,
            this.miMainSlideUpdateSlide,
            this.miMainSlideDeleteSlide,
            this.miMainSlideUpdateSlideFilterOrRegionLanguages,
            this.miMainSlideUpdateSlideFilter,
            this.miMainSlideUpdateSlideRegionLanguages,
            this.toolStripMenuItem1});
            this.miMainSlide.Name = "miMainSlide";
            this.miMainSlide.Size = new System.Drawing.Size(44, 20);
            this.miMainSlide.Text = "Slide";
            // 
            // miMainSlideNewSlide
            // 
            this.miMainSlideNewSlide.Name = "miMainSlideNewSlide";
            this.miMainSlideNewSlide.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.miMainSlideNewSlide.Size = new System.Drawing.Size(293, 22);
            this.miMainSlideNewSlide.Text = "New Slide...";
            this.miMainSlideNewSlide.Click += new System.EventHandler(this.miMainSlideNewSlide_Click);
            // 
            // miMainSlideUpdateSlide
            // 
            this.miMainSlideUpdateSlide.Name = "miMainSlideUpdateSlide";
            this.miMainSlideUpdateSlide.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.U)));
            this.miMainSlideUpdateSlide.Size = new System.Drawing.Size(293, 22);
            this.miMainSlideUpdateSlide.Text = "Update Slide...";
            this.miMainSlideUpdateSlide.Click += new System.EventHandler(this.miMainSlideUpdateSlide_Click);
            // 
            // miMainSlideDeleteSlide
            // 
            this.miMainSlideDeleteSlide.Name = "miMainSlideDeleteSlide";
            this.miMainSlideDeleteSlide.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.miMainSlideDeleteSlide.Size = new System.Drawing.Size(293, 22);
            this.miMainSlideDeleteSlide.Text = "Delete Slide(s)...";
            this.miMainSlideDeleteSlide.Click += new System.EventHandler(this.miMainSlideDeleteSlide_Click);
            // 
            // miMainSlideUpdateSlideFilterOrRegionLanguages
            // 
            this.miMainSlideUpdateSlideFilterOrRegionLanguages.Name = "miMainSlideUpdateSlideFilterOrRegionLanguages";
            this.miMainSlideUpdateSlideFilterOrRegionLanguages.Size = new System.Drawing.Size(293, 22);
            this.miMainSlideUpdateSlideFilterOrRegionLanguages.Text = "Reset Slide Filter and Region/Languages...";
            this.miMainSlideUpdateSlideFilterOrRegionLanguages.Visible = false;
            this.miMainSlideUpdateSlideFilterOrRegionLanguages.Click += new System.EventHandler(this.miMainSlideUpdateSlideFilterOrRegionLanguages_Click);
            // 
            // miMainSlideUpdateSlideFilter
            // 
            this.miMainSlideUpdateSlideFilter.Name = "miMainSlideUpdateSlideFilter";
            this.miMainSlideUpdateSlideFilter.Size = new System.Drawing.Size(293, 22);
            this.miMainSlideUpdateSlideFilter.Text = "Reset Slide Filter...";
            this.miMainSlideUpdateSlideFilter.Visible = false;
            this.miMainSlideUpdateSlideFilter.Click += new System.EventHandler(this.miMainSlideUpdateSlideFilter_Click);
            // 
            // miMainSlideUpdateSlideRegionLanguages
            // 
            this.miMainSlideUpdateSlideRegionLanguages.Name = "miMainSlideUpdateSlideRegionLanguages";
            this.miMainSlideUpdateSlideRegionLanguages.Size = new System.Drawing.Size(293, 22);
            this.miMainSlideUpdateSlideRegionLanguages.Text = "Reset Slide Region/Languages...";
            this.miMainSlideUpdateSlideRegionLanguages.Visible = false;
            this.miMainSlideUpdateSlideRegionLanguages.Click += new System.EventHandler(this.miMainSlideUpdateSlideRegionLanguages_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(293, 22);
            this.toolStripMenuItem1.Text = "Upload Deck...";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // miMainData
            // 
            this.miMainData.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miMainDataRefreshData});
            this.miMainData.Name = "miMainData";
            this.miMainData.Size = new System.Drawing.Size(43, 20);
            this.miMainData.Text = "Data";
            // 
            // miMainDataRefreshData
            // 
            this.miMainDataRefreshData.Name = "miMainDataRefreshData";
            this.miMainDataRefreshData.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.R)));
            this.miMainDataRefreshData.Size = new System.Drawing.Size(181, 22);
            this.miMainDataRefreshData.Text = "Refresh Data";
            this.miMainDataRefreshData.Click += new System.EventHandler(this.miMainDataRefreshData_Click);
            // 
            // stsBottom
            // 
            this.stsBottom.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatusMain});
            this.stsBottom.Location = new System.Drawing.Point(0, 655);
            this.stsBottom.Name = "stsBottom";
            this.stsBottom.Size = new System.Drawing.Size(874, 22);
            this.stsBottom.TabIndex = 6;
            this.stsBottom.Text = "statusStrip1";
            // 
            // lblStatusMain
            // 
            this.lblStatusMain.Name = "lblStatusMain";
            this.lblStatusMain.Size = new System.Drawing.Size(0, 17);
            // 
            // cmnRegionLanguageCategory
            // 
            this.cmnRegionLanguageCategory.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miRegionLanguageCategoryRemoveSlide});
            this.cmnRegionLanguageCategory.Name = "cmnRegionLanguageCategory";
            this.cmnRegionLanguageCategory.Size = new System.Drawing.Size(309, 26);
            // 
            // miRegionLanguageCategoryRemoveSlide
            // 
            this.miRegionLanguageCategoryRemoveSlide.Name = "miRegionLanguageCategoryRemoveSlide";
            this.miRegionLanguageCategoryRemoveSlide.Size = new System.Drawing.Size(308, 22);
            this.miRegionLanguageCategoryRemoveSlide.Text = "Update Slide Filter and/or Region Languages";
            this.miRegionLanguageCategoryRemoveSlide.Click += new System.EventHandler(this.miRegionLanguageCategoryRemoveSlide_Click);
            // 
            // AdminMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(874, 677);
            this.Controls.Add(this.stsBottom);
            this.Controls.Add(this.mnuMain);
            this.Controls.Add(this.splMain);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.mnuMain;
            this.Name = "AdminMain";
            this.Text = "HP Presentation Builder Admin";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.AdminMain_FormClosed);
            this.Load += new System.EventHandler(this.AdminMain_Load);
            this.splMain.Panel1.ResumeLayout(false);
            this.splMain.Panel2.ResumeLayout(false);
            this.splMain.ResumeLayout(false);
            this.navigationPaneLeftPane.ResumeLayout(false);
            this.navigationPanePageRegionLanguages.ResumeLayout(false);
            this.gradientPanel2.ResumeLayout(false);
            this.navigationPanePageFilters.ResumeLayout(false);
            this.gradientPanel3.ResumeLayout(false);
            this.gradientPanel4.ResumeLayout(false);
            this.grpFilterApplyOptions.ResumeLayout(false);
            this.grpFilterApplyOptions.PerformLayout();
            this.splInfo.Panel1.ResumeLayout(false);
            this.splInfo.Panel1.PerformLayout();
            this.splInfo.Panel2.ResumeLayout(false);
            this.splInfo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSlides)).EndInit();
            this.cmnSlideList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bindSlideGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsSlideLibrary)).EndInit();
            this.cmnSlide.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigatorSlideList)).EndInit();
            this.bindingNavigatorSlideList.ResumeLayout(false);
            this.bindingNavigatorSlideList.PerformLayout();
            this.cmnCategory.ResumeLayout(false);
            this.mnuMain.ResumeLayout(false);
            this.mnuMain.PerformLayout();
            this.stsBottom.ResumeLayout(false);
            this.stsBottom.PerformLayout();
            this.cmnRegionLanguageCategory.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SplitContainer splMain;
        private System.Windows.Forms.SplitContainer splInfo;
        private System.Windows.Forms.ContextMenuStrip cmnSlideList;
        private System.Windows.Forms.ToolStripMenuItem miSlideListNewSlide;
        private System.Windows.Forms.ContextMenuStrip cmnCategory;
        private System.Windows.Forms.ToolStripMenuItem miCategoryEdit;
        private System.Windows.Forms.ToolStripMenuItem miCategoryDeleteCat;
        private System.Windows.Forms.ToolStripMenuItem miCategoryRemoveSlide;
        public System.Windows.Forms.WebBrowser webSlideViewer;
        private System.Windows.Forms.MenuStrip mnuMain;
        private System.Windows.Forms.ToolStripMenuItem miMainFile;
        private System.Windows.Forms.ToolStripMenuItem miMainHelp;
		private System.Windows.Forms.ToolStripMenuItem miMainExit;
        private System.Windows.Forms.ToolStripMenuItem miCategoryCategoryAdd;
        private System.Windows.Forms.ToolStripMenuItem miMainModules;
        private System.Windows.Forms.ToolStripMenuItem miMainModulesSupportFiles;
        private System.Windows.Forms.ImageList imlIcons16;
        private System.Windows.Forms.StatusStrip stsBottom;
		private System.Windows.Forms.ToolStripStatusLabel lblStatusMain;
		private System.Windows.Forms.ContextMenuStrip cmnSlide;
		private System.Windows.Forms.ToolStripMenuItem miSlideUpdate;
		private System.Windows.Forms.ToolStripMenuItem miSlideDelete;
		public Admin.wsIpgAdmin.SlideLibrary dsSlideLibrary;
		private System.Windows.Forms.ToolStripMenuItem miFilterToolStrip;
        private System.Windows.Forms.ToolStripMenuItem miMainModulesAnnouncementManagement;
        private System.Windows.Forms.ToolStripMenuItem miMainUserInfo;
        private System.Windows.Forms.ToolStripMenuItem miMainUserInfoDataUpdates;
        private System.Windows.Forms.ToolStripMenuItem miMainUserInfoRegistrants;
		private System.Windows.Forms.ToolStripMenuItem miSlideMultipleUpdate;
        private System.Windows.Forms.ToolStripMenuItem miMainSlide;
        private System.Windows.Forms.ToolStripMenuItem miMainSlideNewSlide;
        private System.Windows.Forms.ToolStripMenuItem miMainSlideUpdateSlide;
        private System.Windows.Forms.ToolStripMenuItem miMainSlideDeleteSlide;
        private System.Windows.Forms.ToolStripMenuItem miMainUpdates;
        private System.Windows.Forms.ToolStripMenuItem miMainUpdatePublishContentUpdate;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem miMainData;
        private System.Windows.Forms.ToolStripMenuItem miMainDataRefreshData;
        private System.Windows.Forms.ToolStripMenuItem miMainSlideUpdateSlideFilterOrRegionLanguages;
        private System.Windows.Forms.ToolStripMenuItem miMainUpdatesPublishLocalizedContentUpdate;
        private Ascend.Windows.Forms.NavigationPane navigationPaneLeftPane;
        private Ascend.Windows.Forms.NavigationPanePage navigationPanePageSlideLibrary;
        private Ascend.Windows.Forms.NavigationPanePage navigationPanePageRegionLanguages;
        private Ascend.Windows.Forms.NavigationPanePage navigationPanePageFilters;
        private Admin.UserControls.RegionLanguageGroupGrid regionLanguageGroupGrid;
        private System.Windows.Forms.Button btnCollapseAllRegions;
        private System.Windows.Forms.Button btnToggleRegionLanguages;
        private System.Windows.Forms.Button btnExpandAllRegions;
        private Ascend.Windows.Forms.GradientPanel gradientPanel2;
        private Ascend.Windows.Forms.GradientCaption gradientCaption3;
        public System.Windows.Forms.DataGridView dgvSlides;
        public System.Windows.Forms.BindingSource bindSlideGrid;
        private Admin.UserControls.FilterTabs filterTabs;
		private Ascend.Windows.Forms.GradientPanel gradientPanel3;
		private System.Windows.Forms.Button btnToggleFilters;
		private Ascend.Windows.Forms.GradientCaption gradientCaption4;
		private Ascend.Windows.Forms.GradientPanel gradientPanel4;
		private System.Windows.Forms.Button btnApplyFilterSettings;
		private System.Windows.Forms.GroupBox grpFilterApplyOptions;
		private System.Windows.Forms.RadioButton rdoFilterShowBoth;
		private System.Windows.Forms.RadioButton rdoFilterShowMain;
		private System.Windows.Forms.RadioButton rdoFilterShowRegLang;
		private Ascend.Windows.Forms.GradientCaption gradientCaption5;
		private System.Windows.Forms.ContextMenuStrip cmnRegionLanguageCategory;
        private System.Windows.Forms.ToolStripMenuItem miRegionLanguageCategoryRemoveSlide;
		private System.Windows.Forms.BindingNavigator bindingNavigatorSlideList;
		private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
		private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
		private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
		private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
		private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
		private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
		private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
		private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
		private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBoxRegionLanguage;
        private System.Windows.Forms.DataGridViewTextBoxColumn slideIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn titleDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fileNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn DateModified;
        private System.Windows.Forms.DataGridViewTextBoxColumn NumInLib;
        private System.Windows.Forms.DataGridViewTextBoxColumn DateRemoved;
        private System.Windows.Forms.ToolStripButton toolStripButtonNewSlide;
        private System.Windows.Forms.ToolStripButton toolStripButtonUpdateSlide;
        private System.Windows.Forms.ToolStripButton toolStripButtonDeleteSlide;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem miMainSlideUpdateSlideFilter;
        private System.Windows.Forms.ToolStripMenuItem miMainSlideUpdateSlideRegionLanguages;
        private System.Windows.Forms.ToolStripMenuItem miSlideResetFiltersAndOrRegionLanguages;
        private System.Windows.Forms.ToolStripMenuItem miSlideResetFilters;
        private System.Windows.Forms.ToolStripMenuItem miSlideResetRegionLanguages;
        private System.Windows.Forms.Button btnApplyRegionLanguageSettings;
        private System.Windows.Forms.ToolStripMenuItem miMainUpdatesPublishLocalizedWebContentUpdate;
        private System.Windows.Forms.ToolStripMenuItem supportFileBatchUploadToolStripMenuItem;
        private System.Windows.Forms.ListBox lstSlideAffiliations;
    }
}