using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Admin
{
	public partial class Help : Form
	{
		public Help()
		{
			InitializeComponent();
		}

		private void Help_Load(object sender, EventArgs e)
		{
			string helpFile = Path.Combine(Application.StartupPath, Properties.Settings.Default.HelpFile);
			if (File.Exists(helpFile))
			{
				webHelp.Navigate(helpFile);
			}
			else
			{
				MessageBox.Show("Could not find the help file.", "Missing Help File", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}
	}
}