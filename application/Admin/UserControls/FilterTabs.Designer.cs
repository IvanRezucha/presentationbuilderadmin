namespace Admin.UserControls
{
    partial class FilterTabs
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabFilters = new System.Windows.Forms.TabControl();
            this.SuspendLayout();
            // 
            // tabFilters
            // 
            this.tabFilters.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabFilters.Location = new System.Drawing.Point(0, 0);
            this.tabFilters.Name = "tabFilters";
            this.tabFilters.SelectedIndex = 0;
            this.tabFilters.Size = new System.Drawing.Size(251, 330);
            this.tabFilters.TabIndex = 1;
            // 
            // FilterTabs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tabFilters);
            this.Name = "FilterTabs";
            this.Size = new System.Drawing.Size(251, 330);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabFilters;
    }
}
