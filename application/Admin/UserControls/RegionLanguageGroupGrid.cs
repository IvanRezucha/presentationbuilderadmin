using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace Admin.UserControls
{
    public partial class RegionLanguageGroupGrid : UserControl
    {
        RegionLanguageControlDS _RegionLanguageDS;

        bool _LastCheckToggle;
        public RegionLanguageGroupGrid()
        {
            InitializeComponent();
            _LastCheckToggle = false;
        }

        private bool _ShowControlBox;

        public bool ShowControlBox
        {
            get { return _ShowControlBox; }
            set 
            { 
                _ShowControlBox = value;
                toolStripControlBox.Visible = _ShowControlBox;
            }
        }


        /// <summary>
        /// Fills the region languages for slides.
        /// </summary>
        public void FillRegionLanguages(wsIpgAdmin.SlideLibrary slideLibrary)
        {
            //grid columns
            //0-RegionLanguageID
            //1-RegionID
            //2-LanguageID
            //3-Region
            //4-Language
            //5-RegionSortOrder
            //6-LanguageSortOrder
            //7-CheckBox
            //8-blank/filler

            //fill the RegionLanguages DataSet
            _RegionLanguageDS = new RegionLanguageControlDS();
            foreach (wsIpgAdmin.SlideLibrary.RegionLanguageRow regLang in slideLibrary.RegionLanguage.Rows)
            {
                RegionLanguageControlDS.RegionLanguageRow rlRow = _RegionLanguageDS.RegionLanguage.NewRegionLanguageRow();
                rlRow.RegionID = regLang.RegionID;
                rlRow.LanguageID = regLang.LanguageID;
                rlRow.RegionLanguageID = regLang.RegionLanguageID;
                rlRow.Region = regLang.RegionRow.RegionName;
                rlRow.Language = regLang.LanguageRow.LongName;
                rlRow.RegionSortOrder = regLang.RegionRow.SortOrder;
                rlRow.LanguageSortOrder = regLang.LanguageRow.SortOrder;
                _RegionLanguageDS.RegionLanguage.AddRegionLanguageRow(rlRow);
            }

    
            //bind data to group grid
            ggRegionLanguages.BindData(_RegionLanguageDS, _RegionLanguageDS.RegionLanguage.TableName);

            //this sets up the grouping--3 is the RegionName column, 5 is the RegionSortOrder column
            ggRegionLanguages.GroupTemplate = new Wirestone.WinForms.GroupGrid.GroupGridDefaultGroup();
            ggRegionLanguages.GroupTemplate.Column = ggRegionLanguages.Columns[3];
            //5 is the RegionSortOrder column-->this is what we would like to set the sort as, but is screws up the grouping
            ggRegionLanguages.Sort(new Wirestone.WinForms.GroupGrid.GroupGridDataRowComparer(3, ListSortDirection.Ascending));
           
            //hide the primary and foreign key columns
            ggRegionLanguages.Columns[0].Visible = false;
            ggRegionLanguages.Columns[1].Visible = false;
            ggRegionLanguages.Columns[2].Visible = false;
            ggRegionLanguages.Columns[3].Visible = false;

            ggRegionLanguages.ExpandAll();

            //add a checkbox column
            DataGridViewCheckBoxColumn col1 = new DataGridViewCheckBoxColumn();
            col1.HeaderText = "";
            ggRegionLanguages.Columns.Add(col1);

            //add a blank column to fill the blank grid area
            DataGridViewTextBoxColumn col2 = new DataGridViewTextBoxColumn();
            col2.HeaderText = "";
            col2.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            ggRegionLanguages.Columns.Add(col2);

            //setup additional column properties
            ggRegionLanguages.Columns[4].ReadOnly = true;
            ggRegionLanguages.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            ggRegionLanguages.Columns[5].Visible = false;
            ggRegionLanguages.Columns[6].Visible = false;
            ggRegionLanguages.Columns[7].ReadOnly = false;
            ggRegionLanguages.Columns[8].ReadOnly = true;


            //let's try to fit more of these into view
            foreach (Wirestone.WinForms.GroupGrid.GroupGridRow ggRow in ggRegionLanguages.Rows)
            {
                if (!ggRow.IsGroupRow)
                {
                    ggRow.Height = 18;
                }
            }

        }
		/// <summary>
		/// Fills the region languages for support files.
		/// </summary>
		public void FillSupportFileRegionLanguages(wsIpgAdmin.SupportFileDS dsSupportFile)
		{
			//grid columns
			//0-RegionLanguageID
			//1-RegionID
			//2-LanguageID
			//3-Region
			//4-Language
			//5-RegionSortOrder
			//6-LanguageSortOrder
			//7-CheckBox
			//8-blank/filler

			//fill the RegionLanguages DataSet
			_RegionLanguageDS = new RegionLanguageControlDS();
			foreach (wsIpgAdmin.SupportFileDS.RegionLanguageRow regLang in dsSupportFile.RegionLanguage.Rows)
			{
				RegionLanguageControlDS.RegionLanguageRow rlRow = _RegionLanguageDS.RegionLanguage.NewRegionLanguageRow();
				rlRow.RegionID = regLang.RegionID;
				rlRow.LanguageID = regLang.LanguageID;
				rlRow.RegionLanguageID = regLang.RegionLanguageID;
				rlRow.Region = regLang.RegionRow.RegionName;
				rlRow.Language = regLang.LanguageRow.LongName;
				rlRow.RegionSortOrder = regLang.RegionRow.SortOrder;
				rlRow.LanguageSortOrder = regLang.LanguageRow.SortOrder;
				_RegionLanguageDS.RegionLanguage.AddRegionLanguageRow(rlRow);
			}

			
			//bind data to group grid
			ggRegionLanguages.BindData(_RegionLanguageDS, _RegionLanguageDS.RegionLanguage.TableName);

			//this sets up the grouping--3 is the RegionName column, 5 is the RegionSortOrder column
			ggRegionLanguages.GroupTemplate = new Wirestone.WinForms.GroupGrid.GroupGridDefaultGroup();
			ggRegionLanguages.GroupTemplate.Column = ggRegionLanguages.Columns[3];
			//5 is the RegionSortOrder column-->this is what we would like to set the sort as, but is screws up the grouping
			ggRegionLanguages.Sort(new Wirestone.WinForms.GroupGrid.GroupGridDataRowComparer(3, ListSortDirection.Ascending));

			//hide the primary and foreign key columns
			ggRegionLanguages.Columns[0].Visible = false;
			ggRegionLanguages.Columns[1].Visible = false;
			ggRegionLanguages.Columns[2].Visible = false;
			ggRegionLanguages.Columns[3].Visible = false;

			ggRegionLanguages.ExpandAll();

			//add a checkbox column
			DataGridViewCheckBoxColumn col1 = new DataGridViewCheckBoxColumn();
			col1.HeaderText = "";
			ggRegionLanguages.Columns.Add(col1);

			//add a blank column to fill the blank grid area
			DataGridViewTextBoxColumn col2 = new DataGridViewTextBoxColumn();
			col2.HeaderText = "";
			col2.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
			ggRegionLanguages.Columns.Add(col2);

			//setup additional column properties
			ggRegionLanguages.Columns[4].ReadOnly = true;
			ggRegionLanguages.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
			ggRegionLanguages.Columns[5].Visible = false;
			ggRegionLanguages.Columns[6].Visible = false;
			ggRegionLanguages.Columns[7].ReadOnly = false;
			ggRegionLanguages.Columns[8].ReadOnly = true;


			//let's try to fit more of these into view
			foreach (Wirestone.WinForms.GroupGrid.GroupGridRow ggRow in ggRegionLanguages.Rows)
			{
				if (!ggRow.IsGroupRow)
				{
					ggRow.Height = 18;
				}
			}

		}

        #region public region/language methods

        /// <summary>
        /// Gets the selected region language IDs.
        /// </summary>
        /// <returns></returns>
        public List<int> GetSelectedRegionLanguageIDs()
        {
            List<int> selectedIDs = new List<int>();
            foreach (Wirestone.WinForms.GroupGrid.GroupGridRow row in ggRegionLanguages.Rows)
            {
                if (!row.IsGroupRow)
                {
                    //if (row.Selected)
                    //{
                    //    row.Selected = false;
                    //}
                    if (row.Cells[7].IsInEditMode)
                    {
                        row.Cells[7].DataGridView.EndEdit();
                    }
                    if (Convert.ToBoolean(row.Cells[7].Value) == true)
                    {
                        selectedIDs.Add(Convert.ToInt32(row.Cells[0].Value));
                    }
                }
            }

            return selectedIDs;
        }

        /// <summary>
        /// Checks the slide region languages.
        /// </summary>
        /// <param name="regionLanguageIDs">The region language I ds.</param>
        public void CheckSlideRegionLanguages(List<int> regionLanguageIDs)
        {
            foreach (Wirestone.WinForms.GroupGrid.GroupGridRow row in ggRegionLanguages.Rows)
            {
                if (!row.IsGroupRow)
                {
                    if (regionLanguageIDs.Contains(Convert.ToInt32(row.Cells[0].Value)))
                    {
                        row.Cells[7].Value = true;
                    }
                }
            }
        }

        /// <summary>
        /// Checks all slide region languages.
        /// </summary>
        public void CheckAllSlideRegionLanguages()
        {
            foreach (Wirestone.WinForms.GroupGrid.GroupGridRow row in ggRegionLanguages.Rows)
            {
                if (!row.IsGroupRow)
                {
                    row.Cells[7].Value = true;
                }
            }
        }

        /// <summary>
        /// Checks all slide region languages.
        /// </summary>
        public void UnCheckAllSlideRegionLanguages()
        {
            foreach (Wirestone.WinForms.GroupGrid.GroupGridRow row in ggRegionLanguages.Rows)
            {
                if (!row.IsGroupRow)
                {
                    row.Cells[7].Value = false;
                }
            }
        }

        public void CollapseAllGroups()
        {
            ggRegionLanguages.CollapseAll();
        }

        public void ExpandAllGroups()
        {
            ggRegionLanguages.ExpandAll();
        }

        #endregion
        private void toolStripButtonToggleChecks_Click(object sender, EventArgs e)
        {
            if (_LastCheckToggle)
            {
                UnCheckAllSlideRegionLanguages();
                _LastCheckToggle = false;
            }
            else
            {
                CheckAllSlideRegionLanguages();
                _LastCheckToggle = true;
            }
        }

        private void toolStripButtonExpandAll_Click(object sender, EventArgs e)
        {
            ExpandAllGroups();
        }

        private void toolStripButtonCollapseAll_Click(object sender, EventArgs e)
        {
            CollapseAllGroups();
        }
    }
}
