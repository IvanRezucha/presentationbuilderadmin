using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace Admin.UserControls
{
    public partial class FilterTabs : UserControl
    {
        wsIpgAdmin.SlideLibrary _SlideLibraryDS;

        public FilterTabs()
        {
            InitializeComponent();
        }

        public void Fill(wsIpgAdmin.SlideLibrary dsFilterData)
        {
            _SlideLibraryDS = new Admin.wsIpgAdmin.SlideLibrary();
            _SlideLibraryDS = dsFilterData;
            FillFilters();
        }

        #region public filter methods

        /// <summary>
        /// Gets the selected filter IDs.
        /// </summary>
        /// <returns>List<int></returns>
        public List<int> GetSelectedFilterIDs()
        {
            List<int> selectedIDs = new List<int>();
            // Loop through the filter group tabs
            foreach (TabPage filter in tabFilters.TabPages)
            {
                // Loop through the tab controls until we find the listview control
                foreach (Control control in filter.Controls)
                {
                    if (control.GetType().Name.CompareTo("ListView") == 0)
                    {
                        // Loop through the filter listview items that are checked and add them as new items
                        foreach (ListViewItem slidefilter in ((ListView)control).CheckedItems)
                        {
                            selectedIDs.Add(((wsIpgAdmin.SlideLibrary.FilterRow)slidefilter.Tag).FilterID);
                        }

                        break;
                    }
                }
            }

            return selectedIDs;
        }

        /// <summary>
        /// Checks the slide filters.
        /// </summary>
        /// <param name="filterIDs">The filter I ds.</param>
        public void CheckSlideFilters(List<int> filterIDs)
        {
            
            // Loop through the filter group tabs
            foreach (TabPage filter in tabFilters.TabPages)
            {
                // Loop through the tab controls until we find the listview control
                foreach (Control control in filter.Controls)
                {
                    if (control.GetType().Name.CompareTo("ListView") == 0)
                    {
                        // Loop through the filter listview items that are checked and add them as new items
                        foreach (ListViewItem slidefilter in ((ListView)control).Items)
                        {
                            if (filterIDs.Contains(((wsIpgAdmin.SlideLibrary.FilterRow)slidefilter.Tag).FilterID))
                            {
                                slidefilter.Checked = true;
                            }
                            else //*ss added due to making filters preselected
                            {
                                slidefilter.Checked = false;
                            }
                        }

                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Checks all filters.
        /// </summary>
        public void CheckAllFilters()
        {

            // Loop through the filter group tabs
            foreach (TabPage filter in tabFilters.TabPages)
            {
                // Loop through the tab controls until we find the listview control
                foreach (Control control in filter.Controls)
                {
                    if (control.GetType().Name.CompareTo("ListView") == 0)
                    {
                        // Loop through the filter listview items that are checked and add them as new items
                        foreach (ListViewItem slidefilter in ((ListView)control).Items)
                        {
                            slidefilter.Checked = true;
                        }

                        break;
                    }
                }
            }           
        }

        /// <summary>
        /// Unheck all filters.
        /// </summary>
        public void UnCheckAllFilters()
        {
            // Loop through the filter group tabs
            foreach (TabPage filter in tabFilters.TabPages)
            {
                // Loop through the tab controls until we find the listview control
                foreach (Control control in filter.Controls)
                {
                    if (control.GetType().Name.CompareTo("ListView") == 0)
                    {
                        // Loop through the filter listview items that are checked and add them as new items
                        foreach (ListViewItem slidefilter in ((ListView)control).Items)
                        {
                            slidefilter.Checked = false;
                        }

                        break;
                    }
                }
            }
        }

        #endregion
        /// <summary>
        /// Fills the filters.
        /// </summary>
        private void FillFilters()
        {
            // Loop through the filter groups and add filter group tabs
            foreach (wsIpgAdmin.SlideLibrary.FilterGroupRow filterGroupRow in _SlideLibraryDS.FilterGroup.Rows)
            {
                // Create the tab and listview
                TabPage filterTab = new TabPage();
                ListView filterList = new ListView();
                filterList.CheckBoxes = true;
                filterList.View = View.List;
                filterList.Dock = DockStyle.Fill;

                // Now add related filter choices
                foreach (wsIpgAdmin.SlideLibrary.FilterRow filterRow in filterGroupRow.GetFilterRows())
                {
                    ListViewItem filterItem = new ListViewItem(filterRow.FilterName);
                    filterItem.Tag = filterRow;
                    filterItem.Checked = true; //Changed to make all selectiosn checked by default *SS
                    filterList.Items.Add(filterItem);
                }

                // Add the tab
                filterTab.Tag = filterGroupRow;
                filterTab.Text = filterGroupRow.FilterGroupName;
                tabFilters.TabPages.Add(filterTab);
                filterTab.Controls.Add(filterList);
            }
        }
    }
}
