namespace Admin.UserControls
{
    partial class SlideFiltersAndLanguages
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabMain = new System.Windows.Forms.TabControl();
            this.tbpFilterGroups = new System.Windows.Forms.TabPage();
            this.filterTabs = new Admin.UserControls.FilterTabs();
            this.tbpRegionsLanguages = new System.Windows.Forms.TabPage();
            this.regionLanguageGroupGrid = new Admin.UserControls.RegionLanguageGroupGrid();
            this.tabMain.SuspendLayout();
            this.tbpFilterGroups.SuspendLayout();
            this.tbpRegionsLanguages.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabMain
            // 
            this.tabMain.Controls.Add(this.tbpFilterGroups);
            this.tabMain.Controls.Add(this.tbpRegionsLanguages);
            this.tabMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabMain.Location = new System.Drawing.Point(0, 0);
            this.tabMain.Name = "tabMain";
            this.tabMain.SelectedIndex = 0;
            this.tabMain.Size = new System.Drawing.Size(443, 485);
            this.tabMain.TabIndex = 0;
            // 
            // tbpFilterGroups
            // 
            this.tbpFilterGroups.Controls.Add(this.filterTabs);
            this.tbpFilterGroups.Location = new System.Drawing.Point(4, 22);
            this.tbpFilterGroups.Name = "tbpFilterGroups";
            this.tbpFilterGroups.Padding = new System.Windows.Forms.Padding(3);
            this.tbpFilterGroups.Size = new System.Drawing.Size(435, 459);
            this.tbpFilterGroups.TabIndex = 0;
            this.tbpFilterGroups.Text = "Filters";
            this.tbpFilterGroups.UseVisualStyleBackColor = true;
            // 
            // filterTabs
            // 
            this.filterTabs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.filterTabs.Location = new System.Drawing.Point(3, 3);
            this.filterTabs.Name = "filterTabs";
            this.filterTabs.Size = new System.Drawing.Size(429, 453);
            this.filterTabs.TabIndex = 0;
            // 
            // tbpRegionsLanguages
            // 
            this.tbpRegionsLanguages.Controls.Add(this.regionLanguageGroupGrid);
            this.tbpRegionsLanguages.Location = new System.Drawing.Point(4, 22);
            this.tbpRegionsLanguages.Name = "tbpRegionsLanguages";
            this.tbpRegionsLanguages.Padding = new System.Windows.Forms.Padding(3);
            this.tbpRegionsLanguages.Size = new System.Drawing.Size(435, 459);
            this.tbpRegionsLanguages.TabIndex = 1;
            this.tbpRegionsLanguages.Text = "Regions and Languages";
            this.tbpRegionsLanguages.UseVisualStyleBackColor = true;
            // 
            // regionLanguageGroupGrid
            // 
            this.regionLanguageGroupGrid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.regionLanguageGroupGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.regionLanguageGroupGrid.Location = new System.Drawing.Point(3, 3);
            this.regionLanguageGroupGrid.Name = "regionLanguageGroupGrid";
            this.regionLanguageGroupGrid.ShowControlBox = false;
            this.regionLanguageGroupGrid.Size = new System.Drawing.Size(429, 453);
            this.regionLanguageGroupGrid.TabIndex = 0;
            // 
            // SlideFiltersAndLanguages
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tabMain);
            this.Name = "SlideFiltersAndLanguages";
            this.Size = new System.Drawing.Size(443, 485);
            this.tabMain.ResumeLayout(false);
            this.tbpFilterGroups.ResumeLayout(false);
            this.tbpRegionsLanguages.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage tbpFilterGroups;
        private System.Windows.Forms.TabPage tbpRegionsLanguages;
        public System.Windows.Forms.TabControl tabMain;
        public FilterTabs filterTabs;
        public RegionLanguageGroupGrid regionLanguageGroupGrid;
    }
}
