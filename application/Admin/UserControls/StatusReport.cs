using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace Admin.UserControls
{
	public partial class StatusReport : UserControl
	{
		bool _successfulRun = true;
		private bool SuccessfulRun
		{
			get { return _successfulRun; }
			set { _successfulRun = value; }
		}

		public StatusReport()
		{
			InitializeComponent();
			picOverallStatus.Image = ((Image)Properties.Resources.Updating);
			lblOverallStatus.Text = "Updating";
		}

		public ActionItem AddActionItem(ActionItem actionItem)
		{
			string statusResult = string.Empty;

			if (actionItem.StatusResult != ActionItem.StatusResultItems.None)
			{
				statusResult = actionItem.StatusResult.ToString();
			}

			DataRow dr = StatusDataSet.Tables["ActionItem"].Rows.Add(new object[] { StatusDataSet.Tables["ActionItem"].Rows.Count, actionItem.ActionItemText, statusResult, actionItem.Message, actionItem.ActionItemImage });
			actionItem.ActionID = Convert.ToInt32(dr[0]);

			return actionItem;
		}

		public void UpdateActionItem(ActionItem actionItem)
		{
			string statusResult = string.Empty;

			if (actionItem.StatusResult != ActionItem.StatusResultItems.None)
			{
				statusResult = actionItem.StatusResult.ToString();
			}
			
			DataRow dr = StatusDataSet.Tables["ActionItem"].Rows.Find(actionItem.ActionID);
			dr.ItemArray = new object[] { actionItem.ActionID, actionItem.ActionItemText, statusResult, actionItem.Message, actionItem.ActionItemImage };
		}

		public ActionItem GetActionItem(int actionID)
		{
			DataRow dr = StatusDataSet.Tables["ActionItem"].Rows.Find(actionID);

			ActionItem actionItem = new ActionItem();
			actionItem.CreateActionItem((ActionItem.ActionItemImageItems)dr["StatusImage"], dr["ActionItem"].ToString(), (ActionItem.StatusResultItems)dr["StatusResult"], dr["Message"].ToString());
			
			return actionItem;
		}

		public void DeleteActionItem(int actionID)
		{
			StatusDataSet.Tables["ActionItem"].Rows.Find(actionID).Delete();
		}

		/// <summary>
		/// Alls the items to be reported on have completed processing.
		/// </summary>
		public void AllItemsCompleted()
		{
			if (SuccessfulRun)
			{	
				picOverallStatus.Image = ((Image)Properties.Resources.Success);
				lblOverallStatus.Text = "Success";
			}
			else
			{
				picOverallStatus.Image = Properties.Resources.Failure;
				lblOverallStatus.Text = "Error";
			}
		}

		private void dgvStatus_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
		{
			int actionItemCount = 0;
			int actionItemsFailed = 0;
			int actionItemsWarning = 0;
			int actionItemsSuccess = 0;

			foreach (DataGridViewRow row in dgvStatus.Rows)
			{
				actionItemCount++;

				// Set status image
				string imageName = StatusDataSet.Tables["ActionItem"].Rows[row.Index]["StatusImage"].ToString();

				switch (imageName)
				{
					case "Blank":
						row.Cells[0].Value = ((Image)Properties.Resources.WhitePNG);
						break;
					case "Failure":
						row.Cells[0].Value = ((Image)Properties.Resources.error_sm);
						break;
					case "Success":
						row.Cells[0].Value = ((Image)Properties.Resources.success_sm);
						break;
					case "Updating":
						row.Cells[0].Value = ((Image)Properties.Resources.updating_sm);
						break;
					case "Warning":
						row.Cells[0].Value = ((Image)Properties.Resources.warning_sm);
						break;
					default:
						row.Cells[0].Value = ((Image)Properties.Resources.WhitePNG);
						break;
				}
				if (ImageAnimator.CanAnimate((Image)row.Cells[0].Value))
				{
					ImageAnimator.Animate(((Image)row.Cells[0].Value), new EventHandler(this.Image_FrameChanged));
				}

				string statusResult = StatusDataSet.Tables["ActionItem"].Rows[row.Index]["StatusResult"].ToString();

				// Check action item status
				if (statusResult == ActionItem.StatusResultItems.Failure.ToString())
				{
					actionItemsFailed++;
				}
				else if (statusResult == ActionItem.StatusResultItems.Warning.ToString())
				{
					actionItemsWarning++;
				}
				else if (statusResult == ActionItem.StatusResultItems.Success.ToString())
				{
					actionItemsSuccess++;
				}
			}

			// Update overall status
			if (Convert.ToBoolean(actionItemsFailed))
				SuccessfulRun = false;

			lblTotalActionItems.Text = actionItemCount + " Total";
			lblTotalSuccesses.Text = actionItemsSuccess + " Success";
			lblTotalErrors.Text = actionItemsFailed + " Error";
			lblTotalWarnings.Text = actionItemsWarning + " Warning";
		}

		private void Image_FrameChanged(object o, EventArgs e)
		{
			//invalidate iconDataGridViewTextBoxColumn to force a refresh enabling gif animation
            if (dgvStatus.Columns.Count > 0)
            {
                dgvStatus.InvalidateColumn(0);
            }	
		}

		/// <summary>
		/// The action item object that gets added to the status report control
		/// </summary>
		public class ActionItem
		{
			#region Fields

			private ActionItemImageItems _actionItemImage;
			private string _actionItemText;
			private StatusResultItems _statusResult;
			private string _message;
			private int actionID;

			#endregion

			#region Properties

			public ActionItemImageItems ActionItemImage
			{
				get { return _actionItemImage; }
				set { _actionItemImage = value; }
			}
			public string ActionItemText
			{
				get { return _actionItemText; }
				set { _actionItemText = value; }
			}
			public StatusResultItems StatusResult
			{
				get { return _statusResult; }
				set { _statusResult = value; }
			}
			public string Message
			{
				get { return _message; }
				set 
				{
					_message = Convert.ToString(value);
				}
			}
			public int ActionID
			{
				get { return actionID; }
				set { actionID = value; }
			}

			#endregion

			public ActionItem()
			{

			}

			public ActionItem(string actionItemText)
			{
				ActionID = -1;
				ActionItemText = actionItemText;
				ActionItemImage = ActionItemImageItems.Blank;
				StatusResult = StatusResultItems.None;
				Message = "";
			}

			public void CreateActionItem(ActionItemImageItems actionItemImage, string actionItemText, StatusResultItems statusResult, string message)
			{
				ActionID = -1;
				ActionItemImage = actionItemImage;
				ActionItemText = actionItemText;
				StatusResult = StatusResult;
				Message = message;
			}

			public enum ActionItemImageItems
			{
				Blank,
				Failure,
				Success,
				Updating,
				Warning
			}

			public enum StatusResultItems
			{
				None = 0,
				Failure,
				Success,
				Warning
			}
		}
	}
}
