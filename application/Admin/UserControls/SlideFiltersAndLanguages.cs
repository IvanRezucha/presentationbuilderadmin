using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace Admin.UserControls
{
    public partial class SlideFiltersAndLanguages : UserControl
    {
        wsIpgAdmin.SlideLibrary _SlideLibraryDS;
        RegionLanguageControlDS _RegionLanguageDS;

        public SlideFiltersAndLanguages()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Fills the control.
        /// </summary>
        /// <param name="dsSlideLibrary">The ds slide library.</param>
        /// <param name="fillType">Type of the fill.</param>
        public void Fill(wsIpgAdmin.SlideLibrary dsSlideLibrary, SlideFiltersAndLanguagesFillType fillType)
        {
            _SlideLibraryDS = new Admin.wsIpgAdmin.SlideLibrary();
            _SlideLibraryDS.Merge(dsSlideLibrary);

            switch (fillType)
            {
                case SlideFiltersAndLanguagesFillType.FiltersOnly:
                    FillFilters();
                    tabMain.TabPages.Remove(tbpRegionsLanguages);
                    break;
                case SlideFiltersAndLanguagesFillType.RegionLanguagesOnly:
                    FillRegionLanguages();
                    tabMain.TabPages.Remove(tbpFilterGroups);
                    break;
                case SlideFiltersAndLanguagesFillType.FiltersAndRegionLanguages:
                    FillFilters();
                    FillRegionLanguages();
                    break;
                default:
                    //do nothing
                    break;
            }
        }

        #region public region/language methods

        ///// <summary>
        ///// Gets the selected region language IDs.
        ///// </summary>
        ///// <returns></returns>
        //public List<int> GetSelectedRegionLanguageIDs()
        //{
        //    List<int> selectedIDs = new List<int>();
        //    foreach (Wirestone.WinForms.GroupGrid.GroupGridRow row in ggRegionLanguages.Rows)
        //    {
        //        if (!row.IsGroupRow)
        //        {
        //            if (Convert.ToBoolean(row.Cells[5].Value) == true)
        //            {
        //                selectedIDs.Add(Convert.ToInt32(row.Cells[0].Value));
        //            }
        //        }
        //    }

        //    return selectedIDs;
        //}

        ///// <summary>
        ///// Checks the slide region languages.
        ///// </summary>
        ///// <param name="regionLanguageIDs">The region language I ds.</param>
        //public void CheckSlideRegionLanguages(List<int> regionLanguageIDs)
        //{
        //    foreach (Wirestone.WinForms.GroupGrid.GroupGridRow row in ggRegionLanguages.Rows)
        //    {
        //        if (!row.IsGroupRow)
        //        {
        //            if (regionLanguageIDs.Contains(Convert.ToInt32(row.Cells[0].Value)))
        //            {
        //                row.Cells[5].Value = true;
        //            }
        //        }
        //    }
        //}

        ///// <summary>
        ///// Checks all slide region languages.
        ///// </summary>
        //public void CheckAllSlideRegionLanguages()
        //{
        //    foreach (Wirestone.WinForms.GroupGrid.GroupGridRow row in ggRegionLanguages.Rows)
        //    {
        //        if (!row.IsGroupRow)
        //        {
        //            row.Cells[5].Value = true;
        //        }
        //    }
        //}

        ///// <summary>
        ///// Checks all slide region languages.
        ///// </summary>
        //public void UnCheckAllSlideRegionLanguages()
        //{
        //    foreach (Wirestone.WinForms.GroupGrid.GroupGridRow row in ggRegionLanguages.Rows)
        //    {
        //        if (!row.IsGroupRow)
        //        {
        //            row.Cells[5].Value = false;
        //        }
        //    }
        //} 

        #endregion

        #region public filter methods

        ///// <summary>
        ///// Gets the selected filter IDs.
        ///// </summary>
        ///// <returns>List<int></returns>
        //public List<int> GetSelectedFilterIDs()
        //{
        //    List<int> selectedIDs = new List<int>();
        //    // Loop through the filter group tabs
        //    foreach (TabPage filter in tabFilters.TabPages)
        //    {
        //        // Loop through the tab controls until we find the listview control
        //        foreach (Control control in filter.Controls)
        //        {
        //            if (control.GetType().Name.CompareTo("ListView") == 0)
        //            {
        //                // Loop through the filter listview items that are checked and add them as new items
        //                foreach (ListViewItem slidefilter in ((ListView)control).CheckedItems)
        //                {
        //                    selectedIDs.Add(((wsIpgAdmin.SlideLibrary.FilterRow)slidefilter.Tag).FilterID);
        //                }

        //                break;
        //            }
        //        }
        //    }

        //    return selectedIDs;
        //}

        ///// <summary>
        ///// Checks the slide filters.
        ///// </summary>
        ///// <param name="filterIDs">The filter I ds.</param>
        //public void CheckSlideFilters(List<int> filterIDs)
        //{
        //    // Loop through the filter group tabs
        //    foreach (TabPage filter in tabFilters.TabPages)
        //    {
        //        // Loop through the tab controls until we find the listview control
        //        foreach (Control control in filter.Controls)
        //        {
        //            if (control.GetType().Name.CompareTo("ListView") == 0)
        //            {
        //                // Loop through the filter listview items that are checked and add them as new items
        //                foreach (ListViewItem slidefilter in ((ListView)control).Items)
        //                {
        //                    if (filterIDs.Contains(((wsIpgAdmin.SlideLibrary.FilterRow)slidefilter.Tag).FilterID))
        //                    {
        //                        slidefilter.Checked = true;
        //                    }
        //                }

        //                break;
        //            }
        //        }
        //    }
        //}

        ///// <summary>
        ///// Checks all filters.
        ///// </summary>
        //public void CheckAllFilters()
        //{
        //    // Loop through the filter group tabs
        //    foreach (TabPage filter in tabFilters.TabPages)
        //    {
        //        // Loop through the tab controls until we find the listview control
        //        foreach (Control control in filter.Controls)
        //        {
        //            if (control.GetType().Name.CompareTo("ListView") == 0)
        //            {
        //                // Loop through the filter listview items that are checked and add them as new items
        //                foreach (ListViewItem slidefilter in ((ListView)control).Items)
        //                {
        //                    slidefilter.Checked = true;
        //                }

        //                break;
        //            }
        //        }
        //    }
        //}

        ///// <summary>
        ///// Unheck all filters.
        ///// </summary>
        //public void UnCheckAllFilters()
        //{
        //    // Loop through the filter group tabs
        //    foreach (TabPage filter in tabFilters.TabPages)
        //    {
        //        // Loop through the tab controls until we find the listview control
        //        foreach (Control control in filter.Controls)
        //        {
        //            if (control.GetType().Name.CompareTo("ListView") == 0)
        //            {
        //                // Loop through the filter listview items that are checked and add them as new items
        //                foreach (ListViewItem slidefilter in ((ListView)control).Items)
        //                {
        //                    slidefilter.Checked = false;
        //                }

        //                break;
        //            }
        //        }
        //    }
        //} 

        #endregion

        #region private methods

        /// <summary>
        /// Fills the filters.
        /// </summary>
        private void FillFilters()
        {

            filterTabs.Fill(_SlideLibraryDS);
            //// Loop through the filter groups and add filter group tabs
            //foreach (wsIpgAdmin.SlideLibrary.FilterGroupRow filterGroupRow in _SlideLibraryDS.FilterGroup.Rows)
            //{
            //    // Create the tab and listview
            //    TabPage filterTab = new TabPage();
            //    ListView filterList = new ListView();
            //    filterList.CheckBoxes = true;
            //    filterList.View = View.List;
            //    filterList.Dock = DockStyle.Fill;

            //    // Now add related filter choices
            //    foreach (wsIpgAdmin.SlideLibrary.FilterRow filterRow in filterGroupRow.GetFilterRows())
            //    {
            //        ListViewItem filterItem = new ListViewItem(filterRow.FilterName);
            //        filterItem.Tag = filterRow;
            //        filterItem.Checked = false;
            //        filterList.Items.Add(filterItem);
            //    }

            //    // Add the tab
            //    filterTab.Tag = filterGroupRow;
            //    filterTab.Text = filterGroupRow.FilterGroupName;
            //    tabFilters.TabPages.Add(filterTab);
            //    filterTab.Controls.Add(filterList);
            //}
        }

        /// <summary>
        /// Fills the region languages.
        /// </summary>
        private void FillRegionLanguages()
        {

            regionLanguageGroupGrid.FillRegionLanguages(_SlideLibraryDS);
            ////grid columns
            ////0-RegionLanguageID
            ////1-RegionID
            ////2-LanguageID
            ////3-Region
            ////4-Language
            ////5-CheckBox
            ////6-blank/filler

            ////fill the RegionLanguages DataSet
            //_RegionLanguageDS = new RegionLanguageControlDS();
            //foreach (wsIpgAdmin.SlideLibrary.RegionLanguageRow regLang in _SlideLibraryDS.RegionLanguage.Rows)
            //{
            //    RegionLanguageControlDS.RegionLanguageRow rlRow = _RegionLanguageDS.RegionLanguage.NewRegionLanguageRow();
            //    rlRow.RegionID = regLang.RegionID;
            //    rlRow.LanguageID = regLang.LanguageID;
            //    rlRow.RegionLanguageID = regLang.RegionLanguageID;
            //    rlRow.Region = regLang.RegionRow.RegionName;
            //    rlRow.Language = regLang.LanguageRow.LongName;
            //    _RegionLanguageDS.RegionLanguage.AddRegionLanguageRow(rlRow);
            //}

            ////bind data to group grid
            //ggRegionLanguages.BindData(_RegionLanguageDS, _RegionLanguageDS.RegionLanguage.TableName);

            ////this sets up the grouping--3 is the Region column
            //ggRegionLanguages.GroupTemplate = new Wirestone.WinForms.GroupGrid.GroupGridDefaultGroup();
            //ggRegionLanguages.GroupTemplate.Column = ggRegionLanguages.Columns[3];
            //ggRegionLanguages.Sort(new Wirestone.WinForms.GroupGrid.GroupGridDataRowComparer(3, ListSortDirection.Ascending));

            ////hide the primary and foreign key columns
            //ggRegionLanguages.Columns[0].Visible = false;
            //ggRegionLanguages.Columns[1].Visible = false;
            //ggRegionLanguages.Columns[2].Visible = false;
            //ggRegionLanguages.Columns[3].Visible = false;

            //ggRegionLanguages.ExpandAll();

            ////add a checkbox column
            //DataGridViewCheckBoxColumn col1 = new DataGridViewCheckBoxColumn();
            //col1.HeaderText = "";
            //ggRegionLanguages.Columns.Add(col1);

            ////add a blank column to fill the blank grid area
            //DataGridViewTextBoxColumn col2 = new DataGridViewTextBoxColumn();
            //col2.HeaderText = "";
            //col2.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            //ggRegionLanguages.Columns.Add(col2);

            ////setup additional column properties
            //ggRegionLanguages.Columns[4].ReadOnly = true;
            //ggRegionLanguages.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            //ggRegionLanguages.Columns[5].ReadOnly = false;
            //ggRegionLanguages.Columns[6].ReadOnly = true;


            ////let's try to fit more of these into view
            //foreach (Wirestone.WinForms.GroupGrid.GroupGridRow ggRow in ggRegionLanguages.Rows)
            //{
            //    if (!ggRow.IsGroupRow)
            //    {
            //        ggRow.Height = 18;
            //    }
            //}

        } 

        #endregion


    }

    public enum SlideFiltersAndLanguagesFillType
    { 
        FiltersOnly,
        RegionLanguagesOnly,
        FiltersAndRegionLanguages 
    }
}
