namespace Admin.UserControls
{
    partial class RegionLanguageGroupGrid
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ggRegionLanguages = new Wirestone.WinForms.GroupGrid.GroupGrid();
            this.toolStripControlBox = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonCollapseAll = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonExpandAll = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonToggleChecks = new System.Windows.Forms.ToolStripButton();
            ((System.ComponentModel.ISupportInitialize)(this.ggRegionLanguages)).BeginInit();
            this.toolStripControlBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // ggRegionLanguages
            // 
            this.ggRegionLanguages.AllowUserToAddRows = false;
            this.ggRegionLanguages.AllowUserToDeleteRows = false;
            this.ggRegionLanguages.AllowUserToResizeColumns = false;
            this.ggRegionLanguages.BackgroundColor = System.Drawing.Color.White;
            this.ggRegionLanguages.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ggRegionLanguages.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.ggRegionLanguages.CollapseIcon = global::Admin.Properties.Resources.CollapsePNG;
            this.ggRegionLanguages.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ggRegionLanguages.ColumnHeadersVisible = false;
            this.ggRegionLanguages.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ggRegionLanguages.ExpandIcon = global::Admin.Properties.Resources.ExpandPNG;
            this.ggRegionLanguages.Location = new System.Drawing.Point(0, 0);
            this.ggRegionLanguages.Name = "ggRegionLanguages";
            this.ggRegionLanguages.RowHeadersVisible = false;
            this.ggRegionLanguages.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ggRegionLanguages.Size = new System.Drawing.Size(257, 314);
            this.ggRegionLanguages.TabIndex = 1;
            // 
            // toolStripControlBox
            // 
            this.toolStripControlBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.toolStripControlBox.BackColor = System.Drawing.Color.White;
            this.toolStripControlBox.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStripControlBox.GripMargin = new System.Windows.Forms.Padding(0);
            this.toolStripControlBox.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripControlBox.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonCollapseAll,
            this.toolStripButtonExpandAll,
            this.toolStripButtonToggleChecks});
            this.toolStripControlBox.Location = new System.Drawing.Point(154, 0);
            this.toolStripControlBox.Name = "toolStripControlBox";
            this.toolStripControlBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.toolStripControlBox.Size = new System.Drawing.Size(103, 25);
            this.toolStripControlBox.TabIndex = 12;
            this.toolStripControlBox.TabStop = true;
            // 
            // toolStripButtonCollapseAll
            // 
            this.toolStripButtonCollapseAll.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButtonCollapseAll.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonCollapseAll.Image = global::Admin.Properties.Resources.CollapsePNG;
            this.toolStripButtonCollapseAll.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonCollapseAll.Name = "toolStripButtonCollapseAll";
            this.toolStripButtonCollapseAll.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonCollapseAll.Text = "toolStripButton1";
            this.toolStripButtonCollapseAll.Click += new System.EventHandler(this.toolStripButtonCollapseAll_Click);
            // 
            // toolStripButtonExpandAll
            // 
            this.toolStripButtonExpandAll.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButtonExpandAll.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonExpandAll.Image = global::Admin.Properties.Resources.ExpandPNG;
            this.toolStripButtonExpandAll.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonExpandAll.Name = "toolStripButtonExpandAll";
            this.toolStripButtonExpandAll.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonExpandAll.Text = "toolStripButton2";
            this.toolStripButtonExpandAll.Click += new System.EventHandler(this.toolStripButtonExpandAll_Click);
            // 
            // toolStripButtonToggleChecks
            // 
            this.toolStripButtonToggleChecks.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButtonToggleChecks.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonToggleChecks.Image = global::Admin.Properties.Resources.GreenCheckPNG;
            this.toolStripButtonToggleChecks.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonToggleChecks.Name = "toolStripButtonToggleChecks";
            this.toolStripButtonToggleChecks.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonToggleChecks.Text = "toolStripButton3";
            this.toolStripButtonToggleChecks.Click += new System.EventHandler(this.toolStripButtonToggleChecks_Click);
            // 
            // RegionLanguageGroupGrid
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.toolStripControlBox);
            this.Controls.Add(this.ggRegionLanguages);
            this.Name = "RegionLanguageGroupGrid";
            this.Size = new System.Drawing.Size(257, 314);
            ((System.ComponentModel.ISupportInitialize)(this.ggRegionLanguages)).EndInit();
            this.toolStripControlBox.ResumeLayout(false);
            this.toolStripControlBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Wirestone.WinForms.GroupGrid.GroupGrid ggRegionLanguages;
        private System.Windows.Forms.ToolStrip toolStripControlBox;
        private System.Windows.Forms.ToolStripButton toolStripButtonCollapseAll;
        private System.Windows.Forms.ToolStripButton toolStripButtonExpandAll;
        private System.Windows.Forms.ToolStripButton toolStripButtonToggleChecks;
    }
}
