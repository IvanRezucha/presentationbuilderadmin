namespace Admin.UserControls
{
	partial class StatusReport
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.lblDetails = new System.Windows.Forms.Label();
			this.pnlOverallStatus = new System.Windows.Forms.Panel();
			this.lblTotalWarnings = new System.Windows.Forms.Label();
			this.lblTotalErrors = new System.Windows.Forms.Label();
			this.lblTotalSuccesses = new System.Windows.Forms.Label();
			this.lblTotalActionItems = new System.Windows.Forms.Label();
			this.lblOverallStatus = new System.Windows.Forms.Label();
			this.picOverallStatus = new System.Windows.Forms.PictureBox();
			this.dgvStatus = new System.Windows.Forms.DataGridView();
			this.iconDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewImageColumn();
			this.actionItemDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.StatusResult = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.messageDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.bindingSource = new System.Windows.Forms.BindingSource(this.components);
			this.StatusDataSet = new System.Data.DataSet();
			this.dataTable1 = new System.Data.DataTable();
			this.dataColumn1 = new System.Data.DataColumn();
			this.dataColumn3 = new System.Data.DataColumn();
			this.dataColumn4 = new System.Data.DataColumn();
			this.dataColumn5 = new System.Data.DataColumn();
			this.dataColumn2 = new System.Data.DataColumn();
			this.pnlOverallStatus.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.picOverallStatus)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dgvStatus)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.bindingSource)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.StatusDataSet)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataTable1)).BeginInit();
			this.SuspendLayout();
			// 
			// lblDetails
			// 
			this.lblDetails.AutoSize = true;
			this.lblDetails.Location = new System.Drawing.Point(3, 71);
			this.lblDetails.Name = "lblDetails";
			this.lblDetails.Size = new System.Drawing.Size(42, 13);
			this.lblDetails.TabIndex = 3;
			this.lblDetails.Text = "Details:";
			// 
			// pnlOverallStatus
			// 
			this.pnlOverallStatus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.pnlOverallStatus.BackColor = System.Drawing.Color.White;
			this.pnlOverallStatus.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.pnlOverallStatus.Controls.Add(this.lblTotalWarnings);
			this.pnlOverallStatus.Controls.Add(this.lblTotalErrors);
			this.pnlOverallStatus.Controls.Add(this.lblTotalSuccesses);
			this.pnlOverallStatus.Controls.Add(this.lblTotalActionItems);
			this.pnlOverallStatus.Controls.Add(this.lblOverallStatus);
			this.pnlOverallStatus.Controls.Add(this.picOverallStatus);
			this.pnlOverallStatus.Location = new System.Drawing.Point(3, 3);
			this.pnlOverallStatus.Name = "pnlOverallStatus";
			this.pnlOverallStatus.Size = new System.Drawing.Size(357, 52);
			this.pnlOverallStatus.TabIndex = 4;
			// 
			// lblTotalWarnings
			// 
			this.lblTotalWarnings.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.lblTotalWarnings.AutoSize = true;
			this.lblTotalWarnings.Location = new System.Drawing.Point(291, 29);
			this.lblTotalWarnings.Name = "lblTotalWarnings";
			this.lblTotalWarnings.Size = new System.Drawing.Size(13, 13);
			this.lblTotalWarnings.TabIndex = 5;
			this.lblTotalWarnings.Text = "0";
			// 
			// lblTotalErrors
			// 
			this.lblTotalErrors.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.lblTotalErrors.AutoSize = true;
			this.lblTotalErrors.Location = new System.Drawing.Point(291, 7);
			this.lblTotalErrors.Name = "lblTotalErrors";
			this.lblTotalErrors.Size = new System.Drawing.Size(13, 13);
			this.lblTotalErrors.TabIndex = 4;
			this.lblTotalErrors.Text = "0";
			// 
			// lblTotalSuccesses
			// 
			this.lblTotalSuccesses.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.lblTotalSuccesses.AutoSize = true;
			this.lblTotalSuccesses.Location = new System.Drawing.Point(209, 29);
			this.lblTotalSuccesses.Name = "lblTotalSuccesses";
			this.lblTotalSuccesses.Size = new System.Drawing.Size(13, 13);
			this.lblTotalSuccesses.TabIndex = 3;
			this.lblTotalSuccesses.Text = "0";
			// 
			// lblTotalActionItems
			// 
			this.lblTotalActionItems.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.lblTotalActionItems.AutoSize = true;
			this.lblTotalActionItems.Location = new System.Drawing.Point(209, 7);
			this.lblTotalActionItems.Name = "lblTotalActionItems";
			this.lblTotalActionItems.Size = new System.Drawing.Size(13, 13);
			this.lblTotalActionItems.TabIndex = 2;
			this.lblTotalActionItems.Text = "0";
			// 
			// lblOverallStatus
			// 
			this.lblOverallStatus.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.lblOverallStatus.AutoSize = true;
			this.lblOverallStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblOverallStatus.Location = new System.Drawing.Point(52, 15);
			this.lblOverallStatus.Name = "lblOverallStatus";
			this.lblOverallStatus.Size = new System.Drawing.Size(83, 16);
			this.lblOverallStatus.TabIndex = 1;
			this.lblOverallStatus.Text = "Updating...";
			// 
			// picOverallStatus
			// 
			this.picOverallStatus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)));
			this.picOverallStatus.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
			this.picOverallStatus.Location = new System.Drawing.Point(12, 7);
			this.picOverallStatus.Name = "picOverallStatus";
			this.picOverallStatus.Size = new System.Drawing.Size(34, 34);
			this.picOverallStatus.TabIndex = 0;
			this.picOverallStatus.TabStop = false;
			// 
			// dgvStatus
			// 
			this.dgvStatus.AllowUserToAddRows = false;
			this.dgvStatus.AllowUserToDeleteRows = false;
			this.dgvStatus.AllowUserToResizeRows = false;
			this.dgvStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.dgvStatus.AutoGenerateColumns = false;
			this.dgvStatus.BackgroundColor = System.Drawing.Color.White;
			this.dgvStatus.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvStatus.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iconDataGridViewTextBoxColumn,
            this.actionItemDataGridViewTextBoxColumn,
            this.StatusResult,
            this.messageDataGridViewTextBoxColumn});
			this.dgvStatus.DataSource = this.bindingSource;
			this.dgvStatus.EnableHeadersVisualStyles = false;
			this.dgvStatus.Location = new System.Drawing.Point(4, 87);
			this.dgvStatus.MultiSelect = false;
			this.dgvStatus.Name = "dgvStatus";
			this.dgvStatus.ReadOnly = true;
			this.dgvStatus.RowHeadersVisible = false;
			this.dgvStatus.Size = new System.Drawing.Size(354, 251);
			this.dgvStatus.TabIndex = 5;
			this.dgvStatus.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvStatus_DataBindingComplete);
			// 
			// iconDataGridViewTextBoxColumn
			// 
			this.iconDataGridViewTextBoxColumn.HeaderText = "";
			this.iconDataGridViewTextBoxColumn.Name = "iconDataGridViewTextBoxColumn";
			this.iconDataGridViewTextBoxColumn.ReadOnly = true;
			this.iconDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
			this.iconDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
			this.iconDataGridViewTextBoxColumn.Width = 20;
			// 
			// actionItemDataGridViewTextBoxColumn
			// 
			this.actionItemDataGridViewTextBoxColumn.DataPropertyName = "ActionItem";
			this.actionItemDataGridViewTextBoxColumn.HeaderText = "Action Item";
			this.actionItemDataGridViewTextBoxColumn.Name = "actionItemDataGridViewTextBoxColumn";
			this.actionItemDataGridViewTextBoxColumn.ReadOnly = true;
			this.actionItemDataGridViewTextBoxColumn.Width = 175;
			// 
			// StatusResult
			// 
			this.StatusResult.DataPropertyName = "StatusResult";
			this.StatusResult.HeaderText = "Status";
			this.StatusResult.Name = "StatusResult";
			this.StatusResult.ReadOnly = true;
			this.StatusResult.Width = 75;
			// 
			// messageDataGridViewTextBoxColumn
			// 
			this.messageDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			this.messageDataGridViewTextBoxColumn.DataPropertyName = "Message";
			this.messageDataGridViewTextBoxColumn.HeaderText = "Message";
			this.messageDataGridViewTextBoxColumn.Name = "messageDataGridViewTextBoxColumn";
			this.messageDataGridViewTextBoxColumn.ReadOnly = true;
			// 
			// bindingSource
			// 
			this.bindingSource.DataMember = "ActionItem";
			this.bindingSource.DataSource = this.StatusDataSet;
			// 
			// StatusDataSet
			// 
			this.StatusDataSet.DataSetName = "StatusDataSet";
			this.StatusDataSet.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTable1});
			// 
			// dataTable1
			// 
			this.dataTable1.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn3,
            this.dataColumn4,
            this.dataColumn5,
            this.dataColumn2});
			this.dataTable1.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "ActionID"}, true)});
			this.dataTable1.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumn1};
			this.dataTable1.TableName = "ActionItem";
			// 
			// dataColumn1
			// 
			this.dataColumn1.AllowDBNull = false;
			this.dataColumn1.AutoIncrement = true;
			this.dataColumn1.Caption = "ActionID";
			this.dataColumn1.ColumnName = "ActionID";
			this.dataColumn1.DataType = typeof(int);
			// 
			// dataColumn3
			// 
			this.dataColumn3.Caption = "ActionItem";
			this.dataColumn3.ColumnName = "ActionItem";
			// 
			// dataColumn4
			// 
			this.dataColumn4.Caption = "StatusResult";
			this.dataColumn4.ColumnName = "StatusResult";
			// 
			// dataColumn5
			// 
			this.dataColumn5.Caption = "Message";
			this.dataColumn5.ColumnName = "Message";
			// 
			// dataColumn2
			// 
			this.dataColumn2.Caption = "StatusImage";
			this.dataColumn2.ColumnName = "StatusImage";
			// 
			// StatusReport
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.dgvStatus);
			this.Controls.Add(this.pnlOverallStatus);
			this.Controls.Add(this.lblDetails);
			this.Name = "StatusReport";
			this.Size = new System.Drawing.Size(363, 342);
			this.pnlOverallStatus.ResumeLayout(false);
			this.pnlOverallStatus.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.picOverallStatus)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dgvStatus)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.bindingSource)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.StatusDataSet)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataTable1)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label lblDetails;
		private System.Windows.Forms.Panel pnlOverallStatus;
		private System.Windows.Forms.DataGridView dgvStatus;
		private System.Data.DataSet StatusDataSet;
		private System.Data.DataTable dataTable1;
		private System.Data.DataColumn dataColumn1;
		private System.Data.DataColumn dataColumn3;
		private System.Data.DataColumn dataColumn4;
		private System.Data.DataColumn dataColumn5;
		private System.Windows.Forms.BindingSource bindingSource;
		private System.Data.DataColumn dataColumn2;
		private System.Windows.Forms.DataGridViewImageColumn iconDataGridViewTextBoxColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn actionItemDataGridViewTextBoxColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn StatusResult;
		private System.Windows.Forms.DataGridViewTextBoxColumn messageDataGridViewTextBoxColumn;
		private System.Windows.Forms.Label lblTotalWarnings;
		private System.Windows.Forms.Label lblTotalErrors;
		private System.Windows.Forms.Label lblTotalSuccesses;
		private System.Windows.Forms.Label lblTotalActionItems;
		private System.Windows.Forms.Label lblOverallStatus;
		private System.Windows.Forms.PictureBox picOverallStatus;
	}
}
