using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Admin
{
	public partial class SlideMultipleUpdate : MasterForm
	{
		public List<int> _slideIDs;
		private HpDm.DAL.Slide _slide;
        private SlideMultipleUpdateType _SlideMultipleUpdateType;

        private wsIpgAdmin.SlideLibrary _slideLibrary;

		public SlideMultipleUpdate(List<int> slideIDs, SlideMultipleUpdateType slideMultipleUpdateType, wsIpgAdmin.SlideLibrary dsSlideLibrary)
		{
			Cursor = Cursors.WaitCursor;

			InitializeComponent();
			// Set the selected rows
			_slideIDs = slideIDs;

            _SlideMultipleUpdateType = slideMultipleUpdateType;

			// Initialize the slideFiltersAndLanguages control
			CustomFeaturesSetup();

            SetupLabels();
			// Fill the slide table
			_slide = new HpDm.DAL.Slide();
            _slideLibrary = new Admin.wsIpgAdmin.SlideLibrary();
            _slideLibrary.Merge(dsSlideLibrary);
            _slideLibrary = dsSlideLibrary;

          

			Cursor = Cursors.Default;
		}

        private void SetupLabels()
        {
            string labelText = "";
            switch (_SlideMultipleUpdateType)
            {
                case SlideMultipleUpdateType.RegionLanguageOnly:
                    labelText = "Reset Slide Regions and Languages";
                    this.Text = labelText;
                    grpSlideMulipleUpdate.Text = labelText;
                    break;
                case SlideMultipleUpdateType.FilterOnly:
                    labelText = "Reset Slide Filters";
                    this.Text = labelText;
                    grpSlideMulipleUpdate.Text = labelText;
                    break;
                case SlideMultipleUpdateType.RegionLanguageAndFilter:
                    labelText = "Reset Slide Filters/Regions and Languages";
                    this.Text = labelText;
                    grpSlideMulipleUpdate.Text = labelText;
                    break;
                default:
                    //do nothing
                    break;
            }
        }

		private void CustomFeaturesSetup()
		{
			wsIpgAdmin.Admin adminWS = new Admin.wsIpgAdmin.Admin();
			wsIpgAdmin.SlideLibrary dsFilterAndRegLangInfo = new Admin.wsIpgAdmin.SlideLibrary();


            if (_SlideMultipleUpdateType == SlideMultipleUpdateType.RegionLanguageOnly || _SlideMultipleUpdateType == SlideMultipleUpdateType.RegionLanguageAndFilter)
            {
			    if (Properties.Settings.Default.IsRegionManagmentEnabled)
			    {
				    dsFilterAndRegLangInfo.Merge(adminWS.RegionsAndLanguagesGet());
			    }
            }

            if (_SlideMultipleUpdateType == SlideMultipleUpdateType.FilterOnly || _SlideMultipleUpdateType == SlideMultipleUpdateType.RegionLanguageAndFilter)
            {
			    if (Properties.Settings.Default.IsFilterManagementEnabled)
			    {
				    dsFilterAndRegLangInfo.Merge(adminWS.FilterGet());
			    }
            }

			Admin.UserControls.SlideFiltersAndLanguagesFillType fillType = Admin.UserControls.SlideFiltersAndLanguagesFillType.FiltersAndRegionLanguages;

            if (_SlideMultipleUpdateType == SlideMultipleUpdateType.RegionLanguageOnly)
            {
                fillType = Admin.UserControls.SlideFiltersAndLanguagesFillType.RegionLanguagesOnly;
            }

            else if (_SlideMultipleUpdateType == SlideMultipleUpdateType.FilterOnly)
            {

                fillType = Admin.UserControls.SlideFiltersAndLanguagesFillType.FiltersOnly;
            }
            else if (_SlideMultipleUpdateType == SlideMultipleUpdateType.RegionLanguageAndFilter)
            {
                fillType = Admin.UserControls.SlideFiltersAndLanguagesFillType.FiltersAndRegionLanguages;
            }
			slideFiltersAndLanguages.Fill(dsFilterAndRegLangInfo, fillType);
		}

		private void btnClose_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void Update_Click(object sender, EventArgs e)
		{
			// Valid form
			if (!IsFormValid())
			{
				return;
			}

			Cursor = Cursors.WaitCursor;
			status.Text = "Updating...";

			List<int> filterIDs;
			List<int> regionLanguageIDs;
			wsIpgAdmin.SlideLibrary.SlideRow slideRow;
			wsIpgAdmin.SlideLibrary.SlideRegionLanguageRow slideRegionLanguageRow;
			wsIpgAdmin.SlideLibrary.SlideFilterRow slideFilterRow;
			
			// Get any Filters or RegionLanguages selected
            //if (_SlideMultipleUpdateType == SlideMultipleUpdateType.FilterOnly)
            //{
            //    filterIDs = slideFiltersAndLanguages.filterTabs.GetSelectedFilterIDs();
            //}
            //else if (_SlideMultipleUpdateType == SlideMultipleUpdateType.RegionLanguageOnly)
            //{
            //    regionLanguageIDs = slideFiltersAndLanguages.regionLanguageGroupGrid.GetSelectedRegionLanguageIDs();
            //}
            //else if (_SlideMultipleUpdateType == SlideMultipleUpdateType.RegionLanguageAndFilter)
            //{
                filterIDs = slideFiltersAndLanguages.filterTabs.GetSelectedFilterIDs();
                regionLanguageIDs = slideFiltersAndLanguages.regionLanguageGroupGrid.GetSelectedRegionLanguageIDs();
            //}


			foreach (int slideID in _slideIDs)
			{
				slideRow = _slideLibrary.Slide.FindBySlideID(slideID);

                if (_SlideMultipleUpdateType == SlideMultipleUpdateType.RegionLanguageOnly || _SlideMultipleUpdateType == SlideMultipleUpdateType.RegionLanguageAndFilter)
                {
                    // Delete all region/language relationship records for this group of slides
                    foreach (wsIpgAdmin.SlideLibrary.SlideRegionLanguageRow srlRow in slideRow.GetSlideRegionLanguageRows())
                    {
                        srlRow.Delete();
                    }

                    // Add new slide region/language records
                    foreach (int regionLanguageID in regionLanguageIDs)
                    {
                        slideRegionLanguageRow = _slideLibrary.SlideRegionLanguage.NewSlideRegionLanguageRow();
                        slideRegionLanguageRow.SlideID = slideRow.SlideID;
                        slideRegionLanguageRow.RegionLanguageID = regionLanguageID;
                        _slideLibrary.SlideRegionLanguage.Rows.Add(slideRegionLanguageRow);
                    } 
                }

                if (_SlideMultipleUpdateType == SlideMultipleUpdateType.FilterOnly || _SlideMultipleUpdateType == SlideMultipleUpdateType.RegionLanguageAndFilter)
                {
                    // Delete all filter relationship records for this group of slides
                    foreach (wsIpgAdmin.SlideLibrary.SlideFilterRow sfRow in slideRow.GetSlideFilterRows())
                    {
                        sfRow.Delete();
                    }
                    // Add new slide filter records
                    foreach (int filterID in filterIDs)
                    {
                        slideFilterRow = _slideLibrary.SlideFilter.NewSlideFilterRow();
                        slideFilterRow.SlideID = slideRow.SlideID;
                        slideFilterRow.FilterID = filterID;
                        _slideLibrary.SlideFilter.Rows.Add(slideFilterRow);
                    } 
                }

			}

            _slide.UpdateCompleted += new HpDm.DAL.Slide.UpdateCompletedDelegate(_slide_UpdateCompleted);
			_slide.UpdateAsync((Admin.wsIpgAdmin.SlideLibrary)_slideLibrary.GetChanges());
		}

		private void _slide_UpdateCompleted(bool success, Admin.wsIpgAdmin.SlideLibrary slideLibrary)
		{
			if (success)
			{
				status.Text = "Successfully updated slides";
                System.Threading.Thread.Sleep(500);
                this.Close();
            }
			else
			{
				status.Text = "Failed to update slides";
			}

			Cursor = Cursors.Default;
		}

		private bool IsFormValid()
		{
			errFormValid.Clear();

            if (_SlideMultipleUpdateType == SlideMultipleUpdateType.RegionLanguageOnly || _SlideMultipleUpdateType == SlideMultipleUpdateType.RegionLanguageAndFilter)
            {
			    // Make sure there is at least one region/language selected
                if (Properties.Settings.Default.IsRegionManagmentEnabled && slideFiltersAndLanguages.regionLanguageGroupGrid.GetSelectedRegionLanguageIDs().Count == 0) ///.GetSelectedRegionLanguageIDs()
			    {
				    MessageBox.Show("Please select at least one region/language for these slides", "Slide error", MessageBoxButtons.OK);
				    errFormValid.SetError(slideFiltersAndLanguages, "Please select at least one region/language for these slides");
				    return false;
			    }
            }

            if (_SlideMultipleUpdateType == SlideMultipleUpdateType.FilterOnly || _SlideMultipleUpdateType == SlideMultipleUpdateType.RegionLanguageAndFilter)
            {
			    // Make sure there is at least one filter selected
			    if (Properties.Settings.Default.IsFilterManagementEnabled && slideFiltersAndLanguages.filterTabs.GetSelectedFilterIDs().Count == 0)
			    {
				    MessageBox.Show("Please select at least one filter for these slides", "Slide error", MessageBoxButtons.OK);
				    errFormValid.SetError(slideFiltersAndLanguages, "Please select at least one filter for these slides");
				    return false;
			    }
            }

			return true;
		}
	}

    public enum SlideMultipleUpdateType
    { 
        RegionLanguageOnly,
        FilterOnly,
        RegionLanguageAndFilter
    }
}