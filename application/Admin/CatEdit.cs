using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace Admin
{
    public partial class CatEdit : MasterForm
    {
        #region class variables

        int CatID;
        int ParentCatID;
        //wsIpgAdmin.SlideLibrary dsSlideLibrary;

        #endregion

        public CatEdit(int catID, int parentCatID, bool hasSlideChildren)
        {
            InitializeComponent();
            CatID = catID;
            ParentCatID = parentCatID;
            if (hasSlideChildren)
            {
                grpDefaultSubCategories.Visible = false;
                this.Height = this.Height - 441;
            }
        }

        private void CatEdit_Load(object sender, EventArgs e)
        {
            FillForm();
            PopulateDefaultSubCats();
        }

        private void FillForm()
        {
            //get the data from the web service
            wsIpgAdmin.Admin admin = new Admin.wsIpgAdmin.Admin();
            dsSlideLibrary = admin.GetCategory(CatID);

            PopulateCategoryTypes();

            //get the category
            wsIpgAdmin.SlideLibrary.CategoryRow category = dsSlideLibrary.Category.FindByCategoryID(CatID);

            if (category == null)//its a new category
            {
                category = dsSlideLibrary.Category.NewCategoryRow();

                category.Category = String.Empty;
                category.ParentCategoryID = ParentCatID;
                category.IsNew = false;
                category.ShowDescendantSlides = false;
                CatID = category.CategoryID;
                dsSlideLibrary.Category.AddCategoryRow(category);
            }
            else//editing an existing category
            {
                txtCategory.Text = category.Category;
                chkNew.Checked = category.IsNew;
                chkCategoryShowDescendantSlides.Checked = category.ShowDescendantSlides;
                CatID = category.CategoryID;
                SelectIcon(category.Icon);
                ParentCatID = category.ParentCategoryID;
            }
        }

        private void PopulateCategoryTypes()
        {
            foreach (wsIpgAdmin.SlideLibrary.CategoryTypeRow catTypeRow in dsSlideLibrary.CategoryType.Rows)
            {
                if (catTypeRow.CategoryTypeID > 1)// a hack to keep them from adding a main category
                {
                    ListViewItem lvItem = new ListViewItem();
                    lvItem.Tag = catTypeRow.CategoryTypeID;
                    lvItem.Text = catTypeRow.CategoryType;
                    lvItem.ImageKey = catTypeRow.Icon;
                    lvwCategoryType.Items.Add(lvItem);
                    lvwSubCatIcon.Items.Add((ListViewItem)lvItem.Clone());
                }
            }
        }

        private void SelectIcon(string icon)
        {
            foreach (ListViewItem lvItem in lvwCategoryType.Items)
            {
                if (lvItem.ImageKey == icon)
                {
                    lvItem.Selected = true;
                    return;
                }
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            bool valResult = ValidateForm();
            if (valResult)
            {
                this.Cursor = Cursors.WaitCursor;
                Save();
                this.Cursor = Cursors.Default;
            }
        }

        private void Save()
        {
            wsIpgAdmin.Admin admin = new Admin.wsIpgAdmin.Admin();

            wsIpgAdmin.SlideLibrary.CategoryRow category;

            category = dsSlideLibrary.Category.FindByCategoryID(CatID);
            category.Category = txtCategory.Text;
            category.IsNew = chkNew.Checked;
            category.Icon = lvwCategoryType.SelectedItems[0].ImageKey;
            category.ShowDescendantSlides = chkCategoryShowDescendantSlides.Checked;

            

            wsIpgAdmin.SlideLibrary dsChanges = (wsIpgAdmin.SlideLibrary)dsSlideLibrary.GetChanges();

            bool wsResult = false;
            string errorMessage = "";

            try
            {
                admin.Shizmo();
                dsSlideLibrary = admin.SaveCategory(dsChanges, CatID);
                wsIpgAdmin.SlideLibrary.CategoryRow catRow = (wsIpgAdmin.SlideLibrary.CategoryRow)dsSlideLibrary.Category.Rows[0];
                CreateDefaultSubCats(catRow.CategoryID);
                wsResult = true;
            }
            catch (Exception ex)
            {
                wsResult = false;
                errorMessage = ex.Message;
            }

            if (wsResult)
            {
                if (((Admin.AdminMain)this.Owner).IsDataFiltered)
                {
                    ((Admin.AdminMain)this.Owner).RefreshFilteredData();
                }
               
                ((Admin.AdminMain)this.Owner).RefreshData();         
            }
            else
            {
                MessageBox.Show("There was an error processing your request. Your change was not saved.");
            }
            this.Close();
        }


        private bool ValidateForm()
        {
            if (txtCategory.Text == "" || txtCategory.TextLength == 0)
            {
                MessageBox.Show("Category Name is required.", "Required field missing", MessageBoxButtons.OK);
                return false;
            }
            if (lvwCategoryType.SelectedItems.Count != 1)
            {
                MessageBox.Show("Icon is required.", "Required field missing", MessageBoxButtons.OK);
                return false;
            }

            foreach (TreeNode node in tvwDefaultSubCats.Nodes)
            {
                if (node.Checked)
                {
                    if (lvwSubCatIcon.SelectedItems.Count <= 0)
                    {
                        MessageBox.Show("Sub-Category Icon is required.", "Required field missing", MessageBoxButtons.OK);
                        return false;
                    }
                }
            }

            return true;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CreateDefaultSubCats(int parentCatID)
        {
            foreach (TreeNode node in tvwDefaultSubCats.Nodes)
            {
                if (node.Checked)
                {
                   AddSubCat(parentCatID, node); 
                } 
            }
        }

        private void AddSubCat(int parentCatID, TreeNode node)
        {
            wsIpgAdmin.SlideLibrary dsTemp = new Admin.wsIpgAdmin.SlideLibrary();
            wsIpgAdmin.SlideLibrary.CategoryRow category;

            category = dsTemp.Category.NewCategoryRow();
            //category = dsSlideLibrary.Category.NewCategoryRow();
            category.Category = node.Text;
            category.IsNew = chkSubCatNew.Checked;
            category.ShowDescendantSlides = chkSubCatShowDescendantSlides.Checked;
            category.Icon = lvwSubCatIcon.SelectedItems[0].ImageKey;
            category.ParentCategoryID = parentCatID;
            //dsSlideLibrary.Category.AddCategoryRow(category);
            dsTemp.Category.AddCategoryRow(category);
            wsIpgAdmin.SlideLibrary dsChanges = (wsIpgAdmin.SlideLibrary)dsTemp.GetChanges();
            wsIpgAdmin.Admin admin = new Admin.wsIpgAdmin.Admin();
            dsTemp = admin.SaveCategory(dsChanges, category.CategoryID);
            //return the new category's id
            int newCatID=  Convert.ToInt32(dsTemp.Category.Rows[0]["CategoryID"].ToString());

            foreach (TreeNode n in node.Nodes)
            {
                if (n.Checked)
                {
                    AddSubCat(newCatID, n);
                }
                
            }

            

            //int catID = category.CategoryID; //this slide group's ID



        }


        private void PopulateDefaultSubCats()
        {
            try
            {
                //Just a good practice -- change the cursor to a 
                //wait cursor while the nodes populate
                this.Cursor = Cursors.WaitCursor;
                //First, we'll load the Xml document
                XmlDocument xDoc = new XmlDocument();
                xDoc.LoadXml(Properties.Resources.DefaultSubCats);
                //Now, clear out the treeview, 
                //and add the first (root) node

                tvwDefaultSubCats.Nodes.Clear();

                foreach (XmlNode xmlNode in xDoc.DocumentElement.ChildNodes)
                {
                    if (xmlNode.NodeType == XmlNodeType.Element)
                    {
                        AddTreeNode(xmlNode, null);
                    }
                }
                //Expand the treeview to show all nodes
                tvwDefaultSubCats.ExpandAll();
            }
            catch (XmlException xExc)
            //Exception is thrown is there is an error in the Xml
            {
                MessageBox.Show(xExc.Message);
            }
            catch (Exception ex) //General exception
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default; //Change the cursor back
            }
        }
        //This function is called recursively until all nodes are loaded
        private void AddTreeNode(XmlNode xmlNode, TreeNode treeNode)
        {
            TreeNode tNode = new TreeNode(xmlNode.Attributes["name"].Value.ToString());
            if (treeNode == null)
            { 
                tvwDefaultSubCats.Nodes.Add(tNode);
            }
            else
            {
                treeNode.Nodes.Add(tNode);
            }
            foreach (XmlNode xNode in xmlNode.ChildNodes)
            {
                if (xNode.NodeType == XmlNodeType.Element)
                {
                    AddTreeNode(xNode, tNode);
                }
            }
        }
    }
}