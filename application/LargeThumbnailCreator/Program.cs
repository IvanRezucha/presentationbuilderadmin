﻿using System;
using System.Collections.Generic;
using System.Text;
using Aspose.Slides;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace LargeThumbnailCreator
{
	class Program
	{
		static void Main(string[] args)
		{
			License lic = new License();
			lic.SetLicense("Aspose.Slides.lic");

			string baseDir = Properties.Settings.Default.BasePath;

			string slidesDir = Properties.Settings.Default.SourcePPTFolder;

			string outDir = Properties.Settings.Default.OutJPGFolder;

			System.IO.DirectoryInfo dir = new DirectoryInfo(Path.Combine(baseDir, slidesDir));

			FileInfo[] files = dir.GetFiles("*.pot");
			
			int count = 1;

			foreach (FileInfo file in files)
			{
				string mess = string.Format("File {0} of {1}", count, files.Length);
				Console.WriteLine(mess);
				System.Diagnostics.Trace.WriteLine(mess);

				//get full size image at 75% jpg quality
				Image fullimage = GetSlideImage(file.FullName, 0, 1, .9398, "");

				if (fullimage == null)
					continue;

				foreach (string percent in Properties.Settings.Default.Percents)
				{

					try
					{
						double ratio = Convert.ToDouble(percent) / 100;

						Image thumb = fullimage.GetThumbnailImage((int)(fullimage.Width * ratio), (int)(fullimage.Height * ratio), null, IntPtr.Zero);

						string mess2 = string.Format("{0}% generated", ratio);
						Console.WriteLine(mess2);
						System.Diagnostics.Trace.WriteLine(mess2);

						string fullFilename = Path.Combine(baseDir, Path.Combine(outDir, string.Format(@"{0}\{1}.jpg", percent, Path.GetFileNameWithoutExtension(file.Name))));
						EncoderParameters encoderParameters = new EncoderParameters(1);
						encoderParameters.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, Properties.Settings.Default.JPGQuality);
						ImageCodecInfo imageCodecInfo = GetEncoderInfo("image/jpeg");
						thumb.Save(fullFilename, imageCodecInfo, encoderParameters);
					}
					catch (Exception ex)
					{
						string mess3 = string.Format("Error encountered, skipping");
						Console.WriteLine(mess3);
						System.Diagnostics.Trace.WriteLine(mess3);
					}

				}

				count++;

			}
		}

		private static ImageCodecInfo GetEncoderInfo(String mimeType)
		{
			int j;
			ImageCodecInfo[] encoders;
			encoders = ImageCodecInfo.GetImageEncoders();
			for (j = 0; j < encoders.Length; ++j)
			{
				if (encoders[j].MimeType == mimeType)
					return encoders[j];
			}
			return null;
		}

		public static Image GetSlideImage(string presentationFilePath, int slideIndex, double width, double height, string licensePath)
		{
			//SetLicense(licensePath);
			Aspose.Slides.Presentation tempPresentation = GetPresentationObject(presentationFilePath);
			if (tempPresentation == null)
				return null;
			return tempPresentation.Slides[slideIndex].GetThumbnail(width, height);
		}

		public static Aspose.Slides.Presentation GetPresentationObject(string presentationFilePath)
		{
			try
			{
				FileStream presStream = new FileStream(presentationFilePath, FileMode.Open, FileAccess.Read);
				Aspose.Slides.Presentation tempPresentation = new Aspose.Slides.Presentation(presStream);
				presStream.Close();
				return tempPresentation;
			}
			catch (Exception)
			{
				return null;
			}
			
		}
	}
}
