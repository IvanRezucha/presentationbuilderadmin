﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace MajorUpgrade.Utilities
{
	public class Upgrader
	{
		public bool DetectMajorUpdate(string productName, string publisherName)
		{
			try
			{
				wsUpgradeService.UpgradeService upgradeServiceInstance = new MajorUpgrade.wsUpgradeService.UpgradeService();
				if (upgradeServiceInstance.IsMajorUpgradeAvailable())
				{
					DialogResult dialogResult = MessageBox.Show("An important update is available.  Would you like to get it now?", "Important application update", MessageBoxButtons.YesNo, MessageBoxIcon.Information);

					if (dialogResult.Equals(DialogResult.Yes))
					{
						string newApplicationManifestUrl = upgradeServiceInstance.NewApplicationManifestUrl();
						UpgradeWizard upgradeWizard = new UpgradeWizard(newApplicationManifestUrl, productName, publisherName);
						if (upgradeWizard.ShowDialog().Equals(DialogResult.Yes))
							return true;
					}
				}
			}

			catch (Exception)
			{

			}

			return false;
		}
	}
}
