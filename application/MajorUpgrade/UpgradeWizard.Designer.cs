﻿namespace MajorUpgrade
{
	partial class UpgradeWizard
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lblOverview = new System.Windows.Forms.Label();
			this.lblTitle = new System.Windows.Forms.Label();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnBack = new System.Windows.Forms.Button();
			this.btnNext = new System.Windows.Forms.Button();
			this.panel1 = new System.Windows.Forms.Panel();
			this.pictureBoxLogo = new System.Windows.Forms.PictureBox();
			this.pictureBoxDetails = new System.Windows.Forms.PictureBox();
			this.linkLabelEmailSupport = new System.Windows.Forms.LinkLabel();
			this.lblIntroduction1 = new System.Windows.Forms.Label();
			this.lblIntroduction2 = new System.Windows.Forms.Label();
			this.lblIntroduction4 = new System.Windows.Forms.Label();
			this.linkLabelEmailSupportIntroduction = new System.Windows.Forms.LinkLabel();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBoxDetails)).BeginInit();
			this.SuspendLayout();
			// 
			// lblOverview
			// 
			this.lblOverview.Location = new System.Drawing.Point(50, 26);
			this.lblOverview.Name = "lblOverview";
			this.lblOverview.Size = new System.Drawing.Size(422, 40);
			this.lblOverview.TabIndex = 1;
			this.lblOverview.Text = "Overview";
			// 
			// lblTitle
			// 
			this.lblTitle.AutoSize = true;
			this.lblTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblTitle.Location = new System.Drawing.Point(12, 9);
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Size = new System.Drawing.Size(32, 13);
			this.lblTitle.TabIndex = 0;
			this.lblTitle.Text = "Title";
			// 
			// btnCancel
			// 
			this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnCancel.Location = new System.Drawing.Point(398, 356);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(75, 23);
			this.btnCancel.TabIndex = 2;
			this.btnCancel.Text = "Cancel";
			this.btnCancel.UseVisualStyleBackColor = true;
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// btnBack
			// 
			this.btnBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnBack.Enabled = false;
			this.btnBack.Location = new System.Drawing.Point(242, 356);
			this.btnBack.Name = "btnBack";
			this.btnBack.Size = new System.Drawing.Size(75, 23);
			this.btnBack.TabIndex = 1;
			this.btnBack.Text = "< Back";
			this.btnBack.UseVisualStyleBackColor = true;
			this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
			// 
			// btnNext
			// 
			this.btnNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnNext.Location = new System.Drawing.Point(317, 356);
			this.btnNext.Name = "btnNext";
			this.btnNext.Size = new System.Drawing.Size(75, 23);
			this.btnNext.TabIndex = 0;
			this.btnNext.Text = "Next >";
			this.btnNext.UseVisualStyleBackColor = true;
			this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.White;
			this.panel1.Controls.Add(this.lblTitle);
			this.panel1.Controls.Add(this.lblOverview);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(481, 75);
			this.panel1.TabIndex = 3;
			// 
			// pictureBoxLogo
			// 
			this.pictureBoxLogo.Image = global::MajorUpgrade.Properties.Resources.Logo;
			this.pictureBoxLogo.InitialImage = null;
			this.pictureBoxLogo.Location = new System.Drawing.Point(0, 81);
			this.pictureBoxLogo.Name = "pictureBoxLogo";
			this.pictureBoxLogo.Size = new System.Drawing.Size(76, 70);
			this.pictureBoxLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pictureBoxLogo.TabIndex = 4;
			this.pictureBoxLogo.TabStop = false;
			// 
			// pictureBoxDetails
			// 
			this.pictureBoxDetails.Location = new System.Drawing.Point(82, 81);
			this.pictureBoxDetails.Name = "pictureBoxDetails";
			this.pictureBoxDetails.Size = new System.Drawing.Size(390, 150);
			this.pictureBoxDetails.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pictureBoxDetails.TabIndex = 5;
			this.pictureBoxDetails.TabStop = false;
			// 
			// linkLabelEmailSupport
			// 
			this.linkLabelEmailSupport.AutoSize = true;
			this.linkLabelEmailSupport.Location = new System.Drawing.Point(8, 361);
			this.linkLabelEmailSupport.Name = "linkLabelEmailSupport";
			this.linkLabelEmailSupport.Size = new System.Drawing.Size(94, 13);
			this.linkLabelEmailSupport.TabIndex = 7;
			this.linkLabelEmailSupport.TabStop = true;
			this.linkLabelEmailSupport.Text = "Email tech support";
			this.linkLabelEmailSupport.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelEmailSupport_LinkClicked);
			// 
			// lblIntroduction1
			// 
			this.lblIntroduction1.AutoSize = true;
			this.lblIntroduction1.Location = new System.Drawing.Point(82, 81);
			this.lblIntroduction1.Name = "lblIntroduction1";
			this.lblIntroduction1.Size = new System.Drawing.Size(276, 13);
			this.lblIntroduction1.TabIndex = 9;
			this.lblIntroduction1.Text = "This upgrade process will only take a couple of minutes.  ";
			// 
			// lblIntroduction2
			// 
			this.lblIntroduction2.AutoSize = true;
			this.lblIntroduction2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblIntroduction2.Location = new System.Drawing.Point(82, 105);
			this.lblIntroduction2.Name = "lblIntroduction2";
			this.lblIntroduction2.Size = new System.Drawing.Size(360, 13);
			this.lblIntroduction2.TabIndex = 10;
			this.lblIntroduction2.Text = "You will not need to download the presentation content again.";
			// 
			// lblIntroduction4
			// 
			this.lblIntroduction4.AutoSize = true;
			this.lblIntroduction4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
			this.lblIntroduction4.Location = new System.Drawing.Point(82, 127);
			this.lblIntroduction4.Name = "lblIntroduction4";
			this.lblIntroduction4.Size = new System.Drawing.Size(179, 91);
			this.lblIntroduction4.TabIndex = 12;
			this.lblIntroduction4.Text = "The upgrade wizard steps are:\r\n\r\n1) Uninstall the current version\r\n\r\n2) Install t" +
				"he latest version\r\n\r\nQuestions or concerns?  Contact us:\r\n";
			// 
			// linkLabelEmailSupportIntroduction
			// 
			this.linkLabelEmailSupportIntroduction.AutoSize = true;
			this.linkLabelEmailSupportIntroduction.Location = new System.Drawing.Point(257, 205);
			this.linkLabelEmailSupportIntroduction.Name = "linkLabelEmailSupportIntroduction";
			this.linkLabelEmailSupportIntroduction.Size = new System.Drawing.Size(143, 13);
			this.linkLabelEmailSupportIntroduction.TabIndex = 13;
			this.linkLabelEmailSupportIntroduction.TabStop = true;
			this.linkLabelEmailSupportIntroduction.Text = "support@hpipgproducts.com";
			this.linkLabelEmailSupportIntroduction.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelEmailSupportIntroduction_LinkClicked);
			// 
			// UpgradeWizard
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(481, 391);
			this.Controls.Add(this.linkLabelEmailSupportIntroduction);
			this.Controls.Add(this.lblIntroduction4);
			this.Controls.Add(this.lblIntroduction2);
			this.Controls.Add(this.lblIntroduction1);
			this.Controls.Add(this.linkLabelEmailSupport);
			this.Controls.Add(this.pictureBoxDetails);
			this.Controls.Add(this.pictureBoxLogo);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnNext);
			this.Controls.Add(this.btnBack);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Name = "UpgradeWizard";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Upgrade Wizard";
			this.Load += new System.EventHandler(this.UpgradeWizard_Load);
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.UpgradeWizard_FormClosing);
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBoxDetails)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label lblOverview;
		private System.Windows.Forms.Label lblTitle;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnBack;
		private System.Windows.Forms.Button btnNext;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.PictureBox pictureBoxLogo;
		private System.Windows.Forms.PictureBox pictureBoxDetails;
		private System.Windows.Forms.LinkLabel linkLabelEmailSupport;
		private System.Windows.Forms.Label lblIntroduction1;
		private System.Windows.Forms.Label lblIntroduction2;
		private System.Windows.Forms.Label lblIntroduction4;
		private System.Windows.Forms.LinkLabel linkLabelEmailSupportIntroduction;


	}
}