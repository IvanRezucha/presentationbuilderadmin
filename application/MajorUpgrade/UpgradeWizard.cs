﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using System.Deployment.Application;

namespace MajorUpgrade
{
	public partial class UpgradeWizard : Form
	{
		#region Properties

		private string DeploymentUrl { get; set; }
		private string CurrentProductName { get; set; }
		private string CurrentPublisherName { get; set; }
		private string UninstallArguments { get; set; }
		private string AppReferencePath { get; set; }
		private WizardStep CurrentStep { get; set; }
		private int UninstallAttemptCount { get; set; }
		private BackgroundWorker UninstallBackgroundWorker { get; set; }
		private bool IsLaserJetInstall
		{
			get
			{
				return Properties.Settings.Default.InstallFlavor == "LaserJet";
			}
		}

		#endregion

		/// <summary>
		/// Initializes a new instance of the <see cref="UpgradeWizard"/> class.
		/// </summary>
		/// <param name="deploymentUrl">The complete URL to the manifest file for the new installer.</param>
		public UpgradeWizard(string deploymentUrl, string productName, string publisherName)
		{
			InitializeComponent();
			DeploymentUrl = deploymentUrl;
			CurrentProductName = productName;
			CurrentPublisherName = publisherName;
			UninstallAttemptCount = 0;
			DetermineInstallParametersAndSetWizardStep();

			UninstallBackgroundWorker = new BackgroundWorker();
			UninstallBackgroundWorker.WorkerReportsProgress = false;
			UninstallBackgroundWorker.WorkerSupportsCancellation = false;
			UninstallBackgroundWorker.DoWork += new DoWorkEventHandler(UninstallBackgroundWorker_DoWork);
			UninstallBackgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(UninstallBackgroundWorker_RunWorkerCompleted);
		}

		#region Uninstall Background Worker Methods

		void UninstallBackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
		{
			ProcessStartInfo psi = new ProcessStartInfo();
			psi.UseShellExecute = true;
			psi.FileName = "rundll32.exe";
			psi.Arguments = UninstallArguments;

			Process proc = Process.Start(psi);
			proc.WaitForExit();
			if (!proc.HasExited)
			{
				proc.Kill();
			}
		}

		void UninstallBackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			Cursor = Cursors.Default;
			BringToFront();

			if (!VerifyUninstall())
			{
				btnNext.Enabled = true;
				UninstallAttemptCount++;
				DialogResult dr;
				if (UninstallAttemptCount < 4)
				{
					dr = MessageBox.Show("The uninstall is not complete.  Please try again.",
						"Not uninstalled!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				}
				else
				{
					dr = MessageBox.Show("The uninstall is not complete.  Would you like to try again or do you want to cancel the wizard?",
						"Not uninstalled!", MessageBoxButtons.RetryCancel, MessageBoxIcon.Exclamation);
				}

				if (dr.Equals(DialogResult.OK) || dr.Equals(DialogResult.Retry))
				{
					Step(WizardStep.UninstallCurrentVersionStart);
				}
				else
				{
					Close();
				}
			}
			else
			{
				Step(WizardStep.InstallNewVersionStart);
			}
		}

		#endregion

		#region Wizard Steps

		/// <summary>
		/// Steps the specified wizard step.
		/// </summary>
		/// <param name="wizardStep">The wizard step.</param>
		private void Step(WizardStep wizardStep)
		{
			Debug.WriteLine(String.Format("'{0}'", DialogResult));
			BringToFront();
			CurrentStep = wizardStep;

			switch (wizardStep)
			{
				case WizardStep.Introduction:
					Debug.WriteLine(CurrentProductName);
					lblTitle.Text = String.Format("Welcome to the {0} upgrade wizard", CurrentProductName.Replace("&", "&&&"));
					lblOverview.Text = String.Format("This wizard will walk you through the steps needed to upgrade to the {1}latest version of the {0}.",
						CurrentProductName.Replace("&", "&&&"), Environment.NewLine);
					ToggleIntroductionLabelsVisibility(true);
					pictureBoxDetails.Visible = false;
					btnBack.Enabled = false;
					btnNext.Enabled = true;
					break;

				case WizardStep.UninstallCurrentVersionStart:
					lblTitle.Text = String.Format("Step 1: Uninstall the current version of {0}", CurrentProductName.Replace("&", "&&&"));
					lblOverview.Text = String.Format("Click the \"Next\" button to open the uninstall window pictured below.{0}"
						+ "Make sure you select \"Remove the application from this computer\" and then click the OK button.", 
						Environment.NewLine);
					ToggleIntroductionLabelsVisibility(false);
					pictureBoxDetails.Image = IsLaserJetInstall ? Properties.Resources.LjUninstallGif : Properties.Resources.IpgUninstallGif;
					pictureBoxDetails.Visible = true;
					btnBack.Enabled = true;
					btnNext.Enabled = true;
					break;

				case WizardStep.InstallNewVersionStart:
					lblTitle.Text = String.Format("Step 2: Install the new version of {0}", CurrentProductName.Replace("&", "&&&"));
					lblOverview.Text = String.Format("Click the \"Finish\" button to exit the wizard and launch the install window pictured below.{0}"
						+ "Click the \"Install\" button to complete the upgrade.", Environment.NewLine);
					btnNext.Text = "Finish";
					ToggleIntroductionLabelsVisibility(false);
					pictureBoxDetails.Image = IsLaserJetInstall ? Properties.Resources.LjInstallGif : Properties.Resources.IpgInstallGif;
					pictureBoxDetails.Visible = true;
					btnBack.Enabled = false;
					btnNext.Enabled = true;
					btnCancel.Enabled = false;
					break;

				default:
					throw new Exception("Unknown wizard step requested.");
			}
		}

		private void ToggleIntroductionLabelsVisibility(bool visible)
		{
			lblIntroduction1.Visible =
				lblIntroduction2.Visible =
				lblIntroduction4.Visible =
				linkLabelEmailSupportIntroduction.Visible = visible;
		}

		#endregion

		#region Back, Next Buttons

		private void btnNext_Click(object sender, EventArgs e)
		{
			switch (CurrentStep)
			{
				case WizardStep.Introduction:
					Step(WizardStep.UninstallCurrentVersionStart);
					break;

				case WizardStep.UninstallCurrentVersionStart:
					Cursor = Cursors.WaitCursor;
					btnBack.Enabled = btnNext.Enabled = false;
					UninstallBackgroundWorker.RunWorkerAsync();
					break;

				case WizardStep.InstallNewVersionStart:
					LaunchInstallWindow();
					DialogResult = DialogResult.Yes;
					
					Application.Exit();
					break;

				default:
					throw new Exception("Unknown wizard step encountered.");
			}
		}
		
		private void btnBack_Click(object sender, EventArgs e)
		{
			switch (CurrentStep)
			{
				case WizardStep.Introduction:
					//nothing...
					break;

				case WizardStep.UninstallCurrentVersionStart:
					Step(WizardStep.Introduction);
					break;

				case WizardStep.InstallNewVersionStart:
					Step(WizardStep.UninstallCurrentVersionStart);
					break;

				case WizardStep.InstallNewVersionNext:
					Step(WizardStep.InstallNewVersionStart);
					break;

				default:
					throw new Exception("Unknown wizard step encountered.");
			}
		}

		#endregion

		#region Install, uninstall helpers
		
		/// <summary>
		/// Verifies the uninstall by checking that the start menu shortcut is gone.
		/// </summary>
		/// <returns></returns>
		private bool VerifyUninstall()
		{
			return !File.Exists(AppReferencePath);
		}

		/// <summary>
		/// Launches the install window.
		/// </summary>
		private void LaunchInstallWindow()
		{
			Process.Start(DeploymentUrl);
		}

		/// <summary>
		/// Determines the install parameters and sets the appropriate wizard step for the user's configuration.
		/// </summary>
		private void DetermineInstallParametersAndSetWizardStep()
		{
			AppReferencePath = Path.Combine(
				Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Programs), CurrentPublisherName),
				CurrentProductName + ".APPREF-MS");

			if (!File.Exists(AppReferencePath))
			{
				//not the expected case... they've somehow managed to uninstall the app after launching it but before getting here...
				Step(WizardStep.InstallNewVersionStart);
			}
			else
			{
				UninstallArguments = "dfshim.dll,ShArpMaintain ";

				string appReferenceDetails = String.Empty;
				using (StreamReader reader = new StreamReader(AppReferencePath))
				{
					appReferenceDetails = reader.ReadToEnd();
					
				}
				
				UninstallArguments += appReferenceDetails.Split('#')[1];
				Step(WizardStep.Introduction);
			}
		}

		#endregion

		#region Closing the form early

		private void btnCancel_Click(object sender, EventArgs e)
		{
			VerifyCancel();
		}
		
		private void UpgradeWizard_FormClosing(object sender, FormClosingEventArgs e)
		{
			//if you've uninstalled the app you must install the new one and I won't let you out of it.
			if ((CurrentStep.Equals(WizardStep.InstallNewVersionStart)) && (!DialogResult.Equals(DialogResult.Yes)))
			{
				e.Cancel = true;
				return;
			}

			//otherwise, make sure they confirm they want to quit.
			Debug.WriteLine(String.Format("'{0}'", DialogResult));
			if ((!DialogResult.Equals(DialogResult.Abort)) && (!DialogResult.Equals(DialogResult.Yes)))
			{
				if (!VerifyCancel())
				{
					e.Cancel = true;
				}
			}
		}

		private bool VerifyCancel()
		{
			DialogResult dr = MessageBox.Show("Are you sure you want to close the upgrade wizard?", "Close Upgrade Wizard?", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
			if (dr.Equals(DialogResult.Yes))
			{
				DialogResult = DialogResult.Abort;
				Close();
				return true;
			}
			else
			{
				return false;
			}
		}

		#endregion

		#region Support Links

		private void linkLabelEmailSupport_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
			EmailSupport();
		}

		private void linkLabelEmailSupportIntroduction_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
			EmailSupport();
		}

		private void EmailSupport()
		{
			Process.Start(String.Format("mailto:{1}?subject={0} Upgrade Support Request", CurrentProductName.Replace("&", "and"), Properties.Settings.Default.SupportEmail));
		}

		private void linkLabelSaveContactInfo_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
			try
			{
				string fileName = String.Format("{0} support.rtf", CurrentProductName);
				string desktopFilePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), fileName);
				using (StreamWriter sw = new StreamWriter(desktopFilePath))
				{
					sw.Write(Properties.Resources.ContactRtf);
				}
				MessageBox.Show(String.Format("{0} has been saved to your desktop.", fileName), "Contact information saved.", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			catch (Exception)
			{
				MessageBox.Show("Sorry, there was an error trying to save the contact information to your desktop.");
			}
		}

		#endregion

		enum WizardStep
		{
			Introduction,
			UninstallCurrentVersionStart,
			InstallNewVersionStart,
			InstallNewVersionNext
		}

		private void UpgradeWizard_Load(object sender, EventArgs e)
		{
			pictureBoxLogo.Image = IsLaserJetInstall ? Properties.Resources.Logo : Properties.Resources.IpgLogo;
		}

		
	}
}
