using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.IO;
using System.Net;
using System.Web.Configuration;

public partial class FileReceiver : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        foreach (string f in Request.Files.AllKeys)
        {
            HttpPostedFile file = Request.Files[f];
            string fileName = "";
            fileName = WebConfigurationManager.AppSettings["RootDir"] + @"\Resources\Ppt\" + file.FileName;
 
            //if it is a .ppt file save it as a .pot
            if (fileName.EndsWith(".ppt"))
            {
                fileName = fileName.Replace(".ppt", ".pot");
            }
            file.SaveAs(fileName);
        } 
    }
}
