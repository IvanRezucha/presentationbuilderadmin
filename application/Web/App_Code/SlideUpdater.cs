using System;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.Configuration;
using System.Configuration;
using System.Diagnostics;
using System.IO;


/// <summary>
/// Summary description for SlideUpdater
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class SlideUpdater : System.Web.Services.WebService
{
    TextWriterTraceListener diags;

    public SlideUpdater()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 

        diags = new TextWriterTraceListener(WebConfigurationManager.AppSettings["RootDir"].ToString() + @"\Resources\UpdateData\SlideUpdater.ws");
    }

    [WebMethod]
    public LocalizedContentUdpateDS BlankLocalizedContentUdpateDS()
    {
        throw new Exception("We need this DataSet in the win app.");
    }

    [WebMethod]
    public int CheckForUpdates(System.DateTime lastUpdate)
    {
        //return the number of data updates since the client's last update
        UpdateDS dsDataUpdate = new UpdateDS();
        UpdateDSTableAdapters.DataUpdateTableAdapter daDataUpdate = new UpdateDSTableAdapters.DataUpdateTableAdapter();
        daDataUpdate.FillWithNewerUpdates(dsDataUpdate.DataUpdate, lastUpdate);
        int updatesCount = dsDataUpdate.DataUpdate.Rows.Count;
        return updatesCount;
       

    }

    /// <summary>
    /// Returns the most recent LocalizedContentUpdateID if there are more recent updates, otherwise null.
    /// </summary>
    /// <param name="lastUpdateID"></param>
    /// <returns></returns>
    [WebMethod]
    public int? CheckForLocalizedContentUpdates(int lastUpdateID)
    {
        LocalizedContentUdpateDSTableAdapters.LocalizedContentUpdateTableAdapter taLocalizedContentUpdate = new LocalizedContentUdpateDSTableAdapters.LocalizedContentUpdateTableAdapter();
        
        try
        {
            return (int?)taLocalizedContentUpdate.GetMostRecentLocalizedContentUpdateId(lastUpdateID);
        }
        catch (Exception ex)
        {
            WriteToTrace(ex.Message);
            throw ex;
        }
    }

    [WebMethod]
    public bool IsLocalizedContentAvailable(int lastUpdateId)
    {        
        LocalizedContentUdpateDSTableAdapters.LocalizedContentUpdateTableAdapter taLocalizedContentUpdate = new LocalizedContentUdpateDSTableAdapters.LocalizedContentUpdateTableAdapter();
        LocalizedContentUdpateDS dsLocalizedContentUpdate = new LocalizedContentUdpateDS();
        
        try
        {
            //get all the updates
            taLocalizedContentUpdate.Fill(dsLocalizedContentUpdate.LocalizedContentUpdate);

            int? mostRecentUpdateId = taLocalizedContentUpdate.GetNewestLocalizedContentUpdateId();

            //get the update row that represents the clients last update and the newest update
            LocalizedContentUdpateDS.LocalizedContentUpdateRow lastClientUpdateRow = dsLocalizedContentUpdate.LocalizedContentUpdate.FindByLocalizedContentUpdateID(lastUpdateId);
            LocalizedContentUdpateDS.LocalizedContentUpdateRow newestContentUpdateRow = dsLocalizedContentUpdate.LocalizedContentUpdate.FindByLocalizedContentUpdateID(mostRecentUpdateId.Value);

            //if this is null, we'll assume something weird has happened and that they need a content update
            if (lastClientUpdateRow == null)
            {
                return true;
            }

            //we'll have to evaluate differenty if NetStorage is used
            if (Convert.ToBoolean(WebConfigurationManager.AppSettings["IsNetStorageSupported"].ToString()))
            {
                //if there is a newer update and that update was scheduled to be available before now, then an update is available
                if (lastClientUpdateRow.ContentUpdateTS < newestContentUpdateRow.ContentUpdateTS && newestContentUpdateRow.ContentUpdateTS < DateTime.UtcNow)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                //if there is a newer update, then an update is available
                if (lastClientUpdateRow.ContentUpdateTS < newestContentUpdateRow.ContentUpdateTS)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        catch (Exception ex)
        {
            WriteToTrace(ex.Message);
            throw ex;
        }
    }

    [WebMethod]
    public int? GetMostRecentLocalizedContentUpdateId()
    {
        LocalizedContentUdpateDSTableAdapters.LocalizedContentUpdateTableAdapter taLocalizedContentUpdate = new LocalizedContentUdpateDSTableAdapters.LocalizedContentUpdateTableAdapter();
        try
        {
            return taLocalizedContentUpdate.GetNewestLocalizedContentUpdateId();
        }
        catch (Exception ex)
        {
            WriteToTrace(ex.Message);
            throw ex;
        }
    }

    [WebMethod]
    public int CheckForDataUpdates(int lastUpdateID)
    {
        UpdateDS dsDataUpdate = new UpdateDS();
        UpdateDSTableAdapters.DataUpdateTableAdapter daDataUpdate = new UpdateDSTableAdapters.DataUpdateTableAdapter();
        daDataUpdate.FillByDataUpdateID(dsDataUpdate.DataUpdate, lastUpdateID);
        if (dsDataUpdate.DataUpdate.Rows.Count <= 0)
        {
            return CheckForUpdates(Convert.ToDateTime("1/1/1900"));
        }
        else 
        {
            return CheckForUpdates(Convert.ToDateTime(dsDataUpdate.DataUpdate.Rows[0]["DataUpdateTS"]));
        }
    }

    [WebMethod]
    public int GetMostRecentUpdateID()
    {
        UpdateDSTableAdapters.DataUpdateTableAdapter daDataUpdate = new UpdateDSTableAdapters.DataUpdateTableAdapter();
        return (int)daDataUpdate.GetMostRecentUpdateID();
    }

    [WebMethod]
    public SlideLibrary GetSlideLibrary()
    {
        SlideLibrary dsSlideLibrary = new SlideLibrary();
        dsSlideLibrary.ReadXml(WebConfigurationManager.AppSettings["Domain"].ToString() + WebConfigurationManager.AppSettings["ResourcesXml"].ToString() +
                                "SlideLibraryData.xml");
        return dsSlideLibrary;
    }

    [WebMethod]
    public string GetCurrentDeleteFileNames()
    {
        string fileNames = "";
        if (File.Exists(WebConfigurationManager.AppSettings["RootDir"].ToString() + @"\Resources\UpdateData\DeleteFiles.txt"))
        {
            fileNames = File.ReadAllText(WebConfigurationManager.AppSettings["RootDir"].ToString() + @"\Resources\UpdateData\DeleteFiles.txt");
        }
        return fileNames;
    }

    [WebMethod]
    public bool IsServiceAvailable()
    {
        return true;
    }

    [WebMethod]
    public void SendClientDataUpdateInfo(int DataVersion, string AppVersion, string OSVersion)
    {
        try
        {
            ClientDataUpdateDSTableAdapters.ClientDataUpdateTableAdapter taClientData = new ClientDataUpdateDSTableAdapters.ClientDataUpdateTableAdapter();
            taClientData.Insert(DateTime.Now, DataVersion, AppVersion, OSVersion);
        }
        catch (Exception ex)
        {
            
            
        }

        
    }

    [WebMethod]
    public SupportFileDS GetSupportFileData()
    {
        try
        {
            SupportFileDS dsSupportFiles = new SupportFileDS();
            SupportFileDSTableAdapters.SupportFileTableAdapter taSupportFiles = new SupportFileDSTableAdapters.SupportFileTableAdapter();
            taSupportFiles.Fill(dsSupportFiles.SupportFile);
            return dsSupportFiles;
        }
        catch (Exception ex)
        {
            //TODO: handle exception     
            throw ex;
        }
    }

    [WebMethod]
    public bool IsAnnouncementsAvailable()
    { 
        AnnouncementDSTableAdapters.AnnouncementTableAdapter taAnnouncement = new AnnouncementDSTableAdapters.AnnouncementTableAdapter();
        try 
	    {
            int? announcemntCount = taAnnouncement.CountAnnouncements();
            if (announcemntCount.HasValue && announcemntCount.Value > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
	    }
	    catch (Exception ex)
	    {
            WriteToTrace(ex.ToString());
		    throw ex;
	    }
        
    }

    private void WriteToTrace(string msg)
    {

        DateTime now = DateTime.Now;

        diags.WriteLine(now.ToShortDateString() + " " + now.ToShortTimeString() + " " + msg);

        diags.Flush();

    }

    #region old zipped stuff
    //[WebMethod]
    //public byte[] GetZippedSlideLibrary()
    //{
    //    byte[] buffer = null;
    //    if (File.Exists(WebConfigurationManager.AppSettings["RootDir"].ToString() + @"\Resources\UpdateData\UpdateSlideLibraryData.zip"))
    //    { 
    //        buffer = File.ReadAllBytes(WebConfigurationManager.AppSettings["RootDir"].ToString() + @"\Resources\UpdateData\UpdateSlideLibraryData.zip");
    //    }
    //    return buffer;
    //}

    //[WebMethod]
    //public byte[] GetCurrentUpdateZippedSlides()
    //{
    //    byte[] buffer = null;
    //    if (File.Exists(WebConfigurationManager.AppSettings["RootDir"].ToString() + @"\Resources\UpdateData\UpdateSlides.zip"))
    //    {
    //        buffer = File.ReadAllBytes(WebConfigurationManager.AppSettings["RootDir"].ToString() + @"\Resources\UpdateData\UpdateSlides.zip");
    //    }
    //    return buffer;
    //}

    //[WebMethod]
    //public byte[] GetFullUpdateZippedSlides()
    //{
    //    byte[] buffer = null;
    //    if (File.Exists(WebConfigurationManager.AppSettings["RootDir"].ToString() + @"\Resources\UpdateData\FullUpdateSlides.zip"))
    //    {
    //        buffer = File.ReadAllBytes(WebConfigurationManager.AppSettings["RootDir"].ToString() + @"\Resources\UpdateData\FullUpdateSlides.zip");
    //    }
    //    return buffer;
    //} 
    #endregion
}

