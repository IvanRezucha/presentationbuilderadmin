﻿using System;
using System.Collections;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;

/// <summary>
/// Summary description for UserRegistrationService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class UserRegistrationService : System.Web.Services.WebService
{

	public UserRegistrationService()
	{

		//Uncomment the following line if using designed components 
		//InitializeComponent(); 
	}

	[WebMethod]
	public UserRegistrationDS.UserProfileDataTable GetUserDataTableByEmail(string Email)
	{
		UserRegistrationDSTableAdapters.UserProfileTableAdapter taUserProfile = new UserRegistrationDSTableAdapters.UserProfileTableAdapter();
		UserRegistrationDS.UserProfileDataTable dtUserProfile = new UserRegistrationDS.UserProfileDataTable();
		
		taUserProfile.FillByEmail(dtUserProfile, Email);
		return dtUserProfile;
	}

	/// <summary>
	/// Takes a UserProfileDataTable and sends it back to the DB
	/// </summary>
	[WebMethod]
	public void SubmitNewUserData(UserRegistrationDS.UserProfileDataTable changedDataTable)
	{
		UserRegistrationDSTableAdapters.UserProfileTableAdapter taUserProfile = new UserRegistrationDSTableAdapters.UserProfileTableAdapter();
		taUserProfile.Update(changedDataTable);
	}

	/// <summary>
	/// Records a user's login date and time to the database in UTC format.
	/// </summary>
	/// <param name="Email">Email of the user to log in</param>
	/// <returns>False if user does not exist in the db, else true.</returns>
	[WebMethod]
	public bool RecordUserLoginDateTime(string Email)
	{
		bool result = true;

		UserRegistrationDSTableAdapters.UserProfileTableAdapter taUserProfile = new UserRegistrationDSTableAdapters.UserProfileTableAdapter();
		UserRegistrationDS.UserProfileDataTable dtUserProfile = new UserRegistrationDS.UserProfileDataTable();
		taUserProfile.FillByEmail(dtUserProfile, Email);
		if (dtUserProfile.Rows.Count < 1)
		{
			result = false;
		}
		else if (dtUserProfile.Rows.Count > 1)
		{
			throw new Exception(String.Format("Database contains more than one record for {0}. Check DB consistency", Email));
		}
		else if (dtUserProfile.Rows.Count == 1)
		{
			dtUserProfile[0].LastLoginDate = DateTime.UtcNow;
			taUserProfile.Update(dtUserProfile);
		}

		return result;
	}

	/// <summary>
	/// Records the amount of time the last install took for a user.
	/// </summary>
	/// <param name="Email">Email identifying user</param>
	/// <param name="installDurationInseconds">Length of time in seconds the install took</param>
	/// <returns>False if user does not exist, else true</returns>
	[WebMethod]
	public bool RecordUserContentInstallDuration(string Email, int installDurationInseconds)
	{
		bool result = true;

		UserRegistrationDSTableAdapters.UserProfileTableAdapter taUserProfile = new UserRegistrationDSTableAdapters.UserProfileTableAdapter();
		UserRegistrationDS.UserProfileDataTable dtUserProfile = new UserRegistrationDS.UserProfileDataTable();
		taUserProfile.FillByEmail(dtUserProfile, Email);
		if (dtUserProfile.Rows.Count < 1)
		{
			result = false;
		}
		else if (dtUserProfile.Rows.Count > 1)
		{
			throw new Exception(String.Format("Database contains more than one record for {0}. Check DB consistency", Email));
		}
		else if (dtUserProfile.Rows.Count == 1)
		{
			dtUserProfile[0].ContentInstallDuration = installDurationInseconds;
			taUserProfile.Update(dtUserProfile);
		}

		return result;
	}

	/// <summary>
	/// Records the amount of time the last content update took for a user.
	/// </summary>
	/// <param name="Email">Email identifying user</param>
	/// <param name="installDurationInseconds">Length of time in seconds the update took</param>
	/// <returns>False if user does not exist, else true</returns>
	[WebMethod]
	public bool RecordUserContentUpdateDuration(string Email, int updateDurationInseconds)
	{
		bool result = true;

		UserRegistrationDSTableAdapters.UserProfileTableAdapter taUserProfile = new UserRegistrationDSTableAdapters.UserProfileTableAdapter();
		UserRegistrationDS.UserProfileDataTable dtUserProfile = new UserRegistrationDS.UserProfileDataTable();
		taUserProfile.FillByEmail(dtUserProfile, Email);
		if (dtUserProfile.Rows.Count < 1)
		{
			result = false;
		}
		else if (dtUserProfile.Rows.Count > 1)
		{
			throw new Exception(String.Format("Database contains more than one record for {0}. Check DB consistency", Email));
		}
		else if (dtUserProfile.Rows.Count == 1)
		{
			dtUserProfile[0].LastContentUpdateDuration = updateDurationInseconds;
			taUserProfile.Update(dtUserProfile);
		}

		return result;
	}

    /// <summary>
    /// Submits a user's selected region languages to the db for tracking
    /// </summary>
    /// <param name="Email">Email identifier of the user</param>
    /// <param name="RegionLanguageList">Integer array containing user's Region Language IDs</param>
    [WebMethod]
    public void SubmitUserRegionLanguagesForUser(string Email, int[] RegionLanguageList)
    {
        int UserID = -1;

        UserRegistrationDS.UserProfileDataTable dtUserProfile = new UserRegistrationDS.UserProfileDataTable();
        UserRegistrationDSTableAdapters.UserProfileTableAdapter taUserProfile = new UserRegistrationDSTableAdapters.UserProfileTableAdapter();

        UserRegistrationDS.UserRegionLanguageDataTable dtUserRegionLanguage = new UserRegistrationDS.UserRegionLanguageDataTable();
        UserRegistrationDSTableAdapters.UserRegionLanguageTableAdapter taUserRegionLanguage = new UserRegistrationDSTableAdapters.UserRegionLanguageTableAdapter();

        //get the user's user id from their email
        taUserProfile.FillByEmail(dtUserProfile, Email);
        if (dtUserProfile.Rows.Count < 1)
        {
            throw new Exception(String.Format("{0} user does not exist", Email));
        }
        else if (dtUserProfile.Rows.Count > 1)
        {
            throw new Exception(String.Format("Database contains more than one record for {0}. Check DB consistency", Email));
        }
        else if (dtUserProfile.Rows.Count == 1)
        {
            UserID = dtUserProfile[0].UserID;
        }

        //delete all the user's current region languages
        taUserRegionLanguage.FillByUserID(dtUserRegionLanguage, UserID);
        foreach (UserRegistrationDS.UserRegionLanguageRow row in dtUserRegionLanguage.Rows)
        {
            row.Delete();
        }
        UserRegistrationDS.UserRegionLanguageRow rowUserRegionLanguage = dtUserRegionLanguage.NewUserRegionLanguageRow();
        //add the user's current region languages
        foreach (int regionLanguage in RegionLanguageList)
        {
            rowUserRegionLanguage = dtUserRegionLanguage.NewUserRegionLanguageRow();
            rowUserRegionLanguage.UserID = UserID;
            rowUserRegionLanguage.RegionLanguageID = regionLanguage;
            dtUserRegionLanguage.AddUserRegionLanguageRow(rowUserRegionLanguage);
        }
        //commit the changes to the db
        if(null != ((UserRegistrationDS.UserRegionLanguageDataTable)dtUserRegionLanguage.GetChanges(System.Data.DataRowState.Deleted)))
            taUserRegionLanguage.Update(((UserRegistrationDS.UserRegionLanguageDataTable)dtUserRegionLanguage.GetChanges(System.Data.DataRowState.Deleted)));
        if (null != ((UserRegistrationDS.UserRegionLanguageDataTable)dtUserRegionLanguage.GetChanges(System.Data.DataRowState.Added)))
            taUserRegionLanguage.Update(((UserRegistrationDS.UserRegionLanguageDataTable)dtUserRegionLanguage.GetChanges(System.Data.DataRowState.Added)));
    }
}

