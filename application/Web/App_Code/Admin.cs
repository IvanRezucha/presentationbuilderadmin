using System;
using System.Collections;
using System.ComponentModel;
using System.CodeDom.Compiler;
using System.Data;
using System.Reflection;
using System.Diagnostics;
using System.Collections.Generic;

using System.IO;
using System.IO.Compression;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.Configuration;

using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

/// <summary>
/// Summary description for Admin
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class Admin : System.Web.Services.WebService
{
    TextWriterTraceListener diags;

    public Admin()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 

        diags = new TextWriterTraceListener(WebConfigurationManager.AppSettings["RootDir"].ToString()  + @"\Resources\UpdateData\UpdateDiags.ws");
    }

    [WebMethod]
    public SlideLibrary GetSlideLibraryDataByRegionLanguage(string regLangIds)
    {
        SlideLibrary dsSlideLibrary = new SlideLibrary();

        try
        {
            SlideLibraryTableAdapters.CategoryTypeTableAdapter daCategoryType = new SlideLibraryTableAdapters.CategoryTypeTableAdapter();
            daCategoryType.Fill(dsSlideLibrary.CategoryType);

            SlideLibraryTableAdapters.CategoryTableAdapter daCategory = new SlideLibraryTableAdapters.CategoryTableAdapter();
            daCategory.Fill(dsSlideLibrary.Category);

            SlideLibraryTableAdapters.SlideTableAdapter daSlide = new SlideLibraryTableAdapters.SlideTableAdapter();
            daSlide.FillByRegionLanguageIds(dsSlideLibrary.Slide, regLangIds);


            dsSlideLibrary.Merge(RegionsAndLanguagesGet());

            SlideLibraryTableAdapters.SlideRegionLanguageTableAdapter taSlideRegionLanguage = new SlideLibraryTableAdapters.SlideRegionLanguageTableAdapter();
            taSlideRegionLanguage.FillByRegionLanguageIds(dsSlideLibrary.SlideRegionLanguage, regLangIds);

            SlideLibraryTableAdapters.SlideCategoryTableAdapter daSlideCategory = new SlideLibraryTableAdapters.SlideCategoryTableAdapter();
            daSlideCategory.FillByRegionLanguageIds(dsSlideLibrary.SlideCategory, regLangIds);

            dsSlideLibrary.Merge(FilterGet());

            SlideLibraryTableAdapters.SlideFilterTableAdapter daSlideFilter = new SlideLibraryTableAdapters.SlideFilterTableAdapter();
            daSlideFilter.FillByRegionLanguageIds(dsSlideLibrary.SlideFilter, regLangIds);
        }
        catch (Exception ex)
        {
            foreach (DataTable dt in dsSlideLibrary.Tables)
            {
                if (dt.HasErrors)
                {

                    foreach (DataRow dr in dt.GetErrors())
                    {
                        System.Diagnostics.Debug.WriteLine(dr.RowError);
                        foreach (DataColumn dc in dr.GetColumnsInError())
                        {
                            System.Diagnostics.Debug.WriteLine(String.Format("Table: {0}, Column: {1}", dt.TableName, dc.ColumnName));
                        }
                    }
                }
            }

            WriteToTrace(ex.Message);
            throw ex;
        }

        return dsSlideLibrary;
    }

    public SlideLibrary GetSlideLibraryData()
    {
        SlideLibrary dsSlideLibrary = new SlideLibrary();

        try
        {
            SlideLibraryTableAdapters.CategoryTypeTableAdapter daCategoryType = new SlideLibraryTableAdapters.CategoryTypeTableAdapter();
            daCategoryType.Fill(dsSlideLibrary.CategoryType);

            SlideLibraryTableAdapters.CategoryTableAdapter daCategory = new SlideLibraryTableAdapters.CategoryTableAdapter();
            daCategory.Fill(dsSlideLibrary.Category);

            SlideLibraryTableAdapters.SlideTableAdapter daSlide = new SlideLibraryTableAdapters.SlideTableAdapter();
            daSlide.Fill(dsSlideLibrary.Slide);

            dsSlideLibrary.Merge(RegionsAndLanguagesGet());

            SlideLibraryTableAdapters.SlideRegionLanguageTableAdapter taSlideRegionLanguage = new SlideLibraryTableAdapters.SlideRegionLanguageTableAdapter();
            taSlideRegionLanguage.Fill(dsSlideLibrary.SlideRegionLanguage);

            SlideLibraryTableAdapters.SlideCategoryTableAdapter daSlideCategory = new SlideLibraryTableAdapters.SlideCategoryTableAdapter();
            daSlideCategory.Fill(dsSlideLibrary.SlideCategory);

            dsSlideLibrary.Merge(FilterGet());

            SlideLibraryTableAdapters.SlideFilterTableAdapter daSlideFilter = new SlideLibraryTableAdapters.SlideFilterTableAdapter();
            daSlideFilter.Fill(dsSlideLibrary.SlideFilter);
        }
        catch (Exception ex)
        {
            WriteToTrace(ex.Message);
            throw ex;
        }

        return dsSlideLibrary;
    }
    #region Category Methods
    [WebMethod]
    public SlideLibrary GetCategory(int categoryID)
    {
        SlideLibrary dsSlideLibrary = new SlideLibrary();

        SlideLibraryTableAdapters.CategoryTypeTableAdapter daCategoryType = new SlideLibraryTableAdapters.CategoryTypeTableAdapter();
        daCategoryType.Fill(dsSlideLibrary.CategoryType);

        SlideLibraryTableAdapters.CategoryTableAdapter daCategory = new SlideLibraryTableAdapters.CategoryTableAdapter();
        daCategory.FillByCategoryID(dsSlideLibrary.Category, categoryID);

        return dsSlideLibrary;
    }

    [WebMethod]
    public SlideLibrary SaveCategory(SlideLibrary dsSlideLibrary, int categoryID)
    {
        try
        {
            SlideLibrary.CategoryRow newCategory = dsSlideLibrary.Category.FindByCategoryID(categoryID);
            SlideLibraryTableAdapters.CategoryTableAdapter daCategory = new SlideLibraryTableAdapters.CategoryTableAdapter();
            daCategory.Update(dsSlideLibrary.Category);
            dsSlideLibrary.AcceptChanges();

            categoryID = newCategory.CategoryID;

            return GetCategory(categoryID);
        }
        catch (Exception ex)
        {
            WriteToTrace(ex.Message);
            throw ex;
        }
        
    }

    /// <summary>
    /// Deletes a Category, its subcategories, and Any related ProductCategory records.
    /// </summary>
    /// <param name="AdminTicket">The user's System.Web.Security.FormsAuthenticationTicket string value.</param>
    /// <param name="CategoryID">The ID of the category you wish to delete</param>
    /// <returns>True for success, otherwise false</returns>
    [WebMethod(Description = "Deletes a Category, its subcategories, and Any related ProductCategory records.")]
    public bool CategoryDelete(int CategoryID)
    {
        System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection();
        try
        {
            //the Stored Procedure used to accomplish the delete also takes care of deleting the subcategories and the related ProductCategory records
            conn.ConnectionString = WebConfigurationManager.ConnectionStrings["MainConn"].ToString();
            conn.Open();

            System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "proc_CategoryDeleteByPrimaryKey";

            System.Data.SqlClient.SqlParameter param = new System.Data.SqlClient.SqlParameter("@CategoryID", CategoryID);

            cmd.Parameters.Add(param);

            cmd.ExecuteNonQuery();

            return true;
        }
        catch (Exception ex)
        {
            //HPSuppliesProducts.Components.Components.TraceManager.Trace("Error attempting to delete a category or related record (Services.Admin.CategoryDelete())", ex);
            return false;
        }
        finally
        {
            conn.Close();
        }
    }

    [WebMethod]
    public bool CategoryMoveUp(int categoryID)
    {
        SlideLibraryTableAdapters.CategoryTableAdapter daCategory = new SlideLibraryTableAdapters.CategoryTableAdapter();
        try
        {
            daCategory.ExchangePeersAscending(categoryID);
            return true;
        }
        catch (Exception ex)
        {
            return false;
        }
        
    }

    [WebMethod]
    public bool CategoryUpdateParent(int categoryID, int parentCategoryID)
    {
        SlideLibraryTableAdapters.CategoryTableAdapter daCategory = new SlideLibraryTableAdapters.CategoryTableAdapter();
        try
        {
            daCategory.UpdateParent(categoryID, parentCategoryID);
            return true;
        }
        catch (Exception ex)
        {
            return false;
        }

    }
    #endregion

    #region Slide Methods

    [WebMethod]
    public SlideLibrary SlideGet(int slideID)
    {
        SlideLibrary dsSlideLibrary = new SlideLibrary();

        dsSlideLibrary.Merge(RegionsAndLanguagesGet());

		// Fill filter groups
		SlideLibraryTableAdapters.FilterGroupTableAdapter daFilterGroup = new SlideLibraryTableAdapters.FilterGroupTableAdapter();
		daFilterGroup.Fill(dsSlideLibrary.FilterGroup);

		// Fill filters
		SlideLibraryTableAdapters.FilterTableAdapter daFilter = new SlideLibraryTableAdapters.FilterTableAdapter();
		daFilter.Fill(dsSlideLibrary.Filter);

        if(slideID != -1)
        {
            SlideLibraryTableAdapters.SlideTableAdapter daSlide = new SlideLibraryTableAdapters.SlideTableAdapter();
            daSlide.FillBySlideID(dsSlideLibrary.Slide, slideID);

            //SlideLibraryTableAdapters.SlideRegionTableAdapter daSlideRegion = new SlideLibraryTableAdapters.SlideRegionTableAdapter();
            //daSlideRegion.FillBySlideID(dsSlideLibrary.SlideRegion, slideID);

            SlideLibraryTableAdapters.SlideRegionLanguageTableAdapter taSlideRegionLanguage = new SlideLibraryTableAdapters.SlideRegionLanguageTableAdapter();
            taSlideRegionLanguage.FillBySlideID(dsSlideLibrary.SlideRegionLanguage, slideID);

			// Fill SlideFilter intersection table and filter it by the slide
			SlideLibraryTableAdapters.SlideFilterTableAdapter daSlidefilter = new SlideLibraryTableAdapters.SlideFilterTableAdapter();
			daSlidefilter.FillBySlideID(dsSlideLibrary.SlideFilter, slideID);
        }

        return dsSlideLibrary;
    }

	[WebMethod (Description	= "Returns a SlideLibrary Dataset with following tables full; Region, Language, Slide, and related relationship tables")]
	public SlideLibrary SlideGetAll()
	{
		SlideLibrary dsSlideLibrary = new SlideLibrary();

		dsSlideLibrary.Merge(RegionsAndLanguagesGet());
        dsSlideLibrary.Merge(FilterGet());

		SlideLibraryTableAdapters.SlideTableAdapter taSlide = new SlideLibraryTableAdapters.SlideTableAdapter();
		taSlide.Fill(dsSlideLibrary.Slide);

		SlideLibraryTableAdapters.SlideRegionLanguageTableAdapter taSlideRegionLanguage = new SlideLibraryTableAdapters.SlideRegionLanguageTableAdapter();
		taSlideRegionLanguage.Fill(dsSlideLibrary.SlideRegionLanguage);

        SlideLibraryTableAdapters.SlideFilterTableAdapter taSlideFilter = new SlideLibraryTableAdapters.SlideFilterTableAdapter();
        taSlideFilter.Fill(dsSlideLibrary.SlideFilter);

		return dsSlideLibrary;
	}

	[WebMethod(Description = "Returns a list of slides that match the comma delimited list of file names that are passed")]
	public SlideLibrary SlideGetByFileNames(string fileNames)
	{
		System.Text.StringBuilder slideIDs = new System.Text.StringBuilder();
		SlideLibrary dsSlideLibrary = new SlideLibrary();
		SlideLibraryTableAdapters.SlideTableAdapter taSlide = new SlideLibraryTableAdapters.SlideTableAdapter();
		SlideLibraryTableAdapters.SlideRegionLanguageTableAdapter taSlideRegionLanguage = new SlideLibraryTableAdapters.SlideRegionLanguageTableAdapter();
		SlideLibraryTableAdapters.SlideFilterTableAdapter taSlideFilter = new SlideLibraryTableAdapters.SlideFilterTableAdapter();

		dsSlideLibrary.Merge(RegionsAndLanguagesGet());
		dsSlideLibrary.Merge(FilterGet());
		taSlide.FillByFileNames(dsSlideLibrary.Slide, fileNames);
		
		// Get a list of slide IDs that will be used to get the specific slide region/language and filter records
		foreach (DataRow slideRow in dsSlideLibrary.Slide.Rows)
		{
			slideIDs.Append(((SlideLibrary.SlideRow)slideRow).SlideID.ToString() + ',');
		}

		if (slideIDs.Length > 0)
		{
			taSlideRegionLanguage.FillBySlideIDs(dsSlideLibrary.SlideRegionLanguage, slideIDs.ToString(0, slideIDs.Length - 1));
			taSlideFilter.FillBySlideIDs(dsSlideLibrary.SlideFilter, slideIDs.ToString(0, slideIDs.Length - 1));
		}

		return dsSlideLibrary;
	}

    //[WebMethod]
    //public SlideLibrary SlideSave(int slideID, SlideLibrary dsSlideLibrary, byte[] slideFileBytes, byte[] jpgFileBytes, string oldFileName)
    //{
    //    SlideLibrary.SlideRow slide = dsSlideLibrary.Slide.FindBySlideID(slideID);

    //    try
    //    {
    //        SlideLibraryTableAdapters.SlideTableAdapter daSlide = new SlideLibraryTableAdapters.SlideTableAdapter();
    //        daSlide.Update(dsSlideLibrary.Slide);

    //        SlideLibraryTableAdapters.SlideRegionTableAdapter daSlideRegion = new SlideLibraryTableAdapters.SlideRegionTableAdapter();
    //        daSlideRegion.DeleteBySlideID(slideID);
    //        daSlideRegion.Update(dsSlideLibrary.SlideRegion);

    //        // Update the filters
    //        SlideLibraryTableAdapters.SlideFilterTableAdapter daSlideFilter = new SlideLibraryTableAdapters.SlideFilterTableAdapter();
    //        daSlideFilter.DeleteBySlideID(slideID);
    //        daSlideFilter.Update(dsSlideLibrary.SlideFilter);

    //        slideID = slide.SlideID;
    //    }
    //    catch (Exception ex)
    //    {
    //        WriteToTrace(ex.Message);
    //        throw ex;
    //    }
        

    //    if (slideFileBytes != null)
    //    {
    //        string writeToDir = WebConfigurationManager.AppSettings["RootDir"].ToString() + @"\" +
    //                            WebConfigurationManager.AppSettings["PowerPointDir"].ToString();
            

    //        //write .pot file
    //        if (!File.Exists(WebConfigurationManager.AppSettings["RootDir"].ToString() + @"\Temp\TempPot.zip"))
    //        {
    //            File.WriteAllText(WebConfigurationManager.AppSettings["RootDir"].ToString() + @"\Temp\TempPot.zip",
    //                              "placeholder file");
    //        }
    //        PlugIns.Helpers.ZipHelper.WriteZippedFileBytes(slideFileBytes, 
    //                                                        writeToDir, 
    //                                                        WebConfigurationManager.AppSettings["RootDir"].ToString() + @"\Temp\TempPot.zip");

    //        //write .jpg file
    //        if (!File.Exists(WebConfigurationManager.AppSettings["RootDir"].ToString() + @"\Temp\TempJpg.zip"))
    //        {
    //            File.WriteAllText(WebConfigurationManager.AppSettings["RootDir"].ToString() + @"\Temp\TempJpg.zip",
    //                              "placeholder file");
    //        }
    //        PlugIns.Helpers.ZipHelper.WriteZippedFileBytes(jpgFileBytes,
    //                                                        WebConfigurationManager.AppSettings["RootDir"].ToString() + @"\" +
    //                                                        WebConfigurationManager.AppSettings["SlideThumbDir"].ToString(),
    //                                                        WebConfigurationManager.AppSettings["RootDir"].ToString() + @"\Temp\TempJpg.zip");


    //        if ((oldFileName.ToLower() != slide.FileName.ToLower()) && (oldFileName != "" || oldFileName != null))
    //        {
    //            //delete the old .pot
    //            if (File.Exists(writeToDir + @"\" + oldFileName))
    //            {
    //                File.Delete(writeToDir + @"\" + oldFileName);
    //            }
    //            //delete the old .jpg
    //            if (File.Exists(WebConfigurationManager.AppSettings["RootDir"].ToString() + @"\" +
    //                            WebConfigurationManager.AppSettings["SlideThumbDir"].ToString() + @"\" +
    //                            oldFileName))
    //            {
    //                File.Delete(WebConfigurationManager.AppSettings["RootDir"].ToString() + @"\" +
    //                            WebConfigurationManager.AppSettings["SlideThumbDir"].ToString() + @"\" +
    //                            oldFileName.Replace(".pot", ".jpg"));
    //            }
    //        }
    //    }

    //    return SlideGet(slideID);
    //}

	[WebMethod (Description = "Updates changes to the Slide and SlideRegionLanguage tables and returns a fresh copy of the data")]
	public SlideLibrary SlideSaveChanges(SlideLibrary dsSlideLibrary)
	{
		SlideLibraryTableAdapters.SlideTableAdapter taSlide = new SlideLibraryTableAdapters.SlideTableAdapter();
		taSlide.Update(dsSlideLibrary.Slide);

		SlideLibraryTableAdapters.SlideRegionLanguageTableAdapter taSlideRegionLanguage = new SlideLibraryTableAdapters.SlideRegionLanguageTableAdapter();
		taSlideRegionLanguage.Update(dsSlideLibrary.SlideRegionLanguage);

        SlideLibraryTableAdapters.SlideFilterTableAdapter taSlideFilter = new SlideLibraryTableAdapters.SlideFilterTableAdapter();
        taSlideFilter.Update(dsSlideLibrary.SlideFilter);

		dsSlideLibrary.AcceptChanges();

		return dsSlideLibrary;
	}

    [WebMethod(Description = "Returns a SlideLibrary dataset with the following tables filled: Slide, SlideCategory, SlideRegionLanguage, SlideFilter")]
    public SlideLibrary SlidesGetByRegionLanguageID(int regionLanguageID)
    {
        SlideLibrary dsSlideLibrary = new SlideLibrary();
        dsSlideLibrary.EnforceConstraints = false;

        SlideLibraryTableAdapters.SlideTableAdapter taSlide = new SlideLibraryTableAdapters.SlideTableAdapter();
        taSlide.FillByRegionLanguageId(dsSlideLibrary.Slide, regionLanguageID);

        SlideLibraryTableAdapters.SlideRegionLanguageTableAdapter taSlideRegionLanguage = new SlideLibraryTableAdapters.SlideRegionLanguageTableAdapter();
        taSlideRegionLanguage.FillByRegionLanguageId(dsSlideLibrary.SlideRegionLanguage, regionLanguageID);

        // Fill SlideFilter intersection table and filter it by the slide
        SlideLibraryTableAdapters.SlideFilterTableAdapter taSlidefilter = new SlideLibraryTableAdapters.SlideFilterTableAdapter();
        taSlidefilter.FillByRegionLanguageID(dsSlideLibrary.SlideFilter, regionLanguageID);

        SlideLibraryTableAdapters.SlideCategoryTableAdapter taSlideCategory = new SlideLibraryTableAdapters.SlideCategoryTableAdapter();
        taSlideCategory.FillByRegionLanguageID(dsSlideLibrary.SlideCategory, regionLanguageID);

        return dsSlideLibrary;
    }

	[WebMethod(Description = "Returns a SlideLibrary dataset with the following tables filled: Slide, SlideCategory, SlideRegionLanguage, SlideFilter")]
	public SlideLibrary SlidesGetByRegionLanguageIDAndFilterIDs(int regionLanguageID, string filterIDs)
	{
		SlideLibrary dsSlideLibrary = new SlideLibrary();
		dsSlideLibrary.EnforceConstraints = false;

		SlideLibraryTableAdapters.SlideTableAdapter taSlide = new SlideLibraryTableAdapters.SlideTableAdapter();
		taSlide.FillByRegionLanguageIdAndFilterIds(dsSlideLibrary.Slide, regionLanguageID, filterIDs);

		SlideLibraryTableAdapters.SlideRegionLanguageTableAdapter taSlideRegionLanguage = new SlideLibraryTableAdapters.SlideRegionLanguageTableAdapter();
		taSlideRegionLanguage.FillByRegionLanguageIdAndFilterIds(dsSlideLibrary.SlideRegionLanguage, regionLanguageID, filterIDs);

		// Fill SlideFilter intersection table and filter it by the slide
		SlideLibraryTableAdapters.SlideFilterTableAdapter taSlidefilter = new SlideLibraryTableAdapters.SlideFilterTableAdapter();
		taSlidefilter.FillByRegionLanguageIdAndFilterIds(dsSlideLibrary.SlideFilter, regionLanguageID, filterIDs);

		SlideLibraryTableAdapters.SlideCategoryTableAdapter taSlideCategory = new SlideLibraryTableAdapters.SlideCategoryTableAdapter();
		taSlideCategory.FillByRegionLanguageIdAndFilterIds(dsSlideLibrary.SlideCategory, regionLanguageID, filterIDs);

		return dsSlideLibrary;
	}

	[WebMethod(Description = "Returns a SlideLibrary dataset based on filterIds with the following tables filled: Slide, SlideCategory, SlideRegionLanguage, SlideFilter")]
	public SlideLibrary SlidesGetByFilterIds(string filterIds)
	{
		SlideLibrary dsSlideLibrary = new SlideLibrary();
		dsSlideLibrary.EnforceConstraints = false;

		SlideLibraryTableAdapters.SlideTableAdapter taSlide = new SlideLibraryTableAdapters.SlideTableAdapter();
		taSlide.FillByFilterIds(dsSlideLibrary.Slide, filterIds);

		SlideLibraryTableAdapters.SlideRegionLanguageTableAdapter taSlideRegionLanguage = new SlideLibraryTableAdapters.SlideRegionLanguageTableAdapter();
		taSlideRegionLanguage.FillByFilterIds(dsSlideLibrary.SlideRegionLanguage, filterIds);

		SlideLibraryTableAdapters.SlideFilterTableAdapter taSlideFilter = new SlideLibraryTableAdapters.SlideFilterTableAdapter();
		taSlideFilter.FillByFilterIds(dsSlideLibrary.SlideFilter, filterIds);

		SlideLibraryTableAdapters.SlideCategoryTableAdapter taSlideCategory = new SlideLibraryTableAdapters.SlideCategoryTableAdapter();
		taSlideCategory.FillByFilterIds(dsSlideLibrary.SlideCategory, filterIds);

		return dsSlideLibrary;
	}

    [WebMethod]
    public bool SlideDelete(int slideID, string fileName)
    {
        try
        {
            SlideLibraryTableAdapters.SlideTableAdapter daSlide = new SlideLibraryTableAdapters.SlideTableAdapter();
            daSlide.Delete(slideID);

            string writeToDir = WebConfigurationManager.AppSettings["RootDir"].ToString() + @"\" + WebConfigurationManager.AppSettings["PowerPointDir"].ToString();
			string thumbnail = WebConfigurationManager.AppSettings["RootDir"].ToString() + @"\" + WebConfigurationManager.AppSettings["SlideThumbDir"].ToString();
			
			// Delete the pot file
			if (File.Exists(writeToDir + @"\" + fileName))
            {
				File.Delete(writeToDir + @"\" + fileName);
            }

			// Delete the thumbnail file
			if (File.Exists(thumbnail + @"\" + fileName.Replace(".pot", ".jpg")))
			{
				File.Delete(thumbnail + @"\" + fileName.Replace(".pot", ".jpg"));
			}
        }
        catch (Exception ex)
        { 
            //TODO: handle failure
            return false;
        }

        return true;
    }

	[WebMethod(Description = "Accepts a two dimensional array of slide IDs and File Names in order to delete several slides at a time")]
	public bool SlideMultipleDelete(object[][] slide)
	{
		for (int i = 0; i < slide.Length; i++)
		{
		    if (!SlideDelete((int)slide[i][0], slide[i][1].ToString()))
		    {
		        return false;
		    }
		}

		return true;
	}

    /// <summary>
    /// 
    /// </summary>
    /// <param name="SlideID"></param>
    /// <param name="DestinationCategoryID"></param>
    /// <param name="SourceCategoryID"></param>
    /// <param name="BeginningSortOrder"></param>
    /// <param name="RegionLanguageID"></param>
    /// <returns></returns>
	[WebMethod]
    public bool SlideCategorization(int SlideID, int DestinationCategoryID, int SourceCategoryID, int SortOrder, int RegionLanguageID)
    {		
			try
			{
				if (SourceCategoryID > -1)
				{
					//This is a MOVE or a delete, remove the product from the source category before continuing
                    //dbConnection.ProductCategoryPeer.DeleteByPrimaryKey(ProductID,SourceCategoryID);
				}

				if (DestinationCategoryID > -1)
				{
					//Add the product to the specified category
					//  uses stored procedure proc_ProductCategoryInsert
                    SlideLibraryTableAdapters.SlideCategoryTableAdapter daSlideCategory = new SlideLibraryTableAdapters.SlideCategoryTableAdapter();
                    daSlideCategory.Insert(SlideID, DestinationCategoryID, RegionLanguageID, SortOrder);
				}

				return true;
			}
			catch (Exception ex)
			{
				//TODO handle failure
				return false;
			}
			finally
			{
				
			}
        }

	[WebMethod(Description = "Accepts an array of slide IDs that allow for mutiple slides to be categorized at once")]
	public bool SlideMultipleCategorization(int[] slideIDs, int DestinationCategoryID, int SourceCategoryID, int BeginningSortOrder, int RegionLanguageID)
	{
        int currentSortOrder = BeginningSortOrder;
		foreach(int slideID in slideIDs)
		{
			SlideCategorization(slideID, DestinationCategoryID, SourceCategoryID, currentSortOrder, RegionLanguageID);
			currentSortOrder++;
		}

		return true;
	}

    [WebMethod]
    public bool IsSlideFilenamePrefixValid(string filenamePrefix)
    {
        SlideLibraryTableAdapters.SlideTableAdapter taSlide = new SlideLibraryTableAdapters.SlideTableAdapter();
        try
        {
            int numOfSlides = (int)taSlide.CountByFileNamePrefix(filenamePrefix);
            if (numOfSlides > 0)
            {
                return false;
            }
            else
            {
                return true;
            }
             
        }
        catch (Exception ex)
        {
            WriteToTrace(ex.ToString());
            throw ex;
        }
    }

    [WebMethod]
    public IsSlideFilenameUniqueResult IsSlideFilenameUnique(string filename)
    {
        SlideLibrary ds = new SlideLibrary();
        SlideLibraryTableAdapters.SlideTableAdapter taSlide = new SlideLibraryTableAdapters.SlideTableAdapter();
        taSlide.FillByFilename(ds.Slide, filename);

        if (ds.Slide.Rows.Count > 0)
        {
            return new IsSlideFilenameUniqueResult(false, ds.Slide[0].SlideID);
        }
        else
        {
            return new IsSlideFilenameUniqueResult(true, null);
        }
    }

    #endregion

    #region Slide Category Methods

    [WebMethod]
    public bool SlideCategoryUpdate(SlideLibrary dsSlideLibrary)
    {
        try
        {
            SlideLibraryTableAdapters.SlideCategoryTableAdapter daSlideCategory = new SlideLibraryTableAdapters.SlideCategoryTableAdapter();
            daSlideCategory.Update(dsSlideLibrary.SlideCategory);
            dsSlideLibrary.AcceptChanges();
            return true;
        }
        catch (Exception ex)
        {
            return false;
        }
    }

    #endregion

    #region Support File Methods

	[WebMethod]
	public bool SupportFileNameExists(string supportFileName)
	{
		SupportFileDS dsSupportFile = new SupportFileDS();
		SupportFileDSTableAdapters.SupportFileTableAdapter daSupportFile = new SupportFileDSTableAdapters.SupportFileTableAdapter();
		daSupportFile.Fill(dsSupportFile.SupportFile);
		foreach (SupportFileDS.SupportFileRow row in dsSupportFile.SupportFile.Rows)
		{
			if (row.FileName.Equals(supportFileName, StringComparison.CurrentCultureIgnoreCase))
				return true;
		}
		return false;
	}
    
    [WebMethod]
    public SupportFileDS SupportFileGet(int supportFileID)
    {
        SupportFileDS dsSupportFile = new SupportFileDS();
		SupportFileDSTableAdapters.SupportFileTableAdapter daSupportFile = new SupportFileDSTableAdapters.SupportFileTableAdapter();
		SupportFileDSTableAdapters.SupportFileRegionLanguageTableAdapter daSupportFileRegionLanguage = new SupportFileDSTableAdapters.SupportFileRegionLanguageTableAdapter();
        daSupportFile.FillBySupportFileID(dsSupportFile.SupportFile, supportFileID);
		daSupportFileRegionLanguage.FillBySupportFileID(dsSupportFile.SupportFileRegionLanguage,supportFileID);
        return dsSupportFile;
    }

    [WebMethod]
    public bool SupportFileDelete(int supportFileID, string fileName)
    {
        try
        {
            SupportFileDSTableAdapters.SupportFileTableAdapter daSupportFile = new SupportFileDSTableAdapters.SupportFileTableAdapter();
            daSupportFile.Delete(supportFileID);

            string writeToDir = WebConfigurationManager.AppSettings["RootDir"].ToString() + @"\" +
                                WebConfigurationManager.AppSettings["PowerPointDir"].ToString();
            if (File.Exists(Path.Combine(writeToDir, fileName)))
            {
                File.Delete(Path.Combine(writeToDir, fileName));
            }

            return true;
        }
        catch (Exception ex)
        {
            return false;
        }
    }

    [WebMethod]
    public SupportFileDS SupportFileSave(int supportFileID, SupportFileDS dsSupportFile)
    {
        SupportFileDS.SupportFileRow supportFile = dsSupportFile.SupportFile.FindBySupportFileID(supportFileID);
        SupportFileDSTableAdapters.SupportFileTableAdapter daSupportFile = new SupportFileDSTableAdapters.SupportFileTableAdapter();
		SupportFileDSTableAdapters.SupportFileRegionLanguageTableAdapter daSupportFileRegionLanguage = new SupportFileDSTableAdapters.SupportFileRegionLanguageTableAdapter();

		//first update deleted items in intersection table
		daSupportFileRegionLanguage.Update(dsSupportFile.SupportFileRegionLanguage.Select("","",DataViewRowState.Deleted));
		//then update other tables
		daSupportFile.Update(dsSupportFile.SupportFile);
		//then update the rest of the intersection table
		daSupportFileRegionLanguage.Update(dsSupportFile.SupportFileRegionLanguage);

        supportFileID = supportFile.SupportFileID;

        return SupportFileGet(supportFileID);
    }

    [WebMethod]
    public SupportFileDS SupportFileSaveMultiple(SupportFileDS dsSupportFiles)
    {
        SupportFileDSTableAdapters.SupportFileTableAdapter daSupportFile = new SupportFileDSTableAdapters.SupportFileTableAdapter();
        SupportFileDSTableAdapters.SupportFileRegionLanguageTableAdapter daSupportFileRegionLanguage = new SupportFileDSTableAdapters.SupportFileRegionLanguageTableAdapter();

        //first update deleted items in intersection table
        daSupportFileRegionLanguage.Update(dsSupportFiles.SupportFileRegionLanguage.Select("", "", DataViewRowState.Deleted));
        //then update other tables
        daSupportFile.Update(dsSupportFiles.SupportFile);
        //then update the rest of the intersection table
        daSupportFileRegionLanguage.Update(dsSupportFiles.SupportFileRegionLanguage);

        dsSupportFiles.AcceptChanges();
        
        return dsSupportFiles;
    }

    [WebMethod]
    public SupportFileDS SupportFileGetAll()
    {
        try
        {
            SupportFileDS dsSupportFile = new SupportFileDS();
			SupportFileDSTableAdapters.SupportFileRegionLanguageTableAdapter daSupportFileRegionLanguage = new SupportFileDSTableAdapters.SupportFileRegionLanguageTableAdapter();
            SupportFileDSTableAdapters.SupportFileTableAdapter daSupportFile = new SupportFileDSTableAdapters.SupportFileTableAdapter();
			SupportFileDSTableAdapters.RegionLanguageTableAdapter daRegionLanguage = new SupportFileDSTableAdapters.RegionLanguageTableAdapter();
			SupportFileDSTableAdapters.RegionTableAdapter daRegion = new SupportFileDSTableAdapters.RegionTableAdapter();
			SupportFileDSTableAdapters.LanguageTableAdapter daLanguage = new SupportFileDSTableAdapters.LanguageTableAdapter();

			daRegionLanguage.Fill(dsSupportFile.RegionLanguage);
            daSupportFile.Fill(dsSupportFile.SupportFile);
			daSupportFileRegionLanguage.Fill(dsSupportFile.SupportFileRegionLanguage);
			daRegion.Fill(dsSupportFile.Region);
			daLanguage.Fill(dsSupportFile.Language);
            return dsSupportFile;
        }
        catch (Exception ex)
        {
            WriteToTrace(ex.ToString());
            throw;
        }
    }
	
    [WebMethod(Description = "Returns a SupportFileDS dataset with all tables filled based on the regionLanguageID.")]
	public SupportFileDS SupportFilesGetByRegionLanguageID(int regionLanguageID)
	{
		SupportFileDS dsSupportFile = new SupportFileDS();
		dsSupportFile.EnforceConstraints = false;

		SupportFileDSTableAdapters.SupportFileTableAdapter taSupportFile = new SupportFileDSTableAdapters.SupportFileTableAdapter();
		taSupportFile.FillByRegionLanguageID(dsSupportFile.SupportFile, regionLanguageID);

		SupportFileDSTableAdapters.SupportFileRegionLanguageTableAdapter taSupportFileRegionLanguage = new SupportFileDSTableAdapters.SupportFileRegionLanguageTableAdapter();
		taSupportFileRegionLanguage.FillByRegionLanguageID(dsSupportFile.SupportFileRegionLanguage, regionLanguageID);

		SupportFileDSTableAdapters.SupportFileTableAdapter taSupportFileFilter = new SupportFileDSTableAdapters.SupportFileTableAdapter();
		taSupportFileFilter.FillByRegionLanguageID(dsSupportFile.SupportFile, regionLanguageID);

		return dsSupportFile;
	}
	
    [WebMethod]
	public SupportFileDS RegionsAndLanguagesGetForSupportFiles()
	{
		SupportFileDS dsSupportFile = new SupportFileDS();

		SupportFileDSTableAdapters.RegionTableAdapter taRegion = new SupportFileDSTableAdapters.RegionTableAdapter();
		taRegion.Fill(dsSupportFile.Region);

		SupportFileDSTableAdapters.LanguageTableAdapter taLanguage = new SupportFileDSTableAdapters.LanguageTableAdapter();
		taLanguage.Fill(dsSupportFile.Language);

		SupportFileDSTableAdapters.RegionLanguageTableAdapter taRegionLanguage = new SupportFileDSTableAdapters.RegionLanguageTableAdapter();
		taRegionLanguage.Fill(dsSupportFile.RegionLanguage);

		return dsSupportFile;
	}

    #endregion

	#region Filter Management Methods

	[WebMethod]
	public SlideLibrary FilterGet()
	{
		SlideLibrary slideLibraryDS = new SlideLibrary();

		try
		{
			// Fill the filter groups
			SlideLibraryTableAdapters.FilterGroupTableAdapter filterGroupTableAdapter = new SlideLibraryTableAdapters.FilterGroupTableAdapter();
			filterGroupTableAdapter.Fill(slideLibraryDS.FilterGroup);

			// Fill the filters
			SlideLibraryTableAdapters.FilterTableAdapter filterTableAdapter = new SlideLibraryTableAdapters.FilterTableAdapter();
			filterTableAdapter.Fill(slideLibraryDS.Filter);
		}
		catch (Exception ex)
		{
			WriteToTrace(ex.Message);
			throw ex;
		}

		return slideLibraryDS;
	}

	[WebMethod]
	public SlideLibrary FilterSave(SlideLibrary slideLibraryDS)
	{
		try
		{
			// Update filter groups
			SlideLibraryTableAdapters.FilterGroupTableAdapter filterGroupTableAdapter = new SlideLibraryTableAdapters.FilterGroupTableAdapter();
			filterGroupTableAdapter.Update(slideLibraryDS.FilterGroup);

            //// Update filters
            SlideLibraryTableAdapters.FilterTableAdapter filterTableAdapter = new SlideLibraryTableAdapters.FilterTableAdapter();
            filterTableAdapter.Update(slideLibraryDS.Filter);
		}
		catch (Exception ex)
		{
			WriteToTrace(ex.Message);
			throw ex;
		}
		
		return FilterGet();
	}

	#endregion 

    #region Regions/Languages

    [WebMethod]
    public SlideLibrary RegionsAndLanguagesGet()
    {
        SlideLibrary dsSlideLibrary = new SlideLibrary();

        SlideLibraryTableAdapters.RegionTableAdapter taRegion = new SlideLibraryTableAdapters.RegionTableAdapter();
        taRegion.Fill(dsSlideLibrary.Region);

        SlideLibraryTableAdapters.LanguageTableAdapter taLanguage = new SlideLibraryTableAdapters.LanguageTableAdapter();
        taLanguage.Fill(dsSlideLibrary.Language);

        SlideLibraryTableAdapters.RegionLanguageTableAdapter taRegionLanguage = new SlideLibraryTableAdapters.RegionLanguageTableAdapter();
        taRegionLanguage.Fill(dsSlideLibrary.RegionLanguage);

        return dsSlideLibrary;
    }

	[WebMethod]
	public void DeleteSlideRegionLanguagesBySlideIDAndRegionLanguageID(int slideID, int regionLanguageID)
	{
		SlideLibraryTableAdapters.SlideRegionLanguageTableAdapter taSlideRegionLanguage = new SlideLibraryTableAdapters.SlideRegionLanguageTableAdapter();

		taSlideRegionLanguage.Delete(slideID, regionLanguageID);
	}

    #endregion	

    #region Announcements
    [WebMethod] 
    public AnnouncementDS AnnouncementsGet()
    {
        try
        {
            AnnouncementDS dsAnnouncement = new AnnouncementDS();
            AnnouncementDSTableAdapters.AnnouncementTableAdapter taAnnouncement = new AnnouncementDSTableAdapters.AnnouncementTableAdapter();
            taAnnouncement.Fill(dsAnnouncement.Announcement);
            return dsAnnouncement;
        }
        catch (Exception ex)
        {
            WriteToTrace(ex.Message);
            throw ex;
        }
        
    }

    [WebMethod]
    public AnnouncementDS AnnouncementsSave(AnnouncementDS dsAnnouncement)
    {
        try
        {
            AnnouncementDSTableAdapters.AnnouncementTableAdapter taAnnouncement = new AnnouncementDSTableAdapters.AnnouncementTableAdapter();
            taAnnouncement.Update(dsAnnouncement);
            return AnnouncementsGet();
        }
        catch (Exception ex)
        {
            WriteToTrace(ex.Message);
            throw ex;
        }

    }
    
    #endregion

    [WebMethod]
    public ClientDataUpdateDS ClientDataUpdateInfoGet()
    {
        try
        {
            ClientDataUpdateDSTableAdapters.ClientDataUpdateTableAdapter taClientData = new ClientDataUpdateDSTableAdapters.ClientDataUpdateTableAdapter();
            ClientDataUpdateDS dsClientDataUpdate = new ClientDataUpdateDS();
            taClientData.Fill(dsClientDataUpdate.ClientDataUpdate);
            return dsClientDataUpdate;
        }
        catch (Exception ex)
        {
            WriteToTrace(ex.Message);
            throw ex;
        }
        
        
    }

    #region content updates

    [WebMethod]
    public bool CreateDataUpdate()
    {
        //OK here's what we're gonna do
        //1.Write the slide libray data 
        //2.Write the support file data
        //3.Add a new record to the DataUpdate table to indicate that we've just made a content update
        //4.Write the DataUpdate DataSet to so the client apps can check for content updates
        //5.(optional) send the xml we just created to NetStorage


        //start creating the current update
        try
        {
            //get a dataset filled with data for the update we a currently building
            SlideLibrary dsNewSlideLibrary;
            dsNewSlideLibrary = this.GetSlideLibraryData();
            dsNewSlideLibrary.WriteXml(WebConfigurationManager.AppSettings["RootDir"].ToString() +
                                     @"\Resources\UpdateData\SlideLibraryData.xml");

            CreateSupportFileContentUpdateFiles();
            
        }
        catch (Exception ex)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(ex.Message);
            sb.Append(System.Environment.NewLine);
            sb.Append(ex.Source);
            sb.Append(System.Environment.NewLine);
            sb.Append(ex.StackTrace);
            sb.Append(System.Environment.NewLine);
            sb.Append(ex.InnerException);
            sb.Append(System.Environment.NewLine);
            WriteToTrace(sb.ToString());
            return false;
        }        
        
        //we need to need to record this content update
        //if NetStorage is used ad some time to allow for the TTL
        UpdateDSTableAdapters.DataUpdateTableAdapter taDataUpdate = new UpdateDSTableAdapters.DataUpdateTableAdapter();
        if (Convert.ToBoolean(WebConfigurationManager.AppSettings["IsNetStorageSupported"].ToString()))
        {
            taDataUpdate.Insert(DateTime.UtcNow.AddHours(36.0));
        }
        else
        {
            taDataUpdate.Insert(DateTime.UtcNow);
        }

        CreateContentUpdateXml();

        if (Convert.ToBoolean(WebConfigurationManager.AppSettings["IsNetStorageSupported"].ToString()))
        {
            FtpContentUpdateXmlFilesToNetStorage();
        }


        return true;

    }

    [WebMethod]
    public void CreateLocalizedContentUpdate()
    {
        //OK here's what we're gonna do
        //1.Write the localized slide libray data 
        //2.Write the support file data
        //3.Add a new record to the LocalizedContentUdpate table to indicate that we've just made a content update
        //4.Write the LocalizedContentUdpate DataSet to so the client apps can check for content updates
        //5.(optional) send the xml we just created to NetStorage

        try
        {
            CreateRegionLanguageSlideLibraryFiles();
            CreateSupportFileContentUpdateFiles();

            //we need to need to record this content update
            //if NetStorage is used ad some time to allow for the TTL
            LocalizedContentUdpateDSTableAdapters.LocalizedContentUpdateTableAdapter taContentUpdate = new LocalizedContentUdpateDSTableAdapters.LocalizedContentUpdateTableAdapter();
            if (Convert.ToBoolean(WebConfigurationManager.AppSettings["IsNetStorageSupported"].ToString()))
            {
                taContentUpdate.Insert(DateTime.UtcNow.AddHours(36.0));
            }
            else
            {
                taContentUpdate.Insert(DateTime.UtcNow);
            }
            
            CreateLocalizedContentUpdateXml();        
            
            if (Convert.ToBoolean(WebConfigurationManager.AppSettings["IsNetStorageSupported"].ToString()))
            {
                FtpContentUpdateXmlFilesToNetStorage();
            }
        }
        catch (Exception ex)
        {
            WriteToTrace(ex.ToString());
            throw ex;
        }


    }

    private void CreateContentUpdateXml()
    {
        UpdateDS dsContentUpdates = new UpdateDS();
        UpdateDSTableAdapters.DataUpdateTableAdapter taContentUpdates = new UpdateDSTableAdapters.DataUpdateTableAdapter();

        taContentUpdates.Fill(dsContentUpdates.DataUpdate);
        dsContentUpdates.WriteXml(WebConfigurationManager.AppSettings["RootDir"].ToString() + @"\Resources\UpdateData\ContentUpdates.xml");
    }

    private void CreateLocalizedContentUpdateXml()
    {
        LocalizedContentUdpateDS dsLocalizedContentUpdate = new LocalizedContentUdpateDS();
        LocalizedContentUdpateDSTableAdapters.LocalizedContentUpdateTableAdapter taLocContUpdate = new LocalizedContentUdpateDSTableAdapters.LocalizedContentUpdateTableAdapter();
        taLocContUpdate.Fill(dsLocalizedContentUpdate.LocalizedContentUpdate);
        dsLocalizedContentUpdate.WriteXml(WebConfigurationManager.AppSettings["RootDir"].ToString() + @"\Resources\UpdateData\LocalizedUpdateData\LocalizedContentUpdates.xml");
    }

    private void CreateRegionLanguageSlideLibraryFiles()
    {
        string localizedUpdatDataFolderToken = @"\Resources\UpdateData\LocalizedUpdateData\";
        try
        {
            //let's back-up the current update files before creating the new ones
            //System.Collections.ArrayList filesToZip = new System.Collections.ArrayList();

            //foreach (string fileToZip in Directory.GetFiles(WebConfigurationManager.AppSettings["RootDir"].ToString() + localizedUpdatDataFolderToken))
            //{
            //    filesToZip.Add(fileToZip);
            //}

            //string archiveFilePath = WebConfigurationManager.AppSettings["RootDir"].ToString() + String.Format(@"\Resources\UpdateData\LocalizedDataArchive-{0}.zip", DateTime.UtcNow.ToString().Replace("/", "-").Replace(":", "_"));
            //PlugIns.Helpers.ZipHelper.ZipFiles(filesToZip, archiveFilePath);

            //we're going to need these
            SlideLibraryTableAdapters.CategoryTableAdapter taCategory = new SlideLibraryTableAdapters.CategoryTableAdapter();
            SlideLibraryTableAdapters.FilterGroupTableAdapter taFilterGroup = new SlideLibraryTableAdapters.FilterGroupTableAdapter();
            SlideLibraryTableAdapters.FilterTableAdapter taFilter = new SlideLibraryTableAdapters.FilterTableAdapter();
            SlideLibraryTableAdapters.LanguageTableAdapter taLanguage = new SlideLibraryTableAdapters.LanguageTableAdapter();
            SlideLibraryTableAdapters.RegionLanguageTableAdapter taRegionLanguage = new SlideLibraryTableAdapters.RegionLanguageTableAdapter();
            SlideLibraryTableAdapters.RegionTableAdapter taRegion = new SlideLibraryTableAdapters.RegionTableAdapter();
            SlideLibraryTableAdapters.SlideCategoryTableAdapter taSlideCategory = new SlideLibraryTableAdapters.SlideCategoryTableAdapter();
            SlideLibraryTableAdapters.SlideFilterTableAdapter taSlideFilter = new SlideLibraryTableAdapters.SlideFilterTableAdapter();
            SlideLibraryTableAdapters.SlideRegionLanguageTableAdapter taSlideRegionLanguage = new SlideLibraryTableAdapters.SlideRegionLanguageTableAdapter();
            SlideLibraryTableAdapters.SlideTableAdapter taSlide = new SlideLibraryTableAdapters.SlideTableAdapter();

            //fill the tables that are not specific to a particular RegionLanguage
            SlideLibrary dsSlideLibrary = new SlideLibrary();
            dsSlideLibrary.EnforceConstraints = false;

            taRegion.Fill(dsSlideLibrary.Region);
            taLanguage.Fill(dsSlideLibrary.Language);
            taRegionLanguage.Fill(dsSlideLibrary.RegionLanguage);

            //write the Region/Language data to disk so the client app can download them
            dsSlideLibrary.WriteXml(Path.Combine(WebConfigurationManager.AppSettings["RootDir"].ToString() + localizedUpdatDataFolderToken, "RegionLanguages.xml"));

            taCategory.Fill(dsSlideLibrary.Category);
            taFilterGroup.Fill(dsSlideLibrary.FilterGroup);
            taFilter.Fill(dsSlideLibrary.Filter);
            //write this data to disk
            //everyone will download this when updating their content, then they will download additional files base on a their selected RegionLanguages
            dsSlideLibrary.WriteXml(Path.Combine(WebConfigurationManager.AppSettings["RootDir"].ToString() + localizedUpdatDataFolderToken, "Main.xml"));

            WriteToTrace(String.Format("Tried to write: {0}", Path.Combine(WebConfigurationManager.AppSettings["RootDir"].ToString() + localizedUpdatDataFolderToken, "Main.xml")));

            List<SlideLibrary> datasets = new List<SlideLibrary>();
            foreach (SlideLibrary.RegionLanguageRow regLangRow in dsSlideLibrary.RegionLanguage.Rows)
            {
                SlideLibrary dsLocalizedSL = new SlideLibrary();
                //we'll turn the contraints off because we're only filling some of the tables
                //this will save bandwidth on the client download
                dsLocalizedSL.EnforceConstraints = false;

                taRegionLanguage.Fill(dsLocalizedSL.RegionLanguage);
                taSlide.FillByRegionLangugageIDAndExistingSlideCategory(dsLocalizedSL.Slide, regLangRow.RegionLanguageID);
                taSlideRegionLanguage.FillByRegionLanguageIDAndHasExistingSlideCategory(dsLocalizedSL.SlideRegionLanguage, regLangRow.RegionLanguageID);
                taSlideCategory.FillByRegionLanguageID(dsLocalizedSL.SlideCategory, regLangRow.RegionLanguageID);
                taSlideFilter.FillByRegionLanguageIdAndHasExistingSlideCategory(dsLocalizedSL.SlideFilter, regLangRow.RegionLanguageID);
                //string fileName = String.Format("{0}-{1}{2}Data.xml", regLangRow.RegionLanguageID.ToString(), dsLocalizedSL.Region.FindByRegionID(regLangRow.RegionID).RegionName, dsLocalizedSL.Language.FindByLanguageID(regLangRow.LanguageID).LongName);
                string fileName = String.Format("{0}-SlideData.xml", regLangRow.RegionLanguageID.ToString());

                dsLocalizedSL.WriteXml(Path.Combine(WebConfigurationManager.AppSettings["RootDir"].ToString() + localizedUpdatDataFolderToken, fileName));
            }
        }
        catch (Exception ex)
        {
            //should probably do some diagnostics here
            throw ex;
        }
    }

    private void CreateSupportFileContentUpdateFiles()
    {
	    //update support file info
        SupportFileDS dsSupportFiles = new SupportFileDS();
        SupportFileDSTableAdapters.SupportFileTableAdapter taSupportFiles = new SupportFileDSTableAdapters.SupportFileTableAdapter();
		SupportFileDSTableAdapters.SupportFileRegionLanguageTableAdapter taSupportFileRegionLanguage = new SupportFileDSTableAdapters.SupportFileRegionLanguageTableAdapter();
		SupportFileDSTableAdapters.RegionLanguageTableAdapter taRegionLanguage = new SupportFileDSTableAdapters.RegionLanguageTableAdapter();

		taRegionLanguage.Fill(dsSupportFiles.RegionLanguage);
		foreach (SupportFileDS.RegionLanguageRow regionLanguage in dsSupportFiles.RegionLanguage.Rows)
		{
			taSupportFileRegionLanguage.FillByRegionLanguageID(dsSupportFiles.SupportFileRegionLanguage, regionLanguage.RegionLanguageID);
			taSupportFiles.FillByRegionLanguageID(dsSupportFiles.SupportFile, regionLanguage.RegionLanguageID);

			dsSupportFiles.WriteXml(String.Format(WebConfigurationManager.AppSettings["RootDir"].ToString() +
				@"\Resources\UpdateData\LocalizedUpdateData\{0}-SupportFileData.xml", regionLanguage.RegionLanguageID.ToString()));
		}
    }

    private void FtpContentUpdateXmlFilesToNetStorage()
    {
        Utilities.FTP.FTPclient ftp = new Utilities.FTP.FTPclient(WebConfigurationManager.AppSettings["NetStorageFtp"].ToString(),
            WebConfigurationManager.AppSettings["NetStorageUserName"].ToString(),
            WebConfigurationManager.AppSettings["NetStoragePassword"].ToString());


        string supportFileFtp = "/31505/UpdateData/SupportFileData.xml";
        string supportFileLocal = Path.Combine(WebConfigurationManager.AppSettings["RootDir"].ToString(), "Resources\\UpdateData\\SupportFileData.xml");

        ftp.Upload(supportFileLocal, supportFileFtp);

        if (Convert.ToBoolean(WebConfigurationManager.AppSettings["IsContentLocalizationSupported"].ToString()))
        {
            ftp.Upload(WebConfigurationManager.AppSettings["RootDir"].ToString() + @"\Resources\UpdateData\LocalizedUpdateData\Main.xml",
                String.Format("/31505/UpdateData/LocalizedUpdateData/{0}", Path.GetFileName(WebConfigurationManager.AppSettings["RootDir"].ToString() + @"\Resources\UpdateData\LocalizedUpdateData\Main.xml")));

            ftp.Upload(WebConfigurationManager.AppSettings["RootDir"].ToString() + @"\Resources\UpdateData\LocalizedUpdateData\RegionLanguages.xml",
                String.Format("/31505/UpdateData/LocalizedUpdateData/{0}", Path.GetFileName(WebConfigurationManager.AppSettings["RootDir"].ToString() + @"\Resources\UpdateData\LocalizedUpdateData\RegionLanguages.xml")));

            SlideLibrary dsRegLangs = new SlideLibrary();
            dsRegLangs.ReadXml(WebConfigurationManager.AppSettings["RootDir"].ToString() + @"\Resources\UpdateData\LocalizedUpdateData\RegionLanguages.xml");

            string localizedUpdatDataFolderToken = @"\Resources\UpdateData\LocalizedUpdateData\";


            foreach (SlideLibrary.RegionLanguageRow regLang in dsRegLangs.RegionLanguage.Rows)
            {
                //if (!regLang.RegionRow.IsClientDefault)
                //{
                    string fileName = String.Format("{0}-SlideData.xml", regLang.RegionLanguageID.ToString());
                    ftp.Upload(Path.Combine(WebConfigurationManager.AppSettings["RootDir"].ToString() + localizedUpdatDataFolderToken, fileName),
                        String.Format("/31505/UpdateData/LocalizedUpdateData/{0}", Path.GetFileName(Path.Combine(WebConfigurationManager.AppSettings["RootDir"].ToString() + localizedUpdatDataFolderToken, fileName))));
                //}

					fileName = String.Format("{0}-SupportFileData.xml", regLang.RegionLanguageID.ToString());
					ftp.Upload(Path.Combine(WebConfigurationManager.AppSettings["RootDir"].ToString() + localizedUpdatDataFolderToken, fileName),
						String.Format("/31505/UpdateData/LocalizedUpdateData/{0}", Path.GetFileName(Path.Combine(WebConfigurationManager.AppSettings["RootDir"].ToString() + localizedUpdatDataFolderToken, fileName))));
            }
            
            ftp.Upload(WebConfigurationManager.AppSettings["RootDir"].ToString() + @"\Resources\UpdateData\LocalizedUpdateData\LocalizedContentUpdates.xml",
                String.Format("/31505/UpdateData/LocalizedUpdateData/{0}", Path.GetFileName(WebConfigurationManager.AppSettings["RootDir"].ToString() + @"\Resources\UpdateData\LocalizedUpdateData\LocalizedContentUpdates.xml")));


        }
        else
        {
            ftp.Upload(WebConfigurationManager.AppSettings["RootDir"].ToString() + @"\Resources\UpdateData\SlideLibraryData.xml",
                String.Format("/31505/UpdateData/{0}", Path.GetFileName(WebConfigurationManager.AppSettings["RootDir"].ToString() + @"\Resources\UpdateData\SlideLibraryData.xml")));
        
            ftp.Upload(WebConfigurationManager.AppSettings["RootDir"].ToString() + @"\Resources\UpdateData\ContentUpdates.xml",
                String.Format("/31505/UpdateData/{0}", Path.GetFileName(WebConfigurationManager.AppSettings["RootDir"].ToString() + @"\Resources\UpdateData\ContentUpdates.xml")));
        }       




    } 

    #endregion

    [WebMethod]
    public string Shizmo()
    {
        return "Shizmo";
    }

    private void WriteToTrace(string msg)
    {

        DateTime now = DateTime.Now;

        diags.WriteLine(now.ToShortDateString() + " " + now.ToShortTimeString() + " " + msg);

        diags.Flush();

    }

    #region old stuff

    [WebMethod]
    public void CreateLocalizedWebContentUpdate()
    {
        string localizedUpdatDataFolderToken = @"\Resources\WebUpdateData\LocalizedUpdateData\";

        SlideLibrary dsRegLang = new SlideLibrary();
        SlideLibraryTableAdapters.RegionLanguageTableAdapter taRegLang = new SlideLibraryTableAdapters.RegionLanguageTableAdapter();
        SlideLibraryTableAdapters.RegionTableAdapter taReg = new SlideLibraryTableAdapters.RegionTableAdapter();
        SlideLibraryTableAdapters.LanguageTableAdapter taLang = new SlideLibraryTableAdapters.LanguageTableAdapter();
        taReg.Fill(dsRegLang.Region);
        taLang.Fill(dsRegLang.Language);
        //taRegLang.FillByRegionIsNotClientDefault(dsRegLang.RegionLanguage);
        taRegLang.Fill(dsRegLang.RegionLanguage);
        string regLangXmlFile = WebConfigurationManager.AppSettings["RootDir"].ToString() + localizedUpdatDataFolderToken + "RegionLanguages.xml";
        dsRegLang.WriteXml(regLangXmlFile);
        foreach (SlideLibrary.RegionLanguageRow regLangRow in dsRegLang.RegionLanguage.Rows)
        {
            WebTreeData dsWebTreeData = new WebTreeData();

            WebTreeDataTableAdapters.CatTableAdapter taCat = new WebTreeDataTableAdapters.CatTableAdapter();
            taCat.Fill(dsWebTreeData.Cat);

            WebTreeDataTableAdapters.SlideCatInfoTableAdapter taSlideCat = new WebTreeDataTableAdapters.SlideCatInfoTableAdapter();
            taSlideCat.FillByRegionLanguageID(dsWebTreeData.SlideCatInfo, regLangRow.RegionLanguageID);

            string webDataFile = WebConfigurationManager.AppSettings["RootDir"].ToString() + String.Format("{0}ds{1}-LocalizedTreeData.xml", localizedUpdatDataFolderToken, regLangRow.RegionLanguageID.ToString());
            dsWebTreeData.WriteXml(webDataFile);

            foreach (DataColumn col in dsWebTreeData.SlideCatInfo.Columns)
            {
                col.ColumnMapping = MappingType.Attribute;
            }

            foreach (DataColumn col in dsWebTreeData.Cat.Columns)
            {
                col.ColumnMapping = MappingType.Attribute;
            }

            string treeDataFile = WebConfigurationManager.AppSettings["RootDir"].ToString() + String.Format("{0}{1}-LocalizedTreeData.xml", localizedUpdatDataFolderToken, regLangRow.RegionLanguageID.ToString());
            dsWebTreeData.WriteXml(treeDataFile);

            string treeDataFileText = File.ReadAllText(treeDataFile);

            //remove this text
            treeDataFileText = treeDataFileText.Replace(String.Format("<WebTreeData xmlns={0}http://tempuri.org/WebTreeData.xsd{0}>", '\"'), "");
            treeDataFileText = treeDataFileText.Replace("</WebTreeData>", "");
            //put in the whole url for the slide file
            //treeDataFileText = treeDataFileText.Replace(String.Format("FileName={0}", '\"'), String.Format("FileName={0}{1}", '\"', WebConfigurationManager.AppSettings["WebSlideFilePrefix"].ToString()));
            //treeDataFileText = treeDataFileText.Replace(String.Format("FileName={0}", '\"'), String.Format("FileName={0}{1}", '\"', "Slides/"));

            File.WriteAllText(treeDataFile, treeDataFileText);
        }
    }



    #endregion

    public struct IsSlideFilenameUniqueResult
    {
        public bool IsUnique;
        public int? SlideId;

        public IsSlideFilenameUniqueResult(bool isUnique, int? slideId)
        { 
            IsUnique = isUnique;
            SlideId = slideId;
        }
    }

 }