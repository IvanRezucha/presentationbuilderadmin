﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        UserWebService.UserRegistrationServiceSoapClient registration = new UserWebService.UserRegistrationServiceSoapClient();
        //veriflbl.Text = registration.ValidateUser("james.cooper", "Wirestone", "WebClient").ToString();
        UserWebService.UserRegistrationDS.UserProfileDataTable usertable = registration.GetUserByEmail("james.cooper@wirestone.com");
        veriflbl.Text = usertable[0].FirstName;
    }
}