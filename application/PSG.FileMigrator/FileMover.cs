﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using PSG.Core.Files;

namespace PSG.FileMigrator
{
    public delegate void ProcessingStartedEventHandler(object sender, ProcessingEventArgs e);
    public delegate void ProcessingCompletedEventHandler(object sender, ProcessingEventArgs e);
    public delegate void FileProcessedEventHandler(object sender, FileProcessedEventArgs e);
    public delegate void RetreivingDirectoryInformationStartedEventHandler(object sender, RetreivingDirectoryInformationStartedEventArgs e);
    public delegate void RetreivingDirectoryInformationCompletedEventHandler(object sender, RetreivingDirectoryInformationCompletedEventArgs e);

    public class ProcessingEventArgs : EventArgs
    {
		public int Count = 0;
    }
    public class RetreivingDirectoryInformationStartedEventArgs : EventArgs
    {
    }
    public class RetreivingDirectoryInformationCompletedEventArgs : EventArgs
    {
        private int fileCount = 0;

        public RetreivingDirectoryInformationCompletedEventArgs(int fileCount)
        {
            this.fileCount = fileCount;
        }

        public int FileCount
        {
            get
            {
                return fileCount;
            }
        }
    }
    public class FileProcessedEventArgs : EventArgs
    {
        private string oldFilePath;
        private string newFilePath;
        private int currentFileNumber;
        private int totalFileCount;
        private TimeSpan elapsedTime;

        public FileProcessedEventArgs(string OldFilePath, string NewFilePath, int TotalFileCount, int CurrentFileNumber, TimeSpan ElapsedTime)
        {
            this.oldFilePath = OldFilePath;
            this.newFilePath = NewFilePath;
            this.totalFileCount = TotalFileCount;
            this.currentFileNumber = CurrentFileNumber;
            this.elapsedTime = ElapsedTime;
        }

        public string OldFilePath
        {
            get
            {
                return oldFilePath;
            }
        }
        public string NewFilePath
        {
            get
            {
                return newFilePath;
            }
        }
        public int TotalFileCount
        {
            get
            {
                return totalFileCount;
            }
        }
        public int CurrentFileNumber
        {
            get
            {
                return currentFileNumber;
            }
        }
        public TimeSpan ElapsedTime
        {
            get
            {
                return elapsedTime;
            }
        }
    }


    public class FileMover
    {

        public event ProcessingStartedEventHandler ProcessingStarted;
        public event ProcessingCompletedEventHandler ProcessingCompleted;
        public event FileProcessedEventHandler FileProcessed;
        public event RetreivingDirectoryInformationStartedEventHandler RetreivingDirectoryInformationStarted;
        public event RetreivingDirectoryInformationCompletedEventHandler RetreivingDirectoryInformationCompleted;


        private string originDirectory = string.Empty;
        private string destinationDirectory = string.Empty;
        private int fileCount = 0;
        private int filesProcessed = 0;
        private DateTime startTime;
        private TimeSpan elapsedTime;
        private volatile bool shouldStop = false;

        public FileMover(string originDirectory, string destinationDirectory)
        {
            this.originDirectory = originDirectory;
            this.destinationDirectory = destinationDirectory;
        }

        public void MoveFiles()
        {
            DirectoryInfo startDirectoryInfo = new DirectoryInfo(this.originDirectory);

            OnRetrievingDirectoryInformationStarted(new RetreivingDirectoryInformationStartedEventArgs());

            FileInfo[] files = startDirectoryInfo.GetFiles();
            fileCount = files.Length;
			

            OnRetrievingDirectoryInformationCompleted(new RetreivingDirectoryInformationCompletedEventArgs(fileCount));

            startTime = DateTime.Now;
			ProcessingEventArgs pea = new ProcessingEventArgs();
			pea.Count = files.Length;

            OnProcessingStarted(pea);

			FileLocation location;
			FileAccessor accessor; 
            foreach (FileInfo file in files)
            {
                filesProcessed++;

				location = new FileLocation(file.Name);
                try
                {
					accessor = new FileAccessor(this.destinationDirectory);
                    byte[] fileBytes = FileAccessor.ReadFile(file.FullName);
                    bool success = accessor.SaveFile(fileBytes, location);
                    if (!success)
                        Console.WriteLine("Error with file: " + file.FullName);
                    elapsedTime = DateTime.Now.Subtract(startTime);

                    OnFileProcessed(new FileProcessedEventArgs(file.FullName, Path.Combine(this.destinationDirectory,location.ShortPath), fileCount, filesProcessed, elapsedTime));
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message + "|||" + ex.Source);
                }
				

                if (shouldStop)
                    break;
            }

            OnProcessingCompleted(new ProcessingEventArgs());
        }

        public void StopMovingFiles()
        {
            shouldStop = true;
        }

        protected virtual void OnRetrievingDirectoryInformationStarted(RetreivingDirectoryInformationStartedEventArgs e)
        {
            RetreivingDirectoryInformationStarted(this, e);
        }
        protected virtual void OnRetrievingDirectoryInformationCompleted(RetreivingDirectoryInformationCompletedEventArgs e)
        {
            RetreivingDirectoryInformationCompleted(this, e);
        }
        protected virtual void OnProcessingStarted(ProcessingEventArgs e)
        {
            ProcessingStarted(this, e);
        }
        protected virtual void OnProcessingCompleted(ProcessingEventArgs e)
        {
            ProcessingCompleted(this, e);
        }
        protected virtual void OnFileProcessed(FileProcessedEventArgs e)
        {
            FileProcessed(this, e);
        }
    }
}
