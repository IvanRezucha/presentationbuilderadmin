﻿using System;
using System.Threading;
using System.Windows.Forms;
using System.IO;
using PSG.Core.Files;

namespace PSG.FileMigrator
{
    public partial class MainForm : Form
    {
        private string originFolderName = string.Empty;
        private string destinationFolderName = string.Empty;
        private FolderBrowserDialog folderBrowser;
        private Thread worker;
        private FileMover mover;

        public MainForm()
        {
            InitializeComponent();
            folderBrowser = new FolderBrowserDialog();
        }


        private void mover_RetreivingDirectoryInformationStarted(object sender, RetreivingDirectoryInformationStartedEventArgs e)
        {

            txtOutput.Invoke(
                   (MethodInvoker)delegate { txtOutput.Text += Environment.NewLine + "Retreiving Directory Information"; }
            );
        }

        private void mover_RetreivingDirectoryInformationCompleted(object sender, RetreivingDirectoryInformationCompletedEventArgs e)
        {
            txtOutput.Invoke(
                   (MethodInvoker)delegate { txtOutput.Text += Environment.NewLine + "Directory Information Retreival Complete: " + e.FileCount + " files"; }
            );
        }

        private void mover_ProcessingStarted(object sender, ProcessingEventArgs e)
        {
			pbMain.Invoke((MethodInvoker)delegate { pbMain.Step = 1; pbMain.Maximum = e.Count; pbMain.Minimum = 0; pbMain.Value = 0; });

            txtOutput.Invoke(
                   (MethodInvoker)delegate { txtOutput.Text += Environment.NewLine + "Processing Started"; }
            );
        }

        private void mover_ProcessingCompleted(object sender, ProcessingEventArgs e)
        {
            txtOutput.Invoke(
                   (MethodInvoker)delegate { txtOutput.Text += Environment.NewLine + "Processing Completed"; }
            );
        }

        private void mover_FileProcessed(object sender, FileProcessedEventArgs e)
        {
			//MessageBox.Show("File Processed");
			pbMain.Invoke((MethodInvoker)delegate { pbMain.PerformStep(); });
			lblOutput.Invoke((MethodInvoker)delegate
			{
				lblOutput.Text = "File " + e.CurrentFileNumber + " of " + e.TotalFileCount + " | Elapsed Time:" + e.ElapsedTime + " | Moved from " + e.OldFilePath + " to " + e.NewFilePath;
			}
			);
        }

        private void SetupHandlers()
        {
            mover.FileProcessed += new FileProcessedEventHandler(mover_FileProcessed);
            mover.ProcessingCompleted += new ProcessingCompletedEventHandler(mover_ProcessingCompleted);
            mover.ProcessingStarted += new ProcessingStartedEventHandler(mover_ProcessingStarted);
            mover.RetreivingDirectoryInformationCompleted += new RetreivingDirectoryInformationCompletedEventHandler(mover_RetreivingDirectoryInformationCompleted);
            mover.RetreivingDirectoryInformationStarted += new RetreivingDirectoryInformationStartedEventHandler(mover_RetreivingDirectoryInformationStarted);
        }

        private void btnChooseStart_Click(object sender, EventArgs e)
        {
            folderBrowser.RootFolder = System.Environment.SpecialFolder.Desktop;
            DialogResult result = folderBrowser.ShowDialog();
            if (result == DialogResult.OK)
            {
                originFolderName = folderBrowser.SelectedPath;
                txtStartFolder.Text = originFolderName;
            }
        }

        private void btnMigrate_Click(object sender, EventArgs e)
        {
            if (Directory.Exists(this.originFolderName) && Directory.Exists(this.destinationFolderName))
            {
                //FileLocation.DIRECTORY_ROOT = destinationFolderName;
                mover = new FileMover(this.originFolderName, this.destinationFolderName);
                SetupHandlers();
                worker = new Thread(mover.MoveFiles);
                Console.WriteLine("File Mover: Thread Starting");
                worker.Start();
            }
            else
            {
                MessageBox.Show("Origin and destination folders need to be valid directories.", "File Migration");
            }
        }

        private void btnChooseDestination_Click(object sender, EventArgs e)
        {
            folderBrowser.RootFolder = System.Environment.SpecialFolder.Desktop;
            DialogResult result = folderBrowser.ShowDialog();
            if (result == DialogResult.OK)
            {
                destinationFolderName = folderBrowser.SelectedPath;
                txtEndFolder.Text = destinationFolderName;
            }
        }

        private void btnStopMigration_Click(object sender, EventArgs e)
        {
            mover.StopMovingFiles();
            Console.WriteLine("File Mover: Stop Requested");
            //worker.Interrupt();
            worker.Abort();
            //worker.Join();
            Console.WriteLine("File Mover: Thread Joined");
            Console.WriteLine("File Mover: Processing Terminated");
        }

        private void cmdCheckPath_Click(object sender, EventArgs e)
        {
            FileLocation location = new FileLocation(txtCheckPath.Text);
            MessageBox.Show(location.ShortPath);
        }
    }
}
