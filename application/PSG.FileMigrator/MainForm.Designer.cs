﻿namespace PSG.FileMigrator
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.txtOutput = new System.Windows.Forms.TextBox();
			this.btnStopMigration = new System.Windows.Forms.Button();
			this.btnMigrate = new System.Windows.Forms.Button();
			this.txtEndFolder = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.txtStartFolder = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.btnChooseDestination = new System.Windows.Forms.Button();
			this.btnChooseStart = new System.Windows.Forms.Button();
			this.txtCheckPath = new System.Windows.Forms.TextBox();
			this.cmdCheckPath = new System.Windows.Forms.Button();
			this.pbMain = new System.Windows.Forms.ProgressBar();
			this.lblOutput = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// txtOutput
			// 
			this.txtOutput.Location = new System.Drawing.Point(156, 154);
			this.txtOutput.Multiline = true;
			this.txtOutput.Name = "txtOutput";
			this.txtOutput.ReadOnly = true;
			this.txtOutput.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.txtOutput.Size = new System.Drawing.Size(400, 455);
			this.txtOutput.TabIndex = 22;
			this.txtOutput.WordWrap = false;
			// 
			// btnStopMigration
			// 
			this.btnStopMigration.Location = new System.Drawing.Point(9, 183);
			this.btnStopMigration.Name = "btnStopMigration";
			this.btnStopMigration.Size = new System.Drawing.Size(141, 24);
			this.btnStopMigration.TabIndex = 21;
			this.btnStopMigration.Text = "Stop Migration";
			this.btnStopMigration.UseVisualStyleBackColor = true;
			this.btnStopMigration.Click += new System.EventHandler(this.btnStopMigration_Click);
			// 
			// btnMigrate
			// 
			this.btnMigrate.Location = new System.Drawing.Point(9, 154);
			this.btnMigrate.Name = "btnMigrate";
			this.btnMigrate.Size = new System.Drawing.Size(141, 23);
			this.btnMigrate.TabIndex = 20;
			this.btnMigrate.Text = "Migrate";
			this.btnMigrate.UseVisualStyleBackColor = true;
			this.btnMigrate.Click += new System.EventHandler(this.btnMigrate_Click);
			// 
			// txtEndFolder
			// 
			this.txtEndFolder.Location = new System.Drawing.Point(8, 63);
			this.txtEndFolder.Name = "txtEndFolder";
			this.txtEndFolder.ReadOnly = true;
			this.txtEndFolder.Size = new System.Drawing.Size(518, 20);
			this.txtEndFolder.TabIndex = 19;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(5, 49);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(295, 13);
			this.label2.TabIndex = 18;
			this.label2.Text = "Select the destination directory where files will be migrated to.";
			// 
			// txtStartFolder
			// 
			this.txtStartFolder.Location = new System.Drawing.Point(8, 22);
			this.txtStartFolder.Name = "txtStartFolder";
			this.txtStartFolder.ReadOnly = true;
			this.txtStartFolder.Size = new System.Drawing.Size(518, 20);
			this.txtStartFolder.TabIndex = 17;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(5, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(253, 13);
			this.label1.TabIndex = 16;
			this.label1.Text = "Select the directory that contains the files to migrate.";
			// 
			// btnChooseDestination
			// 
			this.btnChooseDestination.Location = new System.Drawing.Point(532, 64);
			this.btnChooseDestination.Name = "btnChooseDestination";
			this.btnChooseDestination.Size = new System.Drawing.Size(24, 19);
			this.btnChooseDestination.TabIndex = 15;
			this.btnChooseDestination.Text = "...";
			this.btnChooseDestination.UseVisualStyleBackColor = true;
			this.btnChooseDestination.Click += new System.EventHandler(this.btnChooseDestination_Click);
			// 
			// btnChooseStart
			// 
			this.btnChooseStart.Location = new System.Drawing.Point(532, 24);
			this.btnChooseStart.Name = "btnChooseStart";
			this.btnChooseStart.Size = new System.Drawing.Size(24, 19);
			this.btnChooseStart.TabIndex = 14;
			this.btnChooseStart.Text = "...";
			this.btnChooseStart.UseVisualStyleBackColor = true;
			this.btnChooseStart.Click += new System.EventHandler(this.btnChooseStart_Click);
			// 
			// txtCheckPath
			// 
			this.txtCheckPath.Location = new System.Drawing.Point(17, 319);
			this.txtCheckPath.Name = "txtCheckPath";
			this.txtCheckPath.Size = new System.Drawing.Size(132, 20);
			this.txtCheckPath.TabIndex = 23;
			// 
			// cmdCheckPath
			// 
			this.cmdCheckPath.Location = new System.Drawing.Point(17, 346);
			this.cmdCheckPath.Name = "cmdCheckPath";
			this.cmdCheckPath.Size = new System.Drawing.Size(75, 23);
			this.cmdCheckPath.TabIndex = 24;
			this.cmdCheckPath.Text = "Check Path";
			this.cmdCheckPath.UseVisualStyleBackColor = true;
			this.cmdCheckPath.Click += new System.EventHandler(this.cmdCheckPath_Click);
			// 
			// pbMain
			// 
			this.pbMain.Location = new System.Drawing.Point(8, 117);
			this.pbMain.Name = "pbMain";
			this.pbMain.Size = new System.Drawing.Size(548, 23);
			this.pbMain.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
			this.pbMain.TabIndex = 25;
			// 
			// lblOutput
			// 
			this.lblOutput.AutoSize = true;
			this.lblOutput.Location = new System.Drawing.Point(8, 98);
			this.lblOutput.Name = "lblOutput";
			this.lblOutput.Size = new System.Drawing.Size(0, 13);
			this.lblOutput.TabIndex = 26;
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(572, 621);
			this.Controls.Add(this.lblOutput);
			this.Controls.Add(this.pbMain);
			this.Controls.Add(this.cmdCheckPath);
			this.Controls.Add(this.txtCheckPath);
			this.Controls.Add(this.txtOutput);
			this.Controls.Add(this.btnStopMigration);
			this.Controls.Add(this.btnMigrate);
			this.Controls.Add(this.txtEndFolder);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.txtStartFolder);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.btnChooseDestination);
			this.Controls.Add(this.btnChooseStart);
			this.Name = "MainForm";
			this.Text = "Form 1 Tester";
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtOutput;
        private System.Windows.Forms.Button btnStopMigration;
        private System.Windows.Forms.Button btnMigrate;
        private System.Windows.Forms.TextBox txtEndFolder;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtStartFolder;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnChooseDestination;
        private System.Windows.Forms.Button btnChooseStart;
        private System.Windows.Forms.TextBox txtCheckPath;
        private System.Windows.Forms.Button cmdCheckPath;
		private System.Windows.Forms.ProgressBar pbMain;
		private System.Windows.Forms.Label lblOutput;
    }
}