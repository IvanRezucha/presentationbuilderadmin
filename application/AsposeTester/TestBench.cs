﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace AsposeTester
{
    public partial class TestBench : Form
    {
        public TestBench()
        {
            InitializeComponent();
        }

        private void buttonSelectFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Multiselect = false;
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                textBoxFilepath.Text = dialog.FileName;
            }
        }

        private void buttonCopyWithAspose_Click(object sender, EventArgs e)
        {
            if (File.Exists(textBoxFilepath.Text))
            {                
                SaveFileDialog saveDialog = new SaveFileDialog();
                if (saveDialog.ShowDialog() == DialogResult.OK)
                {
                    System.Collections.ArrayList fileList = new System.Collections.ArrayList();
                    fileList.Add(textBoxFilepath.Text);
                    PlugIns.Helpers.PptBuilder pptBuilder = new PlugIns.Helpers.PptBuilder();
                    pptBuilder.ProgressChanged += new ProgressChangedEventHandler(pptBuilder_ProgressChanged);
                    pptBuilder.RunWorkerCompleted += new RunWorkerCompletedEventHandler(pptBuilder_RunWorkerCompleted);
                    pptBuilder.WorkerReportsProgress = true;
                    pptBuilder.AsposeSlideLicensePath = Path.Combine(Application.StartupPath, @"lic\Aspose.Slides.lic");
                    pptBuilder.CreatePresentationAsync(fileList, saveDialog.FileName);
                }
                //Aspose.Slides.Presentation presentation;
           
                //presentation = PlugIns.Helpers.PptUtils.GetPresentationObject(textBoxFilepath.Text);


            }
        }

        void pptBuilder_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            MessageBox.Show("Done");
        }

        void pptBuilder_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            
        }
    }
}
