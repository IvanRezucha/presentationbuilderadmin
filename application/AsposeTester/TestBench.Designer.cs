﻿namespace AsposeTester
{
    partial class TestBench
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxFilepath = new System.Windows.Forms.TextBox();
            this.labelFilepath = new System.Windows.Forms.Label();
            this.buttonSelectFile = new System.Windows.Forms.Button();
            this.buttonCopyWithAspose = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBoxFilepath
            // 
            this.textBoxFilepath.Location = new System.Drawing.Point(65, 36);
            this.textBoxFilepath.Name = "textBoxFilepath";
            this.textBoxFilepath.Size = new System.Drawing.Size(555, 20);
            this.textBoxFilepath.TabIndex = 0;
            // 
            // labelFilepath
            // 
            this.labelFilepath.AutoSize = true;
            this.labelFilepath.Location = new System.Drawing.Point(12, 36);
            this.labelFilepath.Name = "labelFilepath";
            this.labelFilepath.Size = new System.Drawing.Size(47, 13);
            this.labelFilepath.TabIndex = 1;
            this.labelFilepath.Text = "File path";
            // 
            // buttonSelectFile
            // 
            this.buttonSelectFile.Location = new System.Drawing.Point(626, 36);
            this.buttonSelectFile.Name = "buttonSelectFile";
            this.buttonSelectFile.Size = new System.Drawing.Size(25, 20);
            this.buttonSelectFile.TabIndex = 2;
            this.buttonSelectFile.Text = "...";
            this.buttonSelectFile.UseVisualStyleBackColor = true;
            this.buttonSelectFile.Click += new System.EventHandler(this.buttonSelectFile_Click);
            // 
            // buttonCopyWithAspose
            // 
            this.buttonCopyWithAspose.Location = new System.Drawing.Point(65, 62);
            this.buttonCopyWithAspose.Name = "buttonCopyWithAspose";
            this.buttonCopyWithAspose.Size = new System.Drawing.Size(106, 23);
            this.buttonCopyWithAspose.TabIndex = 3;
            this.buttonCopyWithAspose.Text = "Copy with Aspose";
            this.buttonCopyWithAspose.UseVisualStyleBackColor = true;
            this.buttonCopyWithAspose.Click += new System.EventHandler(this.buttonCopyWithAspose_Click);
            // 
            // TestBench
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(656, 153);
            this.Controls.Add(this.buttonCopyWithAspose);
            this.Controls.Add(this.buttonSelectFile);
            this.Controls.Add(this.labelFilepath);
            this.Controls.Add(this.textBoxFilepath);
            this.Name = "TestBench";
            this.Text = "Aspose.Slides Tester";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxFilepath;
        private System.Windows.Forms.Label labelFilepath;
        private System.Windows.Forms.Button buttonSelectFile;
        private System.Windows.Forms.Button buttonCopyWithAspose;
    }
}