﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PSG.Core.Notifiers;

namespace EmailCanvas
{

    class EmailBlaster
    {
        private static readonly DateTime date = new DateTime(2007, 12, 31, 0, 0, 0, 0, DateTimeKind.Unspecified);
        private static readonly string    emailSubject = "Hewlett-Packard Printing Sales Guide Update Notification";
        private static readonly string emailBody = "Dear HP Printing Sales Guide User,\n\nIn the next few days the Web version of the HP Printing Sales Guide, http://www.hpprintingsalesguide.com/WebApp/Main.aspx, will begin requiring a user login. The process is very simple and intuitive, and is described below. Please, if you have any questions or need any assistance with the new changes, don't hesitate to contact support@hpprintingsalesguide.com.\n\nFirst Time Access:\n\n1. Click the \"I've Already Registered, I Need a Password\" link on the bottom right of the page.\n2. When prompted, enter your email address and click on the \"Get New Password\" button. The email address you use should match the email address that we sent this email to.\n3. Your new password will be emailed to you. Use this new password to login.\n\nNOTE: If our system does not recognize the email address you entered, you will receive a message instructing you to verify that the email address you entered matches the one used to receive this email. If the system does not find a matching email address, you will need to click the \"Sign Up\" link appearing on the initial login screen.\n\nAgain, if you have any questions or need any assistance with the new changes, don't hesitate to contact support@hpprintingsalesguide.com.\n\nThank You,\n\nThe HP Printing Sales Guide Support Team";

        public static void TestBlast()
        {

            //ServiceReference1.UserRegistrationServiceSoapClient userRegistrationService = new ServiceReference1.UserRegistrationServiceSoapClient();
            //ServiceReference1.UserRegistrationDS.UserProfileDataTable users = userRegistrationService.GetActiveUsersFromDate(date);
            //foreach (ServiceReference1.UserRegistrationDS.UserProfileRow row in users)
            //{
            //}
        }

        public static void GetTestEmails()
        {

            //ServiceReference1.UserRegistrationServiceSoapClient userRegistrationService = new ServiceReference1.UserRegistrationServiceSoapClient();
            //ServiceReference1.UserRegistrationDS.UserProfileDataTable users = userRegistrationService.GetActiveUsersFromDate(date);
            //foreach (ServiceReference1.UserRegistrationDS.UserProfileRow row in users)
            //{
            //    Console.WriteLine(row.Email + " " + row.LastLoginDate.ToString());
            //}
            //Console.ReadLine();
        }

        private static string[] ReadProdEmails()
        {
            string[] rows = System.IO.File.ReadAllLines(@"C:\AllPSG_Users2.txt", Encoding.ASCII);
            string[] emails = new string[rows.Length];
            int i = 0;
            foreach (string row in rows)
            {
                string[] fields = row.Split('\t');
                emails[i] = fields[3];
                i++;
            }
            return emails;
        }

        public static void GetProdEmails()
        {
            try
            {
                string[] emails = ReadProdEmails();
                Console.WriteLine("Emails\n\n");
                int i = 1;
                foreach (string email in emails)
                {
                    //EmailNotifier EmailSender = new EmailNotifier(email, "noreply@hpprintingsalesguide.com", emailSubject, emailBody);
                    //EmailSender.Send();
                    Console.WriteLine("Sent Email " + i + " to " + email);
                    i++;
                }
                Console.WriteLine("\n\nTotal: " + i);
            }
            catch (Exception) {
                Console.ReadLine();
            }
               Console.ReadLine();
        }



    }
}
