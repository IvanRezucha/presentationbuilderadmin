﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Xml;
using PSG.WebService.DataSets;
using AnnouncementDSTableAdapters = PSG.WebService.DataSets.AnnouncementDSTableAdapters;

namespace PSG.WebService
{
    public partial class ClientStartPage : System.Web.UI.Page
    {
        int regionId = 0;
        int previewId = 0;
        AnnouncementDS dsAnnouncement;
        AnnouncementDSTableAdapters.AnnouncementTableAdapter taAnnouncement; 

        protected void Page_Load(object sender, EventArgs e)
        {
            dsAnnouncement = new AnnouncementDS();
            taAnnouncement = new AnnouncementDSTableAdapters.AnnouncementTableAdapter();

            ParseQueryString();
            if (previewId!=0)
                ProcessAsPreview();
            else
                ProcessAsActual();
        }
        private void ParseQueryString()
        {
            if (Request.QueryString["regionId"] != null)
                int.TryParse(Request.QueryString["regionId"], out regionId);

            if (regionId == 0)
                regionId = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["DefaultRegionIdForClientStartPage"]); 

            if (Request.QueryString["previewId"] != null)
                int.TryParse(Request.QueryString["previewId"], out previewId);
        }
        private void ProcessAsPreview()
        {
            taAnnouncement.FillByAnnouncementId(dsAnnouncement.Announcement,previewId);
            BindData(dsAnnouncement.Announcement);
        }
        private void ProcessAsActual()
        {
            taAnnouncement.FillByRegionId(dsAnnouncement.Announcement, regionId);
            BindData(dsAnnouncement.Announcement);
        }
        private void BindData(AnnouncementDS.AnnouncementDataTable bindableDataTable)
        {
			if (bindableDataTable.Rows.Count > 0)
			{
				AnnouncementDS.AnnouncementRow row = bindableDataTable.Rows[0] as AnnouncementDS.AnnouncementRow;
				litBody.Text = row.Body;
			}
            /*
            DataView dv = new DataView(bindableDataTable);
            dv.Sort = "PubDate DESC";
            rptAnnouncements.DataSource = dv;
            rptAnnouncements.DataBind();
             */
        }
    }
}
