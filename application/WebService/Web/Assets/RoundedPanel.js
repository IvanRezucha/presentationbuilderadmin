//This class is used to create an instance of a rounded panel.
//The constructor takes the id of the parent element and
//the id of the of the element to be rounded which is the child element.
//This class requires that this css is place inside the page using this class.
/* rounded corners start
.t {background: url(Assets/t.png) 0 0 repeat-x; margin-bottom:5px;}
.b {background: url(Assets/b.png) 0 100% repeat-x; }
.l {background: url(Assets/l.png) 0 0 repeat-y; }
.r {background: url(Assets/r.png) 100% 0 repeat-y; }
.bl {background: url(Assets/bl.png) 0 100% no-repeat; }
.br {background: url(Assets/br.png) 100% 100% no-repeat; }
.tl {background: url(Assets/tl.png) 0 0 no-repeat; }
.tr {background: url(Assets/tr.png) 100% 0 no-repeat; padding:10px; } 
   rounded corners end */


function RoundedPanel(parentElementId,childElementId){
   this.parentElementId = parentElementId;
   this.childElementId = childElementId;
   this.parentElement = document.getElementById(this.parentElementId);
   this.childElement = document.getElementById(this.childElementId);

   this.tDiv  = null;
   this.bDiv  = null;
   this.lDiv  = null;
   this.rDiv  = null;
   this.blDiv = null;
   this.brDiv = null;
   this.tlDiv = null;
   this.trDiv = null;

   this.CreatePanel();
}

RoundedPanel.prototype.CreatePanel = function() {
   this.CreateDivs();
   this.AssignChildren();
}

RoundedPanel.prototype.CreateDivs = function() {
   this.tDiv = document.createElement("div");  
   this.tDiv.className = 't';
   this.bDiv = document.createElement("div");
   this.bDiv.className = 'b';
   this.lDiv = document.createElement("div");
   this.lDiv.className = 'l';
   this.rDiv = document.createElement("div");
   this.rDiv.className = 'r';
   this.blDiv = document.createElement("div");
   this.blDiv.className = 'bl';
   this.brDiv = document.createElement("div");
   this.brDiv.className = 'br';
   this.tlDiv = document.createElement("div");
   this.tlDiv.className = 'tl';
   this.trDiv = document.createElement("div");
   this.trDiv.className = 'tr';
}

RoundedPanel.prototype.AssignChildren = function() {
   this.trDiv.appendChild(this.childElement);
   this.tlDiv.appendChild(this.trDiv);
   this.brDiv.appendChild(this.tlDiv);
   this.blDiv.appendChild(this.brDiv);
   this.rDiv.appendChild(this.blDiv);
   this.lDiv.appendChild(this.rDiv);
   this.bDiv.appendChild(this.lDiv);
   this.tDiv.appendChild(this.bDiv);
   this.parentElement.appendChild(this.tDiv);
}
