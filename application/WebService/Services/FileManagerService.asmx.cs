﻿using System.Web.Configuration;
using System.Web.Services;
using PSG.Core.Files;

namespace PSG.WebService
{
    /// <summary>
    /// Summary description for FileManagerService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class FileManagerService : System.Web.Services.WebService
    {

        [WebMethod]
        public byte[] GetFile(string filename)
        {
            string directoryRoot = WebConfigurationManager.AppSettings["ResourceFilesDirectory"].ToString();
            FileAccessor fileManager = new FileAccessor(directoryRoot);
            return fileManager.GetFile(filename);
        }

        [WebMethod]
        public bool PutFile(byte[] buffer, string filename)
        {
            string directoryRoot = WebConfigurationManager.AppSettings["ResourceFilesDirectory"].ToString();
            FileAccessor fileManager = new FileAccessor(directoryRoot);
            return fileManager.SaveFile(buffer, filename);
        }
    }
}

