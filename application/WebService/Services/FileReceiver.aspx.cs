using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.IO;
using System.Net;
using System.Web.Configuration;
using PSG.Core.Files;

public partial class FileReceiver : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        foreach (string f in Request.Files.AllKeys)
        {
            /*
            HttpPostedFile file = Request.Files[f];
            string fileName = "";
            fileName = WebConfigurationManager.AppSettings["RootDir"] + @"\Resources\Ppt\" + file.FileName;
 
            //if it is a .ppt file save it as a .pot
            if (fileName.EndsWith(".ppt"))
            {
                fileName = fileName.Replace(".ppt", ".pot");
            }
            file.SaveAs(fileName);
             */

            
            HttpPostedFile file = Request.Files[f];
            string fileName = file.FileName;

            if (fileName.EndsWith(".ppt"))
                fileName = fileName.Replace(".ppt", ".pot");

            FileLocation location = new FileLocation(fileName);

            string rootDirectory = WebConfigurationManager.AppSettings["ResourceFilesDirectory"];
            fileName = Path.Combine(rootDirectory,location.ShortPath);
            string directory = Path.Combine(rootDirectory, location.Directory);

            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);

            file.SaveAs(fileName);
        } 
    }
}
