﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using PSG.WebService.DataSets;
using UserVisitDSTableAdapters = PSG.WebService.DataSets.UserVisitDSTableAdapters;

namespace PSG.WebService.Services
{
    /// <summary>
    /// Summary description for UserVisitService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class UserVisitService : System.Web.Services.WebService
    {
        /// <summary>
        /// Takes a UserVisitTable and sends it back to the DB
        /// </summary>
        [WebMethod]
        public void SubmitVisitInformation(UserVisitDS.UserVisitDataTable changedDataTable)
        {
            UserVisitDSTableAdapters.UserVisitTableAdapter taUserVisit = new UserVisitDSTableAdapters.UserVisitTableAdapter();
            taUserVisit.Update(changedDataTable);
        }
    }
}
