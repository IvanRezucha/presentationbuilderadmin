﻿using System;
using System.Web.Services;

/// <summary>
/// Summary description for UpgradeService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class UpgradeService : System.Web.Services.WebService
{

	public UpgradeService()
	{

		//Uncomment the following line if using designed components 
		//InitializeComponent(); 
	}

	[WebMethod]
	public bool IsMajorUpgradeAvailable()
	{
		try
		{
			return Convert.ToBoolean(System.Web.Configuration.WebConfigurationManager.AppSettings["IsMajorUpgradeAvailable"]);
		}
		catch (Exception)
		{
			//todo: enable tracing.
			return false;
		}
	}

	[WebMethod]
	public string NewApplicationManifestUrl()
	{
		return System.Web.Configuration.WebConfigurationManager.AppSettings["NewApplicationManifestUrl"];
	}

}

