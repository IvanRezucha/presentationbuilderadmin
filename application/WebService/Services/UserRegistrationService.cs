﻿using System;
using System.Web.Services;
using PSG.WebService.DataSets;
using System.Web.Security;
using PSG.Core.Notifiers;
using UserRegistrationDSTableAdapters = PSG.WebService.DataSets.UserRegistrationDSTableAdapters;

/// <summary>
/// Summary description for UserRegistrationService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class UserRegistrationService : System.Web.Services.WebService
{

    public UserRegistrationService()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public UserRegistrationDS.UserProfileDataTable GetUserDataTableByEmail(string Email)
    {
        UserRegistrationDSTableAdapters.UserProfileTableAdapter taUserProfile = new UserRegistrationDSTableAdapters.UserProfileTableAdapter();
        UserRegistrationDS.UserProfileDataTable dtUserProfile = new UserRegistrationDS.UserProfileDataTable();

        taUserProfile.FillByEmail(dtUserProfile, Email);
        return dtUserProfile;
    }

    /// <summary>
    /// Takes a UserProfileDataTable and sends it back to the DB
    /// </summary>
    [WebMethod]
    public void SubmitNewUserData(UserRegistrationDS.UserProfileDataTable changedDataTable)
    {
        UserRegistrationDSTableAdapters.UserProfileTableAdapter taUserProfile = new UserRegistrationDSTableAdapters.UserProfileTableAdapter();
        taUserProfile.Update(changedDataTable);
    }

    /// <summary>
    /// Records a user's login date and time to the database in UTC format.
    /// </summary>
    /// <param name="Email">Email of the user to log in</param>
    /// <returns>False if user does not exist in the db, else true.</returns>
    [WebMethod]
    public bool RecordUserLoginDateTime(string Email, string application)
    {
        bool result = true;

        UserRegistrationDSTableAdapters.UserProfileTableAdapter taUserProfile = new UserRegistrationDSTableAdapters.UserProfileTableAdapter();
        UserRegistrationDS.UserProfileDataTable dtUserProfile = new UserRegistrationDS.UserProfileDataTable();
        taUserProfile.FillByEmail(dtUserProfile, Email);
        if (dtUserProfile.Rows.Count < 1)
        {
            result = false;
        }
        else if (dtUserProfile.Rows.Count > 1)
        {
            throw new Exception(String.Format("Database contains more than one record for {0}. Check DB consistency", Email));
        }
        else if (dtUserProfile.Rows.Count == 1)
        {
            UserRegistrationDS.UserProfileRow profileRow = dtUserProfile[0] as UserRegistrationDS.UserProfileRow;
            profileRow.LastLoginDate = DateTime.UtcNow;
            taUserProfile.Update(profileRow);
            LogLogin(profileRow, application);
        }

        return result;
    }

    /// <summary>
    /// Records the amount of time the last install took for a user.
    /// </summary>
    /// <param name="Email">Email identifying user</param>
    /// <param name="installDurationInseconds">Length of time in seconds the install took</param>
    /// <returns>False if user does not exist, else true</returns>
    [WebMethod]
    public bool RecordUserContentInstallDuration(string Email, int installDurationInseconds)
    {
        bool result = true;

        UserRegistrationDSTableAdapters.UserProfileTableAdapter taUserProfile = new UserRegistrationDSTableAdapters.UserProfileTableAdapter();
        UserRegistrationDS.UserProfileDataTable dtUserProfile = new UserRegistrationDS.UserProfileDataTable();
        taUserProfile.FillByEmail(dtUserProfile, Email);
        if (dtUserProfile.Rows.Count < 1)
        {
            result = false;
        }
        else if (dtUserProfile.Rows.Count > 1)
        {
            throw new Exception(String.Format("Database contains more than one record for {0}. Check DB consistency", Email));
        }
        else if (dtUserProfile.Rows.Count == 1)
        {
            dtUserProfile[0].ContentInstallDuration = installDurationInseconds;
            taUserProfile.Update(dtUserProfile);
        }

        return result;
    }

    /// <summary>
    /// Records the amount of time the last content update took for a user.
    /// </summary>
    /// <param name="Email">Email identifying user</param>
    /// <param name="installDurationInseconds">Length of time in seconds the update took</param>
    /// <returns>False if user does not exist, else true</returns>
    [WebMethod]
    public bool RecordUserContentUpdateDuration(string Email, int updateDurationInseconds)
    {
        bool result = true;

        UserRegistrationDSTableAdapters.UserProfileTableAdapter taUserProfile = new UserRegistrationDSTableAdapters.UserProfileTableAdapter();
        UserRegistrationDS.UserProfileDataTable dtUserProfile = new UserRegistrationDS.UserProfileDataTable();
        taUserProfile.FillByEmail(dtUserProfile, Email);
        if (dtUserProfile.Rows.Count < 1)
        {
            result = false;
        }
        else if (dtUserProfile.Rows.Count > 1)
        {
            throw new Exception(String.Format("Database contains more than one record for {0}. Check DB consistency", Email));
        }
        else if (dtUserProfile.Rows.Count == 1)
        {
            dtUserProfile[0].LastContentUpdateDuration = updateDurationInseconds;
            taUserProfile.Update(dtUserProfile);
        }

        return result;
    }

    /// <summary>
    /// Submits a user's selected region languages to the db for tracking
    /// </summary>
    /// <param name="Email">Email identifier of the user</param>
    /// <param name="RegionLanguageList">Integer array containing user's Region Language IDs</param>
    [WebMethod]
    public void SubmitUserRegionLanguagesForUser(string Email, int[] RegionLanguageList)
    {
        int UserID = -1;

        UserRegistrationDS.UserProfileDataTable dtUserProfile = new UserRegistrationDS.UserProfileDataTable();
        UserRegistrationDSTableAdapters.UserProfileTableAdapter taUserProfile = new UserRegistrationDSTableAdapters.UserProfileTableAdapter();

        UserRegistrationDS.UserRegionLanguageDataTable dtUserRegionLanguage = new UserRegistrationDS.UserRegionLanguageDataTable();
        UserRegistrationDSTableAdapters.UserRegionLanguageTableAdapter taUserRegionLanguage = new UserRegistrationDSTableAdapters.UserRegionLanguageTableAdapter();

        //get the user's user id from their email
        taUserProfile.FillByEmail(dtUserProfile, Email);
        if (dtUserProfile.Rows.Count < 1)
        {
            throw new Exception(String.Format("{0} user does not exist", Email));
        }
        else if (dtUserProfile.Rows.Count > 1)
        {
            throw new Exception(String.Format("Database contains more than one record for {0}. Check DB consistency", Email));
        }
        else if (dtUserProfile.Rows.Count == 1)
        {
            UserID = dtUserProfile[0].UserID;
        }

        //delete all the user's current region languages
        taUserRegionLanguage.FillByUserID(dtUserRegionLanguage, UserID);
        foreach (UserRegistrationDS.UserRegionLanguageRow row in dtUserRegionLanguage.Rows)
        {
            row.Delete();
        }
        UserRegistrationDS.UserRegionLanguageRow rowUserRegionLanguage = dtUserRegionLanguage.NewUserRegionLanguageRow();
        //add the user's current region languages
        foreach (int regionLanguage in RegionLanguageList)
        {
            rowUserRegionLanguage = dtUserRegionLanguage.NewUserRegionLanguageRow();
            rowUserRegionLanguage.UserID = UserID;
            rowUserRegionLanguage.RegionLanguageID = regionLanguage;
            dtUserRegionLanguage.AddUserRegionLanguageRow(rowUserRegionLanguage);
        }
        //commit the changes to the db
        if (null != ((UserRegistrationDS.UserRegionLanguageDataTable)dtUserRegionLanguage.GetChanges(System.Data.DataRowState.Deleted)))
            taUserRegionLanguage.Update(((UserRegistrationDS.UserRegionLanguageDataTable)dtUserRegionLanguage.GetChanges(System.Data.DataRowState.Deleted)));
        if (null != ((UserRegistrationDS.UserRegionLanguageDataTable)dtUserRegionLanguage.GetChanges(System.Data.DataRowState.Added)))
            taUserRegionLanguage.Update(((UserRegistrationDS.UserRegionLanguageDataTable)dtUserRegionLanguage.GetChanges(System.Data.DataRowState.Added)));
    }

    [WebMethod]
    public bool ValidateUser(string Email, string Password, string Application)
    {
        bool result = false;
        UserRegistrationDS.UserProfileDataTable profileTable = GetUserByEmail(Email);
        UserRegistrationDS.UserProfileRow profileRow;
        if (profileTable.Rows.Count == 1)
        {
            profileRow = profileTable.Rows[0] as UserRegistrationDS.UserProfileRow;
            if (profileRow.Password == Password)
            {
                result = true;
                LogLogin(profileRow, Application);
            }
        }
        return result;
    }

    [WebMethod]
    public void LogLogin(int userId, DateTime dateTime, string application)
    {
        UserRegistrationDSTableAdapters.UserLoginHistoryTableAdapter ta = new PSG.WebService.DataSets.UserRegistrationDSTableAdapters.UserLoginHistoryTableAdapter();
        ta.Insert(userId, dateTime, application);
    }

    private void LogLogin(UserRegistrationDS.UserProfileRow profileRow, string application)
    {
        LogLogin(profileRow.UserID, DateTime.UtcNow, application);
    }

    [WebMethod]
    public void eirg_reg(string userEmail, DateTime dateTime)
    {
        UserRegistrationDSTableAdapters.eirg_registrationTableAdapter ra = new PSG.WebService.DataSets.UserRegistrationDSTableAdapters.eirg_registrationTableAdapter();
        ra.Insert(userEmail, dateTime);
    }

    [WebMethod]
    public MembershipCreateStatus CreateUser(string Email, string Password, string Passcode)
    {
        if (!System.Configuration.ConfigurationManager.AppSettings["Passcode"].ToLower().Equals(Passcode.ToLower()))
        {
            return MembershipCreateStatus.UserRejected;
        }

        UserRegistrationDS.UserProfileDataTable profileTable = GetUserByEmail(Email);

        int result = 0;
        if (profileTable.Rows.Count == 1)
        {
            UserRegistrationDS.UserProfileRow profileRow = profileTable.Rows[0] as UserRegistrationDS.UserProfileRow;
            if (!string.IsNullOrEmpty(profileRow.Email) && !string.IsNullOrEmpty(profileRow.Password))
                return MembershipCreateStatus.DuplicateEmail;
            else if (!string.IsNullOrEmpty(profileRow.Email) && string.IsNullOrEmpty(profileRow.Password))
            {
                profileRow.Password = Password;
                UserRegistrationDSTableAdapters.UserProfileTableAdapter ta = new PSG.WebService.DataSets.UserRegistrationDSTableAdapters.UserProfileTableAdapter();
                result = ta.Update(profileRow);
            }
        }
        else
        {
            UserRegistrationDS.UserProfileDataTable newUserTable = new UserRegistrationDS.UserProfileDataTable();
            UserRegistrationDSTableAdapters.UserProfileTableAdapter ta = new PSG.WebService.DataSets.UserRegistrationDSTableAdapters.UserProfileTableAdapter();
            result = ta.Insert(Password, String.Empty, string.Empty, Email, string.Empty, string.Empty, false, DateTime.Now, string.Empty, 0, string.Empty, string.Empty, DateTime.Now, 0, 0, 0, false);
        }





        /*
        int result = 0;
        if (profileTable.Rows.Count == 1)
        {
            UserRegistrationDS.UserProfileRow profileRow = profileTable.Rows[0] as UserRegistrationDS.UserProfileRow;
            profileRow.Password = Password;
            UserRegistrationDSTableAdapters.UserProfileTableAdapter ta = new PSG.WebService.DataSets.UserRegistrationDSTableAdapters.UserProfileTableAdapter();
            result = ta.Update(profileRow);

        }
        else
        {
            UserRegistrationDS.UserProfileDataTable newUserTable = new UserRegistrationDS.UserProfileDataTable();
            UserRegistrationDSTableAdapters.UserProfileTableAdapter ta = new PSG.WebService.DataSets.UserRegistrationDSTableAdapters.UserProfileTableAdapter();
            result = ta.Insert(Password, String.Empty, string.Empty, Email, string.Empty, string.Empty, false, DateTime.Now, string.Empty, 0, string.Empty, string.Empty, DateTime.Now, 0, 0, string.Empty, false);
        }
        */

        if (result == 1)
            return MembershipCreateStatus.Success;
        else
            return MembershipCreateStatus.ProviderError;
    }

    [WebMethod]
    public UserRegistrationDS.CountriesDataTable GetCountries()
    {
        UserRegistrationDS.CountriesDataTable countriesTable = new UserRegistrationDS.CountriesDataTable();
        UserRegistrationDSTableAdapters.CountriesTableAdapter ta = new PSG.WebService.DataSets.UserRegistrationDSTableAdapters.CountriesTableAdapter();
        ta.Fill(countriesTable);
        return countriesTable;
    }

    [WebMethod]
    public UserRegistrationDS.JobTitlesDataTable GetJobTitles()
    {
        UserRegistrationDS.JobTitlesDataTable jobTitlesTable = new UserRegistrationDS.JobTitlesDataTable();
        UserRegistrationDSTableAdapters.JobTitlesTableAdapter ta = new PSG.WebService.DataSets.UserRegistrationDSTableAdapters.JobTitlesTableAdapter();
        ta.Fill(jobTitlesTable);
        return jobTitlesTable;
    }

    [WebMethod]
    public MembershipCreateStatus UpdateProfileInformation(string email, string firstName, string lastName, string phoneNumber, string country, int regionId, string companyName, int companySize, string jobTitle, string referrer)
    {
        UserRegistrationDS.UserProfileDataTable profileTable = GetUserByEmail(email);
        int result = 0;
        if (profileTable.Rows.Count == 1)
        {
            UserRegistrationDS.UserProfileRow profileRow = profileTable.Rows[0] as UserRegistrationDS.UserProfileRow;
            profileRow.FirstName = firstName;
            profileRow.LastName = lastName;
            profileRow.Phone = phoneNumber;
            profileRow.Country = country;
            profileRow.RegionId = regionId;
            profileRow.CompanyName = companyName;
            profileRow.CompanySize = companySize;
            profileRow.JobTitle = jobTitle;
            profileRow.ReferenceSource = referrer;
            UserRegistrationDSTableAdapters.UserProfileTableAdapter ta = new PSG.WebService.DataSets.UserRegistrationDSTableAdapters.UserProfileTableAdapter();
            result = ta.Update(profileRow);
        }
        if (result == 1)
            return MembershipCreateStatus.Success;
        else
            return MembershipCreateStatus.ProviderError;
    }

    [WebMethod]
    public UserRegistrationDS.UserProfileDataTable GetUserByEmail(string Email)
    {
        UserRegistrationDSTableAdapters.UserProfileTableAdapter profileTableAdapter = new PSG.WebService.DataSets.UserRegistrationDSTableAdapters.UserProfileTableAdapter();
        UserRegistrationDS.UserProfileDataTable profileTable = profileTableAdapter.GetDataByEmail(Email);
        return profileTable;
    }

    [WebMethod]
    public bool ChangePassword(string email, string oldpassword, string newpassword)
    {
        bool result = false;
        int returnCode = 0;
        UserRegistrationDS.UserProfileDataTable profileTable = GetUserByEmail(email);
        if (profileTable.Rows.Count == 1)
        {
            UserRegistrationDS.UserProfileRow profileRow = profileTable.Rows[0] as UserRegistrationDS.UserProfileRow;
            if (profileRow.Password.Equals(oldpassword))
            {
                profileRow.Password = newpassword;
                profileRow.TemporaryPasswordInUse = false;
                UserRegistrationDSTableAdapters.UserProfileTableAdapter ta = new PSG.WebService.DataSets.UserRegistrationDSTableAdapters.UserProfileTableAdapter();
                returnCode = ta.Update(profileRow);
                if (returnCode == 1)
                    result = true;
            }
        }

        return result;
    }

    [WebMethod]
    public string CreateTempPassword(string email)
    {
        int returnCode = 0;
        UserRegistrationDS.UserProfileDataTable profileTable = GetUserByEmail(email);
        if (profileTable.Rows.Count == 1)
        {
            UserRegistrationDS.UserProfileRow profileRow = profileTable.Rows[0] as UserRegistrationDS.UserProfileRow;
            /*if (!string.IsNullOrEmpty(profileRow.Password))
            {
             */
            string tempPassword = MakeTempPassword(email);
            profileRow.Password = tempPassword;
            profileRow.TemporaryPasswordInUse = true;
            UserRegistrationDSTableAdapters.UserProfileTableAdapter ta = new PSG.WebService.DataSets.UserRegistrationDSTableAdapters.UserProfileTableAdapter();
            returnCode = ta.Update(profileRow);
            if (returnCode == 1)
                EmailUserPassword(email, tempPassword);
            return "PasswordSent";
            /*
            }
            else
            {
                return "UserNotCreatedYet";
            }
             */
        }
        else
        {
            return "UserNotFound";
        }
    }

    [WebMethod]
    public string SetandGetTempPassword(string email)
    {
        int returnCode = 0;
        UserRegistrationDS.UserProfileDataTable profileTable = GetUserByEmail(email);
        if (profileTable.Rows.Count == 1)
        {
            UserRegistrationDS.UserProfileRow profileRow = profileTable.Rows[0] as UserRegistrationDS.UserProfileRow;
            /*if (!string.IsNullOrEmpty(profileRow.Password))
            {
             */
            string tempPassword = MakeTempPassword(email);
            profileRow.Password = tempPassword;
            profileRow.TemporaryPasswordInUse = true;
            UserRegistrationDSTableAdapters.UserProfileTableAdapter ta = new PSG.WebService.DataSets.UserRegistrationDSTableAdapters.UserProfileTableAdapter();
            returnCode = ta.Update(profileRow);
            return tempPassword;
        }
        else
        {
            return "";
        }
    }

    private void EmailUserPassword(string email, string password)
    {
        string body = "Your password has been reset to: " + password + ". Please reset this password on your next login and thanks for using the HP Printing Sales Guide.";
        EmailNotifier emailer = new EmailNotifier(email, "noreply@hpprintingsalesguide.com", "Password Notification", body);
        emailer.Send();

    }
    private string MakeTempPassword(string email)
    {
        string temp = email + DateTime.Now.ToString() + "salt10";
        return Math.Abs(temp.GetHashCode()).ToString();
    }

    [WebMethod]
    public void send_gen_email(string email, string subject, string body)
    {
        //string body = "You have been registered on the Printing Sales Guide from eIRG. Your password has been reset to: " + password + ". Please reset this password on your next login and thanks for using the HP Printing Sales Guide.";
        EmailNotifier emailer = new EmailNotifier(email, "noreply@hpprintingsalesguide.com", subject, body);
        emailer.SendHTML();
    }

}

