using System;
using System.Web.Services;
using PSG.WebService.DataSets;
using RegistrationDSTableAdapters = PSG.WebService.DataSets.RegistrationDSTableAdapters;


/// <summary>
/// Summary description for Registration
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class Registration : System.Web.Services.WebService
{

    public Registration()
    {
        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public RegistrationDS GetRegistrants(RegistrantTypes registrantType)
    {
        RegistrationDS dsRegistrations = new RegistrationDS();
        switch (registrantType)
        {
            case RegistrantTypes.Ipg:
                RegistrationDSTableAdapters.HpPbIpgTableAdapter taIpg = new RegistrationDSTableAdapters.HpPbIpgTableAdapter();
                taIpg.Fill(dsRegistrations.HpPbIpg);
                break;
            case RegistrantTypes.IpgParnter:
                RegistrationDSTableAdapters.HpPbPartnerTableAdapter taIpgPartner = new RegistrationDSTableAdapters.HpPbPartnerTableAdapter();
                taIpgPartner.Fill(dsRegistrations.HpPbPartner);
                break;
            case RegistrantTypes.HpPrinting:
                RegistrationDSTableAdapters.HpPbLaserJetTableAdapter taHpPrinting = new RegistrationDSTableAdapters.HpPbLaserJetTableAdapter();
                taHpPrinting.Fill(dsRegistrations.HpPbLaserJet);
                break;
            case RegistrantTypes.ConsumerSales:
                RegistrationDSTableAdapters.HpPbConsumerSalesTableAdapter taCs = new RegistrationDSTableAdapters.HpPbConsumerSalesTableAdapter();
                taCs.Fill(dsRegistrations.HpPbConsumerSales);
                break;
            case RegistrantTypes.ConsumerSalesPartner:
                RegistrationDSTableAdapters.HpPbConsumerSalesPartnerTableAdapter taCsPartner = new RegistrationDSTableAdapters.HpPbConsumerSalesPartnerTableAdapter();
                taCsPartner.Fill(dsRegistrations.HpPbConsumerSalesPartner);
                break;
            default:
                Exception x = new Exception("Invalid registration database.");
                throw x;
                break;
        }

        return dsRegistrations;
    
    }


    public enum RegistrantTypes
    { 
        Ipg,
        IpgParnter,
        HpPrinting,
        ConsumerSales,
        ConsumerSalesPartner
    }

}

