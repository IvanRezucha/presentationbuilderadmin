﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Web.Services;
using PSG.WebService.DataSets;
using PSG.WebService.DataSets.UserEventsDSTableAdapters;
using PSG.WebService.DataSets.UserRegistrationDSTableAdapters;
using PSG.WebService.DataSets.SlideIndexingDSTableAdapters;
using log4net;

namespace PSG.WebService.Services
{
    /// <summary>
    /// Summary description for UsageLoggingService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class UsageLoggingService : System.Web.Services.WebService
    {
        private ILog _log = LogManager.GetLogger(typeof(UsageLoggingService));
        /// <summary>
        /// Saves a single session in an open state.
        /// </summary>
        /// <param name="session">The GUID of the session beign saved.</param>
        /// <param name="userId">The UserID of the user initiating the session.</param>
        /// <param name="startDate">The start date of the session.</param>
        /// <returns>The SessionID of the newly created session.</returns>
        [WebMethod]
        public long SaveSession(string session, int userId, DateTime startDate)
        {
            SessionsTableAdapter ta = new SessionsTableAdapter();
            try
            {
                object sessionId = ta.SaveSession(session, userId, startDate);
                return Convert.ToInt64(sessionId);
            }
            catch (Exception e)
            {
                Logger.Error("Exception in SaveSession", e);
                return 0;
            }
        }

        /// <summary>
        /// Saves a batch of sessions (open or closed).
        /// </summary>
        /// <param name="sessions">The typed DataTable containing the sessions to save.</param>
        /// <returns>True if there were sessions saved; otherwise false.</returns>
        [WebMethod]
        public bool SaveSessionBatch(SessionItem[] sessions)
        {
            if (sessions.Length < 1)
                return false;
            SessionsTableAdapter ta = new SessionsTableAdapter();
            int saved = 0;
            foreach (SessionItem session in sessions)
            {
                try
                {
                    if (session.EndDate > session.StartDate)
                    {
                        ta.Insert(session.Session, session.UserId, session.StartDate, session.EndDate);// Convert.ToDateTime(row["StartDate"]), Convert.ToDateTime(row["EndDate"]));
                    }
                    else
                    {
                        ta.SaveSession(session.Session, session.UserId, session.StartDate);// Convert.ToInt32(row["UserID"]), Convert.ToDateTime(row["StartDate"]));
                    }
                    saved++;
                }
                catch (System.Data.SqlClient.SqlException sx)
                {
                    if (sx.Number == 2627)
                    {
                        if (session.EndDate != null)
                        {
                            ta.UpdateSession(session.Session, session.EndDate);
                        }
                        else
                        {
                            Logger.Warn("Session foreign key violation", sx);
                        }
                        saved++;
                    }
                    else
                    {
                        Logger.Error("SaveSessionBatch error", sx);
                    }
                }
                catch (Exception e)
                {
                    Logger.Error("SaveSessionBatch error", e);
                }
            }
            return saved > 0;
        }

        /// <summary>
        /// Updates a single session with an ending DateTime.
        /// </summary>
        /// <param name="session">The Session GUID of the session.</param>
        /// <param name="endDate">The DateTime of when the session ends.</param>
        [WebMethod]
        public void UpdateSession(SessionItem item)
        {
            SessionsTableAdapter ta = new SessionsTableAdapter();
            ta.UpdateSession(item.Session, item.EndDate);
        }

        /// <summary>
        /// Retrieves a list of sessions whose ending DateTime is NULL.
        /// </summary>
        /// <returns>A typed DataTable of open sessions.</returns>
        [WebMethod]
        public UserEventsDS.SessionsDataTable GetOpenSessions()
        {
            SessionsTableAdapter ta = new SessionsTableAdapter();
            return ta.GetOpenSessions();
        }
        
        /// <summary>
        /// Retrieves a typed DataTable of sessions.  The DataTable should only contain a single row
        /// containing the details of the session.
        /// </summary>
        /// <param name="session">The session GUID.</param>
        /// <returns>A typed DataTable with a single row of data representing the session in question.</returns>
        [WebMethod]
        public UserEventsDS.SessionsDataTable GetSessionBySession(string session)
        {
            SessionsTableAdapter da = new SessionsTableAdapter();
            return da.GetSessionBySession(session);
        }

        /// <summary>
        /// Saves a single event.
        /// </summary>
        /// <param name="slideId">The SlideID of the slide in question.</param>
        /// <param name="session">The Session GUID.</param>
        /// <param name="typeId">The Slide Type ID of the slide.</param>
        /// <param name="categoryId">The Category ID of the slide.</param>
        /// <param name="tag">Extra data that may or may not be interesting.</param>
        /// <param name="eventDate">The DateTime of when the event took place.</param>
        /// <returns>The EventID of the newly created event.</returns>
        [WebMethod]
        public long SaveEvent(int slideId, string session, int typeId, int categoryId, string tag, DateTime eventDate)
        {
            try
            {
                EventsTableAdapter ta = new EventsTableAdapter();
                object eventId = ta.SaveEvent(slideId, session, typeId, categoryId, tag, eventDate);
                return Convert.ToInt64(eventId);
            }
            catch (Exception e)
            {
                Logger.Error("SaveEvent error", e);
                return 0;
            }
        }

        /// <summary>
        /// Saves a batch of events.
        /// </summary>
        /// <param name="events">The typed DataTable containing events to save.</param>
        /// <returns>True if there were events; otherwise, false.</returns>
        [WebMethod]
        public bool SaveEventsBatch(EventItem[] events)
        {
            if (events.Length < 1)
                return false;
            EventsTableAdapter ta = new EventsTableAdapter();
            int i = 0;
            foreach (EventItem item in events)
            {
                try
                {
                    i += ta.Insert(item.SlideId, item.Session, item.TypeId, item.CategoryId, item.Tag, item.EventDate);
                }
                catch (System.Data.SqlClient.SqlException sx)
                {
                    if (sx.Number == 547 && sx.Message.Contains("FK_Events_Categories")) // FK violation usually caused by unknown category id
                    {
                        i += ta.Insert(item.SlideId, item.Session, item.TypeId, -2, item.Tag + "; provided CategoryID=" + item.CategoryId, item.EventDate);
                    }
                }
                catch (Exception e)
                {
                    Logger.Error("SaveEventsBatch error", e);
                }
            }
            return i > 0;
        }
        
        /// <summary>
        /// Gets a list of event types.
        /// </summary>
        /// <returns>A typed DataTable of event types.</returns>
        [WebMethod]
        public UserEventsDS.EventTypesDataTable GetEventTypes()
        {
            return new EventTypesTableAdapter().GetData();
        }

        /// <summary>
        /// Retrieves a user's UserID.
        /// </summary>
        /// <param name="email">The user's email address</param>
        /// <returns>The user's UserID or "0" if there are no matching records.</returns>
        [WebMethod]
        public int GetUserByEmail(string email)
        {
            UserProfileTableAdapter da = new UserProfileTableAdapter();
            UserRegistrationDS.UserProfileDataTable profile = da.GetDataByEmail(email);
            if (profile.Rows.Count > 0)
            {
                return Convert.ToInt32(profile.Rows[0]["UserID"]);
            }
            return 0;
        }

        /// <summary>
        /// A beacon to test for the availability of the service.
        /// A client should wrap this method and catch any SOAP exception.
        /// </summary>
        /// <returns>True</returns>
        [WebMethod]
        public bool IsServiceAvailable()
        {
            Logger.Info("IsServiceAvailable was called");
            return true;
        }

        /// <summary>
        /// Saves a list of files that the client indexing failed on.
        /// </summary>
        /// <param name="errorList">The list of files that cannot be indexed.</param>
        /// <returns>void</returns>
        [WebMethod]
        public void SaveIndexingErrors(List<string> errorList)
        {
            SlideIndexingDS.SlideIndexErrorsDataTable table = new SlideIndexingDS.SlideIndexErrorsDataTable();
            SlideIndexErrorsTableAdapter ta = new SlideIndexErrorsTableAdapter();
 
            foreach (string error in errorList)
            {
                SlideIndexingDS.SlideIndexErrorsRow row = table.NewRow() as SlideIndexingDS.SlideIndexErrorsRow;
                SetIndexErrorRowValues(error, row);
                ta.Insert(row.FileName, row.Path, row.Exception);
            }
        }

        private void SetIndexErrorRowValues(string error,SlideIndexingDS.SlideIndexErrorsRow row)
        {
            string[] indexError = error.Split(new string[] { ";!;" }, StringSplitOptions.None);
            row.FileName = indexError[0];
            row.Path = indexError[1];
            row.Exception = indexError[2];
        }

        private ILog Logger
        {
            get
            {
                if (_log == null)
                {
                    _log = LogManager.GetLogger(typeof(UsageLoggingService));
                }
                return _log;
            }
        }
    }

    public class SessionItem
    {
        public String Session;
        public int UserId;
        public DateTime StartDate;
        public DateTime EndDate;
    }

    public class EventItem
    {
        public int SlideId;
        public String Session;
        public int TypeId;
        public int CategoryId;
        public String Tag;
        public DateTime EventDate;
    }

}
