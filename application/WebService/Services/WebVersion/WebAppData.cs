using System;
using System.Diagnostics;
using System.Web.Configuration;
using System.Web.Services;
using PSG.WebService.DataSets;
using PSG.Core.Files;

/// <summary>
/// Summary description for WebAppData
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class WebAppData : System.Web.Services.WebService
{
    TextWriterTraceListener diags;
    public WebAppData()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
        diags = new TextWriterTraceListener(WebConfigurationManager.AppSettings["RootDir"].ToString() + @"\Resources\UpdateData\UpdateDiags.ws");

    }

    [WebMethod]
    public SlideLibrary GetCurrentRegionLanguages()
    {
        string localizedUpdateDataFolderToken = @"\Resources\WebUpdateData\LocalizedUpdateData\";
        string regLangFile = WebConfigurationManager.AppSettings["RootDir"].ToString() + localizedUpdateDataFolderToken + "CurrentRegionLangs.xml";
        SlideLibrary dsSlideLibrary = new SlideLibrary();
        try
        {
            dsSlideLibrary.ReadXml(regLangFile);

        }
        catch (Exception ex)
        {
            WriteToTrace(String.Format("{0}{1}{1}{2}", ex.ToString(), System.Environment.NewLine, regLangFile));
            
        } 
        return dsSlideLibrary;
    }

    private void WriteToTrace(string msg)
    {

        DateTime now = DateTime.Now;

        diags.WriteLine(now.ToShortDateString() + " " + now.ToShortTimeString() + " " + msg);

        diags.Flush();

    }
}

