using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.Services;
/// <summary>
/// Summary description for AdminAuthentication
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class AdminAuthentication : System.Web.Services.WebService
{
	int _timeout = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["AuthenticationTimeout"]);

	#region Validate Users and Renew Tickets

	[WebMethod(Description = "Retrieves the admin ticket credentials for a valid user.")]
	public string GetAuthentication(string email, string password)
	{
		int? userID = null;
		FormsAuthenticationTicket ticket;
		string encryptedTicket;
		DateTime expireTime;

		//syntax of the try/finally based on pp. 93-99 of Effective C#
		SqlConnection conn = null;
		SqlCommand command = null;
		SqlDataReader dr = null;
		try
		{
            conn = new SqlConnection(WebConfigurationManager.ConnectionStrings["MainConn"].ConnectionString);
			command = new SqlCommand("proc_AdminUserSelectByEmailAndPassword", conn);
			command.CommandType = CommandType.StoredProcedure;
			command.Parameters.AddWithValue("@email", email);
			command.Parameters.AddWithValue("@password", password);
			conn.Open();
			dr = command.ExecuteReader(CommandBehavior.CloseConnection);
			if (dr.Read())
				userID = Convert.ToInt32(dr["AdminUserID"]);
		}
		catch (Exception ex)
		{
			WriteToErrorLog("Error on GetAuthentication: " + ex.ToString());
			throw ex;
		}
		finally
		{
			if (dr != null)
				dr.Dispose();
			if (command != null)
				command.Dispose();
			if (conn != null)
				conn.Dispose();
		}

		if (userID == null)
			return null;

		ticket = new FormsAuthenticationTicket(userID.ToString(), false, _timeout);
		encryptedTicket = FormsAuthentication.Encrypt(ticket);
		expireTime = DateTime.Now.AddMinutes(_timeout);
		Context.Cache.Insert(encryptedTicket, userID, null, expireTime, TimeSpan.Zero);

		return encryptedTicket;
	}

	[WebMethod(Description = "Updates the admin ticket timeout for a user that is already logged in.")]
	public string RenewTicket(string adminTicket)
	{
		//if this is not a valid ticket, reject it.
		if (Context.Cache[adminTicket] == null)
		{
			return null;
		}

		int userID = int.Parse(FormsAuthentication.Decrypt(adminTicket).Name);
		DateTime expireTime = DateTime.Now.AddMinutes(_timeout);
		Context.Cache.Insert(adminTicket, userID, null, expireTime, TimeSpan.Zero);

		return adminTicket;
	}

	#endregion

	#region Diagnostics

	private void WriteToErrorLog(string msg)
	{
		TextWriterTraceListener exceptionLog = new TextWriterTraceListener(Server.MapPath("ExceptionLog.txt"));
		DateTime now = DateTime.Now;
		exceptionLog.WriteLine(now.ToShortDateString() + " " + now.ToShortTimeString());
		exceptionLog.WriteLine("");
		exceptionLog.WriteLine(msg);
		exceptionLog.WriteLine("");
		exceptionLog.WriteLine("");
		exceptionLog.Flush();
		exceptionLog.Close();
	}

	#endregion
}