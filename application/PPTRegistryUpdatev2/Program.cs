﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Microsoft.Win32;

namespace PPTRegistryUpdatev2
{
    class Program
    {
        static void Main(string[] args)
        {
            string officeVersionKey = string.Empty;
            try
            {
                officeVersionKey = (string)Registry.GetValue(@"HKEY_CLASSES_ROOT\PowerPoint.Application\CurVer", "", null);
            }
            catch (Exception ex)
            {
                Environment.ExitCode = 1;
            }

            if (officeVersionKey == null)
            {
                Environment.ExitCode = 1;
            }
            else if (officeVersionKey.Equals("PowerPoint.Application.14"))
            {
                //Office 2010
                try
                {
                    Registry.ClassesRoot.CreateSubKey(@"PowerPoint.Show.8").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                    Registry.ClassesRoot.CreateSubKey(@"PowerPoint.SlideShow.8").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                    Registry.ClassesRoot.CreateSubKey(@"PowerPoint.Template.8").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                    Registry.ClassesRoot.CreateSubKey(@"PowerPoint.Presentation.12").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                    Registry.ClassesRoot.CreateSubKey(@"PowerPoint.Template.12").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                    Registry.ClassesRoot.CreateSubKey(@"PowerPoint.Show.12").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                    Registry.ClassesRoot.CreateSubKey(@"PowerPoint.TemplateMacroEnabled.12").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                    Registry.ClassesRoot.CreateSubKey(@"PowerPoint.ShowMacroEnabled.12").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                    Registry.ClassesRoot.CreateSubKey(@"PowerPoint.MacroEnabled.Presentation.12").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                    Registry.ClassesRoot.CreateSubKey(@"PowerPoint.MacroEnabled.Show.12").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                    Registry.ClassesRoot.CreateSubKey(@"PowerPoint.MacroEnabled.Template.12").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                    RegistryKey regKeyAppRoot = Registry.CurrentUser.CreateSubKey(@"Software\Microsoft\Windows\Shell\AttachmentExecute\{0002DF01-0000-0000-C000-000000000046}");
                    regKeyAppRoot.SetValue("PowerPoint.Show.8", 0, 0);
                    Environment.ExitCode = 0;
                }
                catch (Exception ex)
                {
                    Environment.ExitCode = 1;
                }
            }
            else if (officeVersionKey.Equals("PowerPoint.Application.12"))
            {
                //Office 2007
                try
                {
                    Registry.ClassesRoot.CreateSubKey(@"PowerPoint.Show.8").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                    Registry.ClassesRoot.CreateSubKey(@"PowerPoint.SlideShow.8").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                    Registry.ClassesRoot.CreateSubKey(@"PowerPoint.Template.8").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                    Registry.ClassesRoot.CreateSubKey(@"PowerPoint.Presentation.12").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                    Registry.ClassesRoot.CreateSubKey(@"PowerPoint.Template.12").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                    Registry.ClassesRoot.CreateSubKey(@"PowerPoint.Show.12").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                    Registry.ClassesRoot.CreateSubKey(@"PowerPoint.TemplateMacroEnabled.12").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                    Registry.ClassesRoot.CreateSubKey(@"PowerPoint.ShowMacroEnabled.12").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                    Registry.ClassesRoot.CreateSubKey(@"PowerPoint.MacroEnabled.Presentation.12").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                    Registry.ClassesRoot.CreateSubKey(@"PowerPoint.MacroEnabled.Show.12").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                    Registry.ClassesRoot.CreateSubKey(@"PowerPoint.MacroEnabled.Template.12").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                    RegistryKey regKeyAppRoot = Registry.CurrentUser.CreateSubKey(@"Software\Microsoft\Windows\Shell\AttachmentExecute\{0002DF01-0000-0000-C000-000000000046}");
                    regKeyAppRoot.SetValue("PowerPoint.Show.8", 0, 0);
                    Environment.ExitCode = 0;
                }
                catch (Exception ex)
                {
                    Environment.ExitCode = 1;
                }
            }
            else if (officeVersionKey.Equals("PowerPoint.Application.11"))
            {
                //Office 2003
                try
                {
                    //writes registry setting to suppress powerpoint open/save dialog box when loading ppt to browser
                    Registry.ClassesRoot.CreateSubKey(@"PowerPoint.Show.8").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                    Registry.ClassesRoot.CreateSubKey(@"PowerPoint.SlideShow.8").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                    Registry.ClassesRoot.CreateSubKey(@"PowerPoint.Template.8").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                    Registry.ClassesRoot.CreateSubKey(@"PowerPoint.Presentation.12").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                    Registry.ClassesRoot.CreateSubKey(@"PowerPoint.Template.12").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                    Registry.ClassesRoot.CreateSubKey(@"PowerPoint.Show.12").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                    Registry.ClassesRoot.CreateSubKey(@"PowerPoint.TemplateMacroEnabled.12").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                    Registry.ClassesRoot.CreateSubKey(@"PowerPoint.ShowMacroEnabled.12").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                    Registry.ClassesRoot.CreateSubKey(@"PowerPoint.MacroEnabled.Presentation.12").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                    Registry.ClassesRoot.CreateSubKey(@"PowerPoint.MacroEnabled.Show.12").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                    Registry.ClassesRoot.CreateSubKey(@"PowerPoint.MacroEnabled.Template.12").SetValue("BrowserFlags", 0, RegistryValueKind.DWord);
                    RegistryKey regKeyAppRoot = Registry.CurrentUser.CreateSubKey(@"Software\Microsoft\Windows\Shell\AttachmentExecute\{0002DF01-0000-0000-C000-000000000046}");
                    regKeyAppRoot.SetValue("PowerPoint.Show.8", 0, 0);
                    Environment.ExitCode = 0;
                }
                catch (Exception ex)
                {
                    Environment.ExitCode = 1;
                }
            }
            else
            {
                try
                {
                    RegistryKey regKeyAppRoot = Registry.CurrentUser.CreateSubKey(@"Software\Microsoft\Windows\Shell\AttachmentExecute\{0002DF01-0000-0000-C000-000000000046}");
                    regKeyAppRoot.SetValue("PowerPoint.Show.8", 0, 0);
                    Environment.ExitCode = 0;
                }
                catch (Exception ex)
                {
                    Environment.ExitCode = 1;
                }
            }
        }
    }
}
