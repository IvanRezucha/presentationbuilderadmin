﻿ALTER DATABASE [$(DatabaseName)]
    ADD FILE (NAME = [ClientRegistration_Data], FILENAME = '$(DefaultDataPath)$(DatabaseName).MDF', SIZE = 3328 KB, MAXSIZE = UNLIMITED, FILEGROWTH = 10 %) TO FILEGROUP [PRIMARY];

