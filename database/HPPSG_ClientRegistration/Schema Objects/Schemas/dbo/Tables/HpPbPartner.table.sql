﻿CREATE TABLE [dbo].[HpPbPartner] (
    [registrationID] INT            IDENTITY (1, 1) NOT NULL,
    [firstName]      NVARCHAR (100) NOT NULL,
    [lastName]       NVARCHAR (100) NOT NULL,
    [email]          NVARCHAR (200) NOT NULL,
    [phone]          NVARCHAR (20)  NOT NULL,
    [isInSurvey]     BIT            NOT NULL,
    [installed]      INT            NOT NULL,
    [companyName]    NVARCHAR (100) NOT NULL,
    [title]          NVARCHAR (100) NOT NULL,
    [companyAddress] NVARCHAR (200) NOT NULL,
    [city]           NVARCHAR (100) NOT NULL,
    [state]          NVARCHAR (50)  NOT NULL,
    [zip]            NVARCHAR (10)  NOT NULL,
    [InstallDate]    DATETIME       NULL
);

