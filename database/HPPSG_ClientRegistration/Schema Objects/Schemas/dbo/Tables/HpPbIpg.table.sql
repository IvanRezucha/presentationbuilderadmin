﻿CREATE TABLE [dbo].[HpPbIpg] (
    [registrationID] INT            IDENTITY (1, 1) NOT NULL,
    [firstName]      NVARCHAR (100) NOT NULL,
    [lastName]       NVARCHAR (100) NOT NULL,
    [email]          NVARCHAR (200) NOT NULL,
    [phone]          NVARCHAR (20)  NOT NULL,
    [jobRole]        NVARCHAR (100) NOT NULL,
    [isInSurvey]     BIT            NOT NULL,
    [installed]      INT            NOT NULL,
    [InstallDate]    DATETIME       NULL
);

