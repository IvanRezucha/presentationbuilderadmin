﻿

CREATE PROCEDURE proc_LaserJetRegistrationSelect
    @registrationID int
AS

SELECT   
	registrationID,
	firstName,
	lastName,
	email,
	phone,
	jobRole,
	isInSurvey,
	installed
	
FROM HpPbLaserJet
WHERE registrationID = @registrationID