﻿CREATE PROCEDURE proc_PartnerRegistrationInsert
    @firstName nvarchar(100),
    @lastName nvarchar(100),
    @email nvarchar(200),   
    @phone nvarchar(20),
    @isInSurvey bit,
    @companyName nvarchar(100),
    @title nvarchar(100),
    @companyAddress nvarchar(200),
    @city nvarchar(100),
    @state nvarchar(50),
    @zip nvarchar(10)

AS

SET NOCOUNT ON

-- If this is the first time registrating then insert the user
IF ((SELECT COUNT(email) FROM HpPbPartner WHERE email = @email) = 0)
BEGIN
	INSERT INTO HpPbPartner(
		firstName,
		lastName,
		email,
		phone,
		isInSurvey,
		companyName,
		title,
		companyAddress,
		city,
		state,
		zip,
		installed)
	VALUES (
		@firstName,
		@lastName,
		@email,
		@phone,
		@isInSurvey,
		@companyName,
		@title,
		@companyAddress,
		@city,
		@state,
		@zip,
		1);
END
-- This user has registered before increment the number of times they installed
ELSE
BEGIN
	UPDATE HpPbPartner 
	SET installed = installed + 1 
	WHERE email = @email 
END