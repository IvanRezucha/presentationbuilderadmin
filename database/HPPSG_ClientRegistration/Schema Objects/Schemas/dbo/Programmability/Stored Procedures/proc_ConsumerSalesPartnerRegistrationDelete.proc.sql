﻿

CREATE PROCEDURE proc_ConsumerSalesPartnerRegistrationDelete
    @registrationID int
AS

DELETE FROM HpPbConsumerSalesPartner
WHERE registrationID = @registrationID