﻿CREATE PROCEDURE proc_ConsumerSalesPartnerRegistrationSelectAll

AS

SELECT   
	registrationID,
	firstName,
	lastName,
	email,
	phone,
	jobRole,
	isInSurvey,
	installed

FROM HpPbConsumerSalesPartner