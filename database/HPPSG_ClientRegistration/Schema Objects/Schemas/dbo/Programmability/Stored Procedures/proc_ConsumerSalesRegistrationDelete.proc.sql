﻿

CREATE PROCEDURE proc_ConsumerSalesRegistrationDelete
    @registrationID int
AS

DELETE FROM HpPbConsumerSales
WHERE registrationID = @registrationID
