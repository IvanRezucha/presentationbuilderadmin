﻿CREATE PROCEDURE proc_PartnerRegistrationUpdate
    @registrationID int,
    @firstName nvarchar(100),
    @lastName nvarchar(100),
    @email nvarchar(200),
    @phone nvarchar(20),
    @isInSurvey bit,
    @companyName nvarchar(100),
    @title nvarchar(100),
    @companyAddress nvarchar(200),
    @city nvarchar(100),
    @state nvarchar(50),
    @zip nvarchar(10),
    @installed int

AS

UPDATE HpPbPartner
SET
	firstName = @firstName,
	lastName = @lastName,
	email = @email,
	phone = @phone,
	isInSurvey = @isInSurvey,
	companyName = @companyName,
	title = @title,
	companyAddress = @companyAddress,
	city = @city,
	state = @state,
	zip = @zip,
	installed = @installed

WHERE registrationID = @registrationID