﻿CREATE PROCEDURE proc_ConsumerSalesPartnerRegistrationUpdate
    @registrationID int,
    @firstName nvarchar(100),
    @lastName nvarchar(100),
    @email nvarchar(200),
    @phone nvarchar(20),
    @jobRole nvarchar(100),
    @isInSurvey bit,
    @installed int

AS

UPDATE HpPbConsumerSalesPartner
SET
	firstName = @firstName,
	lastName = @lastName,
	email = @email,
	phone = @phone,
	jobRole = @jobRole,
	isInSurvey = @isInSurvey,
	installed = @installed

WHERE registrationID = @registrationID