﻿CREATE PROCEDURE proc_IpgRegistrationSelect
    @registrationID int
AS

SELECT   
	registrationID,
	firstName,
	lastName,
	email,
	phone,
	jobRole,
	isInSurvey,
	installed
	
FROM HpPbIpg
WHERE registrationID = @registrationID