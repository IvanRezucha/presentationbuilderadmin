﻿CREATE PROCEDURE proc_IpgRegistrationInsert
    @firstName nvarchar(100),
    @lastName nvarchar(100),
    @email nvarchar(200),   
    @phone nvarchar(20),
    @jobRole nvarchar(100),
    @isInSurvey bit
AS

SET NOCOUNT ON

-- If this is the first time registrating then insert the user
IF ((SELECT COUNT(email) FROM HpPbIpg WHERE email = @email) = 0)
BEGIN
	INSERT INTO HpPbIpg (
		firstName,
		lastName,
		email,
		phone,
		jobRole,
		isInSurvey,
		installed)
	VALUES (
		@firstName,
		@lastName,
		@email,
		@phone,
		@jobRole,
		@isInSurvey,		
		1);
END
-- This user has registered before increment the number of times they installed
ELSE
BEGIN
	UPDATE HpPbIpg 
	SET installed = installed + 1 
	WHERE email = @email 
END