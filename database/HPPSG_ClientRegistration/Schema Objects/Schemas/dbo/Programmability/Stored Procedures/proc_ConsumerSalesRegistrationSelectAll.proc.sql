﻿

CREATE PROCEDURE proc_ConsumerSalesRegistrationSelectAll

AS

SELECT   
	registrationID,
	firstName,
	lastName,
	email,
	phone,
	jobRole,
	isInSurvey,
	installed

FROM HpPbConsumerSales
