﻿

CREATE PROCEDURE proc_ConsumerSalesRegistrationSelect
    @registrationID int
AS

SELECT   
	registrationID,
	firstName,
	lastName,
	email,
	phone,
	jobRole,
	isInSurvey,
	installed
	
FROM HpPbConsumerSales
WHERE registrationID = @registrationID
