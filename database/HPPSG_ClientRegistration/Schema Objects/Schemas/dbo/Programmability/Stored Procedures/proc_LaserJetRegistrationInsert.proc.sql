﻿

CREATE PROCEDURE proc_LaserJetRegistrationInsert
    @firstName nvarchar(100),
    @lastName nvarchar(100),
    @email nvarchar(200),   
    @phone nvarchar(20),
    @jobRole nvarchar(100),
    @isInSurvey bit
AS

SET NOCOUNT ON

-- If this is the first time registrating then insert the user
IF ((SELECT COUNT(email) FROM HpPbLaserJet WHERE email = @email) = 0)
BEGIN
	INSERT INTO HpPbLaserJet (
		firstName,
		lastName,
		email,
		phone,
		jobRole,
		isInSurvey,
		installed)
	VALUES (
		@firstName,
		@lastName,
		@email,
		@phone,
		@jobRole,
		@isInSurvey,		
		1);
END
-- This user has registered before increment the number of times they installed
ELSE
BEGIN
	UPDATE HpPbLaserJet 
	SET installed = installed + 1 
	WHERE email = @email 
END