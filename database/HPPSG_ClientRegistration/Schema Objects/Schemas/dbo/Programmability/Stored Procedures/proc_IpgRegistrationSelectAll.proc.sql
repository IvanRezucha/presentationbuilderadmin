﻿
CREATE PROCEDURE proc_IpgRegistrationSelectAll

AS

SELECT   
	registrationID,
	firstName,
	lastName,
	email,
	phone,
	jobRole,
	isInSurvey,
	installed

FROM HpPbIpg