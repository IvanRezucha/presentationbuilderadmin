﻿

CREATE PROCEDURE proc_LaserJetRegistrationDelete
    @registrationID int
AS

DELETE FROM HpPbLaserJet
WHERE registrationID = @registrationID