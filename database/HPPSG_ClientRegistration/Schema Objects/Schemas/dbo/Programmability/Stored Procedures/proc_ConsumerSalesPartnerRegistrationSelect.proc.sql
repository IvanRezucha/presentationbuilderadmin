﻿CREATE PROCEDURE proc_ConsumerSalesPartnerRegistrationSelect
    @registrationID int
AS

SELECT   
	registrationID,
	firstName,
	lastName,
	email,
	phone,
	jobRole,
	isInSurvey,
	installed
	
FROM HpPbConsumerSalesPartner
WHERE registrationID = @registrationID