﻿CREATE PROCEDURE proc_PartnerRegistrationDelete
    @registrationID int
AS

DELETE FROM HpPbPartner
WHERE registrationID = @registrationID