﻿

CREATE PROCEDURE proc_LaserJetRegistrationSelectAll

AS

SELECT   
	registrationID,
	firstName,
	lastName,
	email,
	phone,
	jobRole,
	isInSurvey,
	installed

FROM HpPbLaserJet