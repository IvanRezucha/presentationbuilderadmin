﻿ALTER DATABASE [$(DatabaseName)]
    ADD FILE (NAME = [IpgShowcase_Data], FILENAME = '$(DefaultDataPath)$(DatabaseName).mdf', SIZE = 42048 KB, MAXSIZE = UNLIMITED, FILEGROWTH = 10 %) TO FILEGROUP [PRIMARY];

