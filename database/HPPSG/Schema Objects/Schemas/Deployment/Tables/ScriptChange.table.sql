﻿CREATE TABLE [Deployment].[ScriptChange]
(
	ID int IDENTITY (1,1) NOT NULL, 
	GUID nchar(36) not null,
	Created datetime not null
)
