﻿
CREATE VIEW [dbo].[vw_ReportTotalUsersInstalled] 
AS
	--total users who have installed: anyone who has a "login date" field value
	select count(*) [Total Users Verified Installed]
	from userprofile
	where lastlogindate is not null
