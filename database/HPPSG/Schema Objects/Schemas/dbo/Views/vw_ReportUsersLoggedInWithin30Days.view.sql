﻿
CREATE VIEW [dbo].[vw_ReportUsersLoggedInWithin30Days] 
AS
	--contrast users who have logged in within last 30 days with users who have not
	select count(*) [Users Logged In Within 30 Days]
	from userprofile
	where lastlogindate is not null and datediff(dd, getdate(), lastlogindate) < 30
