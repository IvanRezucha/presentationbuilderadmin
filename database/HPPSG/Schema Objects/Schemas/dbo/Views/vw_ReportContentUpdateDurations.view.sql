﻿
CREATE VIEW [dbo].[vw_ReportContentUpdateDurations] 
AS
	--content update max
	select Country
		, max(lastcontentupdateduration) [Max Last Content Update Duration]
		, avg(lastcontentupdateduration) [Average Last Content Update Duration]
	from userprofile
	where lastcontentupdateduration is not null
	group by country
	with rollup
