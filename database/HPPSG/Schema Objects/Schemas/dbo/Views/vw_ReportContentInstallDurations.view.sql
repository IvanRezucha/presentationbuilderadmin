﻿
CREATE VIEW [dbo].[vw_ReportContentInstallDurations] 
AS
	--content install max
	select Country
		, max(contentinstallduration) [Max Content Install Duration]
		, avg(contentinstallduration) [Average Content Install Duration]
	from userprofile
	where contentinstallduration is not null
	group by country
	with rollup
