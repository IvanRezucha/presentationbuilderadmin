﻿
CREATE VIEW [dbo].[vw_ReportRegionLanguageUsage] 
AS
	--people subscribed to regions
	select top 100 percent r.regionname Region
		, l.longname Language
		, count(distinct userid) [User Count]
	from userregionlanguage url
		join regionlanguage rl on rl.regionlanguageid = url.regionlanguageid
		join region r on r.regionid = rl.regionid
		join language l on l.languageid = rl.languageid
	group by r.regionname
		, l.longname
	order by count(distinct userid) desc
