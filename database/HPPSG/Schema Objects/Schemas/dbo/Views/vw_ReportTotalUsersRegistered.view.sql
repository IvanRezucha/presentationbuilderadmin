﻿
CREATE VIEW [dbo].[vw_ReportTotalUsersRegistered] 
AS
	--total users: count all users in table
	select count(*) [Total Users Registered]
	from userprofile
