﻿CREATE TABLE [dbo].[Events] (
    [EventID]    BIGINT        IDENTITY (1, 1) NOT NULL,
    [SlideID]    INT           NOT NULL,
    [Session]    VARCHAR (36)  NOT NULL,
    [TypeID]     INT           NOT NULL,
    [CategoryID] INT           NULL,
    [Tag]        VARCHAR (255) NULL,
    [EventDate]  DATETIME      NOT NULL
);

