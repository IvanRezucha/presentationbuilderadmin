﻿CREATE TABLE [dbo].[UserLoginHistory] (
    [UserID]    INT           NULL,
    [LoginDate] DATETIME      NULL,
    [LoginApp]  NVARCHAR (50) NULL
);

