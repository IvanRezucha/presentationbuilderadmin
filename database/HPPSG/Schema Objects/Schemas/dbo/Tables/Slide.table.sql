﻿CREATE TABLE [dbo].[Slide] (
    [SlideID]      INT            IDENTITY (1, 1) NOT NULL,
    [Title]        NVARCHAR (500) NULL,
    [FileName]     NVARCHAR (100) NULL,
    [DateModified] DATETIME       NULL,
    [DateRemoved]  DATETIME       NULL
);

