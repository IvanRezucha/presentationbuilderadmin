﻿CREATE TABLE [dbo].[CategoryType] (
    [CategoryTypeID] INT            IDENTITY (1, 1) NOT NULL,
    [CategoryType]   NVARCHAR (100) NULL,
    [Icon]           NVARCHAR (50)  NULL
);

