﻿CREATE TABLE [dbo].[Announcement] (
    [AnnouncementID] INT            IDENTITY (1, 1) NOT NULL,
    [Title]          NVARCHAR (200) NOT NULL,
    [Body]           TEXT           NOT NULL,
    [PubDate]        DATETIME       NOT NULL,
    [RegionId]       INT            NOT NULL,
    [PreviewGuid]    NVARCHAR (50)  NULL,
    [IsPreview]      BIT            NULL
);

