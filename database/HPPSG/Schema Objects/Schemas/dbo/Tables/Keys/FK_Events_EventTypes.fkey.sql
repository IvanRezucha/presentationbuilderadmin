﻿ALTER TABLE [dbo].[Events]
    ADD CONSTRAINT [FK_Events_EventTypes] FOREIGN KEY ([TypeID]) REFERENCES [dbo].[EventTypes] ([TypeID]) ON DELETE NO ACTION ON UPDATE NO ACTION;

