﻿ALTER TABLE [dbo].[SupportFile]
    ADD CONSTRAINT [PK_SupportFile] PRIMARY KEY CLUSTERED ([SupportFileID] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);

