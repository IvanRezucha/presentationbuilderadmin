﻿ALTER TABLE [dbo].[SupportFileRegionLanguage]
    ADD CONSTRAINT [PK_SupportFileRegionLanguage_1] PRIMARY KEY CLUSTERED ([SupportFileID] ASC, [RegionLanguageID] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);

