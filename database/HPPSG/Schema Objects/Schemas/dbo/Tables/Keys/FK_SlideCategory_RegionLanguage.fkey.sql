﻿ALTER TABLE [dbo].[SlideCategory]
    ADD CONSTRAINT [FK_SlideCategory_RegionLanguage] FOREIGN KEY ([RegionLanguageID]) REFERENCES [dbo].[RegionLanguage] ([RegionLanguageID]) ON DELETE CASCADE ON UPDATE CASCADE;

