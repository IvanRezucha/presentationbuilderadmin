﻿ALTER TABLE [dbo].[SupportFileRegionLanguage]
    ADD CONSTRAINT [FK_SupportFileRegionLanguage_RegionLanguage] FOREIGN KEY ([RegionLanguageID]) REFERENCES [dbo].[RegionLanguage] ([RegionLanguageID]) ON DELETE CASCADE ON UPDATE CASCADE;

