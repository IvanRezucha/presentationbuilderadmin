﻿ALTER TABLE [dbo].[SlideCategory]
    ADD CONSTRAINT [FK_SlideCategory_Slide] FOREIGN KEY ([SlideID]) REFERENCES [dbo].[Slide] ([SlideID]) ON DELETE CASCADE ON UPDATE CASCADE;

