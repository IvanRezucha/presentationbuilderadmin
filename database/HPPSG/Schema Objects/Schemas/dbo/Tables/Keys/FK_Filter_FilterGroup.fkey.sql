﻿ALTER TABLE [dbo].[Filter]
    ADD CONSTRAINT [FK_Filter_FilterGroup] FOREIGN KEY ([FilterGroupID]) REFERENCES [dbo].[FilterGroup] ([FilterGroupID]) ON DELETE CASCADE ON UPDATE CASCADE;

