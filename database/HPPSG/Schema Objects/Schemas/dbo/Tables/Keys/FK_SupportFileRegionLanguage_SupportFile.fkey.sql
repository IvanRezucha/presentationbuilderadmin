﻿ALTER TABLE [dbo].[SupportFileRegionLanguage]
    ADD CONSTRAINT [FK_SupportFileRegionLanguage_SupportFile] FOREIGN KEY ([SupportFileID]) REFERENCES [dbo].[SupportFile] ([SupportFileID]) ON DELETE CASCADE ON UPDATE CASCADE;

