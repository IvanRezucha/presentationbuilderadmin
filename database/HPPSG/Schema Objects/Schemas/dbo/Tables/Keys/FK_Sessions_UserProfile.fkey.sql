﻿ALTER TABLE [dbo].[Sessions]
    ADD CONSTRAINT [FK_Sessions_UserProfile] FOREIGN KEY ([UserID]) REFERENCES [dbo].[UserProfile] ([UserID]) ON DELETE NO ACTION ON UPDATE NO ACTION;

