﻿ALTER TABLE [dbo].[SlideRegionLanguage]
    ADD CONSTRAINT [FK_SlideRegionLanguage_RegionLanguage] FOREIGN KEY ([RegionLanguageID]) REFERENCES [dbo].[RegionLanguage] ([RegionLanguageID]) ON DELETE CASCADE ON UPDATE CASCADE;

