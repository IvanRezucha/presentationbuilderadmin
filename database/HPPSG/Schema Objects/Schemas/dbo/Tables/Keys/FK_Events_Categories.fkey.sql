﻿ALTER TABLE [dbo].[Events]
    ADD CONSTRAINT [FK_Events_Categories] FOREIGN KEY ([CategoryID]) REFERENCES [dbo].[Category] ([CategoryID]) ON DELETE NO ACTION ON UPDATE NO ACTION;

