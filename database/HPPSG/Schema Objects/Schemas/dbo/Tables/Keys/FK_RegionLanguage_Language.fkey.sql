﻿ALTER TABLE [dbo].[RegionLanguage]
    ADD CONSTRAINT [FK_RegionLanguage_Language] FOREIGN KEY ([LanguageID]) REFERENCES [dbo].[Language] ([LanguageID]) ON DELETE NO ACTION ON UPDATE NO ACTION;

