﻿ALTER TABLE [dbo].[SlideFilter]
    ADD CONSTRAINT [PK_SlideFilter] PRIMARY KEY CLUSTERED ([FilterID] ASC, [SlideID] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);

