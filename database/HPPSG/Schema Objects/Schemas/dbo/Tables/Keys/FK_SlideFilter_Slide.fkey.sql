﻿ALTER TABLE [dbo].[SlideFilter]
    ADD CONSTRAINT [FK_SlideFilter_Slide] FOREIGN KEY ([SlideID]) REFERENCES [dbo].[Slide] ([SlideID]) ON DELETE CASCADE ON UPDATE CASCADE;

