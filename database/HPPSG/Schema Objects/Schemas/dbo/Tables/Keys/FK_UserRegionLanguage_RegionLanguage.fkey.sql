﻿ALTER TABLE [dbo].[UserRegionLanguage]
    ADD CONSTRAINT [FK_UserRegionLanguage_RegionLanguage] FOREIGN KEY ([RegionLanguageID]) REFERENCES [dbo].[RegionLanguage] ([RegionLanguageID]) ON DELETE CASCADE ON UPDATE CASCADE;

