﻿ALTER TABLE [dbo].[SlideCategory]
    ADD CONSTRAINT [FK_SlideCategory_Category] FOREIGN KEY ([CategoryID]) REFERENCES [dbo].[Category] ([CategoryID]) ON DELETE CASCADE ON UPDATE CASCADE;

