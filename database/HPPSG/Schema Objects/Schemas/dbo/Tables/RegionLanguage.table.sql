﻿CREATE TABLE [dbo].[RegionLanguage] (
    [RegionLanguageID] INT IDENTITY (1, 1) NOT NULL,
    [RegionID]         INT NOT NULL,
    [LanguageID]       INT NOT NULL
);

