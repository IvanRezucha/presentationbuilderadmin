﻿CREATE TABLE [dbo].[SlideCategory] (
    [SlideID]          INT NOT NULL,
    [CategoryID]       INT NOT NULL,
    [RegionLanguageID] INT NOT NULL,
    [SortOrder]        INT NOT NULL
);

