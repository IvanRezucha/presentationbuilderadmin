﻿CREATE TABLE [dbo].[Region] (
    [RegionID]        INT            IDENTITY (1, 1) NOT NULL,
    [RegionName]      NVARCHAR (100) NULL,
    [IsShownOnClient] BIT            NULL,
    [IsClientDefault] BIT            NULL,
    [IsAdminDefault]  BIT            NULL,
    [SortOrder]       INT            NOT NULL,
    [ShortName]       NVARCHAR (50)  NULL
);

