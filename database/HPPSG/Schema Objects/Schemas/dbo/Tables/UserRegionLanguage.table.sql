﻿CREATE TABLE [dbo].[UserRegionLanguage] (
    [UserRegionLanguageID] INT IDENTITY (1, 1) NOT NULL,
    [UserID]               INT NOT NULL,
    [RegionLanguageID]     INT NOT NULL
);

