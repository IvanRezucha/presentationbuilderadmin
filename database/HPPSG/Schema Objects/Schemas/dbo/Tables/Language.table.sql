﻿CREATE TABLE [dbo].[Language] (
    [LanguageID] INT           IDENTITY (1, 1) NOT NULL,
    [LongName]   NVARCHAR (50) NOT NULL,
    [ShortName]  NVARCHAR (50) NOT NULL,
    [SortOrder]  INT           NOT NULL
);

