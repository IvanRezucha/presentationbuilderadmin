﻿CREATE TABLE [dbo].[Category] (
    [CategoryID]           INT            IDENTITY (1, 1) NOT NULL,
    [CategoryTypeID]       INT            NULL,
    [Category]             NVARCHAR (250) NOT NULL,
    [StringSet]            NVARCHAR (250) COLLATE SQL_Latin1_General_CP850_BIN2 NULL,
    [Depth]                INT            NULL,
    [ParentCategoryID]     INT            NULL,
    [IsProtected]          BIT            NOT NULL,
    [SystemName]           NVARCHAR (50)  NULL,
    [IsNew]                BIT            NOT NULL,
    [Icon]                 NVARCHAR (100) NULL,
    [ShowDescendantSlides] BIT            NOT NULL,
    [Archived]             BIT            NOT NULL
);

