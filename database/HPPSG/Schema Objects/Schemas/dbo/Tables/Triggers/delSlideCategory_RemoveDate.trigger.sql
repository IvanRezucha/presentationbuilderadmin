﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER delSlideCategory_RemoveDate 
   ON  SlideCategory 
   AFTER DELETE, INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--Set the count variable to use
	DECLARE @cnt int
	SET @cnt = 0
	
	--Set the slide ID
	DECLARE @sl int
	
	IF UPDATE(SlideID)
	BEGIN
		SELECT @sl = SlideID from inserted
	END
	ELSE
	BEGIN
		SELECT @sl = SlideID from deleted
	END
	
	
    
    SELECT @cnt = COUNT(*)
    FROM SlideCategory
    WHERE SlideID = @sl
    
    IF @cnt > 0
    BEGIN
		UPDATE Slide
		SET DateRemoved = null
		WHERE SlideID = @sl
    END
    ELSE
    BEGIN
		UPDATE Slide
		SET DateRemoved = getdate()
		WHERE SlideID = @sl
    END

END
