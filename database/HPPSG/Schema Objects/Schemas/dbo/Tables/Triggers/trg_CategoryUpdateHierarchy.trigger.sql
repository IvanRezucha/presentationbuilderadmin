﻿/*
Description: trigger to update hierarchical information for recursive tables when a record is updated or inserted. calls
a proc which updates the node and which also may call a separate proc to udpate child nodes
By: fred herrmann, kurt herrmann, wirestone llc, 2004
Intended use: system updates.  please see database documentation for full explanation.
*/
CREATE  TRIGGER trg_CategoryUpdateHierarchy ON dbo.Category
FOR INSERT, UPDATE
AS

	DECLARE IterateNewRecords CURSOR LOCAL STATIC FOR
		SELECT CategoryID FROM INSERTED

		DECLARE @CategoryID Int
		OPEN IterateNewRecords

		FETCH NEXT FROM IterateNewRecords INTO @CategoryID

		WHILE (@@FETCH_STATUS = 0)
		BEGIN
			EXEC proc_CategoryTriggerUpdateSelf @CategoryID

			FETCH NEXT FROM IterateNewRecords INTO @CategoryID
		END
	CLOSE IterateNewRecords
	DEALLOCATE IterateNewRecords
