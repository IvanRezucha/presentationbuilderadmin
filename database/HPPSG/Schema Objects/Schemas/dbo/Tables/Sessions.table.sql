﻿CREATE TABLE [dbo].[Sessions] (
    [Session]   VARCHAR (36) NOT NULL,
    [UserID]    INT          NOT NULL,
    [StartDate] DATETIME     NOT NULL,
    [EndDate]   DATETIME     NULL
);

