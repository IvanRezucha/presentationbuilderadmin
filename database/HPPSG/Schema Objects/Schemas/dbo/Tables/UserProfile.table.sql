﻿CREATE TABLE [dbo].[UserProfile] (
    [UserID]                    INT            IDENTITY (1, 1) NOT NULL,
    [LastName]                  NVARCHAR (50)  NOT NULL,
    [FirstName]                 NVARCHAR (50)  NOT NULL,
    [Email]                     NVARCHAR (100) NOT NULL,
    [Phone]                     NVARCHAR (50)  NOT NULL,
    [JobTitle]                  NVARCHAR (100) NOT NULL,
    [IsInSurvey]                BIT            NOT NULL,
    [InstallDate]               DATETIME       NOT NULL,
    [CompanyName]               NVARCHAR (100) NULL,
    [CompanySize]               INT            NULL,
    [Country]                   NVARCHAR (50)  NULL,
    [ReferenceSource]           NVARCHAR (500) NULL,
    [LastLoginDate]             DATETIME       NULL,
    [ContentInstallDuration]    INT            NULL,
    [LastContentUpdateDuration] INT            NULL,
    [Password]                  NVARCHAR (50)  NULL,
    [TemporaryPasswordInUse]    BIT            DEFAULT (0) NOT NULL,
    [RegionId]                  INT            DEFAULT (0) NOT NULL
);

