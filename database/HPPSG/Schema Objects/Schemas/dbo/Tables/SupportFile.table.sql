﻿CREATE TABLE [dbo].[SupportFile] (
    [SupportFileID] INT            IDENTITY (1, 1) NOT NULL,
    [FileName]      NVARCHAR (100) NOT NULL,
    [ModifyDate]    DATETIME       NOT NULL
);

