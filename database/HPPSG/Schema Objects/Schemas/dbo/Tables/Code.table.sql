﻿CREATE TABLE [dbo].[Code] (
    [CodeKey]  INT       NOT NULL,
    [CodeChar] NCHAR (1) COLLATE SQL_Latin1_General_CP850_BIN2 NOT NULL
);

