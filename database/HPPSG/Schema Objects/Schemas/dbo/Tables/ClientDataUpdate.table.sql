﻿CREATE TABLE [dbo].[ClientDataUpdate] (
    [ClientDataUpdateID] INT            IDENTITY (1, 1) NOT NULL,
    [UpdateDate]         DATETIME       NOT NULL,
    [DataVersion]        INT            NOT NULL,
    [AppVersion]         NVARCHAR (50)  NOT NULL,
    [OSVersion]          NVARCHAR (200) NULL
);

