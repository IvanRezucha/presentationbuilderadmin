﻿CREATE TABLE [dbo].[Filter] (
    [FilterID]      INT            IDENTITY (1, 1) NOT NULL,
    [FilterName]    NVARCHAR (250) NOT NULL,
    [FilterGroupID] INT            NOT NULL
);

