﻿CREATE TABLE [dbo].[EventTypes] (
    [TypeID] INT          IDENTITY (1, 1) NOT NULL,
    [Name]   VARCHAR (20) NOT NULL
);

