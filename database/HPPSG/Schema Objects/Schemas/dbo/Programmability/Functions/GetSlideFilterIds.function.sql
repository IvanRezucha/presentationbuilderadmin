﻿
CREATE FUNCTION [dbo].[GetSlideFilterIds]
(
	@SlideId int
)
RETURNS nvarchar(2000)
AS
BEGIN
	DECLARE @FilterIDs nvarchar(2000)

	SELECT @FilterIDs = COALESCE(@FilterIDs + ',', '') + CAST(SlideFilter.FilterID AS nvarchar(5)) 
	FROM SlideFilter 
	WHERE SlideFilter.SlideID = @SlideId

	RETURN @FilterIds
END

