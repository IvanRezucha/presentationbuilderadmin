﻿
/*
Description: select an entire group of siblings which are children of a given node
By: kurt herrmann, wirestone llc, 2004
Input: category to select children of
Output: category "node list"
Intended use: any operations involving peer groups
*/
CREATE  FUNCTION [dbo].[func_CategorySelectPeerGroup] 
	( 
		@CategoryID int 
	)   
RETURNS  
	@PeerTable TABLE  
	( 
		CategoryID int, 
		Category nvarchar(250), 
		StringSet nvarchar(250) COLLATE SQL_Latin1_General_CP850_BIN2, 
		Depth int, 
		ParentCategoryID int 
	)  
AS   
BEGIN  
	INSERT INTO @PeerTable (CategoryID, Category, StringSet, Depth, ParentCategoryID) 
		SELECT CategoryID, Category, StringSet, Depth, ParentCategoryID  
		FROM Category
		WHERE ParentCategoryID = (SELECT ParentCategoryID FROM Category WHERE CategoryID = @CategoryID)
			AND Archived = 0 
		ORDER BY StringSet ASC 
	 
	RETURN  
END 

