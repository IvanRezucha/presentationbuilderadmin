﻿


/*
Description: function to select the record for the temporary cache node location
By: kurt herrmann, wirestone llc, 2004
Input: none
Output: single record emulating the category cache node
Intended use: wrapper for single point of access in database.  should always be used to get the cache node instead of
querying for it by any other name, depth, etc.
*/
CREATE FUNCTION func_CategorySelectCacheNode 
	( 
		 
	)   
RETURNS  
	@CacheNodeTable TABLE  
	( 
		CategoryID int, Category nvarchar(250), StringSet nvarchar(250) COLLATE SQL_Latin1_General_CP850_BIN2, Depth int, ParentCategoryID int 
	)  
AS   
BEGIN  
	INSERT INTO @CacheNodeTable (CategoryID, Category, StringSet, Depth, ParentCategoryID) 
		SELECT TOP 1 CategoryID, Category, StringSet, Depth, ParentCategoryID  
		FROM Category 
		WHERE Depth = 2 
		ORDER BY StringSet DESC 
	 
	RETURN  
END 
 








