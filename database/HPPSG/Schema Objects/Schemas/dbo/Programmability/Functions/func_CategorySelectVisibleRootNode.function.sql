﻿


/*
Description: select the root node into which all "active" nodes are placed
By: kurt herrmann, wirestone llc, 2004
Input: nothing
Output: single category node representing the usable root (not the real root)
Intended use: single wrapper function for single point of access to visible root.  always use this
function for getting the root -- never query for it by name or anything else
*/
CREATE FUNCTION func_CategorySelectVisibleRootNode 
	( 
		 
	)   
RETURNS  
	@VisibleRootNodeTable TABLE  
	( 
		CategoryID int, Category nvarchar(250), StringSet nvarchar(250) COLLATE SQL_Latin1_General_CP850_BIN2, Depth int, ParentCategoryID int 
	)  
AS   
BEGIN  
	INSERT INTO @VisibleRootNodeTable (CategoryID, Category, StringSet, Depth, ParentCategoryID) 
		SELECT TOP 1 CategoryID, Category, StringSet, Depth, ParentCategoryID  
		FROM Category 
		WHERE Depth = 2 
		ORDER BY StringSet ASC 
	 
	RETURN  
END 



