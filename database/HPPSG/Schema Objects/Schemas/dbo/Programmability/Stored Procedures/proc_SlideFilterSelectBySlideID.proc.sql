﻿
CREATE PROCEDURE dbo.proc_SlideFilterSelectBySlideID
(
	@SlideID int
)
AS
	SET NOCOUNT ON;
SELECT     FilterID, SlideID
FROM         SlideFilter
WHERE     (SlideID = @SlideID)
