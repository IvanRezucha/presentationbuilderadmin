﻿

/*
Description: procedure to update the parent id for any given node 
we want to update the entire peer group so that the string set gets reset for all nodes
so we will get the peer group, loop till we find the one we are supposed to update,
move it and the remaining to the cache location,
move the remaining back to the parent, and then the desired one to the resting location.
By: kurt herrmann, wirestone llc, 2004
Input: category id to move, new parent category id to move it to
Output: void
Intended use: administration.  always use this proc for updating parent.
*/
CREATE  PROCEDURE proc_CategoryUpdateParent 
	( 
	@CategoryID int,   
	@ParentCategoryID int 
	)  
AS 

IF @ParentCategoryID = -1
BEGIN
	 --if -1, this needs to be added to root
	--so select the visible root for the parent
	SET @ParentCategoryID = (SELECT CategoryID FROM func_CategorySelectVisibleRootNode())
END

	DECLARE @LoopCategoryID int 
	DECLARE @LoopParentCategoryID int 
 
	--get the node's peers into a cursor 
	DECLARE PeerCatCursor CURSOR 
		FOR SELECT CategoryID, ParentCategoryID FROM func_CategorySelectPeerGroup(@CategoryID) 
	OPEN PeerCatCursor 
	FETCH NEXT FROM PeerCatCursor  
		INTO @LoopCategoryID, @LoopParentCategoryID  
	--and keep a flag for when we should start moving nodes around 
	DECLARE @PeerFlag bit 
	SET @PeerFlag = 0 
	--we also need a temp table to keep track of peers we move 
	DECLARE @PeerTable TABLE (CategoryID int)  
	 
	WHILE (@@FETCH_STATUS = 0) --loop while good records are returned 
	BEGIN 
		IF (@PeerFlag = 1) 
		BEGIN 
			--put node id in a temp table so we can move it back later 
			INSERT INTO @PeerTable (CategoryID) VALUES (@LoopCategoryID) 
			--move the node to the cache 
			EXEC proc_CategoryMoveToCache @LoopCategoryID 
		END 
		 
		--if the loop id matches the desired node id, flip the flag. 
		--from here on out we will start moving nodes 
		IF (@LoopCategoryID = @CategoryID) 
		BEGIN 
			--move the node to the cache 
			EXEC proc_CategoryMoveToCache @LoopCategoryID 
			--flip the flag 
			SET @PeerFlag = 1  
		END 
		 
		FETCH NEXT FROM PeerCatCursor 
			INTO @LoopCategoryID, @LoopParentCategoryID  
	END 
	 
	CLOSE PeerCatCursor 
	DEALLOCATE PeerCatCursor 
	 
	--now restore all the nodes we moved to the cache before 
	--this is bizarre.  we can only go record by record with a cursor.  so we will create a 
	--cursor on our temp table variable 
	DECLARE PeerCatCursor CURSOR 
		FOR SELECT CategoryID FROM @PeerTable 
	OPEN PeerCatCursor 
	FETCH NEXT FROM PeerCatCursor  
		INTO @LoopCategoryID 
	WHILE (@@FETCH_STATUS = 0) --loop while good records are returned 
	BEGIN 
		UPDATE Category SET ParentCategoryID = @LoopParentCategoryID WHERE CategoryID = @LoopCategoryID
		 
		FETCH NEXT FROM PeerCatCursor 
			INTO @LoopCategoryID 
	END 
	CLOSE PeerCatCursor 
	DEALLOCATE PeerCatCursor

	--now move desired node to resting location
	UPDATE Category SET ParentCategoryID = @ParentCategoryID WHERE CategoryID = @CategoryID


