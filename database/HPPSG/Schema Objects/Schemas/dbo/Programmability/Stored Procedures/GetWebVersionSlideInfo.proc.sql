﻿
CREATE PROCEDURE [dbo].[GetWebVersionSlideInfo]
(
	@RegionLanguageId int
)
AS
SELECT	SlideCategory.SlideID, SlideCategory.CategoryID, Slide.Title AS label, Slide.FileName, 
		dbo.GetSlideFilterIds(SlideCategory.SlideID) AS filterIDs
FROM SlideCategory 
INNER JOIN Slide ON SlideCategory.SlideID = Slide.SlideID
WHERE SlideCategory.RegionLanguageID = @RegionLanguageId
ORDER BY SortOrder

