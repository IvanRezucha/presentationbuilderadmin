﻿
CREATE PROCEDURE dbo.proc_FilterGroupDelete
(
	@Original_FilterGroupID int,
	@Original_FilterGroup nvarchar(250)
)
AS
	SET NOCOUNT OFF;
DELETE FROM [dbo].[FilterGroup] WHERE (([FilterGroupID] = @Original_FilterGroupID) AND ([FilterGroup] = @Original_FilterGroup))
