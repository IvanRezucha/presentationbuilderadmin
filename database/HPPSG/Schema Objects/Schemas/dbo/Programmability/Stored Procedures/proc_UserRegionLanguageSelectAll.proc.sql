﻿CREATE PROCEDURE [proc_UserRegionLanguageSelectAll] 
AS
	SET NOCOUNT ON;
	SELECT 
		UserRegionLanguageID
		, UserID
		, RegionLanguageID
	FROM UserRegionLanguage
