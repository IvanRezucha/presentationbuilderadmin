﻿




Create   PROCEDURE proc_CategoryUpdate4XmlRead
    @CategoryID int, 
    @CategoryTypeID int,
    @Category nvarchar(250),
    @Description nvarchar(4000),
	@IsNew bit = 0
AS 
 
UPDATE [Category] 
SET 
	Category= @Category,
	Description = @Description,
	CategoryTypeID = @CategoryTypeID,
	IsNew = @IsNew
WHERE CategoryID = @CategoryID





