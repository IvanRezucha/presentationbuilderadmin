﻿

-- Updates a record in the 'SlideCategory' table.
CREATE PROCEDURE proc_SlideCategoryUpdate
	@SortOrder Int,
	@SlideID Int,
	@CategoryID Int
AS
UPDATE SlideCategory 
SET
	SortOrder = @SortOrder
WHERE
	SlideID = @SlideID AND
	CategoryID = @CategoryID


