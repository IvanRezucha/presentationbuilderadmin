﻿

CREATE PROCEDURE proc_SlideCategoryDeleteByPrimaryKey
(
	@SlideID int,
	@CategoryID int
)
AS
DELETE 	SlideCategory
WHERE 	SlideID = @SlideID
AND 		CategoryID = @CategoryID


