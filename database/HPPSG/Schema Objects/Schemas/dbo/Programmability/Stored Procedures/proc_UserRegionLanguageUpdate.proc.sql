﻿CREATE PROCEDURE [proc_UserRegionLanguageUpdate]
(
	@UserRegionLanguageID int
	, @UserID int
	, @RegionLanguageID int
	, @Original_UserRegionLanguageID int
)
AS
	SET NOCOUNT OFF;
	UPDATE UserRegionLanguage
	SET 
		UserID = @UserID
		, RegionLanguageID = @RegionLanguageID
	WHERE UserRegionLanguageID = @Original_UserRegionLanguageID;
	SELECT 
		UserRegionLanguageID
		, UserID
		, RegionLanguageID
	FROM UserRegionLanguage
	WHERE UserRegionLanguageID = @UserRegionLanguageID
