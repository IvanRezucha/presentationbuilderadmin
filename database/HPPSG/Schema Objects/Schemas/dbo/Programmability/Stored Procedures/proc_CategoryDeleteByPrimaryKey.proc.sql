﻿

/*
Description: deletes a node and all its kids from the hierarchy
By: kurt herrmann, wirestone llc, 2004
Input: node id to delete
Output: none
Intended use: administration.  always use this proc to delete a node and/or its children 
*/
CREATE   PROCEDURE [dbo].[proc_CategoryDeleteByPrimaryKey] 
    @CategoryID int 
AS 

	DELETE 
		FROM SlideCategory 
		WHERE CategoryID IN (SELECT CategoryID 
					FROM Category 
					WHERE StringSet LIKE (SELECT StringSet 
								FROM Category 
								WHERE CategoryID = @CategoryID) 
							+ '%')
	
	DECLARE @LoopCategoryID int 
	DECLARE @LoopParentCategoryID int 
 
	--get the node's peers into a cursor 
	DECLARE PeerDelCursor CURSOR 
		FOR SELECT CategoryID, ParentCategoryID FROM func_CategorySelectPeerGroup(@CategoryID) 
	OPEN PeerDelCursor 
	FETCH NEXT FROM PeerDelCursor  
		INTO @LoopCategoryID, @LoopParentCategoryID  
	--and keep a flag for when we should start moving nodes around 
	DECLARE @PeerFlag bit 
	SET @PeerFlag = 0 
	--we also need a temp table to keep track of peers we move 
	DECLARE @PeerTable TABLE (CategoryID int)  
	 
	WHILE (@@FETCH_STATUS = 0) --loop while good records are returned 
	BEGIN 
		IF (@PeerFlag = 1) 
		BEGIN 
			--put node id in a temp table so we can move it back later 
			INSERT INTO @PeerTable (CategoryID) VALUES (@LoopCategoryID) 
			--move the node to the cache 
			EXEC proc_CategoryMoveToCache @LoopCategoryID 
		END 
		 
		--if the loop id matches the desired node id, flip the flag. 
		--delete the record 
		--from here on out we will start moving nodes 
		IF (@LoopCategoryID = @CategoryID) 
		BEGIN 
			--delete node and all kids 
			--DELETE FROM Category 
			--WHERE CategoryID IN  
			--	(SELECT CategoryID FROM Category WHERE StringSet LIKE 
			--		(SELECT StringSet + '%' FROM Category WHERE CategoryID = @CategoryID) 
			--	) 
			
			--Change from delete to archive
			UPDATE Category 
			SET Archived = 1
			WHERE CategoryID IN  
				(SELECT CategoryID FROM Category WHERE StringSet LIKE 
					(SELECT StringSet + '%' FROM Category WHERE CategoryID = @CategoryID) 
				) 
			
			
			--flip the flag 
			SET @PeerFlag = 1  
		END 
		 
		FETCH NEXT FROM PeerDelCursor 
			INTO @LoopCategoryID, @LoopParentCategoryID  
	END 
	 
	CLOSE PeerDelCursor 
	DEALLOCATE PeerDelCursor 
	 
	--now restore all the nodes we moved to the cache before 
	--this is bizarre.  we can only go record by record with a cursor.  so we will create a 
	--cursor on our temp table variable 
	DECLARE PeerDelCursor CURSOR 
		FOR SELECT CategoryID FROM @PeerTable 
	OPEN PeerDelCursor 
	FETCH NEXT FROM PeerDelCursor  
		INTO @LoopCategoryID 
	WHILE (@@FETCH_STATUS = 0) --loop while good records are returned 
	BEGIN 
		EXEC proc_CategoryUpdateParent @LoopCategoryID, @LoopParentCategoryID 
		 
		FETCH NEXT FROM PeerDelCursor 
			INTO @LoopCategoryID 
	END 
	CLOSE PeerDelCursor 
	DEALLOCATE PeerDelCursor


