﻿/*
Description: select a user profile by user name and password.  for logging in to 
either an administration system or a catalog.
By: kurt herrmann, wirestone llc, 2004
Input: email, password
Output: records which match
Intended use: administration by users.
*/
CREATE PROCEDURE [dbo].[proc_AdminUserSelectByEmailAndPassword]
    @Email nvarchar(50),
    @Password nvarchar(50) 
AS
SELECT   
	AdminUserID,
	LastName,
	FirstName,
	Email,
	Password
FROM AdminUser
WHERE Email = @Email AND Password = @Password
