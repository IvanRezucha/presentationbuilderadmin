﻿CREATE PROCEDURE [proc_UserRegionLanguageDelete]
(
	@Original_UserRegionLanguageID int
)
AS
	SET NOCOUNT OFF;
	DELETE FROM UserRegionLanguage 
	WHERE UserRegionLanguageID = @Original_UserRegionLanguageID
