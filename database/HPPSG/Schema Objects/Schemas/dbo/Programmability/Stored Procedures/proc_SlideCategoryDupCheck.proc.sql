﻿

CREATE PROCEDURE proc_SlideCategoryDupCheck
    @SlideID int,
    @CategoryID int
AS

SELECT   
 COUNT(*)
FROM SlideCategory
WHERE 0 = 0 
AND SlideID = @SlideID
AND CategoryID = @CategoryID


