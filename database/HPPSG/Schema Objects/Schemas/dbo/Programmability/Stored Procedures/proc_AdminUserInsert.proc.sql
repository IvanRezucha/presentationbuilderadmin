﻿CREATE PROCEDURE [dbo].[proc_AdminUserInsert]
(
	@AdminUserID int,
	@LastName nvarchar(50),
	@FirstName nvarchar(50),
	@Email nvarchar(100),
	@Password nvarchar(50)
)
AS
	SET NOCOUNT OFF;
INSERT INTO [AdminUser] ([AdminUserID], [LastName], [FirstName], [Email], [Password]) VALUES (@AdminUserID, @LastName, @FirstName, @Email, @Password);
	
SELECT AdminUserID, LastName, FirstName, Email, Password FROM AdminUser WHERE (AdminUserID = @AdminUserID)
