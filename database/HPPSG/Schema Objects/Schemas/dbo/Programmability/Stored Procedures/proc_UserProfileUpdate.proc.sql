﻿CREATE PROCEDURE [proc_UserProfileUpdate]
(
	@UserID int
	, @LastName nvarchar(50)
	, @FirstName nvarchar(50)
	, @Email nvarchar(100)
	, @Phone nvarchar(50)
	, @JobTitle nvarchar(100)
	, @IsInSurvey bit
	, @InstallDate datetime
	, @CompanyName nvarchar(100)
	, @CompanySize int
	, @Country nvarchar(20)
	, @ReferenceSource nvarchar(500)
	, @LastLoginDate datetime
	, @ContentInstallDuration int
	, @LastContentUpdateDuration int
	, @Original_UserID int
)
AS
	SET NOCOUNT OFF;
	UPDATE UserProfile
	SET 
		LastName = @LastName
		, FirstName = @FirstName
		, Email = @Email
		, Phone = @Phone
		, JobTitle = @JobTitle
		, IsInSurvey = @IsInSurvey
		, InstallDate = @InstallDate
		, CompanyName = @CompanyName
		, CompanySize = @CompanySize
		, Country = @Country
		, ReferenceSource = @ReferenceSource
		, LastLoginDate = @LastLoginDate
		, ContentInstallDuration = @ContentInstallDuration
		, LastContentUpdateDuration = @LastContentUpdateDuration
	WHERE UserID = @Original_UserID;
	SELECT 
		UserID
		, LastName
		, FirstName
		, Email
		, Phone
		, JobTitle
		, IsInSurvey
		, InstallDate
		, CompanyName
		, CompanySize
		, Country
		, ReferenceSource
		, LastLoginDate
		, ContentInstallDuration
		, LastContentUpdateDuration
	FROM UserProfile
	WHERE UserID = @UserID
