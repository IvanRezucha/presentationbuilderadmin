﻿


CREATE  PROCEDURE proc_SlideCategoryInsert
    @SlideID int,
    @CategoryID int,
    @SortOrder int
AS

IF @SortOrder = 0
	SET @SortOrder = (SELECT ISNULL(Max(SortOrder),-1) FROM [SlideCategory]) + 1
INSERT INTO [SlideCategory] (
	SlideID,
	CategoryID,
	SortOrder)
VALUES (
	@SlideID,
	@CategoryID,
	@SortOrder)



