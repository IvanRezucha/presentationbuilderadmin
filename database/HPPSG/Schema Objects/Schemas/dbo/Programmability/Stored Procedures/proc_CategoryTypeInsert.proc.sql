﻿

CREATE PROCEDURE proc_CategoryTypeInsert
    @CategoryType nvarchar(100),
    @Icon nvarchar(50)
AS

INSERT INTO [CategoryType] (
	CategoryType,
	Icon)
VALUES (
	@CategoryType,
	@Icon);

SELECT 'NewRecord' = @@IDENTITY


