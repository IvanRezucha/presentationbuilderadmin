﻿CREATE PROCEDURE [proc_UserProfileInsert]
(
	@LastName nvarchar(50)
	, @FirstName nvarchar(50)
	, @Email nvarchar(100)
	, @Phone nvarchar(50)
	, @JobTitle nvarchar(100)
	, @IsInSurvey bit
	, @InstallDate datetime
	, @CompanyName nvarchar(100)
	, @CompanySize int
	, @Country nvarchar(20)
	, @ReferenceSource nvarchar(500)
	, @LastLoginDate datetime
	, @ContentInstallDuration int
	, @LastContentUpdateDuration int
)
AS
	SET NOCOUNT OFF;
	
	INSERT INTO UserProfile
	(
		LastName
		, FirstName
		, Email
		, Phone
		, JobTitle
		, IsInSurvey
		, InstallDate
		, CompanyName
		, CompanySize
		, Country
		, ReferenceSource
		, LastLoginDate
		, ContentInstallDuration
		, LastContentUpdateDuration
	)
	VALUES
	(
		@LastName
		, @FirstName
		, @Email
		, @Phone
		, @JobTitle
		, @IsInSurvey
		, @InstallDate
		, @CompanyName
		, @CompanySize
		, @Country
		, @ReferenceSource
		, @LastLoginDate
		, @ContentInstallDuration
		, @LastContentUpdateDuration
	);
	SELECT 
		UserID
		, LastName
		, FirstName
		, Email
		, Phone
		, JobTitle
		, IsInSurvey
		, InstallDate
		, CompanyName
		, CompanySize
		, Country
		, ReferenceSource
		, LastLoginDate
		, ContentInstallDuration
		, LastContentUpdateDuration
	FROM UserProfile
	WHERE UserID = SCOPE_IDENTITY()
