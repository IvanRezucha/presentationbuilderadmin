﻿

/*
Description: procedure to move a node to the temporary cache node location. 
used primarily for deletes and mass reassignments of subtrees 
so that instead of manual updates, the trigger fires when we move stuff around 
and takes care of it automatically 
By: kurt herrmann, wirestone llc, 2004
Input: category id
Output: void
Intended use: administration.
*/
CREATE  PROCEDURE proc_CategoryMoveToCache(@CategoryID int) AS 

	DECLARE @NewParentID int 
	 
	SET @NewParentID = (SELECT ParentCategoryID FROM func_CategorySelectCacheNode() ) 
 
	UPDATE Category SET ParentCategoryID = @NewParentID 
	WHERE CategoryID = @CategoryID 
	 
	--SELECT 'NewParentID' = @NewParentID


