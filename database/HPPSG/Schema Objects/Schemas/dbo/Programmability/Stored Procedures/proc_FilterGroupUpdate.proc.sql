﻿
CREATE PROCEDURE dbo.proc_FilterGroupUpdate
(
	@FilterGroup nvarchar(250),
	@Original_FilterGroupID int,
	@Original_FilterGroup nvarchar(250),
	@FilterGroupID int
)
AS
	SET NOCOUNT OFF;
UPDATE [dbo].[FilterGroup] SET [FilterGroup] = @FilterGroup WHERE (([FilterGroupID] = @Original_FilterGroupID) AND ([FilterGroup] = @Original_FilterGroup));
	
SELECT FilterGroupID, FilterGroup FROM FilterGroup WHERE (FilterGroupID = @FilterGroupID)
