﻿

CREATE PROCEDURE [dbo].[proc_CategoryInsert]
(
	@Category nvarchar(250),
	@IsNew bit,
	@Icon nvarchar(100),
	@ShowDescendantSlides bit,
	@ParentCategoryID int,
	@NewID int output
)
AS
	SET NOCOUNT OFF;
INSERT INTO Category 
	(Category, IsNew, Icon, ShowDescendantSlides, ParentCategoryID) 
VALUES 
	(@Category, @IsNew, @Icon, @ShowDescendantSlides, @ParentCategoryID);
	
SET @NewID = @@IDENTITY

