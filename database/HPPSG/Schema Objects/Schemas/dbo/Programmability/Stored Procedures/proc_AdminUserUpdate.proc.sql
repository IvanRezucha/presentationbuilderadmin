﻿CREATE PROCEDURE [dbo].[proc_AdminUserUpdate]
(
	@AdminUserID int,
	@LastName nvarchar(50),
	@FirstName nvarchar(50),
	@Email nvarchar(100),
	@Password nvarchar(50),
	@Original_AdminUserID int
)
AS
	SET NOCOUNT OFF;
UPDATE [AdminUser] SET [AdminUserID] = @AdminUserID, [LastName] = @LastName, [FirstName] = @FirstName, [Email] = @Email, [Password] = @Password WHERE (([AdminUserID] = @Original_AdminUserID));
	
SELECT AdminUserID, LastName, FirstName, Email, Password FROM AdminUser WHERE (AdminUserID = @AdminUserID)