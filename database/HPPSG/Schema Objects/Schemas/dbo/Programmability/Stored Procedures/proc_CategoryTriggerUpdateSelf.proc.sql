﻿




/*
NEVER CALL DIRECTLY!
Description: procedure to update stringset and depth for recursive nodes when trigger fires on updates and inserts
By: kurt herrmann, wirestone llc, 2004
Input: category whose kids may or may not need to be updated, maximum depth
Output: none
Intended use: system administration, hierarchy maintenance.  when a node is inserted or updated, a trigger fires which calls 
proc_CategoryTriggerUpdateSelf.  DO NOT USE THIS PROC!
*/
CREATE  PROCEDURE proc_CategoryTriggerUpdateSelf
	@CategoryID int
AS


DECLARE @ParentCategoryID int
SELECT @ParentCategoryID = ParentCategoryID FROM Category WHERE CategoryID=@CategoryID
DECLARE @ParentCategoryDepth int, @ParentCategoryStringSet nvarchar(255), @OldStringSet nvarchar(255), @NewStringSet nvarchar(255), @OldDepth int
SET @ParentCategoryDepth = 0
SET @ParentCategoryStringSet = ''
SELECT @ParentCategoryDepth=Depth, @ParentCategoryStringSet=StringSet FROM Category WHERE CategoryID=@ParentCategoryID
SELECT @OldStringSet=StringSet, @OldDepth=Depth FROM Category WHERE CategoryID=@CategoryID
--let's rework this logic since we've been having spotty results using functions like LEFT on unicode data...
declare @NewCodeNeeded int, @counter int
--if it's a new record the value of @olddepth will be null so we need to fire up the new stringset
--in all cases if the ParentCategorydepth and Category's depth are not a difference of 1, we need an update.
if (@ParentCategoryDepth = 0) or (@olddepth is null) or (@olddepth - @ParentCategorydepth <> 1)
BEGIN
	SET @NewCodeNeeded = 1
END
ELSE
	--if their depth isn't the ParentCategory depth + 1 they need a new stringset
	set @NewCodeNeeded = 0
	set @counter = 1
	
	
	while (@counter <= @ParentCategoryDepth)
	BEGIN
		IF    (UNICODE(SUBSTRING(@OldStringSet,@Counter,1)) <> UNICODE(SUBSTRING(@ParentCategoryStringSet,@Counter,1)))
		BEGIN
			SET @NewCodeNeeded = 1
			BREAK
		END
		SET @Counter = @Counter + 1
	END

IF (@NewCodeNeeded = 1)
BEGIN
   	DECLARE @NewCodeChar nchar(1)
   	SELECT TOP 1 @NewCodeChar=CodeChar FROM Code 
   	WHERE CodeChar NOT IN 
   		(SELECT SUBSTRING(StringSet,@ParentCategoryDepth+1,1) FROM Category 
  		 WHERE ParentCategoryID=@ParentCategoryID     AND CategoryID<>@CategoryID)
	--if there are no code characters left, use the highest possible value so there's still a fighting chance this will sort correctly.
	--in all honesty, if there are no code characters left we should throw an error.
	if ISNULL(@NewCodeChar,nchar(1)) = nchar(1)
	begin
		raiserror('The new code character is bad. Params were @ParentCategoryDepth of %d, @parentCategoryID of %d, and @CategoryID of %d',16,1,@ParentCategoryDepth, @ParentCategoryID, @CategoryID)
		select TOP 1 @NewCodeChar = CodeChar FROM Code
	end
   	SET @NewStringSet = @ParentCategoryStringSet+ISNULL(@NewCodeChar,nchar(65535))
   
   	BEGIN TRAN
   		UPDATE Category SET Depth=@ParentCategoryDepth+1, @OldStringSet=StringSet, 
   			StringSet=@NewStringSet WHERE CategoryID=@CategoryID
   		
		IF (@OldStringSet <> @NewStringSet) 
   		BEGIN
      			EXEC proc_CategoryTriggerUpdateChildren @CategoryID
   		END
		
   	COMMIT TRAN
END




