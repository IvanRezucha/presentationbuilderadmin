﻿




CREATE    PROCEDURE proc_CategoryInsert4XmlRead
    @CategoryTypeID int,
    @Category nvarchar(250),
    @Description nvarchar(3000),
    --@StringSet nvarchar(250),
    --@Depth int,
    @ParentCategoryID int,
    @IsProtected bit,
    @SystemName nvarchar(50),
    @Href nvarchar(200)
	--@IsNew bit = 0,
	--@Icon nvarchar(100),
	--@NewID int output
AS

INSERT INTO [Category] (
	CategoryTypeID,
	Category,
	Description,
	--StringSet,
	--Depth,
	ParentCategoryID,
	IsProtected,
	SystemName,
	Href)
	--IsNew,
	--Icon)
VALUES (
	@CategoryTypeID,
	@Category,
	@Description,
	--@StringSet,
	--@Depth,
	@ParentCategoryID,
	@IsProtected,
	@SystemName,
	@Href)
	--@IsNew,
	--@Icon)

SELECT @@IDENTITY
--SET @NewID = @@IDENTITY
--SELECT 'NewRecord' = @@IDENTITY



