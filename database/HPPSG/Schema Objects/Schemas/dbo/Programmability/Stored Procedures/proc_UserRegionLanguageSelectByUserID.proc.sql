﻿CREATE PROCEDURE [proc_UserRegionLanguageSelectByUserID] 
(
	@UserID int
)
AS
	SET NOCOUNT ON;
	SELECT 
		UserRegionLanguageID
		, UserID
		, RegionLanguageID
	FROM UserRegionLanguage
	WHERE UserID = @UserID
