﻿

CREATE PROCEDURE proc_CategoryTypeUpdate
    @CategoryTypeID int,
    @CategoryType nvarchar(100),
    @Icon nvarchar(50)
AS

UPDATE [CategoryType]
SET
	CategoryType = @CategoryType,
	Icon = @Icon
WHERE CategoryTypeID = @CategoryTypeID


