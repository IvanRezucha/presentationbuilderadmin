﻿

CREATE PROCEDURE proc_CategorySelectAll

AS

SELECT   
	CategoryID,
	CategoryTypeID,
	Category,
	Description,
	StringSet,
	Depth,
	ParentCategoryID,
	IsProtected,
	SystemName,
	Href
FROM Category


