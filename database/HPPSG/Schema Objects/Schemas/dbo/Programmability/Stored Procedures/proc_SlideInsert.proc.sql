﻿

CREATE PROCEDURE proc_SlideInsert
    @Title nvarchar(500),
    @FileName nvarchar(100)
AS

INSERT INTO [Slide] (
	Title,
	FileName)
VALUES (
	@Title,
	@FileName);

SELECT 'NewRecord' = @@IDENTITY


