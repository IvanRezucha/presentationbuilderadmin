﻿
CREATE PROCEDURE dbo.proc_FilterInsert
(
	@Filter nvarchar(250),
	@FilterGroupID int
)
AS
	SET NOCOUNT OFF;
INSERT INTO [Filter] ([Filter], [FilterGroupID]) VALUES (@Filter, @FilterGroupID);
	
SELECT FilterID, Filter, FilterGroupID FROM Filter WHERE (FilterID = SCOPE_IDENTITY())
