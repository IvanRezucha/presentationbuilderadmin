﻿

CREATE PROCEDURE proc_CategoryDupCheck
    @CategoryID int,
    @Category nvarchar(250),
    @IsProtected bit
AS

SELECT   
 CategoryID  
FROM Category
WHERE 0 = 0 
AND CategoryID <> @CategoryID
AND Category = @Category
AND IsProtected = @IsProtected


