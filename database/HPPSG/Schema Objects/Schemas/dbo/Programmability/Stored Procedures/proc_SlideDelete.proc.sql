﻿


CREATE  PROCEDURE proc_SlideDelete
    @SlideID int
AS

--first we need to delete all categories whose sole contents are this slideID
--because of CASCADE on DELETE for the Category-SlideCategory relationship
--the associated SlideCategories will also be deleted

DECLARE @SlideCats TABLE
(
	CategoryID int,
	CategorySlides int
)

INSERT INTO @SlideCats (CategoryID, CategorySlides)
	SELECT SlideCategory.CategoryID, COUNT(SlideCategory.CategoryID) AS CategorySlides
	FROM SlideCategory
	WHERE SlideCategory.CategoryID IN
		(SELECT SlideCategory.CategoryID
		FROM SlideCategory
		WHERE SlideCategory.CategoryID IN 
				(SELECT SlideCategory.CategoryID
				FROM SlideCategory
				WHERE SlideID = @SlideID))
	GROUP BY SlideCategory.CategoryID

DELETE FROM Category
WHERE CategoryID IN
	(SELECT CategoryID
	FROM @SlideCats
	WHERE CategorySlides = 1)

--now we delete the slide
DELETE FROM Slide 
WHERE SlideID = @SlideID





