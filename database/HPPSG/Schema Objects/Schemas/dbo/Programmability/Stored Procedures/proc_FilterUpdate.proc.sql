﻿
CREATE PROCEDURE dbo.proc_FilterUpdate
(
	@Filter nvarchar(250),
	@FilterGroupID int,
	@Original_FilterID int,
	@FilterID int
)
AS
	SET NOCOUNT OFF;
UPDATE [Filter] SET [Filter] = @Filter, [FilterGroupID] = @FilterGroupID WHERE (([FilterID] = @Original_FilterID));
	
SELECT FilterID, Filter, FilterGroupID FROM Filter WHERE (FilterID = @FilterID)
