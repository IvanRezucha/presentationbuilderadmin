﻿

CREATE PROCEDURE proc_CategorySelect
    @CategoryID int
AS

SELECT   
	CategoryID,
	CategoryTypeID,
	Category,
	Description,
	StringSet,
	Depth,
	ParentCategoryID,
	IsProtected,
	SystemName,
	Href,
	IsNew,
	Icon
FROM Category
WHERE CategoryID = @CategoryID


