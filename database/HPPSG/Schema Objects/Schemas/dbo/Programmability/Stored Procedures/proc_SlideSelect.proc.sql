﻿

CREATE PROCEDURE proc_SlideSelect
    @SlideID int
AS

SELECT   
	SlideID,
	Title,
	FileName,
	DateModified
FROM Slide
WHERE SlideID = @SlideID


