﻿CREATE PROCEDURE [proc_UserProfileSelectAll] 
AS
	SET NOCOUNT ON;
	SELECT 
		UserID
		, LastName
		, FirstName
		, Email
		, Phone
		, JobTitle
		, IsInSurvey
		, InstallDate
		, CompanyName
		, CompanySize
		, Country
		, ReferenceSource
		, LastLoginDate
		, ContentInstallDuration
		, LastContentUpdateDuration
	FROM UserProfile
