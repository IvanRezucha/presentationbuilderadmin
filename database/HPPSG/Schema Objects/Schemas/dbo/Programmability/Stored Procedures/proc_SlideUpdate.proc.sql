﻿

CREATE PROCEDURE proc_SlideUpdate
    @SlideID int,
    @Title nvarchar(500),
    @FileName nvarchar(100)
AS

UPDATE [Slide]
SET
	Title = @Title,
	FileName = @FileName
WHERE SlideID = @SlideID


