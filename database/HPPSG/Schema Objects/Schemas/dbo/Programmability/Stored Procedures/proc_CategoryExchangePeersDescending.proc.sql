﻿



/*
Description: swap two adjacent nodes in a peer group
By: kurt herrmann, wirestone llc, 2004
Input: node to demote
Output: void
Intended use: administration - sorting within a peer group 
*/
CREATE PROCEDURE proc_CategoryExchangePeersDescending 
	@CategoryID int 
AS 
 
/* 
if the tree is viewed like this: 
	root 
	  | 
         -------------- 
         |             | 
    node1      node2 
 
then this SP operation will: 
	-move node1 to the cache node 
	-move node2 to the cache node 
	-move node1 to node2's place 
	-move node2 to node1's place 
*/ 
 
 
--declare some local variables we can use for checking field values 
DECLARE @CurrentCategoryID int -- will temp hold current row Categoryid as we loop 
DECLARE @ParentCategoryID int -- will hold the parent cat as soon as we fetch it 
DECLARE @SwitchCategoryID int -- will hold the cat we will want to switch with  
 
--first, get all the peers 
DECLARE PeerCursor SCROLL CURSOR 
	FOR SELECT CategoryID, ParentCategoryID FROM func_CategorySelectPeerGroup(@CategoryID) 
OPEN PeerCursor 
FETCH LAST FROM PeerCursor  
	INTO @CurrentCategoryID, @ParentCategoryID  
 
--make sure that the desired node to move is not the LAST -- we can't do that! 
IF (@CurrentCategoryID <> @CategoryID) 
BEGIN 
	--now find the position of the desired record in the peers 
	WHILE (@@FETCH_STATUS = 0) --loop while good records are returned 
	BEGIN 
		IF (@CurrentCategoryID = @CategoryID) 
		BEGIN 
			FETCH NEXT FROM PeerCursor 
				INTO @SwitchCategoryID, @ParentCategoryID 
			--SELECT 'shizzy' = @SwitchCategoryID 
			BREAK 
		END 
		FETCH PRIOR FROM PeerCursor 
			INTO @CurrentCategoryID, @ParentCategoryID  
	END 
 
	--now switch them out. 
	--move the one we are trying to switch with to the temp node 
	EXEC proc_CategoryMoveToCache @SwitchCategoryID 
	--move desired to temp node 
	EXEC proc_CategoryMoveToCache @CategoryID 
	--now just reassign the parent for each of them 
	EXEC proc_CategoryUpdateParent @SwitchCategoryID, @ParentCategoryID 
	EXEC proc_CategoryUpdateParent @CategoryID, @ParentCategoryID 
	--done 
END 
CLOSE PeerCursor 
DEALLOCATE PeerCursor

