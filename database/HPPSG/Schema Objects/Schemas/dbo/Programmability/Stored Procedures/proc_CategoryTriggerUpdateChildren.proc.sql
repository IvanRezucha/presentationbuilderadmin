﻿




/*
NEVER CALL DIRECTLY!
Description: procedure to update stringset and depth for recursive nodes when trigger fires on updates and inserts
By: kurt herrmann, wirestone llc, 2004
Input: category whose kids may or may not need to be updated, maximum depth
Output: none
Intended use: system administration, hierarchy maintenance.  when a node is inserted or updated, a trigger fires which calls 
proc_CategoryTriggerUpdateSelf which may call this proc.  DO NOT USE THIS PROC!
*/
CREATE  PROCEDURE proc_CategoryTriggerUpdateChildren 
(
	@ParentCategoryID int = NULL, 
	@MaxDepth int = 64
) 
AS 
DECLARE @StartDepth int, @ParentDepth int, @ParentStringSet nvarchar(255)
SET @ParentDepth = NULL

SELECT @ParentDepth = Depth, @ParentStringSet = StringSet
FROM Category
WHERE CategoryID = @ParentCategoryID 

IF (@ParentDepth IS NULL) 
BEGIN
	SET @ParentDepth = 0
	SET @ParentStringSet = '' 
END
SET @StartDepth = @ParentDepth 
DECLARE @CurrentDepth int
SET @CurrentDepth = @ParentDepth + 1 

DECLARE @StackTable TABLE (CategoryID int, Depth int) 
DECLARE @ValueTable TABLE (Depth int, ParentStringSet nvarchar(255), Offset int) 
DECLARE @SingleCategoryID int, @SingleDepth int 
DECLARE @NewStringSet nvarchar(255), @OldStringSet nvarchar(255)
INSERT INTO @StackTable (CategoryID, Depth)
	SELECT CategoryID, @CurrentDepth
	FROM Category
	WHERE ParentCategoryID = @ParentCategoryID
	
INSERT 
	INTO @ValueTable (Depth, ParentStringSet, Offset)
	VALUES (@CurrentDepth, @ParentStringSet, 0) 

DECLARE @Offset int WHILE (@CurrentDepth > @StartDepth) 
BEGIN
	SELECT @Offset = Offset, @ParentStringSet = ParentStringSet
	FROM @ValueTable
	WHERE Depth = @CurrentDepth
	
	SET @SingleCategoryID = NULL
	
	SELECT TOP 1 @SingleCategoryID = CategoryID
	FROM @StackTable
	WHERE Depth = @CurrentDepth 
	IF (@SingleCategoryID IS NOT NULL) 
	BEGIN
		UPDATE @ValueTable
		SET Offset = (@Offset + 1)
		WHERE Depth = @CurrentDepth
		
		SELECT @NewStringSet = @ParentStringSet + CodeChar
		FROM Code
		WHERE CodeKey = @Offset + 1
		
		UPDATE Category
		SET Depth = @CurrentDepth, @OldStringSet = StringSet, StringSet = @NewStringSet
		WHERE CategoryID = @SingleCategoryID
		
		DELETE FROM @StackTable
		WHERE CategoryID = @SingleCategoryID 
		
		IF (@NewStringSet <> @OldStringSet) 
		BEGIN
			SET @CurrentDepth = @CurrentDepth + 1 
			IF ((@CurrentDepth - @StartDepth) > @MaxDepth) 
			BEGIN 
				RAISERROR ('The maximum recursive levels of %d was exceeded building nested set', 16, 1, @MaxDepth) 
			END
			
			INSERT INTO @ValueTable (Depth, ParentStringSet, Offset)
			VALUES (@CurrentDepth, @NewStringSet, 0)
			
			INSERT INTO @StackTable (CategoryID, Depth)
			SELECT CategoryID, @CurrentDepth
			FROM Category
			WHERE ParentCategoryID = @SingleCategoryID 
		END 
	END 
	ELSE 
	BEGIN
		DELETE FROM @ValueTable
		WHERE Depth = @CurrentDepth
		SET @CurrentDepth = @CurrentDepth - 1 
	END 
END




