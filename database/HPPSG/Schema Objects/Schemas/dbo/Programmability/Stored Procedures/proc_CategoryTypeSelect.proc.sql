﻿

CREATE PROCEDURE proc_CategoryTypeSelect
    @CategoryTypeID int
AS

SELECT   
	CategoryTypeID,
	CategoryType,
	Icon
FROM CategoryType
WHERE CategoryTypeID = @CategoryTypeID


