﻿CREATE PROCEDURE [proc_UserProfileSelectByUserID]
(
	@UserID int
)
AS
	SELECT 
		UserID
		, LastName
		, FirstName
		, Email
		, Phone
		, JobTitle
		, IsInSurvey
		, InstallDate
		, CompanyName
		, CompanySize
		, Country
		, ReferenceSource
		, LastLoginDate
		, ContentInstallDuration
		, LastContentUpdateDuration
	FROM UserProfile
	WHERE UserID = @UserID
