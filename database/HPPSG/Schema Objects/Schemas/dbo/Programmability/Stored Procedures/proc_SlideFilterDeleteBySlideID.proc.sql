﻿
CREATE PROCEDURE dbo.proc_SlideFilterDeleteBySlideID
(
	@SlideID int
)
AS
	SET NOCOUNT OFF;
DELETE FROM SlideFilter
WHERE     (SlideID = @SlideID)
