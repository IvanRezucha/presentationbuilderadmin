﻿
CREATE PROCEDURE dbo.proc_SlideFilterInsert
(
	@FilterID int,
	@SlideID int
)
AS
	SET NOCOUNT OFF;
INSERT INTO [dbo].[SlideFilter] ([FilterID], [SlideID]) VALUES (@FilterID, @SlideID);
	
SELECT FilterID, SlideID FROM SlideFilter WHERE (FilterID = @FilterID) AND (SlideID = @SlideID)
