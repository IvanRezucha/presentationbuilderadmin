﻿

CREATE PROCEDURE [dbo].[proc_CategoryUpdate]
(
	@Category nvarchar(250),
	@IsNew bit,
	@Icon nvarchar(100),
	@ShowDescendantSlides bit,
	@ParentCategoryID int,
	@Original_CategoryID int,
	@CategoryID int
)
AS
	SET NOCOUNT OFF;
UPDATE Category 
SET 
	Category = @Category, 
	IsNew = @IsNew, 
	Icon = @Icon, 
	ShowDescendantSlides = @ShowDescendantSlides, 
	ParentCategoryID = @ParentCategoryID 
WHERE ((CategoryID = @Original_CategoryID));
	
