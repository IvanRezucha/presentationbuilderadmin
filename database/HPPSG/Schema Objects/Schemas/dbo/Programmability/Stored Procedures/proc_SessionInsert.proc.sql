﻿CREATE PROCEDURE [dbo].[proc_SessionInsert]
	@SessionID bigint = 0 output,
	@Session varchar(36),
	@UserID int,
	@StartDate datetime
AS
BEGIN
	SET NOCOUNT ON
	insert into Sessions (
		UserID, Session, StartDate
	) values (
		@UserID, @Session, @StartDate
	)
	select @SessionID = @@IDENTITY
	select @SessionID as SessionID
END

