﻿
CREATE PROCEDURE dbo.proc_SildeFilterDelete
(
	@Original_FilterID int,
	@Original_SlideID int
)
AS
	SET NOCOUNT OFF;
DELETE FROM [dbo].[SlideFilter] WHERE (([FilterID] = @Original_FilterID) AND ([SlideID] = @Original_SlideID))
