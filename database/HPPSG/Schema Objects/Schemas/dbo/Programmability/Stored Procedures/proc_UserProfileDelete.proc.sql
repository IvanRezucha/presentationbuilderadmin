﻿CREATE PROCEDURE [proc_UserProfileDelete]
(
	@Original_UserID int
)
AS
	SET NOCOUNT OFF;
	DELETE FROM UserProfile 
	WHERE UserID = @Original_UserID
