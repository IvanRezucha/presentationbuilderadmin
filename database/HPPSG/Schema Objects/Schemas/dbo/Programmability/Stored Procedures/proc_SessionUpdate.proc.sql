﻿
CREATE PROCEDURE [dbo].[proc_SessionUpdate]
	@Session varchar(36),
	@EndDate datetime
AS
BEGIN
	update Sessions set EndDate = @EndDate
	where Session = @Session
END

