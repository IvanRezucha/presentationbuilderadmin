﻿
CREATE PROCEDURE dbo.proc_SlideFilterUpdate
(
	@FilterID int,
	@SlideID int,
	@Original_FilterID int,
	@Original_SlideID int
)
AS
	SET NOCOUNT OFF;
UPDATE [dbo].[SlideFilter] SET [FilterID] = @FilterID, [SlideID] = @SlideID WHERE (([FilterID] = @Original_FilterID) AND ([SlideID] = @Original_SlideID));
	
SELECT FilterID, SlideID FROM SlideFilter WHERE (FilterID = @FilterID) AND (SlideID = @SlideID)
