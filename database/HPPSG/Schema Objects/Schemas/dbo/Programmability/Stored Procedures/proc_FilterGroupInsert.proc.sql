﻿
CREATE PROCEDURE dbo.proc_FilterGroupInsert
(
	@FilterGroup nvarchar(250)
)
AS
	SET NOCOUNT OFF;
INSERT INTO [dbo].[FilterGroup] ([FilterGroup]) VALUES (@FilterGroup);
	
SELECT FilterGroupID, FilterGroup FROM FilterGroup WHERE (FilterGroupID = SCOPE_IDENTITY())
