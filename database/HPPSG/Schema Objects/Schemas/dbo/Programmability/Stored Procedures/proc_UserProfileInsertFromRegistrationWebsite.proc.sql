﻿

CREATE PROCEDURE [dbo].[proc_UserProfileInsertFromRegistrationWebsite]
    @firstName nvarchar(100),
    @lastName nvarchar(100),
    @email nvarchar(200),   
    @phone nvarchar(20),
    @jobRole nvarchar(100),
    @isInSurvey bit
AS

SET NOCOUNT ON

-- If this is the first time registering then insert the user
IF ((SELECT COUNT(email) FROM UserProfile WHERE email = @email) = 0)
BEGIN
	INSERT INTO UserProfile (
		firstName,
		lastName,
		email,
		phone,
		jobTitle,
		isInSurvey,
		installDate)
	VALUES (
		@firstName,
		@lastName,
		@email,
		@phone,
		@jobRole,
		@isInSurvey,
		'8/2/2006');
	--insert into HpPbLaserJet.dbo.userprofile
END
-- This user has registered before so just update
ELSE
BEGIN
	UPDATE UserProfile 
	SET firstName = @firstName,
		lastName = @lastName,
		email = @email,
		phone = @phone,
		jobTitle = @jobRole,
		isInSurvey = @isInSurvey
	WHERE email = @email 
END

