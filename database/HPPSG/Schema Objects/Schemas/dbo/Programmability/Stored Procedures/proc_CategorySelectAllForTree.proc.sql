﻿CREATE PROCEDURE [dbo].[proc_CategorySelectAllForTree]
AS
	SET NOCOUNT ON;
SELECT     CategoryID, CategoryTypeID, Category, IsNew, Icon, ShowDescendantSlides, ParentCategoryID
FROM         Category
WHERE (Depth >= 3)
ORDER BY StringSet

