﻿CREATE PROCEDURE [proc_UserRegionLanguageInsert]
(
	@UserID int
	, @RegionLanguageID int
)
AS
	SET NOCOUNT OFF;
	
	INSERT INTO UserRegionLanguage
	(
		UserID
		, RegionLanguageID
	)
	VALUES
	(
		@UserID
		, @RegionLanguageID
	);
	SELECT 
		UserRegionLanguageID
		, UserID
		, RegionLanguageID
	FROM UserRegionLanguage
	WHERE UserRegionLanguageID = SCOPE_IDENTITY()
