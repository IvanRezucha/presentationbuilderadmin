﻿CREATE PROCEDURE [proc_UserRegionLanguageSelectByUserRegionLanguageID]
(
	@UserRegionLanguageID int
)
AS
	SELECT 
		UserRegionLanguageID
		, UserID
		, RegionLanguageID
	FROM UserRegionLanguage
	WHERE UserRegionLanguageID = @UserRegionLanguageID
