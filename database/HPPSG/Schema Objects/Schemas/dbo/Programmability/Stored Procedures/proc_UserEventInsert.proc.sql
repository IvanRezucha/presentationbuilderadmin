﻿
CREATE PROCEDURE [dbo].[proc_UserEventInsert]
	@EventID bigint = 0 output,
	@SlideID int,
	@Session varchar(36),
	@TypeID int,
	@CategoryID int = null,
	@Tag varchar(255),
	@EventDate datetime
AS
BEGIN
	SET NOCOUNT ON;
	
	if exists(select 0 from Sessions where Session = @Session)
	begin
		insert into Events (
			SlideID, Session, TypeID, CategoryID, Tag, EventDate
		) values (
			@SlideID, @Session, @TypeID, @CategoryID, @Tag, @EventDate
		)
		select @EventID = @@IDENTITY
		select @EventID as EventID
	end
	else
		select 0
END

