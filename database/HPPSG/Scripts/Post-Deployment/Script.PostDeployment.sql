﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
BEGIN TRY
	set @GUID = 'XXX'
	if @Error = 0 and not exists (Select 1 from Deployment.ScriptChange where GUID = @GUID)
	BEGIN
		--STUFF GOES HERE!!!
		
		insert into Deployment.ScriptChange (GUID)
		Select @GUID
	END
	
END TRY
BEGIN CATCH
	print 'ERROR - ' + ERROR_MESSAGE()

	set @Error = 1
END CATCH
*/

Declare @GUID char(36)
Declare @Error bit = 0

BEGIN TRANSACTION

/*
BEGIN TRY
	set @GUID = 'XXX'
	if @Error = 0 and not exists (Select 1 from Deployment.ScriptChange where GUID = @GUID)
	BEGIN
		--STUFF GOES HERE!!!
		
		insert into Deployment.ScriptChange (GUID)
		Select @GUID
	END
	
END TRY
BEGIN CATCH
	print 'ERROR - ' + ERROR_MESSAGE()

	set @Error = 1
END CATCH
*/


IF @Error = 1
BEGIN
	ROLLBACK TRANSACTION
END
ELSE
BEGIN
	COMMIT TRANSACTION
END